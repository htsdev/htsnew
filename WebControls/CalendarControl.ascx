﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CalendarControl.ascx.cs"
    Inherits="DTP.WebControls.CalendarControl" %>
<%--<link href="../Styles.css" rel="stylesheet" type="text/css" />

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>--%>

<link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen">

<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
<%--<aspnew:UpdatePanel ID="upnl_CalendarCntrl" runat="server">
    <ContentTemplate>--%>
        <div class="input-group"<%-- style="width: 200px"--%>>
            <input runat="server" id="txtDate" type="text" class="form-control datepicker" onchange ="date_changed"  data-format="mm/dd/yyyy">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    <%--</ContentTemplate>
    <Triggers>
       <%-- <aspnew:AsyncPostBackTrigger ControlID="btn_sendsms" EventName="Click" />--%>
    <%--</Triggers>--%>
<%--</aspnew:UpdatePanel>--%>           

<%--<aspnew:UpdatePanel ID="upnl_SMSCntrl" runat="server">
    <ContentTemplate>
        <table width="100%" class="clsLeftPaddingTable">
            <tr>
                <td valign="bottom" background="../Images/subhead_bg.gif">
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 26px" class="clssubhead">
                                    Send SMS Alert
                                </td>
                                <td align="right">
                                    <asp:LinkButton ID="lbtn_close2" runat="server" OnClientClick="return HidePopup();">X</asp:LinkButton>
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_SmsError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="rb_existingContacts" runat="server" Text="Existing Contact Numbers"
                        CssClass="clssubhead" GroupName="SMSGROUP" Checked="true" onclick = "RefreshControl('rb_existingContacts');" />
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                    <tr id="lblMsg" runat="server">
                        <td>
                            <asp:Label ID="lblexisting" runat="server" CssClass="Label" ForeColor="Red" Visible= "false"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trContact1" runat="server">
                        <td>
                        <table>
                        <tr>
                            <td style="width: 10px">
                            
                            </td>
                            
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact1_1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>                                    
                                </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact1_2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="TxtContact1_3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_SendSMS1" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_SendSMS1" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_SendSMS1" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                        </td>
                    </tr>
                    </table>
                    
                </td>
            </tr>
            <tr id="trContact2" runat="server">
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact2_1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact2_2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="TxtContact2_3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_SendSMS2" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_SendSMS2" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_SendSMS2" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trContact3" runat="server">
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact3_1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact3_2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="TxtContact3_3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_SendSMS3" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_SendSMS3" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_SendSMS3" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="rb_other" runat="server" Text="Other Contact Numbers" CssClass="clssubhead"
                        GroupName="SMSGROUP" onclick = "return RefreshControl('rb_other');" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="txtOther1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="txtOther2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtOther3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_Other" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_Other" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_Other" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td>
                                <asp:TextBox ID="TxtMessage" runat="server" MaxLength="160" TextMode="MultiLine"
                                    Width="330px" Height="150px" CssClass="clsInputadministration"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td>
                                <asp:Button ID="btn_sendsms" runat="server" Text="Send SMS" CssClass="clsbutton"
                                    OnClick="btn_sendsms_Click" OnClientClick="return ValidateControl('smscontrl')" />
                                <asp:Button ID="btn_close" runat="server" Text="Close" CssClass="clsbutton" OnClientClick="return HidePopup();" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <aspnew:AsyncPostBackTrigger ControlID="btn_sendsms" EventName="Click" />
    </Triggers>
</aspnew:UpdatePanel>--%>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using HTP.Components;
using System.Web.UI.HtmlControls;

namespace lntechNew.WebControls
{
    //Waqas 6666 10/29/2009 added page method
    public delegate void PageMethodHandler();
    public partial class UpdateViolationSRV : System.Web.UI.UserControl
    {

        #region Events

        // Page Event Handler    


        public event PageMethodHandler PageMethod;

        // Page Load Event 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Sabir Khan 8930 02/17/2011 allow updating status from pre trial from disposition page.
                if (iscasedisposition == false)
                    hf_IsDispositionpage.Value = "0";
                else
                    hf_IsDispositionpage.Value = "1";
                ResetControl();

            }

            else
            {
                ResetControl();
                if (hf_courtid.Value != ddl_pcrtloc.SelectedValue)
                    ddl_pcrtloc_SelectedIndexChanged(null, null);

                //Get Pre Trial diversion and Motion Hiring Flags information.
                GetPreTrialDiversionAndMotionHiringflag();

                //Sabir Khan 8862 02/15/2011 JUVNILE flag information
                GetJUVNILEflag();
            }
            // Afaq 8311 09/30/2010 Assign bond violation status count value to hidden field.
            hf_statusCount.Value = BondStatusCount;
        }
        //Sabir 4636 08/23/2008 Method has been created for page load code
        public void ResetControl()
        {
            //Zeeshan Ahmed 3979 5/23/2008 Implement Case Type Changes 
            //Set Default Case Type To Traffic
            //Agha Usman 4271 07/02/2008 - New Case Type Integration
            int caseType = CaseType.Traffic;
            int CaseTypeId = 1;

            //If Case Type Found In Query String
            if (Request.QueryString["casetype"] != null)
                Int32.TryParse(Request.QueryString["casetype"], out CaseTypeId);
            //If Case Number Found Get CaseType From The Case
            else if (Request.QueryString["caseNumber"] != null)
            {
                int TicketId = 0;
                Int32.TryParse(Request.QueryString["caseNumber"], out TicketId);
                CaseTypeId = ClsCase.GetCaseType(TicketId);


            }
            //Set Case Type
            hf_casetype.Value = CaseTypeId.ToString();
            //Yasir Kamal 5748 04/14/2009 Max length set for family law cases
            if (CaseTypeId == 4)
            {
                tb_prm.MaxLength = 3;
            }
            //5748 end

            caseType = CaseTypeId;

            //Nasir 5310 12/29/2008 Assign value to hidden field fro working in Javascript
            int CourtID;
            if (!ddl_pcrtloc.SelectedValue.Equals(""))
            {
                CourtID = Convert.ToInt32(ddl_pcrtloc.SelectedValue);
            }
            else
                CourtID = 0;
            bool IsALRProcess = upnl.IsALRProcess(CourtID);
            hf_IsALRProcess.Value = IsALRProcess.ToString();

            // Get Violations
            FillViolations(0, caseType);
            // Get Courts
            FillCourtLocation(caseType);
            // Get Court Status
            //var courtid = Request.Form("");
            //HiddenField hdnFieldName = this.FindControl("hf_courtid") as HiddenField;
            FillStatus(Convert.ToInt32(hf_courtid.Value), caseType);
            // Set Control Scripts
            setControlScripts();
            // Fill Charge Level
            FillChargeLevel(0);
            //Get NOS Flag Information
            GetNOSFlagInfo();          


            //Waqas 5864 07/30/2009 Active Flag hidden field
            int CaseTicketId = 0;
            Int32.TryParse(Request.QueryString["caseNumber"], out CaseTicketId);
            // Sabir Khan 6286 There is no row at position 0 bug has been fixed...
            if (CaseTicketId > 0)
            {
                ClsCase.TicketID = CaseTicketId;

                if (ClsCase.GetActiveFlag() == 1)
                {
                    hf_activeflag.Value = "1";
                }
            }
            else
            {
                hf_activeflag.Value = "0";
            }
        }

        // Save Violation Event
        protected void btn_popup_Click(object sender, EventArgs e)
        {
            //This Function is Used to Update Violation Information
            updatePanel_update();

            if (PageMethod != null)
                PageMethod();
            // Afaq 8311 09/30/2010 Assign bond violation status count to hidden field.
            hf_statusCount.Value = Convert.ToString(ClsCase.ViolationCountByStatus(CourtViolationStatusType.BOND));
            //Comment By Zeeshan Ahmed
            //No Need To Set This Value Because Popup Has Been Closed
            //if (hf_iscriminalcourt.Value != "1")
            //{
            //    ddl_pvlevel.Text = "0";
            //}

        }

        // Control Pre Render Event
        protected override void OnPreRender(EventArgs e)
        {

            if (!IsPostBack)
            {
                if (ddl_pcrtloc.Items.FindByText("---Choose---") == null)
                    ddl_pstatus.Items.Insert(0, new ListItem("---Choose---", "0"));

                if (!iscasedisposition)
                {
                    ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("DISPOSED"));
                }
            }

        }

        // Court Location Index Changed Event
        protected void ddl_pcrtloc_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetContolLayOut(false);
        }

        protected void lbtn_close_Click(object sender, EventArgs e)
        {
            // TAHIR 4279 06/20/2008
            // FIXED PRICING PLAN ISSUE...
            //SetContolLayOut(true);
            SetContolLayOut(false);
        }

        [System.Web.Services.WebMethod]
        public static string ControlLoad(string name)
        {
            return "Hello " + name + Environment.NewLine + "The Current Time is: "
                + DateTime.Now.ToString();
        }
        #endregion

        #region Variables

        private string error;
        private string gridfield;
        private string gridid;
        private string divid;
        bool iscasedisposition = false;
        string hcviolation;
        string defviolation;
        clsCaseDetail ClsCaseDetail = new clsCaseDetail();
        clsCourts ClsCourts = new clsCourts();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsCase ClsCase = new clsCase();
        //Nasir 5310 12/29/2008 initicalize object to use mettod in this class
        updatePanel upnl = new updatePanel();


        #endregion

        #region Properties


        // Afaq 8311 09/30/2010 Get bond violation status count.
        public string BondStatusCount
        {
            set
            {
                ViewState["BondstatusCount"] = value;
            }
            get { return Convert.ToString(ViewState["BondstatusCount"]); }
        }
        public bool IsCaseDisposition
        {
            set
            {
                iscasedisposition = value;
            }
        }

        public string GridID
        {
            set
            {
                gridid = value;
            }

        }

        public string DivID
        {
            set
            {
                divid = value;
            }
        }

        public string GridField
        {
            set
            {
                gridfield = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return error;
            }
        }

        #endregion

        #region Methods
        // Update Violation Information
        public void updatePanel_update()
        {


            // Initialize Update Panel Class

            //Setting Casuse No and Ticket Number
            ViewState["oldcourtid"] = hf_courtid.Value;
            upnl.CauseNo = tb_causeno.Text;
            upnl.TicketNumber = tb_pticketno.Text;
            /// Added by asghar oscare court 
            upnl.OscareCourtDetail = tb_oscare_court.Text;
            //Setting Violation ID
            upnl.ViolationID = Convert.ToInt32(hf_violationid.Value);
            //Setting Fine And Bond Amount
            upnl.FineAmount = (!string.IsNullOrEmpty(tb_pfineamount.Text)) ? Convert.ToDouble(tb_pfineamount.Text) : 0;
            upnl.BondAmount = (!string.IsNullOrEmpty(tb_pbondamount.Text)) ? Convert.ToDouble(tb_pbondamount.Text) : 0;
            //Setting Bond Flag
            upnl.BondFlag = rbl_bondflag.SelectedValue;
            // Setting Court Date
            if (tb_pmonth.Text == "0" || tb_pmonth.Text == String.Empty || tb_pday.Text == "0" || tb_pday.Text == String.Empty || tb_pyear.Text == "0" || tb_pyear.Text == String.Empty)
            {
                DateTime courtDate = Convert.ToDateTime("01/01/1900");
                upnl.CourtDate = courtDate;
                ViewState["crtdate"] = Convert.ToString(courtDate);
            }
            else
            {
                DateTime courtDate = Convert.ToDateTime(String.Concat(tb_pmonth.Text.ToString() + "/", tb_pday.Text.ToString() + "/", tb_pyear.Text.ToString() + " ", ddl_pcrttime.SelectedValue.ToString()));
                upnl.CourtDate = courtDate;
                ViewState["crtdate"] = Convert.ToString(courtDate);
            }


            //Zeeshan Ahmed 3724 05/13/2008
            DateTime ArrestDate = DateTime.Parse("1/1/1900");

            if (!(tb_arrmonth.Text == "0" || tb_arrmonth.Text == String.Empty || tb_arrday.Text == "0" || tb_arrday.Text == String.Empty || tb_arryear.Text == "0" || tb_arryear.Text == String.Empty))
                ArrestDate = Convert.ToDateTime(String.Concat(tb_arrmonth.Text.ToString() + "/", tb_arrday.Text.ToString() + "/", tb_arryear.Text.ToString()));

            upnl.ArrestDate = ArrestDate;
            // Setting Court Time
            upnl.CourtTime = ddl_pcrttime.SelectedValue;
            //Setting Court Number
            if (tb_prm.Text.ToString() != "")
                upnl.CourtNo = tb_prm.Text.ToString();  //Sabir Khan 7979 07/02/2010 type has been changed from int into string.

            else
                upnl.CourtNo = "0";

            // Added By Zeeshan For Criminal Courts
            if (hf_iscriminalcourt.Value == "1")
            {
                upnl.FineAmount = 0;
                if (hf_Level.Value != "0" && ddl_pvlevel.SelectedValue == "0")
                {
                    ddl_pvlevel.SelectedValue = hf_Level.Value;
                }
                upnl.ChargeLevel = Convert.ToInt32(ddl_pvlevel.SelectedValue);
            }
            else
            {
                upnl.FineAmount = (!string.IsNullOrEmpty(tb_pfineamount.Text)) ? Convert.ToDouble(tb_pfineamount.Text) : 0;
                upnl.ChargeLevel = 0;

            }


            //Setting Court Status
            upnl.Status = Convert.ToInt32(hf_status.Value);
            //Setting Court Location
            upnl.CourtLocation = Convert.ToInt32(hf_courtid.Value);
            //Setting Price Plan
            upnl.PricePlan = Convert.ToInt32(ddl_ppriceplan.SelectedValue);

            if (hf_newviolation.Value == "1")
                upnl.IsNewViolation = true;
            else
                upnl.IsNewViolation = false;

            // Setting Employee ID
            int EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
            upnl.EmpID = EmpID;
            // Specify Grid
            upnl.dgViolationInfo = (DataGrid)Page.FindControl(gridid); // dgViolationInfo;
            // Specify Grid Field
            // upnl.GridField = "hf_ticketviolationid";
            upnl.GridField = gridfield;
            upnl.clsdb = clsdb;
            // Set Case Number
            ClsCase.TicketID = Convert.ToInt32(hf_ticketid.Value);

            upnl.ClsCase = ClsCase;
            upnl.ClsCaseDetail = ClsCaseDetail;
            upnl.bugTracker = bugTracker;

            upnl.TicketViolationID = Convert.ToInt32(hf_ticketviolationid.Value);
            upnl.UpdateAll = cb_updateall.Checked;

            // if Arraingment Waiting  status updated
            if (ddl_pstatus.SelectedValue == "201")
                upnl.ArrignmentWaitingDate = true;
            else
                upnl.ArrignmentWaitingDate = false;

            // if Bond Waiting  status updated
            if (ddl_pstatus.SelectedValue == "202")
                upnl.BondWaitingDate = true;

            else
                upnl.BondWaitingDate = false;

            upnl.CDI = ddl_cdi.SelectedValue;

            //Zeeshan 4163 06/02/2008 Check Missed Court Type
            string CurrMissedCourtType = "-1";
            string PrevMissedCourtType = hf_missedcourttype.Value;

            CurrMissedCourtType = (rbtnNoAction.Checked ? "1" : CurrMissedCourtType);
            CurrMissedCourtType = (rbtnPledOut.Checked ? "2" : CurrMissedCourtType);

            upnl.MissedCourtType = Convert.ToInt32(CurrMissedCourtType);


            //Waqas 5864 06/23/2009 Updating court date to 15 days after arrest date.
            //if (Convert.ToInt32(hf_status.Value) == 237 && hf_casetype.Value == "2" && Convert.ToInt32(hf_violationid.Value) == 16159)
            //{
            //    DateTime dt = upnl.ArrestDate;
            //    if (dt.AddDays(15).DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        dt = dt.AddDays(14);
            //    }
            //    else
            //    {
            //        dt = dt.AddDays(15);
            //    }
            //    upnl.CourtDate = dt;

            //}

            upnl.UpdateViolationDetail();
            //Fahad 5753 04/10/2009 Delete NoSplit Flag when Violation is updated 
            ClsCase.DeleteNoSplitFlag(Convert.ToInt32(hf_ticketid.Value), EmpID);
            // Noufil 4747 09/09/2008 Update Coverage firm if court location is set to Harris Co Family Crt (AG)
            ClsCase.TicketID = Convert.ToInt32(hf_ticketid.Value);
            if (ClsCase.GetActiveFlag() == 1)
            {

                //Sabir Khan 6025 06/11/2009 No need to assign Jackie Miller as attorney to AG Cases by default ... 
                //-------------------------------------------------------------------------------------------------
                //Update Coverage Firm 
                //if ((Convert.ToInt32(hf_oldcourtid.Value) != Convert.ToInt32(ddl_pcrtloc.SelectedValue)) && Convert.ToInt32(ddl_pcrtloc.SelectedValue.ToString()) == 3077)
                //    ClsCaseDetail.UpdateCoverageFirm(Convert.ToInt32(clsCourts.CourtsName.HarrisCountyTitle4D), ClsCase.TicketID);
                //-------------------------------------------------------------------------------------------------
                //Send Civil Case Summary Document
                if (((Convert.ToDateTime(hf_oldccourtdate.Value).ToShortDateString() != Convert.ToDateTime(ViewState["crtdate"].ToString()).ToShortDateString()) || (Convert.ToInt32(hf_status.Value) != Convert.ToInt32(hf_oldstatus.Value))) && Convert.ToInt32(ddl_pcrtloc.SelectedValue) == 3077)
                    MailCivilClient(ClsCase.TicketID, 4);
            }

            error = upnl.Error;


            //added by Agha Usman for task 2630 on 01/22/2008

            if (ddl_pcrtloc.SelectedValue == "3061" || hf_courtloc.Value == "3061")// In case of Orcar client
            {
                if (hf_courtroomno.Value != tb_prm.Text || hv_viostatus.Value != ddl_pstatus.SelectedValue || hf_courttime.Value != ddl_pcrttime.SelectedValue || hf_day.Value != tb_pday.Text || hf_Month.Value != tb_pmonth.Text || hf_year.Value != tb_pyear.Text || hf_courtloc.Value != ddl_pcrtloc.SelectedValue)
                {
                    UpdateTicketMailStatus(int.Parse(hf_ticketid.Value), 1);
                }

                MailOscarClient(Convert.ToInt32(hf_ticketid.Value));
            }
            //end

            //Zeeshan Ahmed 3486 04/04/2008
            //Send Missed Court Letter to Batch If Selected
            //Zeeshan Ahmed 4163 06/03/2008 If Missed Court Type Changed
            if (PrevMissedCourtType != CurrMissedCourtType && hf_status.Value == "105")
            {
                if (rbtnNoAction.Checked || rbtnPledOut.Checked)
                {
                    if (ClsCase.GetActiveFlag() == 1)
                    {
                        //Sabir Khan 5442 02/10/2009 To exclude the records whose no letter flag is set...
                        ClsFlags flags = new ClsFlags();
                        flags.TicketID = Convert.ToInt32(hf_ticketid.Value);
                        if (!(flags.HasFlag(Convert.ToInt32(FlagType.NoLetter))))
                        {
                            Hashtable ticketTable = new Hashtable();
                            ticketTable.Add(ClsCase.TicketID, ClsCase.TicketID);
                            clsDocketCloseOut docketCloseOut = new clsDocketCloseOut();
                            docketCloseOut.EmpId = EmpID;
                            if (rbtnNoAction.Checked)
                                docketCloseOut.SendMissedCourtLetterToBatch(ticketTable, BatchLetterType.MissedCourtLetter);
                            else if (rbtnPledOut.Checked)
                                docketCloseOut.SendMissedCourtLetterToBatch(ticketTable, BatchLetterType.PledOutLetter);
                        }
                    }
                }
            }

            //Zeeshan Ahmed 3757 04/17/2008
            //Add NOS Flag If Cause Number Is Blank
            if (tb_causeno.Text == "" && hf_hasNOS.Value == "0" && hf_UpdateNOS.Value == "1")
            {
                ClsFlags flags = new ClsFlags();
                clsNOS nos = new clsNOS();

                //Check NOS Flag
                flags.FlagID = 15; flags.TicketID = ClsCase.TicketID; flags.EmpID = EmpID;
                //Update NOS Date
                nos.UpdateNOS(flags.TicketID, flags.EmpID);

                //Add If Flag Not Exist
                if (!flags.HasFlag())
                {
                    //Add Flag
                    if (flags.AddNewFlag())
                    {
                        hf_UpdateNOS.Value = "0";
                        hf_hasNOS.Value = "1";
                    }
                }
            }

            // Noufil 6126 07/23/2009 Update new hire status for client
            ClsCase.UpdateClientNewHireStatus(Convert.ToInt32(hf_ticketid.Value), EmpID, 1, true);

        }

        //Check Mail Oscar Client
        private void MailOscarClient(int ticketid)
        {
            string[] keys = { "@TicketID" };
            object[] values = { ticketid };
            DataTable dt = clsdb.Get_DT_BySPArr("USP_HTS_GET_OSCARMAILFLAG", keys, values);

            if (dt.Rows.Count > 0)
                if (Convert.ToInt32(dt.Rows[0]["OscarMailFlag"]) == 1)
                {
                    CaseSummary(ticketid);
                    SendMail();
                    UpdateTicketMailStatus(ticketid, 0);
                    //string[] keys1 = { "@TicketID", "@OscarMailFlag" };
                    //object[] values1 = { ticketid, "0" };
                    //clsdb.ExecuteSP("USP_HTS_SET_OSCARMAILFLAG", keys1, values1);

                }

        }

        private void MailCivilClient(int ticketid, int casetype)
        {
            CaseSummary(ticketid);
            SendCivilMail(casetype);
            UpdateTicketMailStatus(ticketid, 0);
        }

        private void SendCivilMail(int casetype)
        {
            try
            {
                string toUser = (string)ConfigurationManager.AppSettings["CivilUpdateEmailTo"];
                string ccUser = (string)ConfigurationManager.AppSettings["CivilCaseUpdateEmailCc"];
                // Noufil 4940 10/09/2008 Email send for family law
                if (casetype == 3)
                    lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.", "A civil client updated.", Convert.ToString(ViewState["vFullPath"]), toUser, ccUser);
                else if (casetype == 4)
                    lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.", "Family Law client updated.", Convert.ToString(ViewState["vFullPath"]), toUser, ccUser);
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Agha Usman 2630 01/22/2008
        protected void UpdateTicketMailStatus(int TicketId, int status)
        {
            string[] keys = { "@TicketID", "@OscarMailFlag" };
            //object[] values = { Convert.ToInt32(hf_ticketid.Value), "1" };
            object[] values = { Convert.ToInt32(TicketId), status };
            clsdb.ExecuteSP("USP_HTS_SET_OSCARMAILFLAG", keys, values);
        }

        private void CaseSummary(int ticketid)
        {
            try
            {

                DataSet ds;

                string RptSetting = "123456";

                String[] reportname = new String[] { "Notes.rpt", "Matters.rpt" };

                string filename = Server.MapPath("\\Reports") + "\\CaseSummary.rpt";


                string[] keyssub = { "@TicketID" };
                object[] valuesub = { ticketid };
                ds = clsdb.Get_DS_BySPArr("USP_HTS_GET_SubReports", keyssub, valuesub);

                lntechNew.Components.clsCrsytalComponent Cr = new lntechNew.Components.clsCrsytalComponent();
                string path = ConfigurationManager.AppSettings["NTPATHCaseSummary"].ToString();
                string[] key = { "@TicketId_pk", "@Sections" };
                object[] value1 = { ticketid, RptSetting };
                string name = ticketid.ToString() + "-CoverSheet-" + DateTime.Now.ToFileTime() + ".pdf";
                Cr.CreateSubReports(filename, "usp_hts_get_casesummary_report_data", key, value1, reportname, ds, 1, this.Session, this.Response, path, ticketid.ToString(), false, name);

                ViewState["vFullPath"] = path + name;
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Send Email
        private void SendMail()
        {
            try
            {

                //Kazim 2866 2/7/2008 replace the key "osemailto" with "ocupdateemailto" 
                string toUser = (string)ConfigurationManager.AppSettings["OcUpdateEmailTo"];
                string ccUser = (string)ConfigurationManager.AppSettings["OsEmailCC"];
                lntechNew.Components.MailClass.SendMailToAttorneys("This is a system generated email.", "An Oscar client updated.", Convert.ToString(ViewState["vFullPath"]), toUser, ccUser);
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "Email not sent.";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Get All Violations
        private void FillViolations(int courtid, int caseType)
        {
            try
            {
                //Zeeshan Ahmed 3979 5/23/2008 Refactor Function To Get Violations By Case Type
                //Get Violation List
                clsViolations ClsViolations = new clsViolations();
                DataSet ds_Violation = ClsViolations.GetAllViolations(caseType, courtid);


                //Zeeshan Ahmed 3815 06/19/2008 Fixed Update Violation Control Bug
                if (ddl_pvdesc.Items.FindByValue("0") != null)
                    ddl_pvdesc.SelectedValue = "0";

                // Display Violations 
                ddl_pvdesc.Items.Clear();
                ddl_pvdesc.DataSource = ds_Violation.Tables[0];
                ddl_pvdesc.DataBind();


                if (courtid == 0)
                    hf_lastcourtid.Value = "0";
                //ozair 4442 07/22/2008 ALR Hearing status appear for All ALR courts
                //If Not ALR Court (Houston, Fort Bend and Conroe)
                //Nasir 5310 12/23/2008 ALR Court by adding IsALRProcess
                else if (courtid != 0 && !upnl.IsALRProcess(courtid))//courtid != 3047 && courtid != 3048 && courtid != 3070 && courtid != 3079)
                {
                    ddl_pvdesc.SelectedIndex = 0;
                    hf_lastcourtid.Value = "1";
                    //ozair 4470 07/26/2008 checking by value instead of text
                    if (ddl_pvdesc.Items.FindByValue("16159") != null)
                        ddl_pvdesc.Items.Remove(ddl_pvdesc.Items.FindByValue("16159"));
                }

                //If ALR Court(Houston, Fort Bend and Conroe)
                //Nasir 5310 12/23/2008 ALR Court by adding IsALRProcess
                if (upnl.IsALRProcess(courtid))
                {
                    //Kazim 2754 4/3/2008 Check the existance of item before selected it. 
                    if (ddl_pvdesc.Items.FindByValue("16159") != null)
                    {
                        ddl_pvdesc.SelectedValue = "16159"; //Live ID 16159 //local 16159
                        //Waqas 5864 07/17/2009 Fixed Alert issue.
                        hf_violationid.Value = "16159";
                    }


                }
                //end ozair 4442

                if (ddl_pvdesc.Items.FindByValue(hf_violationid.Value) != null)
                    ddl_pvdesc.SelectedValue = hf_violationid.Value;
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Get Selected Price Plan
        private void FillPricePlan()
        {

            try
            {

                bool isNew = false;

                if (hf_newviolation.Value == "1")
                    isNew = true;

                clsPricingPlans ClsPricingPlans = new clsPricingPlans();
                DataSet ds_PricePlan;

                // else
                {
                    // Aziz 1517
                    int courtid;
                    if (("0" == hf_newviolation.Value) && ("0" == ddl_pcrtloc.SelectedValue))
                        courtid = Convert.ToInt32(hf_courtid.Value);
                    else
                        courtid = Convert.ToInt32(ddl_pcrtloc.SelectedValue);


                    ClsPricingPlans.CourtID = Convert.ToInt32(courtid);
                    //Aziz end
                    ddl_pcrtloc.Enabled = true;

                    if (ClsPricingPlans.CourtID == 3061)
                    {
                        tr_oscar.Style["display"] = "table-row";
                        tb_oscare_court.Focus();
                    }
                    else
                    {
                        tb_oscare_court.Text = "";
                        tr_oscar.Style["display"] = "none";
                    }
                }
                //if (IsNewViolation==true)
                if (isNew)
                {
                    ClsPricingPlans.IsNew = 1;
                    ds_PricePlan = ClsPricingPlans.GetAllPlan();
                }
                else
                {
                    ClsPricingPlans.IsNew = 0;
                    ds_PricePlan = ClsPricingPlans.GetAllPlan();
                }


                ddl_ppriceplan.Items.Clear();
                ddl_ppriceplan.Items.Add(new ListItem("--Choose--", "0"));
                for (int i = 0; i < ds_PricePlan.Tables[0].Rows.Count; i++)
                {
                    string description = ds_PricePlan.Tables[0].Rows[i]["PlanShortName"].ToString().Trim();
                    string ID = ds_PricePlan.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_ppriceplan.Items.Add(new ListItem(description, ID));
                }


                //if (IsNewViolation==true)
                if (isNew)
                {
                    try
                    {
                        string date = DateTime.Now.ToShortDateString();
                        DataSet ds_PlanID = ClsPricingPlans.GetPlan(date, ClsPricingPlans.CourtID, 0);
                        ddl_ppriceplan.SelectedValue = ds_PlanID.Tables[0].Rows[0]["planid"].ToString().Trim();
                    }
                    catch (Exception)
                    { }
                }
                else if (ClsCaseDetail.PlanID != 0)
                {
                    try
                    {
                        ddl_ppriceplan.SelectedValue = ClsCaseDetail.PlanID.ToString();
                    }
                    catch (Exception)
                    { }
                }
                else if (ClsCaseDetail.PlanID == 0)
                {
                    try
                    {
                        string date = string.Concat(tb_pmonth.Text, "/", tb_pday.Text, "/", tb_pyear.Text);
                        DataSet ds_PlanID = ClsPricingPlans.GetPlan(date, ClsPricingPlans.CourtID, 1);
                        ddl_ppriceplan.SelectedValue = ds_PlanID.Tables[0].Rows[0]["planid"].ToString().Trim();

                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Fills Court Location and All Price Plan
        private void FillCourtLocation(int caseType)
        {
            try
            {
                //Zeeshan Ahmed 3979 5/23/2008 Refactor Function To Get Courts By Case Type
                DataSet ds_ActiveCourts = ClsCourts.GetAllActiveCourtName(1, 1, caseType);
                //Khalid 2110 02/01/2008
                if (ds_ActiveCourts == null)
                {
                    this.error = "Active courts can't be found";
                    return;
                }

                // Filling Court Location
                ddl_pcrtloc.Items.Clear();
                ddl_pcrtloc.DataSource = ds_ActiveCourts.Tables[0];
                ddl_pcrtloc.DataBind();

                //Need To Be Refactor
                // Filling Price Plan List 
                ddl_ppriceplan.DataSource = clsdb.Get_DT_BySPArr("USP_HTS_getAllPricePlan");
                ddl_ppriceplan.DataBind();
                // noufil 4237 07/07/2008 choose added in dropdown
                ddl_pcrtloc.Items.Insert(0, new ListItem("---- Choose ----", "0"));

            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills Case status drop down list
        public void FillStatus(int courtid, int caseType)
        {
            try
            {
                //Zeeshan Ahmed 3979 5/23/2008 Refactor Function To Get Statuses By Case Type
                ddl_pstatus.Items.Clear();
                DataSet ds_Status = null;

                //ozair 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
                //Nasir 5310 12/24/2008 checking ALR Courts by adding IsALRProcess
                if (upnl.IsALRProcess(courtid))
                    ds_Status = ClsCaseStatus.GetALRCourtCaseStatus(courtid, iscasedisposition);
                else if (courtid == 0)
                    ds_Status = ClsCaseStatus.GetAllCourtCaseStatus();
                else
                    ds_Status = ClsCaseStatus.GetAllCourtCaseStatus(courtid, iscasedisposition);

                ddl_pstatus.DataSource = ds_Status.Tables[0];
                ddl_pstatus.DataBind();
                //ozair 5171 11/19/2008
                ddl_StatusCategory.DataSource = ds_Status.Tables[1];
                ddl_StatusCategory.DataBind();

                pctrstatus.Value = ddl_pstatus.SelectedValue.ToString();
                //ozair/ 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
                // Noufil 4948 10/13/2008 court id 3077 added
                //Nasir 5310 12/24/2008 checking ALR Courts by adding IsALRProcess
                if (!upnl.IsALRProcess(courtid))
                {
                    if (ddl_pstatus.Items.FindByText("ALR Hearing") != null)
                    {
                        ddl_pstatus.Items.Remove(ddl_pstatus.Items.FindByText("ALR Hearing"));
                    }
                    ddl_pstatus.SelectedValue = pctrstatus.Value;
                }
                else //Waqas 5864 07/17/2009 Fix Status Alert Issue
                {
                    hf_status.Value = pctrstatus.Value;
                }
                ddl_pstatus.SelectedValue = Convert.ToString(pctrstatus.Value);
                //ozair 5186 11/21/2008 setting category id acording to selection
                ddl_StatusCategory.Items.FindByText(ddl_pstatus.SelectedItem.Value).Selected = true;
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Set Javascript on the Control
        private void setControlScripts()
        {
            try
            {
                lbtn_close.Attributes.Add("onClick", "closepopup('" + Disable.ClientID + "','pnl_violation');return false;");
                imgbtn_violation.Attributes.Add("onClick", "return DisplayTogglePNew('" + ddl_pvdesc.ClientID + "','" + tb_pvdesc.ClientID + "','" + hf_vstyle.ClientID + "','" + ddl_pcrtloc.ClientID + "','" + hf_lastcourtid.ClientID + "');");
                ImgCrtLoc.Attributes.Add("onClick", "return DisplayToggleP2('" + ddl_pstatus.ClientID + "','" + tb_pstatus.ClientID + "','" + hf_sstyle.ClientID + "','" + ddl_pcrtloc.ClientID + "','" + hf_status.ClientID + "','" + rbtnNoAction.ClientID + "','" + rbtnPledOut.ClientID + "','" + trMissedCourt.ClientID + "');");
                btn_popup.Attributes.Add("onClick", "return submitPopup('" + this.ClientID + "','" + gridid + "','" + divid + "','" + hf_sstyle.ClientID + "');");
                ddl_pcrtloc.Attributes.Add("onChange", "chargelevel('" + this.ClientID + "','" + ddl_pvdesc.ClientID + "','" + tb_pvdesc.ClientID + "','" + hf_vstyle.ClientID + "');");
                ddl_pstatus.Attributes.Add("onchange", "checkMissedCourtStatus(this,'" + hf_status.ClientID + "','" + trMissedCourt.ClientID + "');");
                imgbtncourt.Attributes.Add("onClick", "return ToggleCourt('" + ddl_pcrtloc.ClientID + "','" + lbl_pcname.ClientID + "','" + hf_cstyle.ClientID + "','" + hf_courtid.ClientID + "');");
            }
            catch (Exception ex)
            {
                this.error = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Fill Charge Label 
        private void FillChargeLevel(int courtid)
        {
            clsViolations ClsViolations = new clsViolations();
            DataSet chargelevel = ClsViolations.GetAllChargeLevel(courtid);
            ddl_pvlevel.Items.Clear();
            ddl_pvlevel.DataSource = chargelevel;
            // Zeeshan Haider 10763  ALR question for DWI cases (M-4)
            //ddl_pvlevel.Items.Insert(0, new ListItem("--Choose--", "0"));
            ddl_pvlevel.DataBind();
        }


        //Get NOS Flag Info
        // Rab Nawaz Khan 01/15/2015 moved the code to the BindFlagsNew method to call the SP only once. . . 
        
        private void GetNOSFlagInfo()
        {
            try
            {
                //Zeeshan Ahmed 3757 04/17/2008
                if (Request.QueryString["caseNumber"] != null)
                {
                    int TicketId = 0;
                    Int32.TryParse(Request.QueryString["caseNumber"], out TicketId);

                    if (TicketId != 0)
                    {
                        ClsFlags flags = new ClsFlags();
                        //Check NOS Flag
                        flags.FlagID = 15;
                        flags.TicketID = Convert.ToInt32(Request.QueryString["caseNumber"]);
                        hf_hasNOS.Value = Convert.ToInt32(flags.HasFlag()).ToString();
                    }
                    else
                    {
                        hf_hasNOS.Value = "0";
                    }
                }
                else
                    hf_hasNOS.Value = "0";
            }
            catch (Exception ex)
            {
                hf_hasNOS.Value = "0";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        //Sabir Khan 8623 12/14/2010 Checking for Pre Trial Diversion and Motion Hiring flag info...
        private void GetPreTrialDiversionAndMotionHiringflag()
        {
            try
            {
                if (Request.QueryString["caseNumber"] != null)
                {
                    int TicketId = 0;
                    Int32.TryParse(Request.QueryString["caseNumber"], out TicketId);

                    if (TicketId != 0)
                    {
                        ClsFlags flags = new ClsFlags();                        
                        flags.FlagID = 42;
                        flags.TicketID = Convert.ToInt32(Request.QueryString["caseNumber"]);
                        hf_PreTrialDiversion.Value = Convert.ToInt32(flags.HasFlag()).ToString();
                        flags.FlagID = 43;
                        hf_MotionHiring.Value = Convert.ToInt32(flags.HasFlag()).ToString();
                    }
                    else
                    {
                        hf_PreTrialDiversion.Value = "0";
                        hf_MotionHiring.Value = "0";
                    }
                }
                else
                {
                    hf_PreTrialDiversion.Value = "0";
                    hf_MotionHiring.Value = "0";
                }                

            }
            catch (Exception ex)
            {
                hf_PreTrialDiversion.Value = "0";
                hf_MotionHiring.Value = "0";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        //Sabir Khan 8862 02/15/2011 Checking for JUVNILE Flag info...
        private void GetJUVNILEflag()
        {
            try
            {
                if (Request.QueryString["caseNumber"] != null)
                {
                    int TicketId = 0;
                    Int32.TryParse(Request.QueryString["caseNumber"], out TicketId);

                    if (TicketId != 0)
                    {
                        ClsFlags flags = new ClsFlags();
                        flags.FlagID = 44;
                        flags.TicketID = Convert.ToInt32(Request.QueryString["caseNumber"]);
                        hf_JUVNILE.Value = Convert.ToInt32(flags.HasFlag()).ToString();                       
                    }
                    else
                    {
                        hf_JUVNILE.Value = "0";                        
                    }
                }
                else
                {
                    hf_JUVNILE.Value = "0";                 
                }

            }
            catch (Exception ex)
            {
                hf_JUVNILE.Value = "0";                
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }






        //Set Control Layout
        private void SetContolLayOut(bool FillCourtList)
        {
            //ADDED BY FAHAD FOR DISPLAYING HARRIS COUNTY CRIMINAL COURT STATUS & VIOLATIONS
            //int CourtID = Convert.ToInt32(ddl_pcrtloc.SelectedValue);

            int CourtID = Convert.ToInt32(hf_courtid.Value);

            if (CourtID == 3037)
            {
                pctrstatus.Value = ddl_pstatus.SelectedValue.ToString();
                defviolation = ddl_pvdesc.SelectedValue.ToString();
                hcviolation = ddl_pvdesc.SelectedValue.ToString();

            }
            //ozair 4442 07/22/2008 ALR Court (Houston, Fort Bend and Conroe)
            //Nasir 5310 12/24/2008 checking ALR Courts by adding IsALRProcess
            else if (upnl.IsALRProcess(CourtID))
            {
                hf_status.Value = "0";
                hf_violationid.Value = "0";
                //waqas 5864 07/07/2009 fix ALR Hidden field issue
                hf_IsALRProcess.Value = "True";
            }


            if (("0" == hf_newviolation.Value) && ("0" == ddl_pcrtloc.SelectedValue))
                CourtID = Convert.ToInt32(hf_courtid.Value);
            else
                CourtID = Convert.ToInt32(ddl_pcrtloc.SelectedValue);

            int caseType = Convert.ToInt32(hf_casetype.Value);

            if (FillCourtList)
                FillCourtLocation(caseType);



            FillStatus(CourtID, caseType);
            FillPricePlan();
            FillViolations(CourtID, caseType);
            FillChargeLevel(CourtID);
            // Set Drop Down List Style
            ddl_pvdesc.Style["Display"] = hf_vstyle.Value;
            ddl_pstatus.Style["Display"] = hf_sstyle.Value;
            // Noufil 4369 07/17/2008 Hiding ticket number row for criminal cases
            // Waqas 5864 07/31/2009 Hiding ticket number row for family cases
            tr_ticketno.Style[HtmlTextWriterStyle.Display] = (caseType == 2 || caseType == 4) ? "none" : "block";
            //Yasir Kamal 6194 07/24/2009 hide bond Question row for criminal cases.
            // Noufil 6164 08/11/2009 Show Bond Question only for Traffic Case.
            tr_bond.Style["Display"] = (caseType == 1) ? "block" : "none";
          

            //Zeeshan Ahmed 3724 05/12/2008
            bool CriminalCourt = ClsCourts.CheckCriminalCourt(CourtID);
            //ozair 4442 07/22/2008 Not ALR Courts(houston, Forte Bend, Conroe)
            //Nasir 5310 12/24/2008 checking ALR Courts by adding IsALRProcess
            if (!upnl.IsALRProcess(CourtID))
            {
                if (CriminalCourt)
                {
                    // tahir 5117 11/11/2008 hide Level & CDI ....
                    if (hf_violationid.Value == "18534")
                    {
                        tr_chargelevel.Style["display"] = "none";
                        tr_cdi.Style["display"] = "none";
                    }
                    else
                    {
                        tr_chargelevel.Style["display"] = "table-row";
                        tr_arrestdate.Style["display"] = "table-row";
                    }
                    hf_iscriminalcourt.Value = "1";
                }
                else
                {
                    tr_chargelevel.Style["display"] = "none";
                    tr_arrestdate.Style["display"] = "none";
                    hf_iscriminalcourt.Value = "0";
                }
            }
            else
            {
                //Waqas 5864 07/17/2009 Setting IsALRProcess Hidden filed
                hf_IsALRProcess.Value = "True";
                tr_chargelevel.Style["display"] = "none";
                //tr_fineamount.Style["display"] = "table-row";
                //tr_arrestdate.Style["display"] = "table-row";
            }
            // Rab Nawaz Khan 8997 10/18/2011 Added the logic of allow or restrict the court room numbers for the outside courts
            bool isAllowCourtNumber = ClsCourts.GetAllowedCourtRoomNumbers(CourtID);
            if (isAllowCourtNumber)
                tr_CourtNo.Style["Display"] = "";
            else
                tr_CourtNo.Style["Display"] = "none";

            //Zeeshan Ahmed 3979 5/23/2008 Don't display Fine & Bond Amount For Criminal And Civil case
            // Noufil 6164 08/11/2009 Show Bond Question only for Traffic Case.
            if (hf_casetype.Value == "2" || hf_casetype.Value == "3" || hf_casetype.Value == "3")
            {
                tr_bondamount.Style["display"] = "none";
                //Waqas 5864 07/17/2009 Checking IsALRProcess Hidden filed
                if (hf_IsALRProcess.Value != "True")
                {
                    tr_fineamount.Style["display"] = "none";
                }
            }
            else
            {
                //tr_fineamount.Style["display"] = "table-row";
                //tr_bondamount.Style["display"] = "table-row";
            }

            if (CourtID == 3037)
            {
                // tahir 5117 11/11/2008 hide Level & CDI ....
                if (hf_violationid.Value != "18534")
                {
                    tr_cdi.Style["display"] = "table-row";
                }
            }
            else
                tr_cdi.Style["display"] = "none";

            //added by Ozair for task 2515 on 01/02/2008
            if (CourtID != 3001 && CourtID != 3002 && CourtID != 3003 && hf_casetype.Value == "1")
            {
                //tr_bondamount.Style["display"] = "table-row";
            }
            else
            {
                tr_bondamount.Style["display"] = "none";
            }

            //Added By Zeeshan Ahmed On 1/25/2008
            if (ddl_ppriceplan.Items.FindByValue(hf_planid.Value) != null)
                ddl_ppriceplan.SelectedValue = hf_planid.Value;

            if (ddl_pcrtloc.Items.FindByValue(hf_courtid.Value) != null || hf_cstyle.Value == "block")
            {
                lbl_pcname.Style["display"] = "none";
                ddl_pcrtloc.Style["display"] = "block";

                if (ddl_pcrtloc.Items.FindByValue(hf_courtid.Value) != null)
                {
                    imgbtncourt.Style["display"] = "none";

                    if (FillCourtList)
                        ddl_pcrtloc.SelectedValue = hf_courtid.Value;
                    else
                    {
                        if (ddl_pcrtloc.SelectedValue == "0")
                        {
                            ddl_pcrtloc.SelectedValue = hf_courtid.Value;
                        }
                    }
                }
                else
                {                   
                    imgbtncourt.Style["display"] = "block";
                }
                   
            }
            else
            {
                ddl_pcrtloc.Style["display"] = "none";
                lbl_pcname.Style["display"] = "block";
                lbl_pcname.Text = hf_pcname.Value;
                imgbtncourt.Style["display"] = "block";
            }

            //Temp Comment
            //if (ddl_pcrtloc.SelectedValue != hf_courtid.Value && ddl_pcrtloc.Items.FindByValue(hf_courtid.Value) != null)
            if (ddl_pcrtloc.Items.FindByValue(hf_courtid.Value) == null || ddl_pcrtloc.Items.FindByValue(hf_courtid.Value).Value == "")
                hf_courtid.Value = "0";

            //Fixed By Zeeshan Ahmed Bug ID : 2745
            //Set Charge Level 
            if (ddl_pvlevel.Items.FindByValue(hf_Level.Value) != null)
                ddl_pvlevel.SelectedValue = hf_Level.Value;

            // Set Drop Down List Style
            if (hf_vstyle.Value == "block")
            {
                ddl_pvdesc.Style["Display"] = "block";
                tb_pvdesc.Style["Display"] = "none";
            }
            else
            {
                ddl_pvdesc.Style["Display"] = "none";
                tb_pvdesc.Style["Display"] = "block";
            }


            if (hf_sstyle.Value == "block")
            {
                ddl_pstatus.Style["Display"] = "block";
                tb_pstatus.Style["Display"] = "none";
            }
            else
            {
                ddl_pstatus.Style["Display"] = "none";
                tb_pstatus.Style["Display"] = "block";
            }

            //Zeeshan Ahmed 4163 06/10/2008 Set Status In The List
            if (ddl_pstatus.Items.FindByValue(hf_status.Value) != null && hf_sstyle.Value == "none")
                ddl_pstatus.SelectedValue = hf_status.Value;

            string MissedCourtStatusID = (hf_sstyle.Value == "none") ? hf_status.Value : ddl_pstatus.SelectedValue;
            trMissedCourt.Style["Display"] = (MissedCourtStatusID == "105") ? "block" : "none";
        }

        #endregion

    }

}
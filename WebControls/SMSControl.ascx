﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMSControl.ascx.cs"
    Inherits="DTP.WebControls.SMSControl" %>
<link href="../Styles.css" rel="stylesheet" type="text/css" />

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    
    function ValidateControl(popupid)
    {       
        
        if (document.getElementById(popupid + "_rb_existingContacts").checked)
        {
            if ((!document.getElementById(popupid + "_chk_SendSMS1").checked) && (!document.getElementById(popupid + "_chk_SendSMS2").checked) && (!document.getElementById(popupid + "_chk_SendSMS3").checked))
            {   
                alert("Please select any number to send sms.");
                return false;
            }
            
            if (document.getElementById(popupid + "_chk_SendSMS1").checked)
            {
                if ((document.getElementById(popupid + "_TxtContact1_1").value.length != 3) || (document.getElementById(popupid + "_TxtContact1_2").value.length != 3) || (document.getElementById(popupid + "_TxtContact1_3").value.length != 4))
                {
                    alert("Please enter valid mobile number.");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact1_1").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact1_2").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact1_3").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
            }
            
            if (document.getElementById(popupid + "_chk_SendSMS2").checked)
            {
                if ((document.getElementById(popupid + "_TxtContact2_1").value.length != 3) || (document.getElementById(popupid + "_TxtContact2_2").value.length != 3) || (document.getElementById(popupid + "_TxtContact2_3").value.length != 4))
                {
                    alert("Please enter valid mobile number.");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact2_1").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact2_2").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact2_3").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
            }
            
            if (document.getElementById(popupid + "_chk_SendSMS3").checked)
            {
                if ((document.getElementById(popupid + "_TxtContact3_1").value.length != 3) || (document.getElementById(popupid + "_TxtContact3_2").value.length != 3) || (document.getElementById(popupid + "_TxtContact3_3").value.length != 4))
                {
                    alert("Please enter valid mobile number.");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact3_1").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact3_2").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_TxtContact3_3").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
            }
        }
        
        else if (document.getElementById(popupid + "_rb_other").checked)
        {
            if (document.getElementById(popupid + "_chk_Other").checked)
            {
                if ((document.getElementById(popupid + "_txtOther1").value.length != 3) || (document.getElementById(popupid + "_txtOther2").value.length != 3) || (document.getElementById(popupid + "_txtOther3").value.length != 4))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }  
                              
                if (isNaN(document.getElementById(popupid + "_txtOther1").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_txtOther2").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
                if (isNaN(document.getElementById(popupid + "_txtOther3").value))
                {
                    alert("Please enter valid mobile number format");
                    return false;
                }
            }
            else
            {
                alert("Please select any number to send sms");
                return false;
            }
        }
        
        if (document.getElementById(popupid + "_TxtMessage").value.length ==0)
        {
            alert("Please enter valid sms message to send sms");
            return false;
        }
        return true;
    }
    
    function Testing()
    {
        debugger;
        var modalPopupBehavior = $find('beh_id');
        modalPopupBehavior.show();
        return false;
    }
    
    function RefreshControl(controlid)
    {   
        if (controlid == "rb_existingContacts")
        {
            document.getElementById("smscontrl_chk_Other").checked = false;
            document.getElementById("smscontrl_rb_existingContacts").checked = true;
            document.getElementById("smscontrl_rb_other").checked = false;
        }
        else if (controlid == "rb_other")
        {
            document.getElementById("smscontrl_chk_SendSMS1").checked = false;
            document.getElementById("smscontrl_chk_SendSMS2").checked = false;
            document.getElementById("smscontrl_chk_SendSMS3").checked = false;
            document.getElementById("smscontrl_rb_other").checked = true;
            document.getElementById("smscontrl_rb_existingContacts").checked = false;
        }
        //return false;
    }
</script>

<aspnew:UpdatePanel ID="upnl_SMSCntrl" runat="server">
    <ContentTemplate>
        <table width="100%" class="clsLeftPaddingTable">
            <tr>
                <td valign="bottom" background="../Images/subhead_bg.gif">
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 26px" class="clssubhead">
                                    Send SMS Alert
                                </td>
                                <td align="right">
                                    <asp:LinkButton ID="lbtn_close2" runat="server" OnClientClick="return HidePopup();">X</asp:LinkButton>
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_SmsError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="rb_existingContacts" runat="server" Text="Existing Contact Numbers"
                        CssClass="clssubhead" GroupName="SMSGROUP" Checked="true" onclick = "RefreshControl('rb_existingContacts');" />
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                    <tr id="lblMsg" runat="server">
                        <td>
                            <asp:Label ID="lblexisting" runat="server" CssClass="Label" ForeColor="Red" Visible= "false"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trContact1" runat="server">
                        <td>
                        <table>
                        <tr>
                            <td style="width: 10px">
                            
                            </td>
                            
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact1_1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>                                    
                                </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact1_2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="TxtContact1_3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_SendSMS1" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_SendSMS1" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_SendSMS1" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                        </td>
                    </tr>
                    </table>
                    
                </td>
            </tr>
            <tr id="trContact2" runat="server">
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact2_1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact2_2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="TxtContact2_3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_SendSMS2" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_SendSMS2" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_SendSMS2" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trContact3" runat="server">
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact3_1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="TxtContact3_2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="TxtContact3_3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_SendSMS3" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_SendSMS3" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_SendSMS3" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="rb_other" runat="server" Text="Other Contact Numbers" CssClass="clssubhead"
                        GroupName="SMSGROUP" onclick = "return RefreshControl('rb_other');" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="txtOther1" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 35px">
                                <asp:TextBox ID="txtOther2" runat="server" Width="32px" CssClass="clsInputadministration"
                                    MaxLength="3" onkeyup="return autoTab(this, 3, event)"></asp:TextBox>
                            </td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtOther3" runat="server" Width="48px" CssClass="clsInputadministration"
                                    MaxLength="4" onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                            </td>
                            <td style="width: 20px">
                                <asp:CheckBox ID="chk_Other" runat="server" />
                            </td>
                            <td>
                                <asp:Image ID="img_chk_Other" runat="server" Style="display: none;" />
                            </td>
                            <td>
                                <asp:Label ID="lbl_chk_Other" runat="server" CssClass="clssubhead"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td>
                                <asp:TextBox ID="TxtMessage" runat="server" MaxLength="160" TextMode="MultiLine"
                                    Width="330px" Height="150px" CssClass="clsInputadministration"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 13px">
                            </td>
                            <td>
                                <asp:Button ID="btn_sendsms" runat="server" Text="Send SMS" CssClass="clsbutton"
                                    OnClick="btn_sendsms_Click" OnClientClick="return ValidateControl('smscontrl')" />
                                <asp:Button ID="btn_close" runat="server" Text="Close" CssClass="clsbutton" OnClientClick="return HidePopup();" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <aspnew:AsyncPostBackTrigger ControlID="btn_sendsms" EventName="Click" />
    </Triggers>
</aspnew:UpdatePanel>

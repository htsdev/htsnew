﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using WCtl_Comments.Components;

namespace HTP.WebControls
{

    public partial class Navigation : System.Web.UI.UserControl
    {
        #region Variables

        clsQuoteCallBack objQuoteCallback = new clsQuoteCallBack();
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        int ticketId = 0;

        #endregion

        #region Properties

        //For get and set DataTable containing TicketId's.
        public DataTable DT_TicketIDs
        {
            get { return (DataTable)Session["dt_ticketID"]; }
            set { Session["dt_ticketID"] = value; }
        }

        //For get and set TicketId and used by the Navigation Control.
        public int StartinTicketID
        {
            get { return Convert.ToInt32(ViewState["ticketid"]); }
            set { ViewState["ticketid"] = value; }
        }

        //For get and set Starting Serial Number and used for Navigation. 
        public int StartingSerialNumber
        {
            get { return Convert.ToInt32(ViewState["serialnumber"]); }
            set { ViewState["serialnumber"] = value; }
        }

        //For get and set Modal PopUp Id 
        public string ModalPopUpID
        {
            get { return Convert.ToString(ViewState["ModalPopUpID"]); }
            set { ViewState["ModalPopUpID"] = value; }
        }

        //For get and set Max Serail number of record Set
        private int MaxSerailNo
        {
            get { return Convert.ToInt32(ViewState["MaxSerialnumber"]); }
            set { ViewState["MaxSerialnumber"] = value; }
        }

        //For get and set Title of the Navigation Control.
        public string ControlTitle
        {
            get { return Convert.ToString(ViewState["ControlTitle"]); }
            set { ViewState["ControlTitle"] = value; }
        }

        //For get and set the Title.
        public string Title
        {
            set
            {
                lbl_head.Text = value;
            }
        }

        #endregion

        #region Events

        // Page Event Handler    
        public event PageMethodHandler PageMethod;

        //Event Fired when page uploaded
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lbl_head.Text = ControlTitle;
                    //Filling Quote call Back status
                    FillDropDwonList();
                    //Setting Current Date For Validation Purpose
                    hf_CurrentDate.Value = DateTime.Now.ToString("MM/dd/yyyy");
                }
                lblMessage.Visible = false;
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }
        //Event Fired when User clicked on Next button
        protected void imgNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataView dv = DT_TicketIDs.DefaultView;
                StartingSerialNumber = (StartingSerialNumber == Convert.ToInt32(dv.Table.Rows.Count) ? StartingSerialNumber : StartingSerialNumber + 1); //Afaq 7496 03/22/2010 Remove error "Index 0 is either negative or above rows count"
                dv.RowFilter = ("sno = " + Convert.ToString(StartingSerialNumber));
                //StartingSerialNumber = StartingSerialNumber + 1;
                ticketId = Convert.ToInt32(dv[0]["TicketID_PK"].ToString());
                StartinTicketID = ticketId;
                //Ozair 7496 03/08/2010 
                SetRecordNavigation();
                FillGridControl(ticketId, StartingSerialNumber);//Filling the all information in Grid View.
                UpdateQuoteCallTrackingInfo(ticketId, "Clicked");

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ModalPopUpID"])))//Place this in finally 
                {
                    ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(Convert.ToString(ViewState["ModalPopUpID"]))).Show();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        //Event Fired when User clicked on Previous button
        protected void imgPrev_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataView dv = ((DataTable)Session["dt_ticketID"]).DefaultView;

                StartingSerialNumber = (StartingSerialNumber <= 1) ? 1 : StartingSerialNumber - 1; //Afaq 7496 03/22/2010 Remove error "Index 0 is either negative or above rows count"
                dv.RowFilter = "Sno = " + Convert.ToString(StartingSerialNumber);
                ticketId = Convert.ToInt32(dv[0]["TicketID_PK"].ToString());
                StartinTicketID = ticketId;
                //Ozair 7496 03/08/2010 
                SetRecordNavigation();
                FillGridControl(ticketId, StartingSerialNumber);
                UpdateQuoteCallTrackingInfo(ticketId, "Clicked");
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ModalPopUpID"])))
                {
                    ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(Convert.ToString(ViewState["ModalPopUpID"]))).Show();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

        }

        //Event Fired when User clicked on Last button
        protected void imgLast_Click(object sender, ImageClickEventArgs e)
        {
            try
            {                
                DataView dv = ((DataTable)Session["dt_ticketID"]).DefaultView;
                StartinTicketID = Convert.ToInt32(dv.Table.Rows[dv.Table.Rows.Count - 1]["TicketID_PK"].ToString());
                dv.RowFilter = "TicketID_PK =" + StartinTicketID;
                ticketId = Convert.ToInt32(dv[0]["TicketID_PK"].ToString());
                StartinTicketID = ticketId;
                int sno = Convert.ToInt32(dv[0]["sno"].ToString());
                StartingSerialNumber = Convert.ToInt32(dv[0]["sno"].ToString());
                //Ozair 7496 03/08/2010 
                SetRecordNavigation();
                FillGridControl(ticketId, sno);
                UpdateQuoteCallTrackingInfo(ticketId, "Clicked");
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ModalPopUpID"])))
                {
                    ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(Convert.ToString(ViewState["ModalPopUpID"]))).Show();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }


        }

        //Event Fired when User clicked on First button
        protected void imgFirst_Click(object sender, ImageClickEventArgs e)
        {
            try
            {                
                DataView dv = ((DataTable)Session["dt_ticketID"]).DefaultView;
                dv.RowFilter = "Sno = 1";
                StartingSerialNumber = 1;
                ticketId = Convert.ToInt32(dv[0]["TicketID_PK"].ToString());
                StartinTicketID = ticketId;
                //Ozair 7496 03/08/2010 
                SetRecordNavigation();
                int sno = Convert.ToInt32(dv[0]["sno"].ToString());
                FillGridControl(ticketId, sno);
                UpdateQuoteCallTrackingInfo(ticketId, "Clicked");
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ModalPopUpID"])))
                {
                    ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(Convert.ToString(ViewState["ModalPopUpID"]))).Show();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

        }

        //Event Fired when User Close on Next button
        protected void lbtn_close_Click(object sender, EventArgs e)
        {
            try
            {
                if (PageMethod != null)
                {
                    PageMethod();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        //Event Fired when User clicked on Update button
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateQuoteCallInfo(StartinTicketID);
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ModalPopUpID"])))
                {
                    try
                    {
                        ((AjaxControlToolkit.ModalPopupExtender)Page.FindControl(Convert.ToString(ViewState["ModalPopUpID"]))).Show();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// This method sets the view of the Navigation Control on very first load.
        /// </summary>
        public void ShowDataWithStartinTicketID()
        {
            if (StartinTicketID != 0)
            {
                lbl_head.Text = ControlTitle;
                DataRow[] dr = DT_TicketIDs.Select("TicketID_PK = " + StartinTicketID);
                StartingSerialNumber = Convert.ToInt32(dr[0]["sno"].ToString());
                ticketId = Convert.ToInt32(dr[0]["TicketID_PK"].ToString());
                //
                FillGridControl(ticketId, StartingSerialNumber);
                //
                UpdateQuoteCallTrackingInfo(ticketId, "Clicked");
            }
            else
            {
                throw new ArgumentNullException("Argument StartinTicketID required");
            }
        }

        /// <summary>
        /// Setting Navigation Control Enable Disable
        /// </summary>
        private void SetRecordNavigation()
        {
            if (StartingSerialNumber == 1 && MaxSerailNo == 1)
            {
                imgFirst.Enabled = false;
                imgPrev.Enabled = false;
                imgNext.Enabled = false;
                imgLast.Enabled = false;
            }
            else if (StartingSerialNumber == 1)
            {
                imgFirst.Enabled = false;
                imgPrev.Enabled = false;
                imgNext.Enabled = true;
                imgLast.Enabled = true;
            }
            else if (StartingSerialNumber == MaxSerailNo)
            {
                imgFirst.Enabled = true;
                imgPrev.Enabled = true;
                imgNext.Enabled = false;
                imgLast.Enabled = false;
            }
            else
            {
                imgFirst.Enabled = true;
                imgPrev.Enabled = true;
                imgNext.Enabled = true;
                imgLast.Enabled = true;
            }
        }

        /// <summary>
        /// This method used to fill Grid View Columns on the basis of ticket id.
        /// </summary>
        /// <param name="TicketID">Id through which identify the Client.</param>
        /// <param name="sno">Serial Number of the records.</param>
        private void FillGridControl(int TicketID, int sno)
        {
            DataTable Mydt = objQuoteCallback.GetCaseInfoDetailByTicketId(TicketID);
            if (Mydt.Rows.Count > 0)
            {
                gvClientInfo.Visible = true;
                gvClientInfo.DataSource = Mydt;
                gvClientInfo.DataBind();
            }
            else
            {
                gvClientInfo.Visible = false;
            }
            //For Setting Control on the basis of Ticket Id.
            SetControl(ticketId);
        }

        /// <summary>
        /// This method is used for Setting Control on the basis of Ticket Id.
        /// </summary>
        /// <param name="ticketID">Id through which identify the Client.</param>
        private void SetControl(int ticketID)
        {
            DataTable dt_QuoteInfo = objQuoteCallback.GetQuoteCallBackDataByTicketId(ticketID);

            if (dt_QuoteInfo.Rows.Count > 0)
            {
                lblClientName.Text = dt_QuoteInfo.Rows[0]["Customer"].ToString();
                clsCase objCase = new clsCase();
                lblClientContact.Text = objCase.GetContactNo(ticketID);
                //Yasir Kamal 7355 01/29/2010 display client language.
                lblClientLanguage.Text = dt_QuoteInfo.Rows[0]["LanguageSpeak"].ToString();
                lblQuoteAmount.Text = String.Format("{0:C2}", Convert.ToDouble(dt_QuoteInfo.Rows[0]["calculatedtotalfee"]));
                cmbFollowUp.SelectedValue = dt_QuoteInfo.Rows[0]["Followupid"].ToString();
                WCC_GeneralComments.Initialize(ticketID, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), 1, "Label", "clsinputadministration", "clsbutton", "Label", true);
                WCC_GeneralComments.Button_Visible = false;
                if (Convert.ToInt32(dt_QuoteInfo.Rows[0]["FollowUpYN"]) == 1)
                    optFollowUp.SelectedValue = "1";
                else
                    optFollowUp.SelectedValue = "0";
                if (Convert.ToDateTime(dt_QuoteInfo.Rows[0]["CallBackDate"]).ToShortDateString() == "1/1/1900")
                {
                    CallBackDate.SelectedDate = DateTime.Now;
                }
                else
                {
                    DateTime callback = Convert.ToDateTime(dt_QuoteInfo.Rows[0]["CallBackDate"].ToString().Trim());
                    CallBackDate.SelectedDate = callback;
                }
                ViewState["cmbFollowUpSelected"] = cmbFollowUp.SelectedValue;
                ViewState["cmbFollowUpSelectedText"] = cmbFollowUp.SelectedItem.Text;
                ViewState["optionSelected"] = optFollowUp.SelectedItem.Text;
                ViewState["CallBackDateSelected"] = CallBackDate.SelectedDate.ToShortDateString();
            }
            else
            {
                CallBackDate.SelectedDate = Convert.ToDateTime("01/01/1900");
                lblClientName.Text = string.Empty;
                lblClientContact.Text = string.Empty;
                lblQuoteAmount.Text = string.Empty;
            }

        }

        /// <summary>
        /// This method is used for filling Quote call back status in dropdown List.
        /// </summary>
        private void FillDropDwonList()
        {
            DataSet ds_cmbFollowUp = ClsDb.Get_DS_BySP("usp_HTS_Get_AllQuoteResult");
            cmbFollowUp.DataSource = ds_cmbFollowUp;
            cmbFollowUp.DataTextField = ds_cmbFollowUp.Tables[0].Columns[1].ColumnName;
            cmbFollowUp.DataValueField = ds_cmbFollowUp.Tables[0].Columns[0].ColumnName;
            cmbFollowUp.DataBind();
        }

        /// <summary>
        /// This method is used for Update the all information related to our Client on the basis of TicketId.
        /// </summary>
        /// <param name="TicketID">Id through which identify the Client.</param>
        private void UpdateQuoteCallInfo(int TicketID)
        {
            try
            {
                int followup = 0;
                WCC_GeneralComments.AddComments();
                DateTime callback = CallBackDate.SelectedDate;
                DateTime appDate = Convert.ToDateTime("1/1/1900");
                if (optFollowUp.SelectedValue == "1")
                    followup = 1;
                else
                    followup = 0;
                objQuoteCallback.UpdateQuoteCallBackInfoByTicketId(TicketID, Convert.ToInt32(cmbFollowUp.SelectedValue), followup, Convert.ToDateTime(CallBackDate.SelectedDate), Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)));
                string selectedDate = CallBackDate.SelectedDate.ToShortDateString();

                if (ViewState["CallBackDateSelected"].ToString() != selectedDate)
                {
                    clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: call back date changed from [" + ViewState["CallBackDateSelected"].ToString() + "] to [" + selectedDate + "]", "", TicketID);
                }
                if (cmbFollowUp.SelectedItem.Text != ViewState["cmbFollowUpSelectedText"].ToString())
                {
                    if (cmbFollowUp.SelectedValue == "1" || cmbFollowUp.SelectedValue == "5" || cmbFollowUp.SelectedValue == "14")
                    {
                        clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up call status changed from [" + ViewState["cmbFollowUpSelectedText"].ToString() + "] to [----------]", "", TicketID);
                    }
                    else if (ViewState["cmbFollowUpSelected"].ToString() == "1" || ViewState["cmbFollowUpSelected"].ToString() == "5" || ViewState["cmbFollowUpSelected"].ToString() == "14")
                    {
                        clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up call status changed from [----------] to [" + cmbFollowUp.SelectedItem.Text + "]", "", TicketID);
                    }
                    else
                    {
                        clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up call status changed from [" + ViewState["cmbFollowUpSelectedText"].ToString() + "] to [" + cmbFollowUp.SelectedItem.Text + "]", "", TicketID);
                    }
                }
                if (ViewState["optionSelected"].ToString() != optFollowUp.SelectedItem.Text)
                {
                    clog.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), "Quote Call Back: Follow up changed from [" + ViewState["optionSelected"].ToString() + "] to [" + optFollowUp.SelectedItem.Text + "]", "", TicketID);
                }
                UpdateQuoteCallTrackingInfo(TicketID, "Updated : [ " + cmbFollowUp.SelectedItem.Text + " ]");
                lblMessage.Text = "Record Updated Successfully";
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Record Not Updated Successfully";
                clsLogger.ErrorLog(ex);
            }
            finally
            {
                lblMessage.Visible = true;
            }
        }

        /// <summary>
        /// This method used to maintain the Tracking of the records and for updating the event.
        /// </summary>
        /// <param name="TicketID">Id through which identify the Client.</param>
        /// <param name="EventDesc">This field used to identify the event "Clicked" OR "Update".</param>
        private void UpdateQuoteCallTrackingInfo(int TicketID, string EventDesc)
        {
            objQuoteCallback.UpdateQuoteCallTrackingInfoByTicketId(TicketID, EventDesc, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)));
        }

        #endregion

    }
}
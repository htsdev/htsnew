﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using lntechNew.Components.ClientInfo;

namespace HTP.WebControls
{
    public partial class NisiLetter : System.Web.UI.UserControl
    {
        public event PageMethodHandler PageMethod = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lntechNew.Components.clsCrsytalComponent cr = new lntechNew.Components.clsCrsytalComponent();
                    string fileSaveDirectory = System.Configuration.ConfigurationManager.AppSettings["NisiDocsPath"];
                    ClsNisiCaseHandler nisi = new ClsNisiCaseHandler();

                    if (!System.IO.Directory.Exists(fileSaveDirectory))
                    {
                        System.IO.Directory.CreateDirectory(fileSaveDirectory);
                    }

                    if (Convert.ToInt32(Session["ContactType"]) == 7)
                    {
                        string reportFileName = Server.MapPath(".") + "\\NisiPrintAnswerClientCopy.rpt";
                        DataSet dsClietns = nisi.GetClientLettersToPrint(Convert.ToInt32(Session["TicketId"]));
                        if (dsClietns.Tables[0].Rows.Count > 0)
                        {
                            cr.CreateReportWord(dsClietns, reportFileName, fileSaveDirectory, this.Session, this.Response);
                        }
                        Session["ContactType"] = null;
                    }
                    else
                    {
                        string clientrowId = Session["printAnswers"].ToString();
                        bool newClients = Convert.ToBoolean(Session["newClients"]);
                        string reportFileName = Server.MapPath(".") + "\\ScireFaciasAnswer.rpt";

                        DataSet ds = nisi.GetSelectClientsToPrintLetters(clientrowId, Convert.ToInt32(Session["EmployeeId"]), newClients, Session.SessionID);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            cr.CreateReportWord(ds, reportFileName, fileSaveDirectory, this.Session, this.Response);
                        }
                        Session["printAnswers"] = null;
                        Session["newClients"] = null;
                        Session["ContactType"] = null;
                    }
                }

                if (PageMethod != null)
                    PageMethod();
            }
            catch (Exception ex)
            {
                //lbl_message.Text = ex.Message;
            }

        }
    }
}
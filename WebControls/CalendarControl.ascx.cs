﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;

using System.Threading;
using HTP.WebControls;

namespace DTP.WebControls
{
    public partial class CalendarControl : System.Web.UI.UserControl
    {
        ClsSMS Clssms = new ClsSMS();
        clsCase ClsCase = new clsCase();
        public event PageMethodHandler PageIndexChanged;

        #region Declaration

        string dateformat = "";
        bool dateEnable ;
        DateTime selectedDate;
        string clientID;
        string visibleDate = "";

        #endregion

        #region Properties

        public string Dateformat
        {
            get { return dateformat; }
            set { dateformat = value; }
        }

        public string ClientID
        {
            get { return txtDate.ClientID; }
            set { clientID = txtDate.ClientID; }
        }

        public bool Enabled
        {
            get { return dateEnable; }
            set {
                dateEnable = value;
                if (!value)
                {
                    this.txtDate.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    this.txtDate.Attributes.Remove("disabled");
                }
            }
        }

        public DateTime SelectedDate
        {
            get { return Convert.ToDateTime(txtDate.Attributes["value"]); }
            set { txtDate.Attributes.Add("value", Convert.ToDateTime(value).ToShortDateString()); }
        }


        public DateTime VisibleDate
        {
            get { return Convert.ToDateTime(txtDate.Attributes["value"]); }
            set { txtDate.Attributes.Add("value", Convert.ToDateTime(value).ToShortDateString()); }
        }

        #endregion

        #region Events

        public CalendarControl()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindData();
        }

        protected void date_changed(object sender, EventArgs e)
        {
            if (PageIndexChanged != null)
                PageIndexChanged();
        }

        #endregion

        #region Methods

        public void Reset()
        {
            this.txtDate.Value = "";
        }

        public void BindData()
        {
            try
            {
                //if (!dateEnable)
                //{
                //    this.txtDate.Attributes.Add("disabled", "disabled");
                //}
                //else
                //{
                //    this.txtDate.Attributes.Remove("disabled");
                //}
                //if (dateformat != "")
                //{
                //    txtDate.Attributes.Add("data-format", this.dateformat);
                //}
                //else
                //{
                //    txtDate.Attributes.Add("data-format", "mm/dd/yyyy");
                //}
                //selectedDate = Convert.ToDateTime(SelectedDate.ToShortDateString()).Date;
                //txtDate.Attributes.Add("value", SelectedDate.ToShortDateString());

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }

        }


        #endregion






    }
}
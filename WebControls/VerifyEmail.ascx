﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VerifyEmail.ascx.cs"
    Inherits="HTP.WebControls.VerifyEmail" %>
<aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <asp:Image ID="imageFalse" Visible="false" runat="server" Style="vertical-align:top" />
    <asp:Image ID="imageTrue" Visible="false" runat="server" />
     <asp:LinkButton ID="lbtnVerify" runat="server"  Text="Verify" OnClientClick="return disableTxtBox();"
                        OnClick="lbtnVerify_Click" Style="vertical-align:middle" />   
     <asp:Image ID="imagePleaseWait" runat="server" Style="display: none" />
     <asp:Label ID="lblWaitText" runat="server" Text="Please Wait..." Style="display: none"></asp:Label>
     <asp:HiddenField ID="hf_emailAddress" runat="server" />
     
        
    </ContentTemplate>
    <Triggers>
        <aspnew:AsyncPostBackTrigger ControlID="lbtnVerify" EventName="Click" />
    </Triggers>
</aspnew:UpdatePanel>


<script type="text/javascript">
function disableTxtBox()
{
    var txtBox = document.getElementById('<%=EmailAddressControl%>');
    if (txtBox !=null && txtBox.value != "")
    {
        document.getElementById('<%=imagePleaseWait.ClientID %>').style.display = "";
        document.getElementById('<%=lbtnVerify.ClientID %>').style.display = "none";
        document.getElementById('<%=lblWaitText.ClientID %>').style.display = "";
        var sd =document.getElementById('<%=imageFalse.ClientID %>');
        if (sd!=null)
            document.getElementById('<%=imageFalse.ClientID %>').style.display = "none";
            
        var sdd =document.getElementById('<%=imageTrue.ClientID %>');
        if (sdd !=null)
            document.getElementById('<%=imageTrue.ClientID %>').style.display = "none";
        
    }
    else 
    {
    alert("No email address found to validate.");
    return false;
    }
}  
</script>


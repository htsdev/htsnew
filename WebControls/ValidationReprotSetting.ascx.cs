﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using HTP.Components.ValidationReportConfig;
using lntechNew.Components.ClientInfo;

namespace HTP.WebControls
{
    /// <summary>
    /// Validation Report Setting Class.
    /// </summary>
    public partial class ValidationReprotSetting : System.Web.UI.UserControl
    {
        #region Variables

        readonly ValidationReportSettings _oValidationReportMapper = new ValidationReportSettings();
        DataTable _dtUser;
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
            FillDropDown(ddlUsers);
        }
        /// <summary>
        /// On Init Event.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            Pagingctrl.PageIndexChanged += Pagingctrl_PageIndexChanged;
            Pagingctrl.PageSizeChanged += Pagingctrl_PageSizeChanged;
            Pagingctrl.GridView = gv_records;
        }
        /// <summary>
        /// Paging control page index change event.
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            {
                gv_records.PageIndex = Pagingctrl.PageIndex - 1;
                BindGrid();
            }
        }
        /// <summary>
        /// Paging control page size change event
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            {
                if (pageSize > 0)
                {
                    gv_records.PageIndex = 0;
                    gv_records.PageSize = pageSize;
                    gv_records.AllowPaging = true;
                }
                else
                {
                    gv_records.AllowPaging = false;
                }
                BindGrid();
            }
        }
        //Dont remove these lines of code, as they are needed for  RowEditing event.
        /// <summary>
        /// Gridview RowEditing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        //Dont remove these lines of code, as they are needed for  RowUpdating event.
        /// <summary>
        /// Gridview RowUpdating Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        /// <summary>
        /// Gridview RowDataBound Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbEdit = ((LinkButton)(e.Row.FindControl("lbEdit")));
                    lbEdit.CommandArgument = e.Row.RowIndex.ToString();
                    LinkButton lbSave = ((LinkButton)(e.Row.FindControl("lbSave")));
                    lbSave.CommandArgument = e.Row.RowIndex.ToString();
                    LinkButton lbCancel = ((LinkButton)(e.Row.FindControl("lbCancel")));
                    lbCancel.CommandArgument = e.Row.RowIndex.ToString();
                    LinkButton lbReportTitle = e.Row.FindControl("lbReportTitle") as LinkButton;
                    lbReportTitle.CommandArgument = e.Row.RowIndex.ToString();
                    lbSave.Attributes.Add("onclick", "javascript: return SetSelectedValues(" + e.Row.RowIndex + ");");
                    lbEdit.Attributes.Add("onclick", "javascript: return fnGetEditMode('" + e.Row.RowIndex + "');");
                    lbCancel.Attributes.Add("onclick", "javascript: return GetCancelMode('" + e.Row.RowIndex + "');");

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.ToString();
                clsLogger.ErrorLog(ex);
            }
        }
        /// <summary>
        /// Gridview Page Index Change Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_records.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Gridview Row Command Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            HiddenField hdnReportId;
            try
            {
                int iRow;
                switch (e.CommandName)
                {
                    case "DisplaySchedule":
                        iRow = Convert.ToInt32(e.CommandArgument);
                        hdnReportId = gv_records.Rows[iRow].FindControl("hdnReportId") as HiddenField;
                        mpeTrafficwaiting.Show();
                        ValidationReportScheduling1.ReportId = hdnReportId.Value;
                        ValidationReportScheduling1.DisplayReportSchedule();
                        break;
                    case "Update":
                        {
                            iRow = Convert.ToInt32(e.CommandArgument);
                            hdnReportId = gv_records.Rows[iRow].FindControl("hdnReportId") as HiddenField;
                            object[] obj = { hdnReportId.Value, hdnPriUserMon.Value, hdnPriUserTue.Value, hdnPriUserWed.Value, hdnPriUserThu.Value, hdnPriUserFri.Value, hdnPriUserSat.Value, hdnSecUserMon.Value, hdnSecUserTue.Value, hdnSecUserWed.Value, hdnSecUserThu.Value, hdnSecUserFri.Value, hdnSecUserSat.Value };
                            _oValidationReportMapper.UpdateValidationUsers(obj);
                            BindGrid();
                            Pagingctrl.Visible = true;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Bind grid with dataset.
        /// </summary>
        private void BindGrid()
        {
            //SAEED 7791 06/25/2010 method created.
            try
            {
                var dt = _oValidationReportMapper.GetAllSettings();
                if (dt != null && dt.Rows.Count > 0)
                {
                    gv_records.DataSource = dt.DefaultView;
                    gv_records.DataBind();
                    Pagingctrl.GridView = gv_records;
                    Pagingctrl.PageCount = gv_records.PageCount;
                    Pagingctrl.PageIndex = gv_records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Fill user dropdown with all active user's list.
        /// </summary>
        /// <param name="ddl"></param>
        void FillDropDown(ListControl ddl)
        {
            //SAEED 7791 06/25/2010 method created.
            try
            {
                if (_dtUser == null)
                    _dtUser = _oValidationReportMapper.GetAllUser();
                ddl.DataSource = _dtUser;
                ddl.DataTextField = "username";
                ddl.DataValueField = "employeeid";
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }
        #endregion


    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidationReprotSetting.ascx.cs"
    Inherits="HTP.WebControls.ValidationReprotSetting" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ValidationReportScheduling.ascx" TagName="ValidationReportScheduling"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<link href="../Styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function SetSelectedValues(rowid)
{
    var grdname="gv_records";
    //var rowid = document.getElementById("ValidationReportSettings_hdfrowid").value;
    rowid = parseInt(rowid) + 2;
    var row = "";
    if (rowid > 9)
        row = 'ValidationReportSettings_' + grdname + "_ctl" + rowid;
    else
        row = 'ValidationReportSettings_' + grdname + "_ctl0" + rowid;
    var dll;    
    
    document.getElementById('ValidationReportSettings_hdnPriUserMon').value=document.getElementById(row +"_ddlPriMon").value;        
    document.getElementById('ValidationReportSettings_hdnPriUserTue').value=document.getElementById(row +"_ddlPriTue").value;    
    document.getElementById('ValidationReportSettings_hdnPriUserWed').value=document.getElementById(row +"_ddlPriWed").value;    
    document.getElementById('ValidationReportSettings_hdnPriUserThu').value=document.getElementById(row +"_ddlPriThu").value;    
    document.getElementById('ValidationReportSettings_hdnPriUserFri').value=document.getElementById(row +"_ddlPriFri").value;    
    document.getElementById('ValidationReportSettings_hdnPriUserSat').value=document.getElementById(row +"_ddlPriSat").value;
    document.getElementById('ValidationReportSettings_hdnSecUserMon').value=document.getElementById(row +"_ddlSecMon").value;    
    document.getElementById('ValidationReportSettings_hdnSecUserTue').value=document.getElementById(row +"_ddlSecTue").value;    
    document.getElementById('ValidationReportSettings_hdnSecUserWed').value=document.getElementById(row +"_ddlSecWed").value;    
    document.getElementById('ValidationReportSettings_hdnSecUserThu').value=document.getElementById(row +"_ddlSecThu").value;    
    document.getElementById('ValidationReportSettings_hdnSecUserFri').value=document.getElementById(row +"_ddlSecFri").value;    
    document.getElementById('ValidationReportSettings_hdnSecUserSat').value=document.getElementById(row +"_ddlSecSat").value;
        
    return true;                    
}

function fnGetEditMode(rowid)
	{	
	
	    var grdname="gv_records";
	    rowid = parseInt(rowid) + 2;
        var row = "";
        if (rowid > 9)
            row = 'ValidationReportSettings_' + grdname + "_ctl" + rowid;
        else
            row = 'ValidationReportSettings_' + grdname + "_ctl0" + rowid;
       
        var prevrow = document.getElementById("ValidationReportSettings_hdfrowid").value;
        
        if(prevrow != '0')
        {        
            var grdname="gv_records";
	        rowid1 = parseInt(prevrow) ;
            var row1 = "";
            if (rowid1 > 9)
                row1 = 'ValidationReportSettings_' + grdname + "_ctl" + rowid1;
            else
                row1 = 'ValidationReportSettings_' + grdname + "_ctl0" + rowid1;
                
            document.getElementById(row1 +"_divEditRow").style.display = 'block';
            document.getElementById(row1 +"_divDispRow").style.display = 'none';
            
            document.getElementById(row1 +"_divMon1").style.display = 'none';
            document.getElementById(row1 +"_divMon").style.display = 'block';
            document.getElementById(row1 +"_divTue1").style.display = 'none';
            document.getElementById(row1 +"_divTue").style.display = 'block';
            
            document.getElementById(row1 +"_divWed1").style.display = 'none';
            document.getElementById(row1 +"_divWed").style.display = 'block';
            document.getElementById(row1 +"_divThu1").style.display = 'none';
            document.getElementById(row1 +"_divThu").style.display = 'block';
            document.getElementById(row1 +"_divFri1").style.display = 'none';
            document.getElementById(row1 +"_divFri").style.display = 'block';
            document.getElementById(row1 +"_divSat1").style.display = 'none';
            document.getElementById(row1 +"_divSat").style.display = 'block';
            
            
            document.getElementById(row +"_divEditRow").style.display = 'none';
            document.getElementById(row +"_divDispRow").style.display = 'block';
            
            document.getElementById(row +"_divMon1").style.display = 'block';
            document.getElementById(row +"_divMon").style.display = 'none';
            document.getElementById(row +"_divTue1").style.display = 'block';
            document.getElementById(row +"_divTue").style.display = 'none';
            
            document.getElementById(row +"_divWed1").style.display = 'block';
            document.getElementById(row +"_divWed").style.display = 'none';
            document.getElementById(row +"_divThu1").style.display = 'block';
            document.getElementById(row +"_divThu").style.display = 'none';
            document.getElementById(row +"_divFri1").style.display = 'block';
            document.getElementById(row +"_divFri").style.display = 'none';
            document.getElementById(row +"_divSat1").style.display = 'block';
            document.getElementById(row +"_divSat").style.display = 'none';
            	
            
            FillDropdownList(document.getElementById(row +"_ddlPriMon"),document.getElementById(row +"_hdfPriMon"));
            FillDropdownList(document.getElementById(row +"_ddlSecMon"),document.getElementById(row +"_hdfSecMon"));
            FillDropdownList(document.getElementById(row +"_ddlPriTue"),document.getElementById(row +"_hdfPriTue"));
            FillDropdownList(document.getElementById(row +"_ddlSecTue"),document.getElementById(row +"_hdfSecTue"));
            
            FillDropdownList(document.getElementById(row +"_ddlPriWed"),document.getElementById(row +"_hdfPriWed"));
            FillDropdownList(document.getElementById(row +"_ddlSecWed"),document.getElementById(row +"_hdfSecWed"));
            FillDropdownList(document.getElementById(row +"_ddlPriThu"),document.getElementById(row +"_hdfPriThu"));
            FillDropdownList(document.getElementById(row +"_ddlSecThu"),document.getElementById(row +"_hdfSecThu"));
            FillDropdownList(document.getElementById(row +"_ddlPriFri"),document.getElementById(row +"_hdfPriFri"));
            FillDropdownList(document.getElementById(row +"_ddlSecFri"),document.getElementById(row +"_hdfSecFri"));
            FillDropdownList(document.getElementById(row +"_ddlPriSat"),document.getElementById(row +"_hdfPriSat"));
            FillDropdownList(document.getElementById(row +"_ddlSecSat"),document.getElementById(row +"_hdfSecSat"));
            
            document.getElementById("ValidationReportSettings_hdfrowid").value = rowid
	    return false;
	    }
	    else
	    {
            document.getElementById(row +"_divEditRow").style.display = 'none';
            document.getElementById(row +"_divDispRow").style.display = 'block';
            
            document.getElementById(row +"_divMon1").style.display = 'block';
            document.getElementById(row +"_divMon").style.display = 'none';
            document.getElementById(row +"_divTue1").style.display = 'block';
            document.getElementById(row +"_divTue").style.display = 'none';
            
            document.getElementById(row +"_divWed1").style.display = 'block';
            document.getElementById(row +"_divWed").style.display = 'none';
            document.getElementById(row +"_divThu1").style.display = 'block';
            document.getElementById(row +"_divThu").style.display = 'none';
            document.getElementById(row +"_divFri1").style.display = 'block';
            document.getElementById(row +"_divFri").style.display = 'none';
            document.getElementById(row +"_divSat1").style.display = 'block';
            document.getElementById(row +"_divSat").style.display = 'none';
            
            
            FillDropdownList(document.getElementById(row +"_ddlPriMon"),document.getElementById(row +"_hdfPriMon"));
            FillDropdownList(document.getElementById(row +"_ddlSecMon"),document.getElementById(row +"_hdfSecMon"));
            FillDropdownList(document.getElementById(row +"_ddlPriTue"),document.getElementById(row +"_hdfPriTue"));
            FillDropdownList(document.getElementById(row +"_ddlSecTue"),document.getElementById(row +"_hdfSecTue"));
            
            FillDropdownList(document.getElementById(row +"_ddlPriWed"),document.getElementById(row +"_hdfPriWed"));
            FillDropdownList(document.getElementById(row +"_ddlSecWed"),document.getElementById(row +"_hdfSecWed"));
            FillDropdownList(document.getElementById(row +"_ddlPriThu"),document.getElementById(row +"_hdfPriThu"));
            FillDropdownList(document.getElementById(row +"_ddlSecThu"),document.getElementById(row +"_hdfSecThu"));
            FillDropdownList(document.getElementById(row +"_ddlPriFri"),document.getElementById(row +"_hdfPriFri"));
            FillDropdownList(document.getElementById(row +"_ddlSecFri"),document.getElementById(row +"_hdfSecFri"));
            FillDropdownList(document.getElementById(row +"_ddlPriSat"),document.getElementById(row +"_hdfPriSat"));
            FillDropdownList(document.getElementById(row +"_ddlSecSat"),document.getElementById(row +"_hdfSecSat"));
            
            document.getElementById("ValidationReportSettings_hdfrowid").value = rowid            
	        return false; 
	    }
	   }
function FillDropdownList(ddl,selectedValue)
{

    var ddlUser=document.getElementById('ValidationReportSettings_ddlUsers')
    var option;    
    var selectedIndex=ddl.selectedIndex;
    for(var i=0;i<ddlUser.length;i++)
    {
        option = document.createElement("option");    
        option.text=ddlUser.options[i].text;
        option.value=ddlUser.options[i].value;                
        ddl.options.add(option);
        if(selectedValue.value.trim()==option.text.trim())
        {
            if(selectedValue.value.trim()!='')
                ddl.selectedIndex=i;
            else ddl.selectedIndex=-1;
        }
    }
}	   
function GetCancelMode(rowid)
	   {
	    var grdname="gv_records";
	    rowid = parseInt(rowid) + 2;
        var row = "";
        if (rowid > 9)
            row = 'ValidationReportSettings_' + grdname + "_ctl" + rowid;
        else
            row = 'ValidationReportSettings_' + grdname + "_ctl0" + rowid;
            
	    document.getElementById(row +"_divEditRow").style.display = 'block';
        document.getElementById(row +"_divDispRow").style.display = 'none';
        
        document.getElementById(row +"_divMon").style.display = 'block';        
        document.getElementById(row +"_divMon1").style.display = 'none';
        document.getElementById(row +"_ddlPriMon").value = document.getElementById(row +"_hdfPriMon").value;
        document.getElementById(row +"_ddlSecMon").value = document.getElementById(row +"_hdfSecMon").value;
        
        document.getElementById(row +"_divTue").style.display = 'block';        
        document.getElementById(row +"_divTue1").style.display = 'none';
        document.getElementById(row +"_ddlPriTue").value = document.getElementById(row +"_hdfPriTue").value;
        document.getElementById(row +"_ddlSecTue").value = document.getElementById(row +"_hdfSecTue").value;
        
        document.getElementById(row +"_divWed").style.display = 'block';        
        document.getElementById(row +"_divWed1").style.display = 'none';
        document.getElementById(row +"_ddlPriWed").value = document.getElementById(row +"_hdfPriWed").value;
        document.getElementById(row +"_ddlSecWed").value = document.getElementById(row +"_hdfSecWed").value;
        
        document.getElementById(row +"_divThu").style.display = 'block';        
        document.getElementById(row +"_divThu1").style.display = 'none';
        document.getElementById(row +"_ddlPriThu").value = document.getElementById(row +"_hdfPriThu").value;
        document.getElementById(row +"_ddlSecThu").value = document.getElementById(row +"_hdfSecThu").value;
        
        
        document.getElementById(row +"_divFri").style.display = 'block';        
        document.getElementById(row +"_divFri1").style.display = 'none';
        document.getElementById(row +"_ddlPriFri").value = document.getElementById(row +"_hdfPriFri").value;
        document.getElementById(row +"_ddlSecFri").value = document.getElementById(row +"_hdfSecFri").value;
        
        document.getElementById(row +"_divSat").style.display = 'block';        
        document.getElementById(row +"_divSat1").style.display = 'none';
        document.getElementById(row +"_ddlPriSat").value = document.getElementById(row +"_hdfPriSat").value;
        document.getElementById(row +"_ddlSecSat").value = document.getElementById(row +"_hdfSecSat").value;
        
        
        
	    return false;  
	   }
	   
	   	   
</script>

<div>
    <asp:HiddenField ID="hdnPriUserMon" runat="server" />
    <asp:HiddenField ID="hdnPriUserTue" runat="server" />
    <asp:HiddenField ID="hdnPriUserWed" runat="server" />
    <asp:HiddenField ID="hdnPriUserThu" runat="server" />
    <asp:HiddenField ID="hdnPriUserFri" runat="server" />
    <asp:HiddenField ID="hdnPriUserSat" runat="server" />
    <asp:HiddenField ID="hdnSecUserMon" runat="server" />
    <asp:HiddenField ID="hdnSecUserTue" runat="server" />
    <asp:HiddenField ID="hdnSecUserWed" runat="server" />
    <asp:HiddenField ID="hdnSecUserThu" runat="server" />
    <asp:HiddenField ID="hdnSecUserFri" runat="server" />
    <asp:HiddenField ID="hdnSecUserSat" runat="server" />
    <aspnew:UpdatePanel ID="upValidationReport" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:DropDownList ID="ddlUsers" runat="server" AutoPostBack="true" Style="display: none">
            </asp:DropDownList>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tr>
                    <td style="width: 815px" colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td style="width: 816px" colspan="4">
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td colspan="4">
                                    <table id="Table2" cellspacing="1" cellpadding="1" width="784" border="0">
                                        <tr>
                                            <td valign="middle" background="../Images/subhead_bg.gif" class="clssubhead" height="34">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="clssubhead">
                                                            <strong>
                                                                <asp:Label ID="lbl_DocketDate" runat="server"></asp:Label>
                                                                Validation Report Settings</strong>
                                                        </td>
                                                        <td align="right">
                                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="10">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="100%" colspan="4">
                                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gv_records" runat="server" Width="784px" AutoGenerateColumns="False"
                                        AllowPaging="True" PageSize="20" OnRowDataBound="gv_records_RowDataBound" OnRowCommand="gv_records_RowCommand"
                                        OnPageIndexChanging="gv_records_PageIndexChanging" OnRowEditing="gv_records_RowEditing" OnRowUpdating="gv_records_RowUpdating" CssClass="clsLeftPaddingTable"
                                        FooterStyle-BorderStyle="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Report Title" ItemStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbReportTitle" runat="server" CssClass="clsLeftPaddingTable"
                                                        CommandName="DisplaySchedule" Text='<%# DataBinder.Eval(Container, "DataItem.Title") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User Type" HeaderStyle-CssClass="clssubhead" ItemStyle-Width="100px">
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <td height="1px;">
                                                            &nbsp;
                                                        </td>
                                                        <tr>
                                                            <td height="12px">
                                                                <asp:Label ID="lblPrimaryUser" runat="server" CssClass="clsLeftPaddingTable" Text="Primary User"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1px">
                                                                <hr style="color: #E8E8E8;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="12px">
                                                                <asp:Label ID="lblSecondaryUser" runat="server" CssClass="clsLeftPaddingTable" Text="Secondary User"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <td height="1px;">
                                                            &nbsp;
                                                        </td>
                                                    </table>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MON" HeaderStyle-CssClass="clssubhead">
                                                <ItemTemplate>
                                                    <div runat="server" id="divMon" style="display: block">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblPriMon" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Pri_Mon") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblSecMon" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Mon") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </table>
                                                    </div>
                                                    <div runat="server" id="divMon1" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlPriMon" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlSecMon" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </table>
                                                    </div>
                                                    <asp:HiddenField ID="hdfPriMon" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Pri_Mon") %>' />
                                                    <asp:HiddenField ID="hdfSecMon" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Sec_Mon") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TUE" HeaderStyle-CssClass="clssubhead">
                                                <ItemTemplate>
                                                    <div runat="server" id="divTue" style="display: block">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblPriTue" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Pri_Tue") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblSecTue" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Tue") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div runat="server" id="divTue1" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlPriTue" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlSecTue" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <asp:HiddenField ID="hdfPriTue" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Pri_Tue") %>' />
                                                    <asp:HiddenField ID="hdfSecTue" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Sec_Tue") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="WED" HeaderStyle-CssClass="clssubhead">
                                                <ItemTemplate>
                                                    <div runat="server" id="divWed" style="display: block">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblPriWed" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Pri_Wed") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblSecWed" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Wed") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div runat="server" id="divWed1" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlPriWed" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSecWed" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <asp:HiddenField ID="hdfPriWed" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Pri_Wed") %>' />
                                                    <asp:HiddenField ID="hdfSecWed" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Sec_Wed") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="THU" HeaderStyle-CssClass="clssubhead">
                                                <ItemTemplate>
                                                    <div runat="server" id="divThu" style="display: block">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblPriThu" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Pri_Thu") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblSecThu" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Thu") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div runat="server" id="divThu1" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlPriThu" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlSecThu" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <asp:HiddenField ID="hdfPriThu" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Pri_Thu") %>' />
                                                    <asp:HiddenField ID="hdfSecThu" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Sec_Thu") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FRI" HeaderStyle-CssClass="clssubhead">
                                                <ItemTemplate>
                                                    <div runat="server" id="divFri" style="display: block">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblPriFri" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Pri_Fri") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblSecFri" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Fri") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div runat="server" id="divFri1" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlPriFri" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlSecFri" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <asp:HiddenField ID="hdfPriFri" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Pri_Fri") %>' />
                                                    <asp:HiddenField ID="hdfSecFri" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Sec_Fri") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SAT" HeaderStyle-CssClass="clssubhead">
                                                <ItemTemplate>
                                                    <div runat="server" id="divSat" style="display: block">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblPriSat" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Pri_Sat") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:Label ID="lblSecSat" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Sat") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div runat="server" id="divSat1" style="display: none">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <td height="1px;">
                                                                &nbsp;
                                                            </td>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlPriSat" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="1px">
                                                                    <hr style="color: #E8E8E8;" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="12px">
                                                                    <asp:DropDownList ID="ddlSecSat" runat="server" CssClass="clsInputadministration"
                                                                        Width="70px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <asp:HiddenField ID="hdfPriSat" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Pri_Sat") %>' />
                                                    <asp:HiddenField ID="hdfSecSat" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Sec_Sat") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnReportId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ReportID") %>' />
                                                    <div runat="server" id="divEditRow" style="display: block;" align="center">
                                                        <asp:LinkButton ID="lbEdit" runat="server" Text="Edit" CausesValidation="false" CommandName="Edit"></asp:LinkButton>
                                                    </div>
                                                    <div runat="server" id="divDispRow" style="display: none;" align="center">
                                                        <asp:LinkButton ID="lbSave" runat="server" Text="Save" CommandName="Update"></asp:LinkButton><br />
                                                        <br />
                                                        <asp:LinkButton ID="lbCancel" runat="server" Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" CssClass="clssubhead" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                    </asp:GridView>
                                    <asp:HiddenField ID="hdfrowid" runat="server" Value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 760px" align="left" colspan="4">
                                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </aspnew:UpdatePanel>
</div>
<aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
    <ProgressTemplate>
        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
            CssClass="clsLabel"></asp:Label>
    </ProgressTemplate>
</aspnew:UpdateProgress>
<aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlFollowup" runat="server">
            <uc3:ValidationReportScheduling ID="ValidationReportScheduling1" runat="server" Title="Validation Report Scheduling" />
        </asp:Panel>
    </ContentTemplate>
</aspnew:UpdatePanel>
<aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
    <ContentTemplate>
        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
            PopupControlID="pnlFollowup" TargetControlID="btn">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Button ID="btn" runat="server" Style="display: none;" />
    </ContentTemplate>
</aspnew:UpdatePanel>

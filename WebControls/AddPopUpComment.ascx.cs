﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.Components;

namespace lntechNew.WebControls
{
    public partial class AddPopUpComment : System.Web.UI.UserControl
    {
        #region Variables
        public event PageMethodHandler PageMethod;
        public event PageMethodHandler PageClose;

        // create and instance of bug logger
        clsLogger bugTracker = new clsLogger();

        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();

        // create an instance of session
        clsSession cSession = new clsSession();
        #endregion

        #region Properties

        // Message Title
        public string Title
        {
            set
            {
                lbl_head.Text = value;
            }
        }

        // Ticket Id
        public int TicketId
        {
            get
            {
                if (ViewState["TicketId"] == null)
                {
                    return 0;
                }

                return Convert.ToInt32(ViewState["TicketId"]);
            }
            set
            {
                ViewState["TicketId"] = value;
            }
        }

        // Flag id required means actual value in database
        public int FlagId
        {
            get
            {
                if (ViewState["FlagId"] == null)
                {
                    return 0;
                }

                return Convert.ToInt32(ViewState["FlagId"]);
            }
            set
            {
                ViewState["FlagId"] = value;
            }
        }

        // Flag type
        public int FlagType
        {
            get
            {
                if (ViewState["FlagType"] == null)
                {
                    return 0;
                }

                return Convert.ToInt32(ViewState["FlagType"]);
            }
            set
            {
                ViewState["FlagType"] = value;
            }
        }

        // Set the message about comment
        public string Message
        {
            get
            {
                return lblMessage.Text;
            }
            set
            {
                lblMessage.Text = value;
            }
        }

        // promt message to user on empty comment saving
        public string EmptyErrorMessage
        {
            get
            {
                if (ViewState["EmptyErrorMessage"] == null)
                {
                    return string.Empty;
                }

                return ViewState["EmptyErrorMessage"].ToString();
            }
            set
            {
                ViewState["EmptyErrorMessage"] = value;
            }
        }

        // Associated control id from parent
        public string AssociatedControlId
        {
            get
            {
                if (ViewState["AssociatedControlId"] == null)
                {
                    return string.Empty;
                }

                return ViewState["AssociatedControlId"].ToString();
            }
            set
            {
                ViewState["AssociatedControlId"] = value;
            }
        }

        #endregion

        #region Events
        // Page Load's event handler
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnl_control.Attributes.Add("style", "z-index: 3; display: none; position: absolute; background-color: transparent; left: 0px; top: 0px;");
                btnsave.OnClientClick = "return ValidateInput('" + EmptyErrorMessage + "','" + WCC_Comments.ClientID + "_txt_Comments');";
                

            }
        }

        // Save button's event handler
        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string[] key1 = { "TicketID", "flagID", "EmpID" };
                object[] value1 = { this.TicketId, this.FlagId, cSession.GetCookie("sEmpID", this.Request) };

                ClsDb.ExecuteSP("USP_HTS_Insert_flag_by_ticketnumber_and_flagID", key1, value1);

                WCC_Comments.AddComments();

                pnl_control.Style["display"] = "none";
                pnl_control.Style["position"] = "absolute";

                // Noufil 7157 12/16/2009 Send email to natisha for if Problem Client (Allow Hire) flag added
                if (this.FlagId == Convert.ToInt32(lntechNew.Components.ClientInfo.FlagType.ProblemClient_AllowHire))
                {
                    clsCase objclscase = new clsCase();
                    objclscase.GetClientInfo(this.TicketId);
                    MailClass objmailclass = new MailClass();
                    string content = objmailclass.GetProblemClientEmailBody("Problem Client (Allow Hire) flag has been added for client <a href= http://" + Request.Url.Authority.ToString() + "/clientinfo/ViolationFeeold.aspx?sMenu=61&casenumber=" + this.TicketId + "&search=0 >" + objclscase.FirstName.ToUpper() + " " + objclscase.LastName.ToUpper() + "</a>");
                    string subject = Convert.ToString(ConfigurationManager.AppSettings["ProblemClientEmailSubject"]).Replace("@Flagname", "Problem Client (Allow Hire)");
                    subject = subject.Replace("@clientname", objclscase.FirstName.ToUpper() + " " + objclscase.LastName.ToUpper());
                    LNHelper.MailClass.SendMail(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["ProblemClientEmailTO"], subject, content, "");
                    //MailClass.SendMail(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["ProblemClientEmailTO"], subject, content, "");
                }

                if (PageMethod != null)
                    PageMethod();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                if (PageMethod != null)
                    PageMethod();
            }
            finally
            {//Nasir 6098 08/21/2009 general comment initialize overload method
                WCC_Comments.Initialize(this.TicketId, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), this.FlagType, "Label", "clsinputadministration", "clsbutton", "Label", false);
                WCC_Comments.Button_Visible = false;
            }
        }

        // Close button's event handler
        protected void lbtn_close_Click(object sender, EventArgs e)
        {
            HidePopUp();

        }

        // Cancel button's event handler
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            HidePopUp();
        }

        #endregion

        #region Methods

        // Initialize the default setting to get comments of flag
        public void Initialize(string title, int emplId, int flagId, int ticketId, int flagType, string panelId)
        {
            this.Title = title;
            this.FlagId = flagId;
            this.TicketId = ticketId;
            this.FlagType = flagType;

            pnl_control.Style["display"] = "block";
            pnl_control.Style["position"] = "";

            btnCancel.Attributes.Remove("onClick");
            lbtn_close.Attributes.Remove("onClick");

            btnCancel.Attributes.Add("onClick", "return closeModalPopup('" + panelId + "','" + AssociatedControlId + "');");
            lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + panelId + "','" + AssociatedControlId + "');");

            WCC_Comments.Initialize(TicketId, emplId, flagType, "Label", "clsinputadministration", "clsbutton", true);
            WCC_Comments.Button_Visible = false;

            //Nasir 6098 08/17/2009 Hide check box for other except general and reminder call comments
            if (flagType == 1 || flagType == 3)
            {
                WCC_Comments.chk_Complaint.Visible = true;
                WCC_Comments.lbl_Check_Complaint.Visible = true;
            }
        }

        // Hide the user control
        private void HidePopUp()
        {
            pnl_control.Style["display"] = "none";
            pnl_control.Style["position"] = "absolute";

            if (PageClose != null)
                PageClose();

            WCC_Comments.Initialize(this.TicketId, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), this.FlagType, "Label", "clsinputadministration", "clsbutton", false);
            WCC_Comments.Button_Visible = false;
        }
        #endregion
    }
}
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components.ClientInfo;
using System.Collections.Specialized;
using System.Security.Principal;
using System.Configuration;
using HTP.Components;


namespace lntechNew.WebControls
{


    /// <summary>
    ///		Summary description for ActiveMenu.
    /// </summary>
    public partial class ActiveMenu : System.Web.UI.UserControl
    {
        //Ozair 7379 03/11/2010 added image folder variable
        string imgFolder;
        //Added By Zeeshan
        string mainMenuName;
        string subMenuName;
        //Nasir 7077 02/22/2010 declare veriable
        bool IsSubMenuHidden;
        //
        string casenumber = "";
        clsSession cSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        //Waqas 7077 12/14/2009 adding menu setting class
        MenuSettings MenuDB = new MenuSettings();
        clsENationWebComponents clsDB = new clsENationWebComponents();
        protected System.Web.UI.WebControls.LinkButton LinkButton1;
        protected System.Web.UI.WebControls.LinkButton LinkButton12;
        protected System.Web.UI.WebControls.LinkButton lnkb_LogOff;
        protected System.Web.UI.WebControls.Label lblheading;
        protected System.Web.UI.WebControls.Label lbl_Name;
        protected System.Web.UI.WebControls.Image Image1;
        protected System.Web.UI.WebControls.TextBox txtid;
        protected System.Web.UI.WebControls.TextBox txtsrch;

        //Nasir 7077 02/22/2010 declared variable
        string URL;


        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //				string sTicketId ;
                //				if (ViewState["vTicketID"] != null)
                //					 sTicketId = ViewState["vTicketID"].ToString();
                try
                {
                    hidMenuID.Value = Request.AppRelativeCurrentExecutionFilePath;
                     ViewState["sMenu"] = Convert.ToInt32(Request.QueryString["sMenu"]);
                    ViewState["casenumber"] = Convert.ToInt32(Request.QueryString["casenumber"]);
                    
                }
                catch
                {
                    ViewState["sMenu"] = 0;
                    ViewState["casenumber"] = 0;
                }
                if (!Page.IsPostBack)
                {
                    //Sabir Khan 10920 05/27/2013 Need to display Machine name and Branch of the user.
                    string localIPaddress = string.Empty;
                    localIPaddress = cSession.GetCookie("LocalIPAddress", this.Request);
                   // lblClientIPAddress.Text = localIPaddress;
                    string branchName = string.Empty;
                    branchName = cSession.GetCookie("BranchName", this.Request);
                    //if (lblBranchName != null)
                    //{
                    //    lblBranchName.Text = branchName;
                    //}

                    lbl_Name.Text = cSession.GetCookie("sEmpName", this.Request).ToUpper();
                    string attr=idgenerailinfo.Attributes["href"].ToString();
                    idgenerailinfo.Attributes.Add("href",attr + "&search=0&caseNumber="+ViewState["casenumber"].ToString());

                    string attr1 = idpaymentinfo.Attributes["href"].ToString();
                    idpaymentinfo.Attributes.Add("href", attr1 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());

                    string attr2 = iddocument.Attributes["href"].ToString();
                    iddocument.Attributes.Add("href", attr2 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());

                    string attr3 = idcasehistory.Attributes["href"].ToString();
                    idcasehistory.Attributes.Add("href", attr3 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());

                    string attr4 = idcomment.Attributes["href"].ToString();
                    idcomment.Attributes.Add("href", attr4 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());

                    string attr5 = idassociatedmatters.Attributes["href"].ToString();
                    idassociatedmatters.Attributes.Add("href", attr5 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());

                    string attr6 = idcasedisposition.Attributes["href"].ToString();
                    idcasedisposition.Attributes.Add("href", attr6 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());

                    string attr7 = idviolationinfo.Attributes["href"].ToString();
                    idviolationinfo.Attributes.Add("href", attr7 + "&search=0&caseNumber=" + ViewState["casenumber"].ToString());
                    clsContact clscontact = new clsContact();
                    if (txtid.Text != "" && clscontact.GetContactIDByTicketID(Convert.ToInt32(txtid.Text)) == 0)
                    {
                        associateMenu.Visible = true;
                    }
                    else
                    {
                        associateMenu.Visible = false;
                    }
                    if (Request.QueryString["casenumber"] != null)
                    {
                        casenumber = Request.QueryString["casenumber"].ToString();
                    }

                    //Added by Zeeshan Ahmed 
                    //Hide Logoff Button If User In An NT user
                    IIdentity wid = HttpContext.Current.User.Identity;

                    if (wid.IsAuthenticated)
                    {
                        WindowsIdentity winID = (WindowsIdentity)wid;

                        if (winID.IsAuthenticated)
                        {
                            td_logoff1.Visible = false;
                           // td_logoff2.Visible = false;
                        }
                    }

                    string userId = cSession.GetCookie("sUserID", this.Request);
                    string password = cSession.GetCookie("Password", this.Request);
                    string key = userId + "-HTP_" + password;
                    string encryptedString = "";

                    foreach (char ch in key.ToCharArray())
                    {
                        encryptedString += Convert.ToChar((int)ch + 2);
                    }

                    // string encryptedString = Encrypt(key, "!@#$!@#$!@#$");

                    //this.hlTicketDesk.NavigateUrl = ConfigurationManager.AppSettings["SupportTicket"] + "?HTP=" + encryptedString;

                    //Aziz 4205 06/10/08 Logging page view information
                    LogPageView();

                }
                //txtid.Text = iTiketId.ToString();
                //txtsrch.Text = iSearch.ToString();
                PageDirect();
                QueryWork();
                DirectoryWork();
                //tt.Attributes.Add("onclick", "return OpenPopUpTTM(" + casenumber + ");");
                //tt.Attributes.Add("onclick", "return OpenPopUpTTM();");
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void DirectoryWork()
        {
            //Ozair 7379 03/11/2010 images folder path set
            string serverName = Request.ServerVariables["SERVER_NAME"];
            if (serverName.ToLower() == "localhost")
            {
                ViewState["hfServerAdd"] = "http://" + serverName + ":" + Request.ServerVariables["SERVER_PORT"];
                imgFolder = ViewState["hfServerAdd"] + "/Images";
                
            }
            else
            {
                ViewState["hfServerAdd"] = "http://" + serverName;
                imgFolder = ViewState["hfServerAdd"] + "/Images";
            }
                
        }

        private void QueryWork()
        {
            //---------------------Request work

            //check menu command executed or not
            if (Request["MENU"] == null)
            {
                //NOT then CHECK FOR ANY PREVIUOSE MENU EXECUTION

                //if(Session["topMenu"] == null)
                if (Request.Cookies["topMenu"] == null)
                {
                    // IF NOT FOUND THEN SET MENU FLAG TO 1 WHICH IS HOME	
                    //Session["topMenu"] = "1";
                    cSession.CreateCookie("topMenu", "1", this.Request, this.Response);
                }
            }
            else
            {
                //MENU COMMAND IS EXECUTED AT THIS PAGE RECORD IT		
                //Session["topMenu"] = Request["meNU"]; 
                cSession.CreateCookie("topMenu", Request["meNU"], this.Request, this.Response);
            }
            
            //::::::::::::::::::::::::::::::::::::::::
            //check if submenu command executed
            if (Request["sMENU"] != null)
            {
                //yes then record it
                //Session["subMenu"] = Request["sMENU"]; 
                cSession.CreateCookie("subMenu", Request["sMENU"], this.Request, this.Response);
                cSession.CreateCookie("LastPage", Request.ServerVariables["SCRIPT_NAME"], this.Request, this.Response);
            }
            else
            {

                if (Request["MENU"] != null)
                {  //if menu is triggered then set to selected WHICH MEAN SUB MENU ROUTINE SHOULD EXECUTE DEFAULT ITEM ON MENU LOADING	'no then set to selected WHICH MEAN SUB MENU ROUTINE SHOULD EXECUTE DEFAULT ITEM ON MENU LOADING	'no then set to selected WHICH MEAN SUB MENU ROUTINE SHOULD EXECUTE DEFAULT ITEM ON MENU LOADIN
                    //Session["subMenu"] = 0 ; 
                    cSession.CreateCookie("subMenu", "0", this.Request, this.Response);
                }

                //else last sub menu setting will be preserved
            }
            //			Response.Cookies["topMenu"].Value =Session["topMenu"].ToString() ;
            //			Response.Cookies["subMenu"].Value  =Session["subMenu"].ToString() ;

        }

        private void PageDirect()
        {
            try
            {
                //------------------------------ script if page call direct
                //				Session["topMenu"]=Request.Cookies["topMenu"].Value ; 
                //				Session["subMenu"]=Request.Cookies["subMenu" ].Value ; 

                string tab_page;
                //tab_page="zee";

                tab_page = Request.ServerVariables["SCRIPT_NAME"].ToString().ToUpper();
                //string last=Request.Cookies["LastPage"].Value.ToString().ToUpper();// .ToString().ToUpper();


                //if( Request.Cookies["LastPage"].Value.ToString().ToUpper() != tab_page )
                if (cSession.GetCookie("LastPage", this.Request).ToUpper() != tab_page)
                {
                    //if call any page direct from link in another page then this method will execute and set the menu variables


                    //Change By Ajmal
                    //Waqas 7025 11/24/2009 Using Applciaion state now to hold menu and sub menu
                    DataRow dR = null;
                    try
                    {
                        //Change By Agha Usman For Task Id 3210 February 21 2008
                        int id = Convert.ToInt32((ViewState["sMenu"]));

                        if (id != 0)
                        {
                            DataTable dt = GetSubMenuData();
                            if (dt != null)
                            {
                                dt = FilterData(dt, "URL= '" + tab_page + "' and ID = " + id.ToString() + " and active = true ", "ORDERING,ID ASC");
                                if (dt.Rows.Count != 0)
                                {
                                    dR = dt.Rows[0];
                                }
                            }

                        }
                        else
                        {

                            DataTable dt = GetSubMenuData();
                            if (dt != null)
                            {
                                dt = FilterData(dt, "URL= '" + tab_page + "' and active = true ", "ORDERING, ID ASC");
                                if (dt.Rows.Count != 0)
                                {
                                    dR = dt.Rows[0];
                                }
                            }
                        }

                        if (dR != null)
                        {
                            //							Session["subMenu"]=dR["id"];
                            //							Session["topMenu"]=dR["menuid"];
                            cSession.CreateCookie("subMenu", dR["id"].ToString(), this.Request, this.Response);
                            cSession.CreateCookie("topMenu", dR["menuid"].ToString(), this.Request, this.Response);


                        }
                        //Response.Cookies["LastPage"].Value = tab_page;
                        cSession.CreateCookie("LastPage", tab_page, this.Request, this.Response);
                    }
                    catch (Exception ex)
                    {
                        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    }
                    //finally
                    //{
                    //    dR.Close();
                    //}
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                string a = ex.Message;
            }
        }

        //Waqas 7025 11/24/2009 Using Applciaion state now to hold menu and sub menu 
        /// <summary>
        /// This method is used to get menu items from application state or from XML file.
        /// </summary>
        /// <returns>It returns datatable with menu items</returns>
        public DataTable GetMenuData()
        {
            DataTable dt = null;
            try
            {
                // Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
                if (Application["MENU"] != null && !HasConfigSettAccess(cSession.GetCookie("sUserID", this.Request)))
                {
                    dt = (DataTable)Application["MENU"];
                }
                else
                {
                    //Waqas 7077 12/14/2009 calling menu items
                    // Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
                    DataSet ds = HasConfigSettAccess(cSession.GetCookie("sUserID", this.Request))
                                     ? MenuDB.GetMenuData(cSession.GetCookie("sUserID", this.Request))
                                     : MenuDB.GetMenuData();
                    dt = ds.Tables[0];
                    if (ds.Tables.Count != 0 && !HasConfigSettAccess(cSession.GetCookie("sUserID", this.Request)))
                    {
                        Application["MENU"] = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dt = null;
            }
            return dt;
        }

        /// <summary>
        /// This method is used to get sub menu items from application state or from XML file.
        /// </summary>
        /// <returns>It returns datatable with sub menu items</returns>
        public DataTable GetSubMenuData()
        {
            DataTable dt = null;
            try
            {
                // Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
                if (Application["SUBMENU"] != null && !HasConfigSettAccess(cSession.GetCookie("sUserID", this.Request)))
                {
                    dt = (DataTable)Application["SUBMENU"];
                }
                else
                {
                    //Waqas 7077 12/14/2009 calling menu items
                    // Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
                    DataSet ds = HasConfigSettAccess(cSession.GetCookie("sUserID", this.Request))
                                     ? MenuDB.GetSubMenuData(cSession.GetCookie("sUserID", this.Request))
                                     : MenuDB.GetSubMenuData();
                    dt = ds.Tables[0];
                    if (ds.Tables.Count != 0 && !HasConfigSettAccess(cSession.GetCookie("sUserID", this.Request)))
                    {
                        Application["SUBMENU"] = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dt = null;
            }
            return dt;
        }

        /// <summary>
        /// This method is used to filter data
        /// </summary>
        /// <param name="dt">It represents the data</param>
        /// <param name="filer">filter criteria</param>
        /// <returns></returns>
        public DataTable FilterData(DataTable dt, string filer, string Sort)
        {
            try
            {
                DataView dv = new DataView(dt);
                dv.RowFilter = filer;
                dv.Sort = Sort;
                dt = dv.ToTable();
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dt = null;
            }
            return dt;

        }

        //Waqas 7025 11/24/2009 Using Applciaion state now to hold menu and sub menu
        /// <summary>
        /// This method is used to get menu items.
        /// </summary>
        /// <returns></returns>
        public string mainMenu()
        {
            string strRet = "";
            string page = Request.ServerVariables["SCRIPT_NAME"].ToString();

            try
            {
                //dR = clsDB.Get_DR_BySP("usp_tab_gettopmenu");

                DataTable dt = GetMenuData();

                if (dt != null)
                    foreach (DataRow dR in dt.Rows)
                    {
                        //Nasir 7077 02/20/2010 if File not exist then set URL to /ErrorPageNotFound.aspx
                        URL = System.IO.File.Exists(Server.MapPath(dR["URL"].ToString())) ? dR["URL"].ToString() : "/ErrorPageNotFound.aspx";
                        //Sabir Khan 5519 02/09/2009 Sub ID has changed into IDMAIN...
                        if (Convert.ToInt32(cSession.GetCookie("topMenu", this.Request)) == Convert.ToInt32(dR["IDMAIN"]))
                        {
                            mainMenuName = dR["TITLE"].ToString();
                            //Sabir Khan 5440 01/23/2009  Change the page variable into dr["URL"] and Menu into sMenu....
                            //Nasir 7077 02/25/2010 replace dR["URL"].ToString() to URL
                            //Ozair 7379 03/11/2010 images folder path set
                            strRet+= "<li class='open'><a href='" + URL + "?sMenu=" + dR["ID"] + "'><i class='fa fa-dashboard'></i><span class='title'>'" + dR["TITLE"] + "'</span></a></li>";
                           // strRet += "<td width='80' align='center' background='../Images/subhead_bg.gif'><A href='" + URL + "?sMenu=" + dR["ID"] + "'><font class='stlclslinkselected'>" + dR["TITLE"] + "</font></A></td>";
                            IsSubMenuHidden = Convert.ToBoolean(dR["IsSubMenuHidden"].ToString());
                        }
                        else//Nasir 7077 02/25/2010 replace dR["URL"].ToString() to URL
                            //Ozair 7379 03/11/2010 images folder path set
                            strRet += "<li class=''><a href='" + URL + "?sMenu=" + dR["ID"] + "'><i class='fa fa-dashboard'></i><span class='title'>'" + dR["TITLE"] + "'</span></a></li>";
                        //strRet += "<td width='80' align='center' background='../Images/navi_repeat.jpg'><A href='" + URL + "?sMenu=" + dR["ID"] + "'><font class='stlclslink'>" + dR["TITLE"] + "</font></A></td>";
                    }
                            //Ozair 7379 03/11/2010 images folder path set
               // strRet += "<td align='cente'r width='273' height='30' background='../Images/navi_repeat.jpg'></td>";
                lblheading.Text = mainMenuName;
                return strRet;
               
            }
            catch (Exception ex)
            {
                //ozair 7077 03/10/2010 error log implemented
                clsLogger.ErrorLog(ex);
                return strRet;
            }


        }

        //Waqas 7025 11/24/2009 Using Applciaion state now to hold menu and sub menu
        /// <summary>
        /// This method is used to get sub menu items.
        /// </summary>
        /// <returns></returns>
        public string subMenu()
        {

            string strRet = "";
            try
            {
                //ozair 4521 08/23/2008 take precautionary steps to resolve conversion issue 
                int topMenu = 0;
                int subMenu = 0;
                string tMenu = cSession.GetCookie("topMenu", this.Request);
                string sMenu = cSession.GetCookie("subMenu", this.Request);
                if (tMenu.Length != 0)
                {
                    topMenu = Convert.ToInt32(tMenu);
                }
                if (sMenu.Length != 0)
                {
                    subMenu = Convert.ToInt32(sMenu);
                }
                //end ozair 4521

                int c = 0;

                DataTable dt = GetSubMenuData();
                if (dt != null)
                {
                    dt = FilterData(dt, "menuid= " + topMenu + " and active =true ", "ORDERING, ID ASC");
                }
                if (dt != null)
                    foreach (DataRow dR in dt.Rows)
                    {
                        //Nasir 7077 02/20/2010 if submenu hidden logic and replace dr["URL"] to URL in sholw method
                        //if (topMenu != 16)
                        URL = System.IO.File.Exists(Server.MapPath(dR["URL"].ToString())) ? dR["URL"].ToString() : "/ErrorPageNotFound.aspx";
                        if (!IsSubMenuHidden)
                        {
                            if (topMenu == 15) // MATTER - client info pages.... adding ticketid
                            {
                                c++;  //COUNT SUB MENU
                                int sMenuID = Convert.ToInt32(dR["ID"]);


                                //Nasir 5771 04/17/2009 hide associated matters page if client ID not associated
                                if (URL == "/clientinfo/AssociatedMatters.aspx")
                                {
                                    clsContact clscontact = new clsContact();
                                    //Nasir 5920 06/10/2009 issue fix
                                    if (txtid.Text != "" && clscontact.GetContactIDByTicketID(Convert.ToInt32(txtid.Text)) == 0)
                                    {
                                        strRet += "<td height='25' style='display: none' width='11%'><div align='center' ><span class='StyleSubMenu1'><a href='" + URL + "?sMenu=" + dR["ID"] + "&casenumber=" + txtid.Text + "&search=" + txtsrch.Text + "'>" + dR["TITLE"] + "</a></span></div></td>";
                                    }
                                    else if (sMenuID != subMenu)
                                    {
                                        strRet += "<td height='25' width='11%'><div align='center' ><span class='StyleSubMenu1'><a href='" + URL + "?sMenu=" + dR["ID"] + "&casenumber=" + txtid.Text + "&search=" + txtsrch.Text + "'>" + dR["TITLE"] + "</a></span></div></td>";
                                    }
                                    else
                                    {
                                        subMenuName = dR["TITLE"].ToString();
                                        lblheading.Text = lblheading.Text + " --> " + subMenuName;
                                        strRet += "<td height='25' width='12%' bgcolor='#0066cc'><div align='center'><span class='StyleSubMenuSelected'>" + dR["TITLE"] + "</span></div></td>";
                                    }
                                }
                                else if (sMenuID != subMenu)//if this menu is not active menu
                                    strRet += "<td height='25' width='11%'><div align='center' ><span class='StyleSubMenu1'><a href='" + URL + "?sMenu=" + dR["ID"] + "&casenumber=" + txtid.Text + "&search=" + txtsrch.Text + "'>" + dR["TITLE"] + "</a></span></div></td>";
                                else//yes this is active menu
                                {

                                    subMenuName = dR["TITLE"].ToString();
                                    lblheading.Text = lblheading.Text + " --> " + subMenuName;
                                    strRet += "<td height='25' width='12%' bgcolor='#0066cc'><div align='center'><span class='StyleSubMenuSelected'>" + dR["TITLE"] + "</span></div></td>";
                                }

                                if (c == 7)//line reach to maximum sub menus now change the row
                                {
                                    strRet += "</tr></table><table width='100%' border='1' cellpadding='0' cellspacing='0' style='BORDER-TOP-STYLE: none;' bordercolor='#ff9966' bgcolor='#ffcc00'><tr>";
                                    c = 0;
                                }
                            }
                            else
                            {
                                c++;  //COUNT SUB MENU
                                int sMenuID = Convert.ToInt32(dR["ID"]);

                                if (sMenuID != subMenu)//if this menu is not active menu
                                    strRet += "<td height='25' width='11%'><div align='center' ><span class='StyleSubMenu1'><a href='" + URL + "?sMenu=" + dR["ID"] + "'>" + dR["TITLE"] + "</a></span></div></td>";
                                else//yes this is active menu
                                {

                                    subMenuName = dR["TITLE"].ToString();
                                    lblheading.Text = lblheading.Text + " --> " + subMenuName;
                                    strRet += "<td height='25' width='12%' bgcolor='#0066cc'><div align='center'><span class='StyleSubMenuSelected'>" + dR["TITLE"] + "</span></div></td>";
                                }

                                if (c == 7)//line reach to maximum sub menus now change the row
                                {
                                    strRet += "</tr></table><table width='100%' border='1' cellpadding='0' cellspacing='0' style='BORDER-TOP-STYLE: none;' bordercolor='#ff9966' bgcolor='#ffcc00'><tr>";
                                    c = 0;
                                }
                            }
                        }

                        if (subMenu == 0) //do default menu work				
                        {
                            int selected = Convert.ToInt32(dR["selected"]);
                            if (selected == 1)
                            {
                                //							if (topMenu == 15)
                                //								Response.Redirect(dR["URL"] + "?sMenu=" + dR["ID"] + "&casenumber=" + txtid.Text + "&search=" + txtsrch.Text ); 	
                                //							else

                                //Kazim 4293 6/24/2008 Set Response.Redirect endresponse property to false 

                                Response.Redirect(URL + "?sMenu=" + dR["ID"], false);
                                goto SearchPage;


                            }
                        }
                    }

            SearchPage:
                { }
                return strRet;

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                return strRet;
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);

        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnkb_LogOff.Click += new System.EventHandler(this.lnkb_LogOff_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion


        private void lnkb_LogOff_Click(object sender, System.EventArgs e)
        {
            //Session.Clear();
            HttpCookieCollection cookies = Request.Cookies;
            string[] cookiearr = cookies.AllKeys;
            for (int i = 0; i < cookiearr.Length; i++)
            {
                if (cSession.IsCookieExist(cookiearr[i], this.Request))
                {
                    cSession.DeleteCookie(cookiearr[i], this.Response);
                    Request.Cookies.Remove(cookiearr[i]);
                }
            }
            Session.Abandon();


            string apath = path();
            Response.Redirect(apath);
        }

        private string path()
        {
            string url = "";
            url = Request.ServerVariables["PATH_INFO"];

            if (url.IndexOf("/", 2) != -1)
                url = "../frmlogin.aspx";
            else
                url = "frmLogin.aspx";

            return url;
        }

        //Aziz 4205 06/10/08 Logging page view information
        private void LogPageView()
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(this.Request, this.Response, this.Session))
                {
                    int employeeId = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                    int subMenuId = Convert.ToInt32(ViewState["sMenu"]);

                    //if(submenuid
                    string[] key2 = { "@SubMenuId", "@EmployeeId" };
                    object[] value2 = { subMenuId, employeeId };

                    clsDB.InsertBySPArr("USP_HTP_InsertPageHistory", key2, value2);
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
        }

        // Zeeshan Haider 11387 09/09/2013 Added access to configuration setting report page
        private bool HasConfigSettAccess(string sUserName)
        {
            return ConfigurationManager.AppSettings["ConfigurationSettingReportAccess"].Contains(sUserName.Trim());
        }
    }
}

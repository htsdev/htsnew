﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using HTP.Components.ClientInfo;
using lntechNew.Components.ClientInfo;

namespace HTP.WebControls
{
    //Created By Waqas 5771 04/20/2009
    public partial class ContactIDLookUp : System.Web.UI.UserControl
    {
        public event PageMethodHandler ContactPageMethod;

        #region Properties
        
        public int TicketID
        {
            set
            {
                hdnTicketID.Value = value.ToString();
            }
        }        

        clsContact clscontact;
        clsSession ClsSession = new clsSession();
        
        #endregion
        
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            int Empid = 0;
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false || (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
            {
                Response.Redirect("../frmlogin.aspx", false);
            }            
        }

        protected void btnAssociate_Click(object sender, EventArgs e)
        {
            
            string ContactID = Request.Form["rbnContactID"];
            if (hdnTicketID.Value == null || hdnTicketID.Value == string.Empty || ContactID == null || ContactID == string.Empty)
            {
                return;
            }
            else
            {
                
                clscontact = new clsContact();

                int Empid = 0;
                int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid);
                clscontact.AssociateContactID(Convert.ToInt32(ContactID), Convert.ToInt32(hdnTicketID.Value), Empid);
            }
            if (ContactPageMethod != null)
                ContactPageMethod();
        }

        protected void btnNewContact_Click(object sender, EventArgs e)
        {
            if (hdnTicketID.Value == null || hdnTicketID.Value == string.Empty ||
                hdnLastName.Value == null || hdnLastName.Value == string.Empty ||
                hdnFirstName.Value == null || hdnFirstName.Value == string.Empty ||
                hdnDOB.Value == null || hdnDOB.Value == string.Empty
                )
            {
                ShowMessage("Required Information not available", true);
                return;
            }
            else
            {
                clscontact = new clsContact();
                int Empid = 0;
                int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid);
                int CID = clscontact.CreateNewContact(hdnLastName.Value.ToString(), hdnFirstName.Value.ToString(), hdnDOB.Value.ToString(), Empid, Convert.ToInt32(hdnTicketID.Value));
                if (CID != 0)
                {
                    clscontact.AssociateContactID(CID, Convert.ToInt32(hdnTicketID.Value), Empid);
                }
                string URL = "generalinfo.aspx?sMenu=62&casenumber=" + hdnTicketID.Value + "&search=0";
                Response.Redirect(URL);
            }   
        }

        #endregion

        #region Methods

        /// <summary>
        /// This Method is used to bind this control with Web Pages and show records on the 
        /// basis of selection criteria given as parameters given bellow.
        /// </summary>
        /// <param name="LastName"></param>
        /// <param name="FirstName"></param>
        /// <param name="DOB"></param>
        /// <param name="PanelID"></param>
        public void BindContactLookUpGrid(string LastName, string FirstName, string DOB, string PanelID)
        {
            clscontact = new clsContact();
            DataTable dt = new DataTable();
            try
            {
                if (LastName != null && FirstName != null && DOB != null)
                {
                    hdnLastName.Value = LastName;
                    hdnFirstName.Value = FirstName;
                    hdnDOB.Value = DOB;

                    dt = clscontact.GetContactLookUp(LastName, FirstName.Substring(0, 1), Convert.ToDateTime(DOB));

                    if (dt == null)
                    {
                        ShowMessage("Record not found", true);
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        ShowMessage("Record not found", true);
                    }
                    else
                    {
                        grdvContactLookUp.Visible = true;
                        grdvContactLookUp.DataSource = dt.DefaultView;
                        grdvContactLookUp.DataBind();
                        //GridViewRow gvr = (GridViewRow)grdvContactLookUp.Rows[0];
                        //RadioButton rb = (RadioButton)gvr.FindControl("rbnContactID");
                        //rb.Checked = true;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Record not found", true);
                grdvContactLookUp.Visible = false;
            }

            lbtn_close.Attributes.Remove("onClick");
            lbtn_close.Attributes.Add("onClick", "return closeModalPopup('" + PanelID + "');");
            pnl_CIDLookUp.Attributes.Add("style", "display:block");
        }

        /// <summary>
        /// This procedure is used to display messages on top of this control.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="onlyShowIt"></param>
        protected void ShowMessage(string message, bool onlyShowIt)
        {
            if (!onlyShowIt && lbl_Message.Text != string.Empty)
            {
                lbl_Message.Text = "- " + lbl_Message.Text + "<br />&nbsp;- " + message;
            }
            else
            {
                lbl_Message.Text = message;
                btnAssociate.Enabled = false;
            }
        }

        #endregion


        

    }

}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="POLMControl.ascx.cs"
    Inherits="HTP.WebControls.PolmControl" %>
<link href="../Styles.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
    function DisableControl()
    {   
        // document.getElementById('<%= btn_save.ClientID %>').disabled = true;
        document.getElementById('<%= dd_QuickLegalDescription.ClientID %>').disabled = true;
        document.getElementById('<%= dd_damagevalue.ClientID %>').disabled = true;
        document.getElementById('<%= dd_Bodilydamage.ClientID %>').disabled = true;
        document.getElementById('<%= dd_attorney.ClientID %>').disabled = true;
        try{document.getElementById('<%= dd_Division.ClientID %>').disabled = true;} catch(e){}
        document.getElementById('<%= TxtLegalMatterDescription.ClientID %>').disabled = true;        
        document.getElementById('<%= td_buttons.ClientID %>').style.display = "none";
        return true;
    }
</script>

<asp35:UpdatePanel ID="upPolmControl" runat="server">
    <ContentTemplate>
        <table class="clsLeftPaddingTable" width="800px">
            <tr>
                <td colspan="2" align="left">
                    <span style="color: red">Information with * is mandatory</span>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" colspan="2" height="11">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 20%">
                    <span class="clsLeftPaddingTable">Quick Legal Matter Description:</span>
                </td>
                <td align="left">
                    <table>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="dd_QuickLegalDescription" runat="server" class="clsInputadministration"
                                    DataTextField="Name" DataValueField="Id" Width="180px">
                                </asp:DropDownList>
                                &nbsp;<span style="color: Red">*</span>
                            </td>
                            <td align="left">
                                <asp:RequiredFieldValidator ID="rfvQLMD" ControlToValidate="dd_QuickLegalDescription"
                                    InitialValue="-1" runat="server" ErrorMessage="Please select Quick Legal Matter Description"
                                    ValidationGroup="AddConsultation" Font-Size="8pt" Style="font-family: Tahoma;"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 20%">
                    <span class="clsLeftPaddingTable">Legal Matter Description:</span>
                </td>
                <td align="left">
                    <table>
                        <tr>
                            <td align="left">
                                <asp:TextBox ID="TxtLegalMatterDescription" runat="server" TextMode="MultiLine" CssClass="clsInputadministration"
                                    Width="500px" Height="80px"></asp:TextBox>
                                &nbsp;<span style="color: Red">*</span><br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:RequiredFieldValidator ID="rfvLegalMatterDescription" ControlToValidate="TxtLegalMatterDescription"
                                    runat="server" Font-Size="8pt" Style="font-family: Tahoma;" ErrorMessage="Please enter Legal Matter Description"
                                    ValidationGroup="AddConsultation"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revLegalMatterDescription" runat="server" ControlToValidate="TxtLegalMatterDescription"
                                    ErrorMessage="Too many characters in Legal Matter Description" SetFocusOnError="true"
                                    ValidationGroup="AddConsultation" ValidationExpression="^[\S\s]{1,1969}$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 20%">
                    <span class="clsLeftPaddingTable">How Bad was the damages:</span>
                </td>
                <td align="left">
                    &nbsp;<asp:DropDownList ID="dd_damagevalue" runat="server" class="clsInputadministration"
                        DataValueField="Id" DataTextField="Name" Width="180px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 20%">
                    <span class="clsLeftPaddingTable">Bodily Damage:</span>
                </td>
                <td align="left">
                    &nbsp;<asp:DropDownList ID="dd_Bodilydamage" runat="server" class="clsInputadministration"
                        Width="180px" DataValueField="Id" DataTextField="Name">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 20%">
                    <span class="clsLeftPaddingTable">Attorney:</span>
                </td>
                <td align="left">
                    &nbsp;<asp:DropDownList ID="dd_attorney" runat="server" class="clsInputadministration" 
                        AppendDataBoundItems="false" Width="180px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trDivision" runat="server">
                <td align="right" runat="server" style="width: 20%">
                    <span class="clsLeftPaddingTable">Partner:</span>
                </td>
                <td align="left">
                    <table>
                        <tr>
                            <td align="left" valign="middle">
                                <asp:DropDownList ID="dd_Division" runat="server" class="clsInputadministration"
                                    Width="180px" DataTextField="CompanyName" DataValueField="CompanyId">
                                </asp:DropDownList>
                                &nbsp;<span style="color: Red">*</span>
                            </td>
                            <td align="left">
                                <asp:RequiredFieldValidator ID="rfvDivision" ControlToValidate="dd_Division" InitialValue="-1"
                                    runat="server" Font-Size="8pt" Style="font-family: Tahoma;" ErrorMessage="Please select Partner"
                                    ValidationGroup="AddConsultation"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trButton" runat="server">
                <td align="left" style="width: 20%; height: 40px">
                </td>
                <td style="height: 40px" align="left">
                    <table width="100%">
                        <tr>
                            <td align="left" id="td_buttons" runat="server">
                                <asp:Button ID="btn_save" runat="server" Text="Save" CssClass="clsbutton" Width="70px"
                                    OnClick="btn_save_Click" ValidationGroup="AddConsultation" OnClientClick="return DisableControl();" />
                            </td>
                            <td align="left">
                                <asp35:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="true"
                                    AssociatedUpdatePanelID="upPolmControl">
                                    <ProgressTemplate>
                                        <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="Lblplzwait" runat="server"
                                            CssClass="clssubhead" Text="Please Wait ..."></asp:Label>
                                    </ProgressTemplate>
                                </asp35:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp35:AsyncPostBackTrigger ControlID="btn_save" EventName="Click" />
    </Triggers>
</asp35:UpdatePanel>

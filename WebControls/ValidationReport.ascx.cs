﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace lntechNew.WebControls
{
    //public delegate void PageMethodHandler();
    public partial class ValidationReport : System.Web.UI.UserControl
    {
        public event PageMethodHandler PageMethod = null;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void fillData(string rptName,string cat,string primUser,string secUser,bool isChecked)
        {
            txtrptName.Text = rptName;
           // rbtCat.SelectedItem.Text = cat;
            rbtCat.Items.FindByText(cat).Selected = true;
            ddlPrimaryUser.Items.Add(new ListItem(primUser));
            ddlSecondryUser.Items.Add(new ListItem(secUser));
            chkIsActive.Checked = isChecked;
        }
    }
}
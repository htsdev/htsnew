﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using HTP.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.WebControls
{
    public partial class AutoDialerSettings : System.Web.UI.UserControl
    {
        AutoDialer clsAD = new AutoDialer();
        clsSession cSession = new clsSession();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = Convert.ToInt16(Session["EventTypeID"]);
                FillData();
            }
        }      

        private void FillData()
        {
            string eventName = String.Empty;
            clsAD.EventTypeID = 1;
            DataTable dt = clsAD.GetEventConfigSettingsByID();
            if (dt.Rows.Count > 0)
            {
                dt.Rows[0]["EventTypeDays"].ToString();
                ddl_Days.SelectedValue = dt.Rows[0]["EventTypeDays"].ToString();
                ddl_Courts.SelectedValue = dt.Rows[0]["CourtID"].ToString();
                ddl_Range.SelectedValue = dt.Rows[0]["SettingDateRange"].ToString();
                bool eventState = Convert.ToBoolean(dt.Rows[0]["EventState"]);
                if (eventState == true)
                {
                    rb_Start.Checked = true;
                }
                else
                {
                    rb_Stop.Checked = true;
                }
                tp_DialTime.SelectedTime = Convert.ToDateTime(dt.Rows[0]["DialTime"]);                
            }
        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            clsAD.EventTypeID = 0;
            clsAD.EventTypeName = "";
            clsAD.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
            clsAD.CallDays = Convert.ToInt16(ddl_Days.SelectedValue);
            clsAD.CourtID = Convert.ToInt32(ddl_Courts.SelectedValue);
            clsAD.SettingDateRange = Convert.ToInt16(ddl_Range.SelectedValue);
            clsAD.EventState = rb_Start.Checked;
            clsAD.DialTime = tp_DialTime.SelectedTime;
        }
    }
}
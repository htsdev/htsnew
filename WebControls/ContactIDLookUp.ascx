﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContactIDLookUp.ascx.cs"
    Inherits="HTP.WebControls.ContactIDLookUp" %>
<link href="../Styles.css" type="text/css" rel="Stylesheet" />

<script language="javascript" type="text/javascript">
 function closeModalPopup(popupid)
    {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }
    function CheckOnOff()
    {
        
        var a = 0;
        var all = document.getElementsByTagName("input");
        for(i=0;i<all.length;i++)
            { 
                if(all[i].type=="radio" && all[i].id == "rbnContactID")
                    {
                        if(all[i].checked==true)
                        {
                            a = 1;
                        }                        
                   }
            }
        if(a==0)
        {
            alert("Please select a contact to associate.");
            return false;
        }
    }

</script>
<div aria-hidden="false" role="dialog" tabindex="-1" id="pnl_CIDLookUp" runat="server" class="modal fade in" style="display:none;" >
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                          <asp:LinkButton ID="lbtn_close" runat="server" style="float:right" >X</asp:LinkButton>
                            <h4 class="modal-title"><asp:Label ID="lbl_head" Text="CID Look Up Info" runat="server"></asp:Label></h4>
                            
                      </div>
             <div class="modal-body" style="height:400px;">
                 <div class="table-responsive" data-pattern="priority-columns">
                                       <asp:GridView ID="grdvContactLookUp" runat="server" CssClass="table table-small-font table-bordered table-striped"
                                Style="width: 100%" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <ItemTemplate>
                                            <%--<asp:RadioButton id="rbnContactID" GroupName="rdBox" runat="server" Text='<%# Eval("ContactID") %>' ></asp:RadioButton>--%>
                                            <input id="rbnContactID" name="rbnContactID" type="radio" value='<%# Eval("ContactID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_Name" runat="server" NavigateUrl='<%# "/ClientInfo/AssociatedMatters.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="First Name" DataField="FirstName" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="DOB" DataField="DOB" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="DL" DataField="DLNumber" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" ToolTip='<%# Bind("Address") %>' Text='<%#DataBinder.Eval(Container.DataItem,"Address1").ToString()%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                                   </div>
                 </div>
            <div class="modal-footer">
                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                          <asp:HiddenField ID="hdnTicketID" runat="server" />
                            <asp:HiddenField ID="hdnFirstName" runat="server" />
                            <asp:HiddenField ID="hdnLastName" runat="server" />
                            <asp:HiddenField ID="hdnDOB" runat="server" />
                           <asp:Button ID="btnAssociate" runat="server" CssClass="btn btn-primary" Text="Associate"
                                Width="120px" OnClick="btnAssociate_Click" OnClientClick="return CheckOnOff();" />&nbsp;
                            <asp:Button ID="btnNewContact" runat="server" CssClass="btn btn-primary" Text="New Contact"
                                Width="120px" OnClick="btnNewContact_Click" CausesValidation="false"/>&nbsp;
                </div>
            </div>
         </div>    
</div>
  
<div >
    
</div>

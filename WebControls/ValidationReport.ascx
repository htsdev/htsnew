﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidationReport.ascx.cs" Inherits="lntechNew.WebControls.ValidationReport" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50);
    left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<div id="pnl_control" runat="server">
    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
        width: 342px">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 26px">
                            <asp:Label ID="lbl_head" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable">
                    <tr>
                        <td class="clssubhead" style="width: 171px">
                            Report Name :
                        </td>
                        <td style="height: 30px;width:171px">
                            <asp:TextBox CssClass="clsInputadministration" ID="txtrptName" runat="server" TabIndex="1"
                                    Width="124px" MaxLength="8" Height="16px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 171px">
                            Category :
                        </td>
                        <td style="height: 30px;width:171px">
                            <asp:RadioButtonList ID="rbtCat" runat="server" RepeatDirection="Horizontal">
                             <asp:ListItem Text="Alert" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Category" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 171px">
                            Primary User :
                        </td>
                        <td style="height: 30px;width:171px">
                            <asp:DropDownList ID="ddlPrimaryUser" runat="server" CssClass="clsInputCombo"
                            Height="19px" TabIndex="36" Width="140px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 171px">
                            Secondry User :
                        </td>
                        <td style="height: 30px;width:171px">
                            <asp:DropDownList ID="ddlSecondryUser" runat="server" CssClass="clsInputCombo"
                            Height="19px" TabIndex="36" Width="140px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 171px">
                            InActive :
                        </td>
                        <td style="height: 30px;width: 171px">
                            <asp:CheckBox ID="chkIsActive" runat="server" CssClass="clsLabel" 
                                Width="108px" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="width: 171px">
                             
                        </td>
                        <td style="height: 30px;width:171px">
                            <asp:Button ID="btn_Update" runat="server" CssClass="clsbutton" Text="Update" Width="53px" />
                        </td>
                    </tr>
                    
                    
                </table>
            </td>
        </tr>
    </table>
</div>
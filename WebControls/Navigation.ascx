﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigation.ascx.cs"
    Inherits="HTP.WebControls.Navigation" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet">

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    function ValidateControl() {
        var callBackDate = document.getElementById("<%= this.ClientID %>_CallBackDate");
        if (callBackDate.style.display != "none") {
            //Yasir Kamal 7654 04/05/2010 don't check callback date when 'No' is selected as follow-up.
            if (document.getElementById("<%= this.ClientID %>_optFollowUp_0").checked == true) {
	            dayofweek = weekdayName(callBackDate.value, true);
	            if (dayofweek == "Sat" || dayofweek == "Sun") {
	                alert("Please select any working day.");
	                callBackDate.focus();
	                return false;
	            }
	        }
	    }

        var cmbFollowUp = document.getElementById("<%= this.ClientID %>_cmbFollowUp");

        if (document.getElementById("<%= this.ClientID %>_optFollowUp_0").checked == true) {
            if (cmbFollowUp.value == 1 || cmbFollowUp.value == 5 || cmbFollowUp.value == 14) {
                alert("Please Select Follow Up Call");
                cmbFollowUp.focus();
                return false;
            }
        }

        var gComments = document.getElementById("<%= this.ClientID %>_WCC_GeneralComments_txt_Comments");
        var comments = document.getElementById("<%= this.ClientID %>_WCC_GeneralComments");

        gComments.value = trim(gComments.value);

        if (gComments.value.length == 0) {
            alert("Please enter General Comments.");

            if (gComments.offsetHeight == 0) {
                var com = comments.getElementsByTagName("a");

                if (com[0].innerHTML == 'Edit') {
                    com[0].innerHTML = 'Cancel';
                }

                gComments.style.display = 'block';
            }

            if (gComments.offsetHeight > 0) {
                gComments.focus();
            }
            return false;
        }

        // Prompt for future date if follow-up = Yes.
        if (document.getElementById("<%= this.ClientID %>_optFollowUp_0").checked == true) {
            var d1 = callBackDate.value;
            var d2 = document.getElementById("<%= this.ClientID %>_hf_CurrentDate").value;
            if (compareDates(d1, 'MM/dd/yyyy', d2, 'MM/dd/yyyy') == false) {
                alert("Please enter valid future date for callback");
                callBackDate.focus();
                return false;
            }
        }

        return true;
    }
</script>

<style type="text/css">
    .style1
    {
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 140px;
    }
    .style2
    {
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 140px;
    }
</style>
<div id="pnl_control" runat="server" style="background: white !important;width:60%;">
    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
             <section class="box ">

                 <header class="panel_header">
                            <h2 class="title pull-left"> <asp:Label CssClass="title pull-left" ID="lbl_head" runat="server" ></asp:Label></h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       <asp:LinkButton ID="lbtn_close" runat="server"
                                OnClick="lbtn_close_Click">X</asp:LinkButton>
                            </div>
                      </header>
                 <div class="content-body">
            <div class="row">
                <div class="col-md-4">
                    Client Name:
                </div>
                <div class="col-md-8">
                        <asp:Label ID="lblClientName" runat="server"></asp:Label>
                </div>                       
            </div>
            <div class="row">
                <div class="col-md-4">
                    Contact No:
                </div>
                <div class="col-md-8">
                        <asp:Label ID="lblClientContact" runat="server"></asp:Label>
                </div>                       
            </div>

             <div class="row">
                <div class="col-md-4">
                    Language:
                </div>
                <div class="col-md-8">
                        <asp:Label ID="lblClientLanguage" runat="server"></asp:Label>
                </div>                       
            </div>
            <div class="row">
                <div class="col-md-4">
                    Quote Amount:
                </div>
                <div class="col-md-8">
                        <asp:Label ID="lblQuoteAmount" runat="server"></asp:Label>
                </div>                       
            </div>
            <div class="clear-fix"></div>
            <div class="row">
                <div class="col-md-12">
                   <div id="divGrid" runat="server" style="overflow: auto;">
                            <asp:GridView ID="gvClientInfo" runat="server"
                                BorderColor="Gray" BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Cause Number">
                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" Width="9%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CauseNo" runat="server"  Text='<%# Eval("CauseNumber") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ticket Number">
                                        <HeaderStyle Width="9%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Labe66" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Matter Description">
                                        <HeaderStyle Width="13%" HorizontalAlign="Center" >
                                        </HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Crt Date">
                                        <HeaderStyle Width="7%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_courtdate" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDate","{0:d}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Crt">
                                        <HeaderStyle Width="3%" HorizontalAlign="Center" ></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Crt" runat="server"  Text='<%# DataBinder.Eval(Container,"DataItem.CourtNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Status" runat="server"  Text='<%# DataBinder.Eval(Container,"DataItem.AutoCourtDesc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Crt Location">
                                        <HeaderStyle Width="8%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Loc" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                            <asp:HiddenField ID="hfTicketId" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                </div>
                                    
            </div>

            <div class="row">
                <div class="col-md-4">
                    Follow-Up Call:
                </div>
                <div class="col-md-8">
                        <asp:DropDownList ID="cmbFollowUp" runat="server">
                        </asp:DropDownList>
                </div>                       
            </div>

             <div class="row">
                <div class="col-md-4">
                    Follow-Up:
                </div>
                <div class="col-md-8">
                       <asp:RadioButtonList ID="optFollowUp" runat="server"  RepeatDirection="Horizontal"
                            Font-Names="Tahoma" Font-Size="8pt">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                        </asp:RadioButtonList>
                </div>                       
            </div>
            <div class="row">
                <div class="col-md-4">
                    General Comments:
                </div>
                <div class="col-md-8">
                       <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" />  
                </div>                       
            </div>
            <div class="row">
                <div class="col-md-4">
                   Call Back Date:
                </div>
                <div class="col-md-8">
                        &nbsp;<ew:CalendarPopup ID="CallBackDate" runat="server" AllowArbitraryText="False"
                            CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="True"
                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                            PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" ToolTip="Call Back Date"
                            UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                        </ew:CalendarPopup>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary" Text="Update"
                            OnClick="btnUpdate_Click" OnClientClick="return ValidateControl();"></asp:Button>
                        <asp:HiddenField ID="hf_CurrentDate" runat="server" />
                        <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                </div>                       
            </div>
                     <div class="well-sm"></div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <asp:ImageButton ID="imgFirst" runat="server" ImageUrl="../Images/MoveFirst.gif"
                            OnClick="imgFirst_Click" ToolTip="First Record" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="imgPrev" runat="server" ImageUrl="../Images/MovePrevious.gif"
                            OnClick="imgPrev_Click" ToolTip="Previous Record" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="imgNext" runat="server" ImageUrl="../Images/MoveNext.gif" OnClick="imgNext_Click"
                            ToolTip="Next Record" />
                        &nbsp;&nbsp;
                        <asp:ImageButton ID="imgLast" runat="server" ImageUrl="../Images/MoveLast.gif" OnClick="imgLast_Click"
                            ToolTip="Last Record" />
                </div>
                                     
            </div>
             <div class="row">
                <div class="col-md-12">
                     <asp:Label ID="lblMessage" Visible="false" runat="server" Text=""  ForeColor="Red"></asp:Label>
                </div>
                                     
            </div>

                 </div>
                                     </section>
            
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="imgFirst" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="imgPrev" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="imgNext" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="imgLast" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
</div>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateFollowUpInfoEmailAddress.ascx.cs"
    Inherits="HTP.WebControls.UpdateFollowUpInfoEmailAddress" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>


<link href="../Styles.css" type="text/css" rel="stylesheet" />

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    
    //Ozair 8101 09/29/2010 code refactored where required

    function ClosePopUp(backdiv, frontdiv) {

        document.getElementById(backdiv).style.display = "none";
        document.getElementById(frontdiv).style.display = "none";
        return false;
    }

    function closeModalPopup(popupid) {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }

    function verifyEmail() {

        var status = false;

        var email = document.getElementById('<%= this.ClientID %>_txt_Email').value;
        var AtPos = email.indexOf("@");
        var StopPos = email.lastIndexOf(".");

        if (AtPos == -1 || StopPos == -1) {
            alert("Please enter a valid email address.");
            status = false;
        }
        else {
            status = true;
        }
        return status;
    }

    function ValidateInput() {
        //Muhammad Nasir 10071 12/10/2012 When user edit the email address then system must ask the user to put comments & select call back status then allowed to click update
        var emailValueAtStartup = document.getElementById('<%=this.hf_EmailAtStartup.ClientID %>').value;
        var emailValueAtSubmit = document.getElementById('<%=this.txt_Email.ClientID %>').value;

        //Muhammad Nasir 10071 12/20/2012 check followup date to enter comments
        var followUpDateAtStartup = document.getElementById('<%=this.ClientID %>_cal_followupdate').value;
        var followUpDateAtSubmit = document.getElementById('<%=this.hf_FollowUpDateAtStartup.ClientID %>').value;
        var statusAtStartup = document.getElementById('<%= hf_StatusAtStartup.ClientID %>').value;;
        var statusAtSubmit = document.getElementById('<%= ddl_callback.ClientID %>').value;

        var oldval = document.getElementById('<%=this.hf_FollowUpDate.ClientID %>').value;
        var newval = document.getElementById("<%= this.ClientID %>_cal_followupdate").value;

        emailValueAtStartup = emailValueAtStartup.toLowerCase();
        emailValueAtSubmit = emailValueAtSubmit.toLowerCase();

        if (emailValueAtStartup != emailValueAtSubmit) {
            var checkmail = verifyEmail();
            if (checkmail == false) {
                return false;
            }

            //Muhammad Nasir 10071 11/26/2012 validate call back status before update
            if (document.getElementById('<%= ddl_callback.ClientID %>').value == "-1") {
                alert("Please choose Call Back Status.");
                document.getElementById('<%= ddl_callback.ClientID %>').focus();
            return false;
        }

            //Muhammad Nasir 10071 12/10/2012
            //if(oldval!=newval)
            //{   //Sabir Khan 5977 07/24/2009 To prevent if there is only general comments...
        var genStr = document.getElementById("<%= tb_GeneralComments.ClientID %>").value
            if (trim(genStr) == "") {
                alert("Please enter General Comments");
                return false;
            }
            //} 
        }

        //Muhammad Nasir 10071 12/20/2012 Check followup date
        if (followUpDateAtStartup != followUpDateAtSubmit) {
            var genStr = document.getElementById("<%= tb_GeneralComments.ClientID %>").value
        if (trim(genStr) == "") {
            alert("Please enter General Comments");
            return false;
        }
    }

        //Muhammad Nasir 10071 12/20/2012 Check Status
    if (statusAtStartup != statusAtSubmit) {
        var genStr = document.getElementById("<%= tb_GeneralComments.ClientID %>").value
        if (trim(genStr) == "") {
            alert("Please enter General Comments");
            return false;
        }
    }

    var date = new Date();
    if ((newval < date)) {
        // modify zahoor 4481 08/08/2008
        alert("Please specify valid follow up date");
        // Add Zahoor 4481 08/9/208
        // to avoid closing popup.
        return false;
    }


    return CheckDate(document.getElementById("<%= this.ClientID %>_cal_followupdate").value, document.getElementById("<%= this.ClientID %>_cal_followupdate"))
    ClosePopUp('<%= this.pnl_control.ClientID %>', '<%=this.Disable.ClientID %>');
    }
    //Sabir Khan 5977 07/24/2009 Used to trim the general comments...	
    function trim(stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g, "");
    }

    function ShowPopUp_1(generalcomments, ticid, ticketid, followupdate, lastname, firstname, ticketnumber, causeno) {
        var backdiv = '<%=this.Disable.ClientID %>'
        var frontdiv = '<%= this.pnl_control.ClientID %>'


        document.getElementById('<%= this.tb_GeneralComments.ClientID %>').value = "";

        setpopuplocation(backdiv, frontdiv);
        document.getElementById('<%= this.ClientID %>_chk_UpdateAll').checked = false;
    document.getElementById('<%= this.ClientID %>_lblComments').innerHTML = generalcomments;
        document.getElementById('<%= this.ClientID %>_cal_followupdate').value = followupdate;
        document.getElementById('<%=this.hf_ticketid.ClientID %>').value = ticketid;
        document.getElementById('<%=this.hf_ticid.ClientID %>').value = ticid;
        //Ozair 5723 04/03/2009 Full Name in First Last format.
        document.getElementById("<%= this.ClientID %>_lbl_FullName").innerHTML = firstname + " " + lastname;
        document.getElementById("<%= this.ClientID %>_lblTicketNumber").innerHTML = ticketnumber;
        document.getElementById("<%= this.ClientID %>_lblCauseNo").innerHTML = causeno;
        document.getElementById('<%=this.hf_FollowUpDate.ClientID %>').value = followupdate;
        document.getElementById('<%=this.lblCurrentFollup.ClientID %>').innerHTML = followupdate;
        return false;
    }

    // Set Popup Control Location
    function setpopuplocation(backdiv, frontdiv) {

        var top = 400;
        var left = 400;
        var height = document.body.offsetHeight;
        var width = document.body.offsetWidth

        // Setting Panel Location
        if (width > 1100 || width <= 1280) left = 575;
        if (width < 1100) left = 500;

        // Setting popup display
        document.getElementById(frontdiv).style.top = top + "px";
        document.getElementById(frontdiv).style.left = left + "px";

        if (document.body.offsetHeight > document.body.scrollHeight)
            document.getElementById(backdiv).style.height = document.body.offsetHeight;
        else
            document.getElementById(backdiv).style.height = document.body.scrollHeight;

        document.getElementById(backdiv).style.width = document.body.offsetWidth;
        document.getElementById(backdiv).style.display = ''
        document.getElementById(frontdiv).style.display = ''

        document.body.scrollTop = 0;
    }
    // Noufil 3589 05/29/2008 FUnction for date range control
    function CheckDate(seldate, tbID) {
        today = new Date();
        var diff = Math.ceil(DateDiff(seldate, today));
        court = document.getElementById('<%=this.hf_court.ClientID %>').value;
    courtname = document.getElementById('<%=this.hf_courtname.ClientID %>').value;

    //Waqas 5653 03/24/2009 Not For No LOR
    //Waqas 5697 03/26/2009 For HMC-FTA Follow Up
    //Nasir 5938 05/26/2009 For NOS Follow Up
    //Sabir 5977 07/02/2009 For HMCSettingDiscrepancy Follow Up
    //Nasir 7234 01/08/2010 for HMCSameDayArr 

    //Saeed 8101 09/24/2010 HCJP Auto Update Alert condition added.
    //Muhammad Nadir Siddiqui 9134 05/02/2011 Added 'NO LOR Confirmation'
    if (court != "NO LOR" && court != "NO LOR Confirmation" && court != "HMC-FTA" && court != "Non Hmc" && court != "Split" && court != "ALRHEARING" && court != "NOS" && court != "HMCSettingDiscrepancy" && court != "Bond Alert" && court != "HMCSameDayArr" && court != "HCJPAutoUpdateAlert" && court != "BadAddresses") {  //alert('test');
        //ozair 4442 07/22/2008 ALR Courts(houston, forte bend, conroe)
        //Fahad 5753 04/10/2009 exclude Cases of Split info
        if (((court == "3047" || court == "3048" || court == "3070") && diff > 7) || ((court == "3047" || court == "3048" || court == "3070") && diff < 0)) {
            alert("Please enter date within one week from today for court " + courtname)
            var tst1 = formatDate(dateAdd("d", 7, today), "MM/dd/yyyy");
            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = tst1;
            return false;
        }
        //Muhammad Ali 7747 06/30/2010 for Missing Cause Follow update.
        if (((court == "MissingCauseNumber") && diff > 7) || ((court == "MissingCauseNumber") && diff < 0)) {
            alert("Please enter date within one week from today for Missing Cause Number Follow Up Date");
            var tst1 = formatDate(dateAdd("d", 7, today), "MM/dd/yyyy");
            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = tst1;
            return false;
        }
            // Abid Ali 4912 11/18/2008 -- add one more criteria (court != "Traffic Waiting")
            // Abid Ali 5018 12/26/2008 -- add one more criteria (court != "Family Waiting")
            // Ozair 5098 01/13/2009 -- add one more criteria (court != "ContractFollowUp") for no digned contract report
            // Noufil 5691 04/06/2009 Clause added for Past court date report.
            // Saeed 7844 06/26/2010 add two more criterias (court != "BadEmailAddress" && (court != "AddressValidation"))
            //Saeed 7791 08/13/2010 add one criteria of court!= "Family Waiting" 
        else if (((court != "3047" && court != "3048" && court != "3070" && court != "" && court != "Waiting" && court != "Traffic Waiting" && court != "Family Waiting" && court != "BadEmailAddress" && court != "AddressValidation" && court != "ContractFollowUp" && court != "PastCourtDateHMC") && diff > 14) || ((court != "3047" && court != "3048" && court != "3070" && court != "3041" && court != "" & court != "Waiting" && court != "Traffic Waiting" && court != "Family Waiting" && court != "ContractFollowUp" && court != "PastCourtDateHMC" && court != "BadEmailAddress" && court != "AddressValidation") && diff < 0)) {
            alert("Please enter date within two week from today for court " + courtname)
            //alert(seldate);
            var tst = formatDate(dateAdd("d", 14, today), "MM/dd/yyyy");
            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = tst;
            return false;
        }

        //Yasir Kamal 5734 03/31/2009 followUpdate logic set for 2 business days
        // added zahoor 4481 8/6/2008
        // court = ""  No court is defined in bond report case.
        //Sabir Khan 5727 03/31/2009 Follow update logic has been changed into six from 5 business days...
        //	        else if (((court == "" ) && diff >2 )|| ((court == "" ) && diff <0))
        //	        {
        //	            alert ("Please select follow up date with in 6 business days.")
        //	            //alert(seldate);
        //	            var tst1 = formatDate(dateAdd("d", 2, today),"MM/dd/yyyy"); 
        //	            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value=tst1;	        
        //	            return false;
        //	      }
    }

    var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
    var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth() + 1) + "/" + Date.parseInvariant(seldate).getDate() + "/" + Date.parseInvariant(seldate).getFullYear(), "MM/dd/yyyy");
    newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
    // Adil 7752 05/29/2010 Non-business days check moved here from bottom.
    //Fahad 5098 05/01/2009 Add saturday check 
    //Ozair 5466 01/28/2009 "&&" changed to "||"
    if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {
        alert("Please select any business day");
        return false;
    }
    // end 4481
    // Noufil 4980 10/30/2008 follow Up date condition set for Bond waiting Report	  

    if ((court == "Waiting")) {
        var datediff = Math.ceil(DateDiff(newseldate, today));

        if (datediff >= 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 0; i <= datediff; i++) {

                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }

                today = dateAdd("d", 1, today);

            }


            //Sabir Khan 5695 03/25/2009 Business days changed from 5 to 6 for bond waiting report...
            if ((countbusinessdays > 3) || (countbusinessdays == 0)) {
                alert("Please select follow up date within next 2 business days");
                return false;
            }
        }
        else {
            alert("Please select future date");
            return false;
        }
    }
    if ((court == "")) {
        var datediff = Math.ceil(DateDiff(newseldate, today));

        if (datediff >= 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 0; i <= datediff; i++) {

                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }

                today = dateAdd("d", 1, today);

            }
            var courtname = document.getElementById('<%=this.hf_courtname.ClientID %>').value;
            // Afaq 7752 05/04/2010 add check for dispose alert.               
            if (courtname == "DisposeAlert") {
                //Fahad 7752 06/23/2010 Resolved issues of Allowed days
                if ((countbusinessdays > 6) || (countbusinessdays == 0)) {
                    alert("Please select follow up date within next 5 business days");
                    return false;
                }
            }

        }
        else {
            alert("Please select future date");
            return false;
        }
    }

    //SAEED 7844 06/21/2010 Allow to Add next 5 business days in Follow Up date	   
    if (court == "BadEmailAddress" || court == "AddressValidation") {

        var datediff = Math.ceil(DateDiff(newseldate, today));
        if (datediff >= 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 0; i <= datediff; i++) {
                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }
                today = dateAdd("d", 1, today);
            }
            if ((countbusinessdays > 6) || (countbusinessdays == 0)) {
                alert("Please select Follow Up Date within next 5 business days from today's Date");
                return false;
            }
        }
        else {
            alert("Please select future date");
            return false;
        }
    }
    // 7844 end



    //Fahad 9134 05/2/2011 Allow to Add in Follow Up date in next Show Setting Control business days for NO LOR Confirmation.
    if ((court == "NO LOR Confirmation")) {
        var datediff = Math.ceil(DateDiff(newseldate, today));
        var _BusinessDay = document.getElementById('<%=BusinessDay.ClientID%>').value;

        if (datediff > 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 0; i <= datediff; i++) {

                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }

                today = dateAdd("d", 1, today);

            }
            if ((countbusinessdays > _BusinessDay) || (countbusinessdays == 0)) {

                alert("Please select Follow Up Date in next " + _BusinessDay + " business day(s) from Today's date.");
                return false;
            }

        }
        else {
            alert("Please select future date");
            return false;
        }
    }

    //Fahad 5722 04/07/2009 Allow to Add 2 business day in Follow Up
    if ((court == "Non Hmc")) {

        var datediff = Math.ceil(DateDiff(newseldate, today));

        if (datediff > 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 0; i <= datediff; i++) {

                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }

                today = dateAdd("d", 1, today);

            }

            if ((countbusinessdays > 3) || (countbusinessdays == 0)) {
                alert("Please select follow up date within next 2 business days");
                return false;
            }
        }
        else {
            alert("Please select future date");
            return false;
        }
    }

    //Sabir Khan 6706 10/07/2009 Set follow up date for Bond Alert Report...
    if ((court == "Bond Alert")) {

        var datediff = Math.ceil(DateDiff(newseldate, today));

        if (datediff >= 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 0; i <= datediff; i++) {

                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }

                today = dateAdd("d", 1, today);

            }

            if ((countbusinessdays > 31) || (countbusinessdays == 0)) {
                alert("Please select follow up date within next 30 business days");
                return false;
            }
        }
        else {
            alert("Please select future date");
            return false;
        }
    }



    //Fahad 5753 04/13/2009 Allow to Add next 5 business days in Follow Up date
    if (court == "Split") {

        var datediff = Math.ceil(DateDiff(newseldate, today));
        if (datediff >= 0) {
            var i = 0, countbusinessdays = 0, countHolidays = 0;
            for (i = 1; i <= datediff; i++) {
                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                    countbusinessdays++;
                }
                today = dateAdd("d", 1, today);
            }
            if ((countbusinessdays > 5) || (countbusinessdays == 0)) {
                alert("Please select Follow Up Date within next 5 business days from today's Date");
                return false;
            }

        }
        else {
            alert("Please select future date");
            return false;
        }
    }
    //Saeed 8101 09/24/2010 Allow to add next (n) business days in Follow Up Date
    if (court == "HCJPAutoUpdateAlert") {

        var datediff = Math.ceil(DateDiff(newseldate, today));
        var businessDaysToStop = document.getElementById('<%=this.BusinessDay.ClientID %>').value;
            if (datediff >= 0) {
                var i = 0, countbusinessdays = 0, countHolidays = 0;
                for (i = 1; i <= datediff; i++) {
                    if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                        countbusinessdays++;
                    }
                    today = dateAdd("d", 1, today);
                }

                //Saeed 8101 11/02/2010 if businessDaysToStop=0 then allow to set any future date for follow up date.
                if ((businessDaysToStop != '0') && ((countbusinessdays > businessDaysToStop) || (countbusinessdays == 0))) {
                    alert("Please select Follow Up Date within next " + businessDaysToStop + " business days from today's Date");
                    return false;
                }

            }
            else {
                alert("Please select future date");
                return false;
            }
        }

    // 5734 end

    // Fahad 5098 10/30/2008 follow Up date condition set for Bond waiting Report	  
        if (court == "ContractFollowUp") {
            var datediff = Math.ceil(DateDiff(newseldate, today));
            if (datediff >= 0) {
                var i = 0, countbusinessdays = 0, countHolidays = 0;
                for (i = 1; i <= datediff; i++) {
                    if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                        countbusinessdays++;
                    }
                    today = dateAdd("d", 1, today);
                }
                if ((countbusinessdays > 5) || (countbusinessdays == 0)) {
                    alert("Please select follow up date with in 5 business days");
                    return false;
                }
            }
            else {
                alert("Please select future date");
                return false;
            }
        }
            // Abid Ali 4912 11/13/2008 - Traffic Waiting follow up date
            //Saeed 7791 08/13/2010 remove [court == "Family Waiting"] condition cause family waiting has max two weeks follow up date, while traffic wiating has 4 weeks.
        else if (court == "Traffic Waiting") {
            var datediff = Math.ceil(DateDiff(newseldate, today));

            // Zeeshan 10286 08/16/2012 Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses.	      
            var FollowUpDate = document.getElementById('<%=this.hf_FollowUpDate.ClientID %>').value;
          var FollowUpDays = document.getElementById('<%=this.hf_FollowUpDays.ClientID %>').value;
          var FirstFollowUpDays = document.getElementById('<%=this.hf_FirstFollowUpDays.ClientID %>').value;
          FollowUpDate = FollowUpDate.toUpperCase() == 'N/A' ? '' : FollowUpDate;

          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 0; i <= datediff; i++) {
                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }
                  today = dateAdd("d", 1, today);
              }

              // tahir 5350 12/20/2008 allowed 4 weeks to select...
              //	          if ((countbusinessdays > 20) || (countbusinessdays == 0)) 
              //	          {
              //	              alert("Please enter Follow Up Date with in four weeks from today's date.");
              //	              return false;
              //	          }

              // Zeeshan 10286 08/16/2012 Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses.
              if (FollowUpDate == '' && FirstFollowUpDays > 0) {
                  if (countbusinessdays > FirstFollowUpDays) {
                      alert("Please select date in next " + FirstFollowUpDays + " business days from today's date.");
                      document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = document.getElementById("UpdateFollowUpInfo2_cal_followupdate").defaultValue;
                      return false;
                  }
              } else if (FollowUpDate != '' && FollowUpDays > 0) {
                  if (countbusinessdays > FollowUpDays) {
                      alert("Please select date in next " + FollowUpDays + " business days from today's date.");
                      document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = document.getElementById("UpdateFollowUpInfo2_cal_followupdate").defaultValue;
                      return false;
                  }
              }
          }
          else {
              alert("Please select future date");
              document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = document.getElementById("UpdateFollowUpInfo2_cal_followupdate").defaultValue;
              return false;
          }
      }

          // Abid Ali 5018 11/24/2008 - Family Waiting follow up date
      else if (court == "Family Waiting") {
          var datediff = Math.ceil(DateDiff(newseldate, today));
          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 0; i <= datediff; i++) {
                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }
                  today = dateAdd("d", 1, today);
              }
              // tahir 5350 12/20/2008 allowed 4 weeks to select...
              if ((countbusinessdays > 10) || (countbusinessdays == 0)) {
                  //Saeed 7791 08/13/2010 display two weeks alert message.   
                  alert("Please enter Follow Up Date with in two weeks from today's date.");
                  return false;
              }
          }
          else {
              alert("Please select future date");
              return false;
          }
      }

          //Waqas 5653 03/24/2009 For No LOR
          //Waqas 5697 03/26/2009 For HMC-FTA Follow Up
          // Noufil 5691 04/06/2009 PastCourtDateHMC added
          //Nasir 5938 05/26/2009 For NOS Follow Up
          //Sabir Khan 5977 07/02/2009 For HMCSettingDiscrepancy Follow Up
          //Nasir 7234 01/08/2010 for HMCSameDayArr 

      else if (court == "NO LOR" || court == "HMC-FTA" || court == "PastCourtDateHMC" || court == "NOS" || court == "HMCSettingDiscrepancy" || court == "HMCSameDayArr" || court == "BadAddresses") {

          var BusDay = document.getElementById('<%=this.BusinessDay.ClientID %>').value;
          var datediff = Math.ceil(DateDiff(newseldate, today));
          //Nasir 5974 06/05/2009 fixed issue
          if ((weekday[today.getDay()] == "Sunday") || (weekday[today.getDay()] == "Saturday")) {
              datediff = datediff + 1;
          }
          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 1; i <= datediff; i++) {
                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }
                  today = dateAdd("d", 1, today);
              }

              if ((countbusinessdays > BusDay) || (countbusinessdays == 0)) {

                  alert("Please select Follow Up Date in next " + BusDay + " business day(s) from Today's date.");
                  return false;
              }

          }
          else {
              alert("Please select future date");
              return false;
          }
      }

          // Noufil 5819 05/13/2009 ALRHEARING ADDED
      else if (court == "ALRHEARING") {
          var BusDay = document.getElementById('<%=this.BusinessDay.ClientID %>').value;
              var datediff = Math.ceil(DateDiff(newseldate, today));
              if (datediff >= 0) {
                  var i = 0, countbusinessdays = 0, countHolidays = 0;
                  for (i = 1; i <= datediff; i++) {
                      if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                          countbusinessdays++;
                      }
                      today = dateAdd("d", 1, today);
                  }

                  if (BusDay == 0 && datediff > 0) {
                      alert("Please enter follow-up Date of today's date only.");
                      return false;
                  }
                  else if (parseInt(BusDay, 0) > 0 && ((countbusinessdays > BusDay) || (countbusinessdays == 0))) {
                      alert("Please enter Follow-up Date in next " + BusDay + " business days from Today's date.");
                      return false;
                  }
              }
              else {
                  alert("Please select future Follow-up date.");
                  return false;
              }
          }








}

function DateDiff(date1, date2) {
    var one_day = 1000 * 60 * 60 * 24;
    var objDate1 = new Date(date1);
    var objDate2 = new Date(date2);
    return (objDate1.getTime() - objDate2.getTime()) / one_day;
}
// Haris Ahmed 9021 01/20/2012 Resolve issue to set followup date on popup calendar
function CheckFollowUpDate() {
    //debugger;
    CalendarPopup_Up_SelectDate('UpdateFollowUpInfo2_cal_followupdate', '', 'UpdateFollowUpInfo2_cal_followupdate_div', 'UpdateFollowUpInfo2_cal_followupdate_monthYear', document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value, 1, true, true, 'UpdateFollowUpInfo2_cal_followupdate_Up_PostBack', '', 'UpdateFollowUpInfo2_cal_followupdate_outer_VisibleDate')
}


</script>
<style type="text/css">
    .Labelfrmmain, table.table td span.form-label {
        font-size: 14px !important;
    }
    #gv_Records tbody tr th {
        color:#fff !important;
        font-weight : bold !important;
    }
    .Labelfrmmain, table.table td span.form-label-blue {
        font-size: 14px !important;
        color: #11a2cf !important;
    }

    .clssubhead {
        /* font-weight: bold; */
        font-size: 14px !important;
        color: #3366cc !important;
        font-weight: normal !important;
        text-decoration: none !important;
        ;
    }

    tr.border_bottom td {
        border-bottom: 1pt solid black;
    }

    td {
        border: 0;
    }

    .customlabel label {
        font-family: 'Open Sans', Arial, Helvetica !important;
        font-weight: normal !important;
        color: #555555 !important;
        font-size: 14px !important;
    }

    #pageinfo table {
        width: auto !important;
        margin-top: 8px !important;
    }

    #commentpar a:link:hover {
        background-color: #11a2cf !important;
        color: white !important;
        width: auto !important;
        height: auto !important;
        font-weight: normal !important;
        font-size: 14px !important;
    }

    #pagecountdiv table {
        width: 460px !important;
    }

    .clsLeftPaddingTable tbody tr td {
        font-weight: normal !important;
    }
    /*#11a2cf*/
</style>
<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50); left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<style type="text/css">
    .style1 {
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 170px;
    }
</style>
<div id="pnl_control" runat="server">


    <section id="main-content-popup" class="addnewleadpopup">
        <section class="wrapper main-wrapper row" style="">
            <section class="box ">
                <%--  <header class="panel_header">
                    <h2 class="title pull-left">teset</h2>
                    <div class="actions panel_actions remove-absolute pull-right">

                        <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>--%>
                <div class="content-body">
                    <div class="table-responsive">
                        <table enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 342px">
                            <tr>
                                <td background="../Images/subhead_bg.gif" valign="bottom">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td class="clssubhead" style="height: 26px">
                                                <asp:Label ID="lbl_head" runat="server"></asp:Label>
                                            </td>
                                            <td align="right">&nbsp;
                                                <asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" border="0">
                                        <tr>
                                            <td class="style1">Client Name :
                                            </td>
                                            <td style="height: 20px;">
                                                <asp:Label ID="lbl_FullName" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="tr_TicketNumber" runat="server">
                                            <td class="style1">Ticket Number :
                                            </td>
                                            <td style="height: 20px;">
                                                <asp:Label ID="lblTicketNumber" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="tr_CauseNumber" runat="server">
                                            <td class="style1">Cause Number :
                                            </td>
                                            <td style="height: 20px;">
                                                <asp:Label ID="lblCauseNo" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">Current Follow up Date :
                                            </td>
                                            <td style="height: 30px">
                                                <asp:Label ID="lblCurrentFollup" runat="server" CssClass="clsLabel"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">Next Follow up Date :
                                            </td>
                                            <td style="height: 20px;">
                                                <picker:datepicker id="cal_followupdatenew" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                                               <%-- <ew:CalendarPopup Visible="false" ID="cal_followupdate" runat="server" AllowArbitraryText="False"
                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                                    PadSingleDigits="True" ShowClearDate="False" ShowGoToToday="True" Text="" ToolTip="Date"
                                                    UpperBoundDate="9999-12-29" Width="101px" OnClientChange="callme()" JavascriptOnChangeFunction="CheckDate" onClick="CheckFollowUpDate()">
                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                        ForeColor="Black" />
                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                </ew:CalendarPopup>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">Email Address:</td>
                                            <td style="height: 20px;">
                                                <asp:TextBox ID="txt_Email" runat="server" CssClass="form-control" 
                                                    CausesValidation="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">Contact Numbers :</td>
                                            <td style="height: 20px;">
                                                <asp:Label ID="lblContactNumber1"  style="background-color:#fff !important;" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">&nbsp;</td>
                                            <td style="height: 20px;">
                                                <asp:Label ID="lblContactNumber2" style="background-color:#fff !important;" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">&nbsp;</td>
                                            <td style="height: 20px;">
                                                <asp:Label ID="lblContactNumber3" style="background-color:#fff !important;" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">Call Back Status :
                                            </td>
                                            <td style="height: 20px;">
                                                <asp:DropDownList ID="ddl_callback" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style1">Comments :
                                            </td>
                                            <td style="height: 20px;">&nbsp;
                                            </td>
                                        </tr>
                                        <tr id="tr_comment" runat="server">
                                            <td valign="top" colspan="2">
                                                <table enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 323px;">
                                                    <tr>
                                                        <td style="height: 15px; width: 314px;">
                                                            <div id="divcomment" runat="server" style="overflow: auto; height: 50px; width: 375px;">
                                                                <asp:Label ID="lblComments" runat="server" CssClass="clsLabel"></asp:Label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 314px">
                                                            <asp:TextBox ID="tb_GeneralComments" runat="server" Height="70px" TextMode="MultiLine"
                                                                Width="375px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="hf_ticketid" runat="server" Value="0" />
                                                <asp:HiddenField ID="hf_FollowUpDate" runat="server" />
                                                <asp:HiddenField ID="hf_ticid" runat="server" />
                                                <asp:HiddenField ID="hf_court" runat="server" />
                                                <asp:HiddenField ID="hf_courtname" runat="server" />
                                                <asp:HiddenField ID="hf_todaydate" runat="server" />
                                                <asp:HiddenField ID="Hf_field" runat="server" Value="0" />
                                                <asp:HiddenField ID="hf_FollowUpType" runat="server" />
                                                <asp:HiddenField ID="BusinessDay" runat="server" Value="0" />
                                                <asp:HiddenField ID="hf_ContactID" runat="server" Value="0" />
                                                <asp:HiddenField ID="hf_FollowUpDays" runat="server" Value="0" />
                                                <asp:HiddenField ID="hf_FirstFollowUpDays" runat="server" Value="0" />
                                                <asp:HiddenField ID="hf_VerifyEmailCallBackStatus" runat="server" Value=" " />
                                                <asp:HiddenField ID="hf_isContacted" runat="server" Value="" />
                                                <asp:HiddenField ID="hf_EmailAtStartup" runat="server" Value="" />
                                                <asp:HiddenField ID="hf_FollowUpDateAtStartup" runat="server" Value="" />
                                                <asp:HiddenField ID="hf_StatusAtStartup" runat="server" Value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" style="height: 20px">
                                                <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary btn-sm" Text="Update"
                                                    OnClientClick="return ValidateInput();" OnClick="btnsave_Click" />&nbsp; 
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-default btn-sm" Text="Cancel" />&nbsp;

                            &nbsp;<asp:CheckBox ID="chk_UpdateAll" runat="server" CssClass="clsLabel" Text="Update Selected"
                                Width="108px" Visible="true" />
                                                <asp:CheckBox ID="chk_IsHMCFTARemoved" runat="server" CssClass="clsLabel" Text="Remove Client"
                                                    Width="108px" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </section>



        </section>
    </section>




    <%--<table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 342px">
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 26px">
                            <asp:Label ID="lbl_head" runat="server"></asp:Label>
                        </td>
                        <td align="right">&nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" border="0">
                    <tr>
                        <td class="style1">Client Name :
                        </td>
                        <td style="height: 20px;">
                            <asp:Label ID="lbl_FullName" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_TicketNumber" runat="server">
                        <td class="style1">Ticket Number :
                        </td>
                        <td style="height: 20px;">
                            <asp:Label ID="lblTicketNumber" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_CauseNumber" runat="server">
                        <td class="style1">Cause Number :
                        </td>
                        <td style="height: 20px;">
                            <asp:Label ID="lblCauseNo" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Current Follow up Date :
                        </td>
                        <td style="height: 30px">
                            <asp:Label ID="lblCurrentFollup" runat="server" CssClass="clsLabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Next Follow up Date :
                        </td>
                        <td style="height: 20px;">
                            <ew:CalendarPopup Visible="false" ID="cal_followupdate" runat="server" AllowArbitraryText="False"
                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                PadSingleDigits="True" ShowClearDate="False" ShowGoToToday="True" Text="" ToolTip="Date"
                                UpperBoundDate="9999-12-29" Width="101px" OnClientChange="callme()" JavascriptOnChangeFunction="CheckDate" onClick="CheckFollowUpDate()">
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TextboxLabelStyle CssClass="clstextarea" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                            </ew:CalendarPopup>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Email Address:</td>
                        <td style="height: 20px;">
                            <asp:TextBox ID="txt_Email" runat="server" CssClass="clsInputadministration" Width="200px"
                                CausesValidation="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Contact Numbers :</td>
                        <td style="height: 20px;">
                            <asp:Label ID="lblContactNumber1" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td style="height: 20px;">
                            <asp:Label ID="lblContactNumber2" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">&nbsp;</td>
                        <td style="height: 20px;">
                            <asp:Label ID="lblContactNumber3" CssClass="clsLeftPaddingTable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Call Back Status :
                        </td>
                        <td style="height: 20px;">
                            <asp:DropDownList ID="ddl_callback" runat="server" CssClass="clsInputCombo">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Comments :
                        </td>
                        <td style="height: 20px;">&nbsp;
                        </td>
                    </tr>
                    <tr id="tr_comment" runat="server">
                        <td valign="top" colspan="2">
                            <table enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 323px;">
                                <tr>
                                    <td style="height: 15px; width: 314px;">
                                        <div id="divcomment" runat="server" style="overflow: auto; height: 50px; width: 375px;">
                                            <asp:Label ID="lblComments" runat="server" CssClass="clsLabel"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 314px">
                                        <asp:TextBox ID="tb_GeneralComments" runat="server" Height="70px" TextMode="MultiLine"
                                            Width="375px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hf_ticketid" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_FollowUpDate" runat="server" />
                            <asp:HiddenField ID="hf_ticid" runat="server" />
                            <asp:HiddenField ID="hf_court" runat="server" />
                            <asp:HiddenField ID="hf_courtname" runat="server" />
                            <asp:HiddenField ID="hf_todaydate" runat="server" />
                            <asp:HiddenField ID="Hf_field" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_FollowUpType" runat="server" />
                            <asp:HiddenField ID="BusinessDay" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_ContactID" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_FollowUpDays" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_FirstFollowUpDays" runat="server" Value="0" />
                            <asp:HiddenField ID="hf_VerifyEmailCallBackStatus" runat="server" Value=" " />
                            <asp:HiddenField ID="hf_isContacted" runat="server" Value="" />
                            <asp:HiddenField ID="hf_EmailAtStartup" runat="server" Value="" />
                            <asp:HiddenField ID="hf_FollowUpDateAtStartup" runat="server" Value="" />
                            <asp:HiddenField ID="hf_StatusAtStartup" runat="server" Value="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="height: 20px">
                            <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary btn-sm" Text="Update"
                                OnClientClick="return ValidateInput();" OnClick="btnsave_Click" />&nbsp; 
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-default btn-sm" Text="Cancel" />&nbsp;

                            &nbsp;<asp:CheckBox ID="chk_UpdateAll" runat="server" CssClass="clsLabel" Text="Update Selected"
                                Width="108px" Visible="true" />
                            <asp:CheckBox ID="chk_IsHMCFTARemoved" runat="server" CssClass="clsLabel" Text="Remove Client"
                                Width="108px" Visible="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>--%>
</div>

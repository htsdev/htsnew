<%@ Control Language="c#" AutoEventWireup="false" Inherits="lntechNew.WebControls.ActiveMenu"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" CodeBehind="ActiveMenu.ascx.cs" %>
<%--<link href="../Styles.css" type="text/css" rel="stylesheet" />--%>
<%--<script src="../Scripts/Validationfx.js" type="text/javascript"></script>--%>
<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script lang="javascript" type="text/javascript">
    function OpenPopUpTTM(str) {
        window.open("/backroom/GeneralBugPopUp.aspx?pageurl=" + location.href + "&casenumber=" + str, '', "height=260,width=410,resizable=no,status=no,scrollbar=no,menubar=no,location=no");
        return false;
    }

    function redirect(url) {
        window.open(url);
    }

    function OpenCourtSearch() {
        window.open('http://courtsearch.legalhouston.com');
        return false;
    }
    // Fahad 13/01/2009 5376 ---------Method to open new window in which Business Logic is appear------- 
    function OpenLogic() {
        var myWindow;
        var width = 760;
        var height = 540;
        var left = parseInt((screen.availWidth / 2) - (width / 2));
        var top = parseInt((screen.availHeight / 2) - (height / 2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
        //Ozair 7379 04/29/2010 fix business logic popup issue for POLM and Role Based security nested folder
        myWindow = window.open("<%=ViewState["hfServerAdd"]%>/businesslogic/logicpage.aspx?MenuID=<%=ViewState["sMenu"]%>", "subWind", windowFeatures);
        return false;
    }

    // Noufil 4378 07/23/2008 Function to open a window at page center.... this fuction use to open change password page
    // Sabir Khan 5650 03/12/2009 Function to open a window at page center...
    function openCenteredWindow(url, width, height) {
        var left = parseInt((screen.availWidth / 2) - (width / 2));
        var top = parseInt((screen.availHeight / 2) - (height / 2));
        var windowFeatures = "width=" + width + ",height=" + height +
            ",status,left=" + left + ",top=" + top +
            "screenX=" + left + ",screenY=" + top;
        window.open(url, "", windowFeatures);
        return false;
    }
    // Haris Ahmed 10181 04/13/2009 Function to open a window at page top...
    function openTopWindow(url, width, height) {
        var left = parseInt((screen.availWidth / 2) - (width / 2));
        var top = parseInt((screen.availHeight / 2) - (height / 2));
        var windowFeatures = "width=" + width + ",height=" + height +
            ",status,left=" + left + ",top=" + top +
            "screenX=" + left + ",screenY=" + top + ",scrollbars=yes";
        window.open(url, "", windowFeatures);
        return false;
    }

    $(document).ready(function () {
        selectMenu();
    });
    function selectMenu() {
        $("#mainmenu li").attr("class", "");
        $("#ActiveMenu1_hidMenuID").val();
        $("#mainmenu li a[menu-id!='" + $("#ActiveMenu1_hidMenuID").val() + "']").parent().attr("class", "");
        $("#mainmenu li a[menu-id!='" + $("#ActiveMenu1_hidMenuID").val() + "']").attr("class", "");
        $("#mainmenu li a[menu-id='" + $("#ActiveMenu1_hidMenuID").val() + "']").parent().attr("class", "open");
        $("#mainmenu li a[menu-id='" + $("#ActiveMenu1_hidMenuID").val() + "']").attr("class", "active");
    }
</script>

<!--
<script language="JavaScript1.2">mmLoadMenus();</script>
-->
<body>
    <asp:HiddenField ID="hidMenuID" runat="server" />
    <!-- START TOPBAR -->
    <div class='page-topbar '>
        <div class="logo-area">
            <%--<asp:Image ID="Image2" runat="server" ImageUrl="../Images/enation_logo.gif"></asp:Image>--%>
        </div>
        <div class='quick-area'>
            <div class='pull-left'>
                <ul class="info-menu left-links list-inline list-unstyled">
                    <li class="sidebar-toggle-wrap">
                        <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <li class="hidden-sm hidden-xs searchform">
                        <form action="ui-search.html" method="post">
                            <div class="input-group" style="display: none">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" class="form-control animated fadeIn" placeholder="Search & Enter">
                            </div>
                            <input type='submit' value="">
                        </form>
                    </li>
                </ul>
            </div>
            <div class="addnewleadTopRightBtn">
                <asp:LinkButton ID="lnkb_addnewlead" runat="server" ToolTip="Add New Lead"
                    OnClientClick="return openTopWindow('../clientinfo/addnewlead.aspx',400,420);"><i class="fa fa-user"></i>Add New Lead</asp:LinkButton>
            </div>
            <div class='pull-right'>
                <ul class="info-menu right-links list-inline list-unstyled">
                    <li class="profile">
                        <a href="#" data-toggle="dropdown" class="toggle">
                            <%--<img src="../data/profile/profile-blog.jpg" alt="user-image" class="img-circle img-inline">--%>
                            <asp:Label ID="lbl_Name" Style="font-size: 11px;" runat="server"></asp:Label>
                            <i class="fa fa-angle-down"></i>
                            <%--<span>Mark Yurey <i class="fa fa-angle-down"></i></span>--%>
                        </a>
                        <ul class="dropdown-menu profile animated fadeIn">
                            <li>
                                <asp:LinkButton ID="LinkButton6" runat="server"
                                    OnClientClick="return openCenteredWindow('../backroom/changeEmpPassword.aspx',400,250);">
                                    <i class="fa fa-lock"></i><span>Change Password</span>
                                </asp:LinkButton>
                                <%--<a href="#" onclientclick="return openCenteredWindow('../backroom/changeEmpPassword.aspx',400,250);">
                                    <i class="fa fa-wrench"></i>
                                    Change Password
                                </a>--%>
                            </li>


                            <li style="display: none;"></li>



                            <li>
                                <asp:LinkButton ID="img_Logic" runat="server" OnClientClick="OpenLogic()"> <i class="fa fa-question"></i><span>Help</span></asp:LinkButton>

                            </li>

                            <li>
                                <a href="../backroom/Admin.aspx?sMenu=67"><i class="fa fa-user"></i><span>User Management</span></a>

                                <%--<asp:LinkButton ID="LinkButton1" runat="server"  OnClientClick="window.location('../backroom/Admin.aspx?sMenu=67','_self')" > </asp:LinkButton>--%>
                                
                            </li>

                            <li>
                                <a href="../backroom/Courts.aspx?sMenu=67"><i class="fa fa-gavel"></i><span>Courts</span></a>
                                <%--<asp:LinkButton ID="LinkButton2" runat="server"  OnClientClick="window.location('../backroom/Courts.aspx?sMenu=67','_self')" > <i class="fa fa-info"></i>Courts</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../Backroom/outsidefirm.aspx?sMenu=67"><i class="fa fa-external-link"></i><span>Outside Firm</span></a>
                                <%--<asp:LinkButton ID="LinkButton3" runat="server"  OnClientClick="redirect('../Backroom/outsidefirm.aspx?sMenu=67')" > <i class="fa fa-info"></i>Outside Firm</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../PaymentInfo/FinancialReps.aspx?sMenu=67"><i class="fa fa-calculator"></i><span>Financial Reports</span></a>
                                <%--<asp:LinkButton ID="LinkButton4" runat="server"  OnClientClick="redirect('../PaymentInfo/FinancialReps.aspx?sMenu=67')" > <i class="fa fa-info"></i>Financial Reports</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../backroom/PrincingMain.aspx?sMenu=67"><i class="fa fa-money"></i><span>Price Plan</span></a>
                                <%--<asp:LinkButton ID="LinkButton5" runat="server"  OnClientClick="redirect('../backroom/PrincingMain.aspx?sMenu=67')" > <i class="fa fa-info"></i>Price Plan</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../backroom/CategoryInfo.aspx?sMenu=67"><i class="fa fa-info"></i><span>Category Info</span></a>
                                <%--<asp:LinkButton ID="LinkButton7" runat="server"  OnClientClick="redirect('../backroom/CategoryInfo.aspx?sMenu=67')" > <i class="fa fa-info"></i>Category Info</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../Backroom/ViolationList.aspx?sMenu=100"><i class="fa fa-ticket"></i><span>Manage Violation</span></a>
                                <%--<asp:LinkButton ID="LinkButton8" runat="server"  OnClientClick="redirect('../backroom/AddViolationCategory.aspx?sMenu=67')" > <i class="fa fa-info"></i>Violation Category</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../backroom/TemplateMain.aspx?sMenu=67"><i class="fa fa-columns"></i><span>Pricing Templates</span></a>
                                <%--<asp:LinkButton ID="LinkButton9" runat="server"  OnClientClick="redirect('../backroom/TemplateMain.aspx?sMenu=67')" > <i class="fa fa-info"></i>Pricing Templates</asp:LinkButton>--%>
                            </li>


                            <li>
                                <a href="../backroom/frmSPList.aspx?sMenu=67"><i class="fa fa-pencil-square-o"></i><span>Report Editor</span></a>
                                <%--<asp:LinkButton ID="LinkButton10" runat="server"  OnClientClick="redirect('../backroom/frmSPList.aspx?sMenu=67')" > <i class="fa fa-info"></i>Report Editor</asp:LinkButton>--%>
                            </li>


                            <li>
                                <a href="../backroom/frmReportCategory.aspx?sMenu=67"><i class="fa fa-bar-chart"></i><span>Report Category</span></a>
                                <%--<asp:LinkButton ID="LinkButton11" runat="server"  OnClientClick="redirect('../backroom/frmReportCategory.aspx?sMenu=67')" > <i class="fa fa-info"></i>Report Category</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../Configuration/AttorneyPayout.aspx?sMenu=67"><i class="fa fa-twitch"></i><span>Attorney Payout</span></a>
                                <%--<asp:LinkButton ID="LinkButton12" runat="server"  OnClientClick="redirect('../Configuration/AttorneyPayout.aspx?sMenu=67')" > <i class="fa fa-info"></i>Attorney Payout</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../Reports/PaymentTypes.aspx?sMenu=67"><i class="fa fa-credit-card"></i><span>Payment Types</span></a>
                                <%--<asp:LinkButton ID="LinkButton13" runat="server"  OnClientClick="redirect('../Reports/PaymentTypes.aspx?sMenu=67')" > <i class="fa fa-info"></i>Payment Types</asp:LinkButton>--%>
                            </li>


                            <li>
                                <a href="../Activities/ServiceTicketCategories.aspx?sMenu=148"><i class="fa fa-shield"></i><span>Service Type Categories</span></a>
                                <%--<asp:LinkButton ID="LinkButton14" runat="server"  OnClientClick="redirect('../Activities/ServiceTicketCategories.aspx?sMenu=148')" > <i class="fa fa-info"></i>Service Ticket Categories</asp:LinkButton>--%>
                            </li>

                            <li>
                                <a href="../ErrorLog/Bugtracker.aspx?sMenu=148"><i class="fa fa-exclamation-triangle"></i><span>Error Logs</span></a>
                                <%--<asp:LinkButton ID="LinkButton15" runat="server"  OnClientClick="redirect('../ErrorLog/Bugtracker.aspx?sMenu=148')" > <i class="fa fa-info"></i>Error Logs</asp:LinkButton>--%>
                            </li>


                            <li class="" id="td_logoff1" runat="server">

                                <asp:LinkButton ID="lnkb_LogOff" runat="server"> <i class="fa fa-sign-out"></i><span>Log Off</span></asp:LinkButton>

                            </li>
                        </ul>
                    </li>

                </ul>
                
            </div>
            
           





        </div>

    </div>
    <!-- END TOPBAR -->

    <!-- SIDEBAR - START -->

    <div class="page-sidebar pagescroll">

        <!-- MAIN MENU - START -->
        <div class="page-sidebar-wrapper" id="main-menu-wrapper">

            <!-- USER INFO - START -->
            <div class="profile-info row">

                <%--   <div class="profile-image col-xs-4">
                    <a href="ui-profile.html">
                        <img alt="" src="../data/profile/profile-blog.jpg" class="img-responsive img-circle">
                    </a>
                </div>

                <div class="profile-details col-xs-8">

                    <h3>
                        <a href="ui-profile.html">Mark Yurey</a>

                        <!-- Available statuses: online, idle, busy, away and offline -->
                        <span class="profile-status online"></span>
                    </h3>

                    <p class="profile-title">Administrator</p>

                </div>--%>
            </div>
            <!-- USER INFO - END -->



            <ul class='wraplist' id="mainmenu">


                <li class="">
                    <a href="../frmMain.aspx?sMenu=67" menu-id="~/frmMain.aspx">
                        <i class="fa fa-search"></i>
                        <span class="title">Search</span>
                    </a>
                </li>
                <li class="open" id="limatter">
                    <a href="javascript:;">
                        <i class="fa fa-gears"></i>
                        <span class="title">Matter</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" style='' id="ulmatter">
                        <li>
                            <a id="idviolationinfo" menu-id="~/ClientInfo/ViolationFeeold.aspx" runat="server" class="active" href="../ClientInfo/ViolationFeeold.aspx?sMenu=61">Violation</a>
                        </li>
                        <li>
                            <a id="idgenerailinfo" menu-id="~/ClientInfo/generalinfo.aspx" runat="server" class="" href="../Clientinfo/generalinfo.aspx?sMenu=62">General Info</a>
                        </li>
                        <li>
                            <a id="idpaymentinfo" menu-id="~/ClientInfo/newpaymentinfo.aspx" runat="server" class="" href="../Clientinfo/newpaymentinfo.aspx?sMenu=63">Payment Info</a>
                        </li>
                        <li>
                            <a id="iddocument" menu-id="~/Paperless/Documents.aspx" runat="server" class="" href="../Paperless/Documents.aspx?sMenu=119">Documents</a>
                        </li>
                        <li>
                            <a id="idcasehistory" menu-id="~/Clientinfo/CaseHistory.aspx" runat="server" class="" href="../Clientinfo/CaseHistory.aspx?sMenu=64">History</a>
                        </li>
                        <li>
                            <a id="idcomment" runat="server" menu-id="~/Clientinfo/Comments.aspx" class="" href="../Clientinfo/Comments.aspx?sMenu=221">Comments</a>
                        </li>
                        <li id="associateMenu" runat="server" visible="false">
                            <a id="idassociatedmatters" runat="server" menu-id="~/Clientinfo/AssociatedMatters.aspx" class="" href="../Clientinfo/AssociatedMatters.aspx?sMenu=213">Associated Matters</a>
                        </li>
                        <li>
                            <a id="idcasedisposition" runat="server" menu-id="~/Clientinfo/CaseDisposition.aspx" class="" href="../Clientinfo/CaseDisposition.aspx?sMenu=65">Disposition</a>
                        </li>
                    </ul>
                </li>
                <li id="liactivity">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Activities</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" id="ulactivity">
                        <li>
                            <a id="A1" runat="server" menu-id="~/QuickEntryNew/ReminderCalls.aspx" class="active" href="../QuickEntryNew/ReminderCalls.aspx?sMenu=95">Reminder Calls</a>
                        </li>
                        <li>
                            <a id="A25" runat="server" menu-id="~/Activities/NewPartialPay.aspx" class="active" href="../Activities/NewPartialPay.aspx?sMenu=120">Partial Pay</a>
                        </li>
                        <li>
                            <a id="A2" runat="server" menu-id="~/Activities/QuoteCallBackMain.aspx" class="" href="../Activities/QuoteCallBackMain.aspx?sMenu=39">Quote Call Back</a>
                        </li>
                        <li>
                            <a id="A3" runat="server" menu-id="~/Activities/nexttelemail.aspx" class="" href="../Activities/nexttelemail.aspx?sMenu=26">Text Message</a>
                        </li>
                        <li>
                            <a id="A12" runat="server" menu-id="~/Activities/frmClientLookup.aspx" class="" href="../Activities/frmClientLookup.aspx?sMenu=29">Traffic Alerts</a>
                        </li>
                        <li>
                            <a id="A13" runat="server" menu-id="~/PaymentInfo/CloseOutReport.aspx" class="" href="../PaymentInfo/CloseOutReport.aspx?sMenu=27">Close Out</a>
                        </li>
                        <li>
                            <a id="A4" runat="server" menu-id="~/Activities/DoNotMail.aspx.aspx" class="" href="../Activities/DoNotMail.aspx?sMenu=121">Do Not Mail Update</a>
                        </li>
                        <li>
                            <a id="A5" runat="server" menu-id="~/Activities/frmManualCC.aspx" class="" href="../Activities/frmManualCC.aspx?sMenu=107">Manual CC</a>
                        </li>
                        <li>
                            <a id="A26" runat="server" menu-id="~/Reports/frmRptjurytrial.aspx" class="" href="../Reports/frmRptjurytrial.aspx?sMenu=111">Tried Cases Report</a>
                        </li>
                        <li>
                            <a id="A6" runat="server" menu-id="~/Activities/jurytrial.aspx" class="" href="../Activities/jurytrial.aspx?sMenu=110">Atty - Trials</a>
                        </li>
                        <li>
                            <a id="A11" runat="server" menu-id="~/Reports/BatchPrint.aspx" class="" href="../Reports/BatchPrint.aspx?sMenu=87">Batch Prints</a>
                        </li>
                        <li>
                            <a id="A24" runat="server" menu-id="~/webscan/Newsearch.aspx" class="" href="../webscan/Newsearch.aspx?sMenu=102">BSDA (HMC)</a>
                        </li>
                        <li>
                            <a id="A14" runat="server" menu-id="~/returneddockets/scandocket.aspx" class="" href="../returneddockets/scandocket.aspx?sMenu=114">Scan/Upload Docket</a>
                        </li>
                        <li id="Li1" runat="server" visible="false">
                            <a id="A7" runat="server" menu-id="~/returneddockets/scandocket.aspx" class="" href="../returneddockets/scandocket.aspx?sMenu=114">Scan Docket</a>
                        </li>

                        <li id="Li2" runat="server" visible="false">
                            <a id="A9" runat="server" menu-id="~/returneddockets/scandocket.aspx" class="" href="../returneddockets/scandocket.aspx?sMenu=114">Scan Docket</a>
                        </li>
                        <li>
                            <a id="A16" runat="server" menu-id="~/Reports/OpenServiceTicketReport.aspx" class="" href="../Reports/OpenServiceTicketReport.aspx?sMenu=140">Service Ticket Reports</a>
                        </li>
                        <li>
                            <a id="A17" runat="server" menu-id="~/Activities/PaymentPlans.aspx" class="" href="../Activities/PaymentPlans.aspx?sMenu=136">Payment Due Report</a>
                        </li>
                        <li id="Li3" runat="server" visible="false">
                            <a id="A10" runat="server" menu-id="~/Activities/PaymentPlans.aspx" class="" href="../Activities/PaymentPlans.aspx?sMenu=136">Payment Due Reports</a>
                        </li>
                        <li>
                            <a id="A8" runat="server" menu-id="~/activities/FTACall.aspx" class="" href="../activities/FTACall.aspx?sMenu=195">Missed Call Date Courts</a>
                        </li>
                        <li>
                            <a id="A18" runat="server" menu-id="~/Activities/Lead.aspx" class="" href="../Activities/Lead.aspx?sMenu=205">Leads</a>
                        </li>
                        <li>
                            <a id="A23" runat="server" menu-id="~/Activities/SetCalls.aspx" class="" href="../Activities/SetCalls.aspx?sMenu=194">Set Call</a>
                        </li>
                        <li>
                            <a id="A19" runat="server" menu-id="~/Reports/TrafficWaitingFollowUp.aspx" class="" href="../Reports/TrafficWaitingFollowUp.aspx?sMenu=196">Waiting Follow Up</a>
                        </li>
                        <li>
                            <a id="A20" runat="server" menu-id="~/Reports/ComplaintComments.aspx" class="" href="../Reports/ComplaintComments.aspx?sMenu=223">Complaint Comment Report</a>
                        </li>
                        <li>
                            <a id="A21" runat="server" menu-id="~/quickentrynew/trialdockets.aspx" class="" href="../quickentrynew/trialdockets.aspx?sMenu=97">Trial Docket</a>
                        </li>
                        <li>
                            <a id="A22" runat="server" menu-id="~/quickentrynew/frmRptDocketULNew.aspx" class="" href="../quickentrynew/frmRptDocketULNew.aspx?sMenu=98">Docket Report</a>
                        </li>
                        <%--<li>
                            <a id="A23" runat="server" class="" href="../reports/FrmVoidPayment.aspx?sMenu=30">Void Payment</a>
                        </li>
                        <li>
                            <a id="A24" runat="server" class="" href="../reports/bondreport.aspx?sMenu=183">Bond Alert</a>
                        </li>--%>
                        <li>
                            <a id="A15" runat="server" menu-id="~/Reports/NewDocketCloseOutOptimize.aspx" class="" href="../Reports/NewDocketCloseOutOptimize.aspx?sMenu=104">Docket Close out</a>
                        </li>
                    </ul>
                </li>
                <li id="livalidtion">
                    <a href="javascript:;">
                        <i class="fa fa-check"></i>
                        <span class="title">Validations</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" id="ulvalidtion">
                        <li>
                            <a id="A27" runat="server" menu-id="~/Reports/NOSReport.aspx" class="active" href="../Reports/NOSReport.aspx?sMenu=159">Nos Reports</a>
                        </li>
                        <li>
                            <a id="A28" runat="server" menu-id="~/Reports/SplitReport.aspx" class="active" href="../Reports/SplitReport.aspx?sMenu=60">Split Report</a>
                        </li>
                        <li>
                            <a id="A30" runat="server" menu-id="~/Reports/PendingTrialLetters.aspx" class="active" href="../Reports/PendingTrialLetters.aspx?sMenu=116">NO Trial Letter Report</a>
                        </li>
                        <li>
                            <a id="A31" runat="server" menu-id="~/Reports/DisposedDiscrepancies.aspx" class="active" href="../Reports/DisposedDiscrepancies.aspx?sMenu=131">No Disposition Letter</a>
                        </li>
                        <li>
                            <a id="A32" runat="server" menu-id="~/Reports/bondreport.aspx" class="active" href="../Reports/bondreport.aspx?sMenu=183">Bond Alert</a>
                        </li>
                        <li>
                            <a id="A33" runat="server" menu-id="~/Reports/PastCourtdateNonHMC.aspx" class="active" href="../Reports/PastCourtdateNonHMC.aspx?sMenu=209">Past Court Dates</a>
                        </li>

                         <li>
                            <a id="Ab53" runat="server" menu-id="~/Reports/InternetSignup.aspx" class="active" href="../Reports/InternetSignup.aspx?sMenu=126">Online Internet Signup</a>
                        </li>

                        <li>
                            <a id="Ac53" runat="server" menu-id="~/Reports/BadEmailReport.aspx" class="active" href="../Reports/BadEmailReport.aspx?sMenu=226">Bad Email Address</a>
                        </li>
                        <li>
                            <a id="A34" runat="server" menu-id="~/Reports/ReminderCallValidation.aspx" class="active" href="../Reports/ReminderCallValidation.aspx?sMenu=142">Reminder Calls Alert</a>
                        </li>
                        <li>
                            <a id="A35" runat="server" menu-id="~/Reports/SignedContractReport.aspx" class="active" href="../Reports/SignedContractReport.aspx?sMenu=201">Unsigned Contract</a>
                        </li>
                        <li>
                            <a id="A36" runat="server" menu-id="~/Reports/NOSReport.aspx" class="active" href="../Reports/NOSReport.aspx?sMenu=159">NOS</a>
                        </li>
                        <li>
                            <a id="A37" runat="server" menu-id="~/Reports/WaitingFollowUpCriminal.aspx" class="active" href="../Reports/WaitingFollowUpCriminal.aspx?sMenu=266">Criminal Waiting</a>
                        </li>
                        <li>
                            <a id="A45" runat="server" menu-id="~/reports/NoLORReport.aspx" class="active" href="../reports/NoLORReport.aspx?sMenu=113">No Lor Sent</a>
                        </li>
                        <li>
                            <a id="A46" runat="server" menu-id="~/Reports/LORconfirmation.aspx" class="active" href="../Reports/LORconfirmation.aspx?sMenu=193">No Lor Confirmation Alert</a>
                        </li>
                        <li>
                            <a id="A47" runat="server" menu-id="~/Reports/WaitingFollowUpTraffic.aspx" class="active" href="../Reports/WaitingFollowUpTraffic.aspx?sMenu=264">Traffice Waiting</a>
                        </li>
                        <li>
                            <a id="A48" runat="server" menu-id="~/backroom/ValidationEmail.aspx" class="active" href="../backroom/ValidationEmail.aspx?sMenu=143">Generate Validation Report</a>
                        </li>
                        <li>
                            <a id="A49" runat="server" menu-id="~/Reports/legalconsultation.aspx" class="active" href="../Reports/legalconsultation.aspx?sMenu=192">Leads Report</a>
                        </li>


                    </ul>
                </li>

                <li id="lireport">
                    <a href="javascript:;">
                        <i class="fa fa-bar-chart"></i>
                        <span class="title">Reports</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" id="ulreport">


                        <li>
                            <a id="A51" runat="server" menu-id="~/activities/Arraignment.aspx" class="active" href="../activities/Arraignment.aspx?sMenu=20">Arraignment</a>
                        </li>
                        <li>
                            <a id="A52" runat="server" menu-id="~/quickentrynew/arrcombined.aspx?" class="active" href="../quickentrynew/arrcombined.aspx?sMenu=96">Docket Settings</a>
                        </li>
                        <li>
                            <a id="A38" runat="server" menu-id="~/Reports/FlagsReport.aspx" class="active" href="../Reports/FlagsReport.aspx?sMenu=172">SOL Report</a>
                        </li>
                        <li>
                            <a id="A39" runat="server" menu-id="~/Reports/FlagsReport.aspx" class="active" href="../Reports/FlagsReport.aspx?sMenu=172">Flag report</a>
                        </li>
                        <li>
                            <a id="A40" runat="server" menu-id="~/Reports/frmRptjurytrial.aspx" class="active" href="../Reports/frmRptjurytrial.aspx?sMenu=111">Tried Cases</a>
                        </li>
                        <li>
                            <a id="A41" runat="server" menu-id="~/Reports/ContinuanceReport.aspx" class="active" href="../Reports/ContinuanceReport.aspx?sMenu=118">Continuance Report</a>
                        </li>
                        <li>
                            <a id="A42" runat="server" menu-id="~/Reports/CommentsReport.aspx" class="active" href="../Reports/CommentsReport.aspx?sMenu=222">Consultation Comment Report</a>
                        </li>
                        <li>
                            <a id="A43" runat="server" menu-id="~/Reports/ConcentrationReport.aspx" class="active" href="../Reports/ConcentrationReport.aspx?sMenu=145">Concentration Report</a>
                        </li>
                        <li>
                            <a id="A44" runat="server" menu-id="~/Reports/PastDueCalls.aspx" class="active" href="../Reports/PastDueCalls.aspx?sMenu=207">Past Due Calls Report</a>
                        </li>
                        <li id="Li4">
                            <a id="A50" runat="server" menu-id="~/returneddockets/docketsreports.aspx" class="" href="../returneddockets/docketsreports.aspx?sMenu=115">Scanned Docket</a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="javascript:;">
                        <i class="fa fa-gear"></i>
                        <span class="title">Configuration</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a id="A29" runat="server" menu-id="~/Configuration/AttorneyPayout.aspx" class="active" href="../Configuration/AttorneyPayout.aspx?sMenu=225">Attorney Payout Report</a>
                        </li>
                    </ul>
                </li>

                <%--    <li class="">
                    <a href="blo-search.html">
                        <i class="fa fa-search"></i>
                        <span class="title">Search</span>
                    </a>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-upload"></i>
                        <span class="title">Media</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-media.html">All Media</a>
                        </li>
                        <li>
                            <a class="" href="blo-upload.html">Upload</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-sitemap"></i>
                        <span class="title">Categories</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-categories.html">All Categories</a>
                        </li>
                        <li>
                            <a class="" href="blo-category-add.html">Add Category</a>
                        </li>
                        <li>
                            <a class="" href="blo-category-edit.html">Edit Category</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-files-o"></i>
                        <span class="title">Pages</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-pages.html">All Pages</a>
                        </li>
                        <li>
                            <a class="" href="blo-page-add.html">Add Page</a>
                        </li>
                        <li>
                            <a class="" href="blo-page-edit.html">Edit Page</a>
                        </li>
                        <li>
                            <a class="" href="blo-page-view.html">View Page</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span class="title">Users</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-users.html">All Users</a>
                        </li>
                        <li>
                            <a class="" href="blo-user-add.html">Add User</a>
                        </li>
                        <li>
                            <a class="" href="blo-user-edit.html">Edit User</a>
                        </li>
                        <li>
                            <a class="" href="blo-user-profile.html">User Profile</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-envelope"></i>
                        <span class="title">Mailbox</span>
                        <span class="arrow "></span><span class="label label-accent">4</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-mail-inbox.html">Inbox</a>
                        </li>
                        <li>
                            <a class="" href="blo-mail-compose.html">Compose</a>
                        </li>
                        <li>
                            <a class="" href="blo-mail-view.html">View</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-bar-chart"></i>
                        <span class="title">Reports</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-report-site.html">Site</a>
                        </li>
                        <li>
                            <a class="" href="blo-report-visitors.html">Visitors</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-tags"></i>
                        <span class="title">Tags</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="blo-tags.html">All Tags</a>
                        </li>
                        <li>
                            <a class="" href="blo-tag-add.html">Add Tag</a>
                        </li>
                        <li>
                            <a class="" href="blo-tag-edit.html">Edit Tag</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;">
                        <i class="fa fa-suitcase"></i>
                        <span class="title">Multi Purpose</span>
                        <span class="arrow "></span><span class="label label-accent">HOT</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a class="" href="general.html" target='_blank'>General Admin</a>
                        </li>
                        <li>
                            <a class="" href="hospital.html" target='_blank'>Hospital Admin</a>
                        </li>
                        <li>
                            <a class="" href="music.html" target='_blank'>Music Admin</a>
                        </li>
                        <li>
                            <a class="" href="crm.html" target='_blank'>CRM Admin</a>
                        </li>
                        <li>
                            <a class="" href="socialmedia.html" target='_blank'>Social Media Admin</a>
                        </li>
                        <li>
                            <a class="" href="freelancing.html" target='_blank'>Freelancing Admin</a>
                        </li>
                        <li>
                            <a class="" href="university.html" target='_blank'>University Admin</a>
                        </li>
                        <li>
                            <a class="" href="ecommerce.html" target='_blank'>Ecommerce Admin</a>
                        </li>
                        <li>
                            <a class="" href="blog.html" target='_blank'>Blog Admin</a>
                        </li>
                    </ul>
                </li>--%>
            </ul>

            <%-- <div class="menustats">
                <h5>Project Progress</h5>
                <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                    </div>
                </div>
                <h5>Target Achieved</h5>
                <div class="progress">
                    <div class="progress-bar progress-bar-accent" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                    </div>
                </div>
            </div>--%>
        </div>
        <!-- MAIN MENU - END -->



    </div>
    <!--  SIDEBAR - END -->


    <%--<table id="Table1" style="font-weight: bold; font-size: 11px; width: 100%; color: white;
        font-family: Tahoma, Sans-Serif; height: 30px" cellspacing="0" cellpadding="1"
        width="780" align="left" border="0">
        <tr>
            <td style="width: 780; height: 7px" colspan="9">
            </td>
        </tr>
        <tr>
            <td class="" style="width: 100%; height: 56px" colspan="9">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/enation_logo.gif"></asp:Image>
                            </td>
                            <td width="62%">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="lbl_Name" runat="server" ForeColor="RoyalBlue"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="lblClientIP" runat ="server" Text="Client IP Address" ForeColor="RoyalBlue"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblClientIPAddress" runat ="server"  ForeColor="RoyalBlue"></asp:Label>
                                        </td>                                        
                                    </tr>
                                     <tr>
                                        <td align="right">                                       
                                            <asp:Label ID="lblBranch" runat ="server" Text="Branch" ForeColor="RoyalBlue"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblBranchName" runat ="server"  ForeColor="RoyalBlue"></asp:Label>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td valign="bottom" align="right">
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                       
                                                    </td>
                                                    <td width="1%">
                                                    </td>
                                                    <td align="center">
                                                    </td>
                                                    <td width="1%">
                                                    </td>   
                                                    <td style="width: 353px" align="right">
                                                        <asp:LinkButton ID="lnkb_password" runat="server" Text="Change Password" ToolTip="Change Password"
                                                            OnClientClick="return openCenteredWindow('../backroom/changeEmpPassword.aspx',400,200);"></asp:LinkButton>
                                                    </td>
                                                    <td width="1%">
                                                        <span style="color: RoyalBlue">|</span>
                                                    </td>
                                                    <td align="center">
                                                            <asp:LinkButton ID="lnkb_addnewlead" runat="server" Text="Add New Lead" ToolTip="Add New Lead"
                                                            OnClientClick="return openTopWindow('../clientinfo/addnewlead.aspx',400,420);"></asp:LinkButton>
                                                    </td>
                                                    <td id="td_logoff1" runat="server" width="1%">
                                                        <asp:Label ID="lblSep" runat="server" Text="|" ForeColor="RoyalBlue"></asp:Label>
                                                    </td>
                                                    <td id="td_logoff2" runat="server" align="center">
                                                        <asp:LinkButton ID="lnkb_LogOff" runat="server" ForeColor="RoyalBlue">Log Off</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        &nbsp; <span style="color: RoyalBlue">|</span>&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="img_Logic" onclick="OpenLogic();" runat="server" ImageUrl="~/Images/help.gif"
                                                            ToolTip="Show Business Logic" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
    </table>
    <tr height="7">
        <td colspan="9">
        </td>
    </tr>
    <tr>
        <td>
            <table style="font-weight: bold; font-size: 11px; width: 100%; color: white; font-family: Tahoma, Sans-Serif;
                height: 30px" cellspacing="0" cellpadding="1" width="780" align="left" border="0">
                <tr>
                    <%=mainMenu()%>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            <table bordercolor="#ff9966" cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffcc00"
                border="1">
                <tr>
                    <%=subMenu()%>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            <img height="17" hspace="0" src="../../images/head_icon.gif" width="30" align="left"><asp:Label
                ID="lblheading" runat="server" CssClass="clsmainhead"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="display: none" align="left">
            <asp:TextBox ID="txtid" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtsrch" runat="server"></asp:TextBox>            
        </td>
    </tr>--%>
    <asp:TextBox ID="txtid" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtsrch" runat="server"></asp:TextBox>


</body>

<script lang="javascript" type="text/javascript">
    $(document).ready(function () {
        debugger;
        $("#mainmenu li").attr("class", "");
        $("#ActiveMenu1_hidMenuID").val();
        $("#mainmenu li a[menu-id!='" + $("#ActiveMenu1_hidMenuID").val() + "']").parent().attr("class", "");
        $("#mainmenu li a[menu-id!='" + $("#ActiveMenu1_hidMenuID").val() + "']").attr("class", "");
        $("#mainmenu li a[menu-id='" + $("#ActiveMenu1_hidMenuID").val() + "']").parent().attr("class", "open");
        $("#mainmenu li a[menu-id='" + $("#ActiveMenu1_hidMenuID").val() + "']").attr("class", "active");
        $("#mainmenu li a[menu-id='" + $("#ActiveMenu1_hidMenuID").val() + "']").css("font-weight", "bold");

        //$("#mainmenu li, #limatter").attr("class", "");
        //$("#ulmatter").css("display", "none");
        //$("#liactivity").attr("class", "open");
        //$("#ulactivity").css("display", "block");
        //$("#ActiveMenu1_A18").addClass("active");
        //$("#ActiveMenu1_A18 > a li > span:nth-child(2)").addClass("arrow open");

    });
</script>

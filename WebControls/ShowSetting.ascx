﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowSetting.ascx.cs"
    Inherits="HTP.WebControls.ShowSetting" %>
<link href="../Styles.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
                // Fahad 5808 04/23/2009 Validate Showsetting value Nymber by Adding Following Two Methods
                function CheckName(name)
                { 
                    if(name.length==1 && name.charCodeAt(0)==48 )
                    {
                        return false;
                    }
                    else if(name.length==1 && name.charCodeAt(0)!=48 )
                    {
                        if(!(((name.charCodeAt(0) > 48 && (name.charCodeAt(0) <=57)))))
                       return false; 
                    }
                    else if(name.length==2 && name.charCodeAt(0)==48)
                    {
                        if(name.charCodeAt(1)==48)
                        return false;
                        else if (!(((name.charCodeAt(1) > 48 && (name.charCodeAt(1) <=57)))))
                        return false;
                        
                    }
                    else if(name.length==2 && name.charCodeAt(0)!=48)
                    {
                        if (!(((name.charCodeAt(0) > 48 && (name.charCodeAt(0) <=57)))))
                        return false;
                        else if (!(((name.charCodeAt(0) >= 48 && (name.charCodeAt(0) <=57)))))
                        return false;
                    }
                    if(name.length==3)
                    {
                        if(name.charCodeAt(0)==48 && name.charCodeAt(1)==48 && name.charCodeAt(2)==48)
                        {
                            return false;
                        }
                        else if(name.charCodeAt(0)==48 && name.charCodeAt(1)!=48)
                        {
                           if (!(((name.charCodeAt(1) > 48 && (name.charCodeAt(1) <=57)))))
                           return false;
                           else  if (!(((name.charCodeAt(2) >= 48 && (name.charCodeAt(2) <=57)))))
                           return false;  
                        }
                         else if(name.charCodeAt(0)!=48)
                        {
                           if (!(((name.charCodeAt(0) > 48 && (name.charCodeAt(0) <=57)))))
                           return false;
                           else if(!(((name.charCodeAt(1) >= 48 && (name.charCodeAt(1) <=57)))))
                           return false; 
                           else if (!(((name.charCodeAt(2) >= 48 && (name.charCodeAt(2) <=57)))))
                           return false;  
                        }
                        else if(name.charCodeAt(0)==48 && name.charCodeAt(1)==48 && name.charCodeAt(2)!=48)
                        {
                             if (!(((name.charCodeAt(2) > 48 && (name.charCodeAt(2) <=57)))))
                             return false; 
                        }
                      }
                     return true
                 }   
                        
                        
                        
//                                    for ( i = 0 ; i < name.length ; i++)
//                                    {
//                                        var asciicode =  name.charCodeAt(i)
//                                        
//                                        if(name.charCodeAt(0)==48)
//                                        {
//                                            if(name.charCodeAt(0)==48 && name.charCodeAt(1)==48)
//                                            {
//                                               
//                                                    if (!(((name.charCodeAt(2) >= 48 && (name.charCodeAt(2) <=57)))))
//                                                    return false;
//                                                
//                                             }
//                                            else
//                                            {
//                                                if (!((asciicode >= 48 && asciicode <=57)))
//                                                return false;
//                                            }
//                                            
//                                        }
//                                        else
//                                        {
//                                            if (!((asciicode >= 48 && asciicode <=57)))
//                                            return false;
//                                        }
//                                   }
//                                   
//                     }
//                     return true;
//                                
//              }
              function CheckNumber()
              {

                    var number =document.getElementById ("<%= this.ClientID %>_txtNumberofDays").value ;
                    if(number.length == 0)
                    {
                        alert("Please enter number of business days from 1 till 999");
                        return false;
                    }
                    else if(CheckName(number)==false)
                    {
                        alert("Please enter number of business days from 1 till 999");
                        return false;
                    }
                   
              }
      
        function showhide(ele,caller) 
        {
            var srcElement = document.getElementById(ele);
            
            if(srcElement != null) {
            if(srcElement.style.display == "block") {
               caller.innerHTML = "Show Settings";
               srcElement.style.display= 'none';
            }
            else {
               caller.innerHTML = "Hide Settings";
               srcElement.style.display='block';
            }
            return false;
          }
        }
        //Waqas 5653 03/31/2009 Checking for Negative value
//        function CheckNegative()
//        {
//            var number = document.getElementById ("<%= this.ClientID %>_txtNumberofDays").value;             
//            if(isNaN(number))
//            {
//                    alert("Please enter number of business days from 1 till 999");
//                    return false;
//            }
//            //Fahad 5808 04/21/2009 Allow to user maximum 3 digit no
//            
////            else if(number <= 0)
////            {
////                alert("Please enter number of business days from 1 till 999");
////                return false;
////            }
//        }
</script>

<table width="100%">
    <tr>
        <td style="width: 150px" align="left" runat="server" id="tdPrimary" visible="false">
            <a id="caller" href="#" onclick="showhide('<%=this.ClientID %>_trday',this)">Show Settings</a>
        </td>
        <td align="left" class="" id="trday" runat="server" style="display: none">
            <asp:Label ID="lblHeadingbefore" runat="server" Text="FollowUpDate In Next "></asp:Label>
            <asp:TextBox ID="txtNumberofDays" runat="server" CssClass="clsInputadministration"
                MaxLength="3"></asp:TextBox>
            <asp:HiddenField ID="hfNumberofDays" runat="server" />
            <asp:Label ID="lblHeadingafter" runat="server" Text=" (Business Days)"></asp:Label>
            <asp:Button ID="btnUpdate" runat="server" OnClientClick="return CheckNumber();" OnClick="btnUpdate_Click"
                Text="Update" CssClass="btn btn-primary pull-right" />
        </td>
    </tr>
</table>

﻿using HTP.ClientController;
using System;
using System.Web.UI.WebControls;
using HTP.Components.Services;
using lntechNew.Components.ClientInfo;
using System.Text;
using lntechNew.Components;
using System.Configuration;
using MailMessage = MailBee.Mime.MailMessage;
using FrameWorkEnation.Components;
using System.Web.UI;
using POLMDTO;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using UserDto = RoleBasedSecurity.DataTransferObjects.UserDto;


namespace HTP.WebControls
{
    /// <summary>
    ///     Control to add consultation request.
    /// </summary>
    public partial class PolmControl : UserControl
    {
        #region Properties

        /// <summary>
        ///     This Property is use to show or hide modal pop up. If no show or hiding required set to null or empty.
        /// </summary>
        public string ModalPopupName
        {
            get { return Convert.ToString(ViewState["modalPopupName"]); }
            set { ViewState["modalPopupName"] = value; }
        }

        /// <summary>
        ///     Ticket ID of Client. Must be greater than Zero
        /// </summary>
        public int TicketId
        {
            get { return Convert.ToInt32(ViewState["TicketId"]); }
            set { ViewState["TicketId"] = value; }
        }

        /// <summary>
        ///     Login User ID
        /// </summary>
        public int EmpId
        {
            get { return Convert.ToInt32(ViewState["empid"]); }
            set { ViewState["empid"] = value; }
        }

        /// <summary>
        ///     Login User ID
        /// </summary>
        public int UserIdofTpId
        {
            get { return Convert.ToInt32(ViewState["userIdofTpId"]); }
            set { ViewState["userIdofTpId"] = value; }
        }


        /// <summary>
        ///     Login User is primary or not. By dafault it will be False.
        /// </summary>
        public bool IsPrimaryUser
        {
            get { return Convert.ToBoolean(ViewState["isPrimaryUser"]); }
            set { ViewState["isPrimaryUser"] = value; }
        }

        /// <summary>
        ///     Login User is primary or not. By dafault it will be False.
        /// </summary>
        public string EmpName
        {
            get { return Convert.ToString(ViewState["empName"]); }
            set { ViewState["empName"] = value; }
        }

        /// <summary>
        ///     Check whether ticket id is available or not.
        /// </summary>
        public bool IsTicketIdNotAvailable
        {
            get { return Convert.ToBoolean(ViewState["isTicketIdNotAvailable"]); }
            set { ViewState["isTicketIdNotAvailable"] = value; }
        }

        /// <summary>
        ///     First Name of Client
        /// </summary>
        public string ClientFirstName
        {
            get { return Convert.ToString(ViewState["clientFirstName"]); }
            set { ViewState["clientFirstName"] = value; }
        }

        /// <summary>
        ///     Last Name of CLient
        /// </summary>
        public string ClientLastName
        {
            get { return Convert.ToString(ViewState["clientLastName"]); }
            set { ViewState["clientLastName"] = value; }
        }

        /// <summary>
        ///     Language Speak by client
        /// </summary>
        public int ClientLanguageId
        {
            get { return Convert.ToInt32(ViewState["clientLanguageId"]); }
            set { ViewState["clientLanguageId"] = value; }
        }

        /// <summary>
        ///     Contact Type for client
        /// </summary>
        public int ClientContactTypeId
        {
            get { return Convert.ToInt32(ViewState["clientContactTypeId"]); }
            set { ViewState["clientContactTypeId"] = value; }
        }

        /// <summary>
        ///     Client Contact Number
        /// </summary>
        public string ClientPrimaryContactNumber
        {
            get { return Convert.ToString(ViewState["clientPrimaryContactNumber"]); }
            set { ViewState["clientPrimaryContactNumber"] = value; }
        }

        /// <summary>
        ///     CLient Address
        /// </summary>
        public string ClientAddress
        {
            get { return Convert.ToString(ViewState["clientAddress"]); }
            set { ViewState["clientAddress"] = value; }
        }

        /// <summary>
        ///     Client Email Address
        /// </summary>
        public string ClientEmailAddress
        {
            get { return Convert.ToString(ViewState["clientEmailAddress"]); }
            set { ViewState["clientEmailAddress"] = value; }
        }

        /// <summary>
        ///     Client Dob
        /// </summary>
        public DateTime ClientDob
        {
            get { return Convert.ToDateTime(ViewState["ClientDob"]); }
            set { ViewState["ClientDob"] = value; }
        }

        const int HoutonSourceId = 1;
        const int PrimaryUserRoleId = 3;

        #endregion

        #region Class Declaration

        // Class declare to get information of client.
        readonly clsCase _clsCase = new clsCase();
        readonly RoleBasedSecurityController _roleBasedSecurityController = new RoleBasedSecurityController();
        //Fahad 6766 03/15/2010 Implemented Configuration Service into POLM-Declared variable for using Configuration Service
        readonly ConfigurationKeys _objConfigurationKeys = new ConfigurationKeys();
        readonly PolmController _polmController = new PolmController();

        #endregion

        #region Events

        /// <summary>
        ///     Events Fire when page load
        /// </summary>
        /// <param name="sender">object </param>
        /// <param name="e">EventArgs </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                     //Hide Division Drop down for  Secondary User.
                    //trDivision.Visible = IsPrimaryUser;
                    //UserIdofTpId = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId })[0].UserId;

                    //// Bind all Dropdowns.
                    //BindData();
                }
            }
            catch (Exception ex)
            {
                // Show exception to User
                lblMessage.Text = ex.Message;
                // Log Exception into Database
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        ///     Event fire when user click save button
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        protected void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                // Check Validation of Validation Controls.
                if (Page.IsValid)
                {
                    // Check Valid Ticket id
                    if (TicketId > 0 || (TicketId == 0 && IsTicketIdNotAvailable))
                    {
                        // Initialize new Dto to send new prospect Request.
                        var prospectDto = new ProspectDto
                                              {
                                                  CaseInfo = new CaseInfoDto(),
                                                  ClientInfo = new ClientInfoDto()
                                              };

                        if (TicketId > 0 && !IsTicketIdNotAvailable)
                            // Set prospect Dto properties.
                            prospectDto = SetProspect();
                        else if (IsTicketIdNotAvailable)
                        {
                            prospectDto = SetProspectWithoutTicketId();
                        }

                        // Check Addition Consultation reuqest for prospect.
                        var identity = _polmController.AddProspect(prospectDto);
                        if (identity > 0)
                        {
                            // Show "succusfully added" message to user.
                            lblMessage.Text = "Record Added Successfully.";

                            // If Ticket Id exisits
                            if (TicketId > 0)
                            {
                                // Add information in Client consultation history
                                _polmController.AddPolmConsultationHistory(TicketId, dd_QuickLegalDescription.SelectedItem.ToString(), TxtLegalMatterDescription.Text,
                                                                          (dd_damagevalue.SelectedValue == "-1") ? "N/A" : dd_damagevalue.SelectedItem.ToString(), (dd_Bodilydamage.SelectedValue == "-1") ? "N/A" : dd_Bodilydamage.SelectedItem.ToString(),
                                                                          Convert.ToInt32(dd_attorney.SelectedValue), identity);
                                // Update Client Consultation Flag to 'Yes'
                                _clsCase.UpdateHasConsultationFlag(TicketId, true);

                            }

                            // Send Email and Save Email in client history and document section.
                            SendAndSaveEmail(identity,
                                             prospectDto.ClientInfo.FirstName + " " +
                                             prospectDto.ClientInfo.LastName, Convert.ToInt32(prospectDto.CaseInfo.Division),
                                             prospectDto.ClientInfo.Address);
                            td_buttons.Style[HtmlTextWriterStyle.Display] = "none";
                        }
                        else
                        {
                            // Show "record not added" message to user.
                            lblMessage.Text = "Record not added successfully.";
                            // Enable Controls which were disable during click events.
                            EnableControls();
                            td_buttons.Style[HtmlTextWriterStyle.Display] = "";
                        }
                    }
                    else
                    {
                        // Show "Ticket ID not found" message to user
                        lblMessage.Text = "Client TicketId not found to Add polm Prospect";
                        // Enable Controls which were disable during click events.
                        EnableControls();
                        td_buttons.Style[HtmlTextWriterStyle.Display] = "";
                    }
                }
            }
            catch (Exception ex)
            {
                // Show exception to user
                lblMessage.Text = ex.Message;
                // Log exception into database
                clsLogger.ErrorLog(ex);
                // Enable all disable controls
                EnableControls();
                td_buttons.Style[HtmlTextWriterStyle.Display] = "";
            }
            finally
            {
                // ModalPopupName is not null or empty then show popup else hide.
                if (!string.IsNullOrEmpty(ModalPopupName))
                {
                    // Check control exists on page or not
                    var modalcontrol = Page.FindControl(ModalPopupName);

                    // If modalpopup exist
                    if (modalcontrol != null)
                    {
                        // Show modal popup
                        ((AjaxControlToolkit.ModalPopupExtender)(Page.FindControl(ModalPopupName))).Show();
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Reset controls to Default value
        /// </summary>
        public void ResetControls()
        {
            lblMessage.Text = string.Empty;
            td_buttons.Style[HtmlTextWriterStyle.Display] = "";
            dd_attorney.SelectedIndex = 0;
            dd_Bodilydamage.SelectedIndex = 0;
            dd_damagevalue.SelectedIndex = 0;
            if (trDivision.Visible)
                dd_Division.SelectedIndex = 1;
            dd_QuickLegalDescription.SelectedIndex = 0;
            TxtLegalMatterDescription.Text = string.Empty;
        }


        /// <summary>
        ///     This method will send polm consultation request email  and save that email in "msg" format in client history and docuement section.
        /// </summary>
        private void SendAndSaveEmail(int identity, string clientFullName, int caseDivsion, string address)
        {
            // Get Email Body

            var body = (TicketId > 0) ? CreateEmailBody(_clsCase, identity, address) : CreateEmailBody(identity);
            var mailBody = CreateAlternateView(body);
            // Mail From
            var mailFrom = _objConfigurationKeys.EmailFrom;
            var mailTo = _objConfigurationKeys.EmailTo;
            //Mail To
            //var mailTo = _objConfigurationKeys.EmailTo;

            //var primaryUsers = _roleBasedSecurityController.GetUserByRole(caseDivsion, PrimaryUserRoleId);
            //var primaryUsersEmail = string.Empty;

            //if (primaryUsers != null && primaryUsers.Count > 0)
            //{
            //    for (int i = 0; i < primaryUsers.Count; i++)
            //    {
            //        primaryUsersEmail += primaryUsers[i].Email + ",";
            //    }
            //}
            //primaryUsersEmail.Replace(';', ',');

            // Email Subject
            var mailSubject = _objConfigurationKeys.EmailSubject.Trim();
            mailSubject = mailSubject.Replace("@Id", Convert.ToString(identity));
            mailSubject = mailSubject.Replace("@Name", clientFullName);
            try
            {

                if (!string.IsNullOrEmpty(mailTo))
                {
                    // Send Email to primary users
                    MailClass.SendMail(mailFrom, mailTo, mailSubject, mailBody, "");

                    // Fill prospectDto properties.

                    _polmController.AddComments(new ProspectDto
                                                    {
                                                        ClientInfo = new ClientInfoDto { ClientId = identity },
                                                        CaseInfo = new CaseInfoDto { SourceId = HoutonSourceId },
                                                        UserComments = "Consultation reuqest email has been sent to " + mailTo,
                                                        InsertedCommentsUserId = Convert.ToString(UserIdofTpId)
                                                    });

                    // if ticket id exists then save email in history
                    if (TicketId > 0)
                        // Save Email in Clients documents and history page
                        SaveFile(mailFrom, mailTo, mailSubject, body);


                }
                else
                {
                    // Show exception error
                    lblMessage.Text =
                        "Consultation request added successfully but email not send because no primary user found to send email";
                }
            }
            catch (Exception ex)
            {
                // Show exception error
                lblMessage.Text = "Consultation request added successfully but email not send because " + ex.Message;
            }
        }

        /// <summary>
        ///     This Method Save consulation email in client history and document page
        /// </summary>
        /// <param name="mailFrom">Email address From</param>
        /// <param name="mailTo">Email Address To</param>
        /// <param name="mailSubject">Email Subject</param>
        /// <param name="mailBody">Email Body</param>
        private void SaveFile(string mailFrom, string mailTo, string mailSubject, string mailBody)
        {
            var fileSaveLocation = ConfigurationManager.AppSettings["NTPATHScanImage"];
            string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
            object[] value1 = { DateTime.Now, "msg", "Consultation request generated and emailed", "Other", 0, EmpId, TicketId, 1, 0, "Upload", "" };
            var clsENationWebComponents = new clsENationWebComponents();
            var picName = clsENationWebComponents.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
            var storeFileName = fileSaveLocation + picName + ".msg";
            SaveoOutlookFile(mailFrom, mailTo, mailSubject, mailBody,
                             storeFileName);
        }

        /// <summary>
        ///     Enable all disable control
        /// </summary>
        private void EnableControls()
        {
            btn_save.Enabled = true;
            dd_QuickLegalDescription.Enabled = true;
            dd_damagevalue.Enabled = true;
            dd_Bodilydamage.Enabled = true;
            dd_attorney.Enabled = true;
            dd_Division.Enabled = true;
            TxtLegalMatterDescription.Enabled = true;
        }

        /// <summary>
        ///     Main Method to fill all dropdown.
        /// </summary>
        public void BindData()
        {
            FillQlmd();
            FillDamageValue();
            FillBodilyDamage();
            FillAttorney();
            if (trDivision.Visible)
                FillDivision();
        }

        /// <summary>
        ///     Fill Quick Legal Matter description dropdown
        /// </summary>
        private void FillQlmd()
        {

            var returnList = _polmController.GetAllQuickLegalMatterDescriptionOfSource(HoutonSourceId);
            dd_QuickLegalDescription.DataSource = returnList;
            dd_QuickLegalDescription.DataBind();
            dd_QuickLegalDescription.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Fill Damage Value dropdown
        /// </summary>
        private void FillDamageValue()
        {
            var returnList = _polmController.GetAllDamageValue(true);
            FillDropdown(dd_damagevalue, returnList);
        }

        /// <summary>
        ///     Fill Bodily Damage dropdown
        /// </summary>
        private void FillBodilyDamage()
        {
            var returnList = _polmController.GetAllBodilyDamageOfDivision(HoutonSourceId);
            dd_Bodilydamage.DataSource = returnList;
            dd_Bodilydamage.DataBind();
            dd_Bodilydamage.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Fill Atotrney dropdown
        /// </summary>
        private void FillAttorney()
        {

            var returnList = _roleBasedSecurityController.GetAllAttorney();

            if (returnList.Count > 0)
            {
                dd_attorney.DataSource = returnList;
                dd_attorney.DataTextField = "FullName";
                dd_attorney.DataValueField = "UserId";
                dd_attorney.DataBind();
            }
            dd_attorney.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Fill Division drop down
        /// </summary>
        private void FillDivision()
        {
            var returnList = _roleBasedSecurityController.GetAllDivisions();
            if (returnList != null && returnList.Count > 0)
            {
                dd_Division.DataSource = returnList;
                dd_Division.DataBind();
            }
            dd_Division.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
            dd_Division.SelectedIndex = 1;
        }

        /// <summary>
        ///     Common method to fill all drop downs
        /// </summary>
        /// <param name="dropDown">DropDownList</param>
        /// <param name="dataSource">List<PolmDto></param>
        private void FillDropdown(DropDownList dropDown, List<PolmDto> dataSource)
        {
            dropDown.DataSource = dataSource;
            dropDown.DataBind();
            dropDown.Items.Insert(0, new ListItem("--- Choose ---", "-1"));
        }

        /// <summary>
        ///     Method to set Prospect Dto
        /// </summary>
        /// <returns>ProspectDto</returns>
        private ProspectDto SetProspect()
        {
            ProspectDto prospectDto = null;

            if (_clsCase.GetClientInfo(TicketId))
            {
                prospectDto = new ProspectDto();
                prospectDto.CaseInfo = new CaseInfoDto();
                prospectDto.ClientInfo = new ClientInfoDto();
                prospectDto.ClientInfo.FirstName = _clsCase.FirstName;
                prospectDto.ClientInfo.MiddleName = _clsCase.MiddleName;
                prospectDto.ClientInfo.LastName = _clsCase.LastName;
                prospectDto.ClientInfo.ContactNumber1 = _clsCase.Contact1;
                prospectDto.ClientInfo.ContactType1 = ConvertToNullableInt(_clsCase.ContactType1);
                prospectDto.ClientInfo.ContactNumber2 = _clsCase.Contact2;
                prospectDto.ClientInfo.ContactType2 = ConvertToNullableInt(_clsCase.ContactType2);
                prospectDto.ClientInfo.ContactType3 = ConvertToNullableInt(_clsCase.ContactType3);
                prospectDto.ClientInfo.ContactNumber3 = _clsCase.Contact3;

                prospectDto.ClientInfo.Language = ConvertToNullableInt(_clsCase.LanguageID);
                prospectDto.ClientInfo.EmailAddress = _clsCase.Email;
                prospectDto.ClientInfo.DlNumber = _clsCase.DLNumber;
                string address = string.IsNullOrEmpty(_clsCase.Address1) ? string.Empty : _clsCase.Address1.Trim() + " "
                    + (string.IsNullOrEmpty(_clsCase.City) ? string.Empty : _clsCase.City.Trim())
                    + " " + (string.IsNullOrEmpty(_clsCase.StateDescription) ? string.Empty : _clsCase.StateDescription)
                    + " " + (string.IsNullOrEmpty(_clsCase.Zip) ? string.Empty : _clsCase.Zip.Trim());
                prospectDto.ClientInfo.Address = address;
                prospectDto.CaseInfo.TicketNumber = _clsCase.TicketNo;

                // Status becomes Pending by Default
                prospectDto.CaseInfo.CaseStatus = 8;
                prospectDto.CaseInfo.QuickLegalMatterDescription = Convert.ToInt32(dd_QuickLegalDescription.SelectedValue);
                prospectDto.CaseInfo.DamageValue = SetDropDownValue(dd_damagevalue);
                prospectDto.CaseInfo.Division = (dd_Division.Visible) ? SetDropDownValue(dd_Division) : 1;
                prospectDto.CaseInfo.QuoteandFeeInformation = null;

                var userInfo = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });
                EmpName = userInfo[0].FullName;
                var legalMatter = Server.HtmlEncode(TxtLegalMatterDescription.Text) + " (" + DateTime.Now + " - " + userInfo[0].Abbreviation + ")";
                prospectDto.CaseInfo.LegalMetterDescription = legalMatter;
                prospectDto.CaseInfo.AssigendAttorney = null;
                prospectDto.ClientInfo.Dob = Convert.ToDateTime(_clsCase.DOB);
                prospectDto.CaseInfo.AssignedDate = null;
                // Source : Houston
                prospectDto.CaseInfo.SourceId = 1;
                prospectDto.CaseInfo.BodilyDamage = SetDropDownValue(dd_Bodilydamage);
                prospectDto.CaseInfo.LastUpdatedBy = null;
                prospectDto.CaseInfo.Followupdate = null;
                prospectDto.CaseInfo.ReferringAttorneyId = SetDropDownValue(dd_attorney);
                prospectDto.CaseInfo.ClosingNoteId = null;
                prospectDto.CaseInfo.ClosedDate = null;
                prospectDto.CaseInfo.LastCaseStatusUpdateDate = null;
                prospectDto.CaseInfo.FiledDate = DateTime.Now;

                int? fileBy = userInfo[0].UserId;
                prospectDto.CaseInfo.FiledBy = fileBy;
            }
            return prospectDto;
        }

        /// <summary>
        ///     Method to set Prospect Dto
        /// </summary>
        /// <returns>ProspectDto</returns>
        private ProspectDto SetProspectWithoutTicketId()
        {
            ProspectDto prospectDto = new ProspectDto();
            prospectDto.CaseInfo = new CaseInfoDto();
            prospectDto.ClientInfo = new ClientInfoDto();
            prospectDto.ClientInfo.FirstName = ClientFirstName.Trim();
            prospectDto.ClientInfo.MiddleName = string.Empty;
            prospectDto.ClientInfo.LastName = ClientLastName.Trim();

            string contact1 = ClientPrimaryContactNumber;
            if (contact1.Contains("-"))
                contact1 = contact1.Replace("-", "");
            if (contact1.ToLower().Contains("x"))
                contact1 = contact1.ToLower().Replace("x", "");
            if (contact1.ToLower().Contains(" "))
                contact1 = contact1.ToLower().Replace(" ", "");
            prospectDto.ClientInfo.ContactNumber1 = contact1.Trim();
            prospectDto.ClientInfo.ContactType1 = ClientContactTypeId;
            prospectDto.ClientInfo.ContactNumber2 = string.Empty;
            prospectDto.ClientInfo.ContactType2 = null;
            prospectDto.ClientInfo.ContactType3 = null;
            prospectDto.ClientInfo.ContactNumber3 = string.Empty;

            prospectDto.ClientInfo.Language = ClientLanguageId;
            prospectDto.ClientInfo.EmailAddress = ClientEmailAddress;
            prospectDto.ClientInfo.DlNumber = string.Empty;
            prospectDto.ClientInfo.Address = ClientAddress;
            prospectDto.CaseInfo.TicketNumber = string.Empty;

            // Status becomes Pending by Default
            prospectDto.CaseInfo.CaseStatus = 8;
            prospectDto.CaseInfo.QuickLegalMatterDescription = Convert.ToInt32(dd_QuickLegalDescription.SelectedValue);
            prospectDto.CaseInfo.DamageValue = SetDropDownValue(dd_damagevalue);
            prospectDto.CaseInfo.Division = (dd_Division.Visible) ? SetDropDownValue(dd_Division) : 1;
            prospectDto.CaseInfo.QuoteandFeeInformation = null;

            var userInfo = _roleBasedSecurityController.GetUserByTpId(new UserDto { TpUserId = EmpId });
            EmpName = userInfo[0].FullName;
            var legalMatter = Server.HtmlEncode(TxtLegalMatterDescription.Text) + " (" + DateTime.Now + " - " + userInfo[0].Abbreviation + ")";
            prospectDto.CaseInfo.LegalMetterDescription = legalMatter;
            prospectDto.CaseInfo.AssigendAttorney = null;
            prospectDto.ClientInfo.Dob = ClientDob;
            prospectDto.CaseInfo.AssignedDate = null;
            // Source : Houston
            prospectDto.CaseInfo.SourceId = 1;
            prospectDto.CaseInfo.BodilyDamage = SetDropDownValue(dd_Bodilydamage);
            prospectDto.CaseInfo.LastUpdatedBy = null;
            prospectDto.CaseInfo.Followupdate = null;
            prospectDto.CaseInfo.ReferringAttorneyId = SetDropDownValue(dd_attorney);
            prospectDto.CaseInfo.ClosingNoteId = null;
            prospectDto.CaseInfo.ClosedDate = null;
            prospectDto.CaseInfo.LastCaseStatusUpdateDate = null;
            prospectDto.CaseInfo.FiledDate = DateTime.Now;

            int? fileBy = userInfo[0].UserId;
            prospectDto.CaseInfo.FiledBy = fileBy;

            return prospectDto;
        }

        /// <summary>
        ///     Common method to convert int to nullable int.
        /// </summary>
        /// <param name="valueToconvert">int</param>
        /// <returns>Nullable Int</returns>
        private int? ConvertToNullableInt(int valueToconvert)
        {
            int? convertedValue = null;
            return (valueToconvert > 0) ? valueToconvert : convertedValue;
        }

        /// <summary>
        ///     Common method to dropdown value to nullable int.
        /// </summary>
        /// <param name="dropdown">DropDownList</param>
        /// <returns>Nullable int</returns>
        private int? SetDropDownValue(DropDownList dropdown)
        {
            if (string.IsNullOrEmpty(dropdown.SelectedValue) || dropdown.SelectedValue == "-1")
                return null;
            return Convert.ToInt32(dropdown.SelectedValue);
        }

        /// <summary>
        ///     Email Body to send Consultation email.
        /// </summary>
        /// <param name="clsCase">_clsCase</param>
        /// <param name="identity">Primary Id of Added prospect</param>
        /// <param name="addresss">Complete address of client</param>
        /// <returns>string</returns>
        private string CreateEmailBody(clsCase clsCase, int identity, string addresss)
        {
            clsGeneralMethods clsGeneralMethods = new clsGeneralMethods();
            var dlNumber = string.IsNullOrEmpty(clsCase.DLNumber) ? "N/A" : clsCase.DLNumber;
            var damageValue = (dd_damagevalue.SelectedValue == "-1" || string.IsNullOrEmpty(dd_damagevalue.SelectedValue)) ? "N/A" : dd_damagevalue.SelectedItem.ToString();
            var bodilyDamageValue = (dd_Bodilydamage.SelectedValue == "-1" || string.IsNullOrEmpty(dd_Bodilydamage.SelectedValue)) ? "N/A" : dd_Bodilydamage.SelectedItem.ToString();
            var refAttorney = (dd_attorney.SelectedValue == "-1" || string.IsNullOrEmpty(dd_attorney.SelectedValue)) ? "N/A" : dd_attorney.SelectedItem.ToString();
            var phoneNumbers = clsGeneralMethods.formatPhone(clsCase.Contact1) + " (" + clsCase.ContactTypeDescription1 + ")"
                + ((!string.IsNullOrEmpty(clsCase.Contact2)) ? "<br />" + clsGeneralMethods.formatPhone(clsCase.Contact2) + " (" + clsCase.ContactTypeDescription2 + ")" : "")
            + ((!string.IsNullOrEmpty(clsCase.Contact3)) ? "<br />" + clsGeneralMethods.formatPhone(clsCase.Contact3) + " (" + clsCase.ContactTypeDescription3 + ")" : "");
            string email = (string.IsNullOrEmpty(clsCase.Email)) ? "N/A" : clsCase.Email;

            string anchorLastName = "<a href='http://" + _objConfigurationKeys.WebisteAddress + "/view/EditPolm.aspx?id=" + Encrypt.CLRClass.EncryptFunction(Convert.ToString(identity)) + "'>" + clsCase.LastName + "</a>";

            var stringBuilder = new StringBuilder();
            stringBuilder.Append("<table width='717px'><tr><td style='width: 35%' align='left'><span class='clssubhead'>Last Name :</span></td><td style='width: 65%' align='left'><span class='clsLeftPaddingTable'>" + anchorLastName + "</span></td></tr><tr><td align='left'><span class='clssubhead'>First Name :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + clsCase.FirstName + "</span></td></tr><tr><td align='left'><span class='clssubhead'>DOB :</span></td>");
            stringBuilder.Append("<td align='left'><span class='clsLeftPaddingTable'>" + clsCase.DOB + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Language Speak :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + clsCase.LanguageSpeak + "</span></td></tr><tr><td align='left' valign='top'><span class='clssubhead'>Contact Numbers :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + phoneNumbers + "</span></td></tr>");

            stringBuilder.Append("<tr><td align='left'><span class='clssubhead'>Address :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + addresss + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Email Address :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + email + "</span></td></tr><tr><td align='left'><span class='clssubhead'>DL # :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + dlNumber + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Case Type :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + dd_QuickLegalDescription.SelectedItem + "</span></td>");
            stringBuilder.Append("</tr><tr><td align='left'><span class='clssubhead'>Damage :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + damageValue + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Bodily Damage :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + bodilyDamageValue + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Legal Matter Description :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + TxtLegalMatterDescription.Text + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Referring Sullo Attorney :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + refAttorney + "</span></td></tr>");
            stringBuilder.Append("<tr><td align='left'><span class='clssubhead'>Sales Rep :</span></td><td align='left'><span class='clsLeftPaddingTable'>" + EmpName + "</span></td></tr></table>");

            var emailBody = new StringBuilder();
            emailBody.Append("<html><head><style type='text/css'>.Label { font-weight: normal; color: #000000; font-style: normal; font-family: Verdana; font-size: 8.5pt; border-style: none; border-width: 0px; } .clsLeftPaddingTable { padding-left: 5px; font-size: 8.5pt; color: #123160; font-family: Verdana; } .SmallLabel { font-weight: normal; color: Gray; font-style: normal; font-family: Verdana; font-size: 7pt; border-style: none; border-width: 0px;} .clssubhead { font-weight: bold; font-size: 8.5pt; color: #3366cc; font-family: Verdana; text-decoration: none; } </style> </head>");
            emailBody.Append("<body style='background-color: #F5F5F5'><table width='717px' align='center' style='vertical-align:middle;'><tr><td><img src=\"cid:emailhead\" alt='' /></td></tr>");
            emailBody.Append("<tr><td align='center' style='width: 717px; background-color: White'><span style='font-family:Verdana'><b>Consultation Request</b></span></td></tr>");
            emailBody.Append("<tr><td align='center' style='width: 717px; height: 305px; background-color: White'><table style='height: 300px;' width='80%'><tr><td style='width: 2px'></td></tr><tr><td align='center' valign='top'>" + stringBuilder + "</td></tr><tr><td style='width: 2px'></td></tr></table></td></tr>");
            emailBody.Append("<tr><td align='left' style='width: 717px; background-color: White'><table width='70%'><tr><td style='width: 100%'><span class='clsLeftPaddingTable'>Enation, LLC, <br /> Tel #: 713.839.9026 <br /> Fax #: 713.523.6634 <br /> <u>http://www.sullolaw.com</u></span></td></tr></table></td></tr>");
            emailBody.Append("<tr><td><img src=\"cid:emailfooter\" alt='' /></td></tr>");
            //emailBody.Append("<tr><td align='center' style='width: 717px'><span class='clsLeftPaddingTable'>This e-mail and the files transmitted with it are the property of Sullo & Sullo LLP and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom this e-mail is addressed. If you are not one of this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer. Any other use, retention, dissemination, forwarding, printing, or copying of this e-mail is strictly prohibited. Copyright© 2007, Sullo & Sullo, LLP All Rights Reserved.</span></td></tr></table></body></html>");
            emailBody.Append("<tr><td align='center' style='width: 717px'><span class='clsLeftPaddingTable'>" + _objConfigurationKeys.EmailFooter + "</span></td></tr></table></body></html>");
            return emailBody.ToString();

        }

        /// <summary>
        ///     Email Body to send Consultation email.
        /// </summary>
        /// <param name="identity">Primary Id of Prospect added</param>
        /// <returns>string</returns>
        private string CreateEmailBody(int identity)
        {
            const string dlNumber = "N/A";
            var damageValue = (dd_damagevalue.SelectedValue == "-1" || string.IsNullOrEmpty(dd_damagevalue.SelectedValue)) ? "N/A" : dd_damagevalue.SelectedItem.ToString();
            var bodilyDamageValue = (dd_Bodilydamage.SelectedValue == "-1" || string.IsNullOrEmpty(dd_Bodilydamage.SelectedValue)) ? "N/A" : dd_Bodilydamage.SelectedItem.ToString();
            var refAttorney = (dd_attorney.SelectedValue == "-1" || string.IsNullOrEmpty(dd_attorney.SelectedValue)) ? "N/A" : dd_attorney.SelectedItem.ToString();
            var phoneNumbers = ClientPrimaryContactNumber + " (Home)";

            string anchorLastName = "<a href='http://" + _objConfigurationKeys.WebisteAddress + "/view/EditPolm.aspx?id=" + Encrypt.CLRClass.EncryptFunction(Convert.ToString(identity)) + "'>" + ClientLastName + "</a>";

            var stringBuilder = new StringBuilder();
            stringBuilder.Append("<table width='717px'><tr><td style='width: 30%' align='left'><span class='clssubhead'>Last Name :</span></td><td style='width: 70%' align='left'><span class='clsLeftPaddingTable'>" + anchorLastName + "</span></td></tr><tr><td align='left'><span class='clssubhead'>First Name :</span></td><td><span class='clsLeftPaddingTable'>" + ClientFirstName + "</span></td></tr><tr><td align='left'><span class='clssubhead'>DOB :</span></td>");
            stringBuilder.Append("<td><span class='clsLeftPaddingTable'>" + ClientDob + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Language Speak :</span></td><td><span class='clsLeftPaddingTable'>ENGLISH</span></td></tr><tr><td align='left' valign='top'><span class='clssubhead'>Contact Numbers :</span></td><td><span class='clsLeftPaddingTable'>" + phoneNumbers + "</span></td></tr>");

            stringBuilder.Append("<tr><td align='left'><span class='clssubhead'>Address :</span></td><td><span class='clsLeftPaddingTable'>" + ClientAddress + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Email Address :</span></td><td><span class='clsLeftPaddingTable'>" + ClientEmailAddress + "</span></td></tr><tr><td align='left'><span class='clssubhead'>DL # :</span></td><td><span class='clsLeftPaddingTable'>" + dlNumber + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Case Type :</span></td><td><span class='clsLeftPaddingTable'>" + dd_QuickLegalDescription.SelectedItem + "</span></td>");
            stringBuilder.Append("</tr><tr><td align='left'><span class='clssubhead'>Damage :</span></td><td><span class='clsLeftPaddingTable'>" + damageValue + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Bodily Damage :</span></td><td><span class='clsLeftPaddingTable'>" + bodilyDamageValue + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Legal Matter Description :</span></td><td><span class='clsLeftPaddingTable'>" + TxtLegalMatterDescription.Text + "</span></td></tr><tr><td align='left'><span class='clssubhead'>Referring Sullo Attorney :</span></td><td><span class='clsLeftPaddingTable'>" + refAttorney + "</span></td></tr>");
            stringBuilder.Append("<tr><td align='left'><span class='clssubhead'>Sales Rep :</span></td><td><span class='clsLeftPaddingTable'>" + EmpName + "</span></td></tr></table>");

            var emailBody = new StringBuilder();
            emailBody.Append("<html><head><style type='text/css'>.Label { font-weight: normal; color: #000000; font-style: normal; font-family: Verdana; font-size: 8.5pt; border-style: none; border-width: 0px; } .clsLeftPaddingTable { padding-left: 5px; font-size: 8.5pt; color: #123160; font-family: Verdana; } .SmallLabel { font-weight: normal; color: Gray; font-style: normal; font-family: Verdana; font-size: 7pt; border-style: none; border-width: 0px;} .clssubhead { font-weight: bold; font-size: 8.5pt; color: #3366cc; font-family: Verdana; text-decoration: none; } </style> </head>");
            emailBody.Append("<body style='background-color: #F5F5F5'><table width='717px' align='center' style='vertical-align:middle;'><tr><td><img src=\"cid:emailhead\" alt='' /></td></tr>");
            emailBody.Append("<tr><td align='center' style='width: 717px; background-color: White'><span><b>Consultation Request</b></span></td></tr>");
            emailBody.Append("<tr><td align='center' style='width: 717px; height: 305px; background-color: White'><table style='height: 300px;' width='80%'><tr><td style='width: 2px'></td></tr><tr><td align='center' valign='top'>" + stringBuilder + "</td></tr><tr><td style='width: 2px'></td></tr></table></td></tr>");
            emailBody.Append("<tr><td align='left' style='width: 717px; background-color: White'><table width='70%'><tr><td style='width: 100%'><span class='clsLeftPaddingTable'>Enation, LLC, <br /> Tel #: 713.839.9026 <br /> Fax #: 713.523.6634 <br /> <u>http://www.sullolaw.com</u></span></td></tr></table></td></tr>");
            emailBody.Append("<tr><td><img src=\"cid:emailfooter\" alt='' /></td></tr>");
            // Rab Nawaz 10914 07/30/2013 Removed Disclimer. . . 
            //emailBody.Append("<tr><td align='center' style='width: 717px'><span class='clsLeftPaddingTable'>This e-mail and the files transmitted with it are the property of Sullo & Sullo LLP and/or its affiliates, are confidential, and are intended solely for use of the individual or entity to whom this e-mail is addressed. If you are not one of this message in error, please notify the sender at 713-839-9026 and delete this message immediately from your computer. Any other use, retention, dissemination, forwarding, printing, or copying of this e-mail is strictly prohibited. Copyright© 2007, Sullo & Sullo, LLP All Rights Reserved.</span></td></tr></table></body></html>");
            emailBody.Append("<tr><td align='center' style='width: 717px'><span class='clsLeftPaddingTable'>" + _objConfigurationKeys.EmailFooter + "</span></td></tr></table></body></html>");
            return emailBody.ToString();
        }

        /// <summary>
        ///     Create Alternative view to send email.(replacing body with alternative view)
        /// </summary>
        /// <param name="body">String</param>
        /// <returns>AlternateView</returns>
        private AlternateView CreateAlternateView(string body)
        {
            var avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            var emailhead = new LinkedResource(Server.MapPath("\\images\\emailhead.JPG"), MediaTypeNames.Image.Jpeg) { ContentId = "emailhead" };
            var emailfooter = new LinkedResource(Server.MapPath("\\images\\emailfooter.JPG"), MediaTypeNames.Image.Jpeg) { ContentId = "emailfooter" };
            avHtml.LinkedResources.Add(emailhead);
            avHtml.LinkedResources.Add(emailfooter);
            return avHtml;
        }

        /// <summary>
        ///     Save email to .mgs fromat in disk
        /// </summary>
        /// <param name="from">string</param>
        /// <param name="to">string</param>
        /// <param name="subject">string</param>
        /// <param name="body">string</param>
        /// <param name="saveLocation">string</param>
        private void SaveoOutlookFile(string from, string to, string subject, string body, string saveLocation)
        {
            var msg = new MailMessage();
            msg.To.Add(to);
            msg.From.Email = from;
            msg.Subject = subject;
            msg.BodyHtmlText = body;
            msg.Attachments.Add(Server.MapPath("\\images\\emailhead.JPG"), "1.gif", "emailhead");
            msg.Attachments.Add(Server.MapPath("\\images\\emailfooter.JPG"), "2.gif", "emailfooter");
            msg.Date = DateTime.Now;
            MailBee.OutlookMsg.MsgConvert.MailMessageToMsg(msg, saveLocation);
        }

        #endregion
    }
}

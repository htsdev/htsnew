﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Cobisi.EmailVerify;
//using EmailVerifierComponent;
using System.Reflection;
using lntechNew.Components.ClientInfo;

namespace HTP.WebControls
{
    //Afaq 7937 06/30/2010
    // Email verification control
    public partial class VerifyEmail : System.Web.UI.UserControl
    {

        private string emailAddressCtrl;
        private string imgFalse;
        private string imgTrue;
        private string imgPleaseWait;
        private string btnCss;
        private string lblCss;
        private int ticketId;
        private bool isEmailVerified;
        private int empId;

        /// <summary>
        /// Containing Email address of client.
        /// </summary>
        public string EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Emp id of the logged in user.
        /// </summary>
        public int EmpId
        {
            get { return Convert.ToInt32(empId); }
            set { empId = value; }
        }
        /// <summary>
        /// Contain email status that it is already verified or not.
        /// </summary>
        public bool IsEmailVerified
        {
            get { return isEmailVerified; }
            set { isEmailVerified = value; }
        }
        /// <summary>
        /// Ticket Id
        /// </summary>
        public int TicketId
        {
            get { return ticketId; }
            set { ticketId = value; }
        }

        /// <summary>
        /// Set/Get css of the verify button
        /// </summary>
        public string BtnCss
        {
            get { return btnCss; }
            set { btnCss = value; }
        }
        /// <summary>
        /// Set/Get css of the please wait label 
        /// </summary>
        public string LblCss
        {
            get { return lblCss; }
            set { lblCss = value; }
        }

        /// <summary>
        /// Set/Get Control id of email address text box.
        /// </summary>
        public string EmailAddressControl
        {
            get { return emailAddressCtrl; }
            set { emailAddressCtrl = value; }
        }
        /// <summary>
        /// Set/Get image url of imageFalse .
        /// </summary>
        public string ImgFalseUrl
        {
            get { return imgFalse; }
            set { imgFalse = value; }
        }
        /// <summary>
        /// Set/Get image url of imageTrue.
        /// </summary>
        public string ImgTrueUrl
        {
            get { return imgTrue; }
            set { imgTrue = value; }
        }
        /// <summary>
        /// Set/Get image url of imagePleaseWait.
        /// </summary>
        public string ImgPleaseWaitUrl
        {
            get { return imgPleaseWait; }
            set { imgPleaseWait = value; }
        }


        /// <summary>
        ///     THis event will fire each time when control load.
        /// </summary>
        /// <param name="sender">Control which raise the event</param>
        /// <param name="e">EventArgs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbtnVerify.CssClass = BtnCss;
                lblWaitText.CssClass = lblCss;
                imageFalse.ImageUrl = ImgFalseUrl;
                imageTrue.ImageUrl = ImgTrueUrl;
                imagePleaseWait.ImageUrl = ImgPleaseWaitUrl;
                hf_emailAddress.Value = EmailAddress;
                TextBox txtEmail = (TextBox)Page.FindControl(EmailAddressControl);
                //if (txtEmail.Text == string.Empty)
                //{
                //    imageTrue.Visible = false;
                //    imageFalse.Visible = false;
                //}
                //else
                //{
                imageTrue.Visible = isEmailVerified;
                imageFalse.Visible = !(isEmailVerified);
                //}
            }
        }

        /// <summary>
        /// Set image after update.
        /// </summary>
        public void SetImage()
        {
            imageTrue.Visible = isEmailVerified;
            imageFalse.Visible = !(isEmailVerified);
        }

        /// <summary>
        /// This method verify the email address using EmailVerifier component.
        /// </summary>
        /// <param name="emailAddress">Email address</param>
        /// <returns></returns>
        public bool emailVerified(string emailAddress)
        {
            //EmailVerifier eVerifier = new EmailVerifier();
            //var result = eVerifier.Verify(emailAddress, VerificationLevel.CatchAll); // Level of verification
            //if (result.IsSuccess)
            //{
            //    imageTrue.Visible = true;
            //    imageFalse.Visible = false;
            //    return true;
            //}
            //else
            //{
            //    imageTrue.Visible = false;
            //    imageFalse.Visible = true;
            //    return false;
            //}
            //EmailVerifier emailVerifier = new EmailVerifier();
            ////Verify the email address return 0 if address verified else false.
            //IResult result = emailVerifier.Verify(emailAddress);
            ////Display the image if email address is false.
            //imageTrue.Visible = (result.Code == 1);
            ////Display the image if email address is true.
            //imageFalse.Visible = !(result.Code == 1);
            //return (result.Code == 1);
            return true;

        }
        /// <summary>
        /// This event will wire on verify button click.
        /// </summary>
        /// <param name="sender">Control which raise the event</param>
        /// <param name="e">EventArgs</param>
        protected void lbtnVerify_Click(object sender, EventArgs e)
        {


            //// Take text box from main page and then cast it into a textbox type variable.
            Control txtEmailCtrl = Page.FindControl(EmailAddressControl);

            if (txtEmailCtrl != null)
            {
                TextBox txtEmail = (TextBox)txtEmailCtrl;

                if (txtEmail.Text != "")
                {
                    if (emailVerified(txtEmail.Text))
                    {
                        clsCase cCase = new clsCase();
                        // Update email status against ticketId.
                        cCase.UpdateEmailVerifiedStatus(TicketId, true);

                        hf_emailAddress.Value = txtEmail.Text;
                        // Add notes in history
                        insertEmailNotes("Email address [" + txtEmail.Text + "] has been verified successfully.");

                    }
                    else
                    {
                        // Add notes in history
                        insertEmailNotes("Email address [" + txtEmail.Text + "] has not been verified successfully.");
                    }
                }

            }




            lbtnVerify.Style["display"] = "";

            imagePleaseWait.Style["display"] = "none";

            lblWaitText.Style["display"] = "none";
        }

        /// <summary>
        /// Add nodes in history on verification of each email.
        /// </summary>
        /// <param name="description">Descripton which will add in history</param>
        private void insertEmailNotes(string description)
        {
            clsLogger objLogger = new clsLogger();
            objLogger.AddNote(empId, description, "", TicketId);
        }
        /// <summary>
        /// Check current status of email
        /// </summary>
        /// <param name="emailAdress"></param>
        /// <returns></returns>
        public bool emailStatus()
        {
            TextBox txtEmail = (TextBox)Page.FindControl(EmailAddressControl);
            if (hf_emailAddress.Value != txtEmail.Text)
            {
                return false;
            }
            else { return true; }

        }
    }
}
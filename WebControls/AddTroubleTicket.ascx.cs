using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using System.IO;
using lntechNew.Components.ClientInfo;
using System.Net.Mail;
using lntechNew.backroom;
using System.Threading;

namespace lntechDallasNew.WebControls
{
    public partial class AddTroubleTicket : System.Web.UI.UserControl
    {
        /// <summary>
        /// i have created this component to generalize our code we can put this user control at any project (11/Dec/2007 - Fahad)
        /// </summary>
                
        Bug AddBug = new Bug();
        lntechNew.Components.Attachment AddAttachment = new lntechNew.Components.Attachment();
        Comments AddComments = new Comments();
        clsSession cSession = new clsSession();
        clsLogger BugTracker = new clsLogger();
        string casenumber = "";
        int attachmentid = 0;
        int Empid = 0;
        bool showmessage = true;



        protected void Page_Load(object sender, EventArgs e)
        {
            casenumber = "";
            txt_pageurl.Text = Request.QueryString["pageurl"];
            Empid = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
            if (!IsPostBack)
            {
                BindControls();
                ViewState["vNTPATHTroubleTicketUploads"] = System.Configuration.ConfigurationSettings.AppSettings["NTPATHTroubleTicketUploads"].ToString();
                ViewState["fileextension"] = System.Configuration.ConfigurationSettings.AppSettings["FileExtension"].ToString();
                ViewState["FileSize"] = System.Configuration.ConfigurationSettings.AppSettings["FileSize"].ToString();
                if (Request.QueryString["casenumber"] != "")
                {

                    if (Request.QueryString["casenumber"].ToString() != "undefined")
                    {
                        if (Request.QueryString["casenumber"].ToString() != "0")
                        {

                            casenumber = Request.QueryString["casenumber"];
                            casenumber = casenumber.Substring(0, casenumber.IndexOf(','));

                            if (Convert.ToInt32(casenumber) > 0)
                            {
                                Display(Convert.ToInt32(casenumber));
                            }
                            
                           
                        }
                    }
                }
            }

      

        }

        protected void btnsubmit_Click1(object sender, EventArgs e)
        {
           
            AddGeneralBug();
        }

        private void Display(int CaseNumber)
        {
            try
            {
                DataSet Ds = AddBug.DisplayBug(CaseNumber);

                string causeno = Ds.Tables[0].Rows[0]["casenumassignedbycourt"].ToString();
                txt_ticketno.Text = Ds.Tables[0].Rows[0]["refcasenumber"].ToString();
                txtCauseNo.Text = causeno;

            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message;
                lblMessage.Visible = true;
            }
        }

        private void BindControls()
        {
            AddBug.FillDropdown(ddl_priority, DropdownType.FillPriority);
            AddBug.FillDropdown(ddl_status, DropdownType.Fillstatus);
        }

        private void AddGeneralBug()
        {
            int bugid = 0;
            string filename = "";

            try
            {

                filename = fp_file.PostedFile.FileName.ToString();
               
                    bugid = AddBug.AddNewBug(txt_shortdesc.Text.ToString().Trim(), Empid, Convert.ToInt32(ddl_status.SelectedValue), Convert.ToInt32(ddl_priority.SelectedValue), txt_pageurl.Text.ToString().Trim(), txt_ticketno.Text.ToString(), txtCauseNo.Text.ToString(), txt_Desc.Text.ToString().Trim());
                    ViewState["bugid"] = Convert.ToString(bugid);

                    if (filename != "")
                    {
                        SaveFile(bugid);
                    }
                    if (showmessage == true)
                    {

                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Your Trouble Ticket Has Been Processed.Thank You'); self.close();</script>");
                        ThreadStart st = new ThreadStart(SendMail);
                        Thread t = new Thread(st);
                        t.IsBackground = true;
                        t.Start();
                        if (t.ThreadState == ThreadState.Stopped)
                        {
                            t.Suspend();
                        }
                        
                        
                    }
              
            }
            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message.ToString();
                lblMessage.Visible = true;
            }
        }

        public void SendMail()
        {
            AddBug.SendOPenTroubleTicketEmail(txt_ticketno.Text.ToString(), txt_pageurl.Text.ToString(), ViewState["bugid"].ToString(), ddl_priority.SelectedItem.Text, txt_shortdesc.Text.ToString(), ddl_status.SelectedItem.Text, txt_pageurl.Text.ToString(), true);
        }

        public void SaveFile(int Bugid)
        {
            string FileName = "";
            int filesize = 0;
            string fName = "";
            string fileextensions = Convert.ToString(ViewState["fileextension"]).ToUpper();
            int filelimit = Convert.ToInt32(ViewState["FileSize"]);
            try
            {
                fName = fp_file.PostedFile.FileName.ToString();
                if (fName != "" && fp_file.PostedFile.ContentLength <= filelimit && (fileextensions.Contains(Path.GetExtension(fName).ToUpper())))
                {
                        filesize = fp_file.PostedFile.ContentLength;
                        if (filesize < 0)
                        {
                            lblMessage.Text = "Uploading of File Failed";
                            lblMessage.Visible = true;
                        }
                        else
                        {
                            FileName = fp_file.PostedFile.FileName.Substring(fp_file.PostedFile.FileName.LastIndexOf("\\") + 1);
                            try
                            {
                                attachmentid = AddAttachment.AddNewAttachment(fp_file.PostedFile.FileName.Substring(fp_file.PostedFile.FileName.LastIndexOf("\\") + 1), txt_filedesc.Text.ToString().Trim(), fp_file.PostedFile.ContentLength, System.DateTime.Today, fp_file.PostedFile.ContentType, Empid, Convert.ToInt32(ViewState["bugid"]));
                                fp_file.PostedFile.SaveAs(ViewState["vNTPATHTroubleTicketUploads"].ToString() + Bugid + "_" + attachmentid + "_" + FileName);
                               
                            }
                            catch (Exception ex)
                            {
                                AddAttachment.RollbackAttachment(attachmentid, Convert.ToInt32(ViewState["bugid"]));
                                HttpContext.Current.Response.Write("<script language='javascript'>alert('File Uploading Failed'); </script>");
                                lblMessage.Text = ex.Message.ToString();

                            }
                        }
                }
                    else
                    {
                        showmessage = false;
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('File Uploading Failed.Please Upload File Of Extensions " + fileextensions + " And File Size Must Be Less Than " + filelimit + " Bytes" + " '); </script>");
                    }
                }

            catch (Exception ex)
            {
                BugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                lblMessage.Text = ex.Message.ToString();
                lblMessage.Visible = true;

            }
        }


    }

}
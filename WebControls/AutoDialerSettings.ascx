﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoDialerSettings.ascx.cs"
    Inherits="HTP.WebControls.AutoDialerSettings" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />

<script language="javascript" type="text/javascript">
    function check()
    {
        alert("hello");
        return true;
    }
</script>

<body>
    <table cellpadding="2" cellspacing="2" align="center">
        <tr>
            <td align="right">
                <asp:Label ID="lbl_CallDays" CssClass="clsLabel" runat="server" Text="Call Days :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_Days" CssClass="clsInputadministration" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lbl_CallDaysBusinessDays" CssClass="clsLabel" runat="server" Text="Business Days"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_Courts" runat="server" CssClass="clsLabel" Text="Courts :"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="ddl_Courts" CssClass="clsInputadministration" runat="server">
                    <asp:ListItem Value="0">All Courts</asp:ListItem>
                    <asp:ListItem Value="1">HMC Courts</asp:ListItem>
                    <asp:ListItem Value="2">Non HMC Courts</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_Range" runat="server" CssClass="clsLabel" Text="Setting Date Range :"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_Range" CssClass="clsInputadministration" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem Selected="True">3</asp:ListItem>
                    <asp:ListItem Value="4"></asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lbl_RangeBusinessDays" CssClass="clsLabel" runat="server" Text="Business Days"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_EventState" CssClass="clsLabel" runat="server" Text="Event State :"></asp:Label>
            </td>
            <td colspan="2">
                <asp:RadioButton ID="rb_Start" CssClass="clsLabelNew" runat="server" GroupName="state"
                    Text="Start" />
                <asp:RadioButton ID="rb_Stop" CssClass="clsLabelNew" runat="server" GroupName="state"
                    Text="Stop" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_DialTime" CssClass="clsLabel" runat="server" Text="Dial Time :"></asp:Label>
            </td>
            <td colspan="2">
                <ew:TimePicker runat="server" CssClass="clsLabelNew" ID="tp_DialTime" ImageUrl="~/images/calendar.gif"
                    ControlDisplay="LabelImage" Scrollable="True" NumberOfColumns="4" PopupWidth="250px"
                    PopupHeight="152px" MinuteInterval="15" SelectedTime="10:30 AM">
                    <TimeStyle BackColor="LightSteelBlue" ForeColor="AliceBlue" />
                    <SelectedTimeStyle BackColor="Navy" ForeColor="AliceBlue" />
                </ew:TimePicker>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
                <asp:Button ID="btn_Update" CssClass="clsbutton" runat="server" Text="Update" />
            </td>
        </tr>
    </table>
</body>

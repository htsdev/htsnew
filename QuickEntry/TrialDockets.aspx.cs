using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using System.Data.SqlClient;
using lntechNew.Components;
using System.Configuration;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;

namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for TrialDockets.
	/// </summary>
	public partial class TrialDockets : System.Web.UI.Page
	{
		clsENationWebComponents ClsDb = new clsENationWebComponents();
		protected System.Web.UI.WebControls.DropDownList DDLCourts;
		protected eWorld.UI.CalendarPopup dtpCourtDate;
		protected System.Web.UI.WebControls.TextBox TxtCourtNo;
		protected System.Web.UI.WebControls.DropDownList DropDownList2;
		protected System.Web.UI.WebControls.ListBox LstCaseStatus;
		protected System.Web.UI.WebControls.Table Table_OutPut;
		clsCourts ClsCourt = new clsCourts();
		protected System.Web.UI.WebControls.Button Button_Submit;
		clsSession ClsSession=new clsSession();
		protected System.Web.UI.WebControls.Label lblRecStatus;
		protected System.Web.UI.WebControls.ImageButton imgPrint_Ds_to_Printer;
		protected System.Web.UI.WebControls.ImageButton imgExp_Ds_to_Excel;
		clsCrsytalComponent Cr = new clsCrsytalComponent();
		clsLogger bugTracker = new clsLogger();
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Check for valid Session if not then redirect to login page	
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx");
				}
				ClsSession.SetSessionVariable("sMoved","False",this.Session);
				if(!Page.IsPostBack )
				{
                    //network path
                    ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHDocketPDF"].ToString();
                    ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH1"].ToString();
                    //
                    fillCourts();
					FillGrid();
				}
			}
			catch(Exception ex)
			{
				if (ex.Message != "Cannot find table 0.")
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
				this.lblRecStatus .Visible =true;
				this.lblRecStatus .Text ="Record Not Found";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button_Submit.Click += new System.EventHandler(this.Button_Submit_Click);
			this.imgPrint_Ds_to_Printer.Click += new System.Web.UI.ImageClickEventHandler(this.imgPrint_Ds_to_Printer_Click);
			this.imgExp_Ds_to_Excel.Click += new System.Web.UI.ImageClickEventHandler(this.imgExp_Ds_to_Excel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		DataSet Ds_FillTable;
		private void FillGrid()
		{
			//			string[] key    = {"@courtloc","@courtdate","@page","@courtnumber","@datetype"};
			//			object[] value1 = {DDLCourts.SelectedValue.ToString(),dtpCourtDate.SelectedDate,"1",TxtCourtNo.Text,LstCaseStatus.SelectedValue.ToString()};
			//			DataSet Ds_FillReport = ClsDb.Get_DS_BySPArr("dbo.usp_hts_get_trial_docket_report_new",key,value1);
			FillTable ();
			this.imgExp_Ds_to_Excel .Visible =true;
			this.imgPrint_Ds_to_Printer .Visible =true;
            this.img_pdf.Visible = true;
            this.img_PdfAll.Visible = true;
		}
		private void Button_Submit_Click(object sender, System.EventArgs e)
		{
			try
			{
				FillGrid();
			}
			catch(Exception ex)
			{
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		private void fillCourts()
		{
			try
			{ 
                //Change by Ajmal
                //SqlDataReader DrCourt = ClsCourt.GetAllCourtName();
				IDataReader DrCourt = ClsCourt.GetAllCourtName();
				DDLCourts.DataSource =DrCourt;
				DDLCourts.DataTextField = "shortcourtname";
				DDLCourts.DataValueField = "courtid";
				DDLCourts.DataBind();
				ListItem lstNewItem = new ListItem("All Locations","None");
				DDLCourts.Items.Insert(1,lstNewItem);	
				
				ListItem lstNewItem1 = new ListItem("All Inside Courts","OU");
				
				DDLCourts.Items.Insert(2,lstNewItem1);	


				ListItem lstNewItem2 = new ListItem("All Outsite Courts","JP");
				DDLCourts.Items.Insert(3,lstNewItem2);	


				DDLCourts .Items [1].Selected =true;
				this.dtpCourtDate .SelectedDate =DateTime .Now.AddDays (1);
			}
			catch(Exception ex)
			{
				this.lblRecStatus .Visible =true;
				this.lblRecStatus .Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void imgExp_Ds_to_Excel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			DataSet Exp_DataSet=(DataSet)   Session["PassDG"];
			if(Exp_DataSet !=null)
			{
				Session["ds"]=Exp_DataSet; 
				Response.Redirect ("FrmTableExportToExcel.aspx");
			}
			
		}
		private void imgPrint_Ds_to_Printer_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			DataSet expDataSet=(DataSet) Session["PassDG"];
			if(expDataSet!=null)
			{
				Response.Redirect ("PrintTrialDocket.aspx?courtloc="+DDLCourts.SelectedValue.ToString()+"&courtdate="+dtpCourtDate.SelectedDate.ToShortDateString ()+"&page="+"1"+"&courtnumber="+TxtCourtNo.Text+"&datetype="+LstCaseStatus.SelectedValue.ToString()+"&RecdType="+0);            
			}
			else
			{
				Response.Write ("No DataSet Found");
			}
		}		
		private void FillTable()
		{	
			try
			{
				int VarChk = 0;
				Table_OutPut .CellPadding=0;
				Table_OutPut .CellSpacing=0;	
							
				string[] key    = {"@courtloc","@courtdate","@page","@courtnumber","@datetype"};
				object[] value1 = {DDLCourts.SelectedValue.ToString(),dtpCourtDate.SelectedDate,"1",TxtCourtNo.Text,LstCaseStatus.SelectedValue.ToString()};
				Ds_FillTable = ClsDb.Get_DS_BySPArr("usp_hts_get_trial_docket_report_new",key,value1);
				Session["PassDG"]=Ds_FillTable ;
                string[] courts;
	            //ozair code
                if (Ds_FillTable.Tables[2].Rows.Count > 0)
                {
                    string courtloc=string.Empty;
                    for (int i = 0; i < Ds_FillTable.Tables[2].Rows.Count; i++)
                    {
                        if (courtloc.Length == 0)
                        {
                            courtloc = Ds_FillTable.Tables[2].Rows[i]["currentcourtloc"].ToString();
                        }
                        else
                        {
                            courtloc = courtloc + "," +Ds_FillTable.Tables[2].Rows[i]["currentcourtloc"].ToString();
                        }
                    }
                    courts = courtloc.Split(',');
                    ViewState["vCourts"] = courts;
                } //ozair code end
                // Code for CourtNumber -- Sarim
                if (Ds_FillTable.Tables[3].Rows.Count > 0)
                {
                    string courtloc = string.Empty;
                    for (int i = 0; i < Ds_FillTable.Tables[3].Rows.Count; i++)
                    {
                        if (courtloc.Length == 0)
                        {
                            courtloc = Ds_FillTable.Tables[3].Rows[i]["currentcourtloc"].ToString();
                            courtloc = courtloc + "|" + Ds_FillTable.Tables[3].Rows[i]["currentcourtnum"].ToString();
                        }
                        else
                        {
                            courtloc = courtloc + "," + Ds_FillTable.Tables[3].Rows[i]["currentcourtloc"].ToString();
                            courtloc = courtloc + "|" + Ds_FillTable.Tables[3].Rows[i]["currentcourtnum"].ToString();
                        }
                    }
                    courts = courtloc.Split(',');
                    ViewState["vCourtNum"] = courts;
                }
		       //Sarim Code End

				//Ds_FillTable= formatDataSet(Ds_FillTable);
				int serialNo=0;
				
				if(Ds_FillTable.Tables [0].Rows .Count !=0)
				{
					this.lblRecStatus .Visible =false;
					//Inserting First Court Header
					LeaveOneRow();
					#region
					TableRow Firstrowspan=new TableRow ();
					Firstrowspan.BorderStyle =BorderStyle.None;
					
					TableCell Cell_CourtName_FirstRec=new TableCell ();
					Cell_CourtName_FirstRec.ColumnSpan =11;
					Cell_CourtName_FirstRec.BorderStyle =BorderStyle.None;
					Cell_CourtName_FirstRec.HorizontalAlign=HorizontalAlign.Left ;
					Cell_CourtName_FirstRec.Font.Bold=true;
					Cell_CourtName_FirstRec.Font.Underline =true;
					Cell_CourtName_FirstRec.Font .Size =8;

					Cell_CourtName_FirstRec.Text=Ds_FillTable.Tables[0].Rows[0]["CourtName_Address_DateSet"].ToString ();
					Firstrowspan.Cells .Add(Cell_CourtName_FirstRec);
					Table_OutPut .Rows .Add (Firstrowspan );
					LeaveOneRow();
	
					#endregion
					//Finish Inserting First CourtHeader

					//Insertint First Record
					#region
					string caseNoFirst = Ds_FillTable.Tables[0].Rows[0]["casenumber"].ToString();
                
					TableRow rowInsertFirst =new TableRow ();
					rowInsertFirst.Font .Size =7;
					
					rowInsertFirst.BorderColor=Color.DarkBlue;
					rowInsertFirst.ForeColor =Color.Black;
					rowInsertFirst.BorderStyle=BorderStyle.Double;

					serialNo =1;
					TableCell cell_SerialNoFirst=new TableCell ();
					cell_SerialNoFirst.BorderColor=Color.DarkBlue;
					cell_SerialNoFirst.ForeColor =Color.Black;
                    cell_SerialNoFirst .Height =10;
					//cell_SerialNoFirst .BorderStyle =BorderStyle.Double; 
 
					cell_SerialNoFirst .ColumnSpan =0;
					cell_SerialNoFirst .Width=10;
					cell_SerialNoFirst .Wrap =true;
				
					cell_SerialNoFirst.Font.Size =6; 
					cell_SerialNoFirst.Text="<Font Face=Tahoma>"+"<a href=../ClientInfo/ViolationFeeold.aspx?casenumber="+caseNoFirst+"&search=0>"+serialNo+"</a>";
					//cell_SerialNoFirst.Text=""+serialNo;  
					rowInsertFirst.Cells.Add(cell_SerialNoFirst);

				    
						
					
					TableCell cell_ClientCount_PretrialStatusFirst=new TableCell ();
					cell_ClientCount_PretrialStatusFirst.BorderColor=Color.DarkBlue ;
					cell_ClientCount_PretrialStatusFirst.ForeColor =Color.Black;
					cell_ClientCount_PretrialStatusFirst .Font .Size =7;
					cell_ClientCount_PretrialStatusFirst .Width =130;
					cell_ClientCount_PretrialStatusFirst .Height =10;
					cell_ClientCount_PretrialStatusFirst .Wrap=true;
					cell_ClientCount_PretrialStatusFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
					rowInsertFirst.Cells.Add(cell_ClientCount_PretrialStatusFirst);

					//					TableCell cell_Blank=new TableCell ();
					//					cell_Blank.BorderColor=Color.DarkBlue ;
					//					cell_Blank.ForeColor =Color.Black;
					//					cell_Blank .Font .Size =7;
					//					cell_Blank .Width =20;
					//					cell_Blank .Height =10;
					//					cell_Blank .Wrap=true;
					//					cell_Blank.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["Space"].ToString();
					//					rowInsertFirst.Cells.Add(cell_Blank);
					//				
					TableCell cell_violationcount_lastnameFirst=new TableCell ();
					cell_violationcount_lastnameFirst.BorderColor=Color.DarkBlue ;
					cell_violationcount_lastnameFirst.ForeColor =Color.Black;
					cell_violationcount_lastnameFirst.Font .Size =7;
					cell_violationcount_lastnameFirst .Width =110;
					cell_violationcount_lastnameFirst .Height =10;
					cell_violationcount_lastnameFirst .Wrap=true;
					cell_violationcount_lastnameFirst .Font .Name ="Arial";
					cell_violationcount_lastnameFirst.Text="&nbsp;<Font Face=Arial>"+Ds_FillTable.Tables[0].Rows[0]["violationcount"].ToString()+
						//"<a onclick ='OpenPopUp(trialNotes.aspx," + )' trialNotes.aspx?casenumber="+ Ds_FillTable.Tables[0].Rows[0]["casenumber"].ToString()+">"+Ds_FillTable.Tables[0].Rows[0]["Client_lastname"].ToString()+"</a>";
						"<a href=\"#\" onClick=OpenPopUp('trialNotes.aspx?casenumber="+caseNoFirst+"','casenumber=" + caseNoFirst+ "');>"+Ds_FillTable.Tables[0].Rows[0]["Client_lastname"].ToString()+"</a></font>";
                    //"<a href=\"../ClientInfo/CaseDisposition.aspx?sMenu=65&search=0&casenumber=" + caseNoFirst + "\">" + Ds_FillTable.Tables[0].Rows[0]["Client_lastname"].ToString() + "</a></font>";
					rowInsertFirst.Cells.Add(cell_violationcount_lastnameFirst);


					TableCell cell_FirstName_MiddleNameFirst=new TableCell ();
					cell_FirstName_MiddleNameFirst.BorderColor=Color.DarkBlue ;
					cell_FirstName_MiddleNameFirst.ForeColor =Color.Black;
					cell_FirstName_MiddleNameFirst.Font .Size =7;
					cell_FirstName_MiddleNameFirst .Width =108;
					cell_FirstName_MiddleNameFirst .Height =10;
					cell_FirstName_MiddleNameFirst .Wrap=true;
					cell_FirstName_MiddleNameFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["FMName"].ToString();
					rowInsertFirst.Cells.Add(cell_FirstName_MiddleNameFirst);
					
					//First Row Second Grid 	
					TableCell cell_OffenceDateFirst =new TableCell ();
					cell_OffenceDateFirst.BorderColor=Color.DarkBlue ;
					cell_OffenceDateFirst.ForeColor =Color.Black;
					cell_OffenceDateFirst.Font .Size =7;
					cell_OffenceDateFirst .Width =60;
					cell_OffenceDateFirst .Height =10;
					cell_OffenceDateFirst .Wrap=true;
					cell_OffenceDateFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["offenceDate"].ToString();
					rowInsertFirst.Cells.Add(cell_OffenceDateFirst);

					
					TableCell cell_BondingFirst=new TableCell ();
					cell_BondingFirst.BorderColor=Color.DarkBlue ;
					cell_BondingFirst.ForeColor =Color.Black;
					cell_BondingFirst.Font .Size =7;
					cell_BondingFirst .Font.Bold =true;
					cell_BondingFirst .Wrap=true;
					cell_BondingFirst .Width =50;
					cell_BondingFirst .Height =10; 
					cell_BondingFirst.Text="&nbsp;"+Ds_FillTable .Tables[0].Rows[0]["BondFlag"].ToString();
					rowInsertFirst.Cells.Add(cell_BondingFirst);;

					TableCell cell_CourtTimeFirst=new TableCell ();
					cell_CourtTimeFirst.BorderColor=Color.DarkBlue ;
					cell_CourtTimeFirst.ForeColor =Color.Black;
					cell_CourtTimeFirst.Font .Size =7;
					cell_CourtTimeFirst.Width=75;
					cell_CourtTimeFirst .Height =10;
					//cell_CourtTimeFirst .Wrap=true;
                    cell_CourtTimeFirst.Wrap = false;
					cell_CourtTimeFirst.Font .Bold =true;
					cell_CourtTimeFirst.Text=Ds_FillTable.Tables[0].Rows[0]["currentdateset"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtTimeFirst);

					
					TableCell cell_CourtNumFirst=new TableCell ();
					cell_CourtNumFirst.BorderColor=Color.DarkBlue ;
					cell_CourtNumFirst.ForeColor =Color.Black;
					cell_CourtNumFirst.Font .Size =7;
					cell_CourtNumFirst.Font .Bold =true;
					cell_CourtNumFirst .Width =23;
					cell_CourtNumFirst .Height =10;
					cell_CourtNumFirst .Wrap=true;
					cell_CourtNumFirst.Text="&nbsp;"+"#"+Ds_FillTable.Tables[0].Rows[0]["currentcourtnum"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtNumFirst);
						

					TableCell cell_Trial_DescriptionFirst=new TableCell ();
					cell_Trial_DescriptionFirst.BorderColor=Color.DarkBlue ;
					cell_Trial_DescriptionFirst.ForeColor =Color.Black;
					cell_Trial_DescriptionFirst.Font .Size =7;
					cell_Trial_DescriptionFirst.Font .Bold =true;
					cell_Trial_DescriptionFirst .Wrap =true;
					cell_Trial_DescriptionFirst .Height =10;
					cell_Trial_DescriptionFirst .Width =32;
					cell_Trial_DescriptionFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["Description"].ToString();
					rowInsertFirst.Cells.Add(cell_Trial_DescriptionFirst );


					TableCell cell_OFirstName_LastNameFirst=new TableCell ();            					      
					cell_OFirstName_LastNameFirst.BorderColor=Color.DarkBlue ;
					cell_OFirstName_LastNameFirst.ForeColor =Color.Black;
					cell_OFirstName_LastNameFirst.Font .Size =7;
					cell_OFirstName_LastNameFirst .Wrap=true;
					cell_OFirstName_LastNameFirst .Width =90;
					cell_OFirstName_LastNameFirst .Height =10;
					cell_OFirstName_LastNameFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["OLFname"].ToString();
					rowInsertFirst.Cells.Add(cell_OFirstName_LastNameFirst);

					// First Row --- First Grid
					TableCell cell_MidNumFirst=new TableCell ();            					      
					cell_MidNumFirst.BorderColor=Color.DarkBlue ;
					cell_MidNumFirst.ForeColor =Color.Black;
					cell_MidNumFirst.Font .Size =7;
					cell_MidNumFirst.Wrap=true;
					cell_MidNumFirst.Width =80;
					cell_MidNumFirst.Height =10;
					cell_MidNumFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["midnum"].ToString();
					rowInsertFirst.Cells.Add(cell_MidNumFirst);

					
					TableCell cell_TrialCommentsFirst=new TableCell ();
					cell_TrialCommentsFirst.BorderColor=Color.DarkBlue ;
					cell_TrialCommentsFirst.ForeColor =Color.Black;
					cell_TrialCommentsFirst .Width =152;
					cell_TrialCommentsFirst .Height =10;
					cell_TrialCommentsFirst .Wrap =true;
					cell_TrialCommentsFirst.Font .Size =7;
					cell_TrialCommentsFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["Shortdesc1"].ToString()+
						"<Font Color=Red>"+Ds_FillTable.Tables[0].Rows[0]["trialcomments"].ToString()+"</Font>" ;
					rowInsertFirst.Cells.Add(cell_TrialCommentsFirst);
					Table_OutPut.Rows.Add(rowInsertFirst);
					
					#endregion
					//Finish Adding First Record

					#region start
					for (int IdxRow=1;IdxRow<=Ds_FillTable.Tables [0].Rows .Count-1 ;IdxRow++)
					{
					
						if(Ds_FillTable.Tables[0].Rows[IdxRow][10].ToString()!=Ds_FillTable.Tables[0].Rows[IdxRow-1][10].ToString())
						{	
							LeaveOneRow();
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtName=new TableCell ();
							Cell_CourtName .Font .Size =8;
							Cell_CourtName.Font .Bold =true;
							Cell_CourtName.Font .Underline=true;
							Cell_CourtName.BorderStyle =BorderStyle.None ;
							Cell_CourtName.ColumnSpan=11;
							Cell_CourtName .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtName_FirstRec.BorderStyle=BorderStyle.None; 			
							Cell_CourtName.Text=Ds_FillTable.Tables [0].Rows[IdxRow][10].ToString ();                            
							rowspan.Cells .Add(Cell_CourtName);
							Table_OutPut .Rows .Add(rowspan);		               
							LeaveOneRow();			
						}                                 
						if(Ds_FillTable.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString()!=Ds_FillTable.Tables[0].Rows[IdxRow-1]["currentcourtnum"].ToString()
							&&Ds_FillTable.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString()!=null 
							&& Ds_FillTable.Tables[0].Rows[IdxRow-1]["currentcourtnum"].ToString()!=null
							&& Ds_FillTable.Tables[0].Rows[IdxRow-1]["CourtName_Address_DateSet"].ToString() == Ds_FillTable.Tables[0].Rows[IdxRow]["CourtName_Address_DateSet"].ToString())
						{	
							
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtNum=new TableCell ();
							Cell_CourtNum .Font .Size =8;
							Cell_CourtNum.BorderColor=Color.DarkBlue ;
							Cell_CourtNum.ForeColor =Color.Black;						
							Cell_CourtNum .ColumnSpan=12;
							Cell_CourtNum .Height =10;
							cell_CourtNumFirst .BorderStyle =BorderStyle.NotSet ;
							Cell_CourtNum  .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtNum  .Text="<br>";
							rowspan.Cells .Add(	Cell_CourtNum);
							Table_OutPut .Rows .Add(rowspan);		               
							
			
						}


						TableRow rowInsert=new TableRow ();
						rowInsert .BorderStyle =BorderStyle.Solid; 
						rowInsert .Font .Size=6; 
						serialNo =IdxRow+1;
						
				
						TableCell cell_SerialNo=new TableCell ();
						string caseNo= Ds_FillTable.Tables[0].Rows[IdxRow]["casenumber"].ToString();
						cell_SerialNo.BorderColor=Color.DarkBlue ;
						cell_SerialNo.ForeColor =Color.Black;
						cell_SerialNo.Width=10;
						cell_SerialNo .Height =10;
						cell_SerialNo.Wrap =true;
						cell_SerialNo.Font .Size =6;
						cell_SerialNo.Text="<Font Face=Tahoma><a href=../ClientInfo/ViolationFeeold.aspx?casenumber="+caseNo+"&search=0>"+" "+serialNo+"</a></font>";
						rowInsert.Cells.Add(cell_SerialNo);

						TableCell cell_ClientCount_PretrialStatus=new TableCell ();
						cell_ClientCount_PretrialStatus.BorderColor=Color.DarkBlue ;
						cell_ClientCount_PretrialStatus.ForeColor =Color.Black;
						cell_ClientCount_PretrialStatus .Width =20;
						cell_ClientCount_PretrialStatus.Height =10;
						cell_ClientCount_PretrialStatus .Wrap =true;
						cell_ClientCount_PretrialStatus.Font .Size =7;
						cell_ClientCount_PretrialStatus.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
						rowInsert.Cells.Add(cell_ClientCount_PretrialStatus);
						//
						//						TableCell cell_Blank1=new TableCell ();
						//						cell_Blank1.BorderColor=Color.DarkBlue ;
						//						cell_Blank1.ForeColor =Color.Black;
						//						cell_Blank1 .Font .Size =7;
						//						cell_Blank1 .Width =20;
						//						cell_Blank1 .Height =10;
						//						cell_Blank1 .Wrap=true;
						//						cell_Blank1.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["Space"].ToString();
						//						rowInsert.Cells.Add(cell_Blank1);

				
						
						//TableCell cell15=new TableCell();
						TableCell cell_violationcount_lastname=new TableCell ();
						cell_violationcount_lastname.BorderColor=Color.DarkBlue ;
						cell_violationcount_lastname.ForeColor =Color.Black;
						cell_violationcount_lastname.Font .Size =7;
						cell_violationcount_lastname .Width =110;
						cell_violationcount_lastname .Height =10;
						cell_violationcount_lastname .Wrap =true;
						//cell_violationcount_lastname.BorderStyle =BorderStyle.Double;
						string caseNo1 =Ds_FillTable.Tables[0].Rows[IdxRow]["casenumber"].ToString();
						cell_violationcount_lastname .Font .Name ="Arial";
						cell_violationcount_lastname.Text="<Font Face=Arial>"+Ds_FillTable.Tables[0].Rows[IdxRow]["violationcount"].ToString()+
							"<a href=\"#\" onClick=OpenPopUp('trialNotes.aspx?casenumber="+caseNo1+"','casenumber=" + caseNo1  + "');>"+Ds_FillTable.Tables[0].Rows[IdxRow]["Client_lastname"].ToString()+"</a></font>";
                            //"<a href=\"../ClientInfo/CaseDisposition.aspx?sMenu=65&search=0&casenumber=" + caseNo1 + "\">" + Ds_FillTable.Tables[0].Rows[IdxRow]["Client_lastname"].ToString() + "</a></font>";
						rowInsert.Cells.Add(cell_violationcount_lastname);
					

						TableCell cell_FirstName_MiddleName=new TableCell ();
						cell_FirstName_MiddleName.BorderColor=Color.DarkBlue ;
						cell_FirstName_MiddleName.ForeColor =Color.Black;
						cell_FirstName_MiddleName.Font .Size =7;
						cell_FirstName_MiddleName .Height =10;
						cell_FirstName_MiddleName .Width=108;
						cell_FirstName_MiddleName .Wrap =true;
						cell_FirstName_MiddleName.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["FMName"].ToString();
						rowInsert.Cells.Add(cell_FirstName_MiddleName);
					
						// inner Rows -- Second Grid
						TableCell cell_OffenceDate =new TableCell ();
						cell_OffenceDate.BorderColor=Color.DarkBlue ;
						cell_OffenceDate.ForeColor =Color.Black;
						cell_OffenceDate.Font .Size =7;
						cell_OffenceDate .Width =70;
						cell_OffenceDate .Height =10;
						cell_OffenceDate .Wrap=true;
						cell_OffenceDate.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["offenceDate"].ToString();
						rowInsert.Cells.Add(cell_OffenceDate);
						
						
						TableCell cell_Bonding=new TableCell ();
						cell_Bonding.BorderColor=Color.DarkBlue ;
						cell_Bonding.ForeColor =Color.Black;
						cell_Bonding.Font .Size =7;
						cell_Bonding .Width =50;
						cell_Bonding .Wrap =true;
						cell_Bonding .Height =10;
						cell_Bonding .Font .Bold =true;
						cell_Bonding.Text="&nbsp;"+Ds_FillTable .Tables[0].Rows[IdxRow]["BondFlag"].ToString();
						rowInsert.Cells.Add(cell_Bonding);
					
					

					
						TableCell cell_CourtTime=new TableCell ();
						cell_CourtTime.BorderColor=Color.DarkBlue ;
						cell_CourtTime.ForeColor =Color.Black;
						cell_CourtTime.Font .Size =7;
						cell_CourtTime.Width=75;
						cell_CourtTime .Height =10;
						//cell_CourtTime .Wrap =true;
                        cell_CourtTime.Wrap = false;
						cell_CourtTime .Font .Bold =true;					
						cell_CourtTime.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["currentdateset"].ToString();
						rowInsert.Cells.Add(cell_CourtTime);
					
					

						
						TableCell cell_CourtNum=new TableCell ();
						cell_CourtNum.BorderColor=Color.DarkBlue ;
						cell_CourtNum.ForeColor =Color.Black;	
						cell_CourtNum.Font .Size =7;
						cell_CourtNum .Width =23;
						cell_CourtNum .Wrap=true;
						cell_CourtNum .Height =10;
						cell_CourtNum.Font .Bold =true;
						cell_CourtNum.Text="&nbsp;"+"#"+Ds_FillTable.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString();
						rowInsert.Cells.Add(cell_CourtNum);
					
						
						TableCell cell_Trial_Description=new TableCell ();
						cell_Trial_Description.BorderColor=Color.DarkBlue ;
						cell_Trial_Description.ForeColor =Color.Black;	
						cell_Trial_Description.Font .Bold =true;
						cell_Trial_Description .Width =32;
						cell_Trial_Description .Wrap =true;
						cell_Trial_Description .Height =10;
						cell_Trial_Description.Font .Size =7;
						cell_Trial_Description.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["Description"].ToString();
						rowInsert.Cells.Add(cell_Trial_Description );
					

					
						TableCell cell_OFirstName_LastName=new TableCell ();            					      
						cell_OFirstName_LastName.BorderColor=Color.DarkBlue ;
						cell_OFirstName_LastName.ForeColor =Color.Black;
						cell_OFirstName_LastName.Font .Size =7;										
						cell_OFirstName_LastName .Width =90;
						cell_OFirstName_LastName .Height =10;
						cell_OFirstName_LastName .Wrap =true;
						cell_OFirstName_LastName.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["OLFname"].ToString();
						rowInsert.Cells.Add(cell_OFirstName_LastName);
					
						// Inner Row --- first Grid. 
						TableCell cell_midnum_First =new TableCell ();            					      
						cell_midnum_First.BorderColor=Color.DarkBlue ;
						cell_midnum_First.ForeColor =Color.Black;
						cell_midnum_First.Font .Size =7;										
						cell_midnum_First .Width =80;
						cell_midnum_First .Height =10;
						cell_midnum_First .Wrap =true;
						cell_midnum_First.Text=Ds_FillTable.Tables[0] .Rows[IdxRow]["midnum"].ToString();
						rowInsert.Cells.Add(cell_midnum_First);

						
						TableCell cell22=new TableCell();
						TableCell cell_TrialComments=new TableCell ();
						cell_TrialComments.BorderColor=Color.DarkBlue ;
						cell_TrialComments.ForeColor =Color.Black;
						cell_TrialComments.Font .Size =7;
						cell_TrialComments .Width =152;
						cell_TrialComments .Height =10;
						cell_TrialComments .Wrap =true;
						cell_TrialComments.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["Shortdesc1"].ToString()+
							"<Font Color=Red>"+Ds_FillTable.Tables[0].Rows[IdxRow]["trialcomments"].ToString()+"</Font>" ;;
						rowInsert.Cells.Add(cell_TrialComments);
						Table_OutPut.Rows.Add(rowInsert);		
					}
					#endregion
				}
				
				if(Ds_FillTable.Tables[1] .Rows .Count !=0)
				{
					LeaveOneRow();
					this.lblRecStatus .Visible =false;
					this.imgExp_Ds_to_Excel .Visible =true;
					this.imgPrint_Ds_to_Printer .Visible =true;
					//Inserting First Court Header
					LeaveOneRow();
					#region
					TableRow Firstrowspan=new TableRow ();
					Firstrowspan.BorderStyle =BorderStyle.None;  
					TableCell Cell_CourtName_FirstRec=new TableCell ();
					Cell_CourtName_FirstRec.ColumnSpan =11; 
					Cell_CourtName_FirstRec.BorderStyle =BorderStyle.None;
					Cell_CourtName_FirstRec.HorizontalAlign=HorizontalAlign.Left ;
					Cell_CourtName_FirstRec.Font.Bold=true;
					Cell_CourtName_FirstRec.Font.Underline =true;
					Cell_CourtName_FirstRec.Font .Size =8;
				                
					Cell_CourtName_FirstRec.Text=Ds_FillTable.Tables[1] .Rows[0]["CourtName_Address_DateSet"].ToString ();
					Firstrowspan.Cells .Add(Cell_CourtName_FirstRec);
					Table_OutPut .Rows .Add (Firstrowspan );
					LeaveOneRow();
	
					#endregion
					//Finish Inserting First CourtHeader

					//Insertint First Record
					#region
					serialNo +=1;
					string caseNoFirst = Ds_FillTable.Tables[1] .Rows[0]["casenumber"].ToString();
                
					TableRow rowInsertFirst =new TableRow ();
					rowInsertFirst.Font .Size =7;
					rowInsertFirst.BorderColor=Color.DarkBlue ;
					rowInsertFirst.ForeColor =Color.Black;
					rowInsertFirst.BorderStyle=BorderStyle.Double;   

					
					TableCell cell_SerialNoFirst=new TableCell ();
					cell_SerialNoFirst.BorderColor=Color.DarkBlue ;
					cell_SerialNoFirst.ForeColor =Color.Black;
					cell_SerialNoFirst .Height =10;
					//cell_SerialNoFirst .BorderStyle =BorderStyle.Double; 
 
					cell_SerialNoFirst .ColumnSpan =0;
					cell_SerialNoFirst .Width=10;
					cell_SerialNoFirst .Wrap =true;
				
					cell_SerialNoFirst.Font.Size =6; 
					cell_SerialNoFirst.Text="<Font Face=Tahoma>"+"<a href=../ClientInfo/ViolationFeeold.aspx?casenumber="+caseNoFirst+"&search=0>"+serialNo+"</a>";
					//cell_SerialNoFirst.Text=""+serialNo;  
					rowInsertFirst.Cells.Add(cell_SerialNoFirst);

				    
						
					
					TableCell cell_ClientCount_PretrialStatusFirst=new TableCell ();
					cell_ClientCount_PretrialStatusFirst.BorderColor=Color.DarkBlue ;
					cell_ClientCount_PretrialStatusFirst.ForeColor =Color.Black;
					cell_ClientCount_PretrialStatusFirst .Font .Size =7;
					cell_ClientCount_PretrialStatusFirst .Width =130;
					cell_ClientCount_PretrialStatusFirst .Height =10;
					cell_ClientCount_PretrialStatusFirst .Wrap=true;
					cell_ClientCount_PretrialStatusFirst.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[0]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
					rowInsertFirst.Cells.Add(cell_ClientCount_PretrialStatusFirst);
					//
					//					TableCell cell_Blank=new TableCell ();
					//					cell_Blank.BorderColor=Color.DarkBlue ;
					//					cell_Blank.ForeColor =Color.Black;
					//					cell_Blank .Font .Size =7;
					//					cell_Blank .Width =20;
					//					cell_Blank .Height =10;
					//					cell_Blank .Wrap=true;
					//					cell_Blank.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[0]["Space"].ToString();
					//					rowInsertFirst.Cells.Add(cell_Blank);
				
					TableCell cell_violationcount_lastnameFirst=new TableCell ();
					cell_violationcount_lastnameFirst.BorderColor=Color.DarkBlue ;
					cell_violationcount_lastnameFirst.ForeColor =Color.Black;
					cell_violationcount_lastnameFirst.Font .Size =7;
					cell_violationcount_lastnameFirst .Width =110;
					cell_violationcount_lastnameFirst .Height =10;
					cell_violationcount_lastnameFirst .Wrap=true;
					cell_violationcount_lastnameFirst .Font .Name ="Arial";
					cell_violationcount_lastnameFirst.Text="&nbsp;<Font Face=Arial>"+Ds_FillTable.Tables[1] .Rows[0]["violationcount"].ToString()+
						//"<a onclick ='OpenPopUp(trialNotes.aspx," + )' trialNotes.aspx?casenumber="+ Ds_FillTable.Tables[0] .Rows[0]["casenumber"].ToString()+">"+Ds_FillTable.Tables[0] .Rows[0]["Client_lastname"].ToString()+"</a>";
						//"<a href=\"#\" onClick=OpenPopUp('trialNotes.aspx?casenumber="+caseNoFirst+"','casenumber=" + caseNoFirst+ "');>"+Ds_FillTable.Tables[1] .Rows[0]["Client_lastname"].ToString()+"</a></font>";
					rowInsertFirst.Cells.Add(cell_violationcount_lastnameFirst);


					TableCell cell_FirstName_MiddleNameFirst=new TableCell ();
					cell_FirstName_MiddleNameFirst.BorderColor=Color.DarkBlue ;
					cell_FirstName_MiddleNameFirst.ForeColor =Color.Black;
					cell_FirstName_MiddleNameFirst.Font .Size =7;
					cell_FirstName_MiddleNameFirst .Width =108;
					cell_FirstName_MiddleNameFirst .Height =10;
					cell_FirstName_MiddleNameFirst .Wrap=true;
					cell_FirstName_MiddleNameFirst.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[0]["FMName"].ToString();
					rowInsertFirst.Cells.Add(cell_FirstName_MiddleNameFirst);
					
					//First Row First Grid 	
					TableCell cell_OffenceDateFirst =new TableCell ();
					cell_OffenceDateFirst.BorderColor=Color.DarkBlue ;
					cell_OffenceDateFirst.ForeColor =Color.Black;
					cell_OffenceDateFirst.Font .Size =7;
					cell_OffenceDateFirst .Width =70;
					cell_OffenceDateFirst .Height =10;
					cell_OffenceDateFirst .Wrap=true;
					cell_OffenceDateFirst.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[0]["offenceDate"].ToString();
					rowInsertFirst.Cells.Add(cell_OffenceDateFirst);

					TableCell cell_BondingFirst=new TableCell ();
					cell_BondingFirst.BorderColor=Color.DarkBlue ;
					cell_BondingFirst.ForeColor =Color.Black;
					cell_BondingFirst.Font .Size =7;
					cell_BondingFirst .Font.Bold =true;
					cell_BondingFirst .Wrap=true;
					cell_BondingFirst .Width =50;
					cell_BondingFirst .Height =10; 
					cell_BondingFirst.Text="&nbsp;"+Ds_FillTable .Tables[1].Rows[0]["BondFlag"].ToString();
					rowInsertFirst.Cells.Add(cell_BondingFirst);;

					TableCell cell_CourtTimeFirst=new TableCell ();
					cell_CourtTimeFirst.BorderColor=Color.DarkBlue ;
					cell_CourtTimeFirst.ForeColor =Color.Black;
					cell_CourtTimeFirst.Font .Size =7;
					cell_CourtTimeFirst.Width=75;
					cell_CourtTimeFirst .Height =10;
					//cell_CourtTimeFirst .Wrap=true;
                    cell_CourtTimeFirst.Wrap = false;
					cell_CourtTimeFirst.Font .Bold =true;
					cell_CourtTimeFirst.Text=Ds_FillTable.Tables[1] .Rows[0]["currentdateset"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtTimeFirst);

					
					TableCell cell_CourtNumFirst=new TableCell ();
					cell_CourtNumFirst.BorderColor=Color.DarkBlue ;
					cell_CourtNumFirst.ForeColor =Color.Black;
					cell_CourtNumFirst.Font .Size =7;
					cell_CourtNumFirst.Font .Bold =true;
					cell_CourtNumFirst .Width =23;
					cell_CourtNumFirst .Height =10;
					cell_CourtNumFirst .Wrap=true;
					cell_CourtNumFirst.Text="&nbsp;"+"#"+Ds_FillTable.Tables[1] .Rows[0]["currentcourtnum"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtNumFirst);
						

					TableCell cell_Trial_DescriptionFirst=new TableCell ();
					cell_Trial_DescriptionFirst.BorderColor=Color.DarkBlue ;
					cell_Trial_DescriptionFirst.ForeColor =Color.Black;
					cell_Trial_DescriptionFirst.Font .Size =7;
					cell_Trial_DescriptionFirst.Font .Bold =true;
					cell_Trial_DescriptionFirst .Wrap =true;
					cell_Trial_DescriptionFirst .Height =10;
					cell_Trial_DescriptionFirst .Width =32;
					cell_Trial_DescriptionFirst.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[0]["Description"].ToString();
					rowInsertFirst.Cells.Add(cell_Trial_DescriptionFirst );


					TableCell cell_OFirstName_LastNameFirst=new TableCell ();            					      
					cell_OFirstName_LastNameFirst.BorderColor=Color.DarkBlue ;
					cell_OFirstName_LastNameFirst.ForeColor =Color.Black;
					cell_OFirstName_LastNameFirst.Font .Size =7;
					cell_OFirstName_LastNameFirst .Wrap=true;
					cell_OFirstName_LastNameFirst .Width =90;
					cell_OFirstName_LastNameFirst .Height =10;
					cell_OFirstName_LastNameFirst.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[0]["OLFname"].ToString();
					rowInsertFirst.Cells.Add(cell_OFirstName_LastNameFirst);

					// First Row --- Second Grid
					TableCell cell_MidNumFirst=new TableCell ();            					      
					cell_MidNumFirst.BorderColor=Color.DarkBlue ;
					cell_MidNumFirst.ForeColor =Color.Black;
					cell_MidNumFirst.Font .Size =7;
					cell_MidNumFirst.Wrap=true;
					cell_MidNumFirst.Width =80;
					cell_MidNumFirst.Height =10;
					cell_MidNumFirst.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[0]["midnum"].ToString();
					rowInsertFirst.Cells.Add(cell_MidNumFirst);


					TableCell cell_TrialCommentsFirst=new TableCell ();
					cell_TrialCommentsFirst.BorderColor=Color.DarkBlue ;
					cell_TrialCommentsFirst.ForeColor =Color.Black;
					cell_TrialCommentsFirst .Width =152;
					cell_TrialCommentsFirst .Height =10;
					cell_TrialCommentsFirst .Wrap =true;
					cell_TrialCommentsFirst.Font .Size =7;
					cell_TrialCommentsFirst.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[0]["Shortdesc1"].ToString()+
						"<Font Color=Red>"+Ds_FillTable.Tables[1] .Rows[0]["trialcomments"].ToString()+"</Font>" ;
					rowInsertFirst.Cells.Add(cell_TrialCommentsFirst);
					Table_OutPut.Rows.Add(rowInsertFirst);
					
					#endregion
					//Finish Adding First Record
					#region start
					for (int IdxRow=1;IdxRow<=Ds_FillTable.Tables[1] .Rows .Count-1 ;IdxRow++)
					{
						if(Ds_FillTable.Tables[1] .Rows[IdxRow][10].ToString()!=Ds_FillTable.Tables[1] .Rows[IdxRow-1][10].ToString())
						{	
							LeaveOneRow();
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtName=new TableCell ();
							Cell_CourtName .Font .Size =8;
							Cell_CourtName.Font .Bold =true;
							Cell_CourtName.Font .Underline=true;
							Cell_CourtName.BorderStyle =BorderStyle.None ;
							Cell_CourtName.ColumnSpan=11;
							Cell_CourtName .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtName_FirstRec.BorderStyle=BorderStyle.None; 			
							Cell_CourtName.Text=Ds_FillTable.Tables[1] .Rows[IdxRow][10].ToString ();
							rowspan.Cells .Add(Cell_CourtName);
							Table_OutPut .Rows .Add(rowspan);		               
							LeaveOneRow();			
						}                                 
						if(Ds_FillTable.Tables[1] .Rows[IdxRow]["currentcourtnum"].ToString()!=Ds_FillTable.Tables[1] .Rows[IdxRow-1]["currentcourtnum"].ToString()
							&& Ds_FillTable.Tables[1] .Rows[IdxRow]["currentcourtnum"].ToString()!=null 
							&& Ds_FillTable.Tables[1] .Rows[IdxRow-1]["currentcourtnum"].ToString()!=null
							&& Ds_FillTable.Tables[1].Rows[IdxRow-1]["CourtName_Address_DateSet"].ToString() == Ds_FillTable.Tables[1].Rows[IdxRow]["CourtName_Address_DateSet"].ToString())
						{	
							
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtNum=new TableCell ();
							Cell_CourtNum .Font .Size =8;
							Cell_CourtNum.BorderColor=Color.DarkBlue ;
							Cell_CourtNum.ForeColor =Color.Black;						
							Cell_CourtNum .ColumnSpan=11;
							Cell_CourtNum .Height =10;
							cell_CourtNumFirst .BorderStyle =BorderStyle.NotSet ;
							Cell_CourtNum  .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtNum  .Text="<br>";
							rowspan.Cells .Add(	Cell_CourtNum);
							Table_OutPut .Rows .Add(rowspan);		               							
						}
						TableRow rowInsert=new TableRow ();
						rowInsert .BorderStyle =BorderStyle.Solid; 
						rowInsert .Font .Size=6; 
						serialNo =serialNo+1;
						
						TableCell cell_SerialNo=new TableCell ();
						string caseNo= Ds_FillTable.Tables[1] .Rows[IdxRow]["casenumber"].ToString();
						cell_SerialNo.BorderColor=Color.DarkBlue ;
						cell_SerialNo.ForeColor =Color.Black;
						cell_SerialNo.Width=10;
						cell_SerialNo .Height =10;
						cell_SerialNo.Wrap =true;
						cell_SerialNo.Font .Size =6;
						cell_SerialNo.Text="<Font Face=Tahoma><a href=../ClientInfo/ViolationFeeold.aspx?casenumber="+caseNo+"&search=0>"+" "+serialNo+"</a></font>";
						rowInsert.Cells.Add(cell_SerialNo);

						TableCell cell_ClientCount_PretrialStatus=new TableCell ();
						cell_ClientCount_PretrialStatus.BorderColor=Color.DarkBlue ;
						cell_ClientCount_PretrialStatus.ForeColor =Color.Black;
						cell_ClientCount_PretrialStatus .Width =20;
						cell_ClientCount_PretrialStatus.Height =10;
						cell_ClientCount_PretrialStatus .Wrap =true;
						cell_ClientCount_PretrialStatus.Font .Size =7;
						cell_ClientCount_PretrialStatus.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[IdxRow]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
						rowInsert.Cells.Add(cell_ClientCount_PretrialStatus);

						//						TableCell cell_Blank1=new TableCell ();
						//						cell_Blank1.BorderColor=Color.DarkBlue ;
						//						cell_Blank1.ForeColor =Color.Black;
						//						cell_Blank1 .Font .Size =7;
						//						cell_Blank1 .Width =20;
						//						cell_Blank1 .Height =10;
						//						cell_Blank1 .Wrap=true;
						//						cell_Blank1.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[IdxRow]["Space"].ToString();
						//						rowInsert.Cells.Add(cell_Blank1);
						//
						//				
						//TableCell cell15=new TableCell();
						TableCell cell_violationcount_lastname=new TableCell ();
						cell_violationcount_lastname.BorderColor=Color.DarkBlue ;
						cell_violationcount_lastname.ForeColor =Color.Black;
						cell_violationcount_lastname.Font .Size =7;
						cell_violationcount_lastname .Width =110;
						cell_violationcount_lastname .Height =10;
						cell_violationcount_lastname .Wrap =true;
						//cell_violationcount_lastname.BorderStyle =BorderStyle.Double;
						string caseNo1 =Ds_FillTable.Tables[1] .Rows[IdxRow]["casenumber"].ToString();
						cell_violationcount_lastname .Font .Name ="Arial";

                        cell_violationcount_lastname.Text = "<Font Face=Arial>" + Ds_FillTable.Tables[1].Rows[IdxRow]["violationcount"].ToString() +
                            "<a href=\"#\" onClick=OpenPopUp('trialNotes.aspx?casenumber=" + caseNo1 + "','casenumber=" + caseNo1 + "');>" + Ds_FillTable.Tables[1].Rows[IdxRow]["Client_lastname"].ToString() + "</a></font>";

                        //cell_violationcount_lastname.Text = "<Font Face=Arial>" + Ds_FillTable.Tables[1].Rows[IdxRow]["violationcount"].ToString() +
                        //    "<a href=\"../ClientInfo/CaseDisposition.aspx?sMenu=65&search=0&casenumber=" + caseNo1 + "\">" + Ds_FillTable.Tables[1].Rows[IdxRow]["Client_lastname"].ToString() + "</a></font>";
						
                        
                        rowInsert.Cells.Add(cell_violationcount_lastname);
					

						TableCell cell_FirstName_MiddleName=new TableCell ();
						cell_FirstName_MiddleName.BorderColor=Color.DarkBlue ;
						cell_FirstName_MiddleName.ForeColor =Color.Black;
						cell_FirstName_MiddleName.Font .Size =7;
						cell_FirstName_MiddleName .Height =10;
						cell_FirstName_MiddleName .Width=108;
						cell_FirstName_MiddleName .Wrap =true;
						cell_FirstName_MiddleName.Text="&nbsp;"+Ds_FillTable.Tables[1] .Rows[IdxRow]["FMName"].ToString();
						rowInsert.Cells.Add(cell_FirstName_MiddleName);
					
						// inner Rows -- First Grid
						TableCell cell_OffenceDate =new TableCell ();
						cell_OffenceDate.BorderColor=Color.DarkBlue ;
						cell_OffenceDate.ForeColor =Color.Black;
						cell_OffenceDate.Font .Size =7;
						cell_OffenceDate .Width =70;
						cell_OffenceDate .Height =10;
						cell_OffenceDate .Wrap=true;
						cell_OffenceDate.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[IdxRow]["offenceDate"].ToString();
						rowInsert.Cells.Add(cell_OffenceDate);
						

						TableCell cell_Bonding=new TableCell ();
						cell_Bonding.BorderColor=Color.DarkBlue ;
						cell_Bonding.ForeColor =Color.Black;
						cell_Bonding.Font .Size =7;
						cell_Bonding .Width =50;
						cell_Bonding .Wrap =true;
						cell_Bonding .Height =10;
						cell_Bonding .Font .Bold =true;
						cell_Bonding.Text="&nbsp;"+Ds_FillTable .Tables[1].Rows[IdxRow]["BondFlag"].ToString();
						rowInsert.Cells.Add(cell_Bonding);
					
					

					
						TableCell cell_CourtTime=new TableCell ();
						cell_CourtTime.BorderColor=Color.DarkBlue ;
						cell_CourtTime.ForeColor =Color.Black;
						cell_CourtTime.Font .Size =7;
						cell_CourtTime.Width=75;
						cell_CourtTime .Height =10;
						//cell_CourtTime .Wrap =true;
                        cell_CourtTime.Wrap = false;
						cell_CourtTime .Font .Bold =true;					
						cell_CourtTime.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["currentdateset"].ToString();
						rowInsert.Cells.Add(cell_CourtTime);
					
					

						
						TableCell cell_CourtNum=new TableCell ();
						cell_CourtNum.BorderColor=Color.DarkBlue ;
						cell_CourtNum.ForeColor =Color.Black;	
						cell_CourtNum.Font .Size =7;
						cell_CourtNum .Width =23;
						cell_CourtNum .Wrap=true;
						cell_CourtNum .Height =10;
						cell_CourtNum.Font .Bold =true;
						cell_CourtNum.Text="&nbsp;"+"#"+Ds_FillTable.Tables[1] .Rows[IdxRow]["currentcourtnum"].ToString();
						rowInsert.Cells.Add(cell_CourtNum);
					
						
						TableCell cell_Trial_Description=new TableCell ();
						cell_Trial_Description.BorderColor=Color.DarkBlue ;
						cell_Trial_Description.ForeColor =Color.Black;	
						cell_Trial_Description.Font .Bold =true;
						cell_Trial_Description .Width =32;
						cell_Trial_Description .Wrap =true;
						cell_Trial_Description .Height =10;
						cell_Trial_Description.Font .Size =7;
						cell_Trial_Description.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["Description"].ToString();
						rowInsert.Cells.Add(cell_Trial_Description );
					

					
						TableCell cell_OFirstName_LastName=new TableCell ();            					      
						cell_OFirstName_LastName.BorderColor=Color.DarkBlue ;
						cell_OFirstName_LastName.ForeColor =Color.Black;
						cell_OFirstName_LastName.Font .Size =7;										
						cell_OFirstName_LastName .Width =90;
						cell_OFirstName_LastName .Height =10;
						cell_OFirstName_LastName .Wrap =true;
						cell_OFirstName_LastName.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["OLFname"].ToString();
						rowInsert.Cells.Add(cell_OFirstName_LastName);

					
						// Inner Row --- second Grid. 
						TableCell cell_midnum =new TableCell ();            					      
						cell_midnum.BorderColor=Color.DarkBlue ;
						cell_midnum.ForeColor =Color.Black;
						cell_midnum.Font .Size =7;										
						cell_midnum .Width =80;
						cell_midnum .Height =10;
						cell_midnum .Wrap =true;
						cell_midnum.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["midnum"].ToString();
						rowInsert.Cells.Add(cell_midnum);
  
 

						
						TableCell cell22=new TableCell();
						TableCell cell_TrialComments=new TableCell ();
						cell_TrialComments.BorderColor=Color.DarkBlue ;
						cell_TrialComments.ForeColor =Color.Black;
						cell_TrialComments.Font .Size =7;
						cell_TrialComments .Width =152;
						cell_TrialComments .Height =10;
						cell_TrialComments .Wrap =true;
						cell_TrialComments.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["Shortdesc1"].ToString()+
							"<Font Color=Red>"+Ds_FillTable.Tables[1] .Rows[IdxRow]["trialcomments"].ToString()+"</Font>" ;;
						rowInsert.Cells.Add(cell_TrialComments);
						Table_OutPut.Rows.Add(rowInsert);	
						this.lblRecStatus .Visible =false;
					}
					#endregion
           
				}
				else
				{
					if (Ds_FillTable.Tables[1] .Rows .Count + Ds_FillTable.Tables[0] .Rows .Count < 1 )
					{
						this.lblRecStatus .Visible =true;
						this.lblRecStatus .Text="No Records Found";
					}
				}
			}
			catch(Exception ex)
			{
				this.lblRecStatus .Visible =true;
				this.lblRecStatus .Text="Record Not Found";
				//				TableRow row=new TableRow();
				//				TableCell cell=new TableCell();
				//				cell.Text="<h1><font color='red'>Invalid Value entered for Table Properties:</h1><br>" + ex.ToString();
				//				row.Cells.Add(cell);
				//				Table_OutPut.Rows.Add(row);
				if (ex.Message != "Cannot find table 0.")
					bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		public void LeaveOneRow()
		{
			TableRow tr=new TableRow ();
			tr.BorderStyle =BorderStyle.None; 
			TableCell tc=new TableCell ();
			tc.BorderStyle =BorderStyle.None ;
			tc.Text="<BR>";
			tc.ColumnSpan =12;
			tr.Cells .Add (tc);
			Table_OutPut .Rows .Add (tr);
		}
        protected void img_pdf_Click(object sender, ImageClickEventArgs e)
        {
                //for website
                string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=" + DDLCourts.SelectedValue.ToString() + "&courtdate=" + dtpCourtDate.SelectedDate.ToShortDateString() + "&page=" + "1" + "&courtnumber=" + TxtCourtNo.Text + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 1;
                //website end
                GeneratePDF(path);
                RenderPDF();
        }
        private void GeneratePDF(string path)
        {
            Doc theDoc = new Doc();
            theDoc.Rect.Inset(35, 45);
            theDoc.Page = theDoc.AddPage();
            int theID;
            theID = theDoc.AddImageUrl(path);
            while (true)
            {
                theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }
            string name = Session.SessionID + ".pdf";
            //for website
            ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
            ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
            //website end
            theDoc.Save(ViewState["vFullPath"].ToString());
            theDoc.Clear();
        }
        private void GeneratePDF(string path,int seq)
        {
            if (seq == 0)
            {
                Doc theDoc1 = new Doc();
                theDoc1.Rect.Inset(35, 45);
                theDoc1.Page = theDoc1.AddPage();
                int theID;
                theID = theDoc1.AddImageUrl(path);
                while (true)
                {
                    theDoc1.FrameRect(); // add a black border
                    if (!theDoc1.Chainable(theID))
                        break;
                    theDoc1.Page = theDoc1.AddPage();
                    theID = theDoc1.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc1.PageCount; i++)
                {
                    theDoc1.PageNumber = i;
                    theDoc1.Flatten();
                }
                string name = Session.SessionID + ".pdf";
                //for website
                ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                //website end
                //theDoc1.Save(ViewState["vFullPath"].ToString());
                Session["vdoc1"] = theDoc1;
                //theDoc1.Clear();
            }
            if (seq > 0)
            {
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(35, 45);
                theDoc.Page = theDoc.AddPage();
                int theID;
                theID = theDoc.AddImageUrl(path);
                while (true)
                {
                    theDoc.FrameRect(); // add a black border
                    if (!theDoc.Chainable(theID))
                        break;
                    theDoc.Page = theDoc.AddPage();
                    theID = theDoc.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc.PageCount; i++)
                {
                    theDoc.PageNumber = i;
                    theDoc.Flatten();
                }
                Doc theDoc1 = (Doc) Session["vdoc1"];
                theDoc1.Append(theDoc);
                Session["vdoc1"] = theDoc1;                
                theDoc.Clear();
            }
            //theDoc1.Clear();
        }
        private void RenderPDF()
        {
            //for website
            string vpth = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH1"].ToString().Length) - 1);
            string link = "../DOCSTORAGE" + vpth ;
            link=link + Session.SessionID + ".pdf";
            //website end
            Response.Redirect(link, false);
            string createdate = String.Empty;
            string currDate = DateTime.Today.ToShortDateString();
            DateTime CurrDate = Convert.ToDateTime(currDate);
            DateTime fileDate = new DateTime();
            string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
            for (int i = 0; i < files.Length; i++)
            {
                createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                fileDate = Convert.ToDateTime(createdate);
                if (fileDate.CompareTo(CurrDate) < 0)
                {
                    System.IO.File.Delete(files[i]);
                }
            }
        }
        protected void img_PdfAll_Click(object sender, ImageClickEventArgs e)
        {
            //for website
            string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=None" + "&courtdate=" + dtpCourtDate.SelectedDate.ToShortDateString() + "&page=" + "1" + "&courtnumber=" + TxtCourtNo.Text + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 1;
            //website end
            GeneratePDF(path, 0);

            try
            {
                string[] courtid = (string[])ViewState["vCourts"];
                for (int i = 1; i <= courtid.Length; i++)
                {
                    path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=" + courtid[i - 1] + "&courtdate=" + dtpCourtDate.SelectedDate.ToShortDateString() + "&page=" + "1" + "&courtnumber=" + TxtCourtNo.Text + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 1;
                    GeneratePDF(path, i);
                }
            }
            catch { }
            Doc theDoc1 = (Doc)Session["vdoc1"];
            theDoc1.Save(ViewState["vFullPath"].ToString());
            theDoc1.Clear();
            Session["vdoc1"] = null;
            RenderPDF();
        }
        protected void img_PDFCourt(object sender, ImageClickEventArgs e)
        {
            //for website
            int intCopies = Convert.ToInt32(ddl_Copies.SelectedValue);
            string path = null;

            for (int i = 0; i <= intCopies-1; i++)
            {
                path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=None" + "&courtdate=" + dtpCourtDate.SelectedDate.ToShortDateString() + "&page=" + "1" + "&courtnumber=" + TxtCourtNo.Text + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 1;
                //website end
                GeneratePDF(path, i);
            }

            string[] Courtinfo;
            try
            {
                string[] courtid = (string[])ViewState["vCourtNum"];
                string strCourtNum = null;

                for (int i = intCopies + 1; i <= courtid.Length + intCopies; i++)
                {
                    Courtinfo = courtid[i - 1 - intCopies].Split('|');

                    if (Courtinfo.GetUpperBound(0) == 1)
                    {
                        strCourtNum = Courtinfo[1];
                    }
                    else
                    {
                        strCourtNum = "0";
                    }
                                        
                    path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/quickentry/PrintTrialDocket.aspx?courtloc=" + Courtinfo[0] + "&courtdate=" + dtpCourtDate.SelectedDate.ToShortDateString() + "&page=" + "1" + "&courtnumber=" + strCourtNum + "&datetype=" + LstCaseStatus.SelectedValue.ToString() + "&RecdType=" + 1;
                    GeneratePDF(path, i);
                }
            }
            catch(Exception ex)
            {
                string d = ex.Message;
            
            }
            Doc theDoc1 = (Doc)Session["vdoc1"];
            theDoc1.Save(ViewState["vFullPath"].ToString());
            theDoc1.Clear();
            Session["vdoc1"] = null;
            RenderPDF();
        }
	}
}

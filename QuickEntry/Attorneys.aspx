<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attorneys.aspx.cs" Inherits="lntechNew.QuickEntry.Attorneys" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc3" %>

<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>Atorneys Email Addresses </title>
   <%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>
   
   <script>
   
    
    function checkemail()
    {
    
    //tb_email
    
    if(document.getElementById("tb_email").value !="" )
	{			
				if( isEmail(document.getElementById("tb_email").value)== false)
				{
				alert ("Please Enter Email Address In Correct format.");
				document.getElementById("tb_email").focus(); 
				return false;			   
				}
     }	
	else
	{       	
	        alert ("Please Enter Email Address ");
	        document.getElementById("tb_email").focus(); 
	        return false;
	
	}	   
        
    
    }
    
   
   
   
   
   </script>
   
   
</head>

<!-- BEGIN BODY -->
    <body class=" ">
        
        <form id="form1" runat="server">
            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </asp:Panel>
            
                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">
                                    <!-- PAGE HEADING TAG - START -->
                                    <h1 class="title">Attorney Email</h1>
                                    <!-- PAGE HEADING TAG - END --> 
                                </div>

                                                
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Email</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="tb_email" runat="server" CssClass="form-control" Font-Bold="True"></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">&nbsp;</label>
                                                <div class="controls">
                                                    <asp:Button ID="btn_refresh" runat="server" CommandName="search" CssClass="btn btn-primary" OnClick="btn_refresh_Click" Text="Add" OnClientClick="return checkemail();" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <asp:Label ID="lbl_error" runat="server" CssClass="form-label" Font-Bold="True" ForeColor="Red"></asp:Label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:GridView ID="gv_list" runat="server" AutoGenerateColumns="False" OnRowCommand="gv_list_RowCommand" OnRowDeleting="gv_list_RowDeleting" CssClass="table" OnSelectedIndexChanged="gv_list_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Attorney's Email Addresses" HeaderStyle-HorizontalAlign="center">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lbl_eadd" runat="server" CssClass="form-label" Text=" Attorney's Email Addresses"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_address" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.EmailAddress") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btn_delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.Id") %>'
                                                                CssClass="btn btn-primary" CommandName="delete">Delete</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        
                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                </section>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
        </form>
        
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 


        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Body goes here...

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </body>

























































<%--<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101;
            left: 88px; position: absolute; top: 16px" width="780">
            <tr>
                <td colspan="4" style="width: 815px">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="4" style="width: 816px">
                    <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" colspan="4" valign="top">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" colspan="4" height="11" width="100%">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" rowspan="1" width="100%" >
                                <table width="100%" class="clsleftpaddingtable">
                                    <tr>
                                        <td class="clsleftpaddingtable" style="width: 10px">
                                            <asp:Label ID="Label1" runat="server" CssClass="clssubhead" Text="Enter Email Address :"
                                                Width="132px"></asp:Label></td>
                                        <td  style="width: 181px">
                                            <asp:TextBox ID="tb_email" runat="server"  Width="201px" CssClass="clsinputadministration" Font-Bold="True"></asp:TextBox></td>
                                        <td class="clsleftpaddingtable">
                                            &nbsp;
                                            <asp:Button ID="btn_refresh" runat="server" CommandName="search" CssClass="clsbutton"
                                                OnClick="btn_refresh_Click" Text="Add" Width="72px" OnClientClick="return checkemail();" /></td>
                                        <td class="clsleftpaddingtable" style="width: 100px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" background="../Images/separator_repeat.gif" colspan="4" style="height: 13px">
                            </td>
                        </tr>
                        <tr>
                            <td align="left"  colspan="4" style="width: 100%;">
                                <asp:GridView ID="gv_list" runat="server" AutoGenerateColumns="False" BorderStyle="None" OnRowCommand="gv_list_RowCommand" OnRowDeleting="gv_list_RowDeleting" Width="100%" CssClass="clsleftpaddingtable" OnSelectedIndexChanged="gv_list_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Attorney's Email Addresses" HeaderStyle-HorizontalAlign="center" HeaderStyle-Height="20px">
                                            <HeaderTemplate>
                                                <asp:Label ID="lbl_eadd" runat="server" CssClass="clssubhead" Text=" Attorney's Email Addresses"
                                                    Width="165px"></asp:Label>
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="clsleftpaddingtable" Width="725px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_address" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container,"DataItem.EmailAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="clsleftpaddingtable" Width="100px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btn_delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.Id") %>'
                                                    CssClass="button" CommandName="delete">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" background="../Images/separator_repeat.gif" colspan="4" style="width: 760px;
                                height: 13px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="width: 760px">
                                &nbsp;<asp:Label ID="lbl_error" runat="server" CssClass="clslabel" Font-Bold="True"
                                    ForeColor="Red"></asp:Label></td>
                        </tr>
                    </table>
                    <uc3:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>--%>
</html>

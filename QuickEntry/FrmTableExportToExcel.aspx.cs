using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components;

namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for FrmTableExportTiExcel.
	/// </summary>
	public partial class FrmTableExportTiExcel : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Table TbResults;
	    Table Tb_Return=new Table ();

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session.Count == 0)
			{
				Response.Redirect("../../login.asp",true);
			}
			else
			{ 
				if (Session["ds"]!= null)
				{
					DataSet dispDataSet=(DataSet)Session["ds"];                   
					TbResults=formatDataSetToTable(dispDataSet); 
                    cmpDataGridToExcel.TableToExcel(TbResults, Response);
				
				}
				else
				{
					Response.Redirect("../../login.asp",true);
				}
			}
		}
		private void FillTable()
		{	
		
		}
		
		private Table formatDataSetToTable(DataSet ds_Play)
		{
			 

             TableRow trHeadDate=new TableRow ();
			 TableCell tcHeadDate =new TableCell ();
			 tcHeadDate.Text="Print Date :"+ DateTime.Now;
			 tcHeadDate.ColumnSpan =10;
			 tcHeadDate.HorizontalAlign =HorizontalAlign.Center;
			 tcHeadDate.Font .Size =6;
             trHeadDate.Cells .Add(tcHeadDate);
			 Tb_Return.Rows.Add (trHeadDate);
			 
			 TableRow trHeadDateInfo=new TableRow ();
			 TableCell tcHeadDateInfo =new TableCell ();
			 DateTime dt=new DateTime ();
			 tcHeadDateInfo.Text="SULLO AND SULLO ATTORNEYS:"+dt.ToShortDateString ()+"- Docket  ___ 1___2___3___*";
             tcHeadDateInfo.ColumnSpan =10;
			 tcHeadDateInfo.HorizontalAlign =HorizontalAlign.Center;
			 tcHeadDateInfo.Font .Size =10;
	         tcHeadDateInfo.Font.Bold =true;
			 trHeadDateInfo.Cells .Add(tcHeadDateInfo);
			 Tb_Return.Rows.Add (trHeadDateInfo );

			try
			{
				int VarChk = 0;                 
						
				//ds_Play= formatDataSet(ds_Play);
				int serialNo=0;
				
				if(ds_Play.Tables [0].Rows .Count !=0)
				{
					//Inserting First Court Header
					#region
					TableRow Firstrowspan=new TableRow ();
					Firstrowspan.BorderStyle =BorderStyle.None;  
					TableCell Cell_CourtName_FirstRec=new TableCell ();
					Cell_CourtName_FirstRec.ColumnSpan =11; 
					Cell_CourtName_FirstRec.BorderStyle =BorderStyle.None;
					Cell_CourtName_FirstRec.HorizontalAlign=HorizontalAlign.Left ;
					Cell_CourtName_FirstRec.Font.Bold=true;
					Cell_CourtName_FirstRec.Font.Underline =true;
					Cell_CourtName_FirstRec.Font .Size =10;
                
					Cell_CourtName_FirstRec.Text=ds_Play.Tables[0].Rows[0]["CourtName_Address_DateSet"].ToString ();
					Firstrowspan.Cells .Add(Cell_CourtName_FirstRec);
					Tb_Return .Rows .Add (Firstrowspan );
					LeaveOneRow();
					#endregion
					//Finish Inserting First CourtHeader

					//Insertint First Record
					#region
				
					TableRow rowInsertFirst =new TableRow ();		
					rowInsertFirst.BorderStyle=BorderStyle.Solid;   
 					serialNo =1;
					TableCell cell_SerialNoFirst=new TableCell ();
					cell_SerialNoFirst.BorderColor=Color.DarkBlue ;
					cell_SerialNoFirst.ForeColor =Color.Black;
					cell_SerialNoFirst .Wrap =true;
					cell_SerialNoFirst .Width =25;
					
	    			//cell_SerialNoFirst .BorderStyle =BorderStyle.Double;  
					cell_SerialNoFirst.Text=serialNo.ToString ();
					rowInsertFirst.Cells.Add(cell_SerialNoFirst);
				
						
					//TableCell cell14=new TableCell();
					TableCell cell_ClientCount_PretrialStatusFirst=new TableCell ();
					//cell_ClientCount_PretrialStatusFirst .BorderStyle =BorderStyle.Double;
					cell_ClientCount_PretrialStatusFirst.Text=ds_Play.Tables[0].Rows[0]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
					cell_ClientCount_PretrialStatusFirst .Wrap =true;
					cell_ClientCount_PretrialStatusFirst .Width =130;
					rowInsertFirst.Cells.Add(cell_ClientCount_PretrialStatusFirst);

					TableCell cell_Blank=new TableCell ();
					cell_Blank.Font .Size =7;
					cell_Blank.Font .Name="Arial" ;
					cell_Blank.Wrap =true;
					cell_Blank.Width=20;
					
					cell_Blank.Text="&nbsp;"+ds_Play .Tables[0].Rows[0]["space"].ToString();
					rowInsertFirst.Cells.Add(cell_Blank);

					//TableCell cell15=new TableCell();
					TableCell cell_violationcount_lastnameFirst=new TableCell ();
					//cell_violationcount_lastnameFirst .BorderStyle =BorderStyle.Double;
					cell_violationcount_lastnameFirst.Text=ds_Play.Tables[0].Rows[0]["violationcount"].ToString()+
						ds_Play.Tables[0].Rows[0]["Client_lastname"].ToString();
					cell_violationcount_lastnameFirst .Wrap =true;
					cell_violationcount_lastnameFirst .Width=130;
					rowInsertFirst.Cells.Add(cell_violationcount_lastnameFirst);

					//TableCell cell16=new TableCell();
					TableCell cell_FirstName_MiddleNameFirst=new TableCell ();
					//cell_FirstName_MiddleNameFirst .BorderStyle =BorderStyle.Double;
					cell_FirstName_MiddleNameFirst .Wrap =true;
					cell_FirstName_MiddleNameFirst .Width =108;
					cell_FirstName_MiddleNameFirst.Text=ds_Play.Tables[0].Rows[0]["FMName"].ToString();
					rowInsertFirst.Cells.Add(cell_FirstName_MiddleNameFirst);

					//TableCell cell17=new TableCell();
					TableCell cell_BondingFirst=new TableCell ();
					//cell_BondingFirst .BorderStyle =BorderStyle.Double;
					cell_BondingFirst .Font .Bold =true;
					cell_BondingFirst .Wrap =true; 
					cell_BondingFirst .Width =70;
					cell_BondingFirst.Text=ds_Play .Tables[0].Rows[0]["BondFlag"].ToString();
					rowInsertFirst.Cells.Add(cell_BondingFirst);

					//TableCell cell18=new TableCell();
					TableCell cell_CourtTimeFirst=new TableCell ();
					//cell_CourtTimeFirst .BorderStyle =BorderStyle.Double;
                    cell_CourtTimeFirst.Font .Bold =true;
					cell_CourtTimeFirst .Width =70;
					cell_CourtTimeFirst .Wrap =true;			  
					cell_CourtTimeFirst.Text=ds_Play.Tables[0].Rows[0]["currentdateset"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtTimeFirst);

					//TableCell cell19=new TableCell();
					TableCell cell_CourtNumFirst=new TableCell ();
					//cell_CourtNumFirst .BorderStyle =BorderStyle.Double;
					cell_CourtNumFirst .Width =23;
					cell_CourtNumFirst .Wrap =true;
					cell_CourtNumFirst .Font .Bold =true;
					//cell_CourtNumFirst .BorderStyle =Border
					cell_CourtNumFirst.Text="#"+ds_Play.Tables[0].Rows[0]["currentcourtnum"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtNumFirst);
						
					//TableCell cell20=new TableCell();
					TableCell cell_Trial_DescriptionFirst=new TableCell ();
					cell_Trial_DescriptionFirst .Wrap =true;
					cell_Trial_DescriptionFirst .Font .Bold =true;
					cell_Trial_DescriptionFirst.Text=ds_Play.Tables[0].Rows[0]["Description"].ToString();
					rowInsertFirst.Cells.Add(cell_Trial_DescriptionFirst );

					//TableCell cell21=new TableCell();
					TableCell cell_OFirstName_LastNameFirst=new TableCell ();            					      
					//cell_OFirstName_LastNameFirst .BorderStyle =BorderStyle.Double;
					cell_OFirstName_LastNameFirst.Text=ds_Play.Tables[0].Rows[0]["OLFname"].ToString();
					cell_OFirstName_LastNameFirst .Width =177;	
					cell_OFirstName_LastNameFirst .Wrap =true;
					rowInsertFirst.Cells.Add(cell_OFirstName_LastNameFirst);

					//TableCell cell22=new TableCell();
					TableCell cell_TrialCommentsFirst=new TableCell ();
					//cell_TrialCommentsFirst .BorderStyle =BorderStyle.Double;
					cell_TrialCommentsFirst.Text=ds_Play.Tables[0].Rows[0]["Shortdesc1"].ToString()
						+"<Font Color=Red>"+ds_Play.Tables[0].Rows[0]["trialcomments"].ToString()+"</font>";
					cell_TrialCommentsFirst .Width =157;					
					rowInsertFirst.Cells.Add(cell_TrialCommentsFirst);
					
					Tb_Return.Rows.Add(rowInsertFirst);
					#endregion
					//Finish Adding First Record

           
					for (int IdxRow=1;IdxRow<=ds_Play.Tables [0].Rows .Count-1 ;IdxRow++)
					{
					
						if(ds_Play.Tables[0].Rows[IdxRow]["CourtName_Address_DateSet"].ToString()!=ds_Play.Tables[0].Rows[IdxRow-1]["CourtName_Address_DateSet"].ToString())
						{	
							VarChk +=1;
							LeaveOneRow();
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtName=new TableCell ();
							Cell_CourtName .Font .Size =10;
							Cell_CourtName.Font .Bold =true;
							Cell_CourtName.Font .Underline=true;
							Cell_CourtName.BorderStyle =BorderStyle.None ;
							Cell_CourtName.ColumnSpan=11;
							Cell_CourtName .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtName_FirstRec.BorderStyle=BorderStyle.None; 			
							Cell_CourtName.Text=ds_Play.Tables [0].Rows[IdxRow]["CourtName_Address_DateSet"].ToString ();
							rowspan.Cells .Add(Cell_CourtName);
							Tb_Return .Rows .Add(rowspan);		               
							LeaveOneRow();
						
							//						TableRow Emptyrow=new TableRow ();
							//					   TableCell Cell_EmptyCell=new TableCell ();
							//					   Cell_EmptyCell .Text=" ";
							//					   Cell_EmptyCell.ColumnSpan =10;
							//					   Emptyrow.Cells .Add (Cell_EmptyCell);
							//					   Table_OutPut .Rows.Add (Emptyrow);
						}
						if(ds_Play.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString()!=ds_Play.Tables[0].Rows[IdxRow-1]["currentcourtnum"].ToString())
						{	
							
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtNum=new TableCell ();
							Cell_CourtNum .Font .Size =8;
							Cell_CourtNum .ColumnSpan=11;
							cell_CourtNumFirst .BorderStyle =BorderStyle.NotSet ;
							Cell_CourtNum  .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtNum  .Text="<br>";
							rowspan.Cells .Add(	Cell_CourtNum);
							
							Tb_Return .Rows .Add(rowspan);		               										
						}

						TableRow rowInsert =new TableRow ();	 
						rowInsert .BorderStyle =BorderStyle.Solid; 
						serialNo =IdxRow+1;
						TableCell cell_SerialNo=new TableCell ();
						cell_SerialNo.Text=serialNo.ToString ();
						cell_SerialNo.Width=25;
						cell_SerialNo.Wrap =true;
						rowInsert.Cells.Add(cell_SerialNo);
						
					
					
						//TableCell cell14=new TableCell();
						TableCell cell_ClientCount_PretrialStatus=new TableCell ();
						//cell_ClientCount_PretrialStatus.BorderStyle =BorderStyle.Double;
						cell_ClientCount_PretrialStatus .Width =20;
						cell_ClientCount_PretrialStatus .Wrap =true;
						cell_ClientCount_PretrialStatus.Text=ds_Play.Tables[0].Rows[IdxRow]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
						rowInsert.Cells.Add(cell_ClientCount_PretrialStatus);

						
						TableCell cell_Blank1=new TableCell ();
						cell_Blank1.Font .Size =7;
						cell_Blank1.Font .Name="Arial" ;
						cell_Blank1.Wrap =true;
						cell_Blank1.Width=20;
						cell_Blank1.Text="&nbsp;"+ds_Play.Tables[0].Rows[IdxRow]["space"].ToString();
						rowInsert.Cells.Add(cell_Blank1);

                        						//TableCell cell15=new TableCell();
						TableCell cell_violationcount_lastname=new TableCell ();
						//cell_violationcount_lastname.BorderStyle =BorderStyle.Double;
						cell_violationcount_lastname .Width =130;
						cell_violationcount_lastname .Wrap =true;
						cell_violationcount_lastname.Text=ds_Play.Tables[0].Rows[IdxRow]["violationcount"].ToString()+
						ds_Play.Tables[0].Rows[IdxRow]["Client_lastname"].ToString();
						rowInsert.Cells.Add(cell_violationcount_lastname);

					
						//TableCell cell16=new TableCell();
						TableCell cell_FirstName_MiddleName=new TableCell ();
						//cell_FirstName_MiddleName.BorderStyle =BorderStyle.Double;
						cell_FirstName_MiddleName .Width=108;
						cell_FirstName_MiddleName .Wrap =true;
						cell_FirstName_MiddleName.Text=ds_Play.Tables[0].Rows[IdxRow]["FMName"].ToString();
						rowInsert.Cells.Add(cell_FirstName_MiddleName);
					

						//TableCell cell17=new TableCell();
						TableCell cell_Bonding=new TableCell ();
						//cell_Bonding.BorderStyle =BorderStyle.Double;
						cell_Bonding.Text=ds_Play .Tables[0].Rows[IdxRow]["BondFlag"].ToString();
						cell_Bonding .Width =70;
						cell_Bonding .Wrap =true;
						
						cell_Bonding .Font .Bold =true;
						rowInsert.Cells.Add(cell_Bonding);
					

						//TableCell cell18=new TableCell();
						TableCell cell_CourtTime=new TableCell ();
						//cell_CourtTime.BorderStyle =BorderStyle.Double;
						cell_CourtTime.Width=70;
						
						cell_CourtTime .Wrap =true;							
						cell_CourtTime .Font .Bold =true;
						cell_CourtTimeFirst .Font .Bold =true;
						cell_CourtTime.Text=ds_Play.Tables[0].Rows[IdxRow]["currentdateset"].ToString();
						rowInsert.Cells.Add(cell_CourtTime);
					

						//TableCell cell19=new TableCell();
						TableCell cell_CourtNum=new TableCell ();
						//cell_CourtNum.BorderStyle =BorderStyle.Double;
						cell_CourtNum .Width =23;
						cell_CourtNum .Wrap=true;
						cell_CourtNum.Font .Bold =true;
						cell_CourtNum.Text="#"+ds_Play.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString();
						rowInsert.Cells.Add(cell_CourtNum);
					
						
						//TableCell cell20=new TableCell();
						TableCell cell_Trial_Description=new TableCell ();
						cell_Trial_Description .Font .Bold =true;
						cell_Trial_DescriptionFirst .Font .Bold =true;
						cell_Trial_Description .Width =32;						
						cell_Trial_Description .Wrap =true;
						
						//cell_Trial_Description.BorderStyle =BorderStyle.Double;				
						cell_Trial_Description.Text=ds_Play.Tables[0].Rows[IdxRow]["Description"].ToString();
						rowInsert.Cells.Add(cell_Trial_Description );
					

						//TableCell cell21=new TableCell();
						TableCell cell_OFirstName_LastName=new TableCell ();            					      
						//cell_OFirstName_LastName.BorderStyle =BorderStyle.Double;														
						cell_OFirstName_LastName.Text=ds_Play.Tables[0].Rows[IdxRow]["OLFname"].ToString();
						cell_OFirstName_LastName .Width =177;
						
						cell_OFirstName_LastName .Wrap =true;
						rowInsert.Cells.Add(cell_OFirstName_LastName);
					

						//TableCell cell22=new TableCell();
						TableCell cell_TrialComments=new TableCell ();
						//cell_TrialComments.BorderStyle =BorderStyle.Double;	
						cell_TrialComments .Width =157;
						cell_TrialComments .Wrap =true;
						
						cell_TrialComments.Text=ds_Play.Tables[0].Rows[IdxRow]["Shortdesc1"].ToString()
							+"<Font Color=Red>"+ds_Play.Tables[0].Rows[IdxRow]["trialcomments"].ToString()+"</font>";;
						rowInsert.Cells.Add(cell_TrialComments);
						Tb_Return.Rows.Add(rowInsert);	
						
					}
				}
				if(ds_Play.Tables [1].Rows .Count !=0)
				{
					//Inserting First Court Header
					LeaveOneRow();
					#region
					TableRow Firstrowspan=new TableRow ();
					Firstrowspan.BorderStyle =BorderStyle.None;  
					TableCell Cell_CourtName_FirstRec=new TableCell ();
					Cell_CourtName_FirstRec.ColumnSpan =11; 
					Cell_CourtName_FirstRec.BorderStyle =BorderStyle.None;
					Cell_CourtName_FirstRec.HorizontalAlign=HorizontalAlign.Left ;
					Cell_CourtName_FirstRec.Font.Bold=true;
					Cell_CourtName_FirstRec.Font.Underline =true;
					Cell_CourtName_FirstRec.Font .Size =10;
                
					Cell_CourtName_FirstRec.Text=ds_Play.Tables[1].Rows[0]["CourtName_Address_DateSet"].ToString ();
					Firstrowspan.Cells .Add(Cell_CourtName_FirstRec);
					Tb_Return .Rows .Add (Firstrowspan );
					LeaveOneRow();
					#endregion
					//Finish Inserting First CourtHeader

					//Insertint First Record
					#region
				
					TableRow rowInsertFirst =new TableRow ();		
					rowInsertFirst.BorderStyle=BorderStyle.Solid;   
					serialNo +=1;
					TableCell cell_SerialNoFirst=new TableCell ();
					cell_SerialNoFirst.BorderColor=Color.DarkBlue ;
					cell_SerialNoFirst.ForeColor =Color.Black;
					cell_SerialNoFirst .Wrap =true;
					cell_SerialNoFirst .Width =25;
					
					//cell_SerialNoFirst .BorderStyle =BorderStyle.Double;  
					cell_SerialNoFirst.Text=serialNo.ToString ();
					rowInsertFirst.Cells.Add(cell_SerialNoFirst);
				
						
					//TableCell cell14=new TableCell();
					TableCell cell_ClientCount_PretrialStatusFirst=new TableCell ();
					//cell_ClientCount_PretrialStatusFirst .BorderStyle =BorderStyle.Double;
					cell_ClientCount_PretrialStatusFirst.Text=ds_Play.Tables[1].Rows[0]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
					cell_ClientCount_PretrialStatusFirst .Wrap =true;
					cell_ClientCount_PretrialStatusFirst .Width =130;
					rowInsertFirst.Cells.Add(cell_ClientCount_PretrialStatusFirst);

					TableCell cell_Blank=new TableCell ();
					cell_Blank.Font .Size =7;
					cell_Blank.Font .Name="Arial" ;
					cell_Blank.Wrap =true;
					cell_Blank.Width=20;
					
					cell_Blank.Text="&nbsp;"+ds_Play .Tables[1].Rows[0]["space"].ToString();
					rowInsertFirst.Cells.Add(cell_Blank);

					//TableCell cell15=new TableCell();
					TableCell cell_violationcount_lastnameFirst=new TableCell ();
					//cell_violationcount_lastnameFirst .BorderStyle =BorderStyle.Double;
					cell_violationcount_lastnameFirst.Text=ds_Play.Tables[1].Rows[0]["violationcount"].ToString()+
						ds_Play.Tables[0].Rows[0]["Client_lastname"].ToString();
					cell_violationcount_lastnameFirst .Wrap =true;
					cell_violationcount_lastnameFirst .Width=130;
					rowInsertFirst.Cells.Add(cell_violationcount_lastnameFirst);

					//TableCell cell16=new TableCell();
					TableCell cell_FirstName_MiddleNameFirst=new TableCell ();
					//cell_FirstName_MiddleNameFirst .BorderStyle =BorderStyle.Double;
					cell_FirstName_MiddleNameFirst .Wrap =true;
					cell_FirstName_MiddleNameFirst .Width =108;
					cell_FirstName_MiddleNameFirst.Text=ds_Play.Tables[1].Rows[0]["FMName"].ToString();
					rowInsertFirst.Cells.Add(cell_FirstName_MiddleNameFirst);

					//TableCell cell17=new TableCell();
					TableCell cell_BondingFirst=new TableCell ();
					//cell_BondingFirst .BorderStyle =BorderStyle.Double;
					cell_BondingFirst .Font .Bold =true;
					cell_BondingFirst .Wrap =true; 
					cell_BondingFirst .Width =70;
					cell_BondingFirst.Text=ds_Play .Tables[1].Rows[0]["BondFlag"].ToString();
					rowInsertFirst.Cells.Add(cell_BondingFirst);

					//TableCell cell18=new TableCell();
					TableCell cell_CourtTimeFirst=new TableCell ();
					//cell_CourtTimeFirst .BorderStyle =BorderStyle.Double;
					cell_CourtTimeFirst.Font .Bold =true;
					cell_CourtTimeFirst .Width =70;
					cell_CourtTimeFirst .Wrap =true;			  
					cell_CourtTimeFirst.Text=ds_Play.Tables[1].Rows[0]["currentdateset"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtTimeFirst);

					//TableCell cell19=new TableCell();
					TableCell cell_CourtNumFirst=new TableCell ();
					//cell_CourtNumFirst .BorderStyle =BorderStyle.Double;
					cell_CourtNumFirst .Width =23;
					cell_CourtNumFirst .Wrap =true;
					cell_CourtNumFirst .Font .Bold =true;
					//cell_CourtNumFirst .BorderStyle =Border
					cell_CourtNumFirst.Text="#"+ds_Play.Tables[1].Rows[0]["currentcourtnum"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtNumFirst);
						
					//TableCell cell20=new TableCell();
					TableCell cell_Trial_DescriptionFirst=new TableCell ();
					cell_Trial_DescriptionFirst .Wrap =true;
					cell_Trial_DescriptionFirst .Font .Bold =true;
					cell_Trial_DescriptionFirst.Text=ds_Play.Tables[1].Rows[0]["Description"].ToString();
					rowInsertFirst.Cells.Add(cell_Trial_DescriptionFirst );

					//TableCell cell21=new TableCell();
					TableCell cell_OFirstName_LastNameFirst=new TableCell ();            					      
					//cell_OFirstName_LastNameFirst .BorderStyle =BorderStyle.Double;
					cell_OFirstName_LastNameFirst.Text=ds_Play.Tables[1].Rows[0]["OLFname"].ToString();
					cell_OFirstName_LastNameFirst .Width =177;	
					cell_OFirstName_LastNameFirst .Wrap =true;
					rowInsertFirst.Cells.Add(cell_OFirstName_LastNameFirst);

					//TableCell cell22=new TableCell();
					TableCell cell_TrialCommentsFirst=new TableCell ();
					//cell_TrialCommentsFirst .BorderStyle =BorderStyle.Double;
					cell_TrialCommentsFirst.Text=ds_Play.Tables[1].Rows[0]["Shortdesc1"].ToString()
						+"<Font Color=Red>"+ds_Play.Tables[1].Rows[0]["trialcomments"].ToString()+"</font>";
					cell_TrialCommentsFirst .Width =157;					
					rowInsertFirst.Cells.Add(cell_TrialCommentsFirst);
					
					Tb_Return.Rows.Add(rowInsertFirst);
					#endregion
					//Finish Adding First Record

           
					for (int IdxRow=1;IdxRow<=ds_Play.Tables [1].Rows .Count-1 ;IdxRow++)
					{
					
						if(ds_Play.Tables[1].Rows[IdxRow]["CourtName_Address_DateSet"].ToString()!=ds_Play.Tables[1].Rows[IdxRow-1]["CourtName_Address_DateSet"].ToString())
						{	
							VarChk +=1;
							LeaveOneRow();
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtName=new TableCell ();
							Cell_CourtName .Font .Size =10;
							Cell_CourtName.Font .Bold =true;
							Cell_CourtName.Font .Underline=true;
							Cell_CourtName.BorderStyle =BorderStyle.None ;
							Cell_CourtName.ColumnSpan=11;
							Cell_CourtName .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtName_FirstRec.BorderStyle=BorderStyle.None; 			
							Cell_CourtName.Text=ds_Play.Tables [1].Rows[IdxRow]["CourtName_Address_DateSet"].ToString ();
							rowspan.Cells .Add(Cell_CourtName);
							Tb_Return .Rows .Add(rowspan);		               
							LeaveOneRow();
						
							//						TableRow Emptyrow=new TableRow ();
							//					   TableCell Cell_EmptyCell=new TableCell ();
							//					   Cell_EmptyCell .Text=" ";
							//					   Cell_EmptyCell.ColumnSpan =10;
							//					   Emptyrow.Cells .Add (Cell_EmptyCell);
							//					   Table_OutPut .Rows.Add (Emptyrow);
						}
						if(ds_Play.Tables[1].Rows[IdxRow]["currentcourtnum"].ToString()!=ds_Play.Tables[1].Rows[IdxRow-1]["currentcourtnum"].ToString())
						{	
							
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtNum=new TableCell ();
							Cell_CourtNum .Font .Size =8;
							Cell_CourtNum .ColumnSpan=11;
							cell_CourtNumFirst .BorderStyle =BorderStyle.NotSet ;
							Cell_CourtNum  .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtNum  .Text="<br>";
							rowspan.Cells .Add(	Cell_CourtNum);
							
							Tb_Return .Rows .Add(rowspan);		               										
						}

						TableRow rowInsert =new TableRow ();	 
						rowInsert .BorderStyle =BorderStyle.Solid; 
						serialNo +=1;
						TableCell cell_SerialNo=new TableCell ();
						cell_SerialNo.Text=serialNo.ToString ();
						cell_SerialNo.Width=25;
						cell_SerialNo.Wrap =true;
						rowInsert.Cells.Add(cell_SerialNo);
						
					
					
						//TableCell cell14=new TableCell();
						TableCell cell_ClientCount_PretrialStatus=new TableCell ();
						//cell_ClientCount_PretrialStatus.BorderStyle =BorderStyle.Double;
						cell_ClientCount_PretrialStatus .Width =20;
						cell_ClientCount_PretrialStatus .Wrap =true;
						cell_ClientCount_PretrialStatus.Text=ds_Play.Tables[1].Rows[IdxRow]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
						rowInsert.Cells.Add(cell_ClientCount_PretrialStatus);

						
						TableCell cell_Blank1=new TableCell ();
						cell_Blank1.Font .Size =7;
						cell_Blank1.Font .Name="Arial" ;
						cell_Blank1.Wrap =true;
						cell_Blank1.Width=20;
						cell_Blank1.Text="&nbsp;"+ds_Play.Tables[1].Rows[IdxRow]["space"].ToString();
						rowInsert.Cells.Add(cell_Blank1);

						//TableCell cell15=new TableCell();
						TableCell cell_violationcount_lastname=new TableCell ();
						//cell_violationcount_lastname.BorderStyle =BorderStyle.Double;
						cell_violationcount_lastname .Width =130;
						cell_violationcount_lastname .Wrap =true;
						cell_violationcount_lastname.Text=ds_Play.Tables[1].Rows[IdxRow]["violationcount"].ToString()+
							ds_Play.Tables[1].Rows[IdxRow]["Client_lastname"].ToString();
						rowInsert.Cells.Add(cell_violationcount_lastname);

					
						//TableCell cell16=new TableCell();
						TableCell cell_FirstName_MiddleName=new TableCell ();
						//cell_FirstName_MiddleName.BorderStyle =BorderStyle.Double;
						cell_FirstName_MiddleName .Width=108;
						cell_FirstName_MiddleName .Wrap =true;
						cell_FirstName_MiddleName.Text=ds_Play.Tables[1].Rows[IdxRow]["FMName"].ToString();
						rowInsert.Cells.Add(cell_FirstName_MiddleName);
					

						//TableCell cell17=new TableCell();
						TableCell cell_Bonding=new TableCell ();
						//cell_Bonding.BorderStyle =BorderStyle.Double;
						cell_Bonding.Text=ds_Play .Tables[1].Rows[IdxRow]["BondFlag"].ToString();
						cell_Bonding .Width =70;
						cell_Bonding .Wrap =true;
						
						cell_Bonding .Font .Bold =true;
						rowInsert.Cells.Add(cell_Bonding);
					

						//TableCell cell18=new TableCell();
						TableCell cell_CourtTime=new TableCell ();
						//cell_CourtTime.BorderStyle =BorderStyle.Double;
						cell_CourtTime.Width=70;
						
						cell_CourtTime .Wrap =true;							
						cell_CourtTime .Font .Bold =true;
						cell_CourtTimeFirst .Font .Bold =true;
						cell_CourtTime.Text=ds_Play.Tables[1].Rows[IdxRow]["currentdateset"].ToString();
						rowInsert.Cells.Add(cell_CourtTime);
					

						//TableCell cell19=new TableCell();
						TableCell cell_CourtNum=new TableCell ();
						//cell_CourtNum.BorderStyle =BorderStyle.Double;
						cell_CourtNum .Width =23;
						cell_CourtNum .Wrap=true;
						cell_CourtNum.Font .Bold =true;
						cell_CourtNum.Text="#"+ds_Play.Tables[1].Rows[IdxRow]["currentcourtnum"].ToString();
						rowInsert.Cells.Add(cell_CourtNum);
					
						
						//TableCell cell20=new TableCell();
						TableCell cell_Trial_Description=new TableCell ();
						cell_Trial_Description .Font .Bold =true;
						cell_Trial_DescriptionFirst .Font .Bold =true;
						cell_Trial_Description .Width =32;						
						cell_Trial_Description .Wrap =true;
						
						//cell_Trial_Description.BorderStyle =BorderStyle.Double;				
						cell_Trial_Description.Text=ds_Play.Tables[1].Rows[IdxRow]["Description"].ToString();
						rowInsert.Cells.Add(cell_Trial_Description );
					

						//TableCell cell21=new TableCell();
						TableCell cell_OFirstName_LastName=new TableCell ();            					      
						//cell_OFirstName_LastName.BorderStyle =BorderStyle.Double;														
						cell_OFirstName_LastName.Text=ds_Play.Tables[1].Rows[IdxRow]["OLFname"].ToString();
						cell_OFirstName_LastName .Width =177;
						
						cell_OFirstName_LastName .Wrap =true;
						rowInsert.Cells.Add(cell_OFirstName_LastName);
					

						//TableCell cell22=new TableCell();
						TableCell cell_TrialComments=new TableCell ();
						//cell_TrialComments.BorderStyle =BorderStyle.Double;	
						cell_TrialComments .Width =157;
						cell_TrialComments .Wrap =true;
						
						cell_TrialComments.Text=ds_Play.Tables[1].Rows[IdxRow]["Shortdesc1"].ToString()
							+"<Font Color=Red>"+ds_Play.Tables[1].Rows[IdxRow]["trialcomments"].ToString()+"</font>";;
						rowInsert.Cells.Add(cell_TrialComments);
						Tb_Return.Rows.Add(rowInsert);	
						
					}
				}
				else
				{
					TableRow row=new TableRow();
					TableCell cell=new TableCell();
					cell.ColumnSpan =11;
					cell.Text="No Records Found";
					row.Cells.Add(cell);
					Tb_Return.Rows.Add(row);
				}
			}
			catch(Exception ex)
			{
				TableRow row=new TableRow();
				TableCell cell=new TableCell();
				cell.Text="<h1><font color='red'>Invalid Value entered for Table Properties:</h1><br>" + ex.ToString();
				row.Cells.Add(cell);
				Tb_Return.Rows.Add(row);
			}
			 
             
			  return Tb_Return; 
           }

		public void LeaveOneRow()
		{
			TableRow tr=new TableRow ();
			tr.BorderStyle =BorderStyle.None; 
			TableCell tc=new TableCell ();
			tc.BorderStyle =BorderStyle.None ;
			tc.Text="<BR>";
			tc.ColumnSpan =10;
			tr.Cells .Add (tc);
            Tb_Return .Rows .Add (tr);			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}

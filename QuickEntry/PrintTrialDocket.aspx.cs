using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components; 
namespace lntechNew.QuickEntry
{
	/// <summary>
	/// Summary description for PrintTrialDocket.
	/// </summary>
	public partial class PrintTrialDocket : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblPrintDate;
		protected System.Web.UI.WebControls.Label LblAttrorney;
		protected System.Web.UI.WebControls.Table Table_Print;
		clsENationWebComponents clsDb=new clsENationWebComponents();
        DataSet Ds_FillTable;
		private void Page_Load(object sender, System.EventArgs e)
		{
			lblPrintDate.Text="Print Date:"+ DateTime .Now.ToString ("MM/d/yyyy hh:mm tt");
			
			string courtloc=Request.QueryString ["courtloc"].ToString();
			string dt=Convert.ToString(Request.QueryString ["courtdate"]);
			string courtno=Request.QueryString ["courtnumber"];
			string datetype=Request.QueryString ["datetype"];
			int recdtype=Convert.ToInt32 (Request.QueryString ["RecdType"]);
			if(recdtype==1)
			{
				GetDataSet(courtloc ,dt ,courtno ,datetype); 
			}
			else
			{
				Ds_FillTable =(DataSet)Session["PassDG"];
			}
			FillTable(); 
		}
		public void GetDataSet(string cloc,string dt,string courtno,string datetype)
		{
			string[] key    = {"@courtloc","@courtdate","@page","@courtnumber","@datetype"};
			object[] value1 = {cloc,dt,"1",courtno,datetype };
			Ds_FillTable = clsDb.Get_DS_BySPArr("dbo.usp_hts_get_trial_docket_report_new",key,value1);
		}
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		#region FillTable Method
		private void FillTable()
		{	
			try
			{
				int VarChk = 0;                 
				//	DataSet 
				
				//Ds_FillTable= formatDataSet(Ds_FillTable);
				int serialNo=0;
				
				if(Ds_FillTable.Tables [0].Rows .Count !=0)
				{
					//Inserting First Court Header
					#region
					TableRow Firstrowspan=new TableRow ();
					Firstrowspan.BorderStyle =BorderStyle.None;  
					TableCell Cell_CourtName_FirstRec=new TableCell ();
					Cell_CourtName_FirstRec.ColumnSpan =11; 
					Cell_CourtName_FirstRec.BorderStyle =BorderStyle.None;
					Cell_CourtName_FirstRec.HorizontalAlign=HorizontalAlign.Left ;
					Cell_CourtName_FirstRec.Font.Bold=true;
					Cell_CourtName_FirstRec.Font.Underline =true;
					Cell_CourtName_FirstRec.Font .Size =11;				
				                
					Cell_CourtName_FirstRec.Text=Ds_FillTable.Tables[0].Rows[0]["CourtName_Address_DateSet"].ToString () ;
					Firstrowspan.Cells .Add(Cell_CourtName_FirstRec);
					Table_Print .Rows .Add (Firstrowspan );
					LeaveOneRow();
	
					#endregion
					//Finish Inserting First CourtHeader

					//Insertint First Record
					#region
				
					TableRow rowInsertFirst =new TableRow ();
					//rowInsertFirst.Height= 40;
					rowInsertFirst.BorderStyle=BorderStyle.Solid;   
               
					serialNo =1;
					TableCell cell_SerialNoFirst=new TableCell ();

					cell_SerialNoFirst.BorderColor=Color.Black;
					cell_SerialNoFirst.ForeColor =Color.Black;                          
					cell_SerialNoFirst .Wrap =true;
					cell_SerialNoFirst .Width =15;
					cell_SerialNoFirst .Height =10;

					//cell_SerialNoFirst .BorderStyle =BorderStyle.Double; 
 
					cell_SerialNoFirst .ColumnSpan =0;
					cell_SerialNoFirst.Font .Size =6;
					cell_SerialNoFirst.Text=serialNo.ToString ();
					rowInsertFirst.Cells.Add(cell_SerialNoFirst);
				
			
		
					//TableCell cell14=new TableCell();
					TableCell cell_ClientCount_PretrialStatusFirst=new TableCell ();
					//cell_ClientCount_PretrialStatusFirst .BorderStyle =BorderStyle.Double;
					cell_ClientCount_PretrialStatusFirst.Font .Size =7;
                    
                    cell_ClientCount_PretrialStatusFirst.BorderColor = Color.Black;

					cell_ClientCount_PretrialStatusFirst .Wrap =true;
					cell_ClientCount_PretrialStatusFirst .Width=110;
					cell_ClientCount_PretrialStatusFirst .Height =25;
					cell_ClientCount_PretrialStatusFirst.Text=Ds_FillTable.Tables[0].Rows[0]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
					rowInsertFirst.Cells.Add(cell_ClientCount_PretrialStatusFirst);

					//					TableCell cell_Blank=new TableCell ();
					//					cell_Blank.Font .Size =6;
					//					cell_Blank.Font .Name="Arial" ;
					//					cell_Blank.Wrap =true;
					//					cell_Blank.Width=20;
					//					cell_Blank.Height =10;
					//					cell_Blank.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["space"].ToString();
					//					rowInsertFirst.Cells.Add(cell_Blank);

					//TableCell cell15=new TableCell();
					TableCell cell_violationcount_lastnameFirst=new TableCell ();
					cell_violationcount_lastnameFirst.Font .Size =7;
					//cell_violationcount_lastnameFirst .BorderStyle =BorderStyle.Double;
                    cell_violationcount_lastnameFirst.BorderColor = Color.Black;
					cell_violationcount_lastnameFirst .Font .Name="Arial" ;
					cell_violationcount_lastnameFirst .Wrap =true;
					cell_violationcount_lastnameFirst .Width=50;
					cell_violationcount_lastnameFirst .Height =10;
					cell_violationcount_lastnameFirst.Text=Ds_FillTable.Tables[0].Rows[0]["violationcount"].ToString()+
						Ds_FillTable.Tables[0].Rows[0]["Client_lastname"].ToString();
					rowInsertFirst.Cells.Add(cell_violationcount_lastnameFirst);

				

					//TableCell cell16=new TableCell();
					TableCell cell_FirstName_MiddleNameFirst=new TableCell ();
					cell_FirstName_MiddleNameFirst.Font .Size =7;
					//cell_FirstName_MiddleNameFirst .BorderStyle =BorderStyle.Double;

                    cell_FirstName_MiddleNameFirst.BorderColor = Color.Black;

					cell_FirstName_MiddleNameFirst.Text=Ds_FillTable.Tables[0].Rows[0]["FMName"].ToString();
					cell_FirstName_MiddleNameFirst .Wrap =true;
					cell_FirstName_MiddleNameFirst .Width=50;
					cell_FirstName_MiddleNameFirst .Height =10; 
					rowInsertFirst.Cells.Add(cell_FirstName_MiddleNameFirst);

	
					//--------------------------------------------------------------------------
					//Blank Column between first Name and Offence Date In first Row First Grid. 
					//--------------------------------------------------------------------------
					TableCell cell_Blank1=new TableCell ();
					cell_Blank1.Font .Size =7;
					cell_Blank1.Font .Name="Arial" ;
                    cell_Blank1.BorderColor = Color.Black;
					cell_Blank1.Wrap =true;
					cell_Blank1.Width=20;
					cell_Blank1.Height =10;
					cell_Blank1.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["space"].ToString();
					rowInsertFirst.Cells.Add(cell_Blank1);
					//=====================================================================
		
						

					//First Row First Grid 	
					TableCell cell_OffenceDateFirst =new TableCell ();
					cell_OffenceDateFirst.BorderColor=Color.Black;
					cell_OffenceDateFirst.ForeColor =Color.Black;
					cell_OffenceDateFirst.Font .Size =7;
					cell_OffenceDateFirst .Width =40;
					cell_OffenceDateFirst .Height =10;
					cell_OffenceDateFirst .Wrap=true;
					cell_OffenceDateFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["offenceDate"].ToString();
					rowInsertFirst.Cells.Add(cell_OffenceDateFirst);



					//TableCell cell17=new TableCell();
					TableCell cell_BondingFirst=new TableCell ();
					cell_BondingFirst.Font .Size =7;
					cell_BondingFirst.Font .Bold =true;

                    cell_BondingFirst.BorderColor = Color.Black;


					cell_BondingFirst .Wrap =true; 
					cell_BondingFirst .Width =20;
					cell_BondingFirst .Height =10; 
					//cell_BondingFirst .BorderStyle =BorderStyle.Double;
					cell_BondingFirst.Text=Ds_FillTable .Tables[0].Rows[0]["BondFlag"].ToString();
					rowInsertFirst.Cells.Add(cell_BondingFirst);
              
					//TableCell cell18=new TableCell();
					TableCell cell_CourtTimeFirst=new TableCell ();
					//cell_CourtTimeFirst .BorderStyle =BorderStyle.Double;
					cell_CourtTimeFirst.Font .Size =6;
                    cell_CourtTimeFirst.BorderColor = Color.Black;

					cell_CourtTimeFirst.Font .Bold =true;
					cell_CourtTimeFirst .Width=45;
					cell_CourtTimeFirst .Height =10;
					cell_CourtTimeFirst .Wrap =true;
					cell_CourtTimeFirst.Text=Ds_FillTable.Tables[0].Rows[0]["currentdateset"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtTimeFirst);

					//TableCell cell19=new TableCell();
					TableCell cell_CourtNumFirst=new TableCell ();
					cell_CourtNumFirst.Font .Size =7;
					cell_CourtNumFirst .Width =23;
					cell_CourtNumFirst .Wrap =true;
					cell_CourtNumFirst .Height =10;
                    cell_CourtNumFirst.BorderColor = Color.Black;

					//cell_CourtNumFirst .BorderStyle =BorderStyle.Double;
					cell_CourtNumFirst.Text="&nbsp;"+"#"+Ds_FillTable.Tables[0].Rows[0]["currentcourtnum"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtNumFirst);
						
					//TableCell cell20=new TableCell();
					TableCell cell_Trial_DescriptionFirst=new TableCell ();
					cell_Trial_DescriptionFirst.Font.Size =7;
					cell_Trial_DescriptionFirst .Width=22;
					cell_Trial_DescriptionFirst .Wrap =true;
					cell_Trial_DescriptionFirst .Font .Bold =true;
					cell_Trial_DescriptionFirst .Height =10;
                    cell_Trial_DescriptionFirst.BorderColor = Color.Black;

					//cell_Trial_DescriptionFirst .BorderStyle =BorderStyle.Double;
					cell_Trial_DescriptionFirst.Text=Ds_FillTable.Tables[0].Rows[0]["Description"].ToString();
					rowInsertFirst.Cells.Add(cell_Trial_DescriptionFirst );

					//TableCell cell21=new TableCell();
					TableCell cell_OFirstName_LastNameFirst=new TableCell ();            					      
					cell_OFirstName_LastNameFirst.Font.Size =7;
					cell_OFirstName_LastNameFirst .Width=75;	
					cell_OFirstName_LastNameFirst .Height =10;
					cell_OFirstName_LastNameFirst .Wrap =true;
                    cell_OFirstName_LastNameFirst.BorderColor = Color.Black;

					//cell_OFirstName_LastNameFirst .BorderStyle =BorderStyle.Double;
					cell_OFirstName_LastNameFirst.Text=Ds_FillTable.Tables[0].Rows[0]["OLFname"].ToString();
					rowInsertFirst.Cells.Add(cell_OFirstName_LastNameFirst);

					// First Row --- First Grid
					TableCell cell_MidNumFirst=new TableCell ();            					      
					cell_MidNumFirst.BorderColor=Color.Black ;
					cell_MidNumFirst.ForeColor =Color.Black;
					cell_MidNumFirst.Font .Size =7;
					cell_MidNumFirst.Wrap=true;
					cell_MidNumFirst.Width=35;
					cell_MidNumFirst.Height =10;
					cell_MidNumFirst.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[0]["midnum"].ToString();
					rowInsertFirst.Cells.Add(cell_MidNumFirst);


					//TableCell cell22=new TableCell();
					TableCell cell_TrialCommentsFirst=new TableCell ();
					//cell_TrialCommentsFirst .BorderStyle =BorderStyle.Double;
					cell_TrialCommentsFirst.Font.Size =7;
					cell_TrialCommentsFirst .Width=140;
					cell_TrialCommentsFirst .Height =10;
					cell_TrialCommentsFirst .Wrap =true;
                    cell_TrialCommentsFirst.BorderColor = Color.Black;
					cell_TrialCommentsFirst.Text=Ds_FillTable.Tables[0].Rows[0]["Shortdesc1"].ToString()+
						"<Font Color=Red>"+Ds_FillTable.Tables[0].Rows[0]["trialcomments"].ToString()+"</font>";
					rowInsertFirst.Cells.Add(cell_TrialCommentsFirst);
					Table_Print.Rows.Add(rowInsertFirst);
				
					#endregion
					//Finish Adding First Record

           
					for (int IdxRow=1;IdxRow<=Ds_FillTable.Tables [0].Rows .Count-1 ;IdxRow++)
					{
					
						if(Ds_FillTable.Tables[0].Rows[IdxRow][10].ToString()!=Ds_FillTable.Tables[0].Rows[IdxRow-1][10].ToString())
						{	
							LeaveOneRow();
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtName=new TableCell ();
							Cell_CourtName .Font .Size =10;
							Cell_CourtName.Font .Bold =true;
							Cell_CourtName.Font .Underline=true;
							//Cell_CourtName.BorderStyle =BorderStyle.Solid ;
							Cell_CourtName.ColumnSpan=11;
							Cell_CourtName .HorizontalAlign=HorizontalAlign.Left ;
                            Cell_CourtName.BorderStyle = BorderStyle.None; 			
							Cell_CourtName.Text=Ds_FillTable.Tables [0].Rows[IdxRow][10].ToString ();
							rowspan.Cells .Add(Cell_CourtName);
							Table_Print .Rows .Add(rowspan);		               
							LeaveOneRow();
							

						}
						if(Ds_FillTable.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString()!=Ds_FillTable.Tables[0].Rows[IdxRow-1]["currentcourtnum"].ToString()
							&& Ds_FillTable.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString()!=null 
							&& Ds_FillTable.Tables[0].Rows[IdxRow-1]["currentcourtnum"].ToString()!=null
							&& Ds_FillTable.Tables[0].Rows[IdxRow-1]["CourtName_Address_DateSet"].ToString() == Ds_FillTable.Tables[0].Rows[IdxRow]["CourtName_Address_DateSet"].ToString())
						{	
							
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtNum=new TableCell ();
							Cell_CourtNum .Font .Size =7;
							Cell_CourtNum .ColumnSpan=13;
							//cell_CourtNumFirst .BorderStyle =BorderStyle.Solid ;
							Cell_CourtNum  .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtNum  .Text="<br>";

							Cell_CourtNum.BackColor=System.Drawing.Color.Gray;
																	  																  ;
							
							rowspan.Cells .Add(	Cell_CourtNum);
							Cell_CourtNum .Height =11;
							Table_Print .Rows .Add(rowspan);		               
										
						}

						TableRow rowInsert =new TableRow ();	
 
					//	rowInsert .BorderStyle =BorderStyle.Solid; 
						serialNo =IdxRow+1;
						TableCell cell_SerialNo=new TableCell ();
					
						cell_SerialNo.Text=serialNo.ToString ();
						cell_SerialNo .BorderStyle =BorderStyle.Solid;
                        
                        cell_SerialNo.BorderColor = Color.Black;

						rowInsert.Cells.Add(cell_SerialNo);
						cell_SerialNo.Font .Size=7;
						cell_SerialNo.Width=10;
						cell_SerialNo.Wrap =true;
						cell_SerialNo .Height =10;
						//TableCell cell14=new TableCell();
						TableCell cell_ClientCount_PretrialStatus=new TableCell ();
						cell_ClientCount_PretrialStatus.BorderStyle =BorderStyle.Double;

                        cell_ClientCount_PretrialStatus.BorderColor = Color.Black;

                        cell_ClientCount_PretrialStatus .Font .Size=7;
						cell_ClientCount_PretrialStatus .Width=110;
						cell_ClientCount_PretrialStatus .Wrap =true;
						cell_ClientCount_PretrialStatus.Height =10;
						cell_ClientCount_PretrialStatus.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
						rowInsert.Cells.Add(cell_ClientCount_PretrialStatus);

						//						
						//						TableCell cell_Blank1=new TableCell ();
						//						cell_Blank1.Font .Size =6;
						//						cell_Blank1.Font .Name="Arial" ;
						//						cell_Blank1.Wrap =true;
						//						cell_Blank1.Width=20;
						//						cell_Blank1.Height =10;
						//						cell_Blank1.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["space"].ToString();
						//						rowInsert.Cells.Add(cell_Blank1);

						
						
						//TableCell cell15=new TableCell();
						TableCell cell_violationcount_lastname=new TableCell ();
						//cell_violationcount_lastname.BorderStyle =BorderStyle.Double;
						cell_violationcount_lastname .Font .Size =7;
						cell_violationcount_lastname .Width=50;
						cell_violationcount_lastname .Wrap =true;
                        cell_violationcount_lastname.BorderColor = Color.Black;

						cell_violationcount_lastname .Height =25;
						cell_violationcount_lastname.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["violationcount"].ToString()+
							Ds_FillTable.Tables[0].Rows[IdxRow]["Client_lastname"].ToString();
						rowInsert.Cells.Add(cell_violationcount_lastname);
					

					

						//TableCell cell16=new TableCell();
						TableCell cell_FirstName_MiddleName=new TableCell ();
						cell_FirstName_MiddleName.BorderStyle =BorderStyle.Solid;
                        cell_FirstName_MiddleName.BorderColor = Color.Black;

						cell_FirstName_MiddleName .Font .Size =7;
						cell_FirstName_MiddleName .Width=50;
						cell_FirstName_MiddleName .Wrap =true;
						cell_FirstName_MiddleName .Height =10;
						cell_FirstName_MiddleName.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["FMName"].ToString();
						rowInsert.Cells.Add(cell_FirstName_MiddleName);
					
						//--------------------------------------------------------------------------
						//Blank Column between first Name and Offence Date In inner Row First Grid. 
						//--------------------------------------------------------------------------
						TableCell cell_Blank11=new TableCell ();
						cell_Blank11.Font .Size =7;
						cell_Blank11.Font .Name="Arial" ;
                        
                        cell_Blank11.BorderColor = Color.Black;

						cell_Blank11.Wrap =true;
						cell_Blank11.Width=20;
						cell_Blank11.Height =10;
						cell_Blank11.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["space"].ToString();
						rowInsert.Cells.Add(cell_Blank11);
						//=====================================================================
		
						

						// inner Rows -- First Grid
						TableCell cell_OffenceDate =new TableCell ();
						cell_OffenceDate.BorderColor=Color.Black;
						cell_OffenceDate.ForeColor =Color.Black;
						cell_OffenceDate.Font .Size =7;
						cell_OffenceDate .Width=35;
						cell_OffenceDate .Height =10;
						cell_OffenceDate .Wrap=true;
						cell_OffenceDate.Text="&nbsp;"+Ds_FillTable.Tables[0].Rows[IdxRow]["offenceDate"].ToString();
						rowInsert.Cells.Add(cell_OffenceDate);
						

						//TableCell cell17=new TableCell();
						TableCell cell_Bonding=new TableCell ();
						cell_Bonding.BorderStyle =BorderStyle.Double;
                        cell_Bonding.BorderColor = Color.Black;

						cell_Bonding.Text=Ds_FillTable .Tables[0].Rows[IdxRow]["BondFlag"].ToString();
						cell_Bonding .Width =20;
						cell_Bonding .Wrap =true;
						cell_Bonding .Height =10;
						cell_Bonding.Font .Size =7;
						rowInsert.Cells.Add(cell_Bonding);
					

						//TableCell cell18=new TableCell();
						TableCell cell_CourtTime=new TableCell ();
						cell_CourtTime.BorderStyle =BorderStyle.Solid;
                        cell_CourtTime.BorderColor = Color.Black;

						cell_CourtTime.Font .Size =7;
						cell_CourtTime.Font .Bold =true;
						cell_CourtTime.Width=45;
						cell_CourtTime .Height =10;
						cell_CourtTime .Wrap =true;			
						cell_CourtTime.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["currentdateset"].ToString();
						rowInsert.Cells.Add(cell_CourtTime);
					

						//TableCell cell19=new TableCell();
						TableCell cell_CourtNum=new TableCell ();
						cell_CourtNum.BorderStyle =BorderStyle.Solid;
                        cell_CourtNum.BorderColor = Color.Black;

						cell_CourtNum.Font .Size =7;
						cell_CourtNum .Width =23;
						cell_CourtNum .Wrap=true;
						cell_CourtNum .Height =10;
						cell_CourtNum.Text="&nbsp;"+"#"+Ds_FillTable.Tables[0].Rows[IdxRow]["currentcourtnum"].ToString();
						rowInsert.Cells.Add(cell_CourtNum);
					
						
						//TableCell cell20=new TableCell();
						TableCell cell_Trial_Description=new TableCell ();
						cell_Trial_Description.BorderStyle =BorderStyle.Solid;
                        cell_Trial_Description.BorderColor = Color.Black;
						cell_Trial_Description.Font .Size =7;
						cell_Trial_Description .Font .Bold =true;
						cell_Trial_Description .Width=22;						
						cell_Trial_Description .Wrap =true;
						cell_Trial_Description .Height =10;
						cell_Trial_Description.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["Description"].ToString();
						rowInsert.Cells.Add(cell_Trial_Description );
					

						//TableCell cell21=new TableCell();
						TableCell cell_OFirstName_LastName=new TableCell ();            					      
						cell_OFirstName_LastName.BorderStyle = BorderStyle.Solid;
                        cell_OFirstName_LastName.BorderColor = Color.Black;

						cell_OFirstName_LastName.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["OLFname"].ToString();
						cell_OFirstName_LastName.Font .Size =7;
						cell_OFirstName_LastName .Width=75;;
						cell_OFirstName_LastName .Height =10;
						cell_OFirstName_LastName .Wrap =true;
						rowInsert.Cells.Add(cell_OFirstName_LastName);
					
						
					
						// Inner Row --- first Grid. 
						TableCell cell_midnum =new TableCell ();            					      
						cell_midnum.BorderColor=Color.Black ;
						cell_midnum.ForeColor =Color.Black;
						cell_midnum.Font .Size =7;										
						cell_midnum .Width=35;
						cell_midnum .Height =10;
						cell_midnum .Wrap =true;
						cell_midnum.Text=Ds_FillTable.Tables[0] .Rows[IdxRow]["midnum"].ToString();
						rowInsert.Cells.Add(cell_midnum);


						//TableCell cell22=new TableCell();
						TableCell cell_TrialComments=new TableCell ();
						cell_TrialComments.BorderStyle =BorderStyle.Solid;
                        cell_TrialComments.BorderColor = Color.Black;

						cell_TrialComments.Font .Size =7;
						cell_TrialComments .Width=140;
						cell_TrialComments .Wrap =true;
						cell_TrialComments .Height =10;
						cell_TrialComments.Text=Ds_FillTable.Tables[0].Rows[IdxRow]["Shortdesc1"].ToString()+
							"<Font Color=Red>"+Ds_FillTable.Tables[0].Rows[IdxRow]["trialcomments"].ToString()+"</font>";
						rowInsert.Cells.Add(cell_TrialComments);
						Table_Print.Rows.Add(rowInsert);		
					}
				}
				if(Ds_FillTable.Tables [1].Rows .Count !=0)
				{
					LeaveOneRow();
					//Inserting First Court Header
					#region
					TableRow Firstrowspan=new TableRow ();
					//Firstrowspan.BorderStyle =BorderStyle.Solid;
                    //Firstrowspan.BorderColor = Color.Black;

					TableCell Cell_CourtName_FirstRec=new TableCell ();
					Cell_CourtName_FirstRec.ColumnSpan =11; 
					Cell_CourtName_FirstRec.BorderStyle =BorderStyle.Solid;
                   // Cell_CourtName_FirstRec.BorderColor = Color.Black;

					Cell_CourtName_FirstRec.HorizontalAlign=HorizontalAlign.Left ;
					Cell_CourtName_FirstRec.Font.Bold=true;
					Cell_CourtName_FirstRec.Font.Underline =true;
					Cell_CourtName_FirstRec.Font .Size =10;				
				                
					Cell_CourtName_FirstRec.Text=Ds_FillTable.Tables[1].Rows[0]["CourtName_Address_DateSet"].ToString () ;
					Firstrowspan.Cells .Add(Cell_CourtName_FirstRec);
					Table_Print .Rows .Add (Firstrowspan );
					LeaveOneRow();
	
					#endregion
					//Finish Inserting First CourtHeader

					//Insertint First Record
					#region

					TableRow rowInsertFirst =new TableRow ();
					//rowInsertFirst.BorderStyle=BorderStyle.Solid;   

					serialNo +=1;
					TableCell cell_SerialNoFirst=new TableCell ();
					//cell_SerialNoFirst.BorderColor=Color.Black ;
					cell_SerialNoFirst.ForeColor =Color.Black;                          
					cell_SerialNoFirst .Wrap =true;
					cell_SerialNoFirst .Width =15;
					cell_SerialNoFirst .Height =10;
 
					cell_SerialNoFirst .ColumnSpan =0;
					cell_SerialNoFirst.Font .Size =7;
					cell_SerialNoFirst.Text=serialNo.ToString ();
					rowInsertFirst.Cells.Add(cell_SerialNoFirst);
				
					//TableCell cell14=new TableCell();
					TableCell cell_ClientCount_PretrialStatusFirst=new TableCell ();
					//cell_ClientCount_PretrialStatusFirst .BorderStyle =BorderStyle.Double;
					cell_ClientCount_PretrialStatusFirst.Font .Size =7;
					cell_ClientCount_PretrialStatusFirst .Wrap =true;
					cell_ClientCount_PretrialStatusFirst .Width=110;
					cell_ClientCount_PretrialStatusFirst .Height =10;
					cell_ClientCount_PretrialStatusFirst.Text=Ds_FillTable.Tables[1].Rows[0]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
					rowInsertFirst.Cells.Add(cell_ClientCount_PretrialStatusFirst);
//
//					TableCell cell_Blank=new TableCell ();
//					cell_Blank.Font .Size =6;
//					cell_Blank.Font .Name="Arial" ;
//					cell_Blank.Wrap =true;
//					cell_Blank.Width=20;
//					cell_Blank.Height =10;
//					cell_Blank.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[0]["space"].ToString();
//					rowInsertFirst.Cells.Add(cell_Blank);

					//TableCell cell15=new TableCell();
					TableCell cell_violationcount_lastnameFirst=new TableCell ();
					cell_violationcount_lastnameFirst.Font .Size =7;
					//cell_violationcount_lastnameFirst .BorderStyle =BorderStyle.Double;
					cell_violationcount_lastnameFirst .Font .Name="Arial" ;
					cell_violationcount_lastnameFirst .Wrap =true;
					cell_violationcount_lastnameFirst .Width=50;
					cell_violationcount_lastnameFirst .Height =25;
					cell_violationcount_lastnameFirst.Text=Ds_FillTable.Tables[1].Rows[0]["violationcount"].ToString()+
						Ds_FillTable.Tables[1].Rows[0]["Client_lastname"].ToString();
					rowInsertFirst.Cells.Add(cell_violationcount_lastnameFirst);

				

					//TableCell cell16=new TableCell();
					TableCell cell_FirstName_MiddleNameFirst=new TableCell ();
					cell_FirstName_MiddleNameFirst.Font .Size =7;
					//cell_FirstName_MiddleNameFirst .BorderStyle =BorderStyle.Double;
					cell_FirstName_MiddleNameFirst.Text=Ds_FillTable.Tables[1].Rows[0]["FMName"].ToString();
					cell_FirstName_MiddleNameFirst .Wrap =true;
					cell_FirstName_MiddleNameFirst .Width=50;
					cell_FirstName_MiddleNameFirst .Height =10;
					rowInsertFirst.Cells.Add(cell_FirstName_MiddleNameFirst);


					//--------------------------------------------------------------------------
					//Blank Column between first Name and Offence Date In first Row Second Grid. 
					//--------------------------------------------------------------------------
					TableCell cell_Blank1=new TableCell ();
					cell_Blank1.Font .Size =7;
					cell_Blank1.Font .Name="Arial" ;
					cell_Blank1.Wrap =true;
					cell_Blank1.Width=20;
					cell_Blank1.Height =10;
					cell_Blank1.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[0]["space"].ToString();
					rowInsertFirst.Cells.Add(cell_Blank1);
					//=====================================================================
		
						


						
					//First Row Second Grid 	
					TableCell cell_OffenceDateFirst =new TableCell ();
					cell_OffenceDateFirst.BorderColor=Color.Black ;
					//cell_OffenceDateFirst.ForeColor =Color.Black;
					cell_OffenceDateFirst.Font .Size =7;
					cell_OffenceDateFirst .Width=35;
					cell_OffenceDateFirst .Height =10;
					cell_OffenceDateFirst .Wrap=true;
					cell_OffenceDateFirst.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[0]["offenceDate"].ToString();
					rowInsertFirst.Cells.Add(cell_OffenceDateFirst);



					//TableCell cell17=new TableCell();
					TableCell cell_BondingFirst=new TableCell ();
					cell_BondingFirst.Font .Size =7;
					cell_BondingFirst.Font .Bold =true;
			
					cell_BondingFirst .Wrap =true; 
					cell_BondingFirst .Width =20;
					cell_BondingFirst .Height =10; 
					//cell_BondingFirst .BorderStyle =BorderStyle.Double;
					cell_BondingFirst.Text=Ds_FillTable .Tables[1].Rows[0]["BondFlag"].ToString();
					rowInsertFirst.Cells.Add(cell_BondingFirst);
              
					//TableCell cell18=new TableCell();
					TableCell cell_CourtTimeFirst=new TableCell ();
					//cell_CourtTimeFirst .BorderStyle =BorderStyle.Double;
					cell_CourtTimeFirst.Font .Size =7;
					cell_CourtTimeFirst.Font .Bold =true;
					cell_CourtTimeFirst .Width=45;
					cell_CourtTimeFirst .Height =10;
					cell_CourtTimeFirst .Wrap =true;
					cell_CourtTimeFirst.Text=Ds_FillTable.Tables[1].Rows[0]["currentdateset"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtTimeFirst);

					//TableCell cell19=new TableCell();
					TableCell cell_CourtNumFirst=new TableCell ();
					cell_CourtNumFirst.Font .Size =7;
					cell_CourtNumFirst .Width =23;
					cell_CourtNumFirst .Wrap =true;
					cell_CourtNumFirst .Height =10;
					//cell_CourtNumFirst .BorderStyle =BorderStyle.Double;
					cell_CourtNumFirst.Text="&nbsp;"+"#"+Ds_FillTable.Tables[1].Rows[0]["currentcourtnum"].ToString();
					rowInsertFirst.Cells.Add(cell_CourtNumFirst);
						
					//TableCell cell20=new TableCell();
					TableCell cell_Trial_DescriptionFirst=new TableCell ();
					cell_Trial_DescriptionFirst.Font.Size =7;
					cell_Trial_DescriptionFirst .Width=22;
					cell_Trial_DescriptionFirst .Wrap =true;
					cell_Trial_DescriptionFirst .Font .Bold =true;
					cell_Trial_DescriptionFirst .Height =10;
					//cell_Trial_DescriptionFirst .BorderStyle =BorderStyle.Double;
					cell_Trial_DescriptionFirst.Text=Ds_FillTable.Tables[1].Rows[0]["Description"].ToString();
					rowInsertFirst.Cells.Add(cell_Trial_DescriptionFirst );

					//TableCell cell21=new TableCell();
					TableCell cell_OFirstName_LastNameFirst=new TableCell ();            					      
					cell_OFirstName_LastNameFirst.Font.Size =7;
					cell_OFirstName_LastNameFirst .Width=75;
					cell_OFirstName_LastNameFirst .Height =10;
					cell_OFirstName_LastNameFirst .Wrap =true;
					//cell_OFirstName_LastNameFirst .BorderStyle =BorderStyle.Double;
					cell_OFirstName_LastNameFirst.Text=Ds_FillTable.Tables[1].Rows[0]["OLFname"].ToString();
					rowInsertFirst.Cells.Add(cell_OFirstName_LastNameFirst);

					
					// First Row --- Second Grid
					TableCell cell_MidNumFirst=new TableCell ();            					      
					cell_MidNumFirst.BorderColor=Color.Black ;
					cell_MidNumFirst.ForeColor =Color.Black;
					cell_MidNumFirst.Font .Size =7;
					cell_MidNumFirst.Wrap=true;
					cell_MidNumFirst.Width=35;
					cell_MidNumFirst.Height =10;
					cell_MidNumFirst.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[0]["midnum"].ToString();
					rowInsertFirst.Cells.Add(cell_MidNumFirst);
						

					//TableCell cell22=new TableCell();
					TableCell cell_TrialCommentsFirst=new TableCell ();
					//cell_TrialCommentsFirst .BorderStyle =BorderStyle.Double;
					cell_TrialCommentsFirst.Font.Size =7;
					cell_TrialCommentsFirst .Width=140;
					cell_TrialCommentsFirst .Height =10;
					cell_TrialCommentsFirst .Wrap =true;
					cell_TrialCommentsFirst.Text=Ds_FillTable.Tables[1].Rows[0]["Shortdesc1"].ToString()+
						"<Font Color=Red>"+Ds_FillTable.Tables[1].Rows[0]["trialcomments"].ToString()+"</font>";
					rowInsertFirst.Cells.Add(cell_TrialCommentsFirst);
					Table_Print.Rows.Add(rowInsertFirst);
                    
                    //Table_Print.BorderStyle = BorderStyle.Solid;
                    //Table_Print.BorderWidth = 1;



					#endregion
					//Finish Adding First Record

           
					for (int IdxRow=1;IdxRow<=Ds_FillTable.Tables [1].Rows .Count-1 ;IdxRow++)
					{
					
						if(Ds_FillTable.Tables[1].Rows[IdxRow][10].ToString()!=Ds_FillTable.Tables[1].Rows[IdxRow-1][10].ToString())
						{	
							LeaveOneRow();
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtName=new TableCell ();
							Cell_CourtName .Font .Size =10;
							Cell_CourtName.Font .Bold =true;
							Cell_CourtName.Font .Underline=true;
							Cell_CourtName.BorderStyle =BorderStyle.None ;
							Cell_CourtName.ColumnSpan=11;
							Cell_CourtName .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtName_FirstRec.BorderStyle=BorderStyle.None; 			
							Cell_CourtName.Text=Ds_FillTable.Tables [1].Rows[IdxRow][10].ToString ();
							rowspan.Cells .Add(Cell_CourtName);
							Table_Print .Rows .Add(rowspan);		               
							LeaveOneRow();
							

							//						TableRow Emptyrow=new TableRow ();
							//					   TableCell Cell_EmptyCell=new TableCell ();
							//					   Cell_EmptyCell .Text=" ";
							//					   Cell_EmptyCell.ColumnSpan =10;
							//					   Emptyrow.Cells .Add (Cell_EmptyCell);
							//					   Table_OutPut .Rows.Add (Emptyrow);
						}
						if(Ds_FillTable.Tables[1].Rows[IdxRow]["currentcourtnum"].ToString()!=Ds_FillTable.Tables[1] .Rows[IdxRow-1]["currentcourtnum"].ToString()
							&& Ds_FillTable.Tables[1] .Rows[IdxRow]["currentcourtnum"].ToString()!=null 
							&& Ds_FillTable.Tables[1] .Rows[IdxRow-1]["currentcourtnum"].ToString()!=null
							&& Ds_FillTable.Tables[1].Rows[IdxRow-1]["CourtName_Address_DateSet"].ToString() == Ds_FillTable.Tables[1].Rows[IdxRow]["CourtName_Address_DateSet"].ToString())
						{	
							VarChk +=1;
							TableRow rowspan=new TableRow ();						
							TableCell Cell_CourtNum=new TableCell ();
							Cell_CourtNum .Font .Size =7;
							Cell_CourtNum .ColumnSpan=13;
							cell_CourtNumFirst .BorderStyle =BorderStyle.NotSet ;
							Cell_CourtNum  .HorizontalAlign=HorizontalAlign.Left ;
							Cell_CourtNum  .Text="<br>";
							Cell_CourtNum .Height =11;
							Cell_CourtNum.BackColor=System.Drawing.Color.DarkGray;
							rowspan.Cells .Add(	Cell_CourtNum);
							Table_Print .Rows .Add(rowspan);		               
						}

						TableRow rowInsert =new TableRow ();	
 
						rowInsert .BorderStyle =BorderStyle.Solid; 
						serialNo +=1;
						TableCell cell_SerialNo=new TableCell ();
					
						cell_SerialNo.Text=serialNo.ToString ();
						//cell_SerialNo .BorderStyle =BorderStyle.Double;
						rowInsert.Cells.Add(cell_SerialNo);
						cell_SerialNo.Font .Size=7;
						cell_SerialNo.Width=10;
						cell_SerialNo.Wrap =true;
						cell_SerialNo .Height =10;
						//TableCell cell14=new TableCell();
						TableCell cell_ClientCount_PretrialStatus=new TableCell ();
						//cell_ClientCount_PretrialStatus.BorderStyle =BorderStyle.Double;
						cell_ClientCount_PretrialStatus .Font .Size=6;
						cell_ClientCount_PretrialStatus .Width=110;
						cell_ClientCount_PretrialStatus .Wrap =true;
						cell_ClientCount_PretrialStatus.Height =10;
						cell_ClientCount_PretrialStatus.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["ClientCount_PreTrialStatus_CDLFLAG"].ToString();
						rowInsert.Cells.Add(cell_ClientCount_PretrialStatus);

						
						
						//TableCell cell15=new TableCell();
						TableCell cell_violationcount_lastname=new TableCell ();
						//cell_violationcount_lastname.BorderStyle =BorderStyle.Double;
						cell_violationcount_lastname .Font .Size =7;
						cell_violationcount_lastname .Width=50;
						cell_violationcount_lastname .Wrap =true;
						cell_violationcount_lastname .Height =10;
						cell_violationcount_lastname.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["violationcount"].ToString()+
							Ds_FillTable.Tables[1] .Rows[IdxRow]["Client_lastname"].ToString();
						rowInsert.Cells.Add(cell_violationcount_lastname);
		

						//TableCell cell16=new TableCell();
						TableCell cell_FirstName_MiddleName=new TableCell ();
						//cell_FirstName_MiddleName.BorderStyle =BorderStyle.Double;
						cell_FirstName_MiddleName .Font .Size =7;
						cell_FirstName_MiddleName .Width=50;
						cell_FirstName_MiddleName .Wrap =true;
						cell_FirstName_MiddleName .Height =10;
						cell_FirstName_MiddleName.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["FMName"].ToString();
						rowInsert.Cells.Add(cell_FirstName_MiddleName);
						//--------------------------------------------------------------------------
						//Blank Column between first Name and Offence Date In inner Row second Grid. 
						//--------------------------------------------------------------------------
						TableCell cell_Blank12=new TableCell ();
						cell_Blank12.Font .Size =7;
						cell_Blank12.Font .Name="Arial" ;
						cell_Blank12.Wrap =true;
						cell_Blank12.Width=20;
						cell_Blank12.Height =10;
						cell_Blank12.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[IdxRow]["space"].ToString();
						rowInsert.Cells.Add(cell_Blank12);
						//=====================================================================

						// inner Rows -- Second Grid
						TableCell cell_OffenceDate =new TableCell ();
						cell_OffenceDate.BorderColor=Color.Black ;
						//cell_OffenceDate.ForeColor =Color.Black;
						cell_OffenceDate.Font .Size =7;
						cell_OffenceDate .Width=35;
						cell_OffenceDate .Height =10;
						cell_OffenceDate .Wrap=true;
						cell_OffenceDate.Text="&nbsp;"+Ds_FillTable.Tables[1].Rows[IdxRow]["offenceDate"].ToString();
						rowInsert.Cells.Add(cell_OffenceDate);

						//TableCell cell17=new TableCell();
						TableCell cell_Bonding=new TableCell ();
						//cell_Bonding.BorderStyle =BorderStyle.Double;
						cell_Bonding.Text=Ds_FillTable .Tables[1] .Rows[IdxRow]["BondFlag"].ToString();
						cell_Bonding .Width =20;
						cell_Bonding .Wrap =true;
						cell_Bonding .Height =10;
						cell_Bonding.Font .Size =7;
						rowInsert.Cells.Add(cell_Bonding);
					

						//TableCell cell18=new TableCell();
						TableCell cell_CourtTime=new TableCell ();
						//cell_CourtTime.BorderStyle =BorderStyle.Double;
						cell_CourtTime.Font .Size =7;
						cell_CourtTime.Font .Bold =true;
						cell_CourtTime.Width=45;
						cell_CourtTime .Height =10;
						cell_CourtTime .Wrap =true;			
						cell_CourtTime.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["currentdateset"].ToString();
						rowInsert.Cells.Add(cell_CourtTime);
					

						//TableCell cell19=new TableCell();
						TableCell cell_CourtNum=new TableCell ();
						//cell_CourtNum.BorderStyle =BorderStyle.Double;
						cell_CourtNum.Font .Size =7;
						cell_CourtNum .Width =23;
						cell_CourtNum .Wrap=true;
						cell_CourtNum .Height =10;
						cell_CourtNum.Text="&nbsp;"+"#"+Ds_FillTable.Tables[1] .Rows[IdxRow]["currentcourtnum"].ToString();
						rowInsert.Cells.Add(cell_CourtNum);
					
						
						//TableCell cell20=new TableCell();
						TableCell cell_Trial_Description=new TableCell ();
						//cell_Trial_Description.BorderStyle =BorderStyle.Double;				
						cell_Trial_Description.Font .Size =7;
						cell_Trial_Description .Font .Bold =true;
						cell_Trial_Description .Width=22;						
						cell_Trial_Description .Wrap =true;
						cell_Trial_Description .Height =10;
						cell_Trial_Description.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["Description"].ToString();
						rowInsert.Cells.Add(cell_Trial_Description );

						//TableCell cell21=new TableCell();
						TableCell cell_OFirstName_LastName=new TableCell ();            					      
						//cell_OFirstName_LastName.BorderStyle =BorderStyle.Double;														
						cell_OFirstName_LastName.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["OLFname"].ToString();
						cell_OFirstName_LastName.Font .Size =7;
						cell_OFirstName_LastName .Width=75;
						cell_OFirstName_LastName .Height =10;
						cell_OFirstName_LastName .Wrap =true;
						rowInsert.Cells.Add(cell_OFirstName_LastName);

						TableCell cell_midnum =new TableCell ();            					      
						cell_midnum.BorderColor=Color.Black ;
						cell_midnum.ForeColor =Color.Black;
						cell_midnum.Font .Size =7;										
						cell_midnum .Width=35;
						cell_midnum .Height =10;
						cell_midnum .Wrap =true;
						cell_midnum.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["midnum"].ToString();
						rowInsert.Cells.Add(cell_midnum);
  
						//TableCell cell22=new TableCell();
						TableCell cell_TrialComments=new TableCell ();
						//cell_TrialComments.BorderStyle =BorderStyle.Double;														
						cell_TrialComments.Font .Size =7;
						cell_TrialComments .Width=140;
						cell_TrialComments .Wrap =true;
						cell_TrialComments .Height =10;
						cell_TrialComments.Text=Ds_FillTable.Tables[1] .Rows[IdxRow]["Shortdesc1"].ToString()+
							"<Font Color=Red>"+Ds_FillTable.Tables[1] .Rows[IdxRow]["trialcomments"].ToString()+"</font>";
						rowInsert.Cells.Add(cell_TrialComments);
						Table_Print.Rows.Add(rowInsert);
					}
				}
				else
				{
					if (Ds_FillTable.Tables[1] .Rows .Count + Ds_FillTable.Tables[0] .Rows .Count < 1 )
					{
						TableRow row=new TableRow();
						TableCell cell=new TableCell();
						cell.ColumnSpan =11;
						cell.Text="No Records Found";
						row.Cells.Add(cell);
						Table_Print.Rows.Add(row);
					}
				}
			}
			catch(Exception ex)
			{
				TableRow row=new TableRow();
				TableCell cell=new TableCell();
                if(ex.Message.ToLower()=="cannot find table 0.")
				    cell.Text="<font color='red'>There is no record for this date.</font>";
                else
                    cell.Text = "<font color='red'>" + ex.Message + "</font>";
				row.Cells.Add(cell);
				Table_Print.Rows.Add(row);
			}
		}	
		#endregion	
		//Leave 1 Row Method
		#region
		public void LeaveOneRow()
		{
			TableRow tr=new TableRow ();
			tr.BorderStyle =BorderStyle.None; 
			TableCell tc=new TableCell ();
			tc.BorderStyle =BorderStyle.None ;
			tc.Text="<BR>";
			tc.ColumnSpan =12;
			tr.Cells .Add (tc);
			Table_Print.Rows .Add (tr);
		}

		#endregion	
	}
}
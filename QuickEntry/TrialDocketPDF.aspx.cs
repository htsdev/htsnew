using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;

namespace lntechNew.QuickEntry
{
    public partial class TrialDocketPDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           /*
            string CourtLocation = Request.QueryString["CourtLocation"].ToString();
            DateTime courtdate = (DateTime) Convert.ChangeType(Request.QueryString["courtdate"].ToString(),typeof(DateTime));
            string page = Request.QueryString["page"].ToString();
            string courtnumber = Request.QueryString["courtnumber"].ToString();
            string datetype = Request.QueryString["datetype"].ToString();
           */

           
            string CourtLocation = "None";
            DateTime courtdate = (DateTime) Convert.ChangeType("10/20/2006",typeof(DateTime));
            string page = "1";
            string courtnumber = "0";
            string datetype = "0";
            

            LoadReport(CourtLocation, courtdate, page, courtnumber, datetype);
        }
        private void LoadReport(string CourtLocation, DateTime courtdate, string page, string courtnumber, string datetype)
        {
            clsCrsytalComponent Cr = new clsCrsytalComponent();

            string[] key = { "@courtloc", "@courtdate", "@page", "@courtnumber", "@datetype" };
            object[] value1 = { CourtLocation, courtdate, page, courtnumber, datetype };
            string filename = Server.MapPath("") + "\\MainReportForTrialDocket.rpt";

            Cr.CreateReportTrialDocket(filename, "usp_hts_get_trial_docket_report_new", key, value1, this.Session, this.Response);
        }
    }
}

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ErrorPageNotFound" CodeBehind="ErrorPageNotFound.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>LoginAccesserror</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../Styles.css" type="text/css" rel="stylesheet">
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <table align="center" width="780" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td height="87">
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/lnlogo.jpg"></asp:Image>
            </td>
        </tr>
    </table>
    <table align="center" border="0" width="780" cellspacing="0" cellpadding="0" bordercolor="blue">
        <tr>
            <td height="36" valign="middle" background="images/navi_repeat.jpg">
                <img src="images/accessdenied.jpg" height="36">
            </td>
        </tr>
    </table>
    <table class="clsLeftPaddingTable" width="780" border="0" align="center" cellpadding="0"
        cellspacing="0">
        <tr>
            <br>
            <td>
                <font color="red">Please contact system administrator.Requested page is either inactive
                    or not exist. Click <a href="javascript:history.back()">Here</a> to go Back.</font>
            </td>
        </tr>
    </table>
    <table width="780" border="0" align="center" cellpadding="0" cellspacing="0" id="Table5">
        <tr background="images/separator_repeat.gif">
            <td colspan="5" width="780" height="11" background="images/separator_repeat.gif">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.WebControls;
using FrameWorkEnation.Components;

namespace lntechNew.ClientInfo
{
    public partial class PendingSignature : System.Web.UI.Page
    {
        clsENationWebComponents cls_DataBase = new clsENationWebComponents();
        private ArrayList items = new ArrayList();
        private DataTable dtClient = new DataTable();
        private DataTable dtSignedLetters = new DataTable();
        int CourtID = 0, CategoryID = 0, ContinuanceAmount = 0;
        bool Outside_Flag = false;
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["vSearch"] = Request.QueryString["search"];
                ViewState["vTicketID"] = Request.QueryString["caseNumber"];
                GetClientInfo();
                UpdateList();
                UpdateUnsignedLettersGrid();
                UpdateQueryString();
                GetSignedDocuments();

            }
            
        }
        private void GetSignedDocuments()
        {
            string[] Keys = { "@TicketID" };
            object[] Values = { Request.QueryString["caseNumber"] };
            dtSignedLetters = cls_DataBase.Get_DT_BySPArr("USP_HTS_GET_BATCH_PaymentInfo", Keys, Values);
            gv_SignedDocuments.DataSource = dtSignedLetters;
            gv_SignedDocuments.DataBind();
        }

        private void UpdateQueryString()
        {
            ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
            TextBox txt1 = (TextBox)am.FindControl("txtid");
            txt1.Text = ViewState["vTicketID"].ToString();
            TextBox txt2 = (TextBox)am.FindControl("txtsrch");
            txt2.Text = ViewState["vSearch"].ToString();
        }

        private void GetClientInfo()
        {
            try
            {
                string[] parameterKeys = { "@TicketID" };
                object[] values = { Request.QueryString["caseNumber"] };
                dtClient = cls_DataBase.Get_DT_BySPArr("usp_hts_Get_Client_Info", parameterKeys, values);
                CourtID = Convert.ToInt32(dtClient.Rows[0]["CourtID"]);
                ContinuanceAmount = Convert.ToInt32(dtClient.Rows[0]["ContinuanceAmount"]);
                CategoryID = Convert.ToInt32(dtClient.Rows[0]["CategoryID"]);
            }
            catch (Exception ex)
            {
                if(ex.GetType() == typeof(System.IndexOutOfRangeException))
                    lblMessage.Text="No data found for this client";

            }
        }

        private void UpdateList()
        {
            items.Add("Receipt");
            if (CourtID == 3001 || CourtID == 3002 || CourtID == 3003)
            {
                items.Add("Bond Letter (Inside)");
                Outside_Flag = false;
            }
            else
            {
                items.Add("Bond Letter (Outside)");
                Outside_Flag = true;
            }

            if (ContinuanceAmount > 0)
            {
                items.Add("Letter of Continuation");
            }

            if ((CategoryID == 1 || CategoryID == 2) && Outside_Flag)
            {
                items.Add("Letter of Rep");
            }

            if (CategoryID == 3 || CategoryID == 4 || CategoryID == 5)
            {
                items.Add("Trial Letter");
            }

        }
        

        private void UpdateUnsignedLettersGrid()
        {
            gv_UnsignedDocuments.DataSource = items;
            gv_UnsignedDocuments.DataBind();
        }
    }
}

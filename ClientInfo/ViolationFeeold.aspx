﻿<%@ Page Language="c#" AutoEventWireup="False" Inherits="HTP.ClientInfo.ViolationFeeOld"
    AspCompat="True" SmartNavigation="False" CodeBehind="ViolationFeeold.aspx.cs"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/UpdateViolationSRV.ascx" TagName="UpdateViolationSRV" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/ReadNotes.ascx" TagName="ReadNotes" TagPrefix="uc2" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%--<%@ Register Src="../WebControls/UpdateViolation.ascx" TagName="UpdateViolation"
    TagPrefix="uc3" %>--%>
<%@ Register TagName="ContactInfo" TagPrefix="Contact" Src="~/WebControls/ContactInfo.ascx" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagName="LookUp" TagPrefix="ContactID" Src="~/WebControls/ContactIDLookUp.ascx" %>
<%@ Register TagName="Sms" TagPrefix="sms" Src="~/WebControls/SMSControl.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Matter Page</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bootstrap-dialog.css" rel="stylesheet" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>


    <script src="../Scripts/boxover.js" type="text/javascript"></script>

    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        // Abbas Qamar 10114
        function ShowDL(tdAtt)
        {
            document.getElementById(tdAtt).style.display='block';
            //HideDL('tdDrivingLicense');
        }
		
        function HideDL(tdAtt)
        {
            document.getElementById(tdAtt).style.display='none';
		 
        }
        function ShowSSN(tdAtt)
        {
            // Hafiz 10288 07/12/2012 comment the line.
            // document.getElementById(tdAtt).style.display='block';
            document.getElementById('tdDDLSSN').style.display='none';
            document.getElementById('tdOther').style.display='none';

        }

        function HideSSN(tdAtt)
        {
            document.getElementById(tdAtt).style.display='none';
            document.getElementById('tdDDLSSN').style.display='block';
		   
		    
            var ddlSSN=document.getElementById("DDLSSNQuestion").value;
            if(document.getElementById('RadioButton4').checked==true && ddlSSN==6) {
		      
                document.getElementById('tdOther').style.display='block';
		       
		       
		      
		       
            }
        }
        function ShowHideOtherTextBox()
        {
		   
            var value= document.getElementById('DDLSSNQuestion').value ;
            if(value==6)
            {
		    
                document.getElementById('tdOther').style.display='block';
		        
            }
            else{
                document.getElementById('tdOther').style.display='none';
            }
        }
        //Sabir Khan 11509 11/13/2013 Hide/Show other text for how did you find by value.
        function ShowHideOtherFindByTextBox()
        {
		   
            var value= document.getElementById('ddlFindBy').value ;
            if(value==0)
            {
		    
                document.getElementById('tdOtherFindBy').style.display='block';
		        
            }
            else{
                document.getElementById('tdOtherFindBy').style.display='none';
            }
        }


        function Validation() {
		    
            var dltxt=document.getElementById("txt_dlstr").value;
            var ddlDL=document.getElementById("ddl_dlState").value;
            if(dltxt != "")
            {
                $("#txtErrorMessage").text("If Driving License No not available then select the No Radio button");
                $("#errorAlert").modal();
              //  alert("If Driving License No not available then select the No Radio button");
            }



        }

        // End 10114
        //Waqas 6599 09/30/2009 Suggestion Control
       
        function KeyDownHandler()
        {   
            //Process only the Enter key
            if (event.keyCode == 13)
            {
                //Cancel the default submit
                event.returnValue=false;
                event.cancel = true;
            }
        }       
        
        //Farrukh 9925 11/30/2011 Returns alert message for validation and code redundancy removed		
        function AlertForValidation(obj,msg)
        {
            $("#txtErrorMessage").text(msg);
            $("#errorAlert").modal();
         //   alert(msg);
            obj.focus(); 
            return false;	
        }	
        
        //waqas 6666 10/22/2009 disabled javascript suggestion  

       
        function WaitForViolationAmount()
        {
            document.getElementById("tblViolationAmount").style.display = "block";
            document.getElementById("dgViolationInfo").style.display = "none";          
            //return true;
        }
       
        function WaitForViolationAmountUpdate()
        {
            document.getElementById("tbl_plzwait1").style.display = "block";
            document.getElementById("dgViolationInfo").style.display = "none";          
            return true;       
        }
       
        // tahir 4841 09/23/2008 show/hide sort progress indicator...
        function ShowSortProgress()
        {
            document.getElementById("tblSortProgress").style.display = "block";
            document.getElementById("dgViolationInfo").style.display = "none";  
            //4841 12/20/08 Sending request on server
            return true;
        }
       
        function HideSortProgress()
        {
            document.getElementById("tblSortProgress").style.display = "none";
            document.getElementById("dgViolationInfo").style.display = "block";  
            return false;
        }
        // end 4841
       
        function ValidateViolationAmountPanel()
        {        
            var isRecordSelect = false;
            var allCheckBox = document.getElementById("gvViolationAmount").getElementsByTagName("input");
         
            for ( i = 0 ; i < allCheckBox.length ; i++)
            {
                if ( allCheckBox[i].checked ) 
                {
                    isRecordSelect = true;
                    break;
                }
            }
           
            if (!isRecordSelect)
            {
                $("#txtErrorMessage").text("Please select cause number.");
                $("#errorAlert").modal();
               // alert("Please select cause number.");
                return false;
            }
           
            closeViolationAmountPopup();
            WaitForViolationAmountUpdate();
            return true;        
        }
       
        function CheckAll(checkall)
        {
            var checked = checkall.checked;
            var allCheckBox = document.getElementById("gvViolationAmount").getElementsByTagName("input");
           
            for ( i = 0 ; i < allCheckBox.length ; i++)
                allCheckBox[i].checked = checked;
        }
      
        function closeViolationAmountPopup()
        {
            var modalPopupBehavior = $find('MPEViolationAmount');
            modalPopupBehavior.hide();
            return false;
        }
       
        function ValidateCommentControls()
        { 
            
            //	      //Comments Textboxes
            //		  var dateLenght = 0;
            //		  var newLenght = 0;
            //		  
            //          newLenght = document.getElementById("WCC_GeneralComments_txt_comments").value.length
            //		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

            //		  if (document.getElementById("WCC_GeneralComments_txt_comments").value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerText.length > (5000 - dateLenght ))
            //		  {
            //		    alert("Sorry You cannot type in more than 5000 characters in General comments box")
            //			return false;		  
            //		  }
        }
        var zipcode = null;
		
        function zipcodePoupup(ControlName)
        {	
            CursorIcon();
            zipcode = null;
            zipcode = document.getElementById(ControlName).value;
            ShowMsg()
            //document.getElementById("txtb_StateTraceMsg").value = err;
            //document.getElementById("txtb_StateTraceMsg").style.visibility = 'visible';
        }
        //change cursor icon
        function CursorIcon()
        {
            document.body.style.cursor = 'pointer';
        }
        function CursorIcon2()
        {
            document.body.style.cursor = 'default';
        }
		
        function ShowMsg()
        {
            document.getElementById("txt_zipcodeMsg").value=zipcode;
        }
        
        
        function openScannedDocket(crtdate)
        {
            //alert(crtdate);
            window.open("../returneddockets/docketsreports.aspx?dt="+crtdate);
            return false;
        }
		
        //Kazim 4071 05/21/2008 Remove court and client addresses.
        function ShowCourtDetailPopUp(courtid,ticketid)
        {
            window.open('CourtDetail.aspx?ticketid='+ticketid+ '&courtid='+courtid,'','scrollbars=yes,Resizable=no,status=yes,width=610,height=550');
            //Waqas 6895 11/02/2009 
            //void('');
            //return(false); 
            return false;
        }

        function ShowReadNotes()
        {
	        
            var ticketid = <%=ViewState["vTicketID"]%>;
            var empid = <%=ViewState["empid"]%>;
            var chk = document.getElementById("chkbtn_read");
            if(chk.checked == true)
            {
                var path="../Reports/ReadComments.aspx?ticketid="+ticketid + "&empid=" + empid;
                window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no,width=390,height=215,scrollbars=no");
                return false;
            }
            return true;         
        }
		
        function DeleteReadNotes()
        {
            var check;
		
            check = confirm("Are You Sure You Want To Remove Readnotes Flag? (OK = Yes   Cancel = No)"); 
            if (check == true) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
						
        function show_popup(ticketid)
        {
            window.open('MailerInfo.aspx?TicketID='+ticketid,'','Resizable=No,status=yes,width=370,height=270');
            void('');
            return(false);
        }
		
        function showPhonePanel() 
        {
            DisplayToggle();  
		   
            document.getElementById("pnl_telnos").style.display = 'block';
            document.getElementById("pnl_telnos").style.position = "absolute";
            document.getElementById("pnl_telnos").style.top = 400;			
	      
            document.getElementById("Disable").style.height = document.body.offsetHeight * 2 ;
            document.getElementById("Disable").style.width = document.body.offsetWidth;
            document.getElementById("Disable").style.display = 'block'
		
            return false;
        }
		
        function closetelpopup()
        {
            document.getElementById("pnl_telnos").style.display = 'none';
            document.getElementById("Disable").style.display = 'none';
            DisplayToggle();
            return false;
        }
		
        //Waqas 5864 06/23/2009 For Case Email Summary
        function AddTicketViolationIDs()
        {
            var all = document.getElementsByTagName("input");
            
            document.getElementById("hf_AppTicketViolationIDs").value = "";
            for(i=0;i<all.length;i++)
            { 
                if(all[i].type=="checkbox" && all[i].id == "rbtnViolationID")
                {
                    if(all[i].checked==true)
                    {
                        document.getElementById("hf_AppTicketViolationIDs").value += all[i].value + ",";                                
                    }
                }
            }
                
            return true; 
        }
        
        function CheckEmailSelection()
        {
            if(document.getElementById('hf_EmailAttorneyName').value == '' && document.getElementById('tr_DrpAttonerneyName').style.display == "block")
            {
                $("#txtErrorMessage").text("Please select Attorney Name");
                $("#errorAlert").modal();
               // alert("Please select Attorney Name");
                return false;
            }
            
            //Nasir 6619 11/25/2009 remove message on checking of hf_lblEmailAttorney

         
            var AttorEmail=document.getElementById("txtAttorneyEmail").value;
            
            if(AttorEmail!="" )
            {			
                if( isEmail(AttorEmail)== false)
                {
                    return AlertForValidation(document.getElementById("txtAttorneyEmail"), "Please enter Email Address in Correct format.");	   
                }
            }	
            else
            {				
                return AlertForValidation(document.getElementById("txtAttorneyEmail"), "Please enter Attorney's email address.");  				
            }	
            
            
            
            var a = 0;
            var all = document.getElementsByTagName("input");
            for(i=0;i<all.length;i++)
            { 
                if(all[i].type=="checkbox" && all[i].id == "rbtnViolationID")
                {
                    if(all[i].checked==true)
                    {
                        a = 1;
                    }                        
                }
            }
            if(a==0)
            {
                $("#txtErrorMessage").text("Please select any violation.");
                $("#errorAlert").modal();
               // alert("Please select any violation.");
                return false;
            }
            
            document.getElementById('imgShowEmailProgress').style.display = "block";            
            
            return true;
        }
		
        function SetEmailAttorney()
        {
            //Nasir 6619 12/01/2009 remove hidden field hf_lblEmailAttorney
            document.getElementById('hf_EmailAttorneyName').value = document.getElementById('drpAttorneysOnEmails')[document.getElementById('drpAttorneysOnEmails').selectedIndex].text;		    
            //Nasir 6619 11/25/2009 set attorney email on text box		    
            document.getElementById("txtAttorneyEmail").disabled = false;
            document.getElementById("txtAttorneyEmail").value = document.getElementById('drpAttorneysOnEmails')[document.getElementById('drpAttorneysOnEmails').selectedIndex].value;
            if(document.getElementById('drpAttorneysOnEmails').selectedIndex == 0)
            {
                document.getElementById("txtAttorneyEmail").disabled = true;		    
            }
            return false;
        }
		
        function ShowCaseEmailPanel()
        {
            document.getElementById("pnlEmailCase").style.display = 'block';
            document.getElementById("pnlEmailCase").style.position = "absolute";
            document.getElementById("pnlEmailCase").style.top = 330;			
            document.getElementById("pnlEmailCase").style.left = 400;			
	      
            document.getElementById("DivForEmail").style.height = document.body.offsetHeight ;
            document.getElementById("DivForEmail").style.width = document.body.offsetWidth;
            document.getElementById("DivForEmail").style.display = 'block'
		
            return false;
		
        }
		
        function closeEmailCasepopup()
        {
            document.getElementById("pnlEmailCase").style.display = 'none';
            document.getElementById("DivForEmail").style.display = 'none';
		  
            return false;
        }
		
        function ShowCallbackDays(tdCallbackdays)
        {
            document.getElementById(tdCallbackdays).style.display='block';
        }
		
        function HideCallbackDays(tdCallbackdays)
        {
            document.getElementById(tdCallbackdays).style.display='none';
        }
		
        //waqas 5864 06/12/2009 
		
        function HidePriorAttorney(tdAtt)
        {
            if(document.getElementById('drpAtt').value == "0")
            {
                document.getElementById(tdAtt).style.display='none';		        
            }
            else
            {
                document.getElementById(tdAtt).style.display='block';
            }
        }
		
        //Waqas 6342 08/12/2009 ALR is required or not.
        //Yasir 7150 01/01/2010 ALR BTO, BTS modified.
        function ShowHideALRHearingRequired()
        {
            //Asad Ali 8153  09/23/2010 for ALR Courts Only
            var isALRCourt= document.getElementById('<%=hf_IsALROnly.ClientID%>').value;
		    if(document.getElementById('rBtn_ALRHearingRequiredYes').checked == true )
		    {
		        //hafiz 10288 07/24/2012 commented the below section
		        //		        if(document.getElementById('lbl_ActiveFlag').innerHTML=='1')
		        //		        {
		        //		        
		        //		        //Asad Ali 8153 09/03/2010  Ask Question for ALR only
		        //		           if(isALRCourt=='1')
		        //		            {
		        //    		            document.getElementById('tr_postHireQuestions2').style.display='block';
		        //	    	            document.getElementById('tr_ALRRequiredWhy').style.display='none';		        
		        //		            }
		        //		            document.getElementById('txtALRHearingRequiredAnswer').value = '';
		        //		        }
		        //Asad Ali 8153 09/03/2010  show when yes
		        //end 10288
		        document.getElementById('<%=tr_IntoxilyzerTaken.ClientID%>').style.display='table-row';
		        //hafiz 10288 07/24/2012 commented the below section
		        //ShowHideIntoxilyzer();			        
		    }
		    
            if(document.getElementById('rBtn_ALRHearingRequiredNo').checked == true )
            {
                //hafiz 10288 07/24/2012 commented the below section
                //		        if(document.getElementById('lbl_ActiveFlag').innerHTML=='1')
                //		        {
                //		           //Asad Ali 8153 09/03/2010  Ask Question for ALR only
                //		           if(isALRCourt=='1')
                //		            {
                //		                document.getElementById('tr_ALRRequiredWhy').style.display='block';		        
                //		                document.getElementById('txtALRHearingRequiredAnswer').focus();
                //		            }
                //		        document.getElementById('tr_postHireQuestions2').style.display='none';
                //		        document.getElementById('txtALROfficerName').value = '';
                //		        document.getElementById('txtALROfficerBadgeNumber').value = '';
                //		        document.getElementById('txtALRPrecinct').value = '';
                //		        document.getElementById('txtALRAddress').value = '';
                //		        document.getElementById('txtALRCity').value = '';
                //		        document.getElementById('ddl_ALRState').selectedIndex = 0;
                //		        document.getElementById('txtALRZip').value = ''	;	        
                //		        document.getElementById('txt_Attacc11').value = '';
                //		        document.getElementById('txt_Attcc12').value = '';
                //		        document.getElementById('txt_Attcc13').value = '';
                //		        document.getElementById('txt_Attcc14').value = '';
                //		        document.getElementById('txtALRArrestingAgency').value = '';
                //		        document.getElementById('txtALROfficerMileage').value = '';	
                //		        
                //		        document.getElementById('txtALROBSOfficerName').value = '';
                //		        document.getElementById('txtALROBSOfficerBadgeNumber').value = '';
                //		        document.getElementById('txtALROBSPrecinct').value = '';
                //		        document.getElementById('txtALROBSAddress').value = '';
                //		        document.getElementById('txtALROBSCity').value = '';
                //		        document.getElementById('ddl_ALROBSState').selectedIndex = 0;
                //		        document.getElementById('txtALROBSZip').value = ''	;	        
                //		        document.getElementById('txt_OBSAttacc11').value = '';
                //		        document.getElementById('txt_OBSAttcc12').value = '';
                //		        document.getElementById('txt_OBSAttcc13').value = '';
                //		        document.getElementById('txt_OBSAttcc14').value = '';
                //		        document.getElementById('txtALROBSArrestingAgency').value = '';
                //		        document.getElementById('txtALROBSOfficerMileage').value = '';
                //		        
                //		        
                //		        document.getElementById('rBtnArrOffObsOffYes').checked = false;		        		        
                //                document.getElementById('rBtnArrOffObsOffNo').checked = false;
                //                
                //                ShowHideObserveOfficer();
                //                }
                document.getElementById('drp_IntoxilyzerTaken').selectedIndex = 0;
                //Asad Ali 8153 09/03/2010  show when yes
                document.getElementById('<%=tr_IntoxilyzerTaken.ClientID%>').style.display='none';
                //hafiz 10288 07/24/2012 commented the below section
                //ShowHideIntoxilyzer();
            }
        }
		
		
        function ShowHideObserveOfficer()
        {
            if(document.getElementById('rBtnArrOffObsOffYes').checked == true)
            {
                document.getElementById('tr_ObservingOfficer').style.display='none';
		        
                document.getElementById('txtALROBSOfficerName').value = '';
                document.getElementById('txtALROBSOfficerBadgeNumber').value = '';
                document.getElementById('txtALROBSPrecinct').value = '';
                document.getElementById('txtALROBSAddress').value = '';
                document.getElementById('txtALROBSCity').value = '';
                document.getElementById('ddl_ALROBSState').selectedIndex = 0;
                document.getElementById('txtALROBSZip').value = ''	;	        
                document.getElementById('txt_OBSAttacc11').value = '';
                document.getElementById('txt_OBSAttcc12').value = '';
                document.getElementById('txt_OBSAttcc13').value = '';
                document.getElementById('txt_OBSAttcc14').value = '';
                document.getElementById('txtALROBSArrestingAgency').value = '';
                document.getElementById('txtALROBSOfficerMileage').value = '';
            }
		    
            if(document.getElementById('rBtnArrOffObsOffNo').checked == true)
            {
                document.getElementById('tr_ObservingOfficer').style.display='block';
            }
		    
            else
            {
                document.getElementById('tr_ObservingOfficer').style.display='none';
		        
                document.getElementById('txtALROBSOfficerName').value = '';
                document.getElementById('txtALROBSOfficerBadgeNumber').value = '';
                document.getElementById('txtALROBSPrecinct').value = '';
                document.getElementById('txtALROBSAddress').value = '';
                document.getElementById('txtALROBSCity').value = '';
                document.getElementById('ddl_ALROBSState').selectedIndex = 0;
                document.getElementById('txtALROBSZip').value = ''	;	        
                document.getElementById('txt_OBSAttacc11').value = '';
                document.getElementById('txt_OBSAttcc12').value = '';
                document.getElementById('txt_OBSAttcc13').value = '';
                document.getElementById('txt_OBSAttcc14').value = '';
                document.getElementById('txtALROBSArrestingAgency').value = '';
                document.getElementById('txtALROBSOfficerMileage').value = '';
            }
		    
        }
		
        function ShowBTOName()
        {
            if(document.getElementById('rblBTOName').checked)
            {
                document.getElementById('txtBTOName').disabled = false;
				
            }
            else
            {
                document.getElementById('txtBTOName').disabled = true;
		
            }
		
        }
		
        function ShowBTOObserving()
        {
            if(document.getElementById('rblBTOName').checked)
            {
                document.getElementById('txtBTOName').disabled = false;
				
            }
            else
            {
                document.getElementById('txtBTOName').disabled = true;
                document.getElementById('txtBTOName').value= '';
		
            }
		
        }
		
        function ShowBTOArresting()
        {
            if(document.getElementById('rblBTOName').checked)
            {
                document.getElementById('txtBTOName').disabled = false;
				
            }
            else
            {
                document.getElementById('txtBTOName').disabled = true;
                document.getElementById('txtBTOName').value= '';
            }
		
        }
		
				
		
        function ShowHideIntoxilyzer()
        {
            if(document.getElementById('drp_IntoxilyzerTaken').value == "1")
            {
                document.getElementById('tr_IntoxilyzerResults').style.display='block';
                ShowHideBreathTest();
                //ShowHideBreathTestSupervisor();		        
            }
            else
            {
                document.getElementById('tr_IntoxilyzerResults').style.display='none';		        
		        
                document.getElementById('rbtn_IntoxResultPass').checked= false;
                document.getElementById('rbtn_IntoxResultFail').checked= false;
		        
                //		        document.getElementById('txtBTOFirstName').value = '';
                //		        document.getElementById('txtBTOLastName').value = '';
		        
                document.getElementById('tr_BreathTestOperator').style.display='none';
                //              document.getElementById('tr_BreathTestSame').style.display='none';
                document.getElementById('tr_BreathTestSupervisor').style.display='none';
		        
                document.getElementById('rbBTOArresting').checked = false;
                document.getElementById('rbBTOObserving').checked = false;
                document.getElementById('rblBTOName').checked = false;
                document.getElementById('txtBTOName').value = '';
                		        
                //		        document.getElementById('rBtn_BtoBtsSameYes').checked = false;
                //		        document.getElementById('rBtn_BtoBtsSameNo').checked = false;
		        
                document.getElementById('txtBTSFirstName').value = '';
                document.getElementById('txtBTSLastName').value = '';
		        
            }
		    		    
        }
		
        function ShowHideBreathTest()
        {
            if(document.getElementById('drp_IntoxilyzerTaken').value=='1')
            {
                document.getElementById('tr_BreathTestOperator').style.display='block';
                //document.getElementById('tr_BreathTestSame').style.display='block';
                document.getElementById('tr_BreathTestSupervisor').style.display='block';
		        
                //   ShowHideBreathTestSupervisor();
            }
            else 
            {
                document.getElementById('tr_BreathTestOperator').style.display='none';
                //document.getElementById('tr_BreathTestSame').style.display='none';
                document.getElementById('tr_BreathTestSupervisor').style.display='none';
                document.getElementById('txtBTSFirstName').value = '';
                document.getElementById('txtBTSLastName').value = '';
		         
                document.getElementById('rbBTOArresting').checked = false;
                document.getElementById('rbBTOObserving').checked = false;
                document.getElementById('rblBTOName').checked = false;
                document.getElementById('txtBTOName').value = '';
                //		         document.getElementById('txtBTOFirstName').value = '';
                //		         document.getElementById('txtBTOLastName').value = '';
		         
                //		         document.getElementById('rBtn_BtoBtsSameYes').checked = false;
                //		         document.getElementById('rBtn_BtoBtsSameNo').checked = false;
		         
                //         ShowHideBreathTestSupervisor();
		         
		         
            }
        }
		
        //		function ShowHideBreathTestSupervisor()
        //		{
        //		    if(document.getElementById('rBtn_BtoBtsSameYes').checked == true)
        //		    {
        //		        document.getElementById('tr_BreathTestSupervisor').style.display='none';
        //		        
        //		        document.getElementById('txtBTSFirstName').value = '';
        //		        document.getElementById('txtBTSLastName').value = '';
        //		    }		    
        //		    else if(document.getElementById('rBtn_BtoBtsSameNo').checked == true)
        //		    {
        //		        document.getElementById('tr_BreathTestSupervisor').style.display='block';
        //		    }
        //		    else
        //		    {
        //		        document.getElementById('tr_BreathTestSupervisor').style.display='none';
        //		        document.getElementById('txtBTSFirstName').value = '';
        //		        document.getElementById('txtBTSLastName').value = '';
        //		    }
        //		    
        //		}		
        //7150 end
			
        function ShowAttorneys(tdAtt)
        {
            document.getElementById(tdAtt).style.display='block';
            HidePriorAttorney('td_priorattorney');
        }
		
        function HideAttorneys(tdAtt)
        {
            document.getElementById(tdAtt).style.display='none';
            document.getElementById('td_priorattorney').style.display='none';
		   
            document.getElementById('txtPriorAttorney').value = '';
            document.getElementById('drpAtt').value = "0";
        }
		
        function validatePhonePanel()
        {
            //*******************************************************//
		
            var c11=document.frmviolationfee.ptel11.value;
            var c12=document.frmviolationfee.ptel12.value;
            var c13=document.frmviolationfee.ptel13.value;		
            var c1=c11.length+c12.length+c13.length;
			
            var c21=document.frmviolationfee.ptel21.value;
            var c22=document.frmviolationfee.ptel22.value;
            var c23=document.frmviolationfee.ptel23.value;		
            var c2=c21.length+c22.length+c23.length;
			
            var c31=document.frmviolationfee.ptel31.value;
            var c32=document.frmviolationfee.ptel32.value;
            var c33=document.frmviolationfee.ptel33.value;		
            var c3=c31.length+c32.length+c33.length;				
			
            //===============================================
			
            //Checking if numeric or not zeeshan
            //a;
			
            if (  isNaN(document.frmviolationfee.ptel11.value)==true || isNaN(document.frmviolationfee.ptel12.value)==true ||isNaN(document.frmviolationfee.ptel13.value)==true)// || isNaN(document.frmviolationfee.ptel14.value) == true )	
            {
                return AlertForValidation(document.frmviolationfee.ptel11, "Invalid Phone Number. Phone Number should be like 713-389-9026");


            }
            if (  isNaN(document.frmviolationfee.ptel21.value)==true || isNaN(document.frmviolationfee.ptel22.value)==true ||isNaN(document.frmviolationfee.ptel23.value)==true)// || isNaN(document.frmviolationfee.ptel24.value) == true )	
            {
                return AlertForValidation(document.frmviolationfee.ptel21, "Invalid Phone Number. Phone Number should be like 713-389-9026");


            }
            if (  isNaN(document.frmviolationfee.ptel31.value)==true || isNaN(document.frmviolationfee.ptel32.value)==true ||isNaN(document.frmviolationfee.ptel33.value)==true)// || isNaN(document.frmviolationfee.ptel34.value) == true )	
            {
                return AlertForValidation(document.frmviolationfee.ptel31, "Invalid Phone Number. Phone Number should be like 713-389-9026");	


            }			
            //
			
            if((c1==0) &&(c2==0)&&(c3==0))




            {
                $("#txtErrorMessage").text("Please Enter at least 1 contact Number for this client.");
                $("#errorAlert").modal();
               // alert("Please Enter at least 1 contact Number for this client.")
                document.frmviolationfee.ptel11.focus;				
                return false;			
            }

            else
            {
				
                if((c1==10) ||(c2==10)||(c3==10))
                {

                    if(c1==10)
                    {										
                        if(document.frmviolationfee.ddl_ptype1.selectedIndex=='0')
                        {
                            alert ("Please specify Contact Type.");
                            document.frmviolationfee.ddl_ptype1.focus(); 
                            return(false);			   
                        }			
					
                    }
                    if(c2==10)
                    {				
                        if(document.frmviolationfee.ddl_ptype2.selectedIndex=='0')
                        {
                            alert ("Please specify Contact Type.");
                            document.frmviolationfee.ddl_ptype2.focus(); 
                            return(false);			   
                        }				
                    }
                    if(c3==10)
                    {				
                        if(document.frmviolationfee.ddl_ptype3.selectedIndex=='0')
                        {
                            $("#txtErrorMessage").text("Please specify Contact Type.");
                            $("#errorAlert").modal();
                            //alert ("Please specify Contact Type.");
                            document.frmviolationfee.ddl_ptype3.focus(); 
                            return(false);		
                        }				
                    }
                }
                else
                {	 
                    $("#txtErrorMessage").text("Invalid Phone Number. Phone Number should be like 713-389-9026");
                    $("#errorAlert").modal();
                   // alert ("Invalid Phone Number. Phone Number should be like 713-389-9026");			  
                    return false;	
                }
            }
            if( (c1>0&&c1<10) || (c2>0&&c2<10) || (c3>0&&c3<10) )
            {
                $("#txtErrorMessage").text("Invalid Phone Number. Phone Number should be like 713-389-9026");
                $("#errorAlert").modal();
               // alert("Invalid Phone Number. Phone Number should be like 713-389-9026");				
                return false;
            }
		   
            if ( document.frmviolationfee.rb1.checked != true && document.frmviolationfee.rb2.checked != true && document.frmviolationfee.rb1.checked != true)
            {
                return AlertForValidation(document.frmviolationfee.rb1, "Please select a primary number .");
            }
	     
            if ( document.frmviolationfee.rb1.checked == true && c1==0 )
            {
                return AlertForValidation(document.frmviolationfee.ptel11, "Please specify primary number");	        
            }
	     
            if ( document.frmviolationfee.rb2.checked == true && c2==0 )
            {
                return AlertForValidation(document.frmviolationfee.ptel21, "Please specify primary number");
            }
	     
            if ( document.frmviolationfee.rb3.checked == true && c3==0 )
            {
                return AlertForValidation(document.frmviolationfee.ptel31, "Please specify primary number");
            }		  
		
            //===============================================
			   		
		         
            document.getElementById("tbl_telwait").style.display = 'block';
            document.getElementById("lnkbtn_addphoneno").style.display = 'none';
            closetelpopup();
		            		
        }
		
		
		
        function openSPNSearchPopup()
        {
            var h="<%=hlnk_MidNo.Text %>";
            if(h=="")
                h="0";
            window.open("frmSPNSearch.aspx?SPN="+h);
        }
			
        //Added By Zeeshan Ahmed
        //Search Case Information From the court website
        function openCaseNoSearchPopup(cdi,casenumber)
        {
            window.open("frmSPNSearch.aspx?SPN="+casenumber+"&cdi="+cdi+"&casesearch=1");
        }      			
			
        function plzwait1()
        {
		
            document.getElementById("tbl_plzwait").style.display = 'block';
            document.getElementById("FeeCalc").style.display = 'none';
		
        }

        function openwindow1() 
        {			
            if (lblisNew.innerText == '1')
            {
                $("#txtErrorMessage").text("Please first update the main information.");
                $("#errorAlert").modal();
               // alert('Please first update the main information.');
                return false;
            }
				
            var w =window.open ('CalculationSummary.aspx?casenumber='+<%=ViewState["vTicketID"]%>, null, 'status=yes,left=20,top=20,height=600, width=525,scrollbars=yes'); 
		    return false;
			
        }	
        function openwindow(ticketviolationid,Requesttype,updatecontrol) 
        {
		
		
            if (lblisNew.innerText == '1')
            {
                $("#txtErrorMessage").text("Please first update the main information.");
                $("#errorAlert").modal();
              //  alert('Please first update the main information.');
                return false;
            }
				
            if (Requesttype != "999" ) 
            {
                addnewviolation(updatecontrol);
                return false;
            }
			

        }	
				
        function DateDiff(date1, date2)
        {
            var objDate1=new Date(date1);
            var objDate2=new Date(date2);
            return (objDate1.getTime()-objDate2.getTime())/1000;
        }
		
        //Waqas 6895 11/02/2009 show progress on update
        function ShowProgressOnUpdate()
        {
            document.getElementById("tblProgressUpdatePage").style.display = 'block';
            document.getElementById("btn_Update").style.display = 'none';
            document.getElementById("btnNext").style.display = 'none';
            return false;
        }		
		
        //Waqas Javed 5771 04/16/2009
        function CheckPreReqForCID()
        {
		    
            //First name check
            var inValidChars = "0123456789";
            var Char;
            var firstName=frmviolationfee.txt_FirstName.value;
            var birthmonth=document.getElementById("txt_mm").value;
            var birthdate=document.getElementById("txt_dd").value;
            var birthyear=document.getElementById("txt_yy").value;
			
            //Last name check
            var lastName=frmviolationfee.txt_LastName.value;
            if (lastName=="")
            {
                return AlertForValidation(frmviolationfee.txt_LastName, "Please type in client Last Name.");
            }
			
            for (j = 0; j < lastName.length; j++) 
            { 
                Char = lastName.charAt(j); 
                if (inValidChars.indexOf(Char) > -1) 
                {
                    return AlertForValidation(frmviolationfee.txt_LastName, "Last Name Can not have any numbers");
                }
            }
		    
			
            if (firstName=="")
            {
                return AlertForValidation(frmviolationfee.txt_FirstName, "Please type in client First Name.");
            }
            for (j = 0; j < firstName.length; j++) 
            { 
                Char = firstName.charAt(j); 
                if (inValidChars.indexOf(Char) > -1) 
                {
                    return AlertForValidation(frmviolationfee.txt_FirstName, "First Name Can not have any numbers");
                }
            }
            if(birthmonth == "")
            { 
                return AlertForValidation(document.getElementById("txt_mm"), "Date of birth month can not be empty");
            }
            if(isNaN(birthmonth) == true)
            {
                return AlertForValidation(document.getElementById("txt_mm"), "The month must be a number");
            }
            if(birthdate == "")
            { 
                return AlertForValidation(document.getElementById("txt_dd"), "Date of birth date can not be empty");
            }
            if(isNaN(birthdate) == true)
            {
                return AlertForValidation(document.getElementById("txt_dd"), "The day must be a number");
            }
            if(birthyear == "")
            { 
                return AlertForValidation(document.getElementById("txt_yy"), "Date of birth year can not be empty");
            }
            if(isNaN(birthyear) == true)
            {
                return AlertForValidation(document.getElementById("txt_yy"), "The year must be a number");
            }
			   
            var sDOB = birthmonth + '/' + birthdate + '/' + birthyear;
										
            if (!MMDDYYYYDate(sDOB))
            {			  
                document.getElementById ("txt_dd").focus();
                return(false);
            }
            if(birthyear.length<4)
            {
                $("#txtErrorMessage").text("Please enter a valid 4 digit year greater than 1900");
                $("#errorAlert").modal();
                //alert("Please enter a valid 4 digit year greater than 1900");
                return false;
            }
		    
            var today=new Date();
            var dob=new Date();
            dob.setFullYear(birthyear);
            dob.setMonth(birthmonth-1);
            dob.setDate(birthdate);
         
            if(dob>today)
            {
                return AlertForValidation(document.getElementById ("txt_dd"), "Please Enter Correct Date of Birth, Date of Birth is not Greater than Current Date.");
            }
            debugger;
            if(frmviolationfee.txt_LastName.value != lbltxt_Lastname.innerText)
            {
                return AlertForValidation(frmviolationfee.txt_LastName, "Last Name has been changed, Please update client information before CID Lookup");
            }
		    
            if(frmviolationfee.txt_FirstName.value != lbltxt_FirstName.innerText)
            {
                return AlertForValidation(frmviolationfee.txt_FirstName, "First Name has been changed, Please update client information before CID Lookup");
            }
		    
            if(document.getElementById("txt_mm").value != lbltxt_mm.innerText)
            {
                return AlertForValidation(document.getElementById("txt_mm"), "DOB has been changed, Please update client information before CID Lookup");
            }
			
            if(document.getElementById("txt_dd").value != lbltxt_dd.innerText)
            {
                return AlertForValidation(document.getElementById("txt_dd"), "DOB has been changed, Please update client information before CID Lookup");
            }
			
            if(document.getElementById("txt_yy").value != lbltxt_yy.innerText)
            {
                return AlertForValidation(document.getElementById("txt_yy"), "DOB has been changed, Please update client information before CID Lookup");
            }
			
            document.getElementById("tblCIDProg").style.display = 'block';
            document.getElementById("lnkContactID").style.display = 'none';
		    
        }

        //function ClearCallerID() {
        //        if (document.getElementById("chk_unknown").checked){
        //            document.getElementById("txt_callerId").value = "";
        //            document.getElementById("txt_callerId").disabled = true;
        //            }
        //            else
        //                document.getElementById("txt_callerId").disabled = false;
        //        return false;
        //    }
        function submitForm(i,chk) 
        {		    
            // Afaq 8311 09/30/2010 If Profile has any violation having verified status equal to Bond, return false.
            if (parseInt(document.getElementById('hf_statusCount').value)>0 && document.getElementById('rdbtn_BondYes').checked==false)
            {
                $("#txtErrorMessage").text("You can’t select NO for ‘Does client require bonds ?’ question while profile contains at least one violation with ‘BOND’ case status");
                $("#errorAlert").modal();
              //  alert('You can’t select NO for ‘Does client require bonds ?’ question while profile contains at least one violation with ‘BOND’ case status');
                return false;
            }
          
          
		      
            // Rab Nawaz Khan 11473 10/23/2013 Added for caller ID. . . 
            //	    if ((document.getElementById('txt_callerId').value == "") && (!document.getElementById('chk_unknown').checked))
            //	    {
            //	        alert('Please enter Caller ID of the caller or select Unknown');
            //	        document.getElementById('txt_callerId').focus();
            //	        return false;
            //	    }

            // Abbas Qamar 10114 
            try {
                var ddlDL=document.getElementById("ddl_dlState").value;
                var ddlSSN=document.getElementById("DDLSSNQuestion").value;
			
                if(ddlSSN==0 && document.getElementById('RadioButton4').checked==true) {
                    $("#txtErrorMessage").text("Please Select the SSN Type");
                    $("#errorAlert").modal();
                  //  alert("Please Select the SSN Type");
                    document.getElementById("DDLSSNQuestion").focus();
                    return(false);
                }
	    
	    			
                var ssn = document.getElementById("txt_SSN").value;

                // Hafiz 10288 07/12/2012 commented the below section
                //			
                //          if(ssn.trim().length > 0)
                //			{	
                //    				var v = ssn.replace( /[^0-9]/g , "");
                //                    if(ssn==v )
                //                    {
                //                    	if(ssn.length !=4)
                //                    	{
                //                    	    alert("Please enter Last 4 Digits of your SSN");	
                //	                	    document.getElementById("txt_SSN").focus();
                //                    	    return false;
                //                    	}	
                //                    }
                //				    else
                //                    {
                //            			alert("Please enter numbers 0-9");
                //                	    document.getElementById("txt_SSN").focus();
                //                	    return false;
                //                    }
                //			    
                //			}
	         
                if(ddlDL=='45'|| ddlDL=='14') //should be numeric
                {
                    var txtDl = document.getElementById("txt_dlstr").value;
                    if (isNaN(txtDl))
                    {
                        alert ("Please Enter DL in Numeric");
                        document.getElementById("txt_dlstr").focus();
                        return false;
                    }					
                }

            }
            catch(err)
            {
                //Handle errors here
            }
	    
            // End 10114
            //Sabir Khan 5009 cet session for page refresh ...
            if(i==4 && chk==0)
            {
		           <%-- <%Session["TimeStamp"]=txtTimeStamp.Text;%>;--%>
            }
            //Closed Status Check
        
            if (i == 4 || i == 5) 
            {
                //if (frmviolationfee.ddl_Status.value == 50) 
                if(lbldetailcount.innerText==frmviolationfee.txtulstatus.value)
                {
                    $("#txtErrorMessage").text("Sorry You cannot update or calculate Price on a Status of Closed.");
                    $("#errorAlert").modal();
                   // alert("Sorry You cannot update or calculate Price on a Status of Closed.");
                    return false;
                }
            }
            //Lock Flag Check
            var doyou;
            if (i == 5 ) 
            {
               $("#txtErrorMessage").text("Are you sure you want to lock the amount owed. This will change the amount owed on the payment info page.? (OK = Yes   Cancel = No)");
               $("#errorAlert").modal();
                //doyou = confirm("Are you sure you want to lock the amount owed. This will change the amount owed on the payment info page.? (OK = Yes   Cancel = No)"); 
                //if (doyou == true) 
                //{
                //}
                //else
                //{
                //    return false;
                //}
            }
		
	   
	
            if ((i == 0 ) || (i == 4 ) || (i == 3 ) || (i == 5 ) && (i !=7)) 
            {
                //First name check
                var inValidChars = "0123456789";
                var Char;
                var firstName=frmviolationfee.txt_FirstName.value;
                var birthmonth=document.getElementById("txt_mm").value;
                var birthdate=document.getElementById("txt_dd").value;
                var birthyear=document.getElementById("txt_yy").value;
                if (firstName=="")
                {
                    return AlertForValidation(frmviolationfee.txt_FirstName, "Please type in client First Name.");
                }
                for (j = 0; j < firstName.length; j++) 
                { 
                    Char = firstName.charAt(j); 
                    if (inValidChars.indexOf(Char) > -1) 
                    {
                        return AlertForValidation(frmviolationfee.txt_FirstName, "First Name Can not have any numbers");
                    }
                }
                //Last name check
                var lastName=frmviolationfee.txt_LastName.value;
                if (lastName=="")
                {
                    return AlertForValidation(frmviolationfee.txt_LastName, "Please type in client Last Name.");
                }
			
                if(
                    (document.getElementById("txt_dd").value == "01" || document.getElementById("txt_dd").value == "1") &&
                    (document.getElementById("txt_mm").value == "01" || document.getElementById("txt_mm").value == "1") && 
                    (document.getElementById("txt_yy").value == "1900") && 
                    (document.getElementById("lnkContactID").value == "" || document.getElementById("lnkContactID").value == "CID Lookup")
                  )
                {
                    $("#txtErrorMessage").text("Please select DOB after 01/01/1900");
                    $("#errorAlert").modal();
                    //alert("Please select DOB after 01/01/1900");
                    document.getElementById("txt_dd").focus();
                    return false;
                }
			  
                if(
                    (document.getElementById("lbltxt_dd").value == "01" || document.getElementById("lbltxt_dd").value == "1") &&
                    (document.getElementById("lbltxt_mm").value == "01" || document.getElementById("lbltxt_mm").value == "1") && 
                    (document.getElementById("lbltxt_yy").value == "1900") &&
                    (document.getElementById("lnkContactID").value == "" || document.getElementById("lnkContactID").value == "CID Lookup")
                  )
                {
                    $("#txtErrorMessage").text("Please select DOB after 01/01/1900");
                    $("#errorAlert").modal();
                   // alert("Please select DOB after 01/01/1900");
                    return false;
                }
			
                if(birthmonth == "")
                { 
                    return AlertForValidation(document.getElementById("txt_mm"), "Date of birth month can not be empty");
                }
                if(isNaN(birthmonth) == true)
                {
                    return AlertForValidation(document.getElementById("txt_mm"), "The month must be a number");
                }
                if(birthdate == "")
                { 
                    return AlertForValidation(document.getElementById("txt_dd"), "Date of birth date can not be empty");
                }
                if(isNaN(birthdate) == true)
                {
                    return AlertForValidation(document.getElementById("txt_dd"), "The day must be a number");
                }
                if(birthyear == "")
                { 
                    return AlertForValidation(document.getElementById("txt_yy"), "Date of birth year can not be empty");
                }
                if(isNaN(birthyear) == true)
                {
                    return AlertForValidation(document.getElementById("txt_yy"), "The year must be a number");
                }
			
                //			
                /*
                if( isValidDate (birthyear, birthmonth , birthdate  ) == false )
                {
                    alert('Invalid date of birth entered.\nPlease enter date in MM/DD/YYYY format.');
                    document.getElementById("txt_dd").focus();
                    return false;
                }*/
	     
                //Added by kazim task id:2536
                //The code is written to resolve the problem that Dob should not be greater than Today Date. 
		    
                var sDOB = birthmonth + '/' + birthdate + '/' + birthyear;
										
                if (!MMDDYYYYDate(sDOB))
                {			  
                    document.getElementById ("txt_dd").focus();
                    return(false);
                }
                if(birthyear.length<4)
                {
                    $("#txtErrorMessage").text("Please enter a valid 4 digit year greater than 1900");
                    $("#errorAlert").modal();
                //    alert("Please enter a valid 4 digit year greater than 1900");
                    return false;
                }
		    
                var today=new Date();
                var dob=new Date();
                dob.setFullYear(birthyear);
                dob.setMonth(birthmonth-1);
                dob.setDate(birthdate);
         
                if(dob>today)
                {
                    return AlertForValidation(document.getElementById ("txt_dd"), "Please Enter Correct Date of Birth, Date of Birth is not Greater than Current Date.");
                }
             
                for (j = 0; j < lastName.length; j++) 
                { 
                    Char = lastName.charAt(j); 
                    if (inValidChars.indexOf(Char) > -1) 
                    {
                        return AlertForValidation(frmviolationfee.txt_LastName, "Last Name Can not have any numbers");
                    }
                }
            }
            //Sabir Khan 11509 11/07/2013 Validation for Email Address...
            if((document.getElementById('txtEmailAddress').value == "") && (document.getElementById('chkEmailAddress').checked==false))
            {
                $("#txtErrorMessage").text("If email address is not available then, please check mark the Email Address Not Available option.");
                $("#errorAlert").modal();
              //  alert('If email address is not available then, please check mark the "Email Address Not Available" option.');
                document.getElementById('chkEmailAddress').focus();
                return false;
            }
            if((document.getElementById('txtEmailAddress').value != "") && (document.getElementById('chkEmailAddress').checked==true)) 
            { 
                $("#txtErrorMessage").text("Please either enter Email address or select Email Address not Available checkbox.");
                $("#errorAlert").modal();
              //  alert('Please either enter Email address or select "Email Address not Available" checkbox.'); 
                document.getElementById('chkEmailAddress').focus(); 
                return false; 
            }

          
          
		  
            //            if (document.getElementById('chkEmailAddress').checked == true )
            //		        {
            //			        var chk1 = confirm ("Are you sure the client doesn’t have an email address? This information can be used to send trial notification letters and other related documents to clients. For Processing It without email address, click 'OK’ or ‘CANCEL’ to type in the email address.");
            //			        if(chk1==true)
            //			        {
            //			            document.getElementById('txtEmailAddress').value = '';
            //		                document.getElementById('txtEmailAddress').disabled = true;		                
            //		            }
            //		            else 
            //		            {
            //		                document.getElementById('chkEmailAddress').checked = false;
            //		                document.getElementById('txtEmailAddress').focus();
            //		                return false;
            //		            }
            //		        }          
		
            if(document.getElementById('txtEmailAddress').value!="" )
            {			
                if( isEmail(document.getElementById('txtEmailAddress').value)== false)
                {
                    alert ("Please enter Email Address in Correct format.");
                    document.getElementById('txtEmailAddress').focus(); 
                    return false;			   
                }
            }
		
		   
            if ((i == 0 ) || (i == 4 ) || (i == 3 ) || (i == 2) || (i == 5) && (i !=7)) 
            {
			
                //Waqas 6895 11/02/2009 validation
                var inValidChars = "0123456789";
                var Char;
			
                if ( document.getElementById("txt_CC11") == null)
                {
                    //Ozair 5771 05/16/2009 modified section name commnets   
                    //Waqas 5864 07/16/2009 Message changed in the case of ALR
                    //Waqas 6342 08/24/2009 New changes for all criminal and family cases
                    if(document.getElementById("hf_IsPreHire").value == "1" )
                    {
                        $("#txtErrorMessage").text("Please add contact number in Pre Hire Question section");
                        $("#errorAlert").modal();
                       // alert("Please add contact number in Pre Hire Question section");			
                    }
                    else
                    {
                        $("#txtErrorMessage").text("Please add contact number in fee question section");
                        $("#errorAlert").modal();
                      //  alert("Please add contact number in fee question section");			
                    }
                    return false;
                }
			
			
			
                if ( ! frmviolationfee.txt_CC11.disabled)
                {			
			
                    //Contact No Check
                    var intphonenum1 = frmviolationfee.txt_CC11.value;
                    var intphonenum2 = frmviolationfee.txt_CC12.value;
                    var intphonenum3 = frmviolationfee.txt_CC13.value;
                    var intphonenum4 = frmviolationfee.txt_CC14.value;
                    var intcontacttype = frmviolationfee.ddl_ContactType.value;
			
                    if  (((intphonenum1 == "") || (intphonenum2 == "") || (intphonenum3 == "")) && (i !=7) && document.getElementById("lblContact").value != "")
                    {
                        return AlertForValidation(frmviolationfee.txt_CC11, "Please add Phone Numbers."); 
                    }
                    if  (((intphonenum1 == "") || (intphonenum2 == "") || (intphonenum3 == "")) && (i !=7) ) 
                    {
                        return AlertForValidation(frmviolationfee.txt_CC11, "Invalid Phone Number. Please don't use any dashes or space.");
                    }

                    if ((isNaN(intphonenum1) == true ) && (i !=7)) 
                    {
                        return AlertForValidation(frmviolationfee.txt_CC11, "Invalid Phone Number. Please don't use any dashes or space");
                    }
                    if ((isNaN(intphonenum2) == true )&& (i !=7)) 
                    {
                        return AlertForValidation(frmviolationfee.txt_CC12, "Invalid Phone Number. Please don't use any dashes or space");
                    }
                    if ((isNaN(intphonenum3) == true )&& (i !=7)) 
                    {
                        return AlertForValidation(frmviolationfee.txt_CC13, "Invalid Phone Number. Please don't use any dashes or space");
                    }
                    /*if ((isNaN(intphonenum4) == true )&& (i !=7)) 
                    {
                        alert("Invalid Phone Number. Please don't use any dashes or space");
                        frmviolationfee.txt_CC14.focus();
                        return false;
                    }*/
                    //Contact Type Check
                    if (isNaN(intphonenum1) == false && (intphonenum1 != "") && (i !=7)) 
                    {
                        if (intcontacttype == 0 ) 
                        {
                            return AlertForValidation(frmviolationfee.ddl_ContactType, "Please Select Contact Type");
                        }
                    }
                    //Contact No Check
                    if ((intcontacttype > 0 )&& (i !=7)) 
                    {
                        if  ((intphonenum1 == "") || (intphonenum2 == "") || (intphonenum3 == "")) 
                        {
                            return AlertForValidation(frmviolationfee.txt_CC11, "Invalid Phone Number. Please don't use any dashes or space.");
                        }
                    }
			
                }
			
			
                //Language Check
                var langflag = 1		  	
                if ((frmviolationfee.ddl_language.value == "-1"))
                    langflag = 0;
		 	
                if ((langflag == 0) && (i !=7)) 
                {
                    return AlertForValidation(frmviolationfee.ddl_language, "Please Specify Client's Language");
                }
                // Noufil 6138 08/21/2009 Show javascript message IF user didnt select any language from other language dropdown
                if (frmviolationfee.ddl_language.value == "0" && frmviolationfee.dd_OtherLanguage.value == "-1" && frmviolationfee.dd_OtherLanguage.length > 1)
                {
                    return AlertForValidation(frmviolationfee.dd_OtherLanguage, "Please Specify Client's Language");
                }
			
                // Noufil 6138 08/21/2009 Show javascript message other language didn't contain any further more language
                if (frmviolationfee.ddl_language.value == "0" && frmviolationfee.dd_OtherLanguage.value == "-1" && frmviolationfee.dd_OtherLanguage.length == 1)
                {
                    return AlertForValidation(frmviolationfee.dd_OtherLanguage, "No other language(s) available to add.");
                }
			
                //Waqas 6599 09/19/2009 checking.
                //Farrukh 9925 11/28/2011 Remove Occupation and Employer
                //	        if(document.getElementById('chkIsUnempoyed').checked == false)
                //            {
                //                
                //                var txtoccupation = document.getElementById('txtOccupation').value.trim();
                //                if(txtoccupation == "")
                //                {
                //                    alert("Please enter occupation.");
                //                    document.getElementById('txtOccupation').focus();
                //                    return false; 
                //                }
                //                
                //                for (j = 0; j < txtoccupation.length; j++) 
                //			    { 
                //				    Char = txtoccupation.charAt(j); 
                //				    if (inValidChars.indexOf(Char) > -1) 
                //				    {
                //					    alert("Occupation can not have any numbers");
                //					    document.getElementById('txtOccupation').focus();
                //					    return false;
                //				    }
                //			    }
                //                
                //                
                //                var txtEmployer = document.getElementById('txtEmployer').value.trim();
                //                if(txtEmployer == "")
                //                {
                //                    alert("Please enter Employer.");
                //                    document.getElementById('txtEmployer').focus();
                //                    return false; 
                //                }
                //                
                //                for (j = 0; j < txtEmployer.length; j++) 
                //			    { 
                //				    Char = txtEmployer.charAt(j); 
                //				    if (inValidChars.indexOf(Char) > -1) 
                //				    {
                //					    alert("Employer can not have any numbers");
                //					    document.getElementById('txtEmployer').focus();
                //					    return false;
                //				    }
                //			    }
                //            }
                //            else if(document.getElementById('chkIsUnempoyed').checked == true)
                //            {
                //                if(document.getElementById('txtOccupation').value != "")
                //                {
                //                    alert("'Unemployed or refused to give' is set. Occupation is not required.");
                //                    document.getElementById('txtOccupation').focus();
                //                    return false; 
                //                }
                //            }
        
        
                //Waqas 5864 06/26/2009 
                //Waqas 6342 08/24/2009 New changes for all criminal and family cases
                if(document.getElementById("hf_IsPreHire").value == "1" )
                {
                    if(document.getElementById('rbtnPriorAttorneyYes').checked == false && document.getElementById('rbtnPriorAttorneyNo').checked == false )
                    {
                        return AlertForValidation(document.getElementById('rbtnPriorAttorneyYes'), "Please specify prior attorney (Yes/No)");
                    }
			    
                    if(document.getElementById('rbtnPriorAttorneyYes').checked == true)
                    {
                        if(document.getElementById('drpAtt').value == "0")
                        {
                            return AlertForValidation(document.getElementById('drpAtt'), "Please select prior attorney type");
                        }
			        
                        var Pname = document.getElementById('txtPriorAttorney').value;
                        if(Pname == "")
                        {
                            return AlertForValidation(document.getElementById('txtPriorAttorney'), "Please enter prior attorney name");
                        }
		            
                        for (j = 0; j < Pname.length; j++) 
                        { 
                            Char = Pname.charAt(j); 
                            if (inValidChars.indexOf(Char) > -1) 
                            {
                                return AlertForValidation(document.getElementById('txtPriorAttorney'), "Prior attorney name can not have any numbers");
                            }
                        }		            
                    }			     
                }
                //Asad Ali  8153 09/20/2010 this question is moved in pre hire section so no need to check active client just we have to check for Criminal case 
                var IsCriminalCourt =document.getElementById('<%=hf_ISCriminalCourtForALR.ClientID%>');
	        if(IsCriminalCourt.value=="1" && document.getElementById('rBtn_ALRHearingRequiredYes').checked == false && document.getElementById('rBtn_ALRHearingRequiredNo').checked == false )
	        {
	            return AlertForValidation(frmviolationfee.rBtn_ALRHearingRequiredYes, "Please specify 'Does the client require an ALR Hearing?' (Yes/No)");
	        }
	        //Waqas 6342 08/24/2009 New changes for all criminal and family cases
	        //Hafiz 10288 07/19/2012 commented the below section
	        //			if(document.getElementById("hf_IsALROnly").value == "1" )
	        //			{
	        //			    if(document.getElementById("lbl_ActiveFlag").innerText == "1")
	        //			    {
	        //			        //Waqas 6342 08/12/2009 ALR required
	        //	               
	        //                             
	        //	                if(document.getElementById('rBtn_ALRHearingRequiredNo').checked == true)
	        //                    {
	        //                        var txtWhy = document.getElementById('txtALRHearingRequiredAnswer').value;
	        //	                    if(txtWhy == "")
	        //	                    {
	        //	                        return AlertForValidation(document.getElementById('txtALRHearingRequiredAnswer'), "Please specify an answer if ALR hearing is not required.");
	        //	                    }		                
	        //                    }
	        //                    else
	        //                    {
	        //                    //Yasir Kamal 7150 01/01/2010 display county of arrest
	        //                        if(document.getElementById('HLkCounty').style.display=='block')
	        //                        {
	        //                            alert("Please set County of court.");
	        //			                return false;
	        //                        }
	        //                          
	        //                    
	        //	                    //Waqas 5864 07/17/2009 Check for Officer name
	        //	                    var Offname = document.getElementById('txtALROfficerName').value;
	        //	                    for (j = 0; j < Offname.length; j++) 
	        //	                    { 
	        //		                    Char = Offname.charAt(j); 
	        //		                    if (inValidChars.indexOf(Char) > -1) 
	        //		                    {
	        //		                        return AlertForValidation(document.getElementById('txtALROfficerName'), "Officer name can not have any numbers");
	        //		                    }
	        //	                    }
	        //    		            
	        //    		            
	        //	                    //Waqas 5864 07/17/2009 Check for City
	        //	                    var OffCity = document.getElementById('txtALRCity').value;
	        //	                    for (j = 0; j < OffCity.length; j++) 
	        //	                    { 
	        //		                    Char = OffCity.charAt(j); 
	        //		                    if (inValidChars.indexOf(Char) > -1) 
	        //		                    {
	        //		                        return AlertForValidation(document.getElementById('txtALRCity'), "Officer's City can not have any numbers");
	        //		                    }
	        //	                    }
	        //    		            
	        //    		            
	        //	                    //Waqas 5864 07/30/2009 Check for Zip
	        //    		            
	        //	                    if (isNaN(frmviolationfee.txtALRZip.value))
	        //		                {
	        //		                    return AlertForValidation(frmviolationfee.txtALRZip, "Officer's Zip Code must be numeric.");
	        //		                }
	        //    		            
	        //	                    //Waqas 5864 07/30/2009 Check for Zip
	        //		                //Waqas 6342 08/12/2009 ALR required
	        //	                    if (frmviolationfee.ddl_ALRState.value == "")
	        //		                {
	        //		                    return AlertForValidation(frmviolationfee.ddl_ALRState, "Please specify Officer's state");
	        //		                }
	        //    		            
	        //	                    // Waqas 5864 07/17/2009 Contact No Check
	        //	                    var intphonenum1 = frmviolationfee.txt_Attacc11.value;
	        //	                    var intphonenum2 = frmviolationfee.txt_Attcc12.value;
	        //	                    var intphonenum3 = frmviolationfee.txt_Attcc13.value;
	        //	                    var intphonenum4 = frmviolationfee.txt_Attcc14.value;
	        //    		            
	        //	                    if ((isNaN(intphonenum1) == true ) ) 
	        //	                    {
	        //	                        return AlertForValidation(frmviolationfee.txt_Attacc11, "Invalid Phone Number. Please don't use any dashes or space");
	        //	                    }
	        //	                    if ((isNaN(intphonenum2) == true )) 
	        //	                    {
	        //	                        return AlertForValidation(frmviolationfee.txt_Attcc12, "Invalid Phone Number. Please don't use any dashes or space");
	        //	                    }
	        //	                    if ((isNaN(intphonenum3) == true )) 
	        //	                    {
	        //	                        return AlertForValidation(frmviolationfee.txt_Attcc13, "Invalid Phone Number. Please don't use any dashes or space");
	        //	                    }
	        //    		            
	        //	                    //Waqas 5864 07/17/2009 Check for Arresting Agency name
	        //	                    var ArrestingAgencyname = document.getElementById('txtALRArrestingAgency').value;
	        //	                    for (j = 0; j < ArrestingAgencyname.length; j++) 
	        //	                    { 
	        //		                    Char = ArrestingAgencyname.charAt(j); 
	        //		                    if (inValidChars.indexOf(Char) > -1) 
	        //		                    {
	        //		                        return AlertForValidation(document.getElementById('txtALRArrestingAgency'), "Arresting agency name can not have any numbers");
	        //		                    }
	        //	                    }
	        //    	                
	        //	                    if (isNaN(frmviolationfee.txtALROfficerMileage.value))
	        //		                {
	        //		                    return AlertForValidation(frmviolationfee.txtALROfficerMileage, "Officer mileage must be numeric.");
	        //		                }
	        //                        //Asad Ali 8153 remove validation b/c this ALR hearing Required Question is Moved question move in Pre hire section    
	        //                        if(document.getElementById("lbl_ActiveFlag").innerText=="1" && document.getElementById('rBtnArrOffObsOffYes').checked == false && document.getElementById('rBtnArrOffObsOffNo').checked == false )
	        //                         {
	        //                            return AlertForValidation(frmviolationfee.rBtnArrOffObsOffYes, "Please specify 'Is the Arresting Officer the Observing Officer? ' (Yes/No)");
	        //                         }			               
	        //    		            
	        //    		            //Asad Ali 7991 07/14/2010 Req#4 If "Is the Arresting Officer the Observing Officer" is set to No then we need to enter "Observing Officer Name".
	        //    		            if(document.getElementById('rBtnArrOffObsOffNo').checked == true && frmviolationfee.txtALROBSOfficerName.value=="" && document.getElementById('rBtnArrOffObsOffNo').checked == true )
	        //    		            {
	        //    		                return AlertForValidation(frmviolationfee.txtALROBSOfficerName, "Please specify Observing Officer Name");
	        //    		            } 
	        //    		            if(document.getElementById('rbBTOArresting').checked == true && document.getElementById('txtALROfficerName').value == '') 
	        //                        {                 
	        //                            return AlertForValidation(frmviolationfee.txtALROfficerName, "First specify Arresting Officer Name");
	        //			            }
	        //			                              
	        //			            if(document.getElementById('rBtnArrOffObsOffYes').checked == true && document.getElementById('txtALROfficerName').value == '')
	        //                        {
	        //                            return AlertForValidation(frmviolationfee.txtALROfficerName, "First specify Arresting Officer Name");
	        //                        }
	        //    			                      
	        //    		            
	        //		                //Waqas 6342 08/12/2009 check for observing officer.
	        //		                var OBSOffname = document.getElementById('txtALROBSOfficerName').value;
	        //	                    for (j = 0; j < OBSOffname.length; j++) 
	        //	                    { 
	        //		                    Char = OBSOffname.charAt(j); 
	        //		                    if (inValidChars.indexOf(Char) > -1) 
	        //		                    {
	        //		                        return AlertForValidation(document.getElementById('txtALROBSOfficerName'), "Observing officer name can not have any numbers");
	        //		                    }
	        //	                    }
	        //    		            
	        //    		            
	        //	                    var OBSOffCity = document.getElementById('txtALROBSCity').value;
	        //	                    for (j = 0; j < OBSOffCity.length; j++) 
	        //	                    { 
	        //		                    Char = OBSOffCity.charAt(j); 
	        //		                    if (inValidChars.indexOf(Char) > -1) 
	        //		                    {
	        //		                        return AlertForValidation(document.getElementById('txtALROBSCity'), "Observing officer's city can not have any numbers");
	        //		                    }
	        //	                    }
	        //    		            
	        //    		            
	        //    	                
	        //	                    if (isNaN(frmviolationfee.txtALROBSZip.value))
	        //		                {
	        //		                    return AlertForValidation(frmviolationfee.txtALROBSZip, "Observing officer's zip code must be numeric.");
	        //		                }
	        //    		            
	        //	                    if (frmviolationfee.ddl_ALROBSState.value == "")
	        //		                {
	        //		                    return AlertForValidation(frmviolationfee.ddl_ALROBSState, "Please specify observing officer's state");
	        //		                }
	        //    		            
	        //	                    var intOBSphonenum1 = frmviolationfee.txt_OBSAttacc11.value;
	        //	                    var intOBSphonenum2 = frmviolationfee.txt_OBSAttcc12.value;
	        //	                    var intOBSphonenum3 = frmviolationfee.txt_OBSAttcc13.value;
	        //	                    var intOBSphonenum4 = frmviolationfee.txt_OBSAttcc14.value;
	        //    		            
	        //	                    if ((isNaN(intOBSphonenum1) == true ) ) 
	        //	                    {
	        //	                        return AlertForValidation(frmviolationfee.txt_OBSAttacc11, "Invalid Phone Number. Please don't use any dashes or space");
	        //	                    }
	        //	                    if ((isNaN(intOBSphonenum2) == true )) 
	        //	                    {
	        //	                        return AlertForValidation(frmviolationfee.txt_OBSAttcc12, "Invalid Phone Number. Please don't use any dashes or space");
	        //	                    }
	        //	                    if ((isNaN(intOBSphonenum3) == true )) 
	        //	                    {
	        //	                        return AlertForValidation(frmviolationfee.txt_OBSAttcc13, "Invalid Phone Number. Please don't use any dashes or space");
	        //	                    }
	        //    	                
	        //    	                
	        //	                    var OBSArrestingAgencyname = document.getElementById('txtALROBSArrestingAgency').value;
	        //	                    for (j = 0; j < OBSArrestingAgencyname.length; j++) 
	        //	                    { 
	        //		                    Char = OBSArrestingAgencyname.charAt(j); 
	        //		                    if (inValidChars.indexOf(Char) > -1) 
	        //		                    {
	        //		                        return AlertForValidation(document.getElementById('txtALROBSArrestingAgency'), "Observing arresting agency name can not have any numbers");
	        //		                    }
	        //	                    }
	        //    	                
	        //    	                
	        //	                    if (isNaN(frmviolationfee.txtALROBSOfficerMileage.value))
	        //		                {
	        //		                    return AlertForValidation(frmviolationfee.txtALROBSOfficerMileage, "Observing officer mileage must be numeric.");
	        //		                }
	        //    		            
	        //    	                
	        //    			        //Yasir Kamal 7150 01/01/2010 ALR BTO,BTS modified.
	        //		                if(document.getElementById('drp_IntoxilyzerTaken').value == "1")
	        //                        {
	        //                            //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency 
	        //                            if(document.getElementById("lbl_ActiveFlag").innerText=="1" && document.getElementById('rbtn_IntoxResultPass').checked == false && document.getElementById('rbtn_IntoxResultFail').checked == false )
	        //                             {
	        //                                return AlertForValidation(frmviolationfee.rbtn_IntoxResultPass, "Please specify Intoxilyzer Results (Pass/Fail)");
	        //                             }
	        //                             //Asad Ali 7991 07/14/2010 apply these validation when Intoxilyzer Taken instead of pass fail
	        //                             //if(document.getElementById('rbtn_IntoxResultPass').checked == true)
	        //                             //{     //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency 
	        //                                      /*if(document.getElementById('rbBTOArresting').checked == false && document.getElementById('rbBTOObserving').checked == false && document.getElementById('rblBTOName').checked == false)
	        //                                          {                 
	        //                                             alert("Please specify BTO Name");
	        //			                                 frmviolationfee.rbBTOArresting.focus();
	        //			                                 return false;
	        //			                              }
	        //    			                          */
	        //    			                      
	        //			                          if(document.getElementById('rblBTOName').checked == true && document.getElementById('txtBTOName').value == '')
	        //                                      {         
	        //                                         return AlertForValidation(frmviolationfee.txtBTOName, "Please Enter BTO Name");        
	        //		                              }    
	        //    			                      
	        //			                        var BTOname = document.getElementById('txtBTOName').value;
	        //	                                for (j = 0; j < BTOname.length; j++) 
	        //	                                { 
	        //		                                Char = BTOname.charAt(j); 
	        //		                                if (inValidChars.indexOf(Char) > -1) 
	        //		                                {
	        //		                                    return AlertForValidation(document.getElementById('txtBTOName'), "BTO name can not have any numbers");
	        //		                                }
	        //	                                }
	        //			                      
	        //			                      
	        //			                          if(document.getElementById('rbBTOArresting').checked == true && document.getElementById('txtALROfficerName').value == '') 
	        //                                      {  
	        //                                         return AlertForValidation(frmviolationfee.rbBTOArresting, "First specify Arresting Officer Name");               
	        //		                              }
	        //    			                      if(document.getElementById('rBtnArrOffObsOffYes').checked == true && document.getElementById('txtALROfficerName').value == '')
	        //    			                      {
	        //    			                        return AlertForValidation(frmviolationfee.rbBTOArresting, "First specify Arresting Officer Name");
	        //    			                      }
	        //			                          if(document.getElementById('rbBTOObserving').checked == true && document.getElementById('txtALROBSOfficerName').value == '' && document.getElementById('rBtnArrOffObsOffNo').checked == true)
	        //		                              {    
	        //		                                return AlertForValidation(frmviolationfee.rbBTOObserving, "First specify Observing Officer Name");             
	        //		                              }
	        //    			                          
	        //    			                          //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency
	        //			                        if(document.getElementById('txtBTSFirstName').value == '' && document.getElementById("lbl_ActiveFlag").innerText=="1")
	        //                                     {
	        //                                        return AlertForValidation(frmviolationfee.txtBTSFirstName, "Please specify BTS First Name");
	        //                                     }
	        //    	                            
	        //                                     var BTSFname = document.getElementById('txtBTSFirstName').value;
	        //	                                    for (j = 0; j < BTSFname.length; j++) 
	        //	                                    { 
	        //		                                    Char = BTSFname.charAt(j); 
	        //		                                    if (inValidChars.indexOf(Char) > -1) 
	        //		                                    {
	        //		                                        return AlertForValidation(document.getElementById('txtBTSFirstName'), "BTS first name can not have any numbers");
	        //		                                    }
	        //	                                    }
	        //        	                         //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency
	        //                                    if(document.getElementById('txtBTSLastName').value == '' && document.getElementById("lbl_ActiveFlag").innerText=="1")
	        //                                    {
	        //                                        return AlertForValidation(frmviolationfee.txtBTSLastName, "Please specify BTS Last Name");
	        //                                    }      
	        //        	                         
	        //                                    var BTSLname = document.getElementById('txtBTSLastName').value;
	        //                                    for (j = 0; j < BTSLname.length; j++) 
	        //                                    { 
	        //	                                    Char = BTSLname.charAt(j); 
	        //	                                    if (inValidChars.indexOf(Char) > -1) 
	        //                                        {
	        //                                            return AlertForValidation(document.getElementById('txtBTSLastName'), "BTS last name can not have any numbers");
	        //	                                    }
	        //                                    }    

	        //                            //}
	        //                             
	        //                             
	        //                        }
	        //    	                
	        //                        if(document.getElementById('rbtn_IntoxResultPass').checked == true)
	        //                        {
	        ////                            if(document.getElementById('rBtn_BtoBtsSameYes').checked == false && document.getElementById('rBtn_BtoBtsSameNo').checked == false )
	        ////                             {
	        ////                                alert("Please specify 'Was BTO same as BTS' (Yes/No)");
	        ////			                    frmviolationfee.rBtn_BtoBtsSameYes.focus();
	        ////			                    return false;
	        ////                             }
	        //    	                     
	        ////                             if(document.getElementById('txtBTOFirstName').value == '')
	        ////                             {
	        ////                                alert("Please specify BTO First Name");
	        ////			                    frmviolationfee.txtBTOFirstName.focus();
	        ////			                    return false;
	        ////                             }
	        //    	                     
	        ////                             var BTOFname = document.getElementById('txtBTOFirstName').value;
	        ////	                            for (j = 0; j < BTOFname.length; j++) 
	        ////	                            { 
	        ////		                            Char = BTOFname.charAt(j); 
	        ////		                            if (inValidChars.indexOf(Char) > -1) 
	        ////		                            {
	        ////			                            alert("BTO first name can not have any numbers");
	        ////			                            document.getElementById('txtBTOFirstName').focus();
	        ////			                            return false;
	        ////		                            }
	        ////	                            }
	        //    	                     
	        ////                             if(document.getElementById('txtBTOLastName').value == '')
	        ////                             {
	        ////                                alert("Please specify BTO Last Name");
	        ////			                    frmviolationfee.txtBTOLastName.focus();
	        ////			                    return false;
	        ////                             }
	        ////    	                     
	        ////                             var BTOLname = document.getElementById('txtBTOLastName').value;
	        ////                             for (j = 0; j < BTOLname.length; j++) 
	        ////                             { 
	        ////	                            Char = BTOLname.charAt(j); 
	        ////	                            if (inValidChars.indexOf(Char) > -1) 
	        ////	                            {
	        ////		                            alert("BTO last name can not have any numbers");
	        ////		                            document.getElementById('txtBTOLastName').focus();
	        ////		                            return false;
	        ////	                            }
	        ////                             }
	        //    		                    
	        //                        }
	        //    	                
	        ////                        if(document.getElementById('rBtn_BtoBtsSameNo').checked == true)
	        ////                        {
	        ////                              if(document.getElementById('txtBTSFirstName').value == '')
	        ////                             {
	        ////                                alert("Please specify BTS First Name");
	        ////			                    frmviolationfee.txtBTSFirstName.focus();
	        ////			                    return false;
	        ////                             }
	        ////    	                     
	        ////                             var BTSFname = document.getElementById('txtBTSFirstName').value;
	        ////	                            for (j = 0; j < BTSFname.length; j++) 
	        ////	                            { 
	        ////		                            Char = BTSFname.charAt(j); 
	        ////		                            if (inValidChars.indexOf(Char) > -1) 
	        ////		                            {
	        ////			                            alert("BTS first name can not have any numbers");
	        ////			                            document.getElementById('txtBTSFirstName').focus();
	        ////			                            return false;
	        ////		                            }
	        ////	                            }
	        ////    	                     
	        ////                             if(document.getElementById('txtBTSLastName').value == '')
	        ////                             {
	        ////                                alert("Please specify BTS Last Name");
	        ////			                    frmviolationfee.txtBTSLastName.focus();
	        ////			                    return false;
	        ////                             }      
	        ////    	                     
	        ////                             var BTSLname = document.getElementById('txtBTSLastName').value;
	        ////                             for (j = 0; j < BTSLname.length; j++) 
	        ////                             { 
	        ////	                            Char = BTSLname.charAt(j); 
	        ////	                            if (inValidChars.indexOf(Char) > -1) 
	        ////	                            {
	        ////		                            alert("BTS last name can not have any numbers");
	        ////		                            document.getElementById('txtBTSLastName').focus();
	        ////		                            return false;
	        ////	                            }
	        ////                             }
	        ////                        }
	        //                    }
	        //                }
	        //	     }  
	        //7150 end
	        //End 10288
	        // tahir 4786 10/08/2008 
			
	        var iCaseType = frmviolationfee.ddl_CaseType.value;
			
	        if (iCaseType == "4" && !frmviolationfee.rbtn_vplanyes.checked && !frmviolationfee.rbtn_vplanno.checked)
	        {
	            return AlertForValidation(frmviolationfee.rdbtn_vplanyes, "Please let us know if payment plan needed");
	        }
			
	        // end 4786
			
	        var courtloc = frmviolationfee.ddl_CourtLocation.value;
	        if (courtloc != 3037) 
	        {
	            var strActiveFlag = document.getElementById("lbl_ActiveFlag").innerText;
	            var strIsNew = document.getElementById("lblisNew").innerText;
			    
	            //Validate Fee Questions For Traffic Case
	            if((document.getElementById("questionsdetail") != null) && (i!=7) )
	            {
	                //Bond Check
	                if ( !frmviolationfee.rdbtn_BondYes.checked && !frmviolationfee.rdbtn_BondNo.checked) 
	                {
	                    return AlertForValidation(frmviolationfee.rdbtn_BondNo, "Please let us know if Bonds are required");
	                }
                    
	                //Zeeshan Ahmed 3535 04/08/2008
	                //FTA Modifications 
	                if(i!=7)
	                {
	                    var isInsideCourt = document.getElementById("hfIsInsideCase").value;
	                    var hasFTAViolations = document.getElementById("hfHasFTAViolations").value;
    				    
	                    if ( frmviolationfee.rdbtn_BondYes.checked && isInsideCourt=="1" && hasFTAViolations=="0")
	                    {
	                        return AlertForValidation(frmviolationfee.rdbtn_BondYes, "To change bond flag to Yes. Case must have a FTA ticket.To process the case please add an FTA ticket or change bond flag to No.");
	                    }
	                }
	                //Waqas 6599 09/30/2009 Check for vehicle type for traffic cases 
	                if (!frmviolationfee.rBtnCommercialVehicle.checked && !frmviolationfee.rBtnMotorCycle.checked && !frmviolationfee.rBtnCar.checked) 
	                {
	                    return AlertForValidation(frmviolationfee.rBtnCommercialVehicle, "Please select vehicle type.");
	                }
				    
	                //CDL Check
	                if (!frmviolationfee.rdbtn_CDLYes.checked && !frmviolationfee.rdbtn_CDLNo.checked) 
	                {
	                    return AlertForValidation(frmviolationfee.rdbtn_CDLNo, "Please let us know if you have a CDL");
	                }
				    
	                //Accident Check
	                if (!frmviolationfee.rdbtn_AccidentYes.checked && !frmviolationfee.rdbtn_AccidentNo.checked) 
	                {
	                    return AlertForValidation(frmviolationfee.rdbtn_AccidentNo, "Please let us know if there was an Accident");
	                }
				    
				    		    
	                //Late Fee Check
	                if ( !frmviolationfee.rbtn_lateno.checked  &&  !frmviolationfee.rbtn_lateyes.checked)
	                {
	                    return AlertForValidation(frmviolationfee.rbtn_lateyes, "Please select late fee flag.");
	                }
			    
	                if ( frmviolationfee.hf_latefee.value == "1")
	                {
	                    if (frmviolationfee.rbtn_lateno.checked)
	                    {
	                        return AlertForValidation(frmviolationfee.rbtn_lateyes, "Please select late fee to yes because your court date is too near.");
	                    }
	                }
                    
	                if (strActiveFlag =="0" || strIsNew == "1")
	                {
	                    if (frmviolationfee.rbtn_FollowUp_Yes.checked==false && frmviolationfee.rbtn_FollowUp_No.checked == false)
	                    {
	                        return AlertForValidation(frmviolationfee.rbtn_FollowUp_Yes, "Please select an option for call back.");
	                    }
	                }
				    
	                //Haris Ahmed 10381 08/30/2012 Validate immigration logic
	                //Farrukh 11180 06/28/2013 Removed "SPANISH" language check for immigration validation
	                if (document.getElementById("hfCanBePotentialVisaClients").value == "1"){
	                    if (document.getElementById("rbtn_interestedinworkvisayes").checked == false && document.getElementById("rbtn_interestedinworkvisano").checked == false)
	                    {
	                        return AlertForValidation(document.getElementById("rbtn_interestedinworkvisayes"), "Please select an option for Interested in a Work Visa if you don't have a SSN?");
	                    }
	                    else if (document.getElementById("rbtn_interestedinworkvisayes").checked == true && document.getElementById("txtImmigrationComments").value == '')
	                    {
	                        return AlertForValidation(document.getElementById("txtImmigrationComments"), "Please enter Immigration Comments");
	                    }
	                }
	            }
			
	            //Zeeshan Ahmed 3979 05/15/2008 Validate Fee Questions For Criminal Case
	            if((document.getElementById("cquestionsdetail") != null) && (i!=7) )
	            {
	                //Question 1 Check
	                /*if ( !frmviolationfee.rdbtn_cBondYes.checked && !frmviolationfee.rdbtn_cBondNo.checked) 
				    {
					    alert("Please let us know if Bonds are required");
					    frmviolationfee.rdbtn_cBondNo.focus();
					    return false;
				    }
                                                           
                     //Question 2 Check
				    if (!frmviolationfee.rdbtn_cAccidentYes.checked && !frmviolationfee.rdbtn_cAccidentNo.checked) 
				    {
					    alert("Please let us know if there was an Accident")
					    frmviolationfee.rdbtn_cAccidentNo.focus();
					    return false;
				    }
				    
				    //Question 3 Check
				    if (!frmviolationfee.rdbtn_cCDLYes.checked && !frmviolationfee.rdbtn_cCDLNo.checked) 
				    {
					    alert("Please let us know if you have a CDL");
					    frmviolationfee.rdbtn_cCDLNo.focus();
					    return false;
				    }
			    
			        //Question 4 Check
			        if ( !frmviolationfee.rbtn_clateno.checked  &&  !frmviolationfee.rbtn_clateyes.checked)
				    {
				        alert("Please select late fee flag.");
				        frmviolationfee.rbtn_clateyes.focus();
				        return false;
				    }*/
                    
	                //Question 5 Check
	                if ((strActiveFlag =="0" || strIsNew == "1") && (frmviolationfee.rbtn_cFollowUp_Yes.checked==false && frmviolationfee.rbtn_cFollowUp_No.checked == false))
	                {
	                    return AlertForValidation(frmviolationfee.rbtn_cFollowUp_Yes, "Please select an option for call back.");
	                }
				    
	            }
			
	            //Zeeshan Ahmed 3979 05/15/2008 Validate Fee Questions For Civil Case
	            if((document.getElementById("vquestionsdetail") != null) && (i!=7) )
	            {
	                //Question 1 Check
	                /*if ( !frmviolationfee.rdbtn_vBondYes.checked && !frmviolationfee.rdbtn_vBondNo.checked) 
				    {
					    alert("Please select civil question 1?");
					    frmviolationfee.rdbtn_vBondNo.focus();
					    return false;
				    }
                                                           
                     //Question 2 Check
				    if (!frmviolationfee.rdbtn_vAccidentYes.checked && !frmviolationfee.rdbtn_vAccidentNo.checked) 
				    {
					    alert("Please select civil question 2?");
        			    frmviolationfee.rdbtn_vAccidentNo.focus();
		    		    return false;
				    }
			    
				    //Question 3 Check
				    if (!frmviolationfee.rdbtn_vCDLYes.checked && !frmviolationfee.rdbtn_vCDLNo.checked) 
				    {
					    alert("Please select civil question 3?");
					    frmviolationfee.rdbtn_vCDLNo.focus();
					    return false;
				    }*/
			    
	                //Question 4 Check
	                /* if ( !frmviolationfee.rbtn_vlateno.checked  &&  !frmviolationfee.rbtn_vlateyes.checked)
                     {
                         alert("Please select late fee flag.");
                         frmviolationfee.rbtn_vlateyes.focus();
                         return false;
                     }*/
                    
	                //Question 5 Check
	                if ((strActiveFlag =="0" || strIsNew == "1") && (frmviolationfee.rbtn_vFollowUp_Yes.checked==false && frmviolationfee.rbtn_vFollowUp_No.checked == false))
	                {
	                    return AlertForValidation(frmviolationfee.rbtn_vFollowUp_Yes, "Please select an option for call back.");
	                }
				    
	            }
	        }			
        }
		
        if ((i == 0 ) || (i == 4 ) || (i == 3 ) || (i == 5) && (i !=7))
        {
            //Initial And Final Adjustment Check
            if(lblLockFlag.innerText=="True" || lblTFC.innerText=="1")
            {
				
                var a=frmviolationfee.txt_FinalAdjustment.value *1.0;
                if(a%1>0)
                {
                    return AlertForValidation(frmviolationfee.txt_FinalAdjustment, "Final adjustment amount cannot take decimal values.");
                }
				
				
                if (isNaN(frmviolationfee.txt_FinalAdjustment.value))
                {
                    return AlertForValidation(frmviolationfee.txt_FinalAdjustment, "Final Adjustment must be numeric.");
                }
            } 
            else if(lblLockFlag.innerText=="False" || lblTFC.innerText=="0")
            {
                var a=frmviolationfee.txt_InitialAdjustment.value *1.0;
                if(a%1>0)
                {
                    return AlertForValidation(frmviolationfee.txt_InitialAdjustment, "Initial adjustment amount cannot take decimal values.");
                }
                if (isNaN(frmviolationfee.txt_InitialAdjustment.value))
                {
                    return AlertForValidation(frmviolationfee.txt_InitialAdjustment, "Initial Adjustment must be numeric.");
                }
            }
		  
        }
			
        if((chk == "0" && i =="4" ) || (chk == "0" && i =="2" ))
        {  
            //Fahad 7496 04/02/2010 if ,else -if checked implemente               
            if (CheckCallBack("rbtn_FollowUp_Yes","cal_CallBack") == false) 
                return false;
            else if (CheckCallBack("rbtn_cFollowUp_Yes","cal_cCallBack") == false) 
                return false;
            else if (CheckCallBack("rbtn_vFollowUp_Yes","cal_vCallBack") == false) 
                return false;
        }
	
        if(document.getElementById('ddlFindBy').value == '-1')
        {
            alert ("Please ask the caller how they found out about ABC Law Firm.");
            DisplayToggleFindBy(); 
            return false;			     
        } 
		
        if(document.getElementById('ddlFindBy').value == '0' && document.getElementById('txtOtherFindBy').value == "")
        {
            alert ("Please Specify the Other reason");	
            document.getElementById('txtOtherFindBy').focus();	   
            return false;			     
        }
			
        if ( i==4 && chk==1) { plzwait1();}
        if (i==5 && chk==0) { plzwait1();}
            //Waqas 6895 11/02/2009 show progress on update
        if ( i==4 && chk==0) {ShowProgressOnUpdate(); } //for update
        if ( i==2 && chk==0) {ShowProgressOnUpdate(); } //for update
		
		

        return true;	  
    }
    //Zeeshan Ahmed 3979 05/15/2008 Add Function For Civil And Criminal Cases. Validate Call Back Date
    function CheckCallBack(rbtnYes,calCallBack)
    {
        //	 var lastQDate = new Date(document.getElementById('hf_LastCallbackDate').value);
        if ( document.getElementById(rbtnYes) != null)
        {
            if(document.getElementById(rbtnYes).checked ==true)
            {
                if(document.getElementById(calCallBack).style.display !="none" )
                {
                    dayofweek=weekdayName( document.getElementById(calCallBack).value,true); 
                    if ( dayofweek=="Sun" || dayofweek=="Sat") //Afaq 7496 03/22/2010 Add check for saturday.
                    {
                        return AlertForValidation(document.getElementById(calCallBack), "Please select any working day.");
                    }
                }
            }
        }
    }
	
    function launchCheck(strdate) 
    {
        var today = new Date(); // today
        var date = new Date(strdate); // your launch date
        var oneday = 1000*60*60*24
		
        if (Math.ceil(today.getTime()-date.getTime())/oneday >= 1 ) 
            return false;
		
        return true;
    }
	
    // Noufil 6138 07/31/2009 Show hide Other Check box.
    function CheckLanguage()
    {
        //debugger;
        if (document.getElementById("ddl_language").value == "0")
        {
            document.getElementById("dd_OtherLanguage").style.display = "inline-block";
            document.getElementById("td_otherlanguageImage").style.display = "block";
            document.getElementById("trImmigration").style.display = "none";
            document.getElementById("rbtn_interestedinworkvisayes").disabled = true;
            document.getElementById("rbtn_interestedinworkvisano").disabled = true;
            document.getElementById("rbtn_interestedinworkvisayes").parentNode.disabled = true;
            document.getElementById("rbtn_interestedinworkvisano").parentNode.disabled = true;
            document.getElementById("rbtn_interestedinworkvisayes").checked = false;
            document.getElementById("rbtn_interestedinworkvisano").checked = false;
            document.getElementById("trImmigrationComments").style.display = "none";
            return false;	        
        }
            //Haris Ahmed 10381 08/30/2012 Show hide immigration check box
            //Farrukh 11180 07/02/2013 Display Immigration Question for Both "SPANISH" & "ENGLISH" clients
        else if (document.getElementById("hfCanBePotentialVisaClients").value == "1")

        {
            document.getElementById("dd_OtherLanguage").style.display = "none";
            document.getElementById("td_otherlanguageImage").style.display = "none";
            document.getElementById("trImmigration").style.display = "block";
            document.getElementById("rbtn_interestedinworkvisayes").disabled = false;
            document.getElementById("rbtn_interestedinworkvisano").disabled = false;
            document.getElementById("rbtn_interestedinworkvisayes").parentNode.disabled = false;
            document.getElementById("rbtn_interestedinworkvisano").parentNode.disabled = false;
            return false;	        
        }
        else
        {
            document.getElementById("dd_OtherLanguage").style.display = "none";
            document.getElementById("td_otherlanguageImage").style.display = "none";
            document.getElementById("trImmigration").style.display = "none";
            document.getElementById("rbtn_interestedinworkvisayes").disabled = true;
            document.getElementById("rbtn_interestedinworkvisano").disabled = true;
            document.getElementById("rbtn_interestedinworkvisayes").parentNode.disabled = true;
            document.getElementById("rbtn_interestedinworkvisano").parentNode.disabled = true;
            document.getElementById("rbtn_interestedinworkvisayes").checked = false;
            document.getElementById("rbtn_interestedinworkvisano").checked = false;
            document.getElementById("trImmigrationComments").style.display = "none";
            return false;
        }
    }

    function EnableDisabeEmail()
    {
        if (document.getElementById('chkEmailAddress').checked == true )
        {
          //  var chk1=ConfirmDialog("Are you sure the client doesn’t have an email address? This information can be used to send trial notification letters and other related documents to clients. For Processing It without email address, click 'OK’ or ‘CANCEL’ to type in the email address.");
            //$("#txtErrorMessage").text("Operation canceled by user!");
            //$("#errorAlert").modal();
           // var chk1 = confirm ("Are you sure the client doesn’t have an email address? This information can be used to send trial notification letters and other related documents to clients. For Processing It without email address, click 'OK’ or ‘CANCEL’ to type in the email address.");
           
            bootbox.confirm({
                title:"Confirmation Message",
                message: "Are you sure the client doesn’t have an email address? This information can be used to send trial notification letters and other related documents to clients. For Processing It without email address, click 'OK’ or ‘CANCEL’ to type in the email address.",
                buttons: {
                    confirm: {
                        label: 'Ok'
                    },
                    cancel: {
                        label: 'Cancel'
                    }
                },
                callback: function (result) {
                    if(result==true){
                        document.getElementById('txtEmailAddress').value = '';
                        document.getElementById('txtEmailAddress').disabled = true;
                        
                    }
                    else{
                        document.getElementById('chkEmailAddress').checked = false;
                    }
                    //console.log('This was logged in the callback: ' + result);
                }
            });
            
            //if(chk1==true)
            //{
            //    document.getElementById('txtEmailAddress').value = '';
            //    document.getElementById('txtEmailAddress').disabled = true;
            //    return false;
            //}
            //else 
            //{
            //    document.getElementById('chkEmailAddress').checked = false;
            //    return false;
            //}
        }
        else
        {
            document.getElementById('chkEmailAddress').checked = false;
            document.getElementById('txtEmailAddress').disabled = false;		        	        			
            return false;
		        
        }
		        
    }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        /* Waqas 6599 10/02/2009 styles used by the AutoComplete class */ /* Waqas 6666 10/26/2009  removed style of javascript based auto complete */

        A:link {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }

        A:Visited {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }

        textBoxWidth: {
            width: 1000px;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="checkAdmin()" class="pace-done">
    <%-- <div id="Disable" style="display: none; position: absolute; left: 1; top: 1; height: 1px;
        width: 15px; background-color: Silver; filter: alpha(opacity=50);">
    </div>--%>
    <form id="frmviolationfee" method="post" runat="server" onkeypress="KeyDownHandler();">
        <div class="page-container row-fluid container-fluid">
            
           <%-- <asp35:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                    <asp35:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/3.5.30729.1/MicrosoftAjax.js" />
                </Scripts>
                <Services>
                    <asp35:ServiceReference Path="~/AutoComplete.asmx" />
                </Services>
            </asp35:ScriptManager>--%>
            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
               <%-- <Scripts>
                 <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/3.5.30729.1/MicrosoftAjax.js" />
                </Scripts>--%>
            </aspnew:ScriptManager>
            <aspnew:UpdatePanel ID="upnl_updateall" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <div id="DivForEmail" runat="server" style="display: none; position: absolute; left: 0; top: 0; background-color: Silver; filter: alpha(opacity=50);">
                    </div>

                    <%-- <table id="TableMain" runat = "server" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tbody>
                    <tr>
                        <td colspan="5">--%>
                    <aspnew:UpdatePanel ID="UpdPnlActiveMenu" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnl" runat="server">
                                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <%-- <aspnew:AsyncPostBackTrigger ControlID="ContactLookUp$btnAssociate"></aspnew:AsyncPostBackTrigger>
                                    <aspnew:AsyncPostBackTrigger ControlID="ContactInfo1$btnDisAssociate"></aspnew:AsyncPostBackTrigger>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnCIDInCorrect" />--%>
                            <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                            <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                            <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                        </Triggers>
                    </aspnew:UpdatePanel>
                    <%--</td>
                    </tr>
                    <tr>
                        <td style="width: 780px" id="td_violationBox" runat ="server">--%>

                    <section id="main-content" class="violationFeeoldPage">
    <section class="wrapper main-wrapper row" style="">
        <div class="col-xs-12">
            <asp:Panel id="tr_msgsForCIDandCuase" runat="server" Visible="false">
                                
                                    <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" EnableViewState="true">
                                        <ContentTemplate>
                                               <div class="alert alert-danger alert-dismissable fade in">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <asp:Label ID="test" runat="server" ></asp:Label>
                                            <asp:Label ID="lbl_Message" runat="server" ></asp:Label>
                                                 </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click"></aspnew:AsyncPostBackTrigger>
                                            <aspnew:AsyncPostBackTrigger ControlID="btn_Calculate" EventName="Click"></aspnew:AsyncPostBackTrigger>
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="btnCIDInCorrect" EventName="click"></aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                                            <aspnew:AsyncPostBackTrigger ControlID="btn_Update" EventName="click"></aspnew:AsyncPostBackTrigger>
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="ContactLookUp$btnAssociate" EventName="click">
                                                    </aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                                        </Triggers>
                                    </aspnew:UpdatePanel>
                                
                            </asp:Panel>

             <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblClientType" runat="server" Visible="false">Traffic Prospect</asp:Label>
                 </div>
            <asp:Panel id="tr_InfoMessages" runat="server" Visible="false">

             <asp:Panel id="tr_lblbadnumber" runat="server" Visible="false">
                                              <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblbadnumber" runat="server" Visible="False" 
                                                    >Telephone Number Alert : Please update 
                                            client contact information.</asp:Label>
                                           </div>
                                        </asp:Panel>
                                        <asp:Panel id="tr_lblbademail" runat="server"  Visible="false">
                                             <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblbademail" runat="server" Visible="False"
                                                    >Bad Email</asp:Label>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblProblemClient" runat="server" Visible="false">
                                              <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblProblemClient" runat="server" 
                                                    Visible="False" >This is a problem client.</asp:Label>
                                            <//div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblReturnedCourtNotice" runat="server" Visible="false">
                                              <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblReturnedCourtNotice" runat="server" 
                                                    Visible="False" ></asp:Label>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblDoNotBond" runat="server" Visible="false">
                                             <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblDoNotBond" runat="server" 
                                                    Visible="False"></asp:Label>
                                           </div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblBondForfeiture" runat="server" Visible="false">
                                              <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblBondForfeiture" runat="server"
                                                    Visible="False" ></asp:Label>
                                           </div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblHmcLubMesg1" runat="server" Visible="false">
                                             <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblHmcLubMesg1" runat="server"
                                                    Visible="False" >Please inform the client that Attorney will attempt to reset client's judge trial.</asp:Label>
                                           </div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblHmcLubMesg2" runat="server" Visible="false">
                                             <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblHmcLubMesg2" runat="server"
                                                    Visible="False" >Client must appear on this date and time.</asp:Label>
                                           </div>
                                        </asp:Panel>

                                        <asp:Panel id="tr_lblInActiveCourt" runat="server" Visible="false">
                                              <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <asp:Label ID="lblInActiveCourt" runat="server"
                                                    Visible="False" >Inactive Court.</asp:Label>
                                           </div>

                                        </asp:Panel>
            </asp:Panel>
              <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table runat="server" style="display: none" id="tbl_CIDConfirmation" cellspacing="0"
                                                cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td style="height: 30px">
                                                           <div class="alert alert-danger alert-dismissable fade in">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        <asp:Label ID="lblCIDCorrectConfirmation" runat="server" 
                                                           >Please confirm that the following CID information is 
                                                        correct?</asp:Label>
                                                        <asp:Button ID="btnCIDCorrect" runat="server" CssClass="btn btn-primary" Text="Correct"
                                                            OnClick="btnCIDCorrect_Click"></asp:Button>
                                                        &nbsp;<asp:Button ID="btnCIDInCorrect" runat="server" CssClass="btn btn-primary" Text="Incorrect"
                                                            OnClick="btnCIDInCorrect_Click"></asp:Button>
                                                               </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="ContactLookUp$btnAssociate" EventName="click">
                                                    </aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                                            <%-- <aspnew:AsyncPostBackTrigger ControlID="ContactInfo1$btnDisAssociate" EventName="click">
                                                    </aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                                        </Triggers>
                                    </aspnew:UpdatePanel>
        </div>
    <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title"><asp:label ID="lblFormName" runat="server"></asp:label></h1><!-- PAGE HEADING TAG - END -->                            </div>

                            <div class="pull-right hidden-xs">
                   <%-- <ol class="breadcrumb">
                        <%--<li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="blo-categories.html">Matter</a>
                        </li>
                        <li class="active">
                            <strong>Matter</strong>
                        </li>
                    </ol>--%>
                              <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                
                                                <tr>
                                                    <td style="height: 35px">
                                                        &nbsp;
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>&nbsp;(
                                                        <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>),
                                                        <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink>
                                                        <asp:ImageButton ID="ibtn_SPN_Search" OnClick="ibtn_SPN_Search_Click" runat="server"
                                                            Visible="False" ToolTip="Save Case Information From JIMS Website" ImageUrl="~/Images/SPNSrh.gif">
                                                        </asp:ImageButton>
                                                        <asp:ImageButton ID="ibtn_ViolationDataInfo" runat="server" ToolTip="Violation Data Information"  style="height:14px"
                                                            ImageUrl="~/Images/note04.gif"></asp:ImageButton>
                                                    </td>
                                                    <td align="right">
                                                        <asp:CheckBox ID="chkbtn_read" runat="server" CssClass="checkbox-custom" Text="Read Notes" style="padding-top: 6px;display: block;"
                                                            CausesValidation="True"></asp:CheckBox>
                                                        <asp:CheckBox ID="chkbtn_Delete" runat="server" Visible="False" Width="83px" CssClass="checkbox-custom"
                                                            Text="Read Notes " OnCheckedChanged="chkbtn_read_CheckedChanged" Checked="True"
                                                            AutoPostBack="True"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                            </table>
                </div>
                                
        </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-12">
             <section class="box ">
            <header class="panel_header">
             
                <h2 class="title pull-left">Client Information</h2>
                 
               
                <div class="actions panel_actions remove-absolute pull-right">
                     <asp:Label ID="lblCaseType" runat="server" Visible="true" Text="Case Type"></asp:Label>
                                                            <asp:DropDownList ID="ddl_CaseType" runat="server" CssClass="form-control inline-textbox" DataMember="CaseTypeName"
                                                                DataValueField="CaseTypeId" DataTextField="CaseTypeName">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblManualClient" runat="server" Visible="false" Text="Manual Client"></asp:Label>
                                                            &nbsp;
                                                            <asp:Label ID="lbl_LetterID" runat="server" Visible="false" Text=""></asp:Label>
                                                            <asp:Label ID="lbl_ofirm" runat="server" CssClass="clssubhead"></asp:Label>
                                                            <asp:DropDownList ID="ddl_FirmAbbreviation" runat="server" Visible="False" CssClass="clsInputCombo">
                                                            </asp:DropDownList>
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <%--<a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>--%>
                    <%--<a class="box_close fa fa-times"></a>--%>
                </div>
                    
            </header>
        <div class="content-body">
            <div class="row">
                 <aspnew:UpdatePanel ID="uppnContactID" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">Last Name</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:TextBox ID="txt_LastName" runat="server"  CssClass="form-control"
                                                                    MaxLength="20"></asp:TextBox><asp:Label ID="lbltxt_Lastname" runat="server" CssClass="form-label"
                                                                        Style="display: none;"></asp:Label>
                                <%--<input type="text" value="" class="form-control">--%>
                            </div>
                        </div>
                                                    </div>


                                                    <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">First Name , MI </label>
                            <span class="desc"></span>
                            <div class="controls">
                                 <table cellspacing="1" cellpadding="0" width="100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td id="td_txtName" runat="server" align="left">
                                                                                <asp:TextBox ID="txt_FirstName" runat="server" Width="77%" CssClass="form-control inline-textbox"
                                                                                    MaxLength="20"></asp:TextBox>,
                                                                                <asp:TextBox ID="txt_MiddleName" runat="server" Width="18%" CssClass="form-control inline-textbox" 
                                                                                    MaxLength="1"></asp:TextBox>
                                                                            </td>
                                                                            <td id="td_lblName" width="100%" runat="server" align="left" style="display: none;">
                                                                                <asp:Label ID="lbltxt_FirstName" runat="server" CssClass="form-label" Text=" "></asp:Label>,
                                                                                <asp:Label ID="lbltxt_MiddleName" runat="server" Width="17px" CssClass="form-label" Text=" "></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                <%--<input type="text" value="" class="form-control">--%>
                            </div>
                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">DOB</label>
                            <span class="desc"></span>
                            <div class="controls">

                               <table width="100%" cellspacing="0" cellpadding="0"  border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td id="td_txtDate" width="100%" runat="server" align="left">
                                                                                <asp:TextBox ID="txt_mm" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                                                    align="left" Width="50px" CssClass="form-control inline-textbox" MaxLength="2"></asp:TextBox>
                                                                                <asp:Label
                                                                                        ID="Label2" runat="server" CssClass="form-label" align="left" Text="/"></asp:Label>
                                                                                <asp:TextBox
                                                                                            ID="txt_dd" onkeyup="return autoTab(this, 2, event)" runat="server" align="left"
                                                                                            Width="50px" CssClass="form-control inline-textbox" MaxLength="2"></asp:TextBox>
                                                                                <asp:Label
                                                                                                ID="Label5" runat="server" CssClass="form-label" align="left" Text="/"></asp:Label>
                                                                                <asp:TextBox
                                                                                                    ID="txt_yy" runat="server" Width="60px" CssClass="form-control inline-textbox" align="left"
                                                                                                    MaxLength="4"></asp:TextBox>
                                                                            </td>
                                                                            <td id="td_lbldate" runat="server" align="left" style="display: none;">
                                                                                <asp:Label ID="lbltxt_mm" runat="server" CssClass="form-label" Text=""></asp:Label><asp:Label
                                                                                    ID="Label3" runat="server" CssClass="form-label" Text="/"></asp:Label><asp:Label ID="lbltxt_dd"
                                                                                        runat="server" CssClass="form-label" Text=""></asp:Label><asp:Label ID="Label6" runat="server"
                                                                                            CssClass="form-label" Text="/"></asp:Label><asp:Label ID="lbltxt_yy" runat="server" CssClass="form-label"
                                                                                                Text=""></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                            </div>
                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group CIDLabel">
                            <label class="form-label">CID</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:LinkButton ID="lnkContactID" runat="server" OnClick="lnkContactID_Click" CssClass="form-label"/>
                                                                <asp:HiddenField ID="hdnContactID" runat="server" />
                                                                <table style="display: none" id="tblCIDProg" width="120" align="left">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="height: 13px" class="clssubhead" valign="middle" align="left">
                                                                                <img src="../Images/plzwait.gif" />
                                                                                Please Wait.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                            </div>
                        </div>
                                                    </div>

                                                    <%--<table cellspacing="1" cellpadding="0" width="100%">
                                                        <tr class="">
                                                            <td style="width: 100px; height: 13px" class="clssubhead" align="left">
                                                                Last Name :
                                                            </td>
                                                            <td style="width: 150px" class="FrmTD">
                                                                <asp:TextBox ID="txt_LastName" runat="server" Width="100px" CssClass="clsInputadministration"
                                                                    MaxLength="20"></asp:TextBox><asp:Label ID="lbltxt_Lastname" runat="server" CssClass="Label"
                                                                        Style="display: none;"></asp:Label>
                                                            </td>
                                                            <td style="width: 100px; height: 13px" class="clssubhead" align="left">
                                                                First Name , MI :
                                                            </td>
                                                            <td style="width: 200px" class="FrmTD" valign="middle">
                                                                <table class="clsLabel" cellspacing="1" cellpadding="0" width="100%" border="0">
                                                                    <tbody>
                                                                        <tr class="">
                                                                            <td id="td_txtName" width="150px" runat="server" align="left">
                                                                                <asp:TextBox ID="txt_FirstName" runat="server" Width="100px" CssClass="clsInputadministration"
                                                                                    MaxLength="20"></asp:TextBox>,
                                                                                <asp:TextBox ID="txt_MiddleName" runat="server" Width="20px" CssClass="clsInputadministration"
                                                                                    MaxLength="1"></asp:TextBox>
                                                                            </td>
                                                                            <td id="td_lblName" width="100%" runat="server" align="left" style="display: none;">
                                                                                <asp:Label ID="lbltxt_FirstName" runat="server" CssClass="Label" Text=" "></asp:Label>,
                                                                                <asp:Label ID="lbltxt_MiddleName" runat="server" Width="17px" CssClass="Label" Text=" "></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td style="width: 35px; height: 13px" class="clssubhead" align="left">
                                                                DOB :
                                                            </td>
                                                            <td style="width: 129px" class="FrmTD">
                                                                <table cellspacing="0" cellpadding="0" align="left" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td id="td_txtDate" width="100%" runat="server" align="left">
                                                                                <asp:TextBox ID="txt_mm" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                                                    align="left" Width="18px" CssClass="clsInputadministration" MaxLength="2"></asp:TextBox><asp:Label
                                                                                        ID="Label2" runat="server" CssClass="Label" align="left" Text="/"></asp:Label><asp:TextBox
                                                                                            ID="txt_dd" onkeyup="return autoTab(this, 2, event)" runat="server" align="left"
                                                                                            Width="18px" CssClass="clsInputadministration" MaxLength="2"></asp:TextBox><asp:Label
                                                                                                ID="Label5" runat="server" CssClass="Label" align="left" Text="/"></asp:Label><asp:TextBox
                                                                                                    ID="txt_yy" runat="server" Width="30px" CssClass="clsInputadministration" align="left"
                                                                                                    MaxLength="4"></asp:TextBox>
                                                                            </td>
                                                                            <td id="td_lbldate" runat="server" align="left" style="display: none;">
                                                                                <asp:Label ID="lbltxt_mm" runat="server" CssClass="Label" Text=""></asp:Label><asp:Label
                                                                                    ID="Label3" runat="server" CssClass="Label" Text="/"></asp:Label><asp:Label ID="lbltxt_dd"
                                                                                        runat="server" CssClass="Label" Text=""></asp:Label><asp:Label ID="Label6" runat="server"
                                                                                            CssClass="Label" Text="/"></asp:Label><asp:Label ID="lbltxt_yy" runat="server" CssClass="Label"
                                                                                                Text=""></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td style="width: 50px; height: 13px" class="clssubhead" align="left">
                                                                CID :
                                                            </td>
                                                            <td style="width: 120px" class="FrmTD">
                                                                <asp:LinkButton ID="lnkContactID" runat="server" OnClick="lnkContactID_Click" />
                                                                <asp:HiddenField ID="hdnContactID" runat="server" />
                                                                <table style="display: none" id="tblCIDProg" width="120" align="left">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="height: 13px" class="clssubhead" valign="middle" align="left">
                                                                                <img src="../Images/plzwait.gif" />
                                                                                Please Wait.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                </ContentTemplate>
                                                <Triggers>
                                                    
                                                    <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                                                    
                                                    <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                                                    <aspnew:AsyncPostBackTrigger ControlID="btnCIDCorrect" EventName="click"></aspnew:AsyncPostBackTrigger>
                                                    
                                                    <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                                                </Triggers>
                                            </aspnew:UpdatePanel>
            </div>
        </div>
        </section>
            <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Contact Information</h2>

                <div class="actions panel_actions pull-right">
                    
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <%--<a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>--%>
                    <%--<a class="box_close fa fa-times"></a>--%>
                </div>
            </header>
        <div class="content-body">
            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                            <label class="form-label">Contact Number</label>
                            <span class="desc"></span>
                            <div class="controls">
                                 <asp:TextBox ID="txt_CC11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                    Width="50px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>&nbsp;
                                                                    <asp:TextBox ID="txt_CC12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="50px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox>&nbsp;
                                                                    <asp:TextBox ID="txt_CC13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="50px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox>
                                                                &nbsp;&nbsp;x
                                                                    <asp:TextBox ID="txt_CC14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="65px" CssClass="form-control inline-textbox"></asp:TextBox>&nbsp;
                                                                    <asp:DropDownList ID="ddl_ContactType" runat="server" Width="140px" CssClass="form-control inline-textbox">
                                                                    </asp:DropDownList>

                               
                            </div>
                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                            <label class="form-label">Language </label>
                            <span class="desc"></span>
                            <div class="controls">
                                 
                                                            <asp:DropDownList ID="ddl_language" runat="server" CssClass="form-control inline-textbox" DataTextField="LanguageName"
                                                                DataValueField="LanguageName" onchange="return CheckLanguage();">
                                                            </asp:DropDownList>
                                                        
                                                            <asp:DropDownList ID="dd_OtherLanguage" runat="server" CssClass="form-control inline-textbox" style="display: none">
                                                            </asp:DropDownList>
                                                        
                               
                            </div>
                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <span class="desc"></span>
                            <div class="controls">

                                <asp:TextBox ID="txtEmailAddress" CssClass="form-control inline-textbox" Width="250px" runat="server"></asp:TextBox>
                                            
                                            

                            </div>
                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                           
                            <span class="desc"></span>
                            <div class="controls" style="margin-top: 25px;">
                                
                                                <asp:CheckBox ID="chkEmailAddress" runat="server" Text="Email Address Not Available" onclick="EnableDisabeEmail();" CssClass="checkbox-custom" />
                                           
                            </div>
                        </div>
                                                    </div>

                                                    
            </div>
        </div>
        </section>
            <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Matter Information</h2>

                <div class="actions panel_actions remove-absolute pull-right">
                      <aspnew:UpdatePanel ID="upnlViolationAmount" runat="server" RenderMode="Inline">
                                                        <ContentTemplate>
                                                            <asp:Button Style="cursor: pointer" ID="btnfineamount" Visible="false" runat="server"
                                                                Width="130px" CssClass="clssubhead" Text="Get HMC Fine Amount" BackColor="Transparent"
                                                                BorderStyle="None" OnClientClick="WaitForViolationAmount();" UseSubmitBehavior="false"></asp:Button>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click" />
                                                        </Triggers>
                                                    </aspnew:UpdatePanel>
                                                    <asp:LinkButton ID="lnkbtn_InsertViolation" OnClick="lnkbtn_InsertViolation_Click"
                                                        runat="server">Insert Violation</asp:LinkButton>
                                                    <asp:Label ID="lbl_NewHireBondClient"
                                                            CssClass="clssubhead" runat="server" Text="&nbsp;|&nbsp;"></asp:Label>
                                                    <asp:LinkButton
                                                                ID="lnkbtn_NewHireBondClient" runat="server" OnClick="lnkbtn_NewHireBondClient_Click">New Hire Bond Client</asp:LinkButton><asp:Label
                                                                    ID="lblEmailCaseSummary" CssClass="clssubhead" runat="server" Visible="false"
                                                                    Text="&nbsp;|&nbsp;"></asp:Label><asp:LinkButton ID="lnkEmailCaseSummary" runat="server"
                                                                        Visible="false">Email</asp:LinkButton>
                                                    <asp:Label ID="lbl_SendSMS" CssClass="clssubhead"
                                                                            runat="server" Visible="false" Text="&nbsp;|&nbsp;"></asp:Label>
                                                    <asp:LinkButton ID="lnk_SendSMS"
                                                                                runat="server" Visible="false" OnClientClick="return ShowSMSPopup();">Send SMS Alert

                                                    </asp:LinkButton>
                	<a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
        <div class="content-body">
            <div class="row">
            <div class="col-xs-12">
               
                     <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                              <div class="table-responsive" data-pattern="priority-columns">
                                                            <asp:DataGrid ID="dgViolationInfo" runat="server" CssClass="table table-small-font table-bordered table-striped"
                                                                Width="100%" AutoGenerateColumns="False"
                                                                AllowSorting="True">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Cause Number" SortExpression="causenumber">
                                                                        <HeaderStyle CssClass="form-label" Width="15%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_CauseNo" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Cause Number" SortExpression="causenumber">
                                                                        <HeaderStyle Width="15%" CssClass="form-label"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkBtn_CauseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Ticket Number" SortExpression="refcasenumber">
                                                                        <HeaderStyle Width="15%" CssClass="form-label"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTicketID" runat="server" CssClass="form-label" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>'>
                                                                            </asp:Label>
                                                                            <asp:LinkButton ID="lnkBtn_CaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
                                                                            </asp:LinkButton>
                                                                            <asp:Label ID="lbl_TicketNo" runat="server" Visible="false" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
                                                                            </asp:Label>
                                                                            <asp:Label ID="lbl_ulstatus" runat="server" CssClass="form-label" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ulstatus") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Matter Description" SortExpression="violationDescription">
                                                                        <HeaderStyle Width="20%" CssClass="form-label"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label4" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>'>
                                                                            </asp:Label>
                                                                            <asp:Label ID="lblvnpk" runat="server" CssClass="form-label" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>'>
                                                                            </asp:Label>
                                                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>' />
                                                                            <asp:HiddenField ID="hf_bondamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.Bondamount","{0:F2}") %>' />
                                                                            <asp:HiddenField ID="hf_fineamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.FineAmount","{0:F2}") %>' />
                                                                            <asp:HiddenField ID="hf_courtloc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.shortname") %>' />
                                                                            <asp:HiddenField ID="hf_priceplan" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' />
                                                                            <asp:HiddenField ID="hf_bondflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' />
                                                                            <asp:HiddenField ID="hf_violationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>' />
                                                                            <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>' />
                                                                            <asp:HiddenField ID="hf_courtviolationstatusid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtViolationStatusidmain") %>' />
                                                                            <asp:HiddenField ID="hf_categoryid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' />
                                                                            <asp:HiddenField ID="hf_ticketsviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>' />
                                                                            <asp:HiddenField ID="hf_courtid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' />
                                                                            <asp:HiddenField ID="hf_OscareCourt" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.OscarCourtDetail") %>' />
                                                                            <asp:HiddenField ID="hf_ticketNumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refcasenumber") %>' />
                                                                            <asp:HiddenField ID="hf_courtdate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:d}") %>' />
                                                                            <asp:HiddenField ID="hf_crm" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' />
                                                                            <asp:HiddenField ID="hf_ctime" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>' />
                                                                            <asp:HiddenField ID="hf_VDesc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>' />
                                                                            <asp:HiddenField ID="hf_courtstatus" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.VerCourtDesc") %>' />
                                                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ticketid_pk") %>' />
                                                                            <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.iscriminalcourt") %>' />
                                                                            <asp:HiddenField ID="hf_chargelevel" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.chargelevel") %>' />
                                                                            <asp:HiddenField ID="hf_cdi" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.cdi") %>' />
                                                                            <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.isFTAViolation") %>' />
                                                                            <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasFTAViolations") %>' />
                                                                            <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value='<%# Bind("coveringFirmId") %>' />
                                                                            <asp:HiddenField ID="hf_arrestdate" runat="server" Value='<%# Bind("arrestdate","{0:d}") %>' />
                                                                            <asp:HiddenField ID="hf_casetype" runat="server" Value='<%# Bind("casetypeid") %>' />
                                                                            <asp:HiddenField ID="hf_missedcourttype" runat="server" Value='<%# Bind("missedcourttype") %>' />
                                                                            <asp:HiddenField ID="hf_IsNewClient" runat="server" Value='<%# Bind("isNewClient") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Level" SortExpression="levelcode">
                                                                        <HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_level" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.LevelCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Crt" SortExpression="courtnumbermain">
                                                                        <HeaderStyle Width="5%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_room" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtNumberMain") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Crt Loc" SortExpression="shortname">
                                                                        <HeaderStyle Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                           
                                                                            <asp:LinkButton ID="lnkBtn_CrtLoc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>' />
                                                                            <asp:HiddenField ID="hfCourtAddress" runat="server" Value='<%#Bind("CourtAddress") %>' />
                                                                            <asp:HiddenField ID="hfClientAddress" runat="server" Value='<%#Bind("ClientAddress") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Status" SortExpression="vercourtdesc">
                                                                        <HeaderStyle Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.VerCourtDesc") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Crt Date" SortExpression="courtdatemain">
                                                                        <HeaderStyle Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_courtdate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:d}") %>'></asp:Label>
                                                                            <asp:LinkButton ID="lnkbtn_courtdate" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:d}") %>'></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Crt Time" SortExpression="courtdatemain">
                                                                        <HeaderStyle Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_time" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText=" Court Info" Visible="False">
                                                                        <HeaderStyle Width="25%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <table id="court" cellspacing="0" cellpadding="0" width="191" align="left">
                                                                                <tr>
                                                                                    <td id="vdate" align="left" style="height: 16px">
                                                                                        <asp:Label ID="lblVerified" runat="server" CssClass="form-label" Width="198px" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedDate") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:Label ID="lbl_VStatus" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedStatus") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:Label ID="lblAuto" runat="server" CssClass="form-label" Width="194px" Text='<%# DataBinder.Eval(Container, "DataItem.AutoDate") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:Label ID="lbl_AStatus" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.AutoStatus") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:Label ID="lblScan" runat="server" CssClass="form-label" Width="195px" Text='<%# DataBinder.Eval(Container, "DataItem.ScanDate") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:Label ID="lbl_SStatus" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.ScanStatus") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Bond" Visible="False" SortExpression="bondflag">
                                                                        <HeaderStyle Width="4%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_bond" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Price Plan" Visible="False" SortExpression="plandescription">
                                                                        <HeaderStyle Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_plan" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.PlanDescription") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Fine" SortExpression="fineamount">
                                                                        <HeaderStyle Width="5%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_fineamount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.FineAmount","{0:C2}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Bond" Visible="False" SortExpression="bondamount">
                                                                        <HeaderStyle Width="5%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_bondamount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Bondamount","{0:C2}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Arrest Date" Visible="True" ItemStyle-Width="20%"
                                                                        SortExpression="arrestdate">
                                                                        <HeaderStyle CssClass="clsaspcolumnheader" Width="12%"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblArrestDate" runat="server" CssClass="form-label" Text='<%# Bind("ArrestDate","{0:d}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="12%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Jail Time" Visible="False">
                                                                        <HeaderStyle Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_jailTime" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.JailTimePeriod") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Jail Type" Visible="False">
                                                                        <HeaderStyle Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_jailType" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TypeOfJail") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Max Fine" Visible="False">
                                                                        <HeaderStyle Width="10%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_maxFine" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.MaxFineAmount","{0:C2}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>

                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <div runat="server" id="div_status">
                                                                                <asp:Image ID="img_status" runat="server" ImageUrl="../Images/right.gif" />
                                                                                <asp:HiddenField ID="hf_Discrepency" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.Discrepency") %>' />
                                                                                <asp:HiddenField ID="hf_ArrestingAgency" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ArrestingAgencyName") %>' />
                                                                            </div>
                                                                            <asp:Label ID="lbl_cds" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateScan","{0:d}") %>'
                                                                                Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbl_cdm" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:d}") %>'
                                                                                Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbl_cda" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'
                                                                                Visible="false"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                                  </div>
                                                            <table style="display: none; width: 873px" id="tbl_plzwait1">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 100%; height: 20px" class="clssubhead" valign="middle" align="center">
                                                                            <img alt="Please wait" src="../Images/plzwait.gif" />
                                                                            Please wait While we update your Violation information.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style="display: none; width: 873px" id="tblSortProgress" runat="server">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 100%; height: 20px" class="clssubhead" valign="middle" align="center">
                                                                            <img alt="Please wait" src="../Images/plzwait.gif" />
                                                                            Please wait sorting is in progress.......
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style="display: none; width: 873px" id="tblViolationAmount">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 100%; height: 20px" class="clssubhead" valign="middle" align="center">
                                                                            <img alt="Please wait" src="../Images/plzwait.gif" />
                                                                            Please wait while system gets violation amount from the court website.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style="display: none">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label Style="display: none" ID="lbldetailcount" runat="server" CssClass="Label"></asp:Label>
                                                                            <asp:TextBox Style="display: none" ID="txtulstatus" runat="server" CssClass="clsInputadministration">0</asp:TextBox>
                                                                            <asp:HiddenField ID="hf_latefee" runat="server" Value="0"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hf_lastrow" runat="server"></asp:HiddenField>
                                                                            <asp:Label Style="display: none" ID="lblLockFlag" runat="server" CssClass="Label"></asp:Label>
                                                                            <asp:Label Style="display: none" ID="lblTFC" runat="server" CssClass="Label"></asp:Label>
                                                                            <asp:HiddenField ID="hfHasFTAViolations" runat="server" Value="0"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hfIsInsideCase" runat="server" Value="0"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hf_LastGerenalcommentsUpdatedate" runat="server" />
                                                                            <asp:HiddenField ID="hf_IsPreHire" runat="server"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hf_IsALROnly" runat="server"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hf_smsAlertToNisi" runat="server" />
                                                                            <asp:HiddenField ID="hf_ArrDayOf" runat="server" Value="0" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click"></aspnew:AsyncPostBackTrigger>
                                                            <aspnew:AsyncPostBackTrigger ControlID="btnfineamount" EventName="Click" />
                                                            <aspnew:AsyncPostBackTrigger ControlID="btnViolationAmount" EventName="Click" />
                                                            <aspnew:PostBackTrigger ControlID="btn_Lock"></aspnew:PostBackTrigger>
                                                        </Triggers>
                                                    </aspnew:UpdatePanel>
                   <aspnew:UpdatePanel ID="upnlFineAmount" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="pnlViolationAmount" runat="server" Width="325px" Style="display: none;">
                                                                <table cellpadding="0" cellspacing="0" border="1" style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid"
                                                                    width="300px">
                                                                    <tr>
                                                                        <td>
                                                                            <table class="clsLeftPaddingTable" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="background-image: url(../Images/subhead_bg.gif); height: 34px">
                                                                                            <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td class="clssubhead">Update Violation Amount
                                                                                                        </td>
                                                                                                        <td align="right">&nbsp;<asp:LinkButton ID="lnkbtnupdateviolation" runat="server">X</asp:LinkButton>&nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <asp:Label ID="lblViolationNotFound" runat="server" ForeColor="Red" Font-Bold="True"
                                                                                                CssClass="Label" Text="Violation's Fee Not Found"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" style="height: 25px;">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td align="Right" style="width: 60%;">
                                                                                                        <asp:Button ID="btnok" runat="server" Text="Ok" CssClass="clsbutton" Width="60px"
                                                                                                            OnClientClick="return closeViolationAmountPopup();" />
                                                                                                    </td>
                                                                                                    <td align="Right">
                                                                                                        <asp:Button ID="btnViolationAmount" OnClick="btnViolationAmount_Click1" runat="server"
                                                                                                            CssClass="clsbutton" Text="Update" OnClientClick="return ValidateViolationAmountPanel();"></asp:Button>
                                                                                                        &nbsp; &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <ajaxToolkit:ModalPopupExtender ID="MPEViolationAmount" runat="server" BackgroundCssClass="modalBackground"
                                                                CancelControlID="lnkbtnupdateviolation" PopupControlID="pnlViolationAmount" TargetControlID="btnModalPopup"
                                                                OkControlID="btnModalPopup">
                                                            </ajaxToolkit:ModalPopupExtender>
                                                            <asp:Button Style="display: none" ID="btnModalPopup" runat="server" Text="ShowPopup"></asp:Button>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <aspnew:AsyncPostBackTrigger ControlID="btnfineamount" EventName="Click" />
                                                        </Triggers>
                                                    </aspnew:UpdatePanel>
                   
            </div>


                                                    
            </div>
        </div>
        </section>
            <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Matter Questions</h2>

                <div class="actions panel_actions pull-right">
                      
                	<a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
        <div class="content-body">
            <div class="row">
            <aspnew:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div style="width: 100%; display: none" runat="server" id="tr_PreHireQuestion">
                                                                <table cellspacing="1" cellpadding="0" width="100%">
                                                                    <tbody>
                                                                        <tr style="height: 25px;border: 1px solid #ddd;">

                                                                            <td class="" style="" colspan="6" >&nbsp;Pre Hire Questions
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:HiddenField ID="hf_AppTicketViolationIDs" runat="server" />
                                                                                <asp:Panel Style="display: none; left: 400px; top: 1000px" ID="pnlEmailCase" runat="server"
                                                                                    Width="125px" Height="50px">
                                                                                    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 600px">
                                                                                        <tr>
                                                                                            <td background="../Images/subhead_bg.gif" valign="bottom">
                                                                                                <table border="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td class="clssubhead" style="height: 24px" align="left">
                                                                                                            <asp:Label ID="lbl_head" Text="Email Case Summary" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="right">&nbsp;<asp:LinkButton ID="lbtn_close" runat="server" OnClientClick="return closeEmailCasepopup();">X</asp:LinkButton>
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="height: 100%">
                                                                                                <table cellpadding="1" cellspacing="0" border="1" style="border-collapse: collapse"
                                                                                                    width="100%" class="clsLeftPaddingTable">
                                                                                                    <tr>
                                                                                                        <td class="clssubhead" style="width: 200px" align="left">&nbsp;Client Name :
                                                                                                        </td>
                                                                                                        <td style="height: 20px; width: 300px;" align="left">&nbsp;<asp:Label ID="lblClientEmailName" CssClass="Label" Width="150px" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="clssubhead" style="width: 30%" align="left">&nbsp;Attorney Name :
                                                                                                        </td>
                                                                                                        <td id="tr_DrpAttonerneyName" runat="server" style="height: 16px;" align="left">&nbsp;<asp:DropDownList ID="drpAttorneysOnEmails" CssClass="clsInputCombo" Width="200px"
                                                                                                            runat="server">
                                                                                                        </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td id="tr_lblAttorneyName" runat="server" style="display: none; height: 20px;" align="left">&nbsp;<asp:Label ID="lblEmailAttorneyName" CssClass="Label" Width="200px" runat="server"></asp:Label>
                                                                                                            <asp:HiddenField ID="hf_EmailAttorneyName" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="clssubhead" style="width: 200px" align="left">&nbsp;Due to be paid in full on :
                                                                                                        </td>
                                                                                                        <td style="height: 20px; width: 400px;" align="left">&nbsp;<asp:Label ID="lblPaidPaymentMessage" CssClass="Label" Width="400px" runat="server"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="clssubhead" style="width: 200px" align="left">&nbsp;Email Address :
                                                                                                        </td>
                                                                                                        <td style="height: 20px; width: 400px;" align="left">&nbsp;<asp:TextBox ID="txtAttorneyEmail" CssClass="clsInputadministration" Width="169px"
                                                                                                            Enabled="false" runat="server"></asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" class="clssubhead" style="height: 24px; width: 100%" align="left">&nbsp;Matter Information
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="width: 100%" align="left">
                                                                                                            <asp:Panel ID="pnlViolationEmailScrolPanel" runat="server" ScrollBars="Vertical">
                                                                                                                <asp:GridView ID="grdEmailCaseSummary" runat="server" AutoGenerateColumns="False"
                                                                                                                    CssClass="clsLeftPaddingTable" Style="width: 97%">
                                                                                                                    <Columns>
                                                                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-Width="5px">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="5%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <input id="rbtnViolationID" name="ViolationID" onclick="return AddTicketViolationIDs();"
                                                                                                                                    type="checkbox" value='<%# Eval("TicketsViolationID") %>' />
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText=" Violation Description ">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="40%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lbl_desc" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.violationDescription") %>'></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText=" Case# ">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="10%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lbl_CauseNumber" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CauseNumber") %>'></asp:Label>
                                                                                                                                <asp:Label ID="lbl_refcasenumber" runat="server" Visible="false" CssClass="Label"
                                                                                                                                    Text='<%# DataBinder.Eval(Container,"DataItem.refcasenumber") %>'></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText=" Court Loc ">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="12%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lbl_loc" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.ShortName") %>'></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText=" Crt No.">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="9%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lbl_CrtNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtNumber")   %>'></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText=" Crt Date ">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="12%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lbl_datetime" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:d}") %>'></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:TemplateField HeaderText=" Crt Time ">
                                                                                                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="12%" />
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lbl_time" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                </asp:GridView>
                                                                                                            </asp:Panel>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" class="clssubhead" style="height: 24px; width: 100%" align="left">&nbsp;Case Summary :
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="2" style="width: 100%;">
                                                                                                            <asp:Panel ID="pnlCaseSummary" runat="server" Width="100%" ScrollBars="Vertical"
                                                                                                                Height="42px">
                                                                                                                <asp:Label ID="lblCaseSummaryCommentsonEmails" CssClass="Label" Width="95%" runat="server"></asp:Label>
                                                                                                            </asp:Panel>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table id="imgShowEmailProgress" runat="server" style="display: none">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="height: 20px" class="clssubhead" valign="middle" align="left">
                                                                                                                            <img src="../Images/plzwait.gif" />
                                                                                                                            Sending Email ...
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td style="height: 30px" align="right" valign="middle">
                                                                                                            <asp:Button ID="btnEmail" runat="server" CssClass="clsbutton" Text="Email" Width="75px"
                                                                                                                OnClientClick="return CheckEmailSelection();" OnClick="btnEmail_Click"></asp:Button>
                                                                                                            <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" OnClientClick="return closeEmailCasepopup();"
                                                                                                                Text="Cancel" Width="75px"></asp:Button>&nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                            <asp:MultiView ID="mvFeeQuestion" runat="server" ActiveViewIndex="2">
                                                                <asp:View ID="vCriminal" runat="server">
                                                                    <table style="width: 100%;" id="cquestionsdetail" cellspacing="1" cellpadding="0" class="display table table-hover table-condensed dataTable no-footer"
                                                                        width="100%">
                                                                        <tbody>
                                                                            <tr class="" style="display: none;">
                                                                                <td style="width: 50%" class="">Does client require Bonds ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rdbtn_cBondYes" runat="server" CssClass="checkbox-custom" GroupName="Bond"
                                                                                        Text="Yes" /><asp:RadioButton ID="rdbtn_cBondNo" runat="server" CssClass="checkbox-custom"
                                                                                            GroupName="Bond" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="" style="display: none;">
                                                                                <td style="width: 50%" class="">Does client have a CDL ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rdbtn_cCDLYes" runat="server" CssClass="checkbox-custom" GroupName="CDL"
                                                                                        Text="Yes" /><asp:RadioButton ID="rdbtn_cCDLNo" runat="server" CssClass="checkbox-custom"
                                                                                            GroupName="CDL" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="" style="display: none;">
                                                                                <td style="width: 50%" class="">Was client involved in an accident ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rdbtn_cAccidentYes" runat="server" CssClass="checkbox-custom" GroupName="Accident"
                                                                                        Text="Yes" /><asp:RadioButton ID="rdbtn_cAccidentNo" runat="server" CssClass="checkbox-custom"
                                                                                            GroupName="Accident" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="" style="display: none;">
                                                                                <td style="width: 50%" class="">Is there a late fee ?
                                                                                </td>
                                                                                <td style="height: 25px" class="">
                                                                                    <asp:RadioButton ID="rbtn_clateyes" runat="server" CssClass="checkbox-custom" GroupName="latefee"
                                                                                        Text="Yes" /><asp:RadioButton ID="rbtn_clateno" runat="server" CssClass="checkbox-custom"
                                                                                            GroupName="latefee" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="tr_Speeding" runat="server" class="" style="display: none;">
                                                                                <td style="width: 50%" class="" height="20">Speeding detail(speeding violations only):
                                                                                </td>
                                                                                <td class="" height="20">
                                                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="width: 96px">
                                                                                                    <div style="display: none" id="ddlcSpeed" runat="server" ms_positioning="FlowLayout">
                                                                                                        <asp:DropDownList ID="ddl_cSpeeding" runat="server" CssClass="form-control inline-textbox" DataValueField="ID"
                                                                                                            DataTextField="Description">
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                    <div id="lblcSpeed" runat="server" ms_positioning="FlowLayout">
                                                                                                        <asp:Label ID="lblcSpeeding" runat="server" CssClass="" Width="89px">Speed</asp:Label>
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <img onclick="DisplayToggle('ddlcSpeed','lblcSpeed')" alt="" src="../Images/Rightarrow.gif" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="" height="20">Callback:
                                                                                </td>
                                                                                <td class="" height="20">
                                                                                    <table width="100%" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RadioButton ID="rbtn_cFollowUp_Yes" runat="server" CssClass="checkbox-custom" GroupName="FollowUp"
                                                                                                        onclick="return ShowCallbackDays('td_ccallback_days');" Text="Yes" /><asp:RadioButton
                                                                                                            ID="rbtn_cFollowUp_No" runat="server" CssClass="checkbox-custom" GroupName="FollowUp"
                                                                                                            onclick="return HideCallbackDays('td_ccallback_days');" Text="No" />
                                                                                                </td>
                                                                                                <td id="td_ccallback_days" width="75%" runat="server">
                                                                                                    <asp:Label ID="lblcdate" runat="server" CssClass="form-label" Text="Date: "></asp:Label>
                                                                                                    <picker:datepicker id="cal_cCallBack" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                                                                    <%--<ew:CalendarPopup ID="cal_cCallBack" runat="server" Font-Names="Tahoma" Width="71px"
                                                                                                        ImageUrl="../images/calendar.gif" ToolTip="Select Report Date Range" Font-Size="8pt"
                                                                                                        DisableTextboxEntry="False" UpperBoundDate="12/31/9999 23:59:00" ShowGoToToday="True"
                                                                                                        PadSingleDigits="True" Nullable="True" Culture="(Default)" ControlDisplay="TextBoxImage"
                                                                                                        CalendarLocation="Bottom" AllowArbitraryText="False">
                                                                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                        <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    </ew:CalendarPopup>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vCivil" runat="server">
                                                                    <table style="width: 100%;" id="vquestionsdetail" cellspacing="1" cellpadding="0" class="display table table-hover table-condensed dataTable no-footer"
                                                                        width="100%">
                                                                        <tbody>
                                                                            <tr class="" style="display: none;">
                                                                                <td style="width: 50%" class="">Is there a late fee ?
                                                                                </td>
                                                                                <td style="height: 25px; width: 50%" class="">&nbsp;<asp:RadioButton ID="rbtn_vlateyes" runat="server" CssClass="checkbox-custom" GroupName="latefee"
                                                                                    Text="Yes" /><asp:RadioButton ID="rbtn_vlateno" runat="server" CssClass="checkbox-custom"
                                                                                        GroupName="latefee" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="">Is a payment plan needed?
                                                                                </td>
                                                                                <td style="height: 25px; width: 100%" class="">&nbsp;<asp:RadioButton ID="rbtn_vplanyes" runat="server" CssClass="checkbox-custom" GroupName="plan"
                                                                                    Text="Yes" /><asp:RadioButton ID="rbtn_vplanno" runat="server" CssClass="checkbox-custom"
                                                                                        GroupName="plan" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="" height="20">Callback:
                                                                                </td>
                                                                                <td class="" height="20" align="left" style="width: 50%">
                                                                                    <table width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td width="35%">
                                                                                                    <asp:RadioButton ID="rbtn_vFollowUp_Yes" runat="server" CssClass="checkbox-custom" GroupName="FollowUp"
                                                                                                        onclick="return ShowCallbackDays('td_vcallback_days');" Text="Yes" /><asp:RadioButton
                                                                                                            ID="rbtn_vFollowUp_No" runat="server" CssClass="checkbox-custom" GroupName="FollowUp"
                                                                                                            onclick="return HideCallbackDays('td_vcallback_days');" Text="No" />
                                                                                                </td>
                                                                                                <td id="td_vcallback_days" width="65%" runat="server">
                                                                                                    <asp:Label ID="lblvdate" runat="server" CssClass="clsLabel" Text="Date: "></asp:Label>

                                                                                                    <picker:datepicker id="cal_vCallBack" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                                                                    <%--<ew:CalendarPopup ID="cal_vCallBack" runat="server" Font-Names="Tahoma" Width="71px"
                                                                                                        ImageUrl="../images/calendar.gif" ToolTip="Select Report Date Range" Font-Size="8pt"
                                                                                                        DisableTextboxEntry="False" UpperBoundDate="12/31/9999 23:59:00" ShowGoToToday="True"
                                                                                                        PadSingleDigits="True" Nullable="True" Culture="(Default)" ControlDisplay="TextBoxImage"
                                                                                                        CalendarLocation="Bottom" AllowArbitraryText="False">
                                                                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                        <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    </ew:CalendarPopup>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vtraffic" runat="server">
                                                                    <table style="width: 100%;" id="questionsdetail" cellspacing="1" cellpadding="0"
                                                                        width="100%" class="display table table-hover table-condensed dataTable no-footer">
                                                                        <tbody>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="">Does client require Bonds ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rdbtn_BondYes" runat="server" CssClass="checkbox-custom" Text="Yes"
                                                                                        GroupName="Bond"></asp:RadioButton><asp:RadioButton ID="rdbtn_BondNo" runat="server"
                                                                                            CssClass="checkbox-custom" Text="No" GroupName="Bond"></asp:RadioButton>
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="">Vehicle Type ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rBtnCommercialVehicle" runat="server" CssClass="checkbox-custom"
                                                                                        Text="Commercial Vehicle" GroupName="VehicleType"></asp:RadioButton>&nbsp;
                                                                                            <asp:RadioButton ID="rBtnMotorCycle" runat="server" CssClass="checkbox-custom" Text="Motor Cycle"
                                                                                                GroupName="VehicleType"></asp:RadioButton>&nbsp;
                                                                                            <asp:RadioButton ID="rBtnCar" runat="server" CssClass="checkbox-custom" Text="Car" GroupName="VehicleType"></asp:RadioButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="">Does client have a CDL ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rdbtn_CDLYes" runat="server" CssClass="checkbox-custom" Text="Yes"
                                                                                        GroupName="CDL"></asp:RadioButton><asp:RadioButton ID="rdbtn_CDLNo" runat="server"
                                                                                            CssClass="checkbox-custom" Text="No" GroupName="CDL"></asp:RadioButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="">Was client involved in an accident ?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rdbtn_AccidentYes" runat="server" CssClass="checkbox-custom" Text="Yes"
                                                                                        GroupName="Accident"></asp:RadioButton><asp:RadioButton ID="rdbtn_AccidentNo" runat="server"
                                                                                            CssClass="checkbox-custom" Text="No" GroupName="Accident"></asp:RadioButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="">Is there a late fee ?
                                                                                </td>
                                                                                <td style="height: 25px" class="">
                                                                                    <asp:RadioButton ID="rbtn_lateyes" runat="server" CssClass="checkbox-custom" Text="Yes"
                                                                                        GroupName="latefee"></asp:RadioButton><asp:RadioButton ID="rbtn_lateno" runat="server"
                                                                                            CssClass="checkbox-custom" Text="No" GroupName="latefee"></asp:RadioButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="tr_SpeedingViolations" runat="server" class="" style="display: none;">
                                                                                <td style="width: 50%" class="" height="20">Speeding detail(speeding violations only):
                                                                                </td>
                                                                                <td class="" height="20">
                                                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="width: 96px">
                                                                                                    <div style="display: none" id="ddlSpeed" runat="server" ms_positioning="FlowLayout">
                                                                                                        <asp:DropDownList ID="ddl_Speeding" runat="server" CssClass="form-control inline-textbox" DataValueField="ID"
                                                                                                            DataTextField="Description">
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                    <div id="lblSpeed" runat="server" ms_positioning="FlowLayout">
                                                                                                        <asp:Label ID="lblSpeeding" runat="server" CssClass="" Width="89px">Speed</asp:Label>
                                                                                                    </div>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <img onclick="DisplayToggle()" alt="" src="../Images/Rightarrow.gif" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trImmigration" runat="server" class="" style="display: none">
                                                                                <td style="width: 50%" class="">Interested in a Work Visa if you don't have a SSN?
                                                                                </td>
                                                                                <td style="height: 25px">
                                                                                    <asp:RadioButton ID="rbtn_interestedinworkvisayes" onclick="return ShowCallbackDays('trImmigrationComments');"
                                                                                        runat="server" CssClass="checkbox-custom" GroupName="interestedinworkvisa" Text="Yes" /><asp:RadioButton
                                                                                            ID="rbtn_interestedinworkvisano" onclick="return HideCallbackDays('trImmigrationComments');"
                                                                                            runat="server" CssClass="checkbox-custom" GroupName="interestedinworkvisa" Text="No" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trImmigrationComments" runat="server" class="" style="display: none">
                                                                                <td style="width: 50%" class="" valign="top">Immigration Comments
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtImmigrationComments" runat="server" CssClass="" Width="400px" TextMode="MultiLine"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="">
                                                                                <td style="width: 50%" class="" height="20">Callback:<asp:HiddenField ID="hfCanBePotentialVisaClients" Value="0" runat="server" />
                                                                                </td>
                                                                                <td class="" height="20">
                                                                                    <table width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td >
                                                                                                    <asp:RadioButton ID="rbtn_FollowUp_Yes" onclick="return ShowCallbackDays('td_callback_days');" 
                                                                                                        runat="server" CssClass="checkbox-custom" Text="Yes" GroupName="FollowUp"></asp:RadioButton>
                                                                                                    <asp:RadioButton ID ="rbtn_FollowUp_No" onclick="return HideCallbackDays('td_callback_days');"
                                                                                                            runat="server" CssClass="checkbox-customom" Text="No" GroupName="FollowUp"></asp:RadioButton>
                                                                                                </td>
                                                                                                <td id="td_callback_days"  runat="server">
                                                                                                    <asp:Label ID="label_123" runat="server" CssClass="clsLabel" Text="Date: "></asp:Label>
                                                                                                      <picker:datepicker id="cal_CallBack" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                                                                    
                                                                                                    <%--<ew:CalendarPopup ID="cal_CallBack" runat="server" Font-Names="Tahoma" Width="71px"
                                                                                                        ImageUrl="../images/calendar.gif" ToolTip="Select Report Date Range" Font-Size="8pt"
                                                                                                        DisableTextboxEntry="False" UpperBoundDate="12/31/9999 23:59:00" ShowGoToToday="True"
                                                                                                        PadSingleDigits="True" Nullable="True" Culture="(Default)" ControlDisplay="TextBoxImage"
                                                                                                        CalendarLocation="Bottom" AllowArbitraryText="False">
                                                                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <TextboxLabelStyle CssClass="clstextarea" />
                                                                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                                            ForeColor="Black" />
                                                                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                                                                    </ew:CalendarPopup>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                            <div id="tbBodyCriminalALR" runat="server" style="display: none">
                                                                <table style="width: 100%;" id="Table12" cellspacing="1" cellpadding="0" width="100%" class="display table table-hover table-condensed dataTable no-footer">
                                                                    <tbody>
                                                                        <tr class="">
                                                                            <td style="width: 50%" class="">Did client have a prior attorney ?
                                                                            </td>
                                                                            <td style="height: 25px">
                                                                                <table width="100%">
                                                                                    <tbody id="tbBodyCriminalALRQA" runat="server">
                                                                                        <tr align="left">
                                                                                            <td width="20%">
                                                                                                <asp:RadioButton ID="rbtnPriorAttorneyYes" runat="server" CssClass="checkbox-custom"
                                                                                                    GroupName="PriorYesNO" onclick="return ShowAttorneys('td_priorattorneytype');"
                                                                                                    Text="Yes"></asp:RadioButton>
                                                                                                <asp:RadioButton ID="rbtnPriorAttorneyNo" runat="server" CssClass="checkbox-custom" GroupName="PriorYesNO"
                                                                                                    onclick="return HideAttorneys('td_priorattorneytype');" Text="No"></asp:RadioButton>
                                                                                            </td>
                                                                                            <td id="td_priorattorneytype" style="display: none" runat="Server">
                                                                                                <asp:DropDownList ID="drpAtt" runat="server" CssClass="clsInputCombo" OnChange="return HidePriorAttorney('td_priorattorney');">
                                                                                                    <asp:ListItem Value="0">--------- Choose ---------</asp:ListItem>
                                                                                                    <asp:ListItem Value="1">Court Appointed Attorney</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">Hired Defense Attorney</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td id="td_priorattorney" style="display: none;" runat="Server">
                                                                                                <asp:Label runat="server" ID="Label1" CssClass="clsLabel" Text="Name: "></asp:Label>
                                                                                                <asp:TextBox ID="txtPriorAttorney" CssClass="clsInputadministration" Width="150px"
                                                                                                    runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="" runat="server" id="trDL">
                                                                            <td style="width: 50%" class="">Driver's License/State
                                                                            </td>
                                                                            <td style="height: 25px">
                                                                                <table width="100%">
                                                                                    <tbody id="Tbody1" runat="server">
                                                                                        <tr align="left">
                                                                                            <td width="20%">
                                                                                                <asp:RadioButton ID="RadioButton1" runat="server" CssClass="checkbox-custom" GroupName="DLYesNO"
                                                                                                    onclick="return ShowDL('tdDrivingLicense');" Text="Yes"></asp:RadioButton>
                                                                                                <asp:RadioButton ID="RadioButton2" runat="server" CssClass="checkbox-custom" GroupName="DLYesNO"
                                                                                                    onclick="return HideDL('tdDrivingLicense');" Text="No"></asp:RadioButton>
                                                                                            </td>
                                                                                            <td id="tdDrivingLicense" style="display: none" runat="Server">&nbsp;
                                                                                                    <asp:TextBox ID="txt_dlstr" runat="server"  CssClass="form-control inline-textbox"
                                                                                                        MaxLength="25"></asp:TextBox>&nbsp; /
                                                                                                    <asp:DropDownList ID="ddl_dlState" runat="server"  CssClass="form-control inline-textbox">
                                                                                                    </asp:DropDownList>
                                                                                            </td>
                                                                                            <td id="td2" style="display: none;" runat="Server">
                                                                                                <asp:Label runat="server" ID="Label7" CssClass="clsLabel" Text="Name: "></asp:Label>
                                                                                                <asp:TextBox ID="txtDl" CssClass="clsInputadministration" Width="150px" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="" runat="server" id="trSSN">
                                                                            <td style="width: 50%" class="">Are you a U.S. Citizen?
                                                                            </td>
                                                                            <td style="height: 25px">
                                                                                <table width="100%">
                                                                                    <tbody id="Tbody2" runat="server">
                                                                                        <tr align="left">
                                                                                            <td width="20%">
                                                                                                <asp:RadioButton ID="RadioButton3" runat="server" CssClass="checkbox-custom" GroupName="SSNYesNO"
                                                                                                    onclick="return ShowSSN('tdSSN');" Text="Yes"></asp:RadioButton>
                                                                                                <asp:RadioButton ID="RadioButton4" runat="server" CssClass="checkbox-custom" GroupName="SSNYesNO"
                                                                                                    onclick="return HideSSN('tdSSN');" Text="No"></asp:RadioButton>
                                                                                            </td>
                                                                                            <td class="clsLeftPaddingTable" style="display: none" id="tdSSN" runat="server">&nbsp;
                                                                                                    <asp:TextBox ID="txt_SSN" MaxLength="4" CssClass="clsInputadministration" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="clsLeftPaddingTable" style="display: none" runat="server" id="tdDDLSSN">
                                                                                                <asp:DropDownList ID="DDLSSNQuestion" runat="server" CssClass="clsInputCombo" AppendDataBoundItems="true"
                                                                                                    OnChange="return ShowHideOtherTextBox();">
                                                                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td class="clsLeftPaddingTable" style="display: none" runat="server" id="tdOther">&nbsp;
                                                                                                    <asp:TextBox ID="txt_SSNOther" MaxLength="50" CssClass="clsInputadministration" runat="server"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="">
                                                                            <td style="height: 16px" colspan="7">
                                                                                <div id="tr_ALRhearingRequired" runat="server" style="display: none">
                                                                                    <table style="width: 100%;" id="Table8" cellspacing="1" cellpadding="0" width="100%">
                                                                                        <tbody>
                                                                                            <tr class="clsLeftPaddingTable">
                                                                                                <td>
                                                                                                    <div class="clsLeftPaddingTable" style="display: block;">
                                                                                                        <table id="Table1" cellspacing="1" cellpadding="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td style="width: 50%;" class="">Does the client require an ALR Hearing?
                                                                                                                </td>
                                                                                                                
                                                                                                                <td style="height: 25px">
                                                                                                                    <asp:RadioButton ID="rBtn_ALRHearingRequiredYes" runat="server" CssClass="checkbox-custom"
                                                                                                                        GroupName="ALRYesNo" onclick="return ShowHideALRHearingRequired();" Text="Yes"></asp:RadioButton>
                                                                                                                    <asp:RadioButton ID="rBtn_ALRHearingRequiredNo" runat="server" CssClass="checkbox-custom"
                                                                                                                        GroupName="ALRYesNo" onclick="return ShowHideALRHearingRequired();" Text="No"></asp:RadioButton>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="" runat="server" id="tr_IntoxilyzerTaken" style="display: none">
                                                                            <td style="height: 16px" colspan="7">
                                                                                <div>
                                                                                    <table style="width: 100%;" id="Table61" cellspacing="1" cellpadding="0" width="100%">
                                                                                        <tr class="clsLeftPaddingTable">
                                                                                            <td class="">
                                                                                                <div style="display: block;" id="Table6">
                                                                                                    <table cellspacing="1" cellpadding="0" width="100%">
                                                                                                        <tr class="clsLeftPaddingTable">
                                                                                                            <td style="width: 50%;" class="">Intoxilyzer Taken :
                                                                                                            </td>
                                                                                                          
                                                                                                            <td style="height: 25px">
                                                                                                                <asp:DropDownList ID="drp_IntoxilyzerTaken" runat="server" CssClass="clsInputCombo">
                                                                                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                                                                    <asp:ListItem Value="3">Unknown</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <table width="100%" cellpadding="0" cellspacing="0" class="display table table-hover table-condensed dataTable no-footer">
                                                                <tr class="">
                                                                    <td style="width: 50%" class="" height="20">How did you hear about ABC Law Firm?
                                                                    </td>
                                                                    <td width="27%">
                                                                        <div disabled="true" id="ddlFindByDv" runat="server" ms_positioning="FlowLayout">
                                                                            <asp:DropDownList ID="ddlFindBy" runat="server" CssClass="form-control" DataTextField="FindBy"
                                                                                DataValueField="FindbyID" OnChange="return ShowHideOtherFindByTextBox();">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <img onclick="DisplayToggleFindBy()" alt="" src="../Images/Rightarrow.gif" />
                                                                    </td>
                                                                    <td id="tdOtherFindBy" style="display: none" runat="server">
                                                                        <asp:TextBox ID="txtOtherFindBy" runat="server" CssClass="form-control" ></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 100%;" id="Table13" cellspacing="1" cellpadding="0" width="100%" class="display table table-hover table-condensed dataTable no-footer">
                                                                <tbody id="tbBodyCaseSummary" runat="server" style="display: none">
                                                                    <tr class="">
                                                                        <td style="width: 40%" class="">Case Summary :
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="100%">
                                                                            <asp:TextBox ID="txtCaseSummaryComments" Width="900px" CssClass="clsInputadministration"
                                                                                runat="server" Height="42px" MaxLength="5000" TextMode="MultiLine"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="Click"></aspnew:AsyncPostBackTrigger>
                                                            <aspnew:AsyncPostBackTrigger ControlID="btn_add" EventName="Click"></aspnew:AsyncPostBackTrigger>
                                                            <aspnew:AsyncPostBackTrigger ControlID="btn_Calculate" EventName="Click"></aspnew:AsyncPostBackTrigger>
                                                        </Triggers>
                                                    </aspnew:UpdatePanel>
                                                    
            </div>
        </div>
        </section>
            <section class="box ">
            <header class="panel_header">
                
                <h2 class="title pull-left">Fee Calculation</h2>

                <div class="actions panel_actions remove-absolute pull-right">
                  <asp:LinkButton ID="lnkbtn_Calculation" runat="server" CausesValidation="False">View 
                                                            Calculation</asp:LinkButton>
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <%--<a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>--%>
                    <%--<a class="box_close fa fa-times"></a>--%>
                </div>
            </header>
        <div class="content-body">
            <div class="row">
                 <aspnew:UpdatePanel ID="pnl_feecalculation" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="table-responsive" data-pattern="priority-columns">
                                            <table style="width: 100%" id="FeeCalc" cellspacing="1" cellpadding="0" border="0" class="table table-small-font table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td style="display: none" id="td_SpProm" class="" valign="top"
                                                            align="center" colspan="10" height="20" runat="server" width="50%">
                                                            <asp:Label ID="lbl_SpProm" runat="server" CssClass="form-label" Font-Bold="true" Font-Size="Small"></asp:Label>
                                                            <asp:HiddenField ID="hf_SpProm" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="">
                                                        <td class="form-label">
                                                            <strong>Fee</strong>
                                                        </td>
                                                        <td class="form-label">
                                                            <strong>Adj 1</strong>
                                                        </td>
                                                        <td class="form-label">
                                                            <strong>Adj 2</strong>
                                                        </td>
                                                        <td class="form-label">
                                                            <strong>
                                                                <asp:Label ID="lbl_Cont" runat="server"></asp:Label></strong>
                                                        </td>
                                                        <td class="form-label" id="td_accident" runat="server" style="display: none">
                                                            <strong>Accident</strong>
                                                        </td>
                                                        <td class="form-label" id="td_cdl" runat="server" style="display: none">
                                                            <strong>CDL</strong>
                                                        </td>
                                                        <td class="form-label" id="td_dayOfArr" runat="server" style="display: none">
                                                            <strong>Day of Arr</strong>
                                                        </td>
                                                        <td class="form-label" id="td_speeding" runat="server" style="display: none">
                                                            <strong>Speeding</strong>
                                                        </td>
                                                        <!-- Rab Nawaz Khan 10330 -->
                                                        <td class="form-label">
                                                            <strong>Est. Fee</strong>
                                                        </td>
                                                        <td class="form-label">
                                                            <strong>Paid</strong>
                                                        </td>
                                                        <td class="form-label">
                                                            <strong>Est. Owes</strong>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="form-label">
                                                            <strong>Owes</strong>
                                                        </td>
                                                    </tr>
                                                    <tr class="">
                                                        <td class="Label">
                                                            <asp:Label ID="lbl_Fee" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:TextBox ID="txt_InitialAdjustment" runat="server" Visible="False" Width="63px"
                                                                CssClass="form-control" MaxLength="5"></asp:TextBox><asp:Label ID="lbl_InitialAdjustment"
                                                                    runat="server" CssClass="form-label inline-textbox"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:TextBox ID="txt_FinalAdjustment" runat="server" Width="64px" Height="22px" CssClass="form-control inline-textbox"
                                                                MaxLength="5"></asp:TextBox>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Label ID="lblCont" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="display: none; font-weight: bold" id="td_accidentValue" runat="server">
                                                            <asp:Label ID="lbl_Accident" Visible="true" Text="$50" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="display: none; font-weight: bold" id="td_cdlValue" runat="server">
                                                            <asp:Label ID="lbl_CDL" runat="server" Visible="true" Text="$50" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="display: none; font-weight: bold" id="td_dayOfArrValue" runat="server">
                                                            <asp:Label ID="lbl_DayOfArr" runat="server" Visible="true" Text="$50" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="display: none; font-weight: bold" id="td_speedingValue" runat="server">
                                                            <asp:Label ID="lbl_Speeding" runat="server" Visible="true" Text="$50" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Label ID="lbl_CalculatedFee" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Label ID="lbl_Paid" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Label ID="lbl_EstimatedOwes" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Button ID="btn_Calculate" OnClick="btn_Calculate_Click1" runat="server" CssClass="btn btn-primary"
                                                                Text="Calculate" style="height: 22px;padding-top: 2px !important;display: block;"></asp:Button>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Button ID="btn_Lock" runat="server" CssClass="btn btn-primary" Text="Lock" style="height: 22px;padding-top: 2px !important;display: block;"></asp:Button>
                                                        </td>
                                                        <td style="font-weight: bold">
                                                            <asp:Label ID="lbl_actualowes" runat="server" CssClass="form-label"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
</div>
                                            <table style="display: none; height: 60px" id="tbl_plzwait" class="clssubhead" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td class="clssubhead" valign="middle" align="center">
                                                            <img alt="Please wait" src="../Images/plzwait.gif" />
                                                            &nbsp; Please wait while we process your request.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <aspnew:AsyncPostBackTrigger ControlID="btn_Calculate" EventName="Click"></aspnew:AsyncPostBackTrigger>
                                            <aspnew:PostBackTrigger ControlID="btn_Lock"></aspnew:PostBackTrigger>
                                            <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="Click"></aspnew:AsyncPostBackTrigger>
                                        </Triggers>
                                    </aspnew:UpdatePanel>
            </div>
        </div>
        </section>
            <section class="box ">
            <header class="panel_header">
                
                <h2 class="title pull-left">General Comments</h2>

                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <%--<a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>--%>
                    <%--<a class="box_close fa fa-times"></a>--%>
                </div>
            </header>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                 <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Width="100%"></cc2:WCtl_Comments>
                    </div>
            </div>
        </div>
        </section>
            <section class="box ">
            
        <div class="content-body">
            <div class="row">
                 <asp:Button ID="btn_Update" runat="server" CssClass="btn btn-primary" Text="Update"></asp:Button>
                                    <asp:Button ID="btnNext" runat="server" CssClass="btn btn-primary" Text="Next "></asp:Button>
            </div>
        </div>
        </section>
            </div>

        </section>
                </section>

                    <table id="TableSub" runat="server" cellspacing="0" cellpadding="0" width="780" border="0" style="display: none;">
                        <tbody>
                            

                            <tr>
                                <td colspan="2" align="center">
                                    
                                </td>
                            </tr>
                           
                            <tr>
                                <td style="height: 11px" background="../../images/separator_repeat.gif" colspan="7"></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <%--<table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                
                                                <tr>
                                                    <td style="height: 35px">
                                                        &nbsp;
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>&nbsp;(
                                                        <asp:Label ID="lbl_CaseCount" runat="server"></asp:Label>),
                                                        <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink>
                                                        <asp:ImageButton ID="ibtn_SPN_Search" OnClick="ibtn_SPN_Search_Click" runat="server"
                                                            Visible="False" ToolTip="Save Case Information From JIMS Website" ImageUrl="~/Images/SPNSrh.gif">
                                                        </asp:ImageButton>
                                                        <asp:ImageButton ID="ibtn_ViolationDataInfo" runat="server" ToolTip="Violation Data Information"
                                                            ImageUrl="~/Images/note04.gif"></asp:ImageButton>
                                                    </td>
                                                    <td align="right">
                                                        <asp:CheckBox ID="chkbtn_read" runat="server" Width="85px" CssClass="clsLabel" Text="Read Notes"
                                                            CausesValidation="True"></asp:CheckBox>
                                                        <asp:CheckBox ID="chkbtn_Delete" runat="server" Visible="False" Width="83px" CssClass="clsLabel"
                                                            Text="Read Notes " OnCheckedChanged="chkbtn_read_CheckedChanged" Checked="True"
                                                            AutoPostBack="True"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                            </table>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%" id="read" colspan="2" runat="server">
                                    <uc2:ReadNotes ID="ReadNotes1" runat="server"></uc2:ReadNotes>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Label ID="lbl_readnotes" runat="server" Visible="false" ForeColor="Red" Font-Bold="True"
                                        Width="100%" CssClass="Label" Font-Size="X-Small"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" colspan="4" height="34">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="2">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" colspan="4" height="34">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="clssubhead">&nbsp;Contact Information
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 500%;">
                                    <table id="tblContactInfo" runat="server" cellpadding="0" cellspacing="1" width="116%">
                                        <tr class="clsLeftPaddingTable">
                                            <td colspan="6" style="width: 100px; height: 13px" class="clssubhead" align="left">Contact Number
                                            </td>
                                            <td style="width: 100px; height: 13px" class="clssubhead" align="left">&nbsp;Language
                                            </td>
                                            <td colspan="2" style="width: 100px; height: 13px" class="clssubhead" align="left">&nbsp;&nbsp;&nbsp;&nbsp;Email
                                            </td>
                                        </tr>
                                        <tr class="clsLeftPaddingTable">
                                            <td colspan="6" style="width: 300px;">
                                                <table id="tbl_primaryno" cellspacing="0" cellpadding="0" width="100%" runat="server">
                                                    <tbody>
                                                        <tr>
                                                            <td class="clssubhead" align="left">
                                                            
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <asp:LinkButton ID="lnkbtn_addphoneno" runat="server" Visible="False">Add Phone 
                                                        No</asp:LinkButton>
                                                
                                                <asp:Panel Style="display: none; left: 480px; top: 340px" ID="pnl_telnos" runat="server"
                                                    Width="125px" Height="50px">
                                                    <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; width: 368px; border-bottom: black thin solid"
                                                        id="tbl_telnos" class="clsLeftPaddingTable"
                                                        cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="background-image: url(../Images/subhead_bg.gif); height: 34px" class="clssubhead"
                                                                    valign="middle" background="../images/headbar_headerextend.gif" colspan="3">
                                                                    <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="clssubhead">Add Phone Numbers
                                                                                </td>
                                                                                <td align="right">&nbsp;<asp:LinkButton ID="lnkbtnpclose" runat="server" OnClientClick="return closetelpopup();">X</asp:LinkButton>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 80px; height: 20px" class="clssubhead">&nbsp;Primary No
                                                                </td>
                                                                <td style="width: 200px; height: 20px" class="clssubhead">&nbsp;Phone Number
                                                                </td>
                                                                <td style="width: 100px; height: 20px" class="clssubhead">Type
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 80px; height: 24px">
                                                                    <asp:RadioButton ID="rb1" runat="server" CssClass="Label" GroupName="phone"></asp:RadioButton>
                                                                </td>
                                                                <td style="width: 200px; height: 24px" class="clssubhead">&nbsp;<asp:TextBox ID="ptel11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                    Width="32px" CssClass="clsInputadministration" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="ptel12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="32px" CssClass="clsInputadministration" MaxLength="4"></asp:TextBox>
                                                                    <asp:TextBox ID="ptel13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="32px" CssClass="clsInputadministration" MaxLength="4"></asp:TextBox>
                                                                    X
                                                                                                        <asp:TextBox ID="ptel14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                                            Width="32px" CssClass="clsInputadministration"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px; height: 24px">&nbsp;<asp:DropDownList ID="ddl_ptype1" runat="server" Width="80px" CssClass="clsInputCombo">
                                                                </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 80px; height: 28px">
                                                                    <asp:RadioButton ID="rb2" runat="server" CssClass="Label" GroupName="phone"></asp:RadioButton>
                                                                </td>
                                                                <td style="width: 200px; height: 28px" class="clssubhead">&nbsp;<asp:TextBox ID="ptel21" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                    Width="32px" CssClass="clsInputadministration" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="ptel22" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="32px" CssClass="clsInputadministration" MaxLength="4"></asp:TextBox>
                                                                    <asp:TextBox ID="ptel23" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="32px" CssClass="clsInputadministration" MaxLength="4"></asp:TextBox>
                                                                    X
                                                                                                        <asp:TextBox ID="ptel24" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                                            Width="32px" CssClass="clsInputadministration"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px; height: 28px">&nbsp;<asp:DropDownList ID="ddl_ptype2" runat="server" Width="80px" CssClass="clsInputCombo">
                                                                </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 80px; height: 26px">
                                                                    <asp:RadioButton ID="rb3" runat="server" CssClass="Label" GroupName="phone"></asp:RadioButton>
                                                                </td>
                                                                <td style="width: 200px; height: 26px" class="clssubhead">&nbsp;<asp:TextBox ID="ptel31" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                    Width="32px" CssClass="clsInputadministration" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="ptel32" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="32px" CssClass="clsInputadministration" MaxLength="4"></asp:TextBox>
                                                                    <asp:TextBox ID="ptel33" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="32px" CssClass="clsInputadministration" MaxLength="4"></asp:TextBox>
                                                                    X
                                                                                                        <asp:TextBox ID="ptel34" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                                            Width="32px" CssClass="clsInputadministration"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 100px; height: 26px">&nbsp;<asp:DropDownList ID="ddl_ptype3" runat="server" Width="80px" CssClass="clsInputCombo">
                                                                </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 80px; height: 26px"></td>
                                                                <td style="width: 200px; height: 26px" class="clssubhead"></td>
                                                                <td style="width: 100px; height: 26px">&nbsp;<asp:Button ID="btn_add" OnClick="btn_add_Click" runat="server" Width="63px"
                                                                    CssClass="clsbutton" Text="Add" OnClientClick="return validatePhonePanel();"></asp:Button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </asp:Panel>
                                            </td>

                                            <td>
                                               
                                            </td>

                                           

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table id="TableVio" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            
                                            <tr>
                                               
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="100%" colspan="2">
                                    <table id="vioDetail" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td valign="top">
                                                
                                                </td>
                                            </tr>
                                            <tr id="tr_FeeQuestion" runat="server">
                                                <td style="height: 35px" class="clssubhead" background="../Images/subhead_bg.gif"
                                                    colspan="7">&nbsp;<asp:Label ID="lblFeeQuestion" Text="Matter Questions" CssClass="clsaspcolumnheader"
                                                        runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="tr_QuestionBar" runat="server" style="display: none">
                                                <td style="height: 11px" background="../../images/separator_repeat.gif" colspan="7"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 16px" colspan="7">
                                                    
                                                </td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                
                            </tr>
                            <tr>
                                <td valign="top" width="100%" colspan="6">
                                    
                                </td>
                            </tr>
                            <tr>
                                
                            </tr>
                           
                            <tr>
                                <td align="center" colspan="9">
                                    <table style="display: none;" id="tblProgressUpdatePage" runat="server" align="center">
                                        <tbody>
                                            <tr>
                                                <td style="height: 10px" class="clssubhead" valign="middle" align="center">
                                                    <img src="../Images/plzwait.gif" />
                                                    Please wait while we process your request...
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                           
                           
                            <tr>
                                <td colspan="2">
                                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 9px" class="" valign="top" align="right" colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr style="font-family: Tahoma">
                                <td colspan="5">&nbsp;
                                            <asp:GridView ID="gvViolationAmount" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                                CssClass="clsLeftPaddingTable" Width="325px">
                                                <Columns>
                                                    <asp:BoundField DataField="CauseNumber" HeaderText="Cause Number">
                                                        <ItemStyle CssClass="GridItemStyleBig" />
                                                        <HeaderStyle CssClass="ClsSubHead" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FineAmount" HeaderText="Fine Amount">
                                                        <ItemStyle CssClass="GridItemStyleBig" />
                                                        <HeaderStyle CssClass="ClsSubHead" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="cbcheckall" runat="server" CssClass="Label" onclick="CheckAll(this);" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbcausenumber" runat="server" CssClass="Label" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                </td>
                            </tr>
                            <%--Sabir Khan 5308 12/05/2008 set display is none--%>
                            <tr style="font-family: Tahoma; display: none">
                                <td style="visibility: hidden" colspan="2">
                                    <asp:Label ID="lblCurrentDate" runat="server" CssClass="Label"></asp:Label><asp:Label
                                        ID="lblsearchtype" runat="server" CssClass="Label"></asp:Label><asp:Label ID="lblisNew"
                                            runat="server" CssClass="Label">0</asp:Label><asp:DropDownList ID="ddl_CourtLocation"
                                                runat="server" CssClass="clsInputCombo">
                                            </asp:DropDownList>
                                    <asp:DropDownList ID="ddl_Date_Month" runat="server" Width="53px" CssClass="clsInputCombo">
                                        <asp:ListItem Value="--">--</asp:ListItem>
                                        <asp:ListItem Value="01">Jan</asp:ListItem>
                                        <asp:ListItem Value="02">Feb</asp:ListItem>
                                        <asp:ListItem Value="03">Mar</asp:ListItem>
                                        <asp:ListItem Value="04">Apr</asp:ListItem>
                                        <asp:ListItem Value="05">May</asp:ListItem>
                                        <asp:ListItem Value="06">Jun</asp:ListItem>
                                        <asp:ListItem Value="07">Jul</asp:ListItem>
                                        <asp:ListItem Value="08">Aug</asp:ListItem>
                                        <asp:ListItem Value="09">Sep</asp:ListItem>
                                        <asp:ListItem Value="10">Oct</asp:ListItem>
                                        <asp:ListItem Value="11">Nov</asp:ListItem>
                                        <asp:ListItem Value="12">Dec</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddl_Date_Day" runat="server" Width="39px" CssClass="clsInputCombo">
                                        <asp:ListItem Value="--">--</asp:ListItem>
                                        <asp:ListItem Value="01">01</asp:ListItem>
                                        <asp:ListItem Value="02">02</asp:ListItem>
                                        <asp:ListItem Value="03">03</asp:ListItem>
                                        <asp:ListItem Value="04">04</asp:ListItem>
                                        <asp:ListItem Value="05">05</asp:ListItem>
                                        <asp:ListItem Value="06">06</asp:ListItem>
                                        <asp:ListItem Value="07">07</asp:ListItem>
                                        <asp:ListItem Value="08">08</asp:ListItem>
                                        <asp:ListItem Value="09">09</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                        <asp:ListItem Value="13">13</asp:ListItem>
                                        <asp:ListItem Value="14">14</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="16">16</asp:ListItem>
                                        <asp:ListItem Value="17">17</asp:ListItem>
                                        <asp:ListItem Value="18">18</asp:ListItem>
                                        <asp:ListItem Value="19">19</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="21">21</asp:ListItem>
                                        <asp:ListItem Value="22">22</asp:ListItem>
                                        <asp:ListItem Value="23">23</asp:ListItem>
                                        <asp:ListItem Value="24">24</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="26">26</asp:ListItem>
                                        <asp:ListItem Value="27">27</asp:ListItem>
                                        <asp:ListItem Value="28">28</asp:ListItem>
                                        <asp:ListItem Value="29">29</asp:ListItem>
                                        <asp:ListItem Value="30">30</asp:ListItem>
                                        <asp:ListItem Value="31">31</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddl_Date_Year" runat="server" Width="56px" CssClass="clsInputCombo">
                                        <asp:ListItem Value="--">--</asp:ListItem>
                                        <asp:ListItem Value="2000">2000</asp:ListItem>
                                        <asp:ListItem Value="2001">2001</asp:ListItem>
                                        <asp:ListItem Value="2002">2002</asp:ListItem>
                                        <asp:ListItem Value="2003">2003</asp:ListItem>
                                        <asp:ListItem Value="2004">2004</asp:ListItem>
                                        <asp:ListItem Value="2005">2005</asp:ListItem>
                                        <asp:ListItem Value="2006">2006</asp:ListItem>
                                        <asp:ListItem Value="2007">2007</asp:ListItem>
                                        <asp:ListItem Value="2008">2008</asp:ListItem>
                                        <asp:ListItem Value="2009">2009</asp:ListItem>
                                        <asp:ListItem Value="2010">2010</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddl_Time" runat="server" Width="80px" CssClass="clsInputCombo">
                                        <asp:ListItem Value="--">--</asp:ListItem>
                                        <asp:ListItem Value="8:00AM">8:00AM</asp:ListItem>
                                        <asp:ListItem Value="8:15AM">8:15AM</asp:ListItem>
                                        <asp:ListItem Value="8:30AM">8:30AM</asp:ListItem>
                                        <asp:ListItem Value="8:45AM">8:45AM</asp:ListItem>
                                        <asp:ListItem Value="9:00AM">9:00AM</asp:ListItem>
                                        <asp:ListItem Value="9:15AM">9:15AM</asp:ListItem>
                                        <asp:ListItem Value="9:30AM">9:30AM</asp:ListItem>
                                        <asp:ListItem Value="9:45AM">9:45AM</asp:ListItem>
                                        <asp:ListItem Value="10:00AM">10:00AM</asp:ListItem>
                                        <asp:ListItem Value="10:15AM">10:15AM</asp:ListItem>
                                        <asp:ListItem Value="10:30AM">10:30AM</asp:ListItem>
                                        <asp:ListItem Value="10:45AM">10:45AM</asp:ListItem>
                                        <asp:ListItem Value="11:00AM">11:00AM</asp:ListItem>
                                        <asp:ListItem Value="11:15AM">11:15AM</asp:ListItem>
                                        <asp:ListItem Value="11:30AM">11:30AM</asp:ListItem>
                                        <asp:ListItem Value="11:45AM">11:45AM</asp:ListItem>
                                        <asp:ListItem Value="12:00PM">12:00PM</asp:ListItem>
                                        <asp:ListItem Value="12:15PM">12:15PM</asp:ListItem>
                                        <asp:ListItem Value="12:30PM">12:30PM</asp:ListItem>
                                        <asp:ListItem Value="12:45PM">12:45PM</asp:ListItem>
                                        <asp:ListItem Value="1:00PM">1:00PM</asp:ListItem>
                                        <asp:ListItem Value="1:15PM">1:15PM</asp:ListItem>
                                        <asp:ListItem Value="1:30PM">1:30PM</asp:ListItem>
                                        <asp:ListItem Value="1:45PM">1:45PM</asp:ListItem>
                                        <asp:ListItem Value="2:00PM">2:00PM</asp:ListItem>
                                        <asp:ListItem Value="2:15PM">2:15PM</asp:ListItem>
                                        <asp:ListItem Value="2:30PM">2:30PM</asp:ListItem>
                                        <asp:ListItem Value="2:45PM">2:45PM</asp:ListItem>
                                        <asp:ListItem Value="3:00PM">3:00PM</asp:ListItem>
                                        <asp:ListItem Value="3:15PM">3:15PM</asp:ListItem>
                                        <asp:ListItem Value="3:30PM">3:30PM</asp:ListItem>
                                        <asp:ListItem Value="3:45PM">3:45PM</asp:ListItem>
                                        <asp:ListItem Value="4:00PM">4:00PM</asp:ListItem>
                                        <asp:ListItem Value="4:15PM">4:15PM</asp:ListItem>
                                        <asp:ListItem Value="4:30PM">4:30PM</asp:ListItem>
                                        <asp:ListItem Value="4:45PM">4:45PM</asp:ListItem>
                                        <asp:ListItem Value="5:00PM">5:00PM</asp:ListItem>
                                        <asp:ListItem Value="5:15PM">5:15PM</asp:ListItem>
                                        <asp:ListItem Value="5:30PM">5:30PM</asp:ListItem>
                                        <asp:ListItem Value="5:45PM">5:45PM</asp:ListItem>
                                        <asp:ListItem Value="6:00PM">6:00PM</asp:ListItem>
                                        <asp:ListItem Value="6:15PM">6:15PM</asp:ListItem>
                                        <asp:ListItem Value="6:30PM">6:30PM</asp:ListItem>
                                        <asp:ListItem Value="6:45PM">6:45PM</asp:ListItem>
                                        <asp:ListItem Value="7:00PM">7:00PM</asp:ListItem>
                                        <asp:ListItem Value="7:15PM">7:15PM</asp:ListItem>
                                        <asp:ListItem Value="7:30PM">7:30PM</asp:ListItem>
                                        <asp:ListItem Value="7:45PM">7:45PM</asp:ListItem>
                                        <asp:ListItem Value="8:00PM">8:00PM</asp:ListItem>
                                        <asp:ListItem Value="8:15PM">8:15PM</asp:ListItem>
                                        <asp:ListItem Value="8:30PM">8:30PM</asp:ListItem>
                                        <asp:ListItem Value="8:45PM">8:45PM</asp:ListItem>
                                        <asp:ListItem Value="9:00PM">9:00PM</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="Txt_CourtNo" runat="server" Width="46px" CssClass="clsInputadministration"></asp:TextBox><asp:TextBox
                                        ID="txtTTM" runat="server" Width="0px" CssClass="clsInputadministration">0</asp:TextBox><asp:Label
                                            ID="lblAdmin" runat="server" CssClass="Label"></asp:Label>
                                    <asp:Label ID="lbl_ActiveFlag" runat="server">0</asp:Label>
                                    <asp:TextBox ID="txt_iadj" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblIsQuoteExist" runat="server">false</asp:Label>
                                    <asp:TextBox ID="txtSpeedVal" runat="server" Width="39px">0</asp:TextBox>
                                    <asp:HiddenField ID="hf_contacts" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hf_AutoCourtDate" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hf_isZip7707" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hf_smsmessage" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hf_statusCount" runat="server"></asp:HiddenField>
                                    <asp:Label ID="lblpriechange" runat="server" Visible="False" ForeColor="#C00000"
                                        Font-Bold="True" Width="250px" CssClass="Label"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:TextBox ID="txtTimeStamp" runat="server" Width="89px"></asp:TextBox>
                    <%--  </td>
                    </tr>
                    <tr>
                        <td style="visibility: hidden" valign="top" align="right" colspan="5">
                            
                        </td>
                    </tr>--%>
                    <%-- </tbody>
            </table>--%>
                    <uc4:UpdateViolationSRV ID="UpdateViolationSRV1" runat="server" GridField="hf_ticketviolationid"
                        DivID="tbl_plzwait1" GridID="dgViolationInfo" IsCaseDisposition="false" />
                </ContentTemplate>
                <Triggers>
                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="Click"></aspnew:AsyncPostBackTrigger>
                    <aspnew:AsyncPostBackTrigger ControlID="btn_add" EventName="Click"></aspnew:AsyncPostBackTrigger>
                    <aspnew:AsyncPostBackTrigger ControlID="btnEmail" EventName="Click"></aspnew:AsyncPostBackTrigger>
                </Triggers>
            </aspnew:UpdatePanel>
            <aspnew:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="Pnl1" runat="server">
                        <table id="tablepopup" style="border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid"
                            cellpadding="0"
                            cellspacing="0">
                            <tr>
                                <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                                    style="width: 353px">
                                    <table style="width: 407px" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td class="clssubhead" align="left" style="height: 16px">&nbsp;Alert Box
                                                </td>
                                                <td align="right" style="height: 16px">&nbsp;<asp:LinkButton ID="lnkbtncancelpopup" runat="server">X</asp:LinkButton>&nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr bgcolor="#eff4fb">
                                <td align="center">
                                    <table width="353px" cellspacing="1" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblmessageshow" runat="server" BackColor="#eff4fb" ForeColor="#3366CC"
                                                    Text="Please Enter General Comments Again because some one already update the comments"
                                                    Width="353px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="BtnTar" runat="server" Text="Yes" Style="display: none" />&nbsp;
                                        <asp:Button ID="btnokay" runat="server" Text="Ok" CssClass="clsbutton" Height="27px"
                                            Width="91px" OnClick="btnokay_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td bgcolor="#eff4fb"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="Modal_Message" runat="server" TargetControlID="BtnTar"
                        PopupControlID="Pnl1" BackgroundCssClass="modalBackground">
                    </ajaxToolkit:ModalPopupExtender>
                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender_sms" runat="server" TargetControlID="btnsms"
                        PopupControlID="pnl_sms" BackgroundCssClass="modalBackground" DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnl_sms" runat="server" Width="380px">
                        <table id="table7" style="border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid"
                            cellpadding="0"
                            cellspacing="0" width="100%">
                            <tr>
                                <td align="left">
                                    <sms:Sms runat="server" ID="smscontrl" ModalPopupIDtoHide="ModalPopupExtender_sms"
                                        IsPhoneNumberReadOnly="true" IsOtherNumberReadOnly="false" HistoryNoteMessage="Court setting information sent to client via SMS on "></sms:Sms>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Button runat="server" ID="btnsms" Text="More" Style="display: none;" />
                </ContentTemplate>
                <Triggers>
                    <aspnew:AsyncPostBackTrigger ControlID="btnokay" />
                </Triggers>
            </aspnew:UpdatePanel>
            <aspnew:UpdatePanel ID="upnlContact" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlViewContactInfo" runat="server" Width="340px">
                        <Contact:ContactInfo ID="ContactInfo1" runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PnlContactLookUp" runat="server" Width="340px">
                        <ContactID:LookUp ID="ContactLookUp" runat="server" />
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="MPContactID" runat="server" TargetControlID="btnContactID"
                        PopupControlID="pnlViewContactInfo" BackgroundCssClass="modalBackground" DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    <ajaxToolkit:ModalPopupExtender ID="MPContactLookUp" runat="server" TargetControlID="btnContactLookUp"
                        PopupControlID="PnlContactLookUp" BackgroundCssClass="modalBackground" DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Button runat="server" ID="btnContactID" Text="More" Style="display: none;" />
                    <asp:Button runat="server" ID="btnContactLookUp" Text="More" Style="display: none;" />
                    <asp:HiddenField ID="hf_ISCriminalCourtForALR" runat="server" />
                </ContentTemplate>
            </aspnew:UpdatePanel>
        </div>
    </form>

    <script language="javascript" type="text/ecmascript">

        //Waqas 6599 09/19/2009 Validation for occupation and employer
        //Farrukh 9925 11/28/2011 Remove Occupation and Employer
        //        function CheckUnemploy(th) {
        //            if (th.checked == true) {

        //                document.getElementById("txtOccupation").value = "";
        //                document.getElementById("txtEmployer").value = "";
        //                document.getElementById("txtOccupation").disabled = true;
        //                document.getElementById("tr_Employer").style.display = 'none';
        //            }
        //            else {
        //                document.getElementById("txtOccupation").disabled = false;
        //                document.getElementById("tr_Employer").style.display = 'block';
        //            }
        //        }
        function checkAdmin() {

            //Waqas 6599 09/30/2009 Suggestion control
            //Waqas 6666 10/26/2009 disabled style of javascript based auto complete 
            //createAutoCompleteOccupation();
            //createAutoCompleteEmployer();
            ShowCallBackDays('rbtn_FollowUp_Yes', 'td_callback_days');
            ShowCallBackDays('rbtn_cFollowUp_Yes', 'td_ccallback_days');
            ShowCallBackDays('rbtn_vFollowUp_Yes', 'td_vcallback_days');
            //Haris Ahmed 10381 08/30/2012 Show hide immigration comments
            ShowCallBackDays('rbtn_interestedinworkvisayes', 'trImmigrationComments');
        }

        function DisplayToggle() {
            DisplayToggleNew("ddlSpeed", "lblSpeed");
            DisplayToggleNew("ddlcSpeed", "lblcSpeed");
            DisplayToggleNew("ddlvSpeed", "lblvSpeed");
        }

        function displaySOLmesaage() {
            $("#txtErrorMessage").text("Please remove SOL flag first");
            $("#errorAlert").modal();
           // alert("Please remove SOL flag first");
        }

        //Zeeshan Ahmed 3979 05/15/2008 Modify Function For Civil And Criminal Case
        function ShowCallBackDays(followup, tdfollowupdays) {
            if (document.getElementById(followup) != null) {
                if (document.getElementById(followup).checked)
                    document.getElementById(tdfollowupdays).style.display = 'block';
                else
                    document.getElementById(tdfollowupdays).style.display = 'none';
            }
        }

        //Zeeshan Ahmed 3979 05/15/2008 Modify Function For Civil And Criminal Case
        function DisplayToggleNew(ddlSpeed, lblSpeed) {

            if (document.getElementById(ddlSpeed) != null) {
                var ddl = document.getElementById(ddlSpeed).style;
                var lbl = document.getElementById(lblSpeed).style;

                if (document.getElementById("txtSpeedVal").value == 0) {
                    ddl.display = 'block';
                    lbl.display = 'none';
                    document.getElementById("txtSpeedVal").value = 1;
                }
                else {
                    ddl.display = 'none';
                    lbl.display = 'block';
                    document.getElementById("txtSpeedVal").value = 0;
                }
            }
        }
        
        function DisplayToggleFindBy() 
        {               
            DisplayToggleFindByNew("ddlFindByDv");            
        }
        
        function DisplayToggleFindByNew(ddlFindBy) 
        {
            if (document.getElementById(ddlFindBy) != null) 
            {
                var ddl = document.getElementById(ddlFindBy);                
                if(ddl.disabled == false)
                {
                    ddl.disabled = true;                                        
                }
                else
                {
                    ddl.disabled = false;                                        
                }
            }
        }
        DisplayToggle();
    </script>

    <script type="text/javascript">

        var postbackElement;
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);

        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
        }
        function pageLoaded(sender, args) {
            checkAdmin();
        }

        // Noufil 5884 07/13/2009 Show SMS control modal popup
        function ShowSMSPopup() {
            if (document.getElementById("hf_smsmessage").value.trim() != "0") {

                // Rab Nawaz 9961 01/03/2012 Should not send SMS Alerts to NISI cases
                if (document.getElementById('<%= hf_smsAlertToNisi.ClientID %>').value == "1") {
                    $("#txtErrorMessage").text("Sorry! we cannot sent SMS to NISI cases");
                    $("#errorAlert").modal();
                    // alert('Sorry! we cannot sent SMS to NISI cases');
                    return false;
                }
                document.getElementById("smscontrl_TxtMessage").value = document.getElementById("hf_smsmessage").value;
                document.getElementById("smscontrl_chk_SendSMS1").checked = false;
                document.getElementById("smscontrl_chk_SendSMS2").checked = false;
                document.getElementById("smscontrl_chk_SendSMS3").checked = false;
                document.getElementById("smscontrl_chk_Other").checked = false;
                document.getElementById("smscontrl_img_chk_Other").style.display = "none";
                document.getElementById("smscontrl_img_chk_SendSMS1").style.display = "none";
                document.getElementById("smscontrl_img_chk_SendSMS2").style.display = "none";
                document.getElementById("smscontrl_img_chk_SendSMS3").style.display = "none";
                document.getElementById("smscontrl_lbl_chk_Other").innerText = "";
                document.getElementById("smscontrl_lbl_chk_SendSMS1").innerText = "";
                document.getElementById("smscontrl_lbl_chk_SendSMS2").innerText = "";
                document.getElementById("smscontrl_lbl_chk_SendSMS3").innerText = "";
                document.getElementById("smscontrl_lbl_SmsError").innerText = "";
                var modalPopupBehavior = $find('ModalPopupExtender_sms');
                modalPopupBehavior.show();
            }
            else{
                $("#txtErrorMessage").text("Please set Court setting information of future and status must be in JURY, JUDGE, PRE-TRIAL or ALR Hearing to send SMS alert.");
                $("#errorAlert").modal();
               // alert("Please set Court setting information of future and status must be in JURY, JUDGE, PRE-TRIAL or ALR Hearing to send SMS alert.");
            }
            return false;
        }

        // Noufil 5884 07/13/2009 Hide SMS control modal popup
        function HidePopup() {
            var modalPopupBehavior = $find('ModalPopupExtender_sms');
            modalPopupBehavior.hide();
            return false;
        }
        
    </script>
    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

      <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
    <script type="text/javascript">
        
        function ConfirmDialog(message){
            var rtn=false;
            $('<div id="errorAlert" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"></div>'+
                '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>'+
                '<h4 class="modal-title">Confirm Message</h4></div><div class="modal-body" style="min-height: 93px !important;max-height: 162px;">'+
                '<p id="txtErrorMessage">'+message+'</p></div></div></div>').appendTo('body')
                            .html('<div><h6>'+message+'?</h6></div>')
                            .dialog({
                                modal: true, title: 'Confirm Message', zIndex: 10000, autoOpen: true,
                                width: 'auto', resizable: false,
                                buttons: {
                                    Yes: function () {
                                        // $(obj).removeAttr('onclick');                                
                                        // $(obj).parents('.Parent').remove();
																
                                        //$('body').append('<h1>Confirm Dialog Result: <i>Yes</i></h1>');
                                
                                        rtn= true;

                                        return rtn;
                                       // $(this).dialog("close");
                                    },
                                    No: function () {                           		                            
                                       // $('body').append('<h1>Confirm Dialog Result: <i>No</i></h1>');
                                        rtn= false;
                                        //$(this).dialog("close");
                                    }
                                },
                                close: function (event, ui) {
                                    $(this).remove();
                                }
                            });
            return rtn;
        };
             
    </script>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Popup_Options.aspx.cs" Inherits="lntechNew.ClientInfo.Popup_Options" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Select an Option</title>
    <base target="_self">
</head>
<body>

    <form id="form1" runat="server">
    <div>
        <table style="text-align:left">
            <tr>
                <td >
                    <asp:Label ID="lbl_Type" runat="server" Font-Bold="True" Font-Size="16pt"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    <asp:RadioButtonList ID="rbl_Options" runat="server">
                        <asp:ListItem Selected="True">Print</asp:ListItem>
                        <asp:ListItem>Batch Print</asp:ListItem>
                        <asp:ListItem>Email</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td >
                    <asp:Button ID="btn_OK" runat="server" Text="  Ok  " />&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" /></td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
    
    </div>
    </form>
    <script type="text/javascript">
    
        function Result(flag)
        {
        
        var ltype="<%= lbl_Type.Text %>";
        
        if(ltype == "Receipt")
        {
            if(flag==0)
                {
                    window.returnValue = 0;
                    window.close();
                }
                
             //if (flag == 1)
             else
                {
                    if(document.form1.rbl_Options_0.checked==true)
                        {
                            window.returnValue = 1;
                            window.close();
                        }
                    if(document.form1.rbl_Options_1.checked==true)
                        {
                            window.returnValue = 2;
                            window.close();
                        }
                    if(document.form1.rbl_Options_2.checked==true)
                        {
                            window.returnValue = 3;
                            window.close();
                        }
                }
             }
             
             if(ltype == "TrialNotification")
             {
                var printFlag = "<% = Request.QueryString["PrintFlag"] %>";
                var splitFlag = "<% = Request.QueryString["SplitFlag"] %>";
                var SplitContinue;
	            var PrintContinue;
             if(flag==0)
                {
                    window.returnValue = 0;
                    window.close();
                }
                
             else
                {
                 if(splitFlag==1)
	           {
	               SplitContinue = confirm("The Trial Notification Letter you are printing has a split court date, time or room number. Would you like to continue?");
	               if(SplitContinue==false)
	                {
	                    window.returnValue = 0;
	                    window.close();
	                    return;
                    }    
	           }
	           
               if(document.form1.rbl_Options_0.checked==true)
                        {
                            
                            window.returnValue = 1;
                            window.close();
                        }
                    if(document.form1.rbl_Options_1.checked==true)
                        {
                        if(printFlag==1)
	                    {
	                        PrintContinue = confirm("The case number already exists in batch print queue. Would you like to override the existing letter with this new letter?");
	                    }
	                    if(PrintContinue==false)
	                    {   
	                        window.returnValue = 2;
	                        window.close();
                        }	                        
	                    else
	                    {
                            window.returnValue = 3;
                            window.close();
                        }
                        }
                    if(document.form1.rbl_Options_2.checked==true)
                        {
                            window.returnValue = 4;
                            window.close();
                        }
                }
             
             
             }
        }
    
    </script>
</body>
</html>

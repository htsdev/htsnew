using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
namespace lntechNew.ClientInfo
{
    public partial class frmESignature : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string SessionedID = Session.SessionID + System.DateTime.Now;
                SessionedID = SessionedID.Replace("/", "");
                SessionedID = SessionedID.Replace(":", "");
                SessionedID = SessionedID.Replace(" ", "");
                //Response.Write(SessionedID);   
                string str = "<object id = 'ESign' height ='723' width ='889' classid ='ActiveXSign.dll#ActiveXSign.ESign' viewastext><param id=\"test\"  name =\"Opener\"  value=\"" + Request.QueryString["ticketid"] + "." + SessionedID + "\" /></object>";
                Page.FindControl("t1").Controls.Add(new LiteralControl(str));
            }
        }
    }
}

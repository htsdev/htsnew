using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.Components;
using HTP.Components.ClientInfo;
using System.Data.SqlClient;

namespace HTP.ClientInfo
{
    public partial class CourtDetail : System.Web.UI.Page
    {
        
        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        clsCourts courts = new clsCourts();
        clsGeneralMethods general = new clsGeneralMethods();
        CourtsFiles files = new CourtsFiles();
        clsCaseDetail cCase=new clsCaseDetail ();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Kazim 3533 4/1/2008 class is used for display court detail and it's google map. 
                
                //Kazim 3697 5/6/2008 Add More fields in court detail section also display the uploaded files against the selected court. 

                if (!IsPostBack)
                {
                    //Kazim 4071 05/19/2008 Restore previously added fields(courtname,courtaddress,phoneno)  
                    //Kazim 4071 05/21/2008 Set the clientaddress in hiddenfield.

                    DataSet dsclientinformation = cCase.GetCaseDetail(Convert.ToInt32(Request.QueryString["ticketid"]));

                    if (dsclientinformation != null && dsclientinformation.Tables.Count > 0 && dsclientinformation.Tables[0].Rows.Count > 0)
                    {
                        hfClientAddress.Value = dsclientinformation.Tables[0].Rows[0]["ClientAddress"].ToString();

                    }
                    
                    DataTable dt2 = courts.GetCourtDetails(Convert.ToInt32(Request.QueryString["courtid"]));

                    if (dt2.Rows.Count > 0)
                    {
                        lblCourtName.Text = dt2.Rows[0]["CourtName"].ToString().Trim();
                        lblCourtAddress.Text = dt2.Rows[0]["Address"].ToString().Trim();
                        lblCourtPhoneNo.Text = general.formatPhone(dt2.Rows[0]["Phone"].ToString().Trim());
                        
                    }
                    
                    DataSet ds;
                    courts.CourtID = Convert.ToInt32(Request.QueryString["courtid"]);
                    ds = courts.GetCourtInfo(courts.CourtID);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        lblCourtContact.Text = ds.Tables[0].Rows[0]["courtcontact"].ToString();
                        //Sabir Khan 5941 07/24/2009 Display county name in court detail...
                        lblcountyname.Text = ds.Tables[0].Rows[0]["Countyname"].ToString();
                        if (lblCourtContact.Text == "0")
                        {
                            lblCourtContact.Text = "N/A";
                        }

                        lblCaseIdentification .Text=ds.Tables [0].Rows [0]["CaseIdentifiaction"].ToString ();
                        if (lblCaseIdentification.Text == "0")
                        {
                            lblCaseIdentification.Text = "Ticket";
                        }
                        else if (lblCaseIdentification.Text == "1")
                        {
                            lblCaseIdentification.Text = "Cause Number";
                        }
                        else
                        {
                            lblCaseIdentification.Text = "N/A";
                        }

                        lblAcceptAppeals .Text=ds.Tables [0].Rows [0]["AcceptAppeals"].ToString ();
                        if (lblAcceptAppeals.Text == "0")
                        {
                            lblAcceptAppeals.Text = "No";
                        }
                        else if (lblAcceptAppeals.Text == "1")
                        {
                            lblAcceptAppeals.Text = "Yes";
                        }
                        else if (lblAcceptAppeals.Text == "2")
                        {
                            lblAcceptAppeals.Text = "Ticket & Cause Number";
                        }
                        else
                        {
                            lblAcceptAppeals.Text = "N/A";
                        }
                     
                        lblAppealLocation .Text=ds.Tables [0].Rows [0]["AppealLocation"].ToString ();
                        if (lblAppealLocation.Text == "")
                        {
                            lblAppealLocation.Text = "N/A";
                        }
                        
                        lblInitialSetting .Text=ds.Tables [0].Rows [0]["InitialSetting"].ToString ();
                                               
                        if (lblInitialSetting.Text == "0")
                        {
                            lblInitialSetting.Text = "Appearance";
                        }
                        else if (lblInitialSetting.Text == "1")
                        {
                            lblInitialSetting.Text = "PreTrial";
                        }
                        else if (lblInitialSetting.Text == "2")
                        {
                            lblInitialSetting.Text = "Jury Trial";
                        }
                        else
                        {
                            lblInitialSetting.Text = "N/A";
                        }

                        lblLOR .Text =ds.Tables [0].Rows [0]["LOR"].ToString ();
                        if (lblLOR.Text == "0")
                        {
                            lblLOR.Text = "Fax";
                        }
                        else if (lblLOR.Text == "1")
                        {
                            lblLOR.Text = "Certified Mail";
                        }
                        else if (lblLOR.Text == "2")
                        {
                            lblLOR.Text = "Hand Delievery";
                        }
                        else if (lblLOR.Text == "3")
                        {
                            lblLOR.Text = "Mail";
                        }
                        else
                        {
                            lblLOR.Text = "N/A";
                        }

                        lblAppearanceHire .Text=ds.Tables [0].Rows [0]["AppearnceHire"].ToString ();
                        if (lblAppearanceHire.Text == "")
                        {
                            lblAppearanceHire.Text = "N/A";
                        }
                        
                        lblJudgeTrialHire .Text=ds.Tables [0].Rows [0]["JudgeTrialHire"].ToString ();
                        if (lblJudgeTrialHire.Text == "")
                        {
                            lblJudgeTrialHire.Text = "N/A";
                        }

                        lblJuryTrialHire .Text=ds.Tables [0].Rows [0]["JuryTrialHire"].ToString () ;
                        if (lblJuryTrialHire.Text == "")
                        {
                            lblJuryTrialHire.Text = "N/A";
                        }
                        
                        lblBondType.Text=ds.Tables [0].Rows [0]["bondtype"].ToString ();
                        if (lblBondType.Text == "1")
                        {
                            lblBondType.Text = "Original";
                        }
                        else if (lblBondType.Text == "2")
                        {
                            lblBondType.Text = "Bonds";
                        }
                        else
                        {
                            lblBondType.Text = "N/A";
                        }
                        
                        lblHandwritingAllowed .Text=ds.Tables [0].Rows [0]["HandwritingAllowed"].ToString () ;
                        if (lblHandwritingAllowed.Text == "0")
                        {
                            lblHandwritingAllowed.Text = "No";
                        }
                        else if (lblHandwritingAllowed.Text == "1")
                        {
                            lblHandwritingAllowed.Text = "Yes";
                        }
                        else
                        {
                            lblHandwritingAllowed.Text = "N/A";
                        }

                        lblAdditinalFTAIssued .Text=ds.Tables [0].Rows [0]["AdditionalFTAIssued"].ToString ();
                        if (lblAdditinalFTAIssued.Text == "0")
                        {
                            lblAdditinalFTAIssued.Text = "No";
                        }
                        else if (lblAdditinalFTAIssued.Text == "1")
                        {
                            lblAdditinalFTAIssued.Text = "Yes";
                        }
                        else
                        {
                            lblAdditinalFTAIssued.Text = "N/A";
                        }

                        lblGracePeriod.Text =ds.Tables [0].Rows [0]["GracePeriod"].ToString ();
                        if (lblGracePeriod.Text == "0")
                        {
                            lblGracePeriod.Text = "No";
                        }
                        else if (lblGracePeriod.Text == "1")
                        {
                            lblGracePeriod.Text = "Yes";
                        }
                        else
                        {
                            lblGracePeriod.Text = "N/A";
                        }

                        lblContinuance .Text =ds.Tables [0].Rows [0]["Continuance"].ToString ();
                        if (lblContinuance.Text == "")
                        {
                            lblContinuance.Text = "N/A";
                        }

                        lblSupportingDocuments .Text=ds.Tables [0].Rows [0]["SupportingDocument"].ToString ();
                        if (lblSupportingDocuments.Text == "0")
                        {
                            lblSupportingDocuments.Text = "No";
                        }
                        else if (lblSupportingDocuments.Text == "1")
                        {
                            lblSupportingDocuments.Text = "Yes";
                        }
                        else
                        {
                            lblSupportingDocuments.Text = "N/A";
                        }

                        lblComments.Text = ds.Tables[0].Rows[0]["Comments"].ToString();
                        if (lblComments.Text == "")
                        {
                            lblComments.Text = "N/A";
                        }
                    }
                    files.CourtId = Convert.ToInt32(Request.QueryString["courtid"]);
                    DataTable dt = files.GetUploadedFiles();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        gvCourtFiles.DataSource = dt;
                        gvCourtFiles .DataBind ();
                    }
                 }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void gvCourtFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Footer))
                {
                    string lnk = "javascript: return OpenPopUpNew('../PaperLess/frmPreviewDocuments.aspx?fielid=" + ((HiddenField)e.Row.FindControl("hfFileId")).Value + "')";
                    ((ImageButton)(e.Row.FindControl("imgPreviewDoc"))).Attributes.Add("OnClick", lnk);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

       
    }
}

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using System.Configuration;
using System.IO;
using lntechNew.Components;
using HTP.WebComponents;
using HTP.Components;
using HTP.Components.ClientInfo;
using HTP.Components.Services;
using HTP.ClientController;
using HTP.WebControls;
using System.Web;

namespace HTP.ClientInfo
{

    public partial class GeneralInfo : MatterBasePage
    {

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_update1.Click += new System.EventHandler(this.btn_update1_Click);
            this.ddl_flags.SelectedIndexChanged += new System.EventHandler(this.ddl_flags_SelectedIndexChanged);
            this.DataL_flags.ItemCommand += new System.Web.UI.WebControls.DataListCommandEventHandler(this.DataL_flags_ItemCommand);
            this.btn_update2.Click += new System.EventHandler(this.btn_update2_Click);
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        #region Controls

        protected TextBox txtGI;
        protected TextBox txtAI;
        protected TextBox TextBox3;
        protected DropDownList ddl_flags;
        protected TextBox txt_fname;
        protected TextBox txt_mname;
        protected TextBox txt_lname;
        protected TextBox txt_dlstr;
        protected DropDownList ddl_gender;
        protected TextBox txt_race;
        protected TextBox txt_heightft;
        protected TextBox txt_heightin;
        protected TextBox txt_weight;
        protected DropDownList ddl_haircol;
        protected DropDownList ddl_eyecol;
        protected TextBox txt_add1;
        protected TextBox txt_add2;
        protected TextBox txt_city;
        protected DropDownList ddl_state;
        protected TextBox txt_zip;
        protected DropDownList ddl_contact1;
        protected TextBox txt_cc11;
        protected TextBox txt_cc12;
        protected TextBox txt_cc13;
        protected TextBox txt_cc14;
        protected DropDownList ddl_contact2;
        protected TextBox txt_cc21;
        protected TextBox txt_cc22;
        protected TextBox txt_cc23;
        protected TextBox txt_cc24;
        protected DropDownList ddl_contact3;
        protected TextBox txt_cc31;
        protected TextBox txt_cc32;
        protected TextBox txt_cc33;
        protected TextBox txt_cc34;
        protected TextBox txt_email;
        protected TextBox txt_generalcomments;
        protected TextBox txt_settingnotes;
        protected TextBox txt_trialcomments;
        protected Button btn_update1;
        protected Button btn_update2;
        protected Label lblMessage;
        protected TextBox txt_contactnotes;
        protected DropDownList ddl_dlState;
        protected DropDownList ddl_occupation;
        protected TextBox txt_occupation;
        protected DataList DataL_flags;
        protected HyperLink hlnk_MidNo;
        protected Label lbl_CaseCount;
        protected Label lbl_FirstName;
        protected Label lbl_LastName;
        protected Label lbl_bondflag;
        protected TextBox txt_dob_m;
        protected TextBox txt_dob_d;
        protected TextBox txt_dob_y;
        protected CheckBox chkb_addresscheck;
        protected Image img_cross;
        protected Image img_right;
        protected Button btn_next;
        protected TextBox txt_oldset;
        protected CheckBox chkEmail;

        #endregion

        #region Variables


        //Create an Instance of Data Access Class
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsFirms ClsFirms = new clsFirms();
        clsLogger clog = new clsLogger();
        clsCase ClsCase = new clsCase();
        clsCaseDetail ClsCaseDetail = new clsCaseDetail();
        ClsFlags clsFlags = new ClsFlags();
        clsReadNotes notes = new clsReadNotes();
        clsNOS nos = new clsNOS();
        clsBatch objBatch = new clsBatch();
        //Fahad 6766 03/15/2010 Implemented Configuration Service into POLM-Declared variable for using Configuration Service
        readonly ConfigurationKeys _objConfigurationKeys = new ConfigurationKeys();
        //Farrukh 9332 07/29/2011 RecID
        private int _recId;
        //private static int Recordid = 0;
        //Waqas 5771 04/16/2009
        clsContact ClsContact = new clsContact();

        //Address Verification
        ZP4Wrapper ProcessYDS = new ZP4Wrapper();

        bool flag;
        string barcode = string.Empty;
        string _dp2 = string.Empty;
        string _dpc = string.Empty;
        // Noufil 7937 07/06/2010 Variable comment to remove warning
        //string casenumber = "";
        bool Check;//Nasir 5381 01/17/2009 

        #endregion

        #region Events


        //Kazim 2729 3/27/2008
        //Replace ViewState["Ticketid"] with MatterBase class TicketId Property
        // Page Load Event
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                dvMessage.Visible = false;
                //Waqas 5771 04/16/2009 Checking for session
                int empid;
                if (cSession.IsValidSession(Request, Response, Session) == false ||
                        (!int.TryParse(cSession.GetCookie("sEmpID", Request), out empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (Request.QueryString["caseNumber"] != null && Request.QueryString["caseNumber"].ToString() == "0")
                        Response.Redirect("ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=0", false);
                    //Farrukh 9332 07/29/2011 Activities section updated after updation of Call back status
                    if (Session["ReminID"] != null)
                    {
                        if (Session["ReminID"].ToString() != string.Empty)
                        {
                            FillActivitesGrid();
                            Session["ReminID"] = null;
                        }
                    }

                    //Validating Form Controls
                    btn_update2.Attributes.Add("onclick", " return ValidateControls();");
                    btn_update1.Attributes.Add("onclick", " return ValidateControls();");
                    btn_next.Attributes.Add("onclick", " return ValidateControls();");
                    txt_dlstr.Attributes.Add("onclick", "SelectState()");

                    // Rab Nawaz Khan 01/13/2015 
                    //BindFlags();
                    BindFlagsNew();
                    //Sabir Khan 5009 11/05/2008 set time stamp for page refresh ...
                    txtTimeStamp.Text = DateTime.Now.TimeOfDay.ToString();

                    // Abid Ali 4846 11/10/2008. add event handler for page refresh
                    addPopUpComment_Trial.PageMethod += addPopUpComment_Trial_PageMethod;
                    addPopUpComment_Trial.PageClose += addPopUpComment_Trial_PageClose;


                    //Abbas Qamar 9796 adding page method for UpdateEmailAddress control to refresh the page
                    UpdateEmailAddress1.PageMethod += UpdateEmailAddress1_PageMethod;



                    //Waqas 5771 04/16/2009
                    //Waqas 5771 04/14/2009 Page method added
                    ContactInfo1.ContactPageMethod += delegate
                    {
                        dvMessage.Visible=false;lblMessage.Text = string.Empty;
                        GetClientInformation(ClsCase, TicketId);
                    };

                    ContactLookUp.ContactPageMethod += delegate
                    {
                        dvMessage.Visible= false; lblMessage.Text = string.Empty;
                        GetClientInformation(ClsCase, TicketId);
                    };



                    if (!IsPostBack)
                    {
                        
                        // setting folow up date control style.
                        pnlFlagsEvents.Style["display"] = "none";

                        if (Request.QueryString.Count >= 2)
                        {
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }

                        ViewState["ddl_FirmAbbreviation"] = "none";
                        ViewState["ddl_ContinuanceStatus"] = "none";
                        ViewState["ddl_ContinuanceDate"] = "none";
                        ViewState["vNTPATHScanTemp"] = ConfigurationManager.AppSettings["NTPATHScanTemp"];
                        ViewState["vNTPATHScanImage"] = ConfigurationManager.AppSettings["NTPATHScanImage"];
                        ViewState["empid"] = cSession.GetCookie("sEmpID", this.Request);

                        CheckReadNotes(this.TicketId);

                        string strServer;
                        strServer = "http://" + Request.ServerVariables["SERVER_NAME"];
                        txtSrv.Text = strServer;
                        //Session["objTwain"] = "<OBJECT id='VSTwain1' codeBase='" + strServer + "/VintaSoft.Twain.dll#version=1,6,1,1' height='1' width='1' classid='" + strServer + "/VintaSoft.Twain.dll#VintaSoft.Twain.VSTwain' VIEWASTEXT> </OBJECT>";
                        Session["objTwain"] = "<OBJECT id='OZTwain1' classid='" + strServer + "/OZTwain_1.dll#OZTwain.OZTwain' height='1' width='1' VIEWASTEXT> </OBJECT>";//for scanning function call in javascript.

                        // FillFirms();
                        // FillContinuanceSatus();

                        RefreshData();

                        //ClsCase.TicketID=TicketID;
                        ClsCase.TicketID = TicketId;
                        ClsCase.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", Request));
                        //	ClsCase.EmpID=3992;
                        dvMessage.Visible= false; lblMessage.Text = "";
                        txtsessionid.Text = Session.SessionID;
                        txtempid.Text = cSession.GetCookie("sEmpID", Request);
                        ViewState["empid"] = txtempid.Text;
                        lbtn_Scan.Attributes.Add("OnClick", "return StartScan();");

                        DataSet DsContactUsData = HttpContext.Current.Cache["DsContactUsData"] as DataSet;
                        if (DsContactUsData == null)
                        {
                            DsContactUsData = ClsDb.Get_DS_BySP("Usp_Htp_FillContactUsBasicData");
                            HttpContext.Current.Cache.Insert("DsContactUsData", DsContactUsData, null, DateTime.Now.AddDays(7), System.Web.Caching.Cache.NoSlidingExpiration);
                        }

                        FillPageData(DsContactUsData);
                        //DataSet dsState = ClsDb.Get_DS_BySP("USP_HTS_Get_State");
                        //DataSet dsDlstate = ClsDb.Get_DS_BySP("USP_HTS_Get_DL_State");

                        ////Bind DLState and State with DL/StateDropDown
                        //ddl_state.DataSource = dsState;
                        //ddl_state.DataTextField = dsState.Tables[0].Columns[1].ColumnName;
                        //ddl_state.DataValueField = dsState.Tables[0].Columns[0].ColumnName;
                        //ddl_state.DataBind();

                        //ddl_dlState.DataSource = dsDlstate;
                        //ddl_dlState.DataTextField = dsDlstate.Tables[0].Columns[1].ColumnName;
                        //ddl_dlState.DataValueField = dsDlstate.Tables[0].Columns[0].ColumnName;
                        //ddl_dlState.DataBind();

                        //Bind Contact Type with Contact DropDown
                        //clsContactType cCon = new clsContactType();
                        //DataSet dsContactTy1 = cCon.GetContactType();

                        //for (int i = 0; i < dsContactTy1.Tables[0].Rows.Count; i++)
                        //{
                        //    string svalue = dsContactTy1.Tables[0].Rows[i]["ID"].ToString().Trim();
                        //    string stext = dsContactTy1.Tables[0].Rows[i]["Description"].ToString().Trim();
                        //    ddl_contact1.Items.Add(stext);
                        //    ddl_contact1.Items[i + 1].Value = svalue;
                        //    ddl_contact2.Items.Add(stext);
                        //    ddl_contact2.Items[i + 1].Value = svalue;
                        //    ddl_contact3.Items.Add(stext);
                        //    ddl_contact3.Items[i + 1].Value = svalue;
                        //}
                        //	int TicketID=Convert.ToInt32(Request["casenumber"]);										

                        //commented by ozair																
                        //GetClientInformation(ClsCase,TicketID);	
                        GetClientInformation(ClsCase, TicketId, DsContactUsData);
                        if (hf_court.Value != "0")
                        {
                            tr_spn.Visible = true;
                            lbl_spn.Visible = true;
                            txt_spn.Visible = true;
                        }
                        else
                        {
                            tr_spn.Visible = false;
                            lbl_spn.Visible = false;
                            txt_spn.Visible = false;
                        }

                        //In order to redirect to frmMain when midnumber is clicked

                        //string casetype=cSession.GetCookie("sSearch",this.Request);
                        //ViewState["vSearch"]=Request.QueryString["search"];
                        string casetype = ViewState["vSearch"].ToString();

                        switch (casetype)
                        {
                            case "0":
                                hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                                break;
                            case "1":
                                hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                                break;
                        }
                        //Handling flag events
                        FillFlagList();
                        //Comment by Zeeshan Ahmed 
                        //clsFlags.FillServiceTicketcategories(ddl_category2);

                        // Noufil 6766 02/10/2009 Set Ticket id to POLM control
                        polmControl1.TicketId = TicketId;
                        polmControl1.EmpId = EmpId;
                        polmControl1.ModalPopupName = mpeShowPolm.ClientID;
                        polmControl1.IsPrimaryUser =
                            (cSession.GetCookie("sAccessType", Request) == "2");

                        // Rab Nawaz Khan 11473 10/30/2013 Commented the Polm controler and added the leads history. . .
                        //var polmController = new PolmController();
                        //var dtconsultation = polmController.GetAllPolmConsultationHistory(TicketId);
                        var leadsObj = new clsAddNewLead();
                        DataTable dtleadsHistory = leadsObj.GetLeadsAgainstClients(TicketId);

                        //Afaq 8069 07/24/2010 if consultation comment of case is yes than history section will shown else consultation will show.
                        //if (ViewState["hasConsulation"].ToString() != "2")
                        //{
                        if(ClsCase.IsLeadsDetailsSubmitted)
                        {
                            trConsultationHistory.Visible = trConsultationHistorHead.Visible = ClsCase.IsLeadsDetailsSubmitted;
                            if (dtleadsHistory != null && dtleadsHistory.Rows.Count > 0)
                            {
                                lbl_Norecord.Visible = false;
                                gvConsultationHistory.DataSource = dtleadsHistory;
                                gvConsultationHistory.DataBind();
                            }
                            else
                            {
                                lbl_Norecord.Visible = true;
                            }
                        }
                        //}
                        // Abbas Qamar 9676 11/18/2011 Check whether Bad Email flag is active for this client and Show popup email control if Method return true.
                        ShowPopupOnVerifyLink();
                        lnkbtn_VerifyEmail.Visible = CheckBadEmail(TicketId);
                        Boolean badEmailStatus = CheckBadEmail(TicketId);

                        if (badEmailStatus)
                        {
                            txt_email.Enabled = false;
                        }

                        //Farrukh 9332 07/28/2011 Activities section added under Flag section
                        FillActivitesGrid();
                    }

                    ActiveMenu am = (ActiveMenu)FindControl("ActiveMenu1");
                    TextBox txt1 = (TextBox)am.FindControl("txtid");
                    txt1.Text = TicketId.ToString();
                    TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                    txt2.Text = ViewState["vSearch"].ToString();
                SearchPage:
                    { }
                }
            }
            catch (Exception ePload)
            {
                clog.ErrorLog(ePload.Message, ePload.Source, ePload.TargetSite.ToString(), ePload.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ePload.Message;
            }

        }

        // Abid Ali 4846 11/10/2008 -- handle close option from trial pop up comments
        ///<summary>
        ///</summary>
        public void addPopUpComment_Trial_PageClose()
        {
            pnlFlagsEvents.Style["display"] = "none";
            modalPopupExtenderFlagsEvents.Hide();
        }

        // Abid Ali 4846 11/10/2008 -- handle refresh page from trial pop up comments
        ///<summary>
        ///</summary>
        public void addPopUpComment_Trial_PageMethod()
        {
            ////Fill Flag List
            //CheckReadNotes(this.TicketId);
            //RefreshData();
            //GetClientInformation(ClsCase, this.TicketId);
            //FillFlagList();
            //BindFlags();            

            ////Need To Be Refactor
            ////Fill Comments
            //InitializeCommentControls();
            ////pnlFlagsEvents.Style["display"] = "none"; 
            //modalPopupExtenderFlagsEvents.Hide();
            //ozair 5192 11/21/2008	
            Response.Redirect(Request.Url.AbsoluteUri, false);
        }

        private void BindFlagsNew()
        {
            DataTable dt_Flages = clsFlags.GetFlages(TicketId);
            bool allowHire = false;
            string msgNoHire = string.Empty;
            string msgAllowHire = string.Empty;
            if (dt_Flages != null && dt_Flages.Rows.Count > 0)
            {
                //clsFlags.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", Request));

                foreach (DataRow dtRow in dt_Flages.Rows)
                {
                    if (Convert.ToInt32(dtRow["FlagID"].ToString()) == Convert.ToInt32(FlagType.WrongNumber))
                        lblbadnumber.Visible = true;

                    else if (Convert.ToInt32(dtRow["FlagID"].ToString()) == Convert.ToInt32(FlagType.bademail))
                        lblbademail.Visible = true;

                    else if (Convert.ToInt32(dtRow["FlagID"].ToString()) == Convert.ToInt32(FlagType.ProblemClient_NoHire))
                    {
                        lblProblemClient.Visible = true;
                        msgNoHire = "This is a " + dtRow["Description"].ToString();
                    }
                    else if (Convert.ToInt32(dtRow["FlagID"].ToString()) == Convert.ToInt32(FlagType.ProblemClient_AllowHire))
                    {
                        allowHire = true;
                        msgAllowHire = "This is a " + dtRow["Description"].ToString();
                    }

                }

                tbl_messages.Style["display"] = lblbadnumber.Visible ? "block" : "none";
                tbl_messages.Style["display"] = lblbademail.Visible ? "block" : "none";
            }

            if (lblProblemClient.Visible)
            {
                lblProblemClient.Text = msgNoHire;
            }
            else
            {
                if (allowHire)
                {
                    lblProblemClient.Text = msgAllowHire;
                    lblProblemClient.Visible = true;
                }
                else
                {
                    lblProblemClient.Visible = false;
                }
            }
        
        }
        /*
        private void BindFlags()
        {



            // Noufil 3498 04/05/2008 activte or inactive bad number label
            clsFlags.TicketID = TicketId;
            clsFlags.FlagID = Convert.ToInt32(FlagType.WrongNumber);
            clsFlags.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", Request));

            lblbadnumber.Visible = (clsFlags.HasFlag() ? true : false);
            // Agha 12/05/2008 implementing Bad Email
            clsFlags.FlagID = Convert.ToInt32(FlagType.bademail);
            lblbademail.Visible = (clsFlags.HasFlag() ? true : false);

            // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
            tbl_messages.Style["display"] = lblbadnumber.Visible ? "block" : "none";
            tbl_messages.Style["display"] = lblbademail.Visible ? "block" : "none";



            // tahir 4777 09/10/2008 added Problem client (No hire) logic....
            //clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient);
            //lblProblemClient.Visible = (clsFlags.HasFlag() ? true : false);

            // Agha 4347 07/21/2008 Implementing Problem Client
            clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_NoHire);
            if (clsFlags.HasFlag())
            {
                lblProblemClient.Text = "This is a " + clsFlags.Description;
                lblProblemClient.Visible = true;
            }
            else
            {
                //ozair 4846 11/14/2008 renamed ProblemClient_Becareful to ProblemClient_AllowHire
                clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_AllowHire);
                if (clsFlags.HasFlag())
                {
                    lblProblemClient.Text = "This is a " + clsFlags.Description;
                    lblProblemClient.Visible = true;
                }
                else
                {
                    lblProblemClient.Visible = false;
                }
            }

            // end 4777
        }
        */
        //Initializing Comment controls
        //Nasir 6098 08/18/2009 remove InitializeCommentControls to remove comments from contact page 
        private void InitializeCommentControls()
        {

        }


        // Interact with the case class to get client information	
        private void GetClientInformation(clsCase cCase, int ticid, DataSet contactUsData = null)
        {
            try
            {
                //Hafiz 10288 07/17/2012 for ALR info from Database
                GetALRInfo(ticid, contactUsData);
                //End 10288
                //Getting Client information by TicketID	
                if (cCase.GetClientInfo(ticid))
                {

                    // Abbas Qamar 10093 03-28-2012 hiding the Phone number lable if it exist in non client database.
                    // Rab Nawaz Khan 10195 04/20/2012 Contact from Court field Removed . . . 
                    //string[] key1 = { "@RecordID", "@CaseType" };
                    //object[] value1 = { cCase.Record_Id, cCase.CaseType };

                    //DataTable dtPhoneNumbers = ClsDb.Get_DT_BySPArr("USP_HTP_Get_AllPhoneNumber", key1, value1);

                    //if (dtPhoneNumbers.Rows.Count > 0)
                    //{
                    //    LblContactCourt.Text = "";
                    //    foreach (DataRow dr in dtPhoneNumbers.Rows)
                    //    {
                    //        if (Convert.ToString(dr["PhoneNumber"]) != "" && Convert.ToString(dr["NotinFamily"]) == "1")
                    //        {
                    //            LblContactCourt.Text += "Home : " + Convert.ToString(dr["PhoneNumber"]);
                    //            LblContactCourt.Visible = true;
                    //            trPhoneNumbers.Visible = true;
                    //        }
                    //        else if (Convert.ToString(dr["PhoneNumber"]) != "" && Convert.ToString(dr["NotinFamily"]) == "0")
                    //        {
                    //            LblContactCourt.Text += "Other : " + Convert.ToString(dr["PhoneNumber"]);
                    //            LblContactCourt.Visible = true;
                    //            trPhoneNumbers.Visible = true;
                    //        }
                    //        if ((Convert.ToString(dr["WorkPhoneNumber"]) != "") && (Convert.ToString(dr["PhoneNumber"]) != ""))
                    //        {
                    //            LblContactCourt.Text += ", Work : " + Convert.ToString(dr["WorkPhoneNumber"]);
                    //            LblContactCourt.Visible = true;
                    //            trPhoneNumbers.Visible = true;
                    //        }
                    //        else if ((Convert.ToString(dr["WorkPhoneNumber"]) != "") && (Convert.ToString(dr["PhoneNumber"]) == ""))
                    //        {
                    //            LblContactCourt.Text += " Work : " + Convert.ToString(dr["WorkPhoneNumber"]);
                    //            LblContactCourt.Visible = true;
                    //            trPhoneNumbers.Visible = true;
                    //        }
                    //        if ((Convert.ToString(dr["WorkPhoneNumber"]) == "") && (Convert.ToString(dr["PhoneNumber"]) == ""))
                    //        {
                    //            LblContactCourt.Visible = false;
                    //            trPhoneNumbers.Visible = false;
                    //        }

                    //    }
                    //    txt_cc11.Text = txt_cc12.Text = txt_cc13.Text = txt_cc14.Text = "";
                    //    txt_cc21.Text = txt_cc22.Text = txt_cc23.Text = txt_cc24.Text = "";
                    //}
                    //else
                    //{
                    //    LblContactCourt.Visible = false;
                    //    trPhoneNumbers.Visible = false;

                    //}


                    //Rab Nawaz Khan 01/19/2015 commented the code below to save the DB round trip. . .
                    if (Session["caseData"] != null)
                        Session["caseData"] = null;
                    DataSet dsCaseDetials = ClsCaseDetail.GetCaseDetail(ticid);
                    hf_continuancedate.Value = GetFutureCourtDate(ticid, dsCaseDetials).ToShortDateString();
                    Session["caseData"] = dsCaseDetials;

                    Hf_courtstatus.Value = "0";
                    //noufil 4215  06/16/2008 Initializing the value of iscriminal court
                    ClsCase.TicketID = TicketId;
                    //Get Case Type
                    // RAb Nawaz Khan 01/20/2015 Momved the code to the hf_casetype.Value
                    hf_casetype.Value = ClsCase.CaseType.ToString(); // ClsCase.GetCaseType(TicketId).ToString();
                    //Sabir Khan 5260 11/27/2008 Checks for HCJP , HMC  , IDTicket ...
                    hf_isHCJPcase.Value = "0";
                    hf_isHMCcase.Value = "0";
                    hf_HasIDTicket.Value = "0";
                    //--------------------------------------
                    if (ClsCase.IsHCJPCourt(TicketId, 1))
                    {
                        hf_isHCJPcase.Value = "1";
                    }
                    if (ClsCase.IsInsideCourtCase(TicketId))
                    {
                        hf_isHMCcase.Value = "1";
                    }
                    if (ClsCaseDetail.HasIDTicket(TicketId, dsCaseDetials))
                    {
                        hf_HasIDTicket.Value = "1";
                    }
                    //--------------------------------------
                    //If Court Location Is HCJP & Status Is Waiting
                    if (hf_isHCJPcase.Value.Equals("1") && ClsCase.HasStatus(CourtViolationStatusType.WAIT))
                        Hf_courtstatus.Value = "1";
                    //If Inside Courts And Status Is Arraignment Waiting
                    else if (hf_isHMCcase.Value.Equals("1") && ClsCase.HasStatus(CourtViolationStatusType.ARRWAIT))
                        Hf_courtstatus.Value = "1";
                    else
                        Hf_courtstatus.Value = "0";

                    //Hf_courtstatus.Value = Session["courtstatus"].ToString();
                    //Getting values in to page controls
                    txt_fname.Text = cCase.FirstName.ToUpper();
                    txt_mname.Text = cCase.MiddleName.ToUpper();
                    txt_lname.Text = cCase.LastName.ToUpper();
                    //zeeshan
                    if (cCase.DOB != "" && cCase.DOB.Length == 10)
                    {
                        txt_dob_m.Text = cCase.DOB.Substring(0, 2);
                        txt_dob_d.Text = cCase.DOB.Substring(3, 2);
                        txt_dob_y.Text = cCase.DOB.Substring(6);
                    }

                    //Waqas 5771 04/16/2009
                    lbltxt_fname.Text = cCase.FirstName.ToUpper();
                    lbltxt_mname.Text = cCase.MiddleName.ToUpper();
                    lbltxt_lname.Text = cCase.LastName.ToUpper();

                    if (cCase.DOB != "" && cCase.DOB.Length == 10)
                    {
                        lbltxt_dob_m.Text = cCase.DOB.Substring(0, 2);
                        lbltxt_dob_d.Text = cCase.DOB.Substring(3, 2);
                        lbltxt_dob_y.Text = cCase.DOB.Substring(6);
                    }


                    //Waqas 5771 04/14/2009 check on contact id

                    if (cCase.Contact_ID == 0)
                    {
                        lnkContactID.Text = "CID Lookup";
                        hdnContactID.Value = string.Empty;
                        lnkContactID.Attributes.Add("OnClick", " return CheckPreReqForCID();");
                        EditClientInformation(true);
                    }
                    else
                    {
                        lnkContactID.Text = cCase.Contact_ID.ToString();
                        hdnContactID.Value = cCase.Contact_ID.ToString();
                        lnkContactID.Attributes.Add("OnClick", " return CheckPreReqForCID();");
                        EditClientInformation(false);
                    }

                    if (cCase.ActiveFlag == 0
                        && lnkContactID.Text.ToUpper() != "CID Lookup".ToUpper()
                        && cCase.IsContactIDConfirmedFlag == 0
                        && cCase.Record_Id > 0
                        //&& ClsCase.IsInsideCourtCase(TicketId))
                        && hf_isHMCcase.Value.Equals("1"))
                    {
                        tbl_CIDConfirmation.Style["display"] = "block";
                    }
                    else
                    {
                        tbl_CIDConfirmation.Style["display"] = "none";
                    }


                    txt_dlstr.Text = cCase.DLNumber;
                    //code added by Azee.
                    if (cCase.NoDL == 1)
                    {
                        chk_NoLicense.Checked = true;
                    }
                    else
                    {
                        chk_NoLicense.Checked = false;
                    }
                    ddl_dlState.SelectedValue = cCase.DLStateID.ToString();
                    ddl_gender.SelectedValue = cCase.Gender;
                    txt_race.Text = cCase.Race.ToUpper();
                    //ddl_occupation.SelectedValue=cCase.OccupationID.ToString();					
                    string height = cCase.Height;
                    if (height != "" && height.StartsWith("0") == false)
                    {
                        try
                        {
                            //Kamran 3880 04/29/08 remove spaces and null value bug                            
                            string h = "";
                            for (int i = 0; i < height.Length; i++)
                            {
                                if (height[i] != ' ')
                                    h += height[i];
                            }
                            height = h;
                            if (height != "")
                            {
                                if (height.Contains("ft") == true)
                                {
                                    txt_heightft.Text = height.Substring(0, height.IndexOf("ft")).Trim();
                                    txt_heightin.Text = height.Substring(height.IndexOf("ft") + 2).Trim();
                                }
                                else
                                    txt_heightft.Text = height.Trim();
                            }

                        }
                        catch (Exception ex)
                        {
                            clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        }
                    }
                    txt_weight.Text = cCase.Weight;

                    switch (cCase.IsPR)
                    {
                        case 0:
                            rbtn_PR_No.Checked = true;
                            break;

                        case 1:
                            rbtn_PR_Yes.Checked = true;
                            break;
                        case 3:
                            rbtn_PR_NA.Checked = true;
                            break;
                    }

                    string hcolor = cCase.HairColor;
                    if (hcolor != "")
                    {
                        ddl_haircol.SelectedValue = hcolor;
                    }
                    string ecolor = cCase.EyeColor;
                    if (ecolor != "")
                    {
                        ddl_eyecol.SelectedValue = ecolor;
                    }
                    //txt_ssn.Text=cCase.SSN;
                    //Hafiz 10288 07/12/2012
                    if (hf_casetype.Value != "2")
                        txt_SSN.Text = cCase.SSN;
                    txt_add1.Text = cCase.Address1.ToUpper();
                    txt_add2.Text = cCase.Address2.ToUpper();
                    txt_city.Text = cCase.City.ToUpper();
                    ddl_state.SelectedValue = cCase.StateID.ToString();
                    txt_zip.Text = cCase.Zip.ToUpper();
                    //When no address present
                    if (txt_add1.Text == "" && txt_city.Text == "" && txt_zip.Text == "" && ddl_state.SelectedValue == "0")
                    {
                        txt_city.Text = "HOUSTON";
                        ddl_state.SelectedValue = "45";
                    }
                    ddl_contact1.SelectedValue = cCase.ContactType1.ToString();
                    ddl_contact2.SelectedValue = cCase.ContactType2.ToString();
                    ddl_contact3.SelectedValue = cCase.ContactType3.ToString();
                    //Noufil 5000 10/22/2008 add viewstate to get consulatation flag
                    ViewState["hasConsulation"] = cCase.HasConsultationComments;

                    //if (cCase.HasConsultationComments == 1)
                    if(cCase.IsLeadsDetailsSubmitted)
                    {
                        //// Noufil 6766 02/10/2009 Hide consultation section.
                        tr_consultationHeader.Style[HtmlTextWriterStyle.Display] = "none";
                        tr_consultation.Style[HtmlTextWriterStyle.Display] = "none";
                        rbtn_FreeCon_Yes.Checked = true;
                        rbtn_FreeCon_No.Checked = false;
                        hf_lastConsultationComments.Value = "";
                        // Noufil 6766 COde comment because there is no need to consulation comments now                        
                        //td_free_con.Style[HtmlTextWriterStyle.Display] = "block";                        
                        //hf_lastConsultationComments.Value = Convert.ToString(WCC_ConsultationComments.Label_Text) + Convert.ToString(WCC_ConsultationComments.TextBox_Text);
                    }
                    //else if (cCase.HasConsultationComments == 0)
                    else
                    {
                        //// Noufil 6766 02/10/2009 show consultation section.
                        tr_consultationHeader.Style[HtmlTextWriterStyle.Display] = "";
                        tr_consultation.Style[HtmlTextWriterStyle.Display] = "";
                        rbtn_FreeCon_Yes.Checked = false;
                        rbtn_FreeCon_No.Checked = true;
                        // Noufil 6766 02/10/2009 Hide control.
                        rbtn_FreeCon_No.Visible = true;
                        // Noufil 6766 COde comment because there is no need to consulation comments now   
                        //td_free_con.Style[HtmlTextWriterStyle.Display] = "none";
                    }
                    // Noufil 6766 COde comment because there is no need to consulation comments now   
                    //else
                    //    td_free_con.Style[HtmlTextWriterStyle.Display] = "none";

                    //Formatting telephone numbers
                    string contact1 = cCase.Contact1;
                    if (contact1 != "" && contact1.Length >= 10)
                    {
                        txt_cc11.Text = contact1.Substring(0, 3);
                        txt_cc12.Text = contact1.Substring(3, 3);
                        txt_cc13.Text = contact1.Substring(6, 4);
                        txt_cc14.Text = contact1.Substring(10);
                    }
                    else
                    {
                        txt_cc11.Text = "";
                        txt_cc12.Text = "";
                        txt_cc13.Text = "";
                        txt_cc14.Text = "";
                    }
                    string contact2 = cCase.Contact2;
                    if (contact2 != "" && contact2.Length >= 10)
                    {
                        txt_cc21.Text = contact2.Substring(0, 3);
                        txt_cc22.Text = contact2.Substring(3, 3);
                        txt_cc23.Text = contact2.Substring(6, 4);
                        txt_cc24.Text = contact2.Substring(10);
                    }
                    else
                    {
                        txt_cc21.Text = "";
                        txt_cc22.Text = "";
                        txt_cc23.Text = "";
                        txt_cc24.Text = "";
                    }
                    string contact3 = cCase.Contact3;
                    if (contact3 != "" && contact3.Length >= 10)
                    {
                        txt_cc31.Text = contact3.Substring(0, 3);
                        txt_cc32.Text = contact3.Substring(3, 3);
                        txt_cc33.Text = contact3.Substring(6, 4);
                        txt_cc34.Text = contact3.Substring(10);
                    }
                    else
                    {
                        txt_cc31.Text = "";
                        txt_cc32.Text = "";
                        txt_cc33.Text = "";
                        txt_cc34.Text = "";
                    }


                    // Haris Ahmed 10446 10/02/2012 Enable code
                    //VerifyEmail.IsEmailVerified = VerifyEmail.emailVerified(cCase.Email);
                    //VerifyEmail.EmailAddress = cCase.Email;
                    //VerifyEmail.SetImage();
                    txt_email.Text = cCase.Email;



                    chkEmail.Checked = cCase.EmailNotAvailable;

                    //Nasir 6098 08/18/2009 remove box text  to remove comments from contact page 


                    //Nasir 6098 08/18/2009 remove box text conti and set call to remove comments from contact page 
                    //tb_continuance.Text = cCase.ContinuanceComments;
                    //txt_setcall.Text = cCase.SetcallComments;

                    hf_activeflag.Value = Convert.ToInt32(cCase.ActiveFlag).ToString();
                    hf_continuanceflag.Value = Convert.ToInt32(cCase.ContinuanceFlag).ToString();
                    //Nasir 5381 01/15/2009 set value in hidden field of LastGerenalcommentsUpdatedate
                    hf_LastGerenalcommentsUpdatedate.Value = cCase.LastGerenalcommentsUpdatedate.ToString();

                    InitializeCommentControls();


                    // added by tahir ahmed dt: 01/19/08 02:18 AM
                    // requried for task 2655
                    // Noufil 6766 COde comment because there is no need to consulation comments now   
                    //ViewState["ConsultationComments"] = Convert.ToString(WCC_ConsultationComments.Label_Text) + Convert.ToString(WCC_ConsultationComments.TextBox_Text);

                    //Yasir kamal 7028 11/19/2009 do not display CRT number. (code commented)
                    ///////// Code for Client Code Number
                    //string clcode = cCase.ClientCodeNumber;
                    //Aziz 4062 05/19/08 the mininum length of ClientCourtNumber no should be 10
                    //so that it can be displayed properly.
                    //if (clcode != "0" && clcode.Length > 9)
                    //{
                    //    string temp = clcode.Insert(0, "(");
                    //    temp = temp.Insert(4, ") ");
                    //    hl_callbackno.Text = temp.Insert(9, "-");
                    //}
                    ////

                    //Nasir 6098 08/18/2009 remove tr  to remove comments from contact page 
                    if (cCase.ContinuanceFlag)
                    {

                        tbl_continuanceinfo.Visible = true;
                    }
                    else
                    {

                        tbl_continuanceinfo.Visible = false;
                    }


                    if (cCase.ContinuanceStatus > 0)
                    {
                        // Set Continuance Status    
                        ddl_continuancestatus.SelectedValue = cCase.ContinuanceStatus.ToString();

                    }
                    else
                    {
                        // Default Select Pending Status
                        ddl_continuancestatus.SelectedValue = "1";
                    }

                    ddl_FirmAbbreviation.SelectedValue = cCase.FirmID.ToString();
                    hf_prevfirm.Value = cCase.FirmID.ToString();


                    if (cCase.FirmID != 3000)
                    {
                        DataSet ds = ClsFirms.GetFirmInfo(Convert.ToInt32(cCase.FirmID));

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            lblfaddress1.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                            lblfaddress2.Text = ds.Tables[0].Rows[0]["Address2"].ToString();
                            lblfzipcode.Text = ds.Tables[0].Rows[0]["Zip"].ToString();
                            lblfcity.Text = ds.Tables[0].Rows[0]["City"].ToString();
                            lblfstate.Text = ds.Tables[0].Rows[0]["StateName"].ToString();
                            lblfphone.Text = ds.Tables[0].Rows[0]["Phone"].ToString();

                            if (lblfphone.Text.Length >= 10)
                            {
                                if (lblfphone.Text.Length >= 14)
                                {
                                    string temp = lblfphone.Text.Insert(0, "(");
                                    temp = temp.Insert(4, ") ");
                                    temp = temp.Insert(9, "-");
                                    temp = temp.Insert(14, "(");
                                    lblfphone.Text = temp.Insert(19, ")");

                                }
                                else
                                {
                                    string temp = lblfphone.Text.Insert(0, "(");
                                    temp = temp.Insert(4, ") ");
                                    lblfphone.Text = temp.Insert(9, "-");
                                }
                            }
                        }


                        tbl_firminfo.Visible = true;
                        trfirminformation.Visible = true;
                    }
                    else
                    {
                        tbl_firminfo.Visible = false;
                        trfirminformation.Visible = false;
                    }

                    lbl_bondflag.Text = cCase.BondFlag.ToString();
                    lbl_FirstName.Text = cCase.FirstName.ToUpper();
                    lbl_LastName.Text = cCase.LastName.ToUpper();
                    //addded by ozair
                    hf_LanguageSpeak.Value = cCase.LanguageSpeak.ToUpper();
                    //
                    lbl_CaseCount.Text = cCase.Casecount.ToString();
                    hlnk_MidNo.Text = cCase.MidNumber;
                    if (ClsCase.AddressConfirmFlag == 1)
                    {
                        chkb_addresscheck.Checked = true;
                    }
                    if (ClsCase.YDS == "N")
                    {
                        img_cross.Visible = true;
                        img_right.Visible = false;
                    }
                    if (ClsCase.YDS == "Y" || ClsCase.YDS == "D" || ClsCase.YDS == "S")
                    {
                        img_right.Visible = true;
                        img_cross.Visible = false;
                    }
                    //Added by Azee for Walk in client flag modification..
                    // Sabir Khan 11509 12/18/2013 remove walk in flag
                    //if (ClsCase.WalkInClientFlag != "")
                    //{
                    //    if (ClsCase.WalkInClientFlag == "1")
                    //    {
                    //        rdbtn_Yes.Checked = true;
                    //        rdbtn_No.Checked = false;
                    //    }
                    //    if (ClsCase.WalkInClientFlag == "0")
                    //    {
                    //        rdbtn_No.Checked = true;
                    //        rdbtn_Yes.Checked = false;
                    //    }
                    //}

                    //khalid 25-9-07
                    txt_spn.Text = ClsCase.SPN;
                    hf_court.Value = ClsCase.Court;

                    //Added By Zeeshan For ALR Modifications
                    ViewState["ALRDocument"] = Convert.ToString(Convert.ToInt32(ClsCase.HasALRRequestDocument()));

                    //Noufil 4215 07/01/2008 Check SOl flag
                    hf_isactive.Value = ClsCase.ActiveFlag.ToString();
                    hf_courtdateyear.Value = ClsCase.GetHireDate(TicketId).ToShortDateString();

                    // Noufil 5884 07/02/2009 SHow or hide SMS check box if Mobile is selected from contact drop down.
                    chk_SmsRequired1.Checked = Convert.ToBoolean(ClsCase.SmsFlag1);
                    chk_SmsRequired2.Checked = Convert.ToBoolean(ClsCase.SmsFlag2);
                    chk_SmsRequired3.Checked = Convert.ToBoolean(ClsCase.SmsFlag3);

                    ViewState["chk_SmsRequired1"] = chk_SmsRequired1.Checked;
                    ViewState["chk_SmsRequired2"] = chk_SmsRequired2.Checked;
                    ViewState["chk_SmsRequired3"] = chk_SmsRequired3.Checked;

                    if (ddl_contact1.SelectedValue == "4")
                        td_chk_SMS1.Style[HtmlTextWriterStyle.Display] = "Block";
                    else
                        td_chk_SMS1.Style[HtmlTextWriterStyle.Display] = "none";

                    if (ddl_contact2.SelectedValue == "4")
                        td_chk_SMS2.Style[HtmlTextWriterStyle.Display] = "Block";
                    else
                        td_chk_SMS2.Style[HtmlTextWriterStyle.Display] = "none";

                    td_chk_SMS3.Style[HtmlTextWriterStyle.Display] = ddl_contact3.SelectedValue == "4" ? "Block" : "none";


                    //Abbas Qamar 10114 04-05-2012 checking the Forbend,HMCCC,Montgoemry Criminals Courts if exist the hiding the DL/St Scan and US Citizen question.

                    int CourtId = cCase.IsCriminalCourt;

                    if (CourtId > 0)
                    {
                        tdDrivingLicense.Visible = false;
                        tdCitizen.Visible = false;
                    }



                    // End 10114

                    // Rab Nawaz Khan 11473 10/25/2013 Added to show populated New Lead page. . . 
                    hf_languageSpk.Value = cCase.LanguageSpeak;
                    hf_FirstNameLastName.Value = cCase.FirstName.ToUpper() + " " + cCase.MiddleName.ToUpper() + " " + cCase.LastName.ToUpper();
                    hf_ContactNum.Value = cCase.Contact1;
                    hf_emailadd.Value = cCase.Email;
                    hf_TicketIdForLead.Value = ClsCase.TicketID.ToString();
                    

                }
                else
                {
                    dvMessage.Visible=true;lblMessage.Text = "Information not retrived for the given ticket";
                }

            }
            catch (Exception exGetclient)
            {
                clog.ErrorLog(exGetclient.Message, exGetclient.Source, exGetclient.TargetSite.ToString(), exGetclient.StackTrace);
                dvMessage.Visible=true;dvMessage.Visible=true;lblMessage.Text = exGetclient.Message;
            }
        }

        /// <summary>
        /// Hafiz 10288 07/17/2012 Get the Post hire question data 
        /// </summary>
        /// <param name="ticid"></param>
        private void GetALRInfo(int ticid, DataSet dsContactUsData)
        {

            DataSet ds_Case = ClsCase.GetCase(ticid);
            if (ds_Case.Tables.Count > 0)
            {
                if (ds_Case.Tables[0].Rows.Count > 0)
                {

                    DataRow dRow = ds_Case.Tables[0].Rows[0];

                    int CurrentCaseType = (int)dRow["CaseTypeId"];

                    divClient.InnerText = Convert.ToInt32(dRow["Activeflag"]).ToString() + "~" + CurrentCaseType;
                    ViewState["CurrentCaseType"] = CurrentCaseType.ToString();
                    if (CurrentCaseType == CaseType.Criminal)
                    {
                        //lnkEmailCaseSummary.OnClientClick = "return ShowCaseEmailPanel();";
                        //hf_IsPreHire.Value = "1";
                        tblBodyParts.Style["display"] = "none";
                        trSSN.Style["display"] = "none";


                        //tr_PreHireQuestion.Style["display"] = "block";
                        //tr_QuestionBar.Style["display"] = "block";
                        //tr_FeeQuestion.Style["display"] = "none";

                        //tbBodyCriminalALR.Style["display"] = "block";
                        //tbBodyCaseSummary.Style["display"] = "block";


                        clsENationWebComponents clsdb = new clsENationWebComponents();
                        if (dsContactUsData != null)
                        {
                            //DataSet ds_dlstate = clsdb.Get_DS_BySP("USP_HTS_Get_State");

                            ddl_ALRState.DataSource = dsContactUsData.Tables[0];
                            ddl_ALRState.DataTextField = dsContactUsData.Tables[0].Columns[1].ColumnName;
                            ddl_ALRState.DataValueField = dsContactUsData.Tables[0].Columns[0].ColumnName;
                            ddl_ALRState.DataBind();

                            ddl_ALROBSState.DataSource = dsContactUsData.Tables[0];
                            ddl_ALROBSState.DataTextField = dsContactUsData.Tables[0].Columns[1].ColumnName;
                            ddl_ALROBSState.DataValueField = dsContactUsData.Tables[0].Columns[0].ColumnName;
                            ddl_ALROBSState.DataBind();
                        }

                        //Asad Ali 8153 10/27/2010 fix issue not setting intoxilyzer drop down value 
                        string drp_IntoxilyzerTaken = dRow["ALRIsIntoxilyzerTakenFlag"].ToString();

                        ViewState["drp_IntoxilyzerTaken"] = drp_IntoxilyzerTaken;
                        if (drp_IntoxilyzerTaken == "1")
                        {
                            tr_IntoxilyzerResults.Style[HtmlTextWriterStyle.Display] = "table-row";
                            tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "table-row";
                            tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "table-row";
                        }

                        if (Convert.ToInt32(dRow["Activeflag"]) == 0)
                        {
                            tr_postHireQuestions.Style[HtmlTextWriterStyle.Display] = "block";

                            tr_IntoxilyzerResults.Style[HtmlTextWriterStyle.Display] = "none";
                            tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "none";
                            tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "none";
                            //Waqas 6342 08/12/2009 ALR required
                            //tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "none";
                            //Asad Ali 8153 09/09/2010 move this question in pre hire section 
                            //tr_ALRhearingRequired.Style[HtmlTextWriterStyle.Display] = "none";

                            //lnkEmailCaseSummary.Visible = false;
                            //lblEmailCaseSummary.Visible = false;
                        }
                        else
                        {
                            //tbBodyCriminalALRQA.Disabled = true;
                            //txtCaseSummaryComments.Enabled = false;


                            //Waqas 6342 08/24/2009 For All criminal 

                            //lnkEmailCaseSummary.Visible = true;
                            //lblEmailCaseSummary.Visible = true;

                            //If all violations are of ALR it will return true
                            //if (Convert.ToInt32(dRow["isALLALR"]) == 1)
                            //{

                            //    lnkEmailCaseSummary.Visible = false;
                            //    lblEmailCaseSummary.Visible = false;
                            //}

                            ViewState["ActiveFlag"] = dRow["Activeflag"].ToString();
                            ViewState["IsALRHearingRequired"] = "false";

                            if (Convert.ToInt32(dRow["isALR"]) == 1)
                            {

                                tr_postHireQuestions.Style[HtmlTextWriterStyle.Display] = "block";
                                //tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "block";
                                //Asad Ali 8153 09/09/2010 Move to PreHire Section
                                //Waqas 6342 08/12/2009 ALR required

                                //Yasir Kamal 7150 01/01/2010 display County of Arrest.
                                if (dRow["CountyOfArrest"].ToString().Trim().ToLower() != "")
                                {
                                    lblCountyOfArrest.Text = dRow["CountyOfArrest"].ToString();
                                    HLkCounty.Style[HtmlTextWriterStyle.Display] = "none";
                                    HLkCounty.NavigateUrl = "";
                                    lblCountyOfArrest.Style[HtmlTextWriterStyle.Display] = "block";
                                }
                                else
                                {
                                    HLkCounty.Style[HtmlTextWriterStyle.Display] = "table-row";
                                    HLkCounty.NavigateUrl = "~/backroom/Courts.aspx?ALRCourt=" + dRow["ALRCourt"].ToString();
                                    lblCountyOfArrest.Style[HtmlTextWriterStyle.Display] = "none";
                                }

                                ViewState["IsALRHearingRequired"] = dRow["IsALRHearingRequired"].ToString();

                                if (dRow["IsALRHearingRequired"].ToString().ToLower() == "false")
                                {
                                    //tr_ALRRequiredWhy.Style[HtmlTextWriterStyle.Display] = "table-row";
                                    tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "none";
                                    txtALRHearingRequiredAnswer.Text = dRow["ALRHearingRequiredAnswer"].ToString();
                                }
                                else
                                {


                                   // tr_ALRRequiredWhy.Style[HtmlTextWriterStyle.Display] = "none";
                                    tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "block";
                                    txtALROfficerName.Text = dRow["ALROfficerName"].ToString();
                                    txtALROfficerBadgeNumber.Text = dRow["ALROfficerbadge"].ToString();
                                    txtALRPrecinct.Text = dRow["ALRPrecinct"].ToString();
                                    txtALRAddress.Text = dRow["ALROfficerAddress"].ToString();
                                    txtALRCity.Text = dRow["ALROfficerCity"].ToString();
                                    ddl_ALRState.SelectedValue = dRow["ALROfficerStateID"].ToString();
                                    txtALRZip.Text = dRow["ALROfficerZip"].ToString();

                                    string contact11 = dRow["ALROfficerContactNumber"].ToString().Trim();
                                    if (contact11.Length > 0)
                                    {
                                        if (contact11.Length >= 3)
                                            txt_Attacc11.Text = contact11.Substring(0, 3);
                                        if (contact11.Length >= 6)
                                            txt_Attcc12.Text = contact11.Substring(3, 3);
                                        if (contact11.Length >= 10)
                                            txt_Attcc13.Text = contact11.Substring(6, 4);

                                        if (contact11.Length > 10)
                                            txt_Attcc14.Text = contact11.Substring(10);

                                    }
                                    txtALRArrestingAgency.Text = dRow["ALRArrestingAgency"].ToString();

                                    //-----
                                    if (dRow["ALRMileage"].ToString().Trim() != "0")
                                    {
                                        txtALROfficerMileage.Text = dRow["ALRMileage"].ToString();
                                    }
                                    else
                                    {
                                        txtALROfficerMileage.Text = "";
                                    }


                                    //Waqas 6342 08/13/2009 observing officer
                                    if (dRow["IsALRArrestingObservingSame"].ToString() == "")
                                    {
                                        rBtnArrOffObsOffYes.Checked = false;
                                        rBtnArrOffObsOffNo.Checked = false;
                                    }
                                    else if (dRow["IsALRArrestingObservingSame"].ToString().ToLower() == "false")
                                    {
                                        rBtnArrOffObsOffYes.Checked = false;
                                        rBtnArrOffObsOffNo.Checked = true;

                                        tr_ObservingOfficer.Style[HtmlTextWriterStyle.Display] = "table-row";

                                        txtALROBSOfficerName.Text = dRow["ALRObservingOfficerName"].ToString();
                                        txtALROBSOfficerBadgeNumber.Text = dRow["ALRObservingOfficerBadgeNo"].ToString();
                                        txtALROBSPrecinct.Text = dRow["ALRObservingOfficerPrecinct"].ToString();
                                        txtALROBSAddress.Text = dRow["ALRObservingOfficerAddress"].ToString();
                                        txtALROBSCity.Text = dRow["ALRObservingOfficerCity"].ToString();
                                        ddl_ALROBSState.SelectedValue = dRow["ALRObservingOfficerState"].ToString();
                                        txtALROBSZip.Text = dRow["ALRObservingOfficerZip"].ToString();

                                        string OBScontact11 = dRow["ALRObservingOfficerContact"].ToString().Trim();
                                        if (OBScontact11.Length > 0)
                                        {
                                            if (OBScontact11.Length >= 3)
                                                txt_OBSAttacc11.Text = OBScontact11.Substring(0, 3);
                                            if (OBScontact11.Length >= 6)
                                                txt_OBSAttcc12.Text = OBScontact11.Substring(3, 3);
                                            if (OBScontact11.Length >= 10)
                                                txt_OBSAttcc13.Text = OBScontact11.Substring(6, 4);

                                            if (OBScontact11.Length > 10)
                                                txt_OBSAttcc14.Text = OBScontact11.Substring(10);

                                        }
                                        txtALROBSArrestingAgency.Text = dRow["ALRObservingOfficerArrestingAgency"].ToString();

                                        if (dRow["ALRObservingOfficerMileage"].ToString().Trim() != "0")
                                        {
                                            txtALROBSOfficerMileage.Text = dRow["ALRObservingOfficerMileage"].ToString();
                                        }
                                        else
                                        {
                                            txtALROBSOfficerMileage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        rBtnArrOffObsOffYes.Checked = true;
                                        rBtnArrOffObsOffNo.Checked = false;

                                        tr_ObservingOfficer.Style[HtmlTextWriterStyle.Display] = "none";
                                    }




                                    if (dRow["ALRIsIntoxilyzerTakenFlag"].ToString().Trim() == "1") //Yes
                                    {
                                        tr_IntoxilyzerResults.Style[HtmlTextWriterStyle.Display] = "table-row";

                                        if (dRow["ALRIntoxilyzerResult"].ToString().Trim().ToLower() == "pass")
                                        {
                                            rbtn_IntoxResultPass.Checked = true;
                                            rbtn_IntoxResultFail.Checked = false;




                                            //txtBTOFirstName.Text = dRow["ALRBTOFirstName"].ToString();
                                            //txtBTOLastName.Text = dRow["ALRBTOLastName"].ToString();



                                        }
                                        else if (dRow["ALRIntoxilyzerResult"].ToString().Trim().ToLower() == "fail")
                                        {
                                            rbtn_IntoxResultPass.Checked = false;
                                            rbtn_IntoxResultFail.Checked = true;
                                            //Asad Ali 7991 06/14/2010 We need to display BTO officer name and BTS Officer name question show up even when INtoxilyzer result is 'Fail' or it's 'Pass' 
                                            tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "table-row";
                                            tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "table-row";
                                        }
                                        else
                                        {
                                            rbtn_IntoxResultPass.Checked = false;
                                            rbtn_IntoxResultFail.Checked = false;
                                            tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "none";
                                            tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "none";
                                        }

                                        //Asad Ali 7991 07/31/2010 fix issue when pass check not displaying values
                                        tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "table-row";
                                        //Yasir Kamal 7150 01/01/2010 ALR BTS, BTO modified.
                                        tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "table-row";
                                        // tr_BreathTestSame.Style[HtmlTextWriterStyle.Display] = "block";



                                        if ((dRow["ALROfficerName"].ToString().Trim().ToLower() != string.Empty && dRow["ALRBTOLastName"].ToString().Trim().ToLower() != string.Empty) && (dRow["ALROfficerName"].ToString().Trim().ToLower() == dRow["ALRBTOLastName"].ToString().Trim().ToLower()))
                                        {
                                            rbBTOArresting.Checked = true;
                                            rbBTOObserving.Checked = false;

                                        }
                                        else if ((dRow["ALRObservingOfficerName"].ToString().Trim().ToLower() != string.Empty && dRow["ALRBTOLastName"].ToString().Trim().ToLower() != string.Empty) && (dRow["ALRObservingOfficerName"].ToString().Trim().ToLower() == dRow["ALRBTOLastName"].ToString().Trim().ToLower()))
                                        {
                                            rbBTOArresting.Checked = false;
                                            rbBTOObserving.Checked = true;
                                            txtBTOName.Enabled = false;
                                        }
                                        // else if (dRow["ALRBTOLastName"].ToString().Trim() == "")
                                        else if (dRow["ALRBTOLastName"].ToString().Trim() != string.Empty)
                                        {
                                            rblBTOName.Checked = true;
                                            txtBTOName.Text = dRow["ALRBTOLastName"].ToString();
                                            txtBTOName.Enabled = true;
                                        }
                                        //else
                                        //{
                                        //    rblBTOName.Checked = true;
                                        //    txtBTOName.Text = dRow["ALRBTOLastName"].ToString();
                                        //    txtBTOName.Enabled = true;
                                        //}
                                        if (dRow["ALRBTSFirstName"].ToString().Trim() != string.Empty)
                                        {
                                            txtBTSFirstName.Text = dRow["ALRBTSFirstName"].ToString();
                                        }
                                        if (dRow["ALRBTSLastName"].ToString().Trim() != string.Empty)
                                        {
                                            txtBTSLastName.Text = dRow["ALRBTSLastName"].ToString().Trim();
                                        }


                                    }
                                    else
                                    {
                                        tr_IntoxilyzerResults.Style[HtmlTextWriterStyle.Display] = "none";
                                        tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "none";
                                        tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "none";
                                    }
                                    //7150 end
                                }
                            }
                            else
                            {
                                tr_postHireQuestions.Style[HtmlTextWriterStyle.Display] = "none";
                                //Asad Ali 8153 09/23/2010  move in pre hire section 
                                //tr_ALRhearingRequired.Style[HtmlTextWriterStyle.Display] = "none";


                                //lnkEmailCaseSummary.Visible = true;
                                //lblEmailCaseSummary.Visible = true;
                            }
                        }
                    }
                }
            }
        }

        //Event fired when update button is clicked
        private void btn_update2_Click(object sender, EventArgs e)
        {
            //Hafiz 10288 07/18/2012 Update alr info
            int casetype = -1;
            if (ViewState["CurrentCaseType"] != null)
                casetype = int.Parse(ViewState["CurrentCaseType"].ToString());
            if (divClient.InnerText.Split('~')[0] == "1" && (casetype == CaseType.Criminal)) //|| casetype == CaseType.FamilyLaw
            {
                UpdateALRInfo();
            }
            //End 10288

            UpdateClientInfo();

            //BindFlags();
            BindFlagsNew();
            //nasir 1/12/2009 5381 Redirect to handle event bubuling
            if (Check)
                Response.Redirect(Request.Url.AbsoluteUri, false);

            // Haris Ahmed 10003 01/20/2012 Show the No Bad Address Model Popup or remove No Bad Address Flag.
            if (img_cross.Visible)
                Modal_NoBadAddress.Show();
            else
            {
                ResetNoBadAddressFlag(TicketId, false);

                string fid = GetFidForNoBadAddressFlag(TicketId.ToString());

                if (fid != "")
                {
                    string[] key1 = { "@TicketID", "flagid", "empid", "@fid" };
                    object[] value1 = { TicketId, 45, cSession.GetCookie("sEmpID", Request), fid };

                    ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                }
            }
            // Haris Ahmed 10003 02/29/2012 Move Method to update Flag List
            FillFlagList();
        }

        /// <summary>
        /// For ALR Post Hire Questions
        /// </summary>
        private void UpdateALRInfo()
        {
            clsCase ClsCase = new clsCase();
            ClsCase.TicketID = TicketId;
            ClsCase.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", Request));
            if (Convert.ToInt32(ViewState["ActiveFlag"].ToString().Trim()) == 1)
            {
                //Waqas 6342 08/12/2009 ALR Observing officer
                ClsCase.ALROfficerName = txtALROfficerName.Text;
                ClsCase.ALROfficeBadgeNo = txtALROfficerBadgeNumber.Text;
                ClsCase.ALRPrecinct = txtALRPrecinct.Text;
                ClsCase.ALROfficerCity = txtALRCity.Text;
                ClsCase.ALROfficerAddress = txtALRAddress.Text;
                ClsCase.ALROfficerState = Convert.ToInt32(ddl_ALRState.SelectedValue);
                ClsCase.ALROfficerZip = txtALRZip.Text;
                ClsCase.ALRContactNumber = String.Concat(txt_Attacc11.Text, txt_Attcc12.Text.TrimStart(), txt_Attcc13.Text, txt_Attcc14.Text);
                ClsCase.ALRArrestingAgency = txtALRArrestingAgency.Text;
                if (txtALROfficerMileage.Text.Trim() != "")
                {
                    ClsCase.ALROfficerMileage = Convert.ToDecimal(txtALROfficerMileage.Text);
                }


                if (ViewState["drp_IntoxilyzerTaken"].ToString() == "1")
                {
                    //Hafiz 10288 07/23/2012 added for intoxilyzer flag issue.
                    ClsCase.ALRIntoxilyzerTakenFlag = int.Parse(ViewState["drp_IntoxilyzerTaken"].ToString());

                    if (rbBTOArresting.Checked)
                    {
                        ClsCase.ALRBTOLastName = txtALROfficerName.Text;
                    }
                    if (rbBTOObserving.Checked)
                    {
                        ClsCase.ALRBTOLastName = txtALROBSOfficerName.Text;
                    }
                    if (rblBTOName.Checked)
                    {
                        ClsCase.ALRBTOLastName = txtBTOName.Text;
                    }
                    //ClsCase.ALRBTOFirstName = txtBTOFirstName.Text;
                    //ClsCase.ALRBTOLastName = txtBTOLastName.Text;
                    ClsCase.ALRBTSFirstName = txtBTSFirstName.Text;
                    ClsCase.ALRBTSLastName = txtBTSLastName.Text;

                    if (rbtn_IntoxResultPass.Checked == true)
                    {
                        ClsCase.ALRIntoxilyzerResult = "pass";
                    }
                    else if (rbtn_IntoxResultFail.Checked == true)
                    {
                        ClsCase.ALRIntoxilyzerResult = "fail";
                    }

                    //Waqas 6342 08/24/2009 New changes for all criminal and family cases
                    //if (rBtn_BtoBtsSameYes.Checked == true)
                    //{
                    //    ClsCase.ALRBTOBTSSameFlag = true;
                    //    //ClsCase.ALRBTOBTSSameFlag = true;
                    //}
                    //else if (rBtn_BtoBtsSameNo.Checked == true)
                    //{
                    //    ClsCase.ALRBTOBTSSameFlag = false;
                    //    //ClsCase.ALRBTOBTSSameFlag = false;
                    //    ClsCase.ALRBTSFirstName = txtBTSFirstName.Text;
                    //    ClsCase.ALRBTSLastName = txtBTSLastName.Text;
                    //}
                    //else
                    //{
                    //    ClsCase.ALRBTOBTSSameFlag = null;
                    //}
                }

                //7150 end


                ClsCase.ALRHearingRequiredAnswer = txtALRHearingRequiredAnswer.Text.Trim();

                if (rBtnArrOffObsOffYes.Checked == true)
                {
                    ClsCase.IsALRArrestingObservingSame = true;
                }
                else if (rBtnArrOffObsOffNo.Checked == true)
                {
                    ClsCase.IsALRArrestingObservingSame = false;
                }
                else
                {
                    ClsCase.IsALRArrestingObservingSame = null;
                }


                ClsCase.ALROBSOfficerName = txtALROBSOfficerName.Text;
                ClsCase.ALROBSOfficeBadgeNo = txtALROBSOfficerBadgeNumber.Text;
                ClsCase.ALROBSPrecinct = txtALROBSPrecinct.Text;
                ClsCase.ALROBSOfficerCity = txtALROBSCity.Text;
                ClsCase.ALROBSOfficerAddress = txtALROBSAddress.Text;
                ClsCase.ALROBSOfficerState = Convert.ToInt32(ddl_ALROBSState.SelectedValue);
                ClsCase.ALROBSOfficerZip = txtALROBSZip.Text;
                ClsCase.ALROBSContactNumber = String.Concat(txt_OBSAttacc11.Text, txt_OBSAttcc12.Text.TrimStart(), txt_OBSAttcc13.Text, txt_OBSAttcc14.Text);
                ClsCase.ALROBSArrestingAgency = txtALROBSArrestingAgency.Text;

                if (txtALROBSOfficerMileage.Text.Trim() != "")
                {
                    ClsCase.ALROBSOfficerMileage = Convert.ToDecimal(txtALROBSOfficerMileage.Text);
                }
                if (ViewState["IsALRHearingRequired"].ToString().ToLower() == "true")
                {
                    ClsCase.IsALRHearingRequired = true;
                }
                else
                {
                    ClsCase.IsALRHearingRequired = false;
                }

                ClsCase.UpdateALRPostHireQuestions();

            }
        }

        //Event Fired When flags get selected
        private void ddl_flags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Abid Ali 4846 11/10/2008 -- stop default option
                if (ddl_flags.SelectedIndex == 0) return;

                //Comment By Zeeshan Ahmed
                //
                //if (ddl_priority.SelectedValue != "23")
                {
                    // Abbas Qamar 9676 12/09/2011 check the bad email flag if true then reset the Reviewed Email Status in tbltickets for Bad Email address report.
                    if (ddl_flags.SelectedValue == Convert.ToString(Convert.ToInt32(FlagType.bademail)))
                    {
                        ResetReviewedEmailStatus(TicketId);
                    }

                    // Farrukh 10003 01/20/2012 Set the NoBadAddressFlag in tbltickets for Bad addresses report.
                    if (ddl_flags.SelectedValue == "45")
                    {
                        ResetNoBadAddressFlag(TicketId, true);
                    }

                    // Babar Ahmad 9654 09/30/2011 If Problem Client (No Hire/Allow Hire) flag is set then mark the case as do not mail.
                    if (ddl_flags.SelectedValue == Convert.ToString(Convert.ToInt32(FlagType.ProblemClient_NoHire)) || ddl_flags.SelectedValue == Convert.ToString(Convert.ToInt32(FlagType.ProblemClient_AllowHire)))
                    {
                        ClsCase.GetClientInfo(TicketId);
                        NoMailFlag noMailFlag = new NoMailFlag(ClsCase.FirstName, ClsCase.LastName, ClsCase.Address1, ClsCase.City, ClsCase.StateDescription, ClsCase.Zip, 1, ClsCase.Record_Id);
                        noMailFlag.UpdateFlag();
                    }


                    if (ddl_flags.SelectedValue == Convert.ToString(Convert.ToInt32(FlagType.ProblemClient_AllowHire)) || ddl_flags.SelectedValue == "22")
                    {
                        // Abid Ali 4846 11/10/2008 -- show add trial comments pop up
                        addPopUpComment_Trial.AssociatedControlId = ddl_flags.ClientID;
                        addPopUpComment_Trial.Initialize("Add Trial Comment", Convert.ToInt32(cSession.GetCookie("sEmpID", Request)), Convert.ToInt32(ddl_flags.SelectedValue), TicketId, 4, modalPopupExtenderFlagsEvents.ClientID);
                        modalPopupExtenderFlagsEvents.Show();
                        //Nasir 7157 12/21/2009 select option choose
                        ddl_flags.SelectedIndex = 0;
                    }

                    else
                    {
                        int casenum = TicketId;
                        string[] key1 = { "TicketID", "flagID", "EmpID" };
                        //object[]value1={ClsCase.TicketID,Convert.ToInt32(ddl_flags.SelectedValue),ClsCase.EmpID};
                        object[] value1 = { TicketId, Convert.ToInt32(ddl_flags.SelectedValue), cSession.GetCookie("sEmpID", Request) };

                        // If Continuance Selected
                        if (ddl_flags.SelectedValue == "9")
                        {
                            CheckContinuanceSelection(casenum);
                        }
                        else
                        {
                            //Outside client firm
                            if (ddl_flags.SelectedValue == "8")
                            {
                                hf_checkFirm.Value = "1";
                                tbl_firminfo.Visible = true;
                            }
                            //Noufil 4215 06/16/2008 Update status to Disp
                            else if (ddl_flags.SelectedValue == "35")
                            {
                                ClsCase.UpdateCaseStatus(TicketId, CourtViolationStatusType.DISP, EmpId);

                                if (nos.CheckNOS(TicketId))
                                {
                                    //Delete NOS Flag
                                    clsFlags.DeleteFlagbyFId(TicketId, FlagType.NotOnSystem, EmpId);
                                }
                                Hashtable hs = new Hashtable { { TicketId, TicketId } };

                                clsBatch clsbatch = new clsBatch();
                                clsbatch.SendLetterToBatch(hs, BatchLetterType.SOL, EmpId);
                            }
                            //Set NOS Date
                            else if (ddl_flags.SelectedValue == "15")
                            {
                                nos.UpdateNOS(TicketId, Convert.ToInt32(ViewState["empid"]));
                            }
                            else if (ddl_flags.SelectedValue == "30")
                            {
                                clsLogger log = new clsLogger();
                                log.AddNote(Convert.ToInt32(cSession.GetCookie("sEmpID", Request)), "Fine amount checked",
                                    "Fine amount checked", TicketId);
                            }
                            else if (ddl_flags.SelectedValue == "33")
                            {

                                Hashtable objHashTable = new Hashtable();
                                objHashTable.Add(TicketId, TicketId);
                                objBatch.SendLetterToBatch(objHashTable, BatchLetterType.SetCallLetter, EmpId);
                                lblbadnumber.Visible = true;
                                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                                tbl_messages.Style["display"] = "block";
                            }

                            ClsDb.ExecuteSP("USP_HTS_Insert_flag_by_ticketnumber_and_flagID", key1, value1);
                        }


                        // Noufil 7157 12/16/2009 Send Email for Problem Client (NoHire)
                        if (ddl_flags.SelectedValue == Convert.ToString(Convert.ToInt32(FlagType.ProblemClient_NoHire)))
                        {
                            MailClass objmailclass = new MailClass();
                            string content = objmailclass.GetProblemClientEmailBody(ddl_flags.SelectedItem.Text.ToUpper() + " flag has been added for client <a href= http://" + Request.Url.Authority + "/clientinfo/ViolationFeeold.aspx?sMenu=61&casenumber=" + TicketId + "&search=0 >" + txt_fname.Text + " " + txt_lname.Text + "</a>");
                            string subject = Convert.ToString(ConfigurationManager.AppSettings["ProblemClientEmailSubject"]).Replace("@Flagname", ddl_flags.SelectedItem.Text.ToUpper());
                            subject = subject.Replace("@clientname", txt_fname.Text + " " + txt_lname.Text);
                            LNHelper.MailClass.SendMail(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["ProblemClientEmailTO"], subject, content, "");
                            //MailClass.SendMail(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["ProblemClientEmailTO"], subject, content, "");
                        }

                        //Fill Flag List
                        FillFlagList();
                        //BindFlags();
                        BindFlagsNew();

                        //Need To Be Refactor
                        //Fill Comments

                        InitializeCommentControls();



                        //ozair 5192 11/21/2008	
                        Response.Redirect(Request.Url.AbsoluteUri, false);
                    }


                }
            }
            catch (Exception ee)
            {
                clog.ErrorLog(ee.Message, ee.Source, ee.TargetSite.ToString(), ee.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ee.Message;
            }
        }
        //Sabir Khan 5260 11/27/2008 Checks for Not on system flag ...
        ///<summary>
        ///</summary>
        public void HasNotonSystem()
        {
            if (!clsFlags.HasFlag(15))
            {
                hf_HasNotonSystem.Value = "1";
            }
        }

        private void CheckContinuanceSelection(int casenum)
        {
            bool chkContinuance;

            if (hf_continuancedate.Value == "")
                chkContinuance = true;

            else
            {
                DateTime continuanceDate = DateTime.Now;
                DataSet ds = Session["caseData"] as DataSet;
                if (ds != null)
                { 
                    continuanceDate = GetFutureCourtDate(casenum, ds);
                }

                // IF COURT DATE IS ON MONDAY AND CURRENT DAY IS SATURDAY OR SUNDAY......
                if (continuanceDate.DayOfWeek == DayOfWeek.Monday && (continuanceDate.AddDays(-1).ToShortDateString() == DateTime.Now.ToShortDateString() || continuanceDate.AddDays(-2).ToShortDateString() == DateTime.Now.ToShortDateString()))
                    chkContinuance = false;

                // IF COURT DATE IS ON MONDAY AND CURRENT DAY IS FRIDAY AND AFTER 6:00 PM......
                else if (DateTime.Now.TimeOfDay > Convert.ToDateTime("6:00 PM").TimeOfDay && continuanceDate.DayOfWeek == DayOfWeek.Monday && continuanceDate.AddDays(-3).ToShortDateString() == DateTime.Now.ToShortDateString())
                    chkContinuance = false;

                // IF COURT DATE IS ON NEXT DAY AND CURREN TIME IS GREATER THAN 6:00PM .....
                else if (DateTime.Now.TimeOfDay > Convert.ToDateTime("6:00 PM").TimeOfDay && (continuanceDate.ToShortDateString() == DateTime.Now.AddDays(1).ToShortDateString() || continuanceDate.ToShortDateString() == DateTime.Now.ToShortDateString()))
                    chkContinuance = false;

                else chkContinuance = true;
            }

            if (chkContinuance)
            {
                if (ReCalculatePrice(true))
                {

                    string[] keys = { "TicketID", "flagID", "EmpID", "@ContinuanceDate", "@ContinuanceStatus" };
                    object[] values = { TicketId, 9, cSession.GetCookie("sEmpID", Request), hf_continuancedate.Value, 1 };

                    ClsDb.ExecuteSP("USP_HTS_Insert_Continuance_flag", keys, values);

                    //Nasir 6098 08/18/2009 remove  tr to remove comments from contact page 
                    hf_checkcontinuance.Value = "1";
                    hf_continuanceflag.Value = "1";
                    //tr_continuance.Visible = true;
                    tbl_continuanceinfo.Visible = true;
                    InitializeCommentControls();


                }
                else
                {
                    dvMessage.Visible=true;lblMessage.Text = "Continuance Flag cannot be added.";
                }
            }
            else
            {
                dvMessage.Visible=true;lblMessage.Text = "You cannot add the continuance flag because you court date is too near.";
            }
        }

        //Event Fired When Flag get clicked
        private void DataL_flags_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "FlagClicked")
            {
                try
                {


                    // Getting Flag ID
                    HiddenField hfFlagid = (HiddenField)e.Item.FindControl("hf_flagid");

                    string fid = Convert.ToString(e.CommandArgument);
                    int flagid = Convert.ToInt32(hfFlagid.Value);

                    string[] key1 = { "@TicketID", "flagid", "empid", "@fid" };
                    object[] value1 = { TicketId, flagid, cSession.GetCookie("sEmpID", Request), fid };

                    switch (flagid)
                    {

                        case 8: // Remove Outside Firm Flag
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            hf_checkFirm.Value = "0";
                            hf_prevfirm.Value = "3000";
                            ddl_FirmAbbreviation.SelectedValue = "3000";
                            tbl_firminfo.Visible = false;
                            trfirminformation.Visible = false;
                            break;

                        case 9: // Remove Continuance Flag
                            if (ReCalculatePrice(false))
                            {
                                ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                                hf_checkcontinuance.Value = "0";
                                hf_continuanceflag.Value = "0";

                                //Nasir 6098 08/18/2009 remove tr to remove comments from contact page 

                                tbl_continuanceinfo.Visible = false;
                                hf_unpaid.Value = "0";
                                //tb_continuance.Text = "";

                                //Nasir 6098 08/18/2009 remove comm to remove comments from contact page 

                            }
                            else
                            {
                                dvMessage.Visible=true;lblMessage.Text = "Cannot Remove Conitinuance Flag";
                            }

                            break;

                        case 15: // Not On System                           
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            hf_checkFirm.Value = "0";
                            hf_prevfirm.Value = "3000";
                            ddl_FirmAbbreviation.SelectedValue = "3000";
                            tbl_firminfo.Visible = false;
                            trfirminformation.Visible = false;
                            break;
                        case 23: hf_checkServiceTicket.Value = "101";
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            break;
                        case 33:
                            //Agha Usman 4165 06/03/2008
                            //clsBatch objBatch = new clsBatch();
                            objBatch.deleteLetterFromBatch(BatchLetterType.SetCallLetter, TicketId);
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            lblbadnumber.Visible = false;
                            // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                            tbl_messages.Style["display"] = "none";
                            break;
                        case 35:

                            if (ClsCase.IsInsideCourtCase(TicketId))
                                ClsCase.UpdateCaseStatus(TicketId, CourtViolationStatusType.ARR, EmpId);
                            if (ClsCase.IsHCJPCourt(TicketId, 1))
                                ClsCase.UpdateCaseStatus(TicketId, CourtViolationStatusType.APP, EmpId);

                            objBatch.deleteLetterFromBatch(BatchLetterType.SOL, TicketId);
                            //objBatch.DeleteLetterFromBatch(BatchLetterType.SOL, this.TicketId, this.EmpId);
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            break;
                        case 45: // Haris Ahmed 10003 01/20/2012 Set the NoBadAddressFlag in tbltickets for Bad addresses report.
                            ResetNoBadAddressFlag(TicketId, false);
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            CheckReadNotes(Convert.ToInt32(ViewState["empid"]));
                            Response.Redirect(Request.Url.ToString(), false);
                            break;

                        default:
                            ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                            CheckReadNotes(Convert.ToInt32(ViewState["empid"]));
                            Response.Redirect(Request.Url.ToString(), false);
                            break;

                        //CheckReadNotes(this.TicketId);

                    }
                    //Populate Updated Flag List
                    ddl_flags.Items.Clear();
                    FillFlagList();

                    InitializeCommentControls();
                }
                catch (Exception ee)
                {
                    clog.ErrorLog(ee.Message, ee.Source, ee.TargetSite.ToString(), ee.StackTrace);
                    dvMessage.Visible=true;lblMessage.Text = ee.Message;
                }
            }
        }

        //In order to redirect to next page
        private void btn_next_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateClientInfo();
                //Nasir 5381 01/17/2009 handle event bubleling
                Check = true;
                if (Check)
                    Response.Redirect("NewPaymentInfo.aspx?casenumber=" + TicketId + "&search=" + ViewState["vSearch"], false);
            }
            catch (Exception ee)
            {
                clog.ErrorLog(ee.Message, ee.Source, ee.TargetSite.ToString(), ee.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ee.Message;
            }
        }

        //When update button is clicked
        private void btn_update1_Click(object sender, EventArgs e)
        {
            UpdateClientInfo();
            //nasir 1/12/2009 5381 Redirect to handle event bubuling
            //Response.Redirect(Request.Url.AbsoluteUri, false);
        }

        // OCR and Scanning Events
        protected void lbtn_Scan_Click(object sender, EventArgs e)
        {
            try
            {
                //ViewState["ScanDocType"] = cmbDocType1.SelectedItem.Text;
                ViewState["BookID"] = 0;
                ViewState["DocCount"] = 0;

                //if (txtbID.Text.Length > 0)
                //{
                //    ViewState["sSDDocCount"] = ViewState["sSDDocCount"].ToString();
                //}
                //else
                {
                    ViewState["sSDDocCount"] = 1;
                }
                ViewState["sSDDesc"] = "";//txtDescription.Text.Trim().ToUpper();

                //Perform database Uploading Task

                string searchpat = "*" + Session.SessionID + txtempid.Text + "*.jpg";
                //string[] fileName = Directory.GetFiles(Server.MapPath("tempimages/"), searchpat);
                string[] fileName = Directory.GetFiles(ViewState["vNTPATHScanTemp"].ToString(), searchpat);
                //
                if (fileName.Length > 1)
                {
                    fileName = sortfilesbydate(fileName);
                }

                int empid = Convert.ToInt32(cSession.GetCookie("sEmpID", Request)); //
                int ticketId = (int)Convert.ChangeType(TicketId.ToString(), typeof(int));

                string bType = "Scan DL/ID";//ViewState["ScanDocType"].ToString().Trim();
                int BookId = 0;
                //if (txtbID.Text.Length > 0)
                //{
                //    BookId = Convert.ToInt32(txtbID.Text);
                //}
                string picName;

                int docCount = Convert.ToInt32(ViewState["sSDDocCount"].ToString());
                for (int i = 0; i < fileName.Length; i++)
                {
                    string[] key = { "@updated", "@extension", "@Description", "@DocType", "@Employee", "@TicketID", "@Count", "@Book", "@BookID" };
                    object[] value1 = { DateTime.Now, "JPG", ViewState["sSDDesc"].ToString().ToUpper(), bType, empid, ticketId, docCount, BookId, "" };

                    //call sP and get the book ID back from sP
                    picName = ClsDb.InsertBySPArrRet("usp_Add_ScanImage", key, value1).ToString();
                    string bookI = picName.Split('-')[0];
                    BookId = (int)Convert.ChangeType(bookI, typeof(int));

                    docCount = docCount + 1;
                    ViewState["sSDDocCount"] = Convert.ToString(docCount);

                    //Move file
                    //picDestination = Server.MapPath("images/") + picName + ".jpg"; //DestinationImage
                    string picDestination = ViewState["vNTPATHScanImage"] + picName + ".jpg";

                    File.Copy(fileName[i], picDestination);

                    File.Delete(fileName[i]);
                    RefreshData();
                    //ocr end
                }

                dvMessage.Visible=true;lblMessage.Text = "DL/ID scanned successfully.";
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                dvMessage.Visible=true;lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
            }
        }

        protected void lbtn_OCR_Click(object sender, EventArgs e)
        {
            try
            {
                string picName = ViewState["ScanBookID"] + "-" + ViewState["docid"];//lngDocID + "-" + did;
                //picDestination= Server.MapPath("images/")+ picName + ".jpg"  ; //DestinationImage


                string picDestination = ConfigurationManager.AppSettings["NTPATHScanImage1"] + picName + ".jpg"; //DestinationImage
                clsGeneralMethods cGeneral = new clsGeneralMethods();
                string ocrData = cGeneral.OcrIt(picDestination);
                string[] key2 = { "@doc_id", "@docnum_2", "@data_3" };
                object[] value2 = { Convert.ToInt32(ViewState["ScanBookID"].ToString()), Convert.ToInt32(ViewState["docid"].ToString()), ocrData };
                //call sP and get the book ID back from sP
                ClsDb.InsertBySPArr("usp_scan_insert_tblscandata", key2, value2);
                RefreshData();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                dvMessage.Visible=true;lblMessage.Text = "Operation not completed successfully: Plz Perform this action again.";
            }

        }

        // Adding Scripts to Flags
        protected void DataL_flags_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                LinkButton btn = (LinkButton)e.Item.FindControl("lnk_flags");


                if (btn.Text == "Continuance")
                {

                    DropDownList ddlContinuancestatus = ((DropDownList)e.Item.FindControl("ddl_continuancestatus"));
                    DropDownList ddlContinuancedate = ((DropDownList)e.Item.FindControl("ddl_continuancedate"));
                    e.Item.FindControl("tbl_continuance").Visible = true;

                    //Setting Scripts For Continuance Validation                  
                    btn.Attributes.Add("onClick", "return checkContinuance('" + ((HiddenField)e.Item.FindControl("hf_ispaid")).Value + "');");
                    ddlContinuancedate.Attributes.Add("onChange", "return addContinuance('" + ddlContinuancedate.ClientID + "','" + ((HiddenField)e.Item.FindControl("hf_continuancedate")).Value + "');");

                    //Fill Status
                    // Zeeshan 4949 10/13/2008 Code uncommented.
                    FillContinuanceSatus(ddlContinuancestatus);

                    //Fill Continuance Date
                    FillContinuaneDate(ddlContinuancedate);

                    //If Continuance Not Paid
                    string isPaid = ((HiddenField)e.Item.FindControl("hf_ispaid")).Value;
                    //bool _ispaid = true;

                    if (isPaid == "False" || isPaid == "0")
                    {
                        hf_unpaid.Value = "1";
                        //_ispaid = false;

                        //Modified By Zeeshan Ahmed On 1/7/2008
                        //Added by Ozair for Continuance Modifications
                        ViewState["ddl_ContinuanceStatus"] = e.Item.ClientID;
                        ViewState["ddl_ContinuanceDate"] = e.Item.ClientID;

                    }
                    else
                    {
                        //Do Not Allow User To Change Paid Continuance Information
                        ddlContinuancedate.Enabled = false;
                        ddlContinuancestatus.Enabled = false;
                    }

                    //Select Continuance Status 
                    ddlContinuancestatus.SelectedValue = ((HiddenField)e.Item.FindControl("hf_continuancestatus")).Value;

                    //Set Continuance Date
                    string continuanceDate = ((HiddenField)e.Item.FindControl("hf_continuancedate")).Value;
                    hf_prevcontinuancedate.Value = continuanceDate;

                    if (continuanceDate != "" && continuanceDate != "1/1/1900")
                    {
                        if (ddlContinuancedate.Items.FindByText(continuanceDate) == null)
                        {

                            if (continuanceDate != "" && continuanceDate != "1/1/1900")
                            {
                                ddlContinuancedate.Items.Add(continuanceDate);
                                ddlContinuancedate.SelectedValue = continuanceDate;
                            }
                            else
                                ddlContinuancedate.SelectedIndex = 0;

                        }
                        else
                            ddlContinuancedate.SelectedValue = continuanceDate;
                    }
                    else
                    {
                        //Add Not Available Date Item
                        ddlContinuancedate.Items.Insert(0, new ListItem("N/A", "1/1/1900"));
                        ddlContinuancedate.SelectedIndex = 0;
                    }

                }
                if (btn.Text == "Outside Firm's Client")
                {

                    HtmlControl table = (HtmlControl)e.Item.FindControl("tblfirms");
                    DropDownList ddlFirm = (DropDownList)e.Item.FindControl("ddl_FirmAbbreviation");
                    Label lblFirm = (Label)e.Item.FindControl("lbl_FirmAbbreviation");

                    HtmlImage imgFirmtoggle = (HtmlImage)e.Item.FindControl("img_firmtoggle");
                    FillFirms(ddlFirm);
                    ddlFirm.SelectedValue = hf_prevfirm.Value;
                    ViewState["ddl_FirmAbbreviation"] = e.Item.ClientID;
                    table.Visible = true;

                    if (ddlFirm.Items.FindByValue(hf_prevfirm.Value) != null)
                    {
                        imgFirmtoggle.Visible = false;
                        lblFirm.Visible = false;
                        ddlFirm.Style["display"] = "block";
                        lblFirm.Style["display"] = "none";
                    }
                    else
                    {
                        imgFirmtoggle.Attributes.Add("onclick", "DisplayToggleP('" + e.Item.ClientID + "');");
                        lblFirm.Text = ClsFirms.GetFirmAbbreviation(Convert.ToInt32(hf_prevfirm.Value));
                        imgFirmtoggle.Visible = true;
                        lblFirm.Visible = true;
                        ddlFirm.Style["display"] = "none";
                        lblFirm.Style["display"] = "block";
                    }

                }
                // Code Added By Fahad To Add Followup date when Followup Flag is inserted ( Fahad - 1/4/2008 )
                if (btn.Text == "Not On System")
                {
                    HtmlControl table = (HtmlControl)e.Item.FindControl("tblnos");
                    DropDownList ddlNos = (DropDownList)e.Item.FindControl("ddl_nos");
                    nos.GetFollowUp(ddlNos, TicketId);
                    ddlNos.Enabled = false;
                    table.Visible = true;


                }
                // Haris Ahmed 10003 01/20/2012 Methods used to delete Flag when Address is verified
                if (btn.Text == "No Bad Address")
                {
                    if (img_right.Visible)
                    {
                        ResetNoBadAddressFlag(TicketId, false);

                        string[] key1 = { "@TicketID", "flagid", "empid", "@fid" };
                        object[] value1 = { TicketId, 45, cSession.GetCookie("sEmpID", Request), btn.CommandArgument };

                        ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
                    }
                }

                //Nasir 6098 08/31/2009
                if (btn.Text.Contains("Complaint"))
                {
                    btn.Attributes.Add("onClick", "return Complaint();");
                }

                //Added by kazim,adding responsible flag
                if (btn.Text == "Open Service Ticket")
                {
                    // Zahoor 4770 09/16/08
                    //    ((DropDownList)e.Item.FindControl("ddl_priority")).Visible = true;
                    e.Item.FindControl("lblPriority").Visible = true;
                    e.Item.FindControl("lblServiceTicketCategory").Visible = true;
                    Label lblContinuanceOption = (Label)e.Item.FindControl("lblContinuanceOption");
                    Label lblPriority = (Label)e.Item.FindControl("lbl_priority");

                    HiddenField hfPriority = ((HiddenField)e.Item.FindControl("hf_priority"));
                    HiddenField actPer = (HiddenField)e.Item.FindControl("hf_continuancestatus");
                    // ((Label)e.Item.FindControl("Label1")).Text = "Percentage Complete";
                    ((Label)e.Item.FindControl("lblPercentage")).Text = actPer.Value;
                    ((Label)e.Item.FindControl("lblPercentage")).Text += "%";
                    ViewState["ValidateOSTflag"] = e.Item.ClientID;

                    if (lblContinuanceOption.Text != "" && lblContinuanceOption.Text != null)
                    {
                        ((Label)e.Item.FindControl("lblContinuanceOption")).Text = "[" + lblContinuanceOption.Text + "]";
                    }

                    //copying to main page hidden field
                    //checking for previous value
                    if (Convert.ToInt32(hf_checkServiceTicket.Value) > Convert.ToInt32(actPer.Value) && Convert.ToInt32(actPer.Value) != 100)
                    {
                        hf_checkServiceTicket.Value = actPer.Value;
                    }

                    if (hfPriority.Value != "")
                        switch (int.Parse(hfPriority.Value))
                        {
                            case 0:
                                lblPriority.Text = "Low";
                                break;
                            case 1:
                                lblPriority.Text = "Medium";
                                break;
                            case 2:
                                lblPriority.Text = "High";
                                break;
                            case 3:
                                lblPriority.Text = "High";
                                break;
                            default:
                                lblPriority.Text = string.Empty;
                                break;

                        }
                    // 4770 End
                    //    ((DropDownList)e.Item.FindControl("ddl_priority")).SelectedValue = hf_pririty.Value;
                    //if (act_per.Value != "0")
                    //    ddl_Category.Enabled = false;
                    //((DropDownList)e.Item.FindControl("ddl_priority")).SelectedValue = hf_priority.Value;
                    //((DropDownList)e.Item.FindControl("ddl_priority")).Enabled = false;
                    btn.Attributes.Add("onclick", "return false;");
                    btn.Enabled = false;

                    e.Item.FindControl("img_open").Visible = true;

                    //Sabir Khan 5711 04/01/2009 Height of comment control has been changed.
                    ((Image)e.Item.FindControl("img_open")).Attributes.Add("onClick", "javascript:window.open('../Reports/GeneralCommentsPopup.aspx?ticketid=" + TicketId + "&Fid=" + btn.CommandArgument + "','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no,width=620,height=460,scrollbars=yes')");

                }

                // Agha Usman 4347 07/22/2008
                if (String.Compare(btn.Text, "Problem Client", true) == 0)
                {
                    btn.OnClientClick = "javascript:return confirm('This will delete Problem Client Flag from all the profile of this defendent. Press [OK] to continue or [CANCEL] to stop the process')";
                }
            }
        }

        // Add Continuance Flag and Calculate Price
        protected void DdlContinuancedateSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList ddlSender = (DropDownList)sender;

                if (ddlSender.SelectedValue != hf_prevcontinuancedate.Value && hf_prevcontinuancedate.Value != "")
                {


                    if (hf_updatecontinuance.Value == "1")
                    {
                        string[] keyUpdate = { "@TicketID", "@ContinuanceDate", "@ContinuanceStatus", "@EmpID" };
                        object[] valuesUpdate = { TicketId, ddlSender.SelectedValue, 1, cSession.GetCookie("sEmpID", Request) };
                        ClsDb.ExecuteSP("USP_HTS_Update_Continuance_date", keyUpdate, valuesUpdate);
                        hf_updatecontinuance.Value = "0";
                    }
                    else
                    {
                        if (ReCalculatePrice(true))
                        {
                            string[] key1 = { "TicketID", "flagID", "EmpID", "@ContinuanceDate", "@ContinuanceStatus" };
                            object[] value1 = { TicketId, 9, cSession.GetCookie("sEmpID", Request), ddlSender.SelectedValue, 0 };

                            ClsDb.ExecuteSP("USP_HTS_Insert_Continuance_flag", key1, value1);
                            hf_checkcontinuance.Value = "1";
                            hf_continuanceflag.Value = "1";
                            //Nasir 6098 08/18/2009 remove comtinue tr visibility to remove comments from contact page 


                            tbl_continuanceinfo.Visible = true;
                        }
                        else
                        {
                            dvMessage.Visible=true;lblMessage.Text = "Cannot add continuance flag to the case.";
                        }
                    }
                    FillFlagList();
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
            }

        }

        //Waqas 5771 04/14/2009
        protected void lnkContactID_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnContactID.Value))
            {
                ContactInfo1.TicketID = TicketId;
                ContactInfo1.BindContactInfo(hdnContactID.Value, MPContactID.ClientID, 1, Convert.ToInt32(hf_isactive.Value));
                MPContactID.Show();
            }
            else
            {
                string lastName = txt_lname.Text;
                //string FirstInitial = txt_fname.Text.Substring(0, 1);
                string firstName = txt_fname.Text;
                string dob = txt_dob_m.Text + "/" + txt_dob_d.Text + "/" + txt_dob_y.Text;
                ContactLookUp.TicketID = TicketId;
                ContactLookUp.BindContactLookUpGrid(lastName, firstName, dob, MPContactLookUp.ClientID);
                MPContactLookUp.Show();
            }
        }

        //Waqas 5771 04/14/2009
        protected void btnCIDCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                clsContact cContact = new clsContact();
                int empid;
                int.TryParse(cSession.GetCookie("sEmpID", Request), out empid);
                cContact.UpdateCIDConfirmationFlag(TicketId, 1, empid);
                GetClientInformation(ClsCase, TicketId);
            }
            catch (Exception ex)
            {
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Waqas 5771 04/14/2009
        protected void btnCIDInCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                clsContact cContact = new clsContact();
                int empid;
                int.TryParse(cSession.GetCookie("sEmpID", Request), out empid);
                cContact.UpdateCIDConfirmationFlag(TicketId, 0, empid);
                GetClientInformation(ClsCase, TicketId);
                dvMessage.Visible=true;lblMessage.Text = "This client�s CID has been disassociated. Please select a new CID for this client.";
            }
            catch (Exception ex)
            {
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        protected void gvConsultationHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Rab Nawaz Khan 11473 10/31/2013 commented the code for Add new leads. . . 
                ////Fahad 6766 03/15/2010 Polm Web Site Address coming from Configuration Service.
                //((HyperLink)e.Row.FindControl("lnkSno")).NavigateUrl = "http://" +
                //   _objConfigurationKeys.WebisteAddress
                //    + "/view/EditPolm.aspx?id=" + Encrypt.CLRClass.EncryptFunction(((HiddenField)e.Row.FindControl("hf_polmId")).Value) + "&hview=1";
                if (((Label)e.Row.FindControl("lblCallerId")).Text.Trim().Length > 10)
                {
                    ((Label)e.Row.FindControl("lblCallerId")).ToolTip = ((Label)e.Row.FindControl("lblCallerId")).Text.Trim();
                    ((Label)e.Row.FindControl("lblCallerId")).Text = ((Label)e.Row.FindControl("lblCallerId")).Text.Trim().Substring(0, 7) + "...";
                }

                if (((Label)e.Row.FindControl("lblPersonName")).Text.Trim().Length > 20)
                {
                    ((Label)e.Row.FindControl("lblPersonName")).ToolTip = ((Label)e.Row.FindControl("lblPersonName")).Text.Trim();
                    ((Label)e.Row.FindControl("lblPersonName")).Text = ((Label)e.Row.FindControl("lblPersonName")).Text.Trim().Substring(0, 17) + "...";
                }

                if (((Label)e.Row.FindControl("lblEmail")).Text.Trim().Length > 25)
                {
                    ((Label)e.Row.FindControl("lblEmail")).ToolTip = ((Label)e.Row.FindControl("lblEmail")).Text.Trim();
                    ((Label)e.Row.FindControl("lblEmail")).Text = ((Label)e.Row.FindControl("lblEmail")).Text.Trim().Substring(0, 22) + "...";
                }

                if (((Label)e.Row.FindControl("lblPhone")).Text.Trim().Length > 13)
                {
                    ((Label)e.Row.FindControl("lblPhone")).ToolTip = ((Label)e.Row.FindControl("lblPhone")).Text.Trim();
                    ((Label)e.Row.FindControl("lblPhone")).Text = ((Label)e.Row.FindControl("lblPhone")).Text.Trim().Substring(0, 10) + "...";
                }

                if (((Label)e.Row.FindControl("lblPracticeArea")).Text.Trim().Length > 25)
                {
                    ((Label)e.Row.FindControl("lblPracticeArea")).ToolTip = ((Label)e.Row.FindControl("lblPracticeArea")).Text.Trim();
                    ((Label)e.Row.FindControl("lblPracticeArea")).Text = ((Label)e.Row.FindControl("lblPracticeArea")).Text.Trim().Substring(0, 22) + "...";
                }

                if (((Label)e.Row.FindControl("lblRepname")).Text.Trim().Length > 12)
                {
                    ((Label)e.Row.FindControl("lblRepname")).ToolTip = ((Label)e.Row.FindControl("lblRepname")).Text.Trim();
                    ((Label)e.Row.FindControl("lblRepname")).Text = ((Label)e.Row.FindControl("lblRepname")).Text.Trim().Substring(0, 9) + "...";
                }
            }
        }

        //Ozair 7783 05/12/2010 POLM Consultation close button event
        protected void lbtn_close2_Click(object sender, EventArgs e)
        {
            //Hiding Popup
            mpeShowPolm.Hide();
            //Reloading page again to populate data properly
            Response.Redirect(Request.Url.AbsoluteUri, false);
        }

        // Farrukh 9332 07/28/2011 adding event with datalist
        protected void gv_Activites_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnkActivity = ((HyperLink)(e.Row.FindControl("lnk_Activitity")));
                Label lblTicketId = ((Label)(e.Row.FindControl("lbl_TicketID")));
                Label lblTicketViolationId = ((Label)(e.Row.FindControl("lbl_TicketViolationID")));
                Label lblCourtDate = ((Label)(e.Row.FindControl("lbl_CourtDate")));
                Label lblRowId = ((Label)(e.Row.FindControl("lbl_RowID")));
                Label lblEventTypeId = ((Label)(e.Row.FindControl("lbl_EventTypeID")));
                _recId++;
                lblRowId.Text = _recId.ToString();

                if (lblEventTypeId.Text == "2")
                    lblEventTypeId.Text = "0"; //for ReminderCall

                lnkActivity.NavigateUrl =
                    "javascript:window.open('../QuickEntryNew/remindernotes.aspx?casenumber=" +
                    lblTicketId.Text + "&violationID=" +
                    lblTicketViolationId.Text + "&searchdate=" +
                    lblCourtDate.Text +
                    "&calltype=" + lblEventTypeId.Text + "&Recid=" + lblRowId.Text +
                    "','','status=yes,width=575,height=470,scrollbars=1');void('');";

            }
        }

        // Farrukh 9332 09/07/2011 paging apply
        protected void gv_Activites_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Activites.PageIndex = e.NewPageIndex;
                FillActivitesGrid();
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
                dvMessage.Visible=true;lblMessage.Text = ex.ToString();
            }
        }

        #endregion

        #region Methods


        //Updates Client Info

        private void UpdateClientInfo()
        {
            try
            {
                // tahir 5325 12/18/2008 commented the code as it was not updating the notes 
                // specifically for multiuser issue...

                //Sabir Khan 5009 11/05/2008 Check for page refresh ...
                //string strReq = Request.Form["txtTimeStamp"].ToString();
                //if (Session["TimeStamp"] != null && Session["TimeStamp"].ToString() == strReq.ToString())
                //{

                //Kazim 4258 6/23/2008 Check valid session before excecuting this method 

                //Waqas 5057 03/17/2009 Checking employee info in session
                if (cSession.IsValidSession(Request, Response, Session))
                {
                    //Added by Azee for Walk In Client Flag Modification.
                    // Sabir Khan 11509 12/18/2013 remove walk in flag
                    //if (rdbtn_Yes.Checked && rdbtn_No.Checked == false)
                    //{
                    //    ClsCase.WalkInClientFlag = "1";
                    //}
                    //if (rdbtn_No.Checked && rdbtn_Yes.Checked == false)
                    //{
                    //    ClsCase.WalkInClientFlag = "0";
                    //}
                    //if (rdbtn_No.Checked == false && rdbtn_Yes.Checked == false)
                    //{
                    //    ClsCase.WalkInClientFlag = String.Empty;
                    //}
                    //CodeAddedByAzee
                    int chkNolicense;
                    if (chk_NoLicense.Checked)
                    {
                        chkNolicense = 1;
                    }
                    else
                    {
                        chkNolicense = 0;
                    }

                    if (rbtn_PR_No.Checked)
                        ClsCase.IsPR = 0;
                    else if (rbtn_PR_Yes.Checked)
                        ClsCase.IsPR = 1;
                    else if (rbtn_PR_NA.Checked)
                        ClsCase.IsPR = 3; // NA Selected


                    if (rbtn_FreeCon_Yes.Checked)
                    {
                        ClsCase.HasConsultationComments = 1;
                    }
                    // Noufil 5000 10/24/2008 Viewstate added to check the last consultation selection
                    else if (rbtn_FreeCon_No.Checked && Convert.ToInt32(ViewState["hasConsulation"]) == 1)
                    {
                        ClsCase.HasConsultationComments = 0;
                        // Noufil 6766 COde comment because there is no need to consulation comments now   
                        //WCC_ConsultationComments.DeleteComments();
                        //WCC_ConsultationComments.Initialize(this.TicketId, Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request)), 7, "Label", "clsinputadministration", "clsbutton");
                    }
                    //Sabir Khan 5201 11/24/2008 Set no ConsultationComments for first time...
                    else if (rbtn_FreeCon_No.Checked)
                    {
                        ClsCase.HasConsultationComments = 0;
                    }
                    ClsCase.TicketID = TicketId;
                    ClsCase.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", Request));
                    ClsCase.FirstName = txt_fname.Text.ToUpper();
                    ClsCase.MiddleName = txt_mname.Text.ToUpper();
                    ClsCase.LastName = txt_lname.Text.ToUpper();
                    ClsCase.NoDL = chkNolicense;
                    ClsCase.DOB = txt_dob_m.Text + "/" + txt_dob_d.Text + "/" + txt_dob_y.Text;
                    ClsCase.DLNumber = txt_dlstr.Text;
                    ClsCase.DLStateID = Convert.ToInt32(ddl_dlState.SelectedValue);
                    //Mohammad Ali 9996 01/12/2012 Assign SNN value to the property 
                    //Hafiz 10288 07/12/2012
                    if (hf_casetype.Value != "2")
                        ClsCase.SSN = txt_SSN.Text;

                    ClsCase.Gender = ddl_gender.SelectedValue;
                    ClsCase.Race = txt_race.Text.ToUpper();
                    ClsCase.EmailNotAvailable = chkEmail.Checked;
                    ClsCase.Height = txt_heightft.Text + "ft" + txt_heightin.Text;
                    ClsCase.Weight = txt_weight.Text;
                    ClsCase.HairColor = ddl_haircol.SelectedValue;
                    ClsCase.EyeColor = ddl_eyecol.SelectedValue;
                    ClsCase.Address1 = txt_add1.Text.ToUpper();

                    //khalid 24-9-07
                    if (txt_spn.Text.Length != 0)
                    {
                        ClsCase.SPN = txt_spn.Text;
                    }
                    else
                    {
                        ClsCase.SPN = "''";
                    }

                    //if (txt_add2.Text.ToUpper().IndexOf("APT#") == -1 && txt_aparmentno.Text.Length > 0)
                    //    ClsCase.Address2 = txt_add2.Text.ToUpper() + " Apt#" + txt_aparmentno.Text.ToString();
                    //else
                    ClsCase.Address2 = txt_add2.Text.ToUpper();


                    ClsCase.City = txt_city.Text.ToUpper();
                    ClsCase.StateID = ddl_state.SelectedIndex;
                    ClsCase.Zip = txt_zip.Text.ToUpper();
                    ClsCase.ContactType1 = Convert.ToInt32(ddl_contact1.SelectedValue);
                    ClsCase.ContactType2 = Convert.ToInt32(ddl_contact2.SelectedValue);
                    ClsCase.ContactType3 = Convert.ToInt32(ddl_contact3.SelectedValue);
                    ClsCase.Contact1 = txt_cc11.Text + txt_cc12.Text + txt_cc13.Text + txt_cc14.Text;
                    ClsCase.Contact2 = txt_cc21.Text + txt_cc22.Text + txt_cc23.Text + txt_cc24.Text;
                    ClsCase.Contact3 = txt_cc31.Text + txt_cc32.Text + txt_cc33.Text + txt_cc34.Text;

                    // Noufil 5884 07/02/2009 Set SMS FLag property.
                    ClsCase.SmsFlag1 = Convert.ToInt32(chk_SmsRequired1.Checked);
                    ClsCase.SmsFlag2 = Convert.ToInt32(chk_SmsRequired2.Checked);
                    ClsCase.SmsFlag3 = Convert.ToInt32(chk_SmsRequired3.Checked);

                    ClsCase.Email = txt_email.Text;
                    //Nasir 5381 01/15/2009 set property of LastGerenalcommentsUpdatedate
                    DateTime result;
                    //7832 Muhammad Muneer 09/04/2010 checking hiddenfield value of datetime and setting '01/01/1900' if not
                    ClsCase.LastGerenalcommentsUpdatedate = DateTime.TryParse((hf_LastGerenalcommentsUpdatedate.Value), out result) ? Convert.ToDateTime(hf_LastGerenalcommentsUpdatedate.Value) : Convert.ToDateTime("1/1/1900");

                    ClsCase.AddressConfirmFlag = Convert.ToInt32(chkb_addresscheck.Checked);

                    //  Get Firm Information and Continuance Information from the Flag List
                    int continuanceStatus = 0;
                    string continuanceDate = "1/1/1900";

                    //Added By Zeeshan Ahmed
                    //Get Only Last Continuace Flag
                    bool isFirstContinuance = true;

                    foreach (DataListItem itm in DataL_flags.Items)
                    {
                        LinkButton btn = (LinkButton)itm.FindControl("lnk_flags");

                        if (btn.Text == "Continuance")
                        {
                            if (isFirstContinuance)
                            {
                                continuanceDate = ((DropDownList)itm.FindControl("ddl_continuancedate")).SelectedValue;
                                continuanceStatus = Convert.ToInt32(((DropDownList)itm.FindControl("ddl_continuancestatus")).SelectedValue);
                                isFirstContinuance = false;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(continuanceDate))
                        continuanceDate = "1/1/1900";

                    //Set Firm ID
                    ClsCase.FirmID = Convert.ToInt32(hf_prevfirm.Value);

                    //Set Continuance Information
                    ClsCase.ContinuanceStatus = continuanceStatus;
                    ClsCase.ContinuanceDate = Convert.ToDateTime(continuanceDate);
                    ClsCase.ContinuanceFlag = Convert.ToBoolean(Convert.ToInt32(hf_continuanceflag.Value));

                    //Call zp4 in order to verify Address
                    ClsCase.YDS = "N";

                    ddl_state.SelectedValue = ClsCase.StateID.ToString();

                    string statename = ddl_state.Items[ddl_state.SelectedIndex].Text;
                    if (ConfigurationManager.AppSettings["ProcessYDS"].ToUpper().Equals("TRUE"))
                    {
                        Hashtable htYds = ProcessYDS.ZP4YDS(ClsCase.Address1, ClsCase.City, statename, ClsCase.Zip);
                        if (htYds["YDSSUCCESS"].ToString() == "true" && htYds["YDS"].ToString() != "N")
                        {
                            ClsCase.Zip = htYds["YDSZIP"].ToString();
                            ClsCase.City = htYds["YDSCITY"].ToString();
                            ClsCase.Address1 = htYds["YDSADDRESS"].ToString();
                            ClsCase.YDS = htYds["YDS"].ToString();

                            if (txt_add2.Text.Length <= 0 || txt_add2.Text == "")
                            {
                                if (htYds["YDSAPPARTMENT"].ToString() == "Apt")  // ADDED BY Azee for apartment number modification.. 
                                {
                                    chk_aparmentno.Checked = true;
                                    flag = true;
                                }
                                else
                                {
                                    chk_aparmentno.Checked = false;
                                    flag = false;
                                }
                            }

                            barcode = htYds["YDSBARCODE"].ToString();
                            if (barcode.Length >= 12)
                            {
                                barcode = barcode.Remove(0, 9);
                                _dp2 = barcode.Substring(0, 2);
                                _dpc = barcode.Substring(2);
                                ClsCase.DP2 = _dp2;
                                ClsCase.DPC = _dpc;
                                ClsCase.FLAG1 = htYds["YDS"].ToString();
                                ClsCase.UpdateBarcodeInfo = true;
                            }

                        }
                    }
                    else
                    {
                        ClsCase.Zip = txt_zip.Text;
                        ClsCase.City = txt_city.Text;
                        ClsCase.Address1 = txt_add1.Text;
                        ClsCase.UpdateBarcodeInfo = false;
                    }

                    if (flag == false)
                    {
                        //Nasir 6098 08/18/2009 remove save comment calling to remove comments from contact page 
                        if (SaveComments() && ClsCase.UpdateClientInfo())
                        {
                            //Waqas 5771 04/20/2009
                            AssignContactValues();
                            ClsContact.MidNumber = hlnk_MidNo.Text;
                            ClsContact.UpdateContactInfoToContact();

                            // Noufil 5884 07/29/2009 Add note to hisotry

                            if ((Convert.ToBoolean(ViewState["chk_SmsRequired1"]) != chk_SmsRequired1.Checked))
                            {
                                if ((Convert.ToBoolean(ViewState["chk_SmsRequired1"]) && !chk_SmsRequired1.Checked))
                                {
                                    clog.AddNote(this.EmpId, "SMS Alert flag for " + ClsCase.Contact1 + " set to NO", "", this.TicketId);
                                }
                                else if (!Convert.ToBoolean(ViewState["chk_SmsRequired1"]) && chk_SmsRequired1.Checked)
                                {
                                    clog.AddNote(this.EmpId, "SMS Alert flag for " + ClsCase.Contact1 + " set to YES", "", this.TicketId);
                                }
                            }

                            if ((Convert.ToBoolean(ViewState["chk_SmsRequired2"]) != chk_SmsRequired2.Checked))
                            {
                                if ((Convert.ToBoolean(ViewState["chk_SmsRequired2"]) && !chk_SmsRequired2.Checked))
                                {
                                    clog.AddNote(EmpId, "SMS Alert flag for " + ClsCase.Contact2 + " set to NO", "", TicketId);
                                }
                                else if (!Convert.ToBoolean(ViewState["chk_SmsRequired2"]) && chk_SmsRequired2.Checked)
                                {
                                    clog.AddNote(EmpId, "SMS Alert flag for " + ClsCase.Contact2 + " set to YES", "", TicketId);
                                }
                            }

                            if ((Convert.ToBoolean(ViewState["chk_SmsRequired3"]) != chk_SmsRequired3.Checked))
                            {
                                if ((Convert.ToBoolean(ViewState["chk_SmsRequired3"]) && !chk_SmsRequired3.Checked))
                                {
                                    clog.AddNote(EmpId, "SMS Alert flag for " + ClsCase.Contact3 + " set to NO", "", TicketId);
                                }
                                else if (!Convert.ToBoolean(ViewState["chk_SmsRequired3"]) && chk_SmsRequired3.Checked)
                                {
                                    clog.AddNote(EmpId, "SMS Alert flag for " + ClsCase.Contact3 + " set to YES", "", TicketId);
                                }
                            }

                            //SaveConsultationComments(ClsCase.TicketID);
                            GetClientInformation(ClsCase, ClsCase.TicketID);
                            TicketId = ClsCase.TicketID;
                            dvMessage.Visible=false;lblMessage.Text = "";
                        }
                        else
                        {
                            dvMessage.Visible=true;lblMessage.Text = "Information not updated in DB";
                        }
                    }
                    else
                    {
                        dvMessage.Visible=true;lblMessage.Text = "This is an Apartment address. Please make sure that you get the Apartment number from the client.";
                    }
                }
                //}
                //else
                //{
                //    GetClientInformation(ClsCase, this.TicketId);
                //}
            }
            catch (Exception eUpdateinfo)
            {
                clog.ErrorLog(eUpdateinfo.Message, eUpdateinfo.Source, eUpdateinfo.TargetSite.ToString(), eUpdateinfo.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = eUpdateinfo.Message;
            }

        }

        //Waqas 5771 04/14/2009 
        /// <summary>
        /// This method is used to assign contact informations before updating contact information.
        /// </summary>
        private void AssignContactValues()
        {

            if (hdnContactID.Value != null && hdnContactID.Value != string.Empty)
            {
                ClsContact.ContactID = Convert.ToInt32(hdnContactID.Value);
                ClsContact.DLNumber = txt_dlstr.Text;
                ClsContact.DLStateID = Convert.ToInt32(ddl_dlState.SelectedValue);
                ClsContact.Gender = ddl_gender.SelectedValue;
                ClsContact.Race = txt_race.Text.ToUpper();
                ClsContact.Height = txt_heightft.Text + "ft" + txt_heightin.Text;
                ClsContact.Weight = txt_weight.Text;
                ClsContact.HairColor = ddl_haircol.SelectedValue;
                ClsContact.Eyes = ddl_eyecol.SelectedValue;
                ClsContact.Address1 = txt_add1.Text.ToUpper();
                ClsContact.Address2 = txt_add2.Text.ToUpper();

                ClsContact.City = txt_city.Text.ToUpper();
                ClsContact.StateID_FK = ddl_state.SelectedIndex;
                ClsContact.Zip = txt_zip.Text.ToUpper();
                ClsContact.PhoneTypeID1 = Convert.ToInt32(ddl_contact1.SelectedValue);
                ClsContact.PhoneTypeID2 = Convert.ToInt32(ddl_contact2.SelectedValue);
                ClsContact.PhoneTypeID3 = Convert.ToInt32(ddl_contact3.SelectedValue);
                ClsContact.Phone1 = txt_cc11.Text + txt_cc12.Text + txt_cc13.Text + txt_cc14.Text;
                ClsContact.Phone2 = txt_cc21.Text + txt_cc22.Text + txt_cc23.Text + txt_cc24.Text;
                ClsContact.Phone3 = txt_cc31.Text + txt_cc32.Text + txt_cc33.Text + txt_cc34.Text;
                ClsContact.Email = txt_email.Text;
                ClsContact.MiddleName = txt_mname.Text.ToUpper();
                ClsContact.MidNumber = hlnk_MidNo.Text;
                if (chk_NoLicense.Checked)
                {
                    ClsContact.NoDL = 1;
                }
                else
                {
                    ClsContact.NoDL = 0;
                }



            }

        }

        //Nasir 6098 08/18/2009 modified Save comments  method remove comments from contact page 
        private bool SaveComments()
        {
            bool success = true;
            try
            {
                if (rbtn_FreeCon_Yes.Checked)
                {
                    // Noufil 6766 COde comment because there is no need to consulation comments now   
                    //WCC_ConsultationComments.AddComments();
                    // added by tahir ahmed dt: 01/19/08 02:18 AM
                    // requried for task 2655
                    //if (!hf_lastConsultationComments.Value.Equals(WCC_ConsultationComments.Label_Text + WCC_ConsultationComments.TextBox_Text ))
                    //if (!ViewState["ConsultationComments"].ToString().Equals(WCC_ConsultationComments.Label_Text + WCC_ConsultationComments.TextBox_Text))
                    //    EMailConsultationComments(Convert.ToString(WCC_ConsultationComments.Label_Text) + Convert.ToString(WCC_ConsultationComments.TextBox_Text));
                }
            }
            catch
            {
                success = false;
            }
            return success;
        }

        private string[] sortfilesbydate(string[] fileName)
        {
            for (int i = 0; i < fileName.Length; i++)
            {
                DateTime cDateTime = File.GetCreationTime(fileName[i]);
                for (int j = i + 1; j < fileName.Length; j++)
                {
                    DateTime cDateTime1 = File.GetCreationTime(fileName[j]);
                    if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                    {
                        string fname = fileName[j];
                        fileName[j] = fileName[i];
                        fileName[i] = fname;
                        i = -1;
                        break;
                    }
                }
            }
            return fileName;
        }

        private void RefreshData()
        {
            //Code added by Farhan
            string[] keys = { "@TicketID" };
            object[] values = { TicketId.ToString() };
            DataTable dtTemp;
            dtTemp = ClsDb.Get_DT_BySPArr("usp_HTS_Get_Licence_OCR_Data", keys, values);

            if (Convert.ToInt32(dtTemp.Rows[0]["ReturnCode"].ToString()) == 0)
            {
                lbtn_Scan.Visible = true;
                lbtn_ViewOCR.Visible = false;
                lbtn_OCR.Visible = false;
            }

            //OCR disabled by Farhan (modifications 1/19/2007 )
            /*
            if (Convert.ToInt32(dtTemp.Rows[0]["ReturnCode"].ToString()) == 1)
            {
                lbtn_Scan.Visible = false;
                lbtn_ViewOCR.Visible = true;
                lbtn_OCR.Visible = false;
                ViewState["doc_id"] = dtTemp.Rows[0]["doc_id"].ToString();
                ViewState["docnum"] = dtTemp.Rows[0]["docnum"].ToString();
                ViewState["ResetDesc"] = dtTemp.Rows[0]["ResetDesc"].ToString();
                ViewState["DocExt"] = dtTemp.Rows[0]["DocExtension"].ToString();
                lbtn_ViewOCR.Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + ViewState["doc_id"].ToString() + ",1,0" + ",'" + ViewState["DocExt"].ToString() + "'" + ");");                 
            }
            if (Convert.ToInt32(dtTemp.Rows[0]["ReturnCode"].ToString()) == 2)
            {
                lbtn_Scan.Visible = false;
                lbtn_ViewOCR.Visible = false;
                lbtn_OCR.Visible = true;
                ViewState["docid"] = dtTemp.Rows[0]["docid"].ToString();
                ViewState["ScanBookID"] = dtTemp.Rows[0]["ScanBookID"].ToString();
                ViewState["DocExt"] = dtTemp.Rows[0]["DocExtension"].ToString();
            }*/


            if (Convert.ToInt32(dtTemp.Rows[0]["ReturnCode"].ToString()) == 1)
            {
                lbtn_Scan.Visible = false;
                lbtn_ViewOCR.Visible = true;
                lbtn_OCR.Visible = false;
                ViewState["doc_id"] = dtTemp.Rows[0]["doc_id"].ToString();
                ViewState["docnum"] = dtTemp.Rows[0]["docnum"].ToString();
                ViewState["ResetDesc"] = dtTemp.Rows[0]["ResetDesc"].ToString();
                ViewState["DocExt"] = dtTemp.Rows[0]["DocExtension"].ToString();
                lbtn_ViewOCR.Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + ViewState["doc_id"] + ",1,0" + ",'" + ViewState["DocExt"] + "'" + ");");
            }
            if (Convert.ToInt32(dtTemp.Rows[0]["ReturnCode"].ToString()) == 2)
            {
                lbtn_Scan.Visible = false;
                lbtn_ViewOCR.Visible = true;
                lbtn_OCR.Visible = false;
                ViewState["docid"] = dtTemp.Rows[0]["docid"].ToString();
                ViewState["ScanBookID"] = dtTemp.Rows[0]["ScanBookID"].ToString();
                ViewState["DocExt"] = dtTemp.Rows[0]["DocExtension"].ToString();
                lbtn_ViewOCR.Attributes.Add("OnClick", "javascript:return PopUpShowPreviewPDF(" + ViewState["ScanBookID"] + ",1,0" + ",'" + ViewState["DocExt"] + "'" + ");");
            }
            // End

        }

        //fills Firm Abbreviation drop drop down list
        private void FillFirms(DropDownList ddl_FirmAbbreviation)
        {
            try
            {
                DataSet ds_Firms = ClsFirms.GetActiveFirms(0);
                ddl_FirmAbbreviation.DataSource = ds_Firms.Tables[0];
                ddl_FirmAbbreviation.DataTextField = "FirmAbbreviation";
                ddl_FirmAbbreviation.DataValueField = "FirmID";
                ddl_FirmAbbreviation.DataBind();
                ddl_FirmAbbreviation.SelectedValue = "3000";
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
            }
        }

        //fills Service Ticket Categories drop down list
        //private void FillServiceTicketCategories(DropDownList ddl_ServiceTicketCategories)
        //{
        //    DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_GET_SERVICETICKET_CATEGORIES");
        //    ddl_ServiceTicketCategories.Items.Add(new ListItem("--Choose--", "0"));
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        ddl_ServiceTicketCategories.Items.Add(new ListItem(Convert.ToString(dr["Description"]), Convert.ToString(dr["ID"])));
        //    }
        //}

        // Fill Continous Statuses 
        private void FillContinuanceSatus(DropDownList ddl_continuancestatus)
        {
            try
            {
                DataSet ds = ClsDb.Get_DS_BySP("USP_HTS_GetContinousStatus");
                ddl_continuancestatus.DataSource = ds.Tables[0];
                ddl_continuancestatus.DataValueField = "StatusID";
                ddl_continuancestatus.DataTextField = "Description";
                ddl_continuancestatus.DataBind();
                //ddl_continuancestatus.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (Exception ex)
            {
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
            }
        }

        // Fill Continuance Status

        private void FillContinuaneDate(DropDownList ddl_continuancedate)
        {
            DataSet ds = Session["caseData"] as DataSet;
            DataSet ds_CaseDetail;
            if (ds == null)
                ds_CaseDetail = ClsCaseDetail.GetCaseDetail(this.TicketId);
            else
                ds_CaseDetail = ds;
            // Finding Earlient CourtDate
            DateTime tempdate = DateTime.Parse("1/1/1900");
            ddl_continuancedate.Items.Clear();

            foreach (DataRow dr in ds_CaseDetail.Tables[0].Rows)
            {
                DateTime dt = (DateTime)dr["CourtDateMain"];
                //commented by Ozair
                //string status = dr["vercourtdesc"].ToString();
                string status = dr["ulstatus"].ToString();
                //commented by Ozair
                //if (dt > DateTime.Now && (status == "PRE" || status == "JUR" || status == "JUD"))
                if (dt > DateTime.Now && (status == "3" || status == "4" || status == "5"))
                {
                    if (tempdate.ToShortDateString() != "1/1/1900")
                    {
                        if (tempdate > dt)
                        {
                            tempdate = dt;
                            ddl_continuancedate.Items.Add(dt.ToShortDateString());
                        }
                        else if (tempdate.ToShortDateString() != dt.ToShortDateString())
                        {

                            if (ddl_continuancedate.Items.FindByValue(dt.ToShortDateString()) == null)
                                ddl_continuancedate.Items.Add(dt.ToShortDateString());
                        }
                    }
                    else
                    {
                        tempdate = dt;
                        ddl_continuancedate.Items.Add(dt.ToShortDateString());
                    }
                }
                else if (dt.ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    tempdate = dt;
                }
            }

            if (ddl_continuancedate.Items.Count > 0)
            {
                //ddl_continuancedate.Items.Insert(0, new ListItem("--Select--", "0"));


                try
                {
                    ddl_continuancedate.SelectedValue = tempdate.ToShortDateString();
                }
                catch
                { }

            }

        }

        // Fill Tickets Flag List
        private void FillFlagList()
        {

            try
            {
                string[] key1 = { "@TicketID", "@Includedispose" };
                object[] value1 = { TicketId, 0 };
                // Rab Nawaz Khan 01/18/2015 Added below SP to fill the Flages with one DB call. . .
                DataSet flagsInfo = ClsDb.Get_DS_BySPArr("USP_HTS_FillFlagList", key1, value1);
                // Set Default Value For Open Service Ticket
                hf_checkServiceTicket.Value = "101";
                //Display Flags
                if (flagsInfo != null && flagsInfo.Tables.Count > 0)
                {
                    bool status = false;
                    if (flagsInfo.Tables[2] != null)
                    {
                        DataL_flags.DataSource = flagsInfo.Tables[2];
                        DataL_flags.DataBind();
                    }
                    if (flagsInfo.Tables[1] != null)
                    {
                        if (flagsInfo.Tables[1].Rows[0] != null)
                        {
                            status = Convert.ToInt32(flagsInfo.Tables[1].Rows[0]["isfound"].ToString()) == 1 ? true : false;
                        }
                    }
                    ddl_flags.Items.Clear();
                    if (flagsInfo.Tables[0] != null)
                    {
                        for (int i = 0; i < flagsInfo.Tables[0].Rows.Count; i++)
                        {
                            string svalue = flagsInfo.Tables[0].Rows[i]["flagid_pk"].ToString().Trim();
                            string stext = flagsInfo.Tables[0].Rows[i]["Description"].ToString().Trim();

                            if (!((stext == "ALR Request Confirmation") && status == false))
                            {
                                ddl_flags.Items.Add(new ListItem(stext, svalue));
                            }

                            // Haris Ahmed 10003 03/02/2012 No Bad Address Flag removed when address flag is green
                            if (img_right.Visible)
                            {
                                ddl_flags.Items.Remove(ddl_flags.Items.FindByValue("45"));
                            }
                        }
                    }
                    if (hf_casetype.Value != "2")
                        ddl_flags.Items.Remove(new ListItem("In Custody (Jail)", "38"));

                    if (flagsInfo.Tables[3] != null)
                    {
                        //DataSet dsPreTrialDiversion = ClsCaseDetail.GetCaseDetail(TicketId);
                        bool isValidforPreTrialDiversion = false;
                        bool isValidforMotionHiring = false;
                        //Sabir Khan 8862 02/15/2011 Validation for JUVNILE Flag...
                        bool isJuvnile = false;
                        foreach (DataRow dr in flagsInfo.Tables[3].Rows)
                        {
                            if ((dr["CaseTypeID"].ToString() == "1") && (dr["CourtID"].ToString() != "3001" && dr["CourtID"].ToString() != "3002" && dr["CourtID"].ToString() != "3003") && (dr["CourtViolationStatusIDmain"].ToString() == "101"))
                            {
                                isValidforPreTrialDiversion = true;
                                isValidforMotionHiring = true;
                            }
                            if (dr["CaseTypeID"].ToString() == "1" && (dr["crtCategory"].ToString() == "2") && ((dr["CourtViolationStatusIDmain"].ToString() == "139") || (dr["CourtViolationStatusIDmain"].ToString() == "256") || (dr["CourtViolationStatusIDmain"].ToString() == "257") || (dr["CourtViolationStatusIDmain"].ToString() == "258") || (dr["CourtViolationStatusIDmain"].ToString() == "259")))
                            {
                                isJuvnile = true;
                            }
                        }
                        if (!isValidforPreTrialDiversion)
                            ddl_flags.Items.Remove(ddl_flags.Items.FindByValue("42"));
                        if (!isValidforMotionHiring)
                            ddl_flags.Items.Remove(ddl_flags.Items.FindByValue("43"));
                        if (!isJuvnile)
                        {
                            ddl_flags.Items.Remove(ddl_flags.Items.FindByValue("44"));
                        }
                    }
                    if (flagsInfo.Tables[4] != null) // Checking No Calls Flag and No Letter Flag
                    {
                        if (flagsInfo.Tables[4].Rows[0] != null)
                        {
                            if (Convert.ToInt32(flagsInfo.Tables[4].Rows[0]["NoCall"].ToString()) > 0)
                                lbl_nocalls.Text = "No Calls";
                            else
                                lbl_nocalls.Text = "";

                            if (Convert.ToInt32(flagsInfo.Tables[4].Rows[0]["NoLetter"].ToString()) > 0)
                                lbl_NoLetters.Text = "No Letters";
                            else
                                lbl_NoLetters.Text = "";

                            //Nasir 6098 09/01/2009 remove complaint flag
                            ddl_flags.Items.Remove(ddl_flags.Items.FindByValue("37"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
            }

        }

        //  Recalculate Case Price 
        private bool ReCalculatePrice(bool contFlag)
        {

            try
            {
                clsCaseDetail clscasedetail = new clsCaseDetail();
                DataSet dsCase = ClsCase.GetCase(TicketId);
                ClsCase.LateFee = Convert.ToInt32(dsCase.Tables[0].Rows[0]["LateFlag"]);
                DataSet dsCaseDetail = clscasedetail.GetCaseDetail(TicketId);
                // Rab Nawaz Khan 10330 07/16/2012 DayOfArr Flage added in the methoed parameters. . . 
                return Convert.ToBoolean(ClsCase.CalculateFee(TicketId, true, dsCaseDetail.Tables[0].Rows.Count, Convert.ToBoolean(dsCase.Tables[0].Rows[0]["AccidentFlag"]), Convert.ToBoolean(dsCase.Tables[0].Rows[0]["CDLFlag"]), Convert.ToInt32(dsCase.Tables[0].Rows[0]["SpeedingID"]), contFlag, Convert.ToDouble(dsCase.Tables[0].Rows[0]["InitialAdjustment"]), Convert.ToDouble(dsCase.Tables[0].Rows[0]["Adjustment"]), false, ClsCase.EmpID, true, ClsCase.HasPaymentPlan, ClsCase.DayOfArrFlag)); // tahir 4786 10/08/2008
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
                return false;
            }
        }

        private DateTime GetFutureCourtDate(int casenumber, DataSet dsCaseDetail)
        {
            // Added By Zeeshan Ahmed for Continuance Date
            //clsCaseDetail clscasedetail = new clsCaseDetail();
            //DataSet dsCaseDetail = clscasedetail.GetCaseDetail(casenumber);
            // Finding Earlient CourtDate
            DateTime tempdate = DateTime.Parse("1/1/1900");

            foreach (DataRow dr in dsCaseDetail.Tables[0].Rows)
            {
                //Added By Ozair
                //Continuance can only be added in these statuses
                if (hf_Status.Value == "0")
                {
                    if (dr["ulstatus"].ToString() == "3" || dr["ulstatus"].ToString() == "4" || dr["ulstatus"].ToString() == "5")
                    {
                        hf_Status.Value = "1";
                    }
                    else
                    {
                        hf_Status.Value = "0";
                    }
                }
                // End
                DateTime dt = (DateTime)dr["CourtDateMain"];
                //Modified By Ozair
                DateTime dt2;
                try
                {
                    dt2 = (DateTime)dr["violationdate"];
                }
                catch (Exception)
                {
                    dt2 = new DateTime(1900, 1, 1);
                }


                // IF COURT DATE IS NOT AVAILABLE THEN USE ESTIMATED COURT DATE BY ADDING 21 DAYS TO VIOLATION DATE.
                if (dt2.ToShortDateString() != (new DateTime(1900, 1, 1)).ToShortDateString())
                {
                    if (dt.ToShortDateString() == (new DateTime(1900, 1, 1)).ToShortDateString())
                        dt = dt2.AddDays(21);
                }
                // Modifications End
                string status = dr["vercourtdesc"].ToString();

                if (dt > DateTime.Now && (status == "PRE" || status == "JUR" || status == "JUD"))
                {
                    if (tempdate.ToShortDateString() != "1/1/1900")
                    {
                        if (tempdate > dt)
                        {
                            tempdate = dt;

                        }
                    }
                    else
                    {
                        tempdate = dt;
                    }
                }
                else if (dt.ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    tempdate = dt;
                }
            }
            //hf_ContinuanceCheck.Value = tempdate.ToShortDateString();
            return tempdate;


        }

        private void CheckReadNotes(int ticketid)
        {
            bool checkflag;
            checkflag = notes.CheckReadNoteComments(ticketid);
            if (checkflag)
            {

                read.Style[HtmlTextWriterStyle.Display] = "block";

            }
            else
            {

                read.Style[HtmlTextWriterStyle.Display] = "none";
            }


        }

        //Waqas 5771 04/15/2009 
        /// <summary>
        /// To enable client information editable
        /// </summary>
        /// <param name="check"></param>
        private void EditClientInformation(bool check)
        {
            if (check)
            {
                txt_lname.Style["display"] = "block";
                lbltxt_lname.Style["display"] = "none";

                td_txtName.Style["display"] = "block";
                td_lblName.Style["display"] = "none";

                td_txtDate.Style["display"] = "block";
                td_lbldate.Style["display"] = "none";
            }
            else
            {
                txt_lname.Style["display"] = "none";
                lbltxt_lname.Style["display"] = "block";

                td_txtName.Style["display"] = "none";
                td_lblName.Style["display"] = "block";

                td_txtDate.Style["display"] = "none";
                td_lbldate.Style["display"] = "block";
            }
        }

        // Farrukh 9332 07/28/2011 Fill Tickets Activites Grid
        private void FillActivitesGrid()
        {

            try
            {
                //Handling Activites events
                string[] key1 = { "@TicketID" };
                object[] value1 = { TicketId };

                DataSet dsActivites = ClsDb.Get_DS_BySPArr("USP_HTP_Get_Activities", key1, value1);
                gv_Activites.DataSource = dsActivites;
                gv_Activites.DataBind();

                if (gv_Activites.Rows.Count == 0)
                {
                    lblError.Text = "No Records";
                    gv_Activites.Visible = false;
                }
                else
                {
                    gv_Activites.Visible = true;
                }

            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                dvMessage.Visible=true;lblMessage.Text = ex.Message;
            }

        }




        /// <summary>
        ///Nasir 5381 01/20/2009 handle event bubleling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnok_Click(object sender, EventArgs e)
        {
            Modal_Message.Hide();
            Response.Redirect(Request.Url.AbsoluteUri, false);
        }

        // Babar Ahmad 9676 11/21/2011 Check bad email flag. 
        /// <summary>
        /// This method checks whether a client has bad email flag or not. 
        /// </summary>
        /// <param name="ticketId">Ticket ID or record</param>
        /// <returns>True / False</returns>
        private bool CheckBadEmail(int ticketId)
        {
            string[] key = { "@ticketId" };
            object[] value = { ticketId };
            int badEmailResult = Convert.ToInt32(ClsDb.ExecuteScalerBySp("usp_htp_CheckBadEmail", key, value));
            if (badEmailResult == 1)
                return true;
            return false;
        }

        // Babar Ahmad 9676 11/04/2011 For Verify email link. Popup will be shown when link is clicked.

        void ShowPopupOnVerifyLink()
        {
            clsCase cCase = new clsCase();

            cCase.GetClientInfo(TicketId);

            int ticketId = TicketId;


            Label firstName = new Label();
            firstName.Text = cCase.FirstName;
            //(Label)e.Row.FindControl("lbl_FirstName");
            Label lastName = new Label();

            lastName.Text = cCase.LastName;//(Label)e.Row.FindControl("lbl_LastName");

            TextBox txtEmail = new TextBox();
            txtEmail.Text = cCase.Email;

            string generalComments = cCase.GeneralComments;
            UpdateEmailAddress1.CallFrom = "Contacted";

            lnkbtn_VerifyEmail.OnClientClick = "return ShowPopUpForVerifyEmail('VerifyEmailAddress','" + ticketId + "','" + firstName.Text + "','" + lastName.Text + "','" + txt_email.Text + "','" + generalComments + "','0')";

        }




        //Abbas Qamar  9676  12-01-2011 Page Mehtod for refresh the page 
        void UpdateEmailAddress1_PageMethod()
        {
            Response.Redirect(Request.Url.AbsoluteUri, false);
        }


        //Abbas Qamar 9676 on 09/12/2011 This function is used to set the bad email flag for the against Ticket ID
        /// <summary>
        /// This function is used to set the bad email flag for the against Ticket ID
        /// </summary>
        public void ResetReviewedEmailStatus(int ticketId)
        {
            string[] keys = { "@ticketid" };
            object[] values = { ticketId };

            ClsDb.ExecuteSP("usp_htp_Reset_ReviewedEmailStatus_Flag", keys, values);

        }

        // Farrukh 10003 01/20/2012 This Method is used to set No Bad Address Flag against Ticket ID
        /// <summary>
        /// This Method is used to set No Bad Address Flag against Ticket ID
        /// </summary>
        public void ResetNoBadAddressFlag(int ticketId, Boolean noBadAddressFlag)
        {
            string[] keys = { "@ticketid", "@IsNoBadAddressFlag" };
            object[] values = { ticketId, noBadAddressFlag };

            ClsDb.ExecuteSP("usp_htp_Reset_NoBadAddress_Flag", keys, values);
        }

        // Haris Ahmed 10003 01/20/2012 This Method is used to get Fid for No Bad Address Flag against Ticket ID
        /// <summary>
        /// This Method is used to get Fid for No Bad Address Flag against Ticket ID
        /// </summary>
        public string GetFidForNoBadAddressFlag(string ticketId)
        {
            DataSet ds = ClsDb.Get_DS_BySPByOneParmameter("usp_htp_Get_FidForNoBadAddressFlagAgainstTicketId", "@ticketid", ticketId);
            string fid = string.Empty;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    fid = ds.Tables[0].Rows[0][0].ToString();
                }
            }

            return fid;
        }

        #endregion



        // Haris Ahmed 10003 01/20/2012 Methods used to verify from the client that his/her address is correct. Click 'Yes' to accept the address or Click 'No' to cancel"
        protected void btnYes_Click(object sender, EventArgs e)
        {
            Modal_NoBadAddress.Hide();

            // Haris Ahmed 10003 03/02/2012 Check if No Bad Address Flag ready exists
            string fid = GetFidForNoBadAddressFlag(TicketId.ToString());

            if (fid == "")
            {
                ResetNoBadAddressFlag(TicketId, true);

                string[] key1 = { "TicketID", "flagID", "EmpID" };
                object[] value1 = { TicketId, 45, cSession.GetCookie("sEmpID", Request) };
                ClsDb.ExecuteSP("USP_HTS_Insert_flag_by_ticketnumber_and_flagID", key1, value1);
            }

            Response.Redirect(Request.Url.AbsoluteUri, false);
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            Modal_NoBadAddress.Hide();

            ResetNoBadAddressFlag(TicketId, false);

            string fid = GetFidForNoBadAddressFlag(TicketId.ToString());

            if (fid != "")
            {
                string[] key1 = { "@TicketID", "flagid", "empid", "@fid" };
                object[] value1 = { TicketId, 45, cSession.GetCookie("sEmpID", Request), fid };

                ClsDb.ExecuteSP("USP_HTS_Delete_EventFlag_by_Ticketnumber_and_FlagID", key1, value1);
            }

            Response.Redirect(Request.Url.AbsoluteUri, false);
        }

        private void FillPageData(DataSet dsData)
        {
            if (dsData != null)
            {
                //Bind DLState and State with DL/StateDropDown
                if (dsData.Tables[0] != null && dsData.Tables[0].Rows.Count > 0)
                {
                    ddl_state.DataSource = dsData.Tables[0];
                    ddl_state.DataTextField = dsData.Tables[0].Columns[1].ColumnName;
                    ddl_state.DataValueField = dsData.Tables[0].Columns[0].ColumnName;
                    ddl_state.DataBind();
                }
                if (dsData.Tables[1] != null && dsData.Tables[1].Rows.Count > 0)
                {
                    ddl_dlState.DataSource = dsData.Tables[1];
                    ddl_dlState.DataTextField = dsData.Tables[1].Columns[1].ColumnName;
                    ddl_dlState.DataValueField = dsData.Tables[1].Columns[0].ColumnName;
                    ddl_dlState.DataBind();
                }

                if (dsData.Tables[2] != null && dsData.Tables[2].Rows.Count > 0)
                {

                    for (int i = 0; i < dsData.Tables[2].Rows.Count; i++)
                    {
                        string svalue = dsData.Tables[2].Rows[i]["ID"].ToString().Trim();
                        string stext = dsData.Tables[2].Rows[i]["Description"].ToString().Trim();
                        ddl_contact1.Items.Add(stext);
                        ddl_contact1.Items[i + 1].Value = svalue;
                        ddl_contact2.Items.Add(stext);
                        ddl_contact2.Items[i + 1].Value = svalue;
                        ddl_contact3.Items.Add(stext);
                        ddl_contact3.Items[i + 1].Value = svalue;
                    }
                }
            }

        }
    }
}

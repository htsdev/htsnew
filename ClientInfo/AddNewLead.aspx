﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewLead.aspx.cs" Inherits="lntechNew.ClientInfo.AddNewLead" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add New Lead</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/GeneralValidations.js" type="text/javascript">
    </script>

    <style type="text/css">
        .style1
        {
            width: 80px;
        }
        .clsLeftPaddingTable TD
        {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
        }
    </style>
</head>
<body id="bd_addnewlead" runat="server" style="margin: 0px">

    <script src="/Scripts/jquery-1.4.4.js" type="text/javascript"></script>

    <script src="/Scripts/jquery.maskedinput-1.2.2.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        FocusPhone = function(ids, source) {
            var tdbox = document.getElementById(ids);

            var chk = document.getElementById("<%=isContactInternational.ClientID%>");

            if (chk.checked)
            { $(tdbox).unmask(); }
            else {
                $(tdbox).unmask();
                $(tdbox).mask("999-999-9999?X9999");

            }
            if (source == "checkbox") {
                tdbox.value = "";
                tdbox.focus();
            }

        };

        FocusDate = function(ids) {
            var tdbox = document.getElementById(ids);
            $(tdbox).unmask();
            $(tdbox).mask("99/99/9999");

        };
    </script>

    <script type="text/javascript">

        function isEmail(emails) {
            var regex = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
            if (!emails.match(regex)) {
                return false;
            }


            return true;

        }
        function ValidateDepuyContactUs() {
            
            // Rab Nawaz Khan 11473 10/21/2013 Added Caller Id validation . . .
            if ((document.getElementById("<%=txt_CallerId.ClientID%>").value == "") && (!document.getElementById("<%=chk_unknown.ClientID%>").checked)) {
                alert('Please enter Caller ID of the caller or select Unknown');
                document.getElementById("<%=txt_CallerId.ClientID%>").focus();
                return false;
            }
            if ((document.getElementById("<%=txt_Name.ClientID%>").value == "") && (!document.getElementById("<%=chk_unknownPerson.ClientID%>").checked)) {
                alert('Please enter Name or select Unknown');
                document.getElementById("<%=txt_Name.ClientID%>").focus();
                return false;
            }
            if (!CheckName(document.getElementById("<%=txt_Name.ClientID%>").value)) {
                alert("Please enter Name in alphabets");
                document.getElementById("<%=txt_Name.ClientID%>").focus();
                return false;
            }
            if (document.getElementById('<%= ddl_leadStatus.ClientID %>').value == '0') {
                var ddlLanguage = document.getElementById("<%=ddlLanguage.ClientID %>");
                if (ddlLanguage.options[ddlLanguage.selectedIndex].value == "--Select any Language--") {
                    alert("Please select Language");
                    ddlLanguage.focus();
                    return false;
                }
            }
            if (document.getElementById('<%= ddl_leadStatus.ClientID %>').value == '0') {
                if (document.getElementById("<%=txt_Email.ClientID%>").value == "") {
                    alert('Please enter email address');
                    document.getElementById("<%=txt_Email.ClientID%>").focus();
                    return false;
                }

                if (checkValidation(document.getElementById("<%=txt_Email.ClientID%>")) == false) {
                    document.getElementById("<%=txt_Email.ClientID%>").focus();
                    return false;
                }
            }
            if (document.getElementById("<%=isContactInternational.ClientID%>").checked) {
                if (document.getElementById("<%=txt_Phone.ClientID%>").value != "" && isNaN(document.getElementById("<%=txt_Phone.ClientID%>").value)) {
                    alert("Phone number entered is in incorrect format");
                    document.getElementById("<%=txt_Phone.ClientID%>").value = ""
                    document.getElementById("<%=txt_Phone.ClientID%>").focus();
                    return false;
                }

                var strphone = document.getElementById("<%=txt_Phone.ClientID%>").value;
                if (document.getElementById("<%=txt_Phone.ClientID%>").value != "" && strphone.length < 10) {
                    alert("Your telephone number should include the area code.");
                    document.getElementById("<%=txt_Phone.ClientID%>").focus();
                    return false;
                }
            }


            var ddlQuestion = document.getElementById("<%=ddlQuestion.ClientID %>");
            if (document.getElementById('ddl_leadStatus').value == '0') {
                var ddlQuestion = document.getElementById("<%=ddlQuestion.ClientID %>");
                if (ddlQuestion.options[ddlQuestion.selectedIndex].value == "0") {
                    alert("Please select valid Practice Area ");
                    ddlQuestion.focus();
                    return false;
                } 
            }
           if (ddlQuestion.options[ddlQuestion.selectedIndex].value == "7" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "28" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "29" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "30" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "31" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "1") {
                if (document.getElementById("<%=ddlZimmer.ClientID %>").value == "0" && ddlQuestion.options[ddlQuestion.selectedIndex].value == "1") {
                    alert("Please select zimmer device.");
                    document.getElementById("<%=ddlZimmer.ClientID %>").focus();
                    return false;
                }
                if (document.getElementById("<%=ddlManufacturer.ClientID%>").value == "0" && (ddlQuestion.options[ddlQuestion.selectedIndex].value == "7" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "28" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "29" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "30" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "31")) {
                    alert("Please select hip device.");
                    document.getElementById("<%=ddlManufacturer.ClientID%>").focus();
                    return false;
                }
                
                if (document.getElementById("<%=ddlIsPain.ClientID%>").value == "0" && (ddlQuestion.options[ddlQuestion.selectedIndex].value == "7" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "28" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "29" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "30" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "31" || ddlQuestion.options[ddlQuestion.selectedIndex].value == "1")) {
                    alert("Please select level of pain.");
                    document.getElementById("<%=ddlIsPain.ClientID%>").focus();
                    return false;
                }
            }
            //Haris Ahmed 10292 05/21/2012 Validate date in Transvagianal Mesh Question
            if (ddlQuestion.options[ddlQuestion.selectedIndex].value == "24") {
                if (!checkdate(document.getElementById('<%=txtTMDate1.ClientID %>').value)) {
                    return false;
                }
            }
            if (document.getElementById("<%=txtComments.ClientID%>").value == "") {
                alert('Please enter comments');
                document.getElementById("<%=txtComments.ClientID%>").focus();
                return false;
            }

            
            document.getElementById("<%=btn_submit.ClientID%>").style.display = "none";
            return true;
        }
        function CheckComments(name) {
            for (i = 0; i < name.length; i++) {
                var asciicode = name.charCodeAt(i)
                if ((asciicode == 60) || (asciicode == 62))
                    return false;
            }
            return true;
        }

        function showHidContactUs(id) {
            if (id == "0") {
                document.getElementById('tbl_ProductDefect').style.display = "none";
                document.getElementById('tbl_Actos').style.display = "none";
                document.getElementById('tbl_Pradaxa').style.display = "none";
                document.getElementById('tbl_TransvagianalMesh').style.display = "none";
            }
            else if (id == "7" || id == "28" || id == "29" || id == "30" || id == "31" || id == "1") {
                document.getElementById('tbl_ProductDefect').style.display = "";
                document.getElementById('tbl_Actos').style.display = "none";
                document.getElementById('tbl_Pradaxa').style.display = "none";
                document.getElementById('tbl_TransvagianalMesh').style.display = "none";
                if (id == "7" || id == "28" || id == "29" || id == "30" || id == "31") {
                    document.getElementById('<%=lbl_Manufacturer.ClientID %>').innerHTML = "What hip device do you have? ";
                    document.getElementById('<%=ddlManufacturer.ClientID %>').style.display = "";
                    document.getElementById('<%=ddlZimmer.ClientID %>').style.display = "none";
                }
                else if (id == "1") {
                    document.getElementById('<%=lbl_Manufacturer.ClientID %>').innerHTML = "What knee device do you have? ";
                    document.getElementById('<%=ddlManufacturer.ClientID %>').style.display = "none";
                    document.getElementById('<%=ddlZimmer.ClientID %>').style.display = "";
                }
            }
            else if (id == "23") {
                document.getElementById('tbl_ProductDefect').style.display = "none";
                document.getElementById('tbl_Actos').style.display = "";
                document.getElementById('tbl_Pradaxa').style.display = "none";
                document.getElementById('tbl_TransvagianalMesh').style.display = "none";
            }
            //Haris Ahmed 10292 05/21/2012 Setting controls when selecting Transvagianal Mesh
            else if (id == "24") {
                document.getElementById('tbl_ProductDefect').style.display = "none";
                document.getElementById('tbl_Actos').style.display = "none";
                document.getElementById('tbl_Pradaxa').style.display = "none";
                document.getElementById('tbl_TransvagianalMesh').style.display = "";
            }
            //Haris Ahmed 10292 05/21/2012 Setting controls when selecting Pradaxa
            else if (id == "27") {
                document.getElementById('tbl_ProductDefect').style.display = "none";
                document.getElementById('tbl_Actos').style.display = "none";
                document.getElementById('tbl_Pradaxa').style.display = "";
                document.getElementById('tbl_TransvagianalMesh').style.display = "none";
            }
            else {
                document.getElementById('tbl_ProductDefect').style.display = "none";
                document.getElementById('tbl_Actos').style.display = "none";
                document.getElementById('tbl_Pradaxa').style.display = "none";
                document.getElementById('tbl_TransvagianalMesh').style.display = "none";
            }
        }
        
        function CheckName(name) {
            for (i = 0; i < name.length; i++) {
                var asciicode = name.charCodeAt(i)
                if (!((asciicode > 64 && asciicode <= 90) || (asciicode >= 97 && asciicode <= 122) || (asciicode == 32) || (asciicode == 46)))
                    return false;
            }
            return true;
        }
        //Haris Ahmed 10292 05/21/2012 Add function to validate Date
        function checkdate(input) {
            var validformat = /^\d{2}\/\d{2}\/\d{4}$/;  //Basic check for format validity
            var givenDate = new Date(input);
            var toDate = new Date();
            var returnval = false;
            if (!validformat.test(input))
                alert("Please enter valid date.");
            else { //Detailed check for valid date ranges
                var monthfield = input.split("/")[0];
                var dayfield = input.split("/")[1];
                var yearfield = input.split("/")[2];
                var dayobj = new Date(yearfield, monthfield - 1, dayfield);
                if ((dayobj.getMonth() + 1 != monthfield) || (dayobj.getDate() != dayfield) || (dayobj.getFullYear() != yearfield))
                    alert("Please enter valid date.");
                else if (yearfield < 1900 || givenDate > toDate) {
                    alert('Please enter date between 01/01/1900 and today.');
                    // myDate is between startDate and endDate
                }
                else
                    returnval = true;
            }
            if (returnval == false) document.getElementById("<%=txtTMDate1.ClientID%>").focus();
            return returnval;
        }
        function pad(number, length) {

            var str = '' + number;
            while (str.length < length) {
                str = '0' + str;
            }

            return str;

        }
        // Rab Nawaz Khan 11473 10/25/2013 if Unknown check marked then clear the text box values. . .
        function ClearValues() {
            if (document.getElementById("<%=chk_unknown.ClientID%>").checked) {
                document.getElementById("<%=txt_CallerId.ClientID%>").value = "";
                document.getElementById("<%=txt_CallerId.ClientID%>").disabled = true;
            }
            else
                document.getElementById("<%=txt_CallerId.ClientID%>").disabled = false;
            if (document.getElementById("<%=chk_unknownPerson.ClientID%>").checked) {
                document.getElementById("<%=txt_Name.ClientID%>").value = "";
                document.getElementById("<%=txt_Name.ClientID%>").disabled = true;
            }
            else
                document.getElementById("<%=txt_Name.ClientID%>").disabled = false;
            return false;
        }
        
        // Rab Nawaz Khan 11473 10/25/2013 used to refresh the parent page if close button clicked
        function closeAddNewLeads() {
            window.opener.location.reload();
            window.close();
        }
        
        // Rab Nawaz Khan 11473 11/11/2013 used to change the required fields. . .
        function ChangeRequiredFields(value) {
            if(value == '9' || value == '10') {
                document.getElementById('<%= dv_language.ClientID %>').style.display = "none";
                document.getElementById('<%= dv_email.ClientID %>').style.display = "none";
                document.getElementById('<%= dv_prcArea.ClientID %>').style.display = "none";
            }
            else {
                document.getElementById('<%= dv_language.ClientID %>').style.display = "block";
                document.getElementById('<%= dv_language.ClientID %>').style.display = "inline";
                document.getElementById('<%= dv_email.ClientID %>').style.display = "block";
                document.getElementById('<%= dv_email.ClientID %>').style.display = "inline";
                document.getElementById('<%= dv_prcArea.ClientID %>').style.display = "block";
                document.getElementById('<%= dv_prcArea.ClientID %>').style.display = "inline";
            }
        }
    </script>

    <form id="form1" runat="server">
      <%--  <div class="page-container row-fluid container-fluid">--%>
        <asp35:ScriptManager ID="ScriptManager1" runat="server"></asp35:ScriptManager>
        

            <asp35:UpdatePanel ID="upnlResult" runat="server">
                    <ContentTemplate>
                         <div ID="lbl_errorshow" runat="server" class="alert alert-danger alert-dismissable fade in" Visible="False">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl_error" runat="server" ></asp:Label>
                             </div>
            <asp:HiddenField ID="hf_ticketId" runat="Server" Value="0" />
                        
              <asp35:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                        <ProgressTemplate>
                                            <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                        </ProgressTemplate>
                                    </asp35:UpdateProgress>
           
            <section id="main-content-popup class="addNewLeadPopup">
                <section class="wrapper main-wrapper row" style="">
                     <div class="col-xs-12">
                         <div class="page-title" style="display: none;>
                
                              <h1 class="title">Add Lead</h1>
                    
              
              
            
                        </div>

                      </div>
                    <div class="clearfix"></div>
                     <div class="col-xs-12">
                         <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Add New Lead</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                        <div class="content-body">
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Lead Status</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddl_leadStatus" onchange="ChangeRequiredFields(this.value)"
                                        CssClass="form-control" runat="server" Width="100px">
                                    </asp:DropDownList>
                                                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Caller ID </label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                      <asp:TextBox ID="txt_CallerId" runat="server" CssClass="form-control inline-textbox" MaxLength="50"
                                        Text="" Width="180px"></asp:TextBox>&nbsp;
                                    <asp:CheckBox runat="server" onclick="ClearValues(this);" ID="chk_unknown" CssClass="form-label"
                                        Text="Unknown" />
                                    <span style="color: Red">&nbsp;*</span>
                                                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Name </label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                      <asp:TextBox ID="txt_Name" runat="server" CssClass="form-control inline-textbox" MaxLength="50"
                                        Text="" Width="180px"></asp:TextBox>&nbsp;
                                    <asp:CheckBox runat="server" onclick="ClearValues(this);" ID="chk_unknownPerson"
                                        CssClass="form-label" Text="Unknown" />
                                    
                                             <span style="color: Red">&nbsp;*</span>                 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Language</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:DropDownList ID="ddlLanguage" onchange="showHidContactUs(<%=ddlQuestion.ClientID%>);"
                                        CssClass="form-control inline-textbox" runat="server" Width="254px">
                                        <asp:ListItem>--Select any Language--</asp:ListItem>
                                        <asp:ListItem>English</asp:ListItem>
                                        <asp:ListItem>Spanish</asp:ListItem>
                                        <asp:ListItem>Other</asp:ListItem>
                                    </asp:DropDownList>
                                    <div id="dv_language" style="display: inline" runat="server">
                                        <span style="color: Red">&nbsp;*</span></div>
                                                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:TextBox ID="txt_Email" runat="server" CssClass="form-control inline-textbox" MaxLength="200"
                                        Text="" Width="250px"></asp:TextBox>
                                    <div id="dv_email" style="display: inline" runat="server">
                                        <span style="color: Red">&nbsp;*</span></div>
                                                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Phone</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:TextBox ID="txt_Phone" runat="server" onfocus="FocusPhone(this.id,'textbox');" onkeypress="return isNumber(event)"
                                        CssClass="form-control" Width="250px" MaxLength="30"></asp:TextBox>
                                                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">&nbsp;</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                      <asp:CheckBox ID="isContactInternational" runat="server" CssClass="form-label" onclick="javascript:FocusPhone('txt_Phone','checkbox');"
                                        Text="Outside USA" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Practice Area</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                      <asp:DropDownList ID="ddlQuestion" onchange="showHidContactUs(this.value);" CssClass="form-control inline-textbox"
                                        runat="server" Width="254px">
                                    </asp:DropDownList>
                                    <div id="dv_prcArea" style="display: inline" runat="server">
                                        <span style="color: Red">&nbsp;*</span></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="tbl_ProductDefect" style="display: none">
                                     <div class="col-md-3">
                                        <div class="form-group">
                                            <asp:Label ID="lbl_Manufacturer" CssClass="form-label" runat="server">Manufacturer</asp:Label>
                                            <span class="desc"></span>
                                            <div class="controls">
                                               <asp:DropDownList ID="ddlManufacturer" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlZimmer" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-md-3">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" CssClass="form-label" runat="server">Level of Pain [1=lowest - 10=highest]</asp:Label>
                                            <span class="desc"></span>
                                            <div class="controls">
                                              <asp:DropDownList ID="ddlIsPain" CssClass="clsInputCombo" runat="server">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                    <asp:ListItem Value="7">7</asp:ListItem>
                                                    <asp:ListItem Value="8">8</asp:ListItem>
                                                    <asp:ListItem Value="9">9</asp:ListItem>
                                                    <asp:ListItem Value="10">10</asp:ListItem>
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                        </div>
                            
                                </div>
                            <div id="tbl_Actos" style="display: none">
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Have you been diagnosed with bladder cancer?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:RadioButtonList ID="rblChoice1" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                                </asp:RadioButtonList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label"> Do you have any of the following symptoms? (check all that apply)</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:CheckBoxList ID="cblSymptoms" runat="server">
                                                    <asp:ListItem Value="1">Blood in the urine (bright red, dark yellow, or cola colored)</asp:ListItem>
                                                    <asp:ListItem Value="2">Frequent or painful urination</asp:ListItem>
                                                    <asp:ListItem Value="3">Frequent urinary tract infection</asp:ListItem>
                                                    <asp:ListItem Value="4">Lower back or abdominal pain</asp:ListItem>
                                                    <asp:ListItem Value="5">Not sure</asp:ListItem>
                                                </asp:CheckBoxList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">   Have you ever take Actos, Actoplus Met, Actoplus Met XR, or Duetact?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:RadioButtonList ID="rblChoice2" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                                </asp:RadioButtonList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">  How long did you ever take Actos, Actoplus Met, Actoplus Met XR, and/or Duetact?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                         <asp:RadioButtonList ID="rblChoice3" runat="server">
                                                    <asp:ListItem Value="1">More than one year</asp:ListItem>
                                                    <asp:ListItem Value="0">Less than 1 year</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                                </asp:RadioButtonList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label"> Have you or a loved one had an Internal Bleed or Brain Hemorrhage due to Pradaxa?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                          <asp:RadioButtonList ID="rblPradaxaC1" runat="server">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                                </asp:RadioButtonList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Has this Internal Bleed or Brain Hemorrhage cause the death of a loved one?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                           <asp:RadioButtonList ID="rblPradaxaC2" runat="server">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                                </asp:RadioButtonList>
                                    </div>
                                </div>
                             </div>
                                
                           </div>
                            <div id="tbl_TransvagianalMesh" style="display: none">
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">When was your surgery date?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                          <asp:TextBox ID="txtTMDate1" CssClass="form-control" Width="100px" runat="server"
                                                    onfocus="FocusDate(this.id);"></asp:TextBox>&nbsp;<asp:Label ID="lblDateFormat" Text="mm/dd/yyyy"
                                                        runat="server" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Was your surgery due to the following?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                          <asp:CheckBoxList ID="cblTMSymptoms" runat="server">
                                                    <asp:ListItem Value="1">Pelvic Organ Prolapse (POP)</asp:ListItem>
                                                    <asp:ListItem Value="2">Stress Urinary Incontinence (SUI)</asp:ListItem>
                                                    <asp:ListItem Value="3">Hernia</asp:ListItem>
                                                    <asp:ListItem Value="4">Other</asp:ListItem>
                                                </asp:CheckBoxList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label">Did you receive a Bladder Sling?</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                          <asp:RadioButtonList ID="rblTMChoice1" runat="server">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Not sure</asp:ListItem>
                                                </asp:RadioButtonList>
                                    </div>
                                </div>
                             </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label"> How was your Mesh Implanted?</label>
                                        <span class="desc"></span>
                                        <div class="controls">
                                             <asp:RadioButtonList ID="rblTMChoice2" runat="server">
                                                    <asp:ListItem Value="1">Transvaginally</asp:ListItem>
                                                    <asp:ListItem Value="0">Abdominally</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="2">Other</asp:ListItem>
                                                </asp:RadioButtonList>
                                        </div>
                                    </div>
                                 </div>
                                
                            </div>
                            <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label">Comments</label>
                                        <span class="desc"></span>
                                        <div class="controls">
                                              <asp:TextBox ID="txtComments" runat="server" CssClass="form-control inline-textbox" Height="60px"
                                        Text="" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                    <span style="color: Red; vertical-align: top;">&nbsp;*</span>
                                        </div>
                                    </div>
                                 </div>
                            <hr />
                            <div class="clearfix"></div>
                            <div class="row">
                            <div class="col-md-12">
                            <div class="pull-right">
                            <asp:Button ID="btn_submit" runat="server" class="btn btn-primary" Text="Submit" OnClientClick="return ValidateDepuyContactUs();"
                                        OnClick="btn_submit_Click"  />
                                    <asp:Button ID="btn_Clear" runat="server" class="btn btn-primary" Text="Clear" 
                                        OnClick="btn_Clear_Click" />
                                    <asp:Button ID="btn_Close" runat="server" class="btn btn-primary" OnClientClick="return closeAddNewLeads();"
                                        TabIndex="9" Text="Close"  />
                           </div>             
                          </div>         
                          </div>
                          
                          
                          </div>
                                
                                
                </section>
                     </div>
                 </section>
            </section>
            
            <div>
                
                        
                   
            </div>
                         </ContentTemplate>
                </asp35:UpdatePanel>
        <%--</div>--%>
    </form>
       <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
</body>
</html>

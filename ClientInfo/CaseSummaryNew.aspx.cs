

//// REPORT CREATED BY FAHAD FOR DIPLAYING CRYSTAL REPORT DOCUMENT
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.WebControls;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using System.Configuration;


namespace lntechNew.ClientInfo
{
    public partial class CaseSummaryNew : System.Web.UI.Page
    {
        clsCase cCase = new clsCase();
        clsLogger bugtracker = new clsLogger();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        String reportsetting = "";
        int EmpID;
       

        private int Ticketid;

        public int TicketID
        {
            get { return Ticketid; }
            set { Ticketid = value; }
        }
    
     

      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                    if (Request.QueryString.Count >= 2)
                    {
                        ViewState["vTicketId"] = Request.QueryString["casenumber"];
                        ViewState["vSearch"] = Request.QueryString["search"];
                    }
                    else
                    {
                        Response.Redirect("../frmMain.aspx", false);
                        goto SearchPage;
                    }

                    



                    ViewState["vEmpID"] = cSession.GetCookie("sEmpID", this.Request);
                    TicketID = Convert.ToInt32(ViewState["vTicketId"]);
                    
                    
                    if (TicketID == 0)
                    {
                        Response.Redirect("ViolationFeeold.aspx?newt=1", false);
                    }
                    EmpID = Convert.ToInt32(ViewState["vEmpID"]);

                    GetIds();

                    //ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                    //TextBox txt1 = (TextBox)am.FindControl("txtid");
                    //txt1.Text = ViewState["vTicketId"].ToString();

                    //TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                    //txt2.Text = ViewState["vSearch"].ToString();

                     SearchPage:
                    { }	
                

            }

        }

        private void GetIds()
        {
            ViewState["setting"] = "";
            if (Contact.Checked)
            {
                reportsetting = "1";
            }
            if (Matter.Checked)
            {
                reportsetting += "2";
            }
            if (Flags.Checked)
            {
                reportsetting += "3";
            }
             if (Comments.Checked)
            {
                reportsetting += "4";
            }
            if (History.Checked)
            {
                reportsetting += "5";
            }
            if (Billing.Checked)
            {
                reportsetting += "6";
            }

            ViewState["setting"] = reportsetting;

           
            
          
        }


        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                GetIds();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message.ToString();
                bugtracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

      



   
    }
}

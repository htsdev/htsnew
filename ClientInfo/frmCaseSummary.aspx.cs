using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using lntechNew.WebControls;
using WebSupergoo.ABCpdf6;
using WebSupergoo.ABCpdf6.Objects;
using WebSupergoo.ABCpdf6.Atoms;
using System.Configuration;

namespace lntechNew.ClientInfo
{
	/// <summary>
	/// Summary description for frmCaseSummary.
	/// </summary>
	public partial class frmCaseSummary : System.Web.UI.Page
	{
		int srch;
		int TicketID;
        string DisplaySections;
		clsLogger clog = new clsLogger();
		//clsCase	cCaseHistory =new clsCase();
		clsSession cSession = new clsSession(); 
		clsCaseDetail cCaseDetail=new clsCaseDetail();
		clsENationWebComponents ClsDb = new clsENationWebComponents();

		protected System.Web.UI.WebControls.Label lblOwesAmount;
		protected System.Web.UI.WebControls.Label lblFirmName;
		protected System.Web.UI.WebControls.Label lblCourtNo;
		protected System.Web.UI.WebControls.Label Label4;	
		protected System.Web.UI.WebControls.Label lblAct;
		protected System.Web.UI.WebControls.DataGrid DGDesc;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblFlagStatusACC;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label lblFlagStatusCDL;
		protected System.Web.UI.WebControls.Label lblmsg;
		protected System.Web.UI.WebControls.Label lblOffName;
		protected System.Web.UI.WebControls.Label lblDay;
		protected System.Web.UI.WebControls.Label lblBond;
		protected System.Web.UI.WebControls.Label lblGC;
		protected System.Web.UI.WebControls.Label lblGeneral;
		protected System.Web.UI.WebControls.Label lblCN;
		protected System.Web.UI.WebControls.Label lblContact;
		protected System.Web.UI.WebControls.Label lblTC;
		protected System.Web.UI.WebControls.Label lblTrial;
		protected System.Web.UI.WebControls.Label lblSC;
		protected System.Web.UI.WebControls.Label lblSetting;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Label lblCity;
		protected System.Web.UI.WebControls.Label lblb;
		protected System.Web.UI.WebControls.Label lblS;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected System.Web.UI.WebControls.Label lblC1;
		protected System.Web.UI.WebControls.Label lblCon1;
		protected System.Web.UI.WebControls.Label lblC2;
		protected System.Web.UI.WebControls.Label lblCon2;
		protected System.Web.UI.WebControls.Label lblC3;
		protected System.Web.UI.WebControls.Label lblCon3;
		protected System.Web.UI.WebControls.Label lblDriv;
		protected System.Web.UI.WebControls.Label lblDL;
		protected System.Web.UI.WebControls.Label lblDateOfBirth;
		protected System.Web.UI.WebControls.Label lblDOB;
		protected System.Web.UI.WebControls.Label lblMidNum;
		protected System.Web.UI.WebControls.Label lblMid;
		protected System.Web.UI.WebControls.Label lblLan;
		protected System.Web.UI.WebControls.Label lblLanguage;
		protected System.Web.UI.WebControls.DataList DLFlag;
		protected System.Web.UI.WebControls.DataGrid dg_CaseHeader;
		protected System.Web.UI.WebControls.DataList DLNote;
		protected System.Web.UI.WebControls.Label lblFee;
		protected System.Web.UI.WebControls.Label lblCon;
		protected System.Web.UI.WebControls.Label lblRep;
		protected System.Web.UI.WebControls.Label lblAdj;
		protected System.Web.UI.WebControls.Label lblTotFee;
		protected System.Web.UI.WebControls.Label lblPay;
		protected System.Web.UI.WebControls.Label lblOwe;
		protected System.Web.UI.WebControls.Label lblInitialFee;
		protected System.Web.UI.WebControls.Label lblContinuance;
		protected System.Web.UI.WebControls.Label lblReRep;
		protected System.Web.UI.WebControls.Label lblAdjustment;
		protected System.Web.UI.WebControls.Label lblTotalFee;
		protected System.Web.UI.WebControls.Label lblPaid;
		protected System.Web.UI.WebControls.Label lblOwes;  
		//protected lntechNew.WebControls.ActiveMenu ActiveMenu1;

        clsGeneralMethods clsgenral = new clsGeneralMethods();
		private void Page_Load(object sender, System.EventArgs e)
		{

			// Put user code to initialize the page here
			try
			{
              
                if (Request.QueryString.Count<4)
                {
                    if (cSession.IsValidSession(this.Request) == false)  // for session expired
                    {

                        Response.Redirect("../frmlogin.aspx", false);
                    }
                    else
                    {
                        pload();
                    }
                    //ActiveMenu am = (ActiveMenu) this.FindControl("ActiveMenu1");
                    //TextBox txt1 = (TextBox) am.FindControl("txtid");
                    //txt1.Text = ViewState["vTicketId"].ToString();
                    //TextBox txt2 = (TextBox) am.FindControl("txtsrch");
                    //txt2.Text = ViewState["vSearch"].ToString();

                    string cno =Request.QueryString["casenumber"];
                    string search =Request.QueryString["search"];                   
                }
                
                else
                {
                    pload();                    
                }

                //Set network path
                ViewState["vNTPATHDocketPDF"] = ConfigurationSettings.AppSettings["NTPATHSummary"].ToString();
                ViewState["vNTPATH1"] = ConfigurationSettings.AppSettings["NTPATH"].ToString();

                //// Delte previos genrated pdf files
                //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(ViewState["vNTPATHDocketPDF"].ToString());
                //foreach (System.IO.FileInfo fi in di.GetFiles())
                //{
                //    if (fi.Exists)
                //        fi.Delete();
                //}
			}
			catch (Exception ex)
			{
				lblmsg.Text  = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		
		}

        void pload()
        {
            try
            {
                if (IsPostBack != true)
                {
                    //srch   = Convert.ToInt32(cSession.GetCookie("sSearch",this.Request));
                    srch = Convert.ToInt32(Request.QueryString["search"]);
                    //ViewState["vTicketId"] = cSession.GetCookie("sTicketID" ,this.Request);
                    
                    if(Request.QueryString["casenumber"] != null)
                        ViewState["vTicketId"] = Request.QueryString["casenumber"];
                    
                    if (Request.QueryString["search"] != null)
                        ViewState["vSearch"] = Request.QueryString["search"];

                    if(ViewState["vTicketId"] != null)
                        TicketID = Convert.ToInt32(ViewState["vTicketId"]);

                    if (TicketID.ToString() == null)  // If TicketID is null
                    {
                        lblmsg.Text = "There is no TicketID";
                        lblb.Visible = false;
                    }
                    else if (TicketID == 0)    //96892;	
                    {
                        Response.Redirect("ViolationFeeold.aspx?newt=1", false);
                    }
                    else
                    {
                        //TicketID = Convert.ToInt32(cSession.GetSessionVariable("sticketID" ,this.Session));
                        GetValue();
                        // Fill Notes in datagrid
                        //DataSet	DsGetNotes= clog.GetNotes(TicketID);  //commented by ozair

                        // Fill Summary InformationDetail
                        //DataSet	DsSummaryDetail=cCaseDetail.GetSummaryInfoDetail(TicketID);//commented by ozair
                        //DataSet DsSummaryDetail = cCaseDetail.GetSummaryInfoDetail(Convert.ToInt32(ViewState["vTicketId"]));
                        //DGDesc.DataSource = DsSummaryDetail;
                        //DGDesc.DataBind();

                        ////fill flags

                        //string[] key2 ={ "@ticketid" };
                        ////object[] value1={TicketID};//commented by ozair
                        //object[] value1 ={ Convert.ToInt32(ViewState["vTicketId"]) };
                        //DataSet Dsflag = ClsDb.Get_DS_BySPArr("usp_HTS_Get_flags_Info_by_TicketNumber ", key2, value1);
                        //DLFlag.DataSource = Dsflag;
                        //DLFlag.DataBind();

                        img_pdf.Attributes.Add("onclick", "return Validate();");

                    }
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

		// Fill Summary Information
		private void GetValue()
		{
			try
			{
				string[] key1={"@TicketId_pk"};
				//object[] value1={TicketID};//commneted by ozair
				object[] value1={Convert.ToInt32(ViewState["vTicketId"])};
				string strdate;
				string middate;
				string strtime;
				

                // GridView Contact Information
                //DataSet ds_contact = cCaseDetail.GetSummaryInformation(Convert.ToInt32(ViewState["vTicketId"]));
                //GV_contact.DataSource = ds_contact.Tables[0];
                //GV_contact.DataBind();

                // Matter                    
                DataSet dsDetail = ClsDb.Get_DS_BySPArr("USP_HTS_GET_SummaryInfoDetail ", key1, value1);
                if (dsDetail.Tables[0].Rows.Count > 0)
                {    
                    lblofficername.Text = dsDetail.Tables[0].Rows[0]["officername"].ToString();

                    GV_matter.DataSource = dsDetail.Tables[0].DefaultView;
                    GV_matter.DataBind();
                }
                else
                    td_lblofficername.Visible = false;

                // History
                DataSet DsGetNotes = clog.GetNotes(Convert.ToInt32(ViewState["vTicketId"]));
                DLNote.DataSource = DsGetNotes;
                DLNote.DataBind();

                // Flags
                DataSet DsGetFlags = ClsDb.Get_DS_BySPArr("USP_HTS_GET_ALLFLAGS ", key1, value1);
                if (DsGetFlags.Tables[0].Rows.Count > 0)
                {
                    DL_Flag.DataSource = DsGetFlags;
                    DL_Flag.DataBind();
                }


                IDataReader dr = cCaseDetail.GetSummaryInfo (Convert.ToInt32(ViewState["vTicketId"]));   
				if (dr.Read()) //  Read data in DataReader
				{

                    lblkbtn_name.Text = dr["lastName"].ToString() + ", " + dr["FirstName"].ToString() + " " + dr["MiddleName"].ToString(); 

                    // Address
                    lbladd.Text = dr["Address"].ToString();
                    labeldob.Text = dr["DOBirth"].ToString();
                    labeldl1.Text = dr["DLNum"].ToString();
                    lbl_midnumber.Text = dr["MidNumber"].ToString();
                    lbllang1.Text = dr["Language"].ToString();
                                        
                    // Telephone numbers
                    if (dr["Contact1"].ToString() != "")
                    {
                        lbltel1.Text = clsgenral.formatPhone(dr["Contact1"].ToString());
                        if ((dr["Contactype1"].ToString() != null) && (dr["Contactype1"].ToString() != ""))
                            lbltel1.Text +="("+ clsgenral.formatPhone(dr["Contactype1"].ToString())+")";
                    }
                    
                    if (dr["Contact2"].ToString() != "")
                    {
                        lbltel2.Text = clsgenral.formatPhone(dr["Contact2"].ToString());
                        if ((dr["Contactype2"].ToString() != null) && (dr["Contactype2"].ToString() != ""))
                            lbltel2.Text += "("+clsgenral.formatPhone(dr["Contactype2"].ToString())+")";
                    }
                    if (dr["Contact3"].ToString() != "")
                    {
                        lbltel3.Text = clsgenral.formatPhone(dr["Contact3"].ToString());
                        if ((dr["Contactype3"].ToString() != null) && (dr["Contactype3"].ToString() != ""))
                            lbltel3.Text += "(" + clsgenral.formatPhone(dr["Contactype3"].ToString()) + ")";
                    }


                    if ((dr["email"].ToString() != "") && (dr["email"].ToString() != ""))
                    {
                        lblemail.Text = dr["email"].ToString();
                    }
                    // Comments
                    if ((dr["GenComments"].ToString() != null) && dr["GenComments"].ToString() != "")
                    {
                        tr_lblGenral.Visible = true;
                        lblGeneral.Text = dr["GenComments"].ToString();
                        tr_genral.Visible = true;
                    }
                    else
                    {
                        tr_genral.Visible = false;
                        tr_lblGenral.Visible = false;
                    }
                    if ((dr["ConComments"].ToString() != null) && (dr["ConComments"].ToString() != ""))
                    {
                        tr_lblContact.Visible = true;
                        lblContact.Text = dr["ConComments"].ToString();
                        tr_contactnotes.Visible = true;

                    }
                    else
                    {
                        tr_lblContact.Visible = false;
                        tr_contactnotes.Visible = false;
                    }
                    if ((dr["TriComments"].ToString() != null) && (dr["TriComments"].ToString() != ""))
                    {
                        tr_lbltrial.Visible = true;
                        lblTrial.Text = dr["TriComments"].ToString();
                        tr_trialcomments.Visible = true;
                    }
                    else
                    {
                        tr_lbltrial.Visible =false;
                        tr_trialcomments.Visible = false;
                    }
                    if ((dr["SetComments"].ToString() != null) && (dr["SetComments"].ToString() != ""))
                    {
                        tr_lblSetting.Visible = true;
                        lblSetting.Text = dr["SetComments"].ToString();
                        tr_settingcomments.Visible = true;
                    }
                    else
                    {
                        tr_lblSetting.Visible = false;
                        tr_settingcomments.Visible = false;
                    }
                    
                    // Billing
                    lblInitialFee.Text = Convert.ToDouble(dr["Initial"].ToString()).ToString("$###0;($###0)"); //"$" + dr["Initial"].ToString(); 
                    lblContinuance.Text = Convert.ToDouble(dr["Continuance"].ToString()).ToString("$###0;($###0)");
                    lblReRep.Text = Convert.ToDouble(dr["Rerap"].ToString()).ToString("$###0;($###0)");
                    lbladjustment1.Text = Convert.ToDouble(dr["Adjust"].ToString()).ToString("$###0;($###0)");
                    lbltotalfee1.Text = Convert.ToDouble(dr["TotFee"].ToString()).ToString("$###0;($###0)");
                    lbladj1.Text = Convert.ToDouble(dr["InitialAdjustment"].ToString()).ToString("$###0;($###0)");
                    lblowes1.Text = Convert.ToDouble(dr["AmountOwes"].ToString()).ToString("$###0;($###0)");
                    lblpaidamount1.Text = Convert.ToDouble(dr["PaidAmount"].ToString()).ToString("$###0;($###0)");
			


					// Flags
                    //if (dr["AccidentFlag"].ToString() == "Yes")
                    //{
                    //    tdacc.Visible = true;
                    //    lblac1.Text = "ACC";
                    //}
                    //else
                    //    tdacc.Visible = false;
                    //if (dr["CDLFlag"].ToString() =="Yes")
                    //{
                    //    tdcdl.Visible = true;
                    //    lblcd1.Text = "CDL";
                    //}
                    //else 
                    //    tdcdl.Visible = false;
                    //if ((dr["FirmAbb"].ToString() != null) && (dr["FirmAbb"].ToString() != ""))
                    //{
                    //    tdfirms.Visible = true;
                    //    lblfirms1.Text = "Outside Firms Client";
                    //}
                    //else
                    //    tdfirms.Visible = false;
                    //if ((dr["bond"].ToString() != null) && (dr["bond"].ToString() != ""))
                    //{
                    //    tdbonds.Visible = true;
                    //    lblbonds1.Text ="Bonds";
                    //}
                    //else
                    //    tdbonds.Visible = false;
 
				}

				dr.Close(); 	


						
					
								
			}
			catch (Exception ex)
			{
				lblmsg.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
	}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			//this.lnkName1.Click += new System.EventHandler(this.lnkName1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
               
        protected void img_pdf_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DisplaySections = "";
                if (chk_contact.Checked)
                    DisplaySections += "1";
                if (chk_matter.Checked)
                    DisplaySections += ",2";
                if (chk_flag.Checked)
                    DisplaySections += ",3";
                if (chk_comments.Checked)
                    DisplaySections += ",4";
                if (chk_history.Checked)
                    DisplaySections += ",5";
                if (chk_billing.Checked)
                    DisplaySections += ",6,";

                //for website
                string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/ClientInfo/frmCaseSummaryPrint.aspx?casenumber=" + Request.QueryString["casenumber"] + "&search=" + Request.QueryString["search"] + "&DisplaySections=" + DisplaySections + "&date=" + DateTime.Now.ToString();
                //website end
                GeneratePDF(path);
                RenderPDF();
                
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        // This method will Generate PDF report by using ABC PDF.
        private void GeneratePDF(string path)
        {
            try
            {
                Doc theDoc = new Doc();
                theDoc.Rect.Inset(0, 0);

                theDoc.Page = theDoc.AddPage();
                theDoc.Rect.Bottom = 50.0;
                theDoc.Rect.Top = 760.0;
                //theDoc.HtmlOptions.BrowserWidth = 800;
                


                int theID;
                theID = theDoc.AddImageUrl(path);
                while (true)
                {
                    theDoc.FrameRect(); // add a black border
                    if (!theDoc.Chainable(theID))
                        break;
                    theDoc.Page = theDoc.AddPage();
                    theID = theDoc.AddImageToChain(theID);
                }
                for (int i = 1; i <= theDoc.PageCount; i++)
                {
                    theDoc.PageNumber = i;
                    theDoc.Flatten();
                }
                string name = Session.SessionID + ".pdf";
                //for website
                ViewState["vFullPath"] = ViewState["vNTPATHDocketPDF"].ToString() + name;
                ViewState["vPath"] = ViewState["vNTPATHDocketPDF"].ToString();
                //website end
                theDoc.Save(ViewState["vFullPath"].ToString().Replace("\\\\","\\"));
                theDoc.Clear();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Display PDF file generated by ABC PDF in a browser. 
        private void RenderPDF()
        {
            try
            {
              
            //    for website
                string vpth = ViewState["vNTPATHDocketPDF"].ToString().Substring((ViewState["vNTPATH1"].ToString().Length) - 1);
                string link = "../DOCSTORAGE" + vpth;
                link = link + Session.SessionID + ".pdf";
                link = link.Replace("\\\\", "\\");

                Response.Redirect(link, false);


                string createdate = String.Empty;
                string currDate = DateTime.Today.ToShortDateString();
                DateTime CurrDate = Convert.ToDateTime(currDate);
                DateTime fileDate = new DateTime();

                // delete all files that are one day old. 
                string[] files = System.IO.Directory.GetFiles(ViewState["vPath"].ToString(), "*.pdf");
                for (int i = 0; i < files.Length; i++)
                {
                    createdate = System.IO.File.GetCreationTime(files[i]).ToShortDateString();
                    fileDate = Convert.ToDateTime(createdate);

                    if (fileDate.CompareTo(CurrDate) < 0)
                    {
                        System.IO.File.Delete(files[i]);
                    }
                }

               
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void lblkbtn_name_Click(object sender, EventArgs e)
        {
            try
            {    //commented by ozair
                //Response.Redirect("ViolationFeeold.aspx?search=" + srch + "&caseNumber=" + TicketID,false); // Go to the ViolationFeeOld page
                Response.Redirect("ViolationFeeold.aspx?search=" + srch + "&caseNumber=" + Convert.ToInt32(ViewState["vTicketId"]), false); // Go to the ViolationFeeOld page
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Format Phone Number like 021-4414-15454544
        
        protected void GV_matter_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (( e.Row.RowType  != DataControlRowType.Header) && ( e.Row.RowType  != DataControlRowType.Footer) )
                {
                    Label lbldes = (Label)e.Row.FindControl("lbl_vdescription");

                    if (lbldes.Text != "")
                    {
                        HtmlControl ctrl = (HtmlControl)e.Row.FindControl("div_status");
                        string descript = lbldes.Text;
                        ctrl.Attributes.Add("Title", "hideselects=[on] offsetx=[-200] offsety=[0] singleclickstop=[on] requireclick=[off] header=[] body=[<table width='225px' border='0' cellspacing='0'  class='label' style='BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none'><tr><td class='Label'>" + descript + "</td></tr></table>]");
                        // ctrl.Attributes.Add("Title", descript);
                    }
                    if (lbldes.Text.Length > 35)
                    {
                        lbldes.Text = lbldes.Text.Substring(0, 35);
                        lbldes.Text += "....";
                    }
                }
            }
            catch(Exception ex)
            {
                lblmsg.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


      
	}
}

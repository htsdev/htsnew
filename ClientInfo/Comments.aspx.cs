﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.Components.ClientInfo;
using HTP.Components.Services;
using lntechNew.WebControls;

namespace HTP.ClientInfo
{
    public partial class Comments : System.Web.UI.Page
    {
        #region variables

        clsLogger bugTracker = new clsLogger();
        DataTable dt = new DataTable();
        DataTable dt_Commtype = new DataTable();
        DataView dvCom = new DataView();
        clsSession ClsSession = new clsSession();
        GridView gvIndex = new GridView();
        PagingControl pg_Index = new PagingControl();
        bool IsSort = false;
        int index;
        string controlName;

        bool IsSearch;

        #endregion

        #region Properites

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;
                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }

        }

        public string GridViewSortExpression
        {
            get
            {
                if (ViewState["sortExpression"] == null)
                    ViewState["sortExpression"] = "";
                return ViewState["sortExpression"].ToString();
            }
            set { ViewState["sortExpression"] = value; }

        }



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    lbl_Message.Text = "";
                    if (!IsPostBack)
                    {
                        if (Request.QueryString["caseNumber"] != null && Request.QueryString["caseNumber"].ToString() == "0")
                            Response.Redirect("ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=0", false);
                        if (Request.QueryString.Count >= 2)
                        {
                            ViewState["vTicketId"] = Request.QueryString["casenumber"];
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }

                        GridViewSortExpression = "sortCommDate";
                        GridViewSortDirection = SortDirection.Descending;



                        CommentService ComtSer = new CommentService();
                        dt_Commtype = ComtSer.GetAllCommentTypes();
                        Session["dtComments"] = ComtSer.GetClientCommentsByTicketID(Convert.ToInt32(ViewState["vTicketId"].ToString())).Tables[0];
                        FillCommentTypes();
                        BindRepeter();

                        tblDateWise1.Attributes.Add("style", "display:none");
                        tblDateWise2.Visible = false;
                        trCommWise.Visible = true;
                        IsSearch = true;
                        BindGrid();
                        IsSearch = false;


                        ViewState["empid"] = ClsSession.GetCookie("sEmpID", this.Request);
                    }

                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);

                    controlName = Request.Params.Get("__EVENTTARGET");
                    if (controlName != null && controlName.Length > 11 && controlName.Substring(0, 11) == "rptComments")
                    {
                        index = Convert.ToInt32(controlName.Substring(15, 2));




                        gvIndex = (GridView)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[5];
                        pg_Index = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[3];

                        pg_Index.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pg_PageIndexChanged);
                        pg_Index.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pg_PageSizeChanged);

                        //pg_Index.Size = lntechNew.WebControls.RecordsPerPage.Ten;

                        pg_Index.GridView = gvIndex;


                    }

                    //SetPaging(index);
                    //PagingEvent(index, gvIndex.PageIndex);

                    Pagingctrl.GridView = gv_Records;
                    ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                    TextBox txt1 = (TextBox)am.FindControl("txtid");
                    txt1.Text = ViewState["vTicketId"].ToString();

                    TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                    txt2.Text = ViewState["vSearch"].ToString();

                SearchPage:
                    { }
                }

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);

            }
        }

        protected void gv_Records_Sorting(object sender, GridViewSortEventArgs e)
        {

            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending && sortExpression == GridViewSortExpression)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            GridViewSortExpression = sortExpression;
            BindGrid();
            //SortGrid(sortExpression);
            //Pagingctrl.PageCount = gv_Records.PageCount;
            //Pagingctrl.PageIndex = gv_Records.PageIndex;
        }

        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                BindGrid();
                //dvCom = (DataView)Session["dvCom"];
                //gv_Records.DataSource = dvCom;
                //gv_Records.DataBind();
            }
            catch (Exception ex)
            {

                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {

            string sortExpression = e.SortExpression;

            if ((SortDirection)ViewState[gvIndex.ClientID + "_SortDirection"] == SortDirection.Ascending && sortExpression == ViewState[gvIndex.ClientID + "_SortExp"].ToString())
            {
                ViewState[gvIndex.ClientID + "_SortDirection"] = SortDirection.Descending;
            }
            else
            {
                ViewState[gvIndex.ClientID + "_SortDirection"] = SortDirection.Ascending;
            }

            ViewState[gvIndex.ClientID + "_SortExp"] = sortExpression;
            IsSort = true;
            PagingEvent(index, gvIndex.PageIndex);

        }

        protected void lnkComm_Click(object sender, EventArgs e)
        {
            if (lnkComm.Text == "Comments Type View")
            {
                lnkComm.Text = "Date Time View";
                tblDateWise1.Attributes.Add("style", "display:none");
                tblDateWise2.Visible = false;
                trCommWise.Visible = true;
                CommentService ComtSer = new CommentService();
                dt_Commtype = ComtSer.GetAllCommentTypes();
                Session["dtComments"] = ComtSer.GetClientCommentsByTicketID(Convert.ToInt32(ViewState["vTicketId"].ToString())).Tables[0];
                BindRepeter();
            }
            else
            {
                lnkComm.Text = "Comments Type View";
                trCommWise.Visible = false;
                tblDateWise1.Attributes.Add("style", "display:block");
                tblDateWise2.Visible = true;
                IsSearch = true;
                BindGrid();
                IsSearch = false;

            }


        }

        protected void btn_lnkAddComm(object sender, EventArgs e)
        {
            FillCommentTypes();
            btnSubmit.Style["display"] = "block";
            btnDummy.Style["display"] = "none";
            MPEComments.Show();
          

        }

        protected void gv_Data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView gv = ((GridView)sender);
                gv.PageIndex = e.NewPageIndex;

                DataView dvv = new DataView((DataTable)Session["dtComments"]);
                dvv.RowFilter = "CommentID=" + "1";
                gv.DataSource = dvv;
                gv.PageSize = 1;
                gv.DataBind();

                PagingControl pg = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1];
                pg.GridView = gv;

                pg.PageIndex = gv.PageIndex;
                pg.PageCount = gv.PageCount;
                pg.SetPageIndex();
                pg.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lbk_next_Click(object sender, EventArgs e)
        {
            int index = Convert.ToInt32((((LinkButton)sender).ClientID).Substring(15, 2));
            int Currentpage = Convert.ToInt32(((Label)((((System.Web.UI.Control)(rptComments.Items[index]))).Controls[3].FindControl("LB_curr"))).Text);
            PagingEvent(index, Currentpage);

        }

        protected void lbk_previous_Click(object sender, EventArgs e)
        {
            try
            {
                int index = Convert.ToInt32((((LinkButton)sender).ClientID).Substring(15, 2));
                int Currentpage = Convert.ToInt32(((Label)((((System.Web.UI.Control)(rptComments.Items[index]))).Controls[3].FindControl("LB_curr"))).Text);
                PagingEvent(index, Currentpage - 2);
                //GridView gv = (GridView)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[3];
                //PagingControl pg = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1];
                //string Currentpage = ((Label)((((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1].FindControl("LB_curr"))).Text;

                //gv.PageIndex = Convert.ToInt32(Currentpage) - 2;

                //DataView dvv = new DataView((DataTable)Session["dtComments"]);
                //dvv.RowFilter = "CommentID=" + "1";
                //gv.DataSource = dvv;
                //gv.PageSize = 1;
                //gv.DataBind();

                //pg.GridView = gv;
                //pg.PageIndex = gv.PageIndex;
                //pg.PageCount = gv.PageCount;
                //pg.SetPageIndex();
                //pg.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lbk_First_Click(object sender, EventArgs e)
        {
            try
            {
                int index = Convert.ToInt32((((LinkButton)sender).ClientID).Substring(15, 2));
                //int Currentpage = Convert.ToInt32(((Label)((((System.Web.UI.Control)(rptComments.Items[index]))).Controls[3].FindControl("LB_curr"))).Text);
                PagingEvent(index, 0);


                //GridView gv = (GridView)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[3];
                //PagingControl pg = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1];
                //string Currentpage = ((Label)((((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1].FindControl("LB_curr"))).Text;

                //gv.PageIndex = 0;

                //DataView dvv = new DataView((DataTable)Session["dtComments"]);
                //dvv.RowFilter = "CommentID=" + "1";
                //gv.DataSource = dvv;
                //gv.PageSize = 1;
                //gv.DataBind();

                //pg.GridView = gv;
                //pg.PageIndex = gv.PageIndex;
                //pg.PageCount = gv.PageCount;
                //pg.SetPageIndex();
                //pg.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void lbk_Last_Click(object sender, EventArgs e)
        {
            try
            {

                int index = Convert.ToInt32((((LinkButton)sender).ClientID).Substring(15, 2));
                PagingControl pg = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[3];
                PagingEvent(index, pg.PageCount);

                //GridView gv = (GridView)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[3];
                //PagingControl pg = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1];
                //string Currentpage = ((Label)((((System.Web.UI.Control)(rptComments.Items[0]))).Controls[1].FindControl("LB_curr"))).Text;

                //gv.PageIndex = pg.PageCount;

                //DataView dvv = new DataView((DataTable)Session["dtComments"]);
                //dvv.RowFilter = "CommentID=" + "1";
                //gv.DataSource = dvv;
                //gv.PageSize = 1;
                //gv.DataBind();

                //pg.GridView = gv;
                //pg.PageIndex = gv.PageIndex;
                //pg.PageCount = gv.PageCount;
                //pg.SetPageIndex();
                //pg.Visible = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (WCC_Comments.TextBox_Text.Length > 0)
                {
                    WCC_Comments.AddComments();
                }

                if (!(lnkComm.Text == "Comments Type View"))
                {
                    CommentService ComtSer = new CommentService();
                    dt_Commtype = ComtSer.GetAllCommentTypes();
                    Session["dtComments"] = ComtSer.GetClientCommentsByTicketID(Convert.ToInt32(ViewState["vTicketId"].ToString())).Tables[0];
                    BindRepeter();
                }
                else
                {
                    IsSearch = true;
                    BindGrid();
                    IsSearch = false;
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
            }
            finally
            {
                btnSubmit.Enabled = true;
            }
        }

        protected void rptComments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gv = (GridView)e.Item.FindControl("gv_CommRecords");
                PagingControl PageCon = (PagingControl)e.Item.FindControl("Pagingctrl_typewise");
                LinkButton btnPrev = (LinkButton)e.Item.FindControl("lnkPrevious");
                LinkButton btnFirst = (LinkButton)e.Item.FindControl("lnkFirst");
                LinkButton btnNext = (LinkButton)e.Item.FindControl("lnkNext");
                LinkButton btnLast = (LinkButton)e.Item.FindControl("lnkLast");
                //HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("tr_CommRecords");

                if (gv != null)
                {
                    DataRowView drv = (DataRowView)e.Item.DataItem;
                    // gv.PageIndexChanging += new GridViewPageEventHandler(gv_Data_PageIndexChanging);
                    DataView dv = GetCommWiseData(Convert.ToInt32(drv["CommentID"]));
                    gv.DataSource = dv;
                    gv.DataBind();

                    ViewState[gv.ClientID + "_SortExp"] = GridViewSortExpression;
                    ViewState[gv.ClientID + "_SortDirection"] = GridViewSortDirection;

                    if (dv.Count <= 0 || gv.PageIndex == gv.PageCount - 1)
                    {
                        btnNext.Visible = false;
                        btnLast.Visible = false;
                    }

                }

                btnFirst.Visible = false;
                btnPrev.Visible = false;

                //if (gv.Rows.Count == 0 || gv.PageCount ==1)
                //{
                //    tr.Visible = false;
                //}

                PageCon.Size = lntechNew.WebControls.RecordsPerPage.Ten;
                PageCon.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pg_PageIndexChanged);
                PageCon.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pg_PageSizeChanged);
                PageCon.GridView = gv;
                PageCon.PageCount = gv.PageCount;
                PageCon.PageIndex = gv.PageIndex;
                PageCon.SetPageIndex();
                PageCon.Visible = true;
            }

        }

        protected void ddlCommentType_SelectedIndexChanged(object sender, EventArgs e)
        {

            WCC_Comments.Initialize(Convert.ToInt32(ViewState["vTicketId"].ToString()), Convert.ToInt32(ViewState["empid"].ToString()), Convert.ToInt32(ddlCommentType.SelectedValue.ToString()), "Label", "clsinputadministration", "clsbutton", "clsLabel", true);
            WCC_Comments.chk_Complaint.Checked = false; //SAEED 7859 06/24/2010 Uncheck isComplaint checkbox everytime when user change 'comment type'.
            WCC_Comments.btn_AddComments.Visible = false;
            if (!(ddlCommentType.SelectedValue == "1" || ddlCommentType.SelectedValue == "3"))
            {
                WCC_Comments.lbl_Check_Complaint.Visible = false;
                WCC_Comments.chk_Complaint.Visible = false;
                WCC_Comments.Label_Visible = false;

            }
            else
            {
                WCC_Comments.lbl_Check_Complaint.Visible = true;
                WCC_Comments.chk_Complaint.Visible = true;
                WCC_Comments.Label_Visible = false;
            }
           

            MPEComments.Show();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hlnk_Edit", "CancelMobel();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideCheckbox", "HideCheckBox();", true);

        }

        #endregion

        #region Method

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void Pagingctrl_PageIndexChanged()
        {
            try
            {
                if (Session["dvCom"] != null)
                {

                    //dvCom = (DataView)Session["dvCom"];
                    gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                    BindGrid();

                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// on page index chage grid page change
        /// </summary>
        void Pg_PageIndexChanged()
        {
            try
            {
                if (Session["dvCom"] != null)
                {

                    //dvCom = (DataView)Session["dvCom"];
                    gvIndex.PageIndex = pg_Index.PageIndex - 1;
                    PagingEvent(index, gvIndex.PageIndex);


                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                {
                    gv_Records.AllowPaging = false;
                }
                BindGrid();
                //dvCom = (DataView)Session["dvCom"];
                //gv_Records.DataSource = dvCom;
                //gv_Records.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// grid size change
        /// </summary>
        /// <param name="pageSize"></param>
        void Pg_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gvIndex.PageIndex = 0;
                    gvIndex.PageSize = pageSize;
                    gvIndex.AllowPaging = true;
                }
                else
                {
                    gvIndex.AllowPaging = false;
                }
                PagingEvent(index, 0);
                //dvCom = (DataView)Session["dvCom"];
                //gv_Records.DataSource = dvCom;
                //gv_Records.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        /// <summary>
        /// get data and bind grid for comment report
        /// </summary>
        public void BindGrid()
        {
            DataSet ds = new DataSet();

            if (IsSearch)
            {
                CommentService ComtSer = new CommentService();
                ds = ComtSer.GetClientCommentsByTicketID(Convert.ToInt32(ViewState["vTicketId"].ToString()));

                Session["dvCom"] = ds.Tables[0].DefaultView;

                if (ds.Tables[1].Rows.Count > 0)
                {
                    lbl_FirstName.Text = ds.Tables[1].Rows[0]["Firstname"].ToString();
                    lbl_LastName.Text = ds.Tables[1].Rows[0]["Lastname"].ToString();
                    lbl_CaseCount.Text = ds.Tables[1].Rows[0]["CaseCount"].ToString();
                    hlnk_MidNo.Text = ds.Tables[1].Rows[0]["Midnum"].ToString().Trim();
                    if (hlnk_MidNo.Text != "")
                    {
                        string casetype = Request.QueryString["search"].ToString();
                        hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=" + casetype + "&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                    }
                }

            }

            if (Session["dvCom"] != null && dt.Rows.Count == 0)
            {
                dvCom = (DataView)Session["dvCom"];
                dt = dvCom.ToTable();

            }
            if (dt.Rows.Count > 0)
            {
                dvCom = dt.DefaultView;
                dvCom.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
                Session["dvCom"] = dvCom;
                gv_Records.DataSource = dvCom;
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
                gv_Records.Visible = true;
                lbl_Message.Text = "";
            }
            else
            {
                Pagingctrl.PageCount = 0;
                Pagingctrl.PageIndex = 0;
                Pagingctrl.SetPageIndex();
                lbl_Message.Text = "No Records Found";
                gv_Records.Visible = false;
            }

        }

        /// <summary>
        /// Fill comment types in dropdownlist
        /// </summary>
        private void FillCommentTypes()
        {
            CommentService ComtSer = new CommentService();
            dt_Commtype = ComtSer.GetAllCommentTypes();
            ClsFlags clsflag = new ClsFlags();
            clsflag.TicketID = Convert.ToInt32(ViewState["vTicketId"]);
            clsflag.FlagID = 9;
            if (!clsflag.HasFlag())
            {
                dt_Commtype.Rows.RemoveAt(3);
            }
            Session["dt_Commtype"] = dt_Commtype;
            if (dt_Commtype.Rows.Count > 0)
            {
                CommentService CommSer = new CommentService();
                ddlCommentType.DataSource = dt_Commtype;
                ddlCommentType.DataBind();
                ListItem lstCaseType = new ListItem("Select", "-1");
                ddlCommentType.Items.Insert(0, lstCaseType);
            }
            WCC_Comments.Style["display"] = "none";
            WCC_Comments.Label_Visible = false;


        }

        /// <summary>
        /// bind comment wise data in repeter
        /// </summary>
        private void BindRepeter()
        {
            rptComments.DataSource = (DataTable)Session["dt_Commtype"];
            rptComments.DataBind();
        }

        /// <summary>
        /// filter data by given comment id
        /// </summary>
        /// <param name="CommentID"></param>
        /// <returns></returns>
        public DataView GetCommWiseData(int CommentID)
        {
            DataView dv = new DataView((DataTable)Session["dtComments"]);
            dv.RowFilter = "CommentID=" + CommentID;
            dv.Sort = GridViewSortExpression + " " + (GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC");
            return dv;

        }

        /// <summary>
        /// rebind all comment wise grid to maintain state of paging and size
        /// </summary>
        /// <param name="index"></param>
        /// <param name="Currentpage"></param>
        public void PagingEvent(int index, int Currentpage)
        {
            try
            {
                GridView gv = (GridView)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[5];
                PagingControl pg = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[3];
                //HtmlTableRow tr = (HtmlTableRow)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[7];

                LinkButton btnFirst = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[7];
                LinkButton btnPrev = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[9];
                LinkButton btnNext = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[11];
                LinkButton btnLast = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[index]))).Controls[13];

                gv.PageIndex = Currentpage;

                dt_Commtype = (DataTable)Session["dt_Commtype"];
                DataView dvv = new DataView((DataTable)Session["dtComments"]);

                dvv.RowFilter = "CommentID=" + (Convert.ToInt32(dt_Commtype.Rows[index][0])).ToString();
                dvv.Sort = ViewState[gv.ClientID + "_SortExp"] + " " + ((SortDirection)ViewState[gv.ClientID + "_SortDirection"] == SortDirection.Ascending ? "ASC" : "DESC");
                gv.DataSource = dvv;
                //gv.PageSize = 1;
                gv.DataBind();

                pg.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                pg.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);

                pg.GridView = gv;
                pg.PageIndex = gv.PageIndex;
                pg.PageCount = gv.PageCount;
                pg.SetPageIndex();
                pg.Visible = true;

                if (gv.PageIndex > 0 && gv.PageIndex < gv.PageCount - 1)
                {
                    btnFirst.Visible = true;
                    btnPrev.Visible = true;
                    btnNext.Visible = true;
                    btnLast.Visible = true;
                }

                if (gv.PageIndex <= 0)
                {
                    btnFirst.Visible = false;
                    btnPrev.Visible = false;
                }
                else if (gv.PageIndex > 0)
                {
                    btnFirst.Visible = true;
                    btnPrev.Visible = true;
                }


                if (gv.PageIndex == gv.PageCount - 1)
                {
                    btnNext.Visible = false;
                    btnLast.Visible = false;
                }
                else if (gv.PageIndex < gv.PageCount - 1)
                {
                    btnNext.Visible = true;
                    btnLast.Visible = true;

                }

                if (gv.Rows.Count == 0)
                {
                    btnFirst.Visible = false;
                    btnPrev.Visible = false;
                    btnNext.Visible = false;
                    btnLast.Visible = false;
                }
                //if (gv.Rows.Count == 0 || gv.PageCount == 1)
                //{
                //    tr.Visible = false;
                //}

                SetPaging(index);



            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }



        }

        /// <summary>
        /// rebind all comment wise grid to maintain state of paging and size except given index
        /// </summary>
        /// <param name="index"></param>
        public void SetPaging(int index)
        {

            for (int i = 0; i < dt_Commtype.Rows.Count; i++)
            {
                if (i != index)
                {
                    GridView gv_CommentType = (GridView)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[5];
                    PagingControl pg_test = (PagingControl)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[3];
                    //HtmlTableRow tr = (HtmlTableRow)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[7];

                    LinkButton btnFirst = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[7];
                    LinkButton btnPrev = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[9];
                    LinkButton btnNext = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[11];
                    LinkButton btnLast = (LinkButton)(((System.Web.UI.Control)(rptComments.Items[i]))).Controls[13];
                    //gv_test.PageSize = 1;

                    DataView dvv_test = new DataView((DataTable)Session["dtComments"]);

                    dvv_test.RowFilter = "CommentID=" + (Convert.ToInt32(dt_Commtype.Rows[i][0])).ToString();
                    if (!IsSort)
                    {
                        dvv_test.Sort = ViewState[gv_CommentType.ClientID + "_SortExp"] + " " + ((SortDirection)ViewState[gv_CommentType.ClientID + "_SortDirection"] == SortDirection.Ascending ? "ASC" : "DESC");
                    }
                    else
                    {
                        dvv_test.Sort = ViewState[gv_CommentType.ClientID + "_SortExp"] + " " + ((SortDirection)ViewState[gv_CommentType.ClientID + "_SortDirection"] == SortDirection.Ascending ? "ASC" : "DESC");

                    }
                    gv_CommentType.DataSource = dvv_test;
                    gv_CommentType.DataBind();


                    pg_test.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pg_PageIndexChanged);
                    pg_test.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pg_PageSizeChanged);

                    pg_test.GridView = gv_CommentType;
                    pg_test.PageIndex = gv_CommentType.PageIndex;
                    pg_test.PageCount = gv_CommentType.PageCount;
                    pg_test.SetPageIndex();

                    if (gv_CommentType.PageIndex > 0 && gv_CommentType.PageIndex < gv_CommentType.PageCount - 1)
                    {
                        btnFirst.Visible = true;
                        btnPrev.Visible = true;
                        btnNext.Visible = true;
                        btnLast.Visible = true;
                    }
                    else if (gv_CommentType.PageIndex <= 0)
                    {
                        btnFirst.Visible = false;
                        btnPrev.Visible = false;
                    }

                    if (gv_CommentType.PageIndex == gv_CommentType.PageCount - 1)
                    {
                        btnNext.Visible = false;
                        btnLast.Visible = false;
                    }
                    else if (gv_CommentType.PageIndex < gv_CommentType.PageCount - 1)
                    {
                        btnNext.Visible = true;
                        btnLast.Visible = true;

                    }

                    if (gv_CommentType.Rows.Count == 0)
                    {
                        btnFirst.Visible = false;
                        btnPrev.Visible = false;
                        btnNext.Visible = false;
                        btnLast.Visible = false;
                    }
                    //if (gv_CommentType.Rows.Count == 0 || gv_CommentType.PageCount == 1)
                    //{
                    //    tr.Visible = false;
                    //}
                    if ((Convert.ToInt32(dt_Commtype.Rows[i][0])).ToString() == "6")
                    {
                        gv_CommentType.Visible = false;
                    }
                }
            }

        }


        #endregion

    }
}

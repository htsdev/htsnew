using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
namespace lntechNew.ClientInfo
{
    public partial class frmSPNSearch : System.Web.UI.Page
    {
        private string SPNUserName, SPNPassword;
        private clsGeneralMethods cls_gen = new clsGeneralMethods();
        clsUser ur = new clsUser();
        clsSession objSession = new clsSession();

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                ur.LoginID = objSession.GetCookie("sUserID", this.Request);
                ur.Password = objSession.GetCookie("Password", this.Request);
                ur.AuthenticateUser();
                SPNUserName = ur.UserNameSPN;
                SPNPassword = ur.PasswordSPN;

                if (Request.QueryString["casesearch"] != null)
                {
                    Response.Write(cls_gen.GetCaseNoSearchPage(SPNUserName, SPNPassword, Request.QueryString["SPN"], Request.QueryString["cdi"]));
                }
                else
                {
                    Response.Write(cls_gen.GetSPNSearchPage(SPNUserName, SPNPassword, Request.QueryString["SPN"]));
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}

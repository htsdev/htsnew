using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace lntechNew.ClientInfo
{
    public partial class Popup_Options : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btn_Cancel.Attributes.Add("onclick", "Result(0);");
            btn_OK.Attributes.Add("onclick", "Result(1);");

            if (!IsPostBack)
            {
                lbl_Type.Text = Request.QueryString["LetterType"];
            }
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourtDetail.aspx.cs" Inherits="HTP.ClientInfo.CourtDetail" EnableViewState ="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Court Detail</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
      <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAp3emSG1MlsDZIjgNlK3U-xQZ9FmlFZPu9FT8VFF5QiOydCV_IxTezfuonxslcx2coXLBwyvSxsOBZg"
        type="text/javascript"></script>

    <style type="text/css">
        v:
        {
            behavior: url(#default#VML);
        body
        {
            font-family: Verdana, Arial, sans serif;
            font-size: 11px;
            margin: 2px;
        }
        table.directions th
        {
            background-color: #EEEEEE;
        }
        img
        {
            color: #000000;
        }
        
        .style13
        {
            border-width: 0;
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            text-align: left;
            width: 96px;
        }
        .style14
        {
            border-width: 0;
            font-family: Tahoma;
            font-size: 8pt;
            color: #123160;
            text-align: left;
            width: 25%;
        }
    </style>

   

</head>
<body onload="load()" onunload="GUnload()">
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>

        <section id="main-content-popup" class="">
                <section class="wrapper main-wrapper row" style="">
                    
                    
                    <div class="col-xs-12">
                         <div class="page-title">
                
                              <h1 class="title">Court Processing Detail</h1>
                        </div>
                        </div>

                      <div class="clearfix"></div>

                    <div class="col-xs-12">
                         
                        <section class="box ">
                       
                               <div class="content-body">
                                   <div class="row">
                                         <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label"> Court Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblCourtName" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label"> Court Address</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblCourtAddress" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>

                                       <div class="clearfix"></div>

                                       <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label"> Phone No</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblCourtPhoneNo" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>


                            
                            </div>
                                   </div>
                             
                             



                             </section>

                        <div class="clearfix"></div>

                        <ajaxToolkit:TabContainer ID="Tabs" runat="server" ActiveTabIndex ="1">
                        <ajaxToolkit:TabPanel ID="pnl_ByDescription" CssClass="btn btn-primary" runat="server" HeaderText="Case Processing Detail"
                            TabIndex="0">
                            <ContentTemplate>

                                <section class="box ">
                                     <header class="panel_header">
                            <h2 class="title pull-left">Case Processing Detail</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                       
                               <div class="content-body">
                                   
                                   <div class="row">




                                         <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Court Contact</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblCourtContact" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  County</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblcountyname" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>

                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Case Identification</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblCaseIdentification" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Accept Appeals</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblAcceptAppeals" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>

                                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Appeal Location</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblAppealLocation" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Initial Setting</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblInitialSetting" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>

                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  LOR</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblLOR" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Appearance Hire</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblAppearanceHire" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>


                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Judge Trial Hire</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblJudgeTrialHire" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Jury Trial Hire</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblJuryTrialHire" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>

                                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Bond Type</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblBondType" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">   Handwriting Allowed</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblHandwritingAllowed" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>

                                       
                                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Additional FTA Issued</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblAdditinalFTAIssued" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">   Grace Period</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblGracePeriod" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>

                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Continuance</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblContinuance" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">   Supporting Documents</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblSupportingDocuments" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>
                                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">   Comments</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                       <asp:Label ID="lblComments" runat="server" CssClass="form-label" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                      <div class="clearfix"></div>


                                       </div>

                                   </div>
                             </section>

                                        <div class="clearfix"></div>

                                <section class="box ">
                                     <header class="panel_header">
                            <h2 class="title pull-left">Uploaded Files</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                       
                               <div class="content-body">
                                   
                                   <div class="row">

                                         <div class="col-md-12">
                                <div class="form-group">
                                    <%--<label class="form-label">  Court Contact</label>--%>
                                    <span class="desc"></span>
                                    <div class="controls">
                                         <asp:GridView ID="gvCourtFiles" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                Font-Names="Verdana" Font-Size="2px" OnRowDataBound="gvCourtFiles_RowDataBound"
                                                Width="100%">
                                                <FooterStyle CssClass="GrdFooter" />
                                                <AlternatingRowStyle BackColor="#EEEEEE" />
                                                <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-CssClass="GrdLbl" HeaderStyle-CssClass="GrdHeader" DataField="Uploaded Date"
                                                        HeaderText="Uploaded Date" />
                                                    <asp:BoundField ItemStyle-CssClass="GrdLbl" HeaderStyle-CssClass="GrdHeader" DataField="DocumentName"
                                                        HeaderText="Document Name" />
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgPreviewDoc" runat="server" ImageUrl="../images/Preview.gif"
                                                                Width="19" />
                                                            <%--<asp:HiddenField ID="hfFileId" runat="server" Value='<%# bind("id") %>' />--%>
                                                            <asp:HiddenField ID="hfFileId" runat="server" Value='' />
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                                              
                                    </div>
                                </div>
                            </div>

                                       </div>
                                   </div>
                                    </section>
                                       

                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="pnl_ByDescriptione" CssClass="btn btn-primary" runat="server" HeaderText="Map" TabIndex="1">
                            <ContentTemplate>
                                
                                <section class="box ">
                                     
                       
                               <div class="content-body">
                                   
                                   <div class="row">

                                         <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Directions</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <div id="directions" style="width: 250px">
                                            </div>
                                        </div>
                                    </div>
                                             </div>

                                       <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">  Map</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <div id="map" style="width: 310px; height: 400px">
                                            </div>
                                        </div>
                                    </div>
                                             </div>
                                       <div class="clearfix"></div>
                                        <div class="col-md-6">
                                <div class="form-group">
                                    
                                    <span class="desc"></span>
                                    <div class="controls">
                                         <asp:Button ID="btn_Close" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="window.close();"
                                            Width="94px"></asp:Button>
                                        
                                        </div>
                                    </div>
                                             </div>




                                       </div>
                                   </div>
                                    </section>








                                </ContentTemplate>
                                </ajaxToolkit:TabPanel>
                            </ajaxToolkit:TabContainer>

                         





                        <asp:HiddenField ID="hfClientAddress" runat="server" />
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="clsLabel"
                ForeColor="Red"></asp:Label>





                        </div>
















                    </section>
            </section>











  
    </form>
     <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
     <script type="text/javascript">
    //<![CDATA[
    
    //Kazim 4071 5/21/2008 Get Client and Court Addresses 
 
    var ClientAddress = document.getElementById("hfClientAddress").value;
    var CourtAddress =  document.getElementById("lblCourtAddress").innerText;
   
    var map;
    var gdir;
    var geocoder = null;
    var addressMarker;
    

    function OpenPopUpNew(path)  //(var path,var name)
    {
	    
	    window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
	    return false;
	}
	
    function querySt(ji)
    {
        hu = window.location.search.substring(1);
        gy = hu.split("&");
        for (i=0;i<gy.length;i++)
        {
            ft = gy[i].split("=");
            if (ft[0] == ji)
            {return ft[1];}
        }
    }

    function load() {
      
      if (GBrowserIsCompatible()) {      
        map = new GMap2(document.getElementById("map"));

        gdir = new GDirections(map, document.getElementById("directions"));
        GEvent.addListener(gdir, "load", onGDirectionsLoad);
        GEvent.addListener(gdir, "error", handleErrors);

        setDirections(ClientAddress, CourtAddress, "en_US");
      }
    }
    
    function setDirections(fromAddress, toAddress, locale) {
      gdir.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": locale });
    }

    function handleErrors(){
        if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS) {
            $("#txtErrorMessage").text("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
            $("#errorAlert").modal();
        }
            //alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
        else if (gdir.getStatus().code == G_GEO_SERVER_ERROR){

            $("#txtErrorMessage").text("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
            $("#errorAlert").modal();
            //alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
        }
           
        else if (gdir.getStatus().code == G_GEO_MISSING_QUERY) {

            $("#txtErrorMessage").text("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);
            $("#errorAlert").modal();
            //alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);
        }
        else if (gdir.getStatus().code == G_GEO_BAD_KEY) {
            $("#txtErrorMessage").text("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);
            $("#errorAlert").modal();
            //   alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);
        }

        else if (gdir.getStatus().code == G_GEO_BAD_REQUEST) {
            $("#txtErrorMessage").text("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
            $("#errorAlert").modal();
            //   alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
        }

        else {
            $("#txtErrorMessage").text("An unknown error occurred.");
            $("#errorAlert").modal();
            //alert("An unknown error occurred.");
        }
	   
	}

	function onGDirectionsLoad(){ 
          // Use this function to access information about the latest load()
          // results.

          // e.g.
	  // document.getElementById("getStatus").innerHTML = gdir.getStatus().code;
	  // and yada yada yada...
	}


    //]]>
    </script>

    <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

</body>
</html>

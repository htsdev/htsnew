using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;

namespace lntechNew.ClientInfo
{
    public partial class MailerInfo : System.Web.UI.Page
    {
        clsENationWebComponents clsDB = new clsENationWebComponents();
        clsLogger bugTracker = new clsLogger();
        clsCase cCase = new clsCase();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //added by ozair
                if (!IsPostBack)
                {
                    DataTable dt = cCase.GetMailerDetail(Convert.ToInt32(Request.QueryString["TicketID"]));
                    //modified by ozair
                    if (dt.Rows.Count > 0)
                    {
                        lbl_Name.Text = dt.Rows[0]["OfficerName"].ToString().Trim();
                        lbl_UploadDate.Text = dt.Rows[0]["UploadDate"].ToString().Trim();
                        lbl_MailerDate.Text = dt.Rows[0]["MailDate"].ToString().Trim();
                        lbl_MailerID.Text = dt.Rows[0]["MailerID"].ToString().Trim();
                        lbl_LetterType.Text = dt.Rows[0]["LetterName"].ToString().Trim();
                        if (lbl_MailerID.Text == "0")
                            lbl_MailerID.Text = "Not Available";

                        dt=cCase.GetAllMailerLetter(Convert.ToInt32(Request.QueryString["TicketID"]));
                        
                        gv_ShowAllLettres.DataSource = dt;
                        gv_ShowAllLettres.DataBind();
                        
                    }
                    else
                    {
                        lblMessage.Text = "No records found.";
                    }

                    //Kazim 3938 6/9/2008 Get Traffic Alert Information for the client 

                    DataSet dsCaseInfo=cCase.GetCase(Convert.ToInt32(Request.QueryString["TicketID"]));
                    if (dsCaseInfo.Tables.Count > 0 && dsCaseInfo.Tables[0].Rows.Count > 0 && dsCaseInfo.Tables[0].Columns.Count > 0)
                    {
                        if (dsCaseInfo.Tables[0].Rows[0]["IsTrafficAlert"].ToString() == "True" && dsCaseInfo.Tables[0].Rows[0]["ActiveFlag"].ToString()=="1")
                        {
                       
                            trLetterType.Style[HtmlTextWriterStyle.Display] = "none";
                            trMailDate.Visible=false;
                            trMailerId.Visible=false;
                            trTrafficAlert.Visible=true;
                            lblTrafficAlert.Text = "Hired from Traffic Alert";
                        }
                        if (dsCaseInfo.Tables[0].Rows[0]["IsTrafficAlert"].ToString() == "True" && dsCaseInfo.Tables[0].Rows[0]["ActiveFlag"].ToString() == "0")
                        {

                            trLetterType.Style[HtmlTextWriterStyle.Display] = "none";
                            trMailDate.Style[HtmlTextWriterStyle.Display] = "none";
                            trMailerId.Style[HtmlTextWriterStyle.Display] = "none";
                            trTrafficAlert.Visible=true;
                            lblTrafficAlert.Text = "Move from Traffic Alert";
                        }
                    }
                                     
                   
                   
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
    }
}

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.ClientInfo.PaymentInfo" smartNavigation="False" Codebehind="PaymentInfo.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Payment Info</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/validatecreditcard.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		// IF CreditCard OPTION IS SELECTED THAN SHOW THE Credit INFO Table ......
        
        //--Adil
		function OpenDocumentSelector(ticketid)
        {
        //alert(ticketid);
        window.open ('frmDocumentSelector.aspx?ticketid=' + ticketid , '', 'status=no,left=280,top=500,height=130, width=490,scrollbars=no'); 
        return false;
        }
        //--Adil
        
        function ValidateBatchPrintForHMC()
        {
            var courtid=document.getElementById("lbl_courtid").innerText;
				if (courtid==3001 ||courtid==3002 || courtid==3003)
				{
				
				}
        }
        
		function MaxLength()
		{
		if(document.getElementById("txt_paycomments").value.length >300)
		{
		alert("The length of character is not in the specified Range");
		document.getElementById("txt_paycomments").focus();
		}
		}
		 function ValidateLenght()
	     { 
			var cmts = document.getElementById("txt_paycomments").value;
			if (cmts.length > 300)
			{
			    event.returnValue=false;
                event.cancel = true;
			}
	    }
		function hideyds()
		{		
			document.getElementById("lbl_yds").style.display="none";		
			//added By Ozair
					
		 if(document.getElementById("ddl_paymentmethod").value=='5')
			ShowHideCreditDetail();	
			//
		}
		
		function ShowHideCreditDetail()
		{      
		
	           var td=document.getElementById("tblcreditcard");
	           //var txt=document.getElementById("txt_occupation");			
	    	 if(document.frmpayment.ddl_paymentmethod.value=='5')	
				{					    
				    td.style.display = 'block';
					//txt.value ='';								
			    }
			else
			    {			   
			      td.style.display ='none';			      	
				}											
		}
		//In order to select appropriate card by entering num		
		function SelectCard()
		{
		//a;
			var cardnum=document.getElementById("txt_cardno").value;
			var ddl_cardtype=document.getElementById("ddl_cardtype");
			
			if (cardnum.charAt(0)==4)
			{
				ddl_cardtype.value=	1			
			}	
			if (cardnum.charAt(0)==5)
			{
				ddl_cardtype.value=	2			
			}		
			if (cardnum.charAt(0)==3)
			{
				ddl_cardtype.value=	3			
			}	
			if (cardnum.charAt(0)==6)
			{
				ddl_cardtype.value=	4			
			}	
		}
		
		//IF Case Status Is PreTrial then show the pretrial radio buttons 
		function ShowHidePreTrialTable()
		{   
		//a;        
			   //var btn =document.getElementById("btn_emailrep");
	           var table1=document.getElementById("tblpretrial");	           
	           var flag	=document.getElementById("lbl_pretrialflag").innerText;			
	                
	    	 if(flag!='0')	
				{			
					//alert(flag);		    
				    table1.style.display = 'block';												
			    }
			else
			    {	
			      //alert(btn.value); 		   
			      table1.style.display ='none';			      				     
			      //btn.disabled=true;			      
				}			
		}
		// Code by Maria For Batch Letter
		function BatchLetter()
		{
			var retval = confirm("Do you want to send this letter in batch.Press OK to Yes and Cancel to No");
			if (retval)
			return true;
			else
			return false;
		}
		
		//To Open Reports
		function PrintLetter(flag)
		{
		  if(flag==1)
		  {
			//alert(flag)			
			OpenPopUp("frmCaseSummary.aspx?casenumber=<%=ViewState["vTicketId"]%>&search=<%=ViewState["vSearch"]%>");	
			return false;	
		  }
		  //for EmailTrial leter..
		  if(flag == 9)
		  { 
		    var retBatch=false; 
		    
		   OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
		   return false;
				
		  }
		  if(flag==2)
		  {	 
		  var retBatch=false;
		  var option = confirm("Trial Noticiation Letter generated. Would you like to 'PRINT NOW' or 'SEND TO BATCH'?\n Click [OK] to print now or click [CANCEL] to send to batch."); 
		  if (option  )
		    retBatch =false;
     	  else
		    retBatch="";
		    
		  var courtid = document.getElementById("lbl_court").innerText;
	            var flg_split = document.getElementById("lbl_IsSplit").innerText;
	            var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerText;
		  OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&CourtID="+courtid+"&flg_split="+flg_split+"&flg_print="+flg_print );	
		  /*
		        var WinSettings = "center:yes;resizable:no;dialogHeight:220px;dialogwidth:350px;scroll:no";
                var Arg = 1;*/
                
		    //  var retBatch = BatchLetter();
			
	//		if (document.getElementById("lbl_trialbuttonflag").innerText==0)
	//		{	
		
		/*
	           	var courtid = document.getElementById("lbl_court").innerText;
	            var flg_split = document.getElementById("lbl_IsSplit").innerText;
	            var flg_print = document.getElementById("lbl_IsAlreadyInBatchPrint").innerText;
	            var SplitContinue; //= true;
	            var PrintContinue;// = true;
	           
	     */
	           
	          /*  if(flg_split==1)
	           {
	                SplitContinue = confirm("The Trial Notification Letter you are printing has a split court date, time or room number. Would you like to continue?");
	           }
	           if(SplitContinue==false)
	           return;
	            */
	          
	           /*  Farhan
				if (retBatch == true)
				 {  
				 
				    if(flg_print==1)
	                    {
	                        PrintContinue = confirm("Case number "+courtid +" already exists in batch print queue. Would you like to override the existing letter with this new letter");
	                    }
	                if(PrintContinue==false)
	                    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=0");	
	                else
				        OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=1");	
				 
				}
				 else 
				 {
			//	alert("here");
				 OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");	
				 //location.reload(true);
				
				}			 
			 */ //End
			 
			 
			 /*
			 var WinSettings = "center:yes;resizable:no;dialogHeight:220px;dialogwidth:350px;scroll:no";
			    
                var Arg = 1;
                var url = "Popup_Options.aspx?LetterType=TrialNotification&SplitFlag="+flg_split+"&PrintFlag="+flg_print;
                
			    var conf = window.showModalDialog(url,Arg,WinSettings);
			    
			    if(conf == 1)
			    {
			    OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=false&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>");
			    }
			    if(conf == 2)
			    {
			    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=0");	
			    }
			    if(conf == 3)
			    {
			    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" +"&Exists=1");	
			    }
			    if(conf == 4)
			    {
			    OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=false&casenumber=<%=ViewState["vTicketId"]%>" + "&flag=" + flag + "&emailaddress=<%=ViewState["emailid"]%>"+"&EmailFlag=1");
			    }
			   */ 
			   
	//		}
	/*
			else
			{
			 var x=confirm("There are conflicting court dates with this Trial Letter. Please verify this is correct. Press [ OK ] to Continue");
				if (x)
				{
					if (retBatch == true)
					{  
					OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
					}
					else 
					{
					OpenPopUp("../Reports/Word_TrialNotification.aspx?Batch=" + retBatch);	
				//	location.reload(true);
					}			 
				}
		   }
		   */
		   return false;
		  }						
			if(flag==3)
		    {	
			//	var retBatch = BatchLetter();
			//	if (retBatch == true)
			//	{  
			//	OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
			//	}	
			//	else 
			//	{  
						  
				//OpenPopUp("../Reports/word_receipt.aspx?casenumber=<%=ViewState["vTicketId"]%>");
				window.open("../Reports/word_receipt.aspx?casenumber=<%=ViewState["vTicketId"]%>",'',"left=150,top=50,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");	
				

			//	}
			    //End
			   /* Commented by Farhan Sabir
			    var WinSettings = "center:yes;resizable:no;dialogHeight:220px;dialogwidth:350px;scroll:no";
                var Arg = 1;
			    var conf = window.showModalDialog("Popup_Options.aspx?LetterType=Receipt",Arg,WinSettings);
			    if(conf == 1)
			    {
			    OpenPopUp("../Reports/word_receipt.aspx?casenumber=" + <%=ViewState["vTicketId"]%>+ "&EmailFlag=0");
			    }
			    if(conf == 2)
			    {
			    OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=true&LetterID=" + flag + "&casenumber=<%=ViewState["vTicketId"]%>" );	
			    }
			    if(conf == 3)
			    {
			    OpenPopUp("../Reports/word_receipt.aspx?casenumber="+ <%=ViewState["vTicketId"]%>+ "&EmailFlag=1");
			    }
			    */ //End
			return false;	
			}		
			if(flag==4)
			{	
			var courtid=document.getElementById("lbl_courtid").innerText;
				if (courtid==3001 ||courtid==3002 || courtid==3003)
				{
				  OpenPopUp("../Reports/Word_Bond.aspx?CourtType=0" + "&casenumber=<%=ViewState["vTicketId"]%>");	
				}
				else
				{
				  OpenPopUp("../Reports/Word_Bond.aspx?CourtType=1" + "&casenumber=<%=ViewState["vTicketId"]%>");	
				}		
			return false;	
			}			
			if(flag==5)
		    {	 /*var retBatch = BatchLetter();
			//alert(flag)
				if (retBatch == true)
				{  
				OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
				}	
				else 
				{  */
				OpenPopUp("../Reports/FrmMainCONTINUANCE.aspx?Batch=" + retBatch + "&casenumber=<%=ViewState["vTicketId"]%>");	
				//}
			return false;	
			}
		//	if(flag==6)
		  //  {	
			//alert(flag)		//letter of rep
//			OpenPopUp("../Reports/main.aspx");	
//			return false;	
//			}
			if(flag==6)
		    {	 //var retBatch = BatchLetter();
				/*if (retBatch == true)
				{  
				OpenPopUpSmall("../Reports/frmBatchEntry.aspx?Batch=" + retBatch + "&LetterID=" + flag);	
				}
				else 
				{	*/
				
				OpenPopUp("../Reports/FrmMainLetterofRep.aspx?Batch=" + retBatch + "&lettertype=6&casenumber=<%=ViewState["vTicketId"]%>");	
				//}
			return false;	
			}
			
			if(flag==11)
		    {	
		        OpenPopUp("../Reports/FrmMainLetterofRep.aspx?Batch=" + retBatch + "&lettertype=7&casenumber=<%=ViewState["vTicketId"]%>");	
				return false;	
			}
			if(flag==12)
		    {	
				OpenPopUp("../Reports/FrmMainLetterofRep.aspx?Batch=" + retBatch + "&lettertype=8&casenumber=<%=ViewState["vTicketId"]%>");	
				return false;	
			}
			if(flag==8)
		    {				    		    
			OpenPopUp("../../reports/Payment_Receipt_Contract.asp?TicketID=<%=ViewState["vTicketid"]%>");	
			return false;	
			}
			/*			
			if(flag==9)
		    {	
			//alert(flag)		
			OpenPopUp("../Reports/Word_TrialNotification.aspx");	
			return false;	
			}
			*/		  
		}
		
		function OpenPopUp(path)  //(var path,var name)
		{
		
		 window.open(path,'',"height=505,width=600,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		  //return true;
		}
		function OpenPopUpSmall(path)  //(var path,var name)
		{
		
		 window.open(path,'',"fullscreen=no,toolbar=no,width=2,height=2,left=0,top=0,status=no,menubar=no,resizable=no");
		  //return true;
		}
		
		function OpenSchedule(owes,schid,activeflag)
		{
			if (document.getElementById("lbl_lockflag").innerText=="False")
			{
				alert("Please First 'Lock' the Price on Violation Fees Page");
				return false;			
			}			
			if (document.getElementById("lbl_contactcheck").innerText=="False")
			{
				alert("Please first specify client Contact Num(s) on General Info Page");
				return false;			
			}

//          // commented .... as we have implemented 'No DL' option on general info page.				
//			if (document.getElementById("lbl_dlstate").innerText=="0" && document.getElementById("lbl_activeflag").innerText=="0" )
//			{
//				alert("Please Specify DL State on General Info Page ");	
//				return false;			
//			}
			
			if (document.getElementById("lbl_addresscheck").innerText=="0")
			{
				alert("Please verify the Address Check Box on General Info Page");	
				return false;			
			}	
		
				var PDFWin
			    PDFWin = window.open("SchedulePayment.aspx?owes="+owes+"&schid="+schid+"&activeflag="+activeflag+"&casenumber=<%=ViewState["vTicketId"]%>",'',"fullscreen=no,toolbar=no,width=370,height=165,left=100,top=300,status=no,menubar=no,scrollbars=yes,resizable=yes");				
				//window.history.back(); 					
				return false;
		}
		
		function ValidateForm()
		{
		
		    if (document.getElementById("txt_paycomments").value.length >500)
		    {
				alert("Too Many Characters. Please type in up to 500 characters.")
				return false;
		    }
		  
		}
		//Checking fields required for process amount 
		function ProcessAmount()
		{		
				//Added By Ozair
				if(document.getElementById("lbl_ToProcess").innerText!="0")
				{
				    alert("Payment cannot be processed! Case status of underlying violation(s) has not been set.")
				    return false;
				}
			<%Session["TimeStemp"]=txtTimeStemp.Text;%>;
			//
			if (document.getElementById("lbl_lockflag").innerText=="False")
			{
				alert("Please First 'Lock' the Price on Violation Fees Page");
				return false;			
			}			
			if (document.getElementById("lbl_contactcheck").innerText=="False")
			{
				alert("Please first specify client Contact Num(s) on General Info Page");
				return false;			
			}

//            // commented .... as we have implemented 'No DL' option on general info page.				
//			if (document.getElementById("lbl_dlstate").innerText=="0" && document.getElementById("lbl_activeflag").innerText=="0" )
//			{
//				alert("Please Specify DL State on General Info Page ");	
//				return false;			
//			}
			
			if (document.getElementById("lbl_addresscheck").innerText=="0")
			{
				alert("Please verify the Address Check Box on General Info Page");	
				return false;			
			}
			
					
								
			//Amount Related
			var intamount=0;
			var intowes=0;
			intamount=document.getElementById("txt_amount").value;
			intowes=(document.getElementById("lbl_owes").innerText)*1;			
			
			if (isNaN(document.getElementById("txt_amount").value) == false && intamount!="" && intamount !=0  )
			{			
				if( intamount > intowes &&(document.getElementById("ddl_paymentmethod").value!=8) )
				{ 
					alert ("Incorrect payment amount. A client cannot pay more than he owes");
					document.getElementById("txt_amount").focus(); 
					return false;				
				}
				if(intamount<0)
				{
					alert ("Incorrect payment amount. A client cannot owe a negative");
					document.getElementById("txt_amount").focus(); 
					return false;				
				}				
			}
			else
			{
					alert ("Incorrect payment amount.");
					document.getElementById("txt_amount").focus(); 
					return false;							
			}	
			
			if (document.getElementById("ddl_paymentmethod").value==0)
			{
				alert ("Please Specify your payment method.");
				document.getElementById("ddl_paymentmethod").focus(); 
				return false;				
			}
			var paid=(document.getElementById("lbl_paid").innerText)*1;		
			if (document.getElementById("ddl_paymentmethod").value==8 && paid < intamount )
			{
				alert ("Cannot Refund more than the amount paid.");
				document.getElementById("txt_amount").focus(); 
				return false;				
			}					
					//credit card related
			if (document.getElementById("ddl_paymentmethod").value==5)
			{
			   if(document.getElementById("txt_name").value=="")
			   {
				alert ("Please enter the credit card holder's name")		
				document.getElementById("txt_name").focus(); 
				return false;				
			   }
			   if(document.getElementById("txt_cardno").value=="")
			   {
				alert ("Please enter your credit card number.")
				document.getElementById("txt_cardno").focus(); 
				return false;				
			   }
			   if(document.getElementById("ddl_cardtype").value=="--Choose--")
			   {
				//alert ("Please enter the credit card type.")	   
				//document.getElementById("ddl_cardtype").focus(); 
				//return false;				
			   }	
			   //CALLING FUNCTION TO VALIDATE CARD NUMBER
			   
			   if (CheckCardNumber(document.forms[0])==false)		   
			   return false; 			   
			    //added by ozair
			   if(document.getElementById("ddl_paymentmethod").value!='5')
					HideCreditDetail();
			//
			   if (document.getElementById("lbl_yds").innerText=="N")
				{			
					doyou =confirm("The address provided is not correct.\nWould you like to continue with the payment?");
					if (doyou == true)		
					{
						//document.getElementById("Showbtn").disabled = true;
						document.getElementById("Showbtn").style.display = 'none';
						document.getElementById("hidebtn").style.display = 'block';
						document.getElementById("hidebtn").disabled = true;
					
						//document.getElementById("hidebtn").style.visibility = 'hidden';
						//document.getElementById("hidebtn").style.display = 'block';
						
						return true;					
					}					
					return false;				
				}
							
			}//credit card process ends
			
			//var btnprocess=document.getElementById("btn_process");
			
			//alert(btnprocess.cssText);
			
			if (document.getElementById("lbl_yds").innerText=="N")
			{			
				doyou =confirm("The address provided is not correct.\nWould you like to continue with the payment?");
				if (doyou == true)
				{
			//		btnprocess.className="label";
			//		btnprocess.style.backgroundColor="123160";
					
					document.getElementById("Showbtn").style.display = 'none';
					document.getElementById("hidebtn").style.display = 'block';
					document.getElementById("hidebtn").disabled = true;
					return true;
				}
				else 
				{
				return false;				
				}
			}
		//	a;	
			
				 //alert(btnprocess.cssText);
			  //     btnprocess.color="#5F5F5F";
			    //   btnprocess.backgroundColor="#5F5F5F";
			  //     btnprocess.className="label";
			 //      alert(btnprocess.backgroundColor);
			//       return false; 			
			
		}	
		//////////////////////////////////In order to hide credit card after process payment
		function HideCreditDetail()
		{           		
	         var td=document.getElementById("tblcreditcard");	           
	    	 if(document.frmpayment.ddl_paymentmethod.value=='5')	
				{			   
			      td.style.display ='none';			      	
				}											
		}
		/////////////////////////////CREDIT CARD////////////////////////////////////////		
		//To prompt for void
		function PromptVoid()
		{
		
		 doyou = confirm("Are you sure you want to void this transaction? (OK = Yes   Cancel = No)"); 
         if (doyou == true)
         {
			return true;
         }
         return false;			
		}
		//to confirm for schedule delete
		function PromptDelete()
		{	
		
		 doyou = confirm("Are you sure you want to Delete the Schedule Amount? (OK = Yes   Cancel = No)"); 
         if (doyou == true)
         {
			//	document.getElementById("lbl_schcount").innerText=="1" && 
			if(document.getElementById("lbl_paid").innerText=="0")
			{
				if((document.getElementById("txt_schedulesum").value)*1==(document.getElementById("lbl_owes").innerText)*1)		
				{	
					abc=confirm("This will move the Client to Quote. Click (OK = Yes  Cancel = No) ");
					if (abc==true)
					{
						return true;
					}
					return false;
				}
			}			
			//
			return true;
         }
         return false;			
		}		
				
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body onload="JavaScript: hideyds();">
		<form id="frmpayment" method="post" runat="server">
            &nbsp;<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<TR>
					<TD colSpan="5"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
				</TR>
				<TR>
					<TD id="td_yds">
						<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="780" border="0">
							<TBODY>
								<TR>
									<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2">
									<TABLE id="tblname" cellSpacing="0" cellPadding="0"
											width="776" border="0">
											<TR>
												<TD style="width: 63%; height: 19px"><asp:label id="lbl_LastName" runat="server" CssClass="Label" Width="32px"></asp:label>,
													<asp:label id="lbl_FirstName" runat="server" CssClass="Label"></asp:label>(
													<asp:label id="lbl_CaseCount" runat="server"></asp:label>),
													<asp:hyperlink id="hlnk_MidNo" runat="server"></asp:hyperlink>
                                                    <A href="#"></A></TD>
												<TD align="right" style="width: 19%; height: 19px"><asp:button id="btn_ESignature" runat="server" CssClass="clsbutton"   Text="Sign Documents"  Width="99px"></asp:button>
                                                    <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" EnableViewState="False"
                                                        Text="Update" /></TD>
												<TD align="right" width="6%" style="height: 19px"><asp:button id="btn_next" runat="server" CssClass="clsbutton" Text="Next" EnableViewState="False" Width="46px"></asp:button></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD colSpan="3" rowSpan="1" align="center"><asp:label id="lblMessage" runat="server" ForeColor="Red" Font-Names="Verdana" Font-Size="X-Small"></asp:label></TD>
								</TR>
								<TR>
									<TD class="clssubhead" background="../Images/subhead_bg.gif"
										colSpan="2" height="34px">&nbsp; Fee/Balance Information</TD>
								</TR>
								
								<TR>
									<TD colSpan="2">
										<TABLE id="tblTotalfee" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR class="clsLeftPaddingTable">
												<TD class="clsLeftPaddingTable" vAlign="top" height="20">&nbsp; Total Fee
												</TD>
												<TD vAlign="top" height="20">&nbsp; $
													<asp:label id="lbl_totalfee" runat="server" CssClass="label"></asp:label></TD>
												<TD class="clsLeftPaddingTable" vAlign="top" height="20">&nbsp;Paid</TD>
												<TD vAlign="top" height="20">&nbsp; $
													<asp:label id="lbl_paid" runat="server" CssClass="label"></asp:label></TD>
												<TD class="clsLeftPaddingTable" vAlign="top" height="20">&nbsp;Owes</TD>
												<TD vAlign="top" height="20">&nbsp; $
													<asp:label id="lbl_owes" runat="server" CssClass="label"></asp:label></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
								</TR> <!-- next section starts -->
								<TR>
									<TD class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34px">&nbsp; 
										Payment 
										Information&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
								</TR>
								
								<TR class="clsLeftPaddingTable">
									<TD vAlign="top" width="50%"><!--Payment Detail---ajax-->
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD>
													<TABLE id="processtable" cellSpacing="0" cellPadding="0" width="382" border="0">
														<TR class="clsLeftPaddingTable">
															<TD class="clsLeftPaddingTable" height="20">Amount($)</TD>
															<TD height="20"><asp:textbox id="txt_amount" runat="server" CssClass="clsinputadministration" MaxLength="5"></asp:textbox></TD>
														</TR>
														<TR class="clsLeftPaddingTable">
															<TD class="clsLeftPaddingTable" height="20">Payment Date</TD>
															<TD height="20"><ew:calendarpopup id="date_payment" runat="server" Width="90px" Font-Names="Tahoma" Font-Size="8pt"
																	DisplayOffsetY="-130" ToolTip="Payment Process Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
																	ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
																	ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True" Enabled="False">
																	<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
																	<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="White"></WeekdayStyle>
																	<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="Yellow"></MonthHeaderStyle>
																	<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
																		BackColor="AntiqueWhite"></OffMonthStyle>
																	<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="White"></GoToTodayStyle>
																	<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="LightGoldenrodYellow"></TodayDayStyle>
																	<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="Orange"></DayHeaderStyle>
																	<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="LightGray"></WeekendStyle>
																	<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="Yellow"></SelectedDateStyle>
																	<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="White"></ClearDateStyle>
																	<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
																		BackColor="White"></HolidayStyle>
																</ew:calendarpopup></TD>
														</TR>
														<TR class="clsLeftPaddingTable">
															<TD class="clsLeftPaddingTable" height="7">Method
															</TD>
															<TD height="7"><asp:dropdownlist id="ddl_paymentmethod" runat="server" CssClass="clsinputcombo"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" height="20"></TD>
															<TD class="clsLeftPaddingTable" align="right" height="20">
																<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD id="hidebtn" align="right"><asp:button id="Button1" runat="server" CssClass="clsbutton" Text="Process"></asp:button></TD>
																		<TD id="Showbtn" align="right"><asp:button id="btn_process" runat="server" CssClass="clsbutton" Text="Process" OnClientClick="MaxLength();"></asp:button></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
											</TR>
											<TR>
												<TD>
													<TABLE class="clsleftpaddingtable" id="tblcreditcard" style="DISPLAY: none" cellSpacing="0"
														cellPadding="0" width="382" border="0">
														<TR class="clsLeftPaddingTable">
															<TD class="clsaspcolumnheader" vAlign="top">Credit Card Info</TD>
															<TD vAlign="top"></TD>
														</TR>
														<TR>
															<TD class="clsleftpaddingtable" vAlign="top" height="20">Name</TD>
															<TD vAlign="top" height="20">&nbsp;
																<asp:textbox id="txt_name" runat="server" CssClass="clsinputadministration" Width="176px" MaxLength="20"></asp:textbox></TD>
														</TR>
														<TR>
															<TD class="clsleftpaddingtable" vAlign="top" height="20">Card 
																Number</TD>
															<TD vAlign="top" height="20">&nbsp;
																<asp:textbox id="txt_cardno" onblur="SelectCard();" runat="server" CssClass="clsinputadministration"
																	Width="176px" MaxLength="20"></asp:textbox></TD>
														</TR>
														<TR>
															<TD class="clsleftpaddingtable" style="DISPLAY: none" vAlign="top" height="20">Card 
																Type</TD>
															<TD style="DISPLAY: none" vAlign="top" height="20">&nbsp;
																<asp:dropdownlist id="ddl_cardtype" runat="server" CssClass="clsinputcombo" Width="176px"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<TD class="clsleftpaddingtable" vAlign="top" height="21">Expiration 
																Date</TD>
															<TD height="21">&nbsp;
																<asp:dropdownlist id="ddl_expmonth" runat="server" CssClass="clsinputcombo" Width="48px">
																	<asp:ListItem Value="01" Selected="True">01</asp:ListItem>
																	<asp:ListItem Value="02">02</asp:ListItem>
																	<asp:ListItem Value="03">03</asp:ListItem>
																	<asp:ListItem Value="04">04</asp:ListItem>
																	<asp:ListItem Value="05">05</asp:ListItem>
																	<asp:ListItem Value="06">06</asp:ListItem>
																	<asp:ListItem Value="07">07</asp:ListItem>
																	<asp:ListItem Value="08">08</asp:ListItem>
																	<asp:ListItem Value="09">09</asp:ListItem>
																	<asp:ListItem Value="10">10</asp:ListItem>
																	<asp:ListItem Value="11">11</asp:ListItem>
																	<asp:ListItem Value="12">12</asp:ListItem>
																</asp:dropdownlist>&nbsp;/&nbsp;
																<asp:dropdownlist id="ddl_expyear" runat="server" CssClass="clsinputcombo" Width="51px"></asp:dropdownlist></TD>
														</TR>
														<TR>
															<TD class="clsleftpaddingtable" vAlign="top" height="1">Cin</TD>
															<TD  height="1">&nbsp;
																<asp:textbox id="txt_cin" runat="server" CssClass="clsinputadministration" Width="56px" MaxLength="10"></asp:textbox></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</TD>
									<TD vAlign="top" style="width: 392px" >
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD>
													<TABLE id="tblpaymentgrid" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR class="clsleftpaddingtable">
															<TD class="clsaspcolumnheader" vAlign="top" height="5">Payment 
																Detail</TD>
														</TR>
														<TR>
															<TD vAlign="top"><asp:datagrid id="dg_PaymentDetail" runat="server" CssClass="clsleftpaddingtable" Width="384px"
																	CellPadding="0" AutoGenerateColumns="False" PageSize="2">
																	<Columns>
																		<asp:TemplateColumn HeaderText="Rep">
																			<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label id=lbl_emp runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.abbreviation") %>'>
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Amount($)">
																			<HeaderStyle Width="20%" CssClass="clsaspcolumnheader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label id=lbl_amount runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.paid") %>'>
																				</asp:Label>
																				<asp:Label id=lbl_invoicenum runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.invoicenumber_pk") %>' Visible="False">
																				</asp:Label>
																				<asp:Label id=lbl_vflag runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.voidflag") %>' Visible="False">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Payment Date">
																			<HeaderStyle Width="38%" CssClass="clsaspcolumnheader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label id=lbl_paydate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.recdate") %>'>
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Method">
																			<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																			<ItemTemplate>
																				<asp:Label id=lbl_paymethod runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
																				</asp:Label>&nbsp;
																				<asp:Label id=lbl_transnum runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.transnum") %>'>
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn>
																			<ItemTemplate>
																				<asp:LinkButton id="lnkb_edit" runat="server" Text="E" Enabled="False" CommandName="Edit" CausesValidation="false">Void</asp:LinkButton>
																			</ItemTemplate>
																			<EditItemTemplate>
																				&nbsp;
																			</EditItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																	<PagerStyle NextPageText="&amp;gt; Next" PrevPageText="Previous &amp;lt; "></PagerStyle>
																</asp:datagrid></TD>
														</TR>
													</TABLE>
												</TD>
											</TR> <!--pay comments-->
											<TR>
											</TR>
											<TR class="clsLeftPaddingTable">
												<TD vAlign="top">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD><asp:textbox id="txt_paycomments" runat="server" CssClass="clsinputadministration" Width="176px"
																	TextMode="MultiLine" Height="52px" onkeypress="ValidateLenght();" ></asp:textbox></TD>
															<TD>
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD class="clsLeftPaddingTable">Certified Mail</TD>
																	</TR>
																	<TR>
																		<TD class="clsLeftPaddingTable"><asp:textbox id="txt_certified" runat="server" CssClass="clsinputadministration" MaxLength="20"></asp:textbox></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR> <!--aa--> <!--main-->
								<TR>
									<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
								</TR> <!-- next section starts -->
								<TR>
									<TD class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34px">&nbsp; 
										Schedule Payment 
										Information&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:linkbutton id="lnkb_Viol" runat="server" BackColor="#EEC75E">Schedule Payment</asp:linkbutton></TD>
								</TR>
								<!--cutted-->
								<TR>
									<TD width="100%" colSpan="2">
										<TABLE id="tblschedulepay" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD><asp:datagrid id="dg_Schedule" runat="server" CssClass="clsleftpaddingtable" Width="776px" CellPadding="0"
														AutoGenerateColumns="False" PageSize="2">
														<Columns>
															<asp:TemplateColumn HeaderText="Amount($)">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id=lnkb_amount runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.amount") %>' CommandName="AmountClicked">
																	</asp:LinkButton>
																	<asp:Label id=lbl_schd_id runat="server" Width="12px" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.scheduleid") %>' Visible="False">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Payment Date">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_schd_date runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.paymentdate","{0:MM/dd/yyyy}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Days Left">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_dayleft runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.daysleft") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn>
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id="lnkb_editschedule" runat="server">Edit</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:ButtonColumn Text="Delete" CommandName="Delete">
																<HeaderStyle HorizontalAlign="Right" CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:ButtonColumn>
														</Columns>
														<PagerStyle NextPageText="&amp;gt; Next" PrevPageText="Previous &amp;lt; "></PagerStyle>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR> <!--cutted-->
								<TR>
									<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
								</TR> <!-- next section starts -->
								
								<TR>
									<TD class="clssubhead" background="../Images/subhead_bg.gif"
										colSpan="2" height="34px">&nbsp; Credit Card Information</TD>
								</TR>
								<TR>
									<TD width="100%" colSpan="2">
										<TABLE id="tblbottomcredit1" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD><asp:datagrid id="dg_creditcard" runat="server" CssClass="clsleftpaddingtable" Width="776px" CellPadding="0"
														AutoGenerateColumns="False">
														<Columns>
															<asp:TemplateColumn HeaderText="Name">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_name runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Card Number">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_ccnum runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ccnum") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Card Type">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_cardtype runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.cctype") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Trans #">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_transnumm runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.transnum") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Cin">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_cin runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.cin") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Exp Date">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_expdate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.expdate") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Auth Code">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_authcode runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.auth_code") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Response">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lbl_response" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.response_reason_text") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
				
				<TR>
					<TD width="100%" colSpan="2">
						<TABLE id="tblpretrial" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
							</TR>
							<TR>
								<TD background="../../images/headbar_headerextend.gif" colSpan="2"
									height="5"></TD>
							</TR>
							<TR>
								<TD class="clssubhead" background="../Images/subhead_bg.gif"
									colSpan="2" height="34">&nbsp; PRE-TRIAL REQUEST</TD>
							</TR>
							<TR>
								<TD width="100%" colSpan="2">
									<TABLE id="tblpresub" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD vAlign="top" width="74%"><FONT face="Verdana" color="#3366cc" size="1">
													<P>City of Houston has implemented a new pretrial conference for people interested 
														in taking probation or defensive driving. At the judges' discretion, these 
														options may or may not be available to you at your Jury Trial. Would you like 
														the attorney to automatically reset your case for a Jury Trial or would you 
														like to attend your pretrial to request defensive driving or probation?
													</P>
													<P>La ciudad de Houston ha puesto una nueva conferencia antes de juicio para la 
														gente interesada en tomar probación o clase de conducir. Con la discreción de 
														los jueces, estas opciones puede también estar disponible en su tribunal de 
														jurado. ¿ Usted le gustaria que el abogado reajustar automáticamente su caso 
														para un tribunal de jurado o usted le gustaria asistir a su conferencia para 
														solicitar clase de conducir o probación?.</P>
													<P><FONT color="red" size="2">(A majority of our clients set their case directly to 
															Jury Trial because they want to avoid an extra court appearance.) </FONT>
													</P>
												</FONT>
											</TD>
											<TD class="clsleftpaddingtable" vAlign="top" width="26%"><asp:radiobuttonlist id="rbtn_trial" runat="server" CssClass="clslabelnew" Width="176px" CellPadding="0"
													CellSpacing="0">
													<asp:ListItem Value="1" Selected="True">Reset Case For Jury Trial</asp:ListItem>
													<asp:ListItem Value="2">Client will be at pretrial </asp:ListItem>
													<asp:ListItem Value="3">Have Legal Assistant Call</asp:ListItem>
													<asp:ListItem Value="4">Probation Request</asp:ListItem>
													<asp:ListItem Value="5">DSC Request</asp:ListItem>
												</asp:radiobuttonlist></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
				</TR>
				<TR>
					<TD class="clssubhead"  background="../Images/subhead_bg.gif"
						colSpan="2" height="34">&nbsp; Print Summary</TD>
				</TR>
				
				<TR>
					<TD width="100%" colSpan="2">
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD><asp:datagrid id="dg_Print" runat="server" CssClass="clsleftpaddingtable" Width="776px" CellPadding="0"
										AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Printed Date">
												<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.PrintDate") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Letter Type">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label2 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.LetterName") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Notes">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label3 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Note") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Employee">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label4 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.UserAbb") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="File">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<asp:LinkButton id="lnkbtn_File" runat="server">View</asp:LinkButton>
													<asp:Label id=lbl_recid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RecordID") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
				</TR>
				<TR>
					<TD width="100%" colSpan="2">
						<TABLE id="tblbuttons" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD ><asp:button id="btn_coversheet" runat="server" CssClass="clsbutton" Text="Cover Sheet" Visible="False" OnClick="btn_coversheet_Click1" ></asp:button><asp:button id="btn_Receipt" runat="server" CssClass="clsbutton" Text="Receipt" Visible="False"></asp:button><asp:button id="btn_emailrep" runat="server" CssClass="clsbutton" Text="Email Receipt" Visible="False"></asp:button><asp:button id="btn_bond" runat="server" CssClass="clsbutton" Text="Bonds" Visible="False"></asp:button><asp:button id="btn_letterofRep" runat="server" CssClass="clsbutton" Text="Letter of Rep" Visible="False"></asp:button><asp:button id="btn_contuance" runat="server" CssClass="clsbutton" Text="Continuance" Visible="False"></asp:button><asp:button id="btn_trialLetter" CssClass="clsbutton" Text="Trial Letter" Visible="False" Runat="server"></asp:button><asp:button id="btn_polm" runat="server" CssClass="clsbutton" Text="Polm" Visible="False"></asp:button><asp:Button ID="btn_EmailTrial" runat="server" CssClass="clsbutton"
                                    Text="EmailTrial" Visible="False" /><asp:Button ID="btn_Limine" runat="server" CssClass="clsbutton" Text="Motion in Limine"
                                        Visible="False" Width="108px" /><asp:Button ID="btn_Discovery" runat="server" CssClass="clsbutton" Text="Motion for Discovery"
                                        Visible="False" Width="127px" />
                                </TD>
								<TD vAlign="top" align="right"></TD>
							</TR>
							<TR>
								<TD background="../../images/separator_repeat.gif" colSpan="6" height="11"></TD>
							</TR>
							<TR>
								<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
							</TR>
							<TR>
								<TD style="VISIBILITY: hidden; width: 780px; height: 40px;" vAlign="top" align="right"><asp:textbox id="txtTimeStemp" runat="server" Width="89px"></asp:textbox><asp:label id="lbl_schcount" runat="server"></asp:label><asp:label id="lbl_activeflag" runat="server">active</asp:label><asp:label id="lbl_dlstate" runat="server">dlstate</asp:label><asp:label id="lbl_yds" runat="server">yds</asp:label><asp:label id="lbl_trialbuttonflag" runat="server">trialbtnf</asp:label><asp:label id="lbl_addresscheck" runat="server"></asp:label><asp:label id="lbl_contactcheck" runat="server">concheck</asp:label>&nbsp;<asp:textbox id="txt_schedulesum" runat="server" Width="3px"></asp:textbox>&nbsp;<asp:label id="lbl_courtid" runat="server"></asp:label><asp:label id="lbl_scheduleid" runat="server"></asp:label><asp:label id="lbl_pretrialflag" runat="server">pretrialflag</asp:label><asp:label id="lbl_lockflag" runat="server"></asp:label>
                                    <asp:Label ID="lbl_OscarActiveFlag" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_OscarCourtID" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_ToProcess" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_court" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsSplit" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server"></asp:Label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<SCRIPT language="javascript"> ShowHidePreTrialTable(); document.getElementById("hidebtn").style.display="none";	 
				</SCRIPT>
				</TBODY>
			</TABLE>
			
            
          </td>
        
          
            
            </TR>
            </TABLE>
            
         
          
            
			</form>
	</body>
</HTML>

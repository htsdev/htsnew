using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using MSXML2;

namespace lntechNew.ClientInfo
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            UpdateViolation1.PageMethod += delegate() { mymethod(); };
              
            if (!IsPostBack)
            {



                string url = "http://www.texasonline.state.tx.us/NASApp/rap/apps/chotpa/jsp/eng/welcome.jsp";
                XMLHTTP ObjHttp = new XMLHTTP();
                //Connecting to site
                ObjHttp.open("post", url, false, null, null);
                //Sending parameters
                ObjHttp.send("");

                string response = ObjHttp.responseText;
                string sessionid = "GXHC_gx_session_id_";
                int initialindex = response.IndexOf(sessionid);
                int indexofvalue = response.IndexOf("value", initialindex);
                string sessionvalue = response.Substring(indexofvalue + 6, response.IndexOf('>', indexofvalue + 6) - (indexofvalue + 6));
                sessionvalue = sessionvalue.Substring(1);
                sessionvalue = sessionvalue.Substring(0, sessionvalue.Length - 1);

                
                url = "http://www.texasonline.state.tx.us/NASApp/rap/apps/chotpa/jsp/eng/legal.jsp";
                ObjHttp.open("post", url, false, null, null);
                ObjHttp.send("_instname=&_sign=open&_currpage=welcome&_pageid=1&_sign=open&_pageclass=&_selector=&_nextpage=&GXHC_gx_session_id_=" + sessionvalue);
                //ObjHttp.send("");
                         

                url = "https://www.texasonline.state.tx.us/NASApp/rap/apps/chotpa/jsp/eng/logon.jsp";
                ObjHttp.open("post", url, false, null, null);
                ObjHttp.send("_instname=&_sign=open&_currpage=legal&_pageid=1&_sign=open&_pageclass=&_selector=&_nextpage=logon.jsp&GXHC_gx_session_id_=" + sessionvalue);
                //ObjHttp.send("");
                                
                url = "https://www.texasonline.state.tx.us/NASApp/rap/apps/chotpa/jsp/eng/summary.jsp";
                ObjHttp.open("post", url, false, null, null);
                ObjHttp.send("ticket_number=077995251&_instname=&_sign=open&_currpage=logon&_pageid=1&_pageclass=RapLogon&_selector=&_nextpage=summary.jsp&GXHC_gx_session_id_=" + sessionvalue);

              
                if (ObjHttp.status.ToString() == "200")
                {
                    //return ObjHttp.responseText;
                }
                else
                {
                    //return "";
                }
                
                
                // UpdateViolation1.PageMethod += new lntechNew.WebControls.PageMethodHandler(UpdateViolation1_PageMethod);
                // mymethod();
             
                
            }

        }

        void UpdateViolation1_PageMethod()
        {
           
        }

        protected void dgViolationInfo_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
           
            DataRowView drv = (DataRowView) e.Item.DataItem;
            if (drv != null)
            {
                LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkBtn_CaseNo");
                lnkBtn.Attributes.Add("onClick", "return showpopup('" + UpdateViolation1.ClientID + "','" + e.Item.ClientID + "',0);");
            }
        }

        public void mymethod()
        {
            clsCaseDetail ClsCaseDetail = new clsCaseDetail();
            DataSet ds_CaseDetail = ClsCaseDetail.GetCaseDetail(124141);
            //hf_lastrow.Value = ds_CaseDetail.Tables[0].Rows.Count.ToString();
            dgViolationInfo.DataSource = ds_CaseDetail;
            dgViolationInfo.DataBind();
                       

        }


    }
}

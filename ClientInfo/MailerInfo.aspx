<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailerInfo.aspx.cs" Inherits="lntechNew.ClientInfo.MailerInfo" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Violation Data Information</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <aspnew:ScriptManager ID="scriptManager" runat="server">
        </aspnew:ScriptManager>
        <section id="main-content-popup" class="">
                <section class="wrapper main-wrapper row" style="">
                    <div class="col-xs-12"
                        <div class="alert alert-danger alert-dismissable fade in"  style="display:none">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>>
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="label"
                    ForeColor="Red"></asp:Label>
                            </div>

                        <asp:Panel id="trTrafficAlert" runat="server" Visible="false">
                            <div class="alert alert-danger alert-dismissable fade in"  style="display:none">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>>
                                    <asp:Label ID="lblTrafficAlert" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    </div>
                                </asp:Panel>
                    </div>
                    <div class="col-xs-12">
                         <div class="page-title">
                
                              <h1 class="title">Violation Data Information</h1>
                    
              
              
            
                        </div>

                      </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <ajaxToolkit:TabContainer ID="Tabs" runat="server">
                        <ajaxToolkit:TabPanel ID="pnl_ByDescription" runat="server" HeaderText="General"
                            TabIndex="0">
                            <ContentTemplate>
                         <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Violation Data Information</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                        <div class="content-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Officer Name</label>
                                    
                                    <span class="desc"></span>
                                    <div class="controls">
                                     <asp:Label ID="lbl_Name" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                           <asp:Panel ID="trMailDate" runat="server" >
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Mail Date</label>
                                    
                                    <span class="desc"></span>
                                    <div class="controls">
                                     <asp:Label ID="lbl_MailerDate" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                            </asp:Panel>
                            <asp:Panel ID="trMailerId" runat="server" >
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Mailer ID</label>
                                    
                                    <span class="desc"></span>
                                    <div class="controls">
                                    <asp:Label ID="lbl_MailerID" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                </asp:Panel>
                            <asp:Panel id="trLetterType" runat="server" runat="server">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label class="form-label">Letter Type</label>
                                    
                                    <span class="desc"></span>
                                    <div class="controls">
                                    <asp:Label ID="lbl_LetterType" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>
                                </asp:Panel>
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label class="form-label">Upload Date</label>
                                    
                                    <span class="desc"></span>
                                    <div class="controls">
                                    <asp:Label ID="lbl_UploadDate" runat="server" CssClass="clsLabel" ForeColor="#3366CC"></asp:Label>
                                                              
                                    </div>
                                </div>
                            </div>

                            
                </section>
                                 </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="pnl_ByDescriptione" runat="server" HeaderText="Mailer"
                            TabIndex="1">
                            <ContentTemplate>
                                <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Mailer Information</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                        <div class="content-body">
                               <div class="table-responsive" data-pattern="priority-columns">
                           <asp:GridView ID="gv_ShowAllLettres" runat="server" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped"
                                                BorderColor="White" Width="300">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Mail Date" DataField="MailDate" ItemStyle-CssClass="GridItemStyle"
                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField HeaderText="Mailer Type" DataField="MailerType" ItemStyle-CssClass="GridItemStyle"
                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="Left" />
                                                </Columns>
                                            </asp:GridView>
                                   </div>
                                
                                
                </section>
                                </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>

                         <hr />
                         <asp:Button ID="btn_Close" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="window.close();"></asp:Button>
                       
                     </div>
                    </section>
            </section>
       
                               
                         
                   
                
    </form>
      <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

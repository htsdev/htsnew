﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="HTP.ClientInfo.Comments"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comments</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png" />
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png" />
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png" />
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png" />
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->




    <link href="../Styles.css" type="text/css" rel="Stylesheet" />

    <script type="text/javascript" language="javascript">
        function CommentPopUp() {
            if (document.getElementById("WCC_Comments_lbl_comments") != null) {
                document.getElementById("WCC_Comments_lbl_comments").innerHTML = "";


            }
        }
        function MaintainGrid() {

            document.getElementById("tblDateWise1").style.display = 'none';
            document.getElementById("tblDateWise2").style.display = 'none';
            document.getElementById("trCommWise").style.display = 'block';

        }

        function ChangeGrid() {
            //document.getElementById("trdate").style.display ='block';
            if (document.getElementById("lnkComm").innerHTML == "Date Time View") {
                document.getElementById("lnkComm").innerHTML = "Comment Type Wise";
                document.getElementById("tblDateWise1").style.display = 'none';
                document.getElementById("tblDateWise2").style.display = 'none';
                document.getElementById("trCommWise").style.display = 'block';
            }
            else {
                document.getElementById("lnkComm").innerHTML = "Date Time View";
                document.getElementById("tblDateWise1").style.display = 'block';
                document.getElementById("tblDateWise2").style.display = 'block';
                document.getElementById("trCommWise").style.display = 'none';
            }

            return false;
        }


        function Check() {
            var select = document.getElementById("ddlCommentType");
            var select = document.getElementById("ddlCommentType");
            if (select.options[select.selectedIndex].value == -1) {
                alert("Please Select Comment Type");
                select.focus();
                return false;

            }
            else if (document.getElementById("WCC_Comments_txt_Comments").value == "")   //SAEED 7859 06/08/2010, Change innerHtml with value attribute. Because google chrome doesn't support innerHtml.
            {
                alert("Please Enter Comments");
                document.getElementById("WCC_Comments_txt_Comments").focus();
                return false;
            }

            document.getElementById("btnSubmit").style.display = 'none';
            document.getElementById("btnDummy").style.display = 'block';
            return true;
            //Sys.WebForms.PageRequestManager.getInstance()._updateControls(['tupd_panel','tupnl_Repeter'], ['btnSubmit'], [], 90);
        }

        function CheckForSelect() {

            var select = document.getElementById("ddlCommentType");
            if (select.options[select.selectedIndex].value == -1) {
                return false;
            }
            return true;

        }

        function HideCheckBox() {
            var select = document.getElementById("ddlCommentType");
            var Chk_Complaint = document.getElementById("WCC_Comments_chk_Complaint");
            if (select.options[select.selectedIndex].value == 1 || select.options[select.selectedIndex].value == 3) {
                Chk_Complaint.style.display = "block";
                document.getElementById("WCC_Comments_lbl_Complaint").style.display = "block";
            }
            if (Chk_Complaint != null) {
                Chk_Complaint.checked = false;
            }
        }

        

       
        function HidePopup() {
            debugger;
            var modalPopupBehavior = $find('MPEComments');
            modalPopupBehavior.hide();
            return false;
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            </aspnew:ScriptManager>
             <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
            <aspnew:UpdatePanel ID="upd_panel" runat="server">

                <ContentTemplate>

                    <asp:Panel ID="pnl" runat="server">
                       
                    </asp:Panel>
                    <section id="main-content" class="clientInfoCommentsPage" id="TableMain">
        <section class="wrapper main-wrapper row" id="TableGrid" style="">

             <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Comments</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            <div class="pull-right hidden-xs" id="TableSub">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                
                                                <tr>
                                                    <td style="height: 35px">
                                                        &nbsp;
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="form-label"></asp:Label>,
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="form-label"></asp:Label>&nbsp;(
                                                        <asp:Label ID="lbl_CaseCount" runat="server" CssClass="form-label"></asp:Label>),
                                                        <asp:HyperLink ID="hlnk_MidNo" runat="server" CssClass="form-label"></asp:HyperLink>
                                                        
                                                    </td>
                                                   
                                                </tr>
                    <tr>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    
                                </td>
                            </tr>
                                            </table>

                </div>




            </div>
                 </div>

            <div class="clearfix"></div>

            <section class="box" id="Table1" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:LinkButton ID="lnkComm" runat="server" OnClick="lnkComm_Click" Text="Date Time View"></asp:LinkButton>
                                    <asp:LinkButton ID="lnk_AddComments" OnClick="btn_lnkAddComm" runat="server" Text="Add Comments"></asp:LinkButton>
                                    
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>
            <div  id="tblDateWise1" runat="server">
                <section class="box">

                     <header class="panel_header">

                         <h2 class="title pull-left">
                             Comments
                           

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <uc3:PagingControl ID="Pagingctrl" runat="server" />
                    <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../Images/plzwait.gif" /><asp:Label ID="lbl1" runat="server" CssClass="clsLabel"
                                                        Text="Please Wait ......"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                    

                                    <aspnew:UpdatePanel ID="upnlResult" runat="server">
                                                <ContentTemplate>
                                                    <div class="table-responsive" data-pattern="priority-columns">
                                                    <asp:GridView ID="gv_Records" runat="server" AllowPaging="True" AllowSorting="True"
                                                        AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped" OnPageIndexChanging="gv_Records_PageIndexChanging"
                                                        OnSorting="gv_Records_Sorting" PageSize="20" Width="100%">
                                                        <Columns>
                                                            <%--<asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="&lt;u&gt;Date Time&lt;/u&gt;"
                                                                SortExpression="sortCommDate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CommDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CommDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Day">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Days" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CommDay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Comment Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CommType" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CommType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderText="Comment">
                                                                <ItemTemplate>
                                                                    <div style="height: 40px; width: 500px; overflow: auto">
                                                                        <asp:Label ID="lbl_Comm" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Comm") %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderText="&lt;u&gt;Rep&lt;/u&gt;" SortExpression="Rep">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Rep" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                            Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                                    </asp:GridView>
                                                        </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                                </Triggers>
                                            </aspnew:UpdatePanel>
                                    <asp:Label ID="lbl_Message" runat="server" Font-Names="Verdana" Font-Size="X-Small" CssClass="form-label"
                                                            ForeColor="Red"></asp:Label>
                                    
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                
                            
                           

                         
                 </section>
            </div>
            


             <div class="clearfix"></div>
             <section class="box" id="tblDateWise2" style="display:none" runat="server">

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>

             <section class="box" id="" style="display:none">

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">

                                    <ajaxToolkit:ModalPopupExtender ID="MPEComments" runat="server" BackgroundCssClass="modalBackground"
                                    CancelControlID="lbtn_cmn_close" PopupControlID="pnlAddComments" TargetControlID="btndummynone">
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Button ID="btndummynone" runat="server" Style="display: none" />
                                    
                                                        </div>
                                                
                                    </div>
                       </div>
                    </div>
                     </div>
                 </section>
             <div class="clearfix"></div>


            <asp:Panel ID="trCommWise" runat="server" >

                        <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="upnl_Repeter">
                                    <ProgressTemplate>
                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl2" runat="server" CssClass="clsLabel"
                                            Text="Please Wait ......"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                         <aspnew:UpdatePanel ID="upnl_Repeter" runat="server">
                                    <ContentTemplate>
                                         <asp:Repeater ID="rptComments" runat="server" OnItemDataBound="rptComments_ItemDataBound">
                                            <ItemTemplate>
                                               
                                                
                                                     <section class="box" id="" style="">
                     <header class="panel_header">

                         <h2 class="title pull-left">
                             
                             <asp:Label ID="lblComm" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CommentType") %>'></asp:Label>
                             

                         </h2>
                         <div class="actions panel_actions pull-right">
                      <uc3:PagingControl ID="Pagingctrl_typewise" runat="server" />
                             <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:GridView ID="gv_CommRecords" runat="server" AllowPaging="True" AllowSorting="True"
                                                                AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped" OnPageIndexChanging="gv_Data_PageIndexChanging"
                                                                OnSorting="gv_Sorting" PageSize="10" Width="100%">
                                                                <EmptyDataTemplate>
                                                                    <table align="center" class="clsLeftPaddingTable">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbl_Emptydata" runat="server" ForeColor="Red" Text="No Records Found"
                                                                                    Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <%--<asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="&lt;u&gt;Date Time&lt;/u&gt;"
                                                                        SortExpression="sortCommDate">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_CommDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommDate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>--%>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Day">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Days" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CommDay") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                        HeaderText="Comment">
                                                                        <ItemTemplate>
                                                                            <div style="height: 40px; width: 500px; overflow: auto">
                                                                                <asp:Label ID="lbl_Comm" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Comm") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                        HeaderText="&lt;u&gt;Rep&lt;/u&gt;" SortExpression="Rep">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Rep" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Rep") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <PagerSettings Mode="NextPreviousFirstLast" Visible="false" NextPageText="&nbsp;Next &gt;"
                                                                    PreviousPageText="&lt; Previous" FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;"
                                                                    LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                            </asp:GridView>
                                    </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>


                                     <div class="clearfix"></div>

                                                <section class="box" id="" style="display:none">
                    

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">

                                    <asp:LinkButton ID="lnkFirst" runat="server" OnClick="lbk_First_Click" Text="&lt;&lt; First Page"></asp:LinkButton>
                                                            &nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lbk_previous_Click" Text="&lt; Previous"></asp:LinkButton>
                                                            &nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnkNext" runat="server" OnClick="lbk_next_Click" Text="Next &gt;"></asp:LinkButton>
                                                            &nbsp;&nbsp;
                                                            <asp:LinkButton ID="lnkLast" runat="server" OnClick="lbk_Last_Click" Text="Last Page &gt;&gt;"></asp:LinkButton>
                                      
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>


                                     <div class="clearfix"></div>


                                                   


                                                    
                                            </ItemTemplate>
                                        </asp:Repeater>
                                         </ContentTemplate>
                                    <Triggers>
                                        <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                </asp:Panel>

                  

            <div class="clearfix"></div>

            <section class="box" >
            
            <div class="content-body" style="display:none">
                <div class="row">
                      <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                     <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                    </div>
                                                                </div>
                          </div>
                    </div>
                </div>
                </section>

            <div class="clearfix"></div>

             <%--<section class="box" >
            
            <div class="content-body" style="display:none">
                <div class="row">
                      <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">--%>
                                    
                                     <asp:Panel ID="pnlAddComments" runat="server" Width="455px">

                                         <section class="box" id="">
                 <header class="panel_header">
                     <h2 class="title pull-left">Add Comments</h2>
                     <div class="actions panel_actions pull-right">
                     <asp:LinkButton ID="lbtn_cmn_close" runat="server" CssClass="closeCustomBtn2">X</asp:LinkButton>
                       
                    
                </div>
            </header>
                 <div class="content-body">
            <div class="row">

                 <div class="col-md-12">
                                                        <div class="form-group">
                            <label class="form-label"> Comments Type :</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:DropDownList ID="ddlCommentType" AutoPostBack="true" runat="server" DataTextField="CommentType"
                                                                DataValueField="CommentID" OnSelectedIndexChanged="ddlCommentType_SelectedIndexChanged"
                                                                CssClass="form-control">
                                                            </asp:DropDownList>
                                </div>
                                                            </div>
                     </div>

                 <div class="clearfix"></div>

                 <div class="col-md-12">
                                                        <div class="form-group">
                           
                            <div class="controls">
                                <cc2:WCtl_Comments ID="WCC_Comments" runat="server" Width="420px" />
                                </div>
                                                            </div>
                     </div>

                 <div class="clearfix"></div>

                <div class="col-md-12">
                                                        <div class="form-group">
                        
                            <div class="controls">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary inline-textbox addCommentsBtn" OnClientClick="return Check();"
                                                                                        OnClick="btnSubmit_Click"  Text="Add" />
                                                                                    <asp:Button ID="btnDummy" runat="server" Text="Add" Width="50px" Style="display: none" disabled="true"
                                                                                        CssClass="btn btn-primary" />
                                <asp:Button ID="btn_cancel" runat="server" CssClass="btn btn-primary" OnClientClick="return HidePopup();" 
                                                                                        Text="Cancel" Visible="false" />
                                <asp:HiddenField ID="hfLORCourtId" runat="server" Value="test" />
                                </div>
                                                            </div>
                     </div>



                </div>
                     </div>
                                             </section>




                                   <%-- <table border="2" enableviewstate="true" style="border-left-color: navy; border-bottom-color: navy;
                                        border-top-color: navy; border-collapse: collapse; border-right-color: navy"
                                        width="455px" class="clsLeftPaddingTable">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" valign="bottom">
                                                <table border="0" style="height: 26px" width="100%">
                                                    <tr>
                                                        <td class="form-label" style="height: 26px">
                                                            Add Comments
                                                        </td>
                                                        <td align="right">
                                                            <asp:LinkButton ID="lbtn_cmn_close" runat="server">X</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" class="clsLeftPaddingTable " width="100%">
                                                    <tr>
                                                        <td class="form-label" colspan="2">
                                                            Comments Type :
                                                            <asp:DropDownList ID="ddlCommentType" AutoPostBack="true" runat="server" DataTextField="CommentType"
                                                                DataValueField="CommentID" OnSelectedIndexChanged="ddlCommentType_SelectedIndexChanged"
                                                                CssClass="clsInputCombo">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" width="100%">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <cc2:WCtl_Comments ID="WCC_Comments" runat="server" Width="420px" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" width="100%">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td align="right" width="100%">
                                                                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClientClick="return Check();"
                                                                                        OnClick="btnSubmit_Click" Width="50px" Text="Add" />
                                                                                    <asp:Button ID="btnDummy" runat="server" Text="Add" Width="50px" Style="display: none" disabled="true"
                                                                                        CssClass="btn btn-primary" /> 
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btn_cancel" runat="server" CssClass="btn btn-primary" OnClientClick="return HidePopup();" Width="50px"
                                                                                        Text="Cancel" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hfLORCourtId" runat="server" Value="test" />
                                            </td>
                                        </tr>
                                    </table>--%>




                                </asp:Panel>
                                 <%--   </div>
                                                                </div>
                          </div>
                    </div>
                </div>
                </section>--%>




            </section>
                 </section>




                </ContentTemplate>

            </aspnew:UpdatePanel>


        </div>

    </form>
      <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#hlnk_Edit').click(function () {
                HidePopup();
            })
        });
       
        function HandleComments() {
            if (document.getElementById('hlnk_Edit') != null) {             
                 if (document.getElementById('hlnk_Edit').innerText == 'Cancel') {
                    HidePopup();

                }
            }

        }

        function CancelMobel()
        {
            var button = document.getElementById('hlnk_Edit');
            button.onclick = function () {
                HidePopup();
            }
        }

    </script>
</body>


</html>

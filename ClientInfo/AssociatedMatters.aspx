﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssociatedMatters.aspx.cs"
    Inherits="HTP.ClientInfo.AssociatedMatters" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagName="Info" TagPrefix="Contact" Src="~/WebControls/ContactInfo.ascx" %>
<%@ Register TagName="LookUp" TagPrefix="Contact" Src="~/WebControls/ContactIDLookUp.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Matter Page</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->


    <script type="text/javascript">
    
       function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
       
       function Confirmation(recordid,CaseTypeID)
        {

           var ans= confirm("Removing CID. Would you like to continue?");
            if(ans)
            {
        
            var CID = gup('cid');                       
                if (CID != "")
                {
                    window.location="AssociatedMatters.aspx?type=UnLink&RecordID="+recordid + "&cid="+CID+"&CaseType="+CaseTypeID;
                }
                else
                {
                var casenumber= gup( 'casenumber' );                
                window.location="AssociatedMatters.aspx?type=UnLink&RecordID="+recordid + "&casenumber="+casenumber+"&CaseType="+CaseTypeID;
                }
                
            }
   
        }

    </script>

</head>
<body class="pace-done">
    <form id="form1" runat="server" >
         <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            </aspnew:ScriptManager>
             <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
               <section id="main-content" class="">
                    <section class="wrapper main-wrapper row" style="">
                         <div class="col-xs-12">
                            <div class="col-md-6">
                                <div class="pull-left">
                                    <h1 class="title">Associated Matters</h1>
                                </div>
                            </div>
                             <div class="col-md-6 pullright">
                                 <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td align="left" width="80%" style="height: 20px">
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                            <asp:Label ID="lbl_CaseCount" runat="server" CssClass="Label"></asp:Label>)
                                            <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink><a href="#"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                <td align="left" width="80%" style="height: 20px">
                                    <asp:Label ID="lblMessage" runat="server" CssClass="Label"></asp:Label>
                                </td>
                                 </tr>
                                </table>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                            <div class="col-xs-12">
                                <section class="box ">
                                    <header class="panel_header">
                                    <h2 class="title pull-left">Associated Matters</h2>

                                    <div class="actions panel_actions pull-right">
                       
                	                    <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                       <%-- <div class="table-responsive" data-pattern="priority-columns">--%>
                                        <asp:TreeView ID="TreeView1" runat="server">
                                        </asp:TreeView>
                                            <%--</div>--%>
                                    </div>
                                </div>
                             </section>
                            </div>
                    </section>
               </section>
    <div>
        <%--<table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
           
            <tr>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>--%>
    </div>
          </div>
    </form>
    
    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

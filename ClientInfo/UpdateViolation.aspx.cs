using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;



namespace lntechNew.ClientInfo
{
    /// <summary>
    /// Summary description for UpdateViolation.
    /// </summary>
    public partial class UpdateViolation : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label lbl_LastName;
        protected System.Web.UI.WebControls.Label lbl_FirstName;
        protected System.Web.UI.WebControls.Label lbl_CaseCount;
        protected System.Web.UI.WebControls.Label lblStatusId;
        protected System.Web.UI.WebControls.Label lbl_MidNo;
        protected System.Web.UI.WebControls.DropDownList ddl_Date_Month;
        protected System.Web.UI.WebControls.DropDownList ddl_Date_Day;
        protected System.Web.UI.WebControls.DropDownList ddl_Date_Year;
        protected System.Web.UI.WebControls.DropDownList ddl_Time;
        protected System.Web.UI.WebControls.TextBox txt_TicketNo;
        protected System.Web.UI.WebControls.DropDownList ddl_ViolationDescription;
        protected System.Web.UI.WebControls.TextBox txt_FineAmount;
        protected System.Web.UI.WebControls.TextBox txt_BondAmount;
        protected System.Web.UI.WebControls.CheckBox chkb_Bond;
        protected System.Web.UI.WebControls.Label lbl_Day;
        protected System.Web.UI.WebControls.Label lbl_AutoStatus;
        protected System.Web.UI.WebControls.Label lbl_AutoCourtDate;
        protected System.Web.UI.WebControls.Label lbl_AutoTime;
        protected System.Web.UI.WebControls.Label lbl_AutoNo;
        protected System.Web.UI.WebControls.Label lbl_ScanStatus;
        protected System.Web.UI.WebControls.Label lbl_ScanCourtDate;
        protected System.Web.UI.WebControls.Label lbl_ScanTime;
        protected System.Web.UI.WebControls.Label lbl_ScanNo;
        protected System.Web.UI.WebControls.Label lbl_MainStatus;
        protected System.Web.UI.WebControls.Label lbl_MainCourtDdate;
        protected System.Web.UI.WebControls.Label lbl_MainTime;
        protected System.Web.UI.WebControls.Label lbl_MainNo;
        protected System.Web.UI.WebControls.DropDownList ddl_Status;
        protected System.Web.UI.WebControls.HyperLink hlnk_AddOfficer;
        protected System.Web.UI.WebControls.DataGrid dgViolationInfo;

        clsCase ClsCase = new clsCase();
        clsCaseDetail ClsCaseDetail = new clsCaseDetail();
        clsCourts ClsCourts = new clsCourts();
        protected System.Web.UI.WebControls.DropDownList ddl_CourtLocation;
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();
        clsViolations ClsViolations = new clsViolations();
        protected System.Web.UI.WebControls.TextBox txt_CourtNo;
        protected System.Web.UI.WebControls.Label lbl_OfficerName;
        protected System.Web.UI.WebControls.Label lbl_AutoStatusID;
        protected System.Web.UI.WebControls.HyperLink hlnk_AutoVerified;
        protected System.Web.UI.WebControls.LinkButton lnkb_InsertViolation;
        protected System.Web.UI.WebControls.Button btn_Update;
        clsPricingPlans ClsPricingPlans = new clsPricingPlans();
        protected System.Web.UI.WebControls.TextBox txt_SequenceNo;
        private static string sTVIDs = string.Empty;
        protected System.Web.UI.WebControls.Label lbl_Message;
        protected System.Web.UI.WebControls.TextBox txt_FTicketCount;
        protected System.Web.UI.WebControls.TextBox txt_BondFlag;
        protected System.Web.UI.WebControls.TextBox txt_IsNew;
        protected MagicAjax.UI.Controls.AjaxPanel AjaxPanel1;
        protected System.Web.UI.WebControls.DropDownList ddl_PricePlan;
        protected MagicAjax.UI.Controls.AjaxPanel Ajaxpanel2;
        protected System.Web.UI.WebControls.Label lbldetailcount;
        protected System.Web.UI.WebControls.CheckBox chkb_Main;
        protected System.Web.UI.WebControls.Label lblAdmin;
        protected System.Web.UI.WebControls.Label lbl_ViolationDescription;
        protected System.Web.UI.WebControls.Label lbl_ViolationID;
        protected System.Web.UI.WebControls.Label lbldisplay;
        protected MagicAjax.UI.Controls.AjaxPanel Ajaxpanel3;
        protected System.Web.UI.WebControls.TextBox TxtValue;
        protected System.Web.UI.WebControls.Label lbl_Status;
        protected System.Web.UI.WebControls.Label lblDispo;
        protected System.Web.UI.WebControls.TextBox txt_CauseNo;
        protected System.Web.UI.WebControls.TextBox txtinactive;
        protected System.Web.UI.WebControls.Label lblinactiveCourtName;
        protected System.Web.UI.WebControls.Label lblinactiveCourtID;
        clsLogger bugTracker = new clsLogger();
        protected System.Web.UI.WebControls.Button btn_Update1;
        clsSession ClsSession = new clsSession();
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Check for valid Session if not then redirect to login page
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
                }

                //if poped up from case disposition
                if (Request.QueryString["Dispo"] == "1")
                {
                    lblDispo.Text = "1";
                }
                else
                {
                    lblDispo.Text = "0";
                }

                if (ClsSession.GetCookie("sIsChanged", this.Request) != "True")
                {
                    ClsCaseDetail.TicketViolationID = Convert.ToInt32(Request.QueryString["ticketsViolationID"]);
                    //ClsSession.CreateCookie(  "sTicketViolationID",ClsCaseDetail.TicketViolationID.ToString(),this.Request,this.Response);
                    ViewState["vTicketViolationID"] = ClsCaseDetail.TicketViolationID;
                }
                //ClsCase.TicketID=Convert.ToInt32(ClsSession.GetCookie("sTicketID",this.Request));
                ClsCase.TicketID = Convert.ToInt32(Request.QueryString["ticketid"]);
                ViewState["vTicketID"] = ClsCase.TicketID;
                ViewState["vSearch"] = Convert.ToInt32(Request.QueryString["search"]);
                if (!IsPostBack)
                {
                    if (ClsSession.GetCookie("sIsChanged", this.Request) == "True" && Request.QueryString.Count > 0)
                    {
                        ClsCaseDetail.TicketViolationID = Convert.ToInt32(Request.QueryString["ticketsViolationID"]);
                        //ClsSession.CreateCookie("sTicketViolationID",ClsCaseDetail.TicketViolationID.ToString(),this.Request, this.Response);
                        ViewState["vTicketViolationID"] = ClsCaseDetail.TicketViolationID;
                    }

                    //Check for User Type 2 means Admin, 1 means application user
                    //price plan will be visible to admin users only
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "2")
                    {
                        dgViolationInfo.Columns[8].Visible = true;
                        lblAdmin.Text = "1";
                    }
                    else if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "1")
                    {
                        dgViolationInfo.Columns[8].Visible = false;
                        lblAdmin.Text = "0";
                    }
                    //page validation
                    btn_Update.Attributes.Add("OnClick", "javascript:return submitForm();");
                    btn_Update1.Attributes.Add("OnClick", "javascript:return submitForm();");


                    FillCourtLocation();
                    FillStatus();
                    FillViolations();
                    DisplayInfo();
                    CheckCaseStatus();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //fills Violation description drop down list
        private void FillViolations()
        {
            try
            {
                DataSet ds_Violation = ClsViolations.GetAllViolations();
                ddl_ViolationDescription.Items.Clear();
                for (int i = 0; i < ds_Violation.Tables[0].Rows.Count; i++)
                {
                    string description = ds_Violation.Tables[0].Rows[i]["Description"].ToString().Trim();
                    string ID = ds_Violation.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_ViolationDescription.Items.Add(new ListItem(description, ID));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //fills price plan drop down list and one of them is selected  
        //accorrding to court location and it's effective date
        private void FillPricePlan()
        {
            try
            {
                DataSet ds_PricePlan;
                if (txtinactive.Text == "1")
                {
                    try
                    {
                        ClsPricingPlans.CourtID = Convert.ToInt32(lblinactiveCourtID.Text);
                        ddl_PricePlan.Enabled = false;
                    }
                    catch { }
                }
                else
                {
                    ClsPricingPlans.CourtID = Convert.ToInt32(ddl_CourtLocation.SelectedValue);
                    ddl_PricePlan.Enabled = true;
                }
                //if (IsNewViolation==true)
                if (txt_IsNew.Text == "1")
                {
                    ClsPricingPlans.IsNew = 1;
                    ds_PricePlan = ClsPricingPlans.GetAllPlan();
                }
                else
                {
                    ClsPricingPlans.IsNew = 0;
                    ds_PricePlan = ClsPricingPlans.GetAllPlan();
                }
                ddl_PricePlan.Items.Clear();
                ddl_PricePlan.Items.Add(new ListItem("--Choose--", "0"));
                for (int i = 0; i < ds_PricePlan.Tables[0].Rows.Count; i++)
                {
                    string description = ds_PricePlan.Tables[0].Rows[i]["PlanShortName"].ToString().Trim();
                    string ID = ds_PricePlan.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_PricePlan.Items.Add(new ListItem(description, ID));
                }
                //if (IsNewViolation==true)
                if (txt_IsNew.Text == "1")
                {
                    try
                    {
                        string date = DateTime.Now.ToShortDateString();
                        DataSet ds_PlanID = ClsPricingPlans.GetPlan(date, ClsPricingPlans.CourtID, 0);
                        ddl_PricePlan.SelectedValue = ds_PlanID.Tables[0].Rows[0]["planid"].ToString().Trim();
                    }
                    catch (Exception)
                    { }
                }
                else if (ClsCaseDetail.PlanID != 0)
                {
                    try
                    {
                        ddl_PricePlan.SelectedValue = ClsCaseDetail.PlanID.ToString();
                    }
                    catch (Exception)
                    { }
                }
                else if (ClsCaseDetail.PlanID == 0)
                {
                    try
                    {
                        string date = string.Concat(txt_Month.Text, "/", txt_Day.Text, "/", txt_Year.Text);
                        DataSet ds_PlanID = ClsPricingPlans.GetPlan(date, ClsPricingPlans.CourtID, 1);
                        ddl_PricePlan.SelectedValue = ds_PlanID.Tables[0].Rows[0]["planid"].ToString().Trim();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //fills the Court location drop down list
        private void FillCourtLocation()
        {
            try
            {
                DataSet ds_Court = ClsCourts.GetAllActiveCourtName();
                ddl_CourtLocation.Items.Clear();
                for (int i = 0; i < ds_Court.Tables[0].Rows.Count; i++)
                {
                    string shortCourtName = ds_Court.Tables[0].Rows[i]["ShortName"].ToString().Trim();
                    string courtID = ds_Court.Tables[0].Rows[i]["courtid"].ToString().Trim();
                    ddl_CourtLocation.Items.Add(new ListItem(shortCourtName, courtID));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //fills case status drop down list
        private void FillStatus()
        {
            try
            {
                DataSet ds_Status = ClsCaseStatus.GetAllCourtCaseStatus();
                ddl_Status.Items.Clear();
                string Description = "---Choose----";
                string ID = "0";
                ddl_Status.Items.Add(new ListItem(Description, ID));
                for (int i = 0; i < ds_Status.Tables[0].Rows.Count; i++)
                {
                    Description = ds_Status.Tables[0].Rows[i]["Description"].ToString().Trim();
                    ID = ds_Status.Tables[0].Rows[i]["ID"].ToString().Trim();
                    ddl_Status.Items.Add(new ListItem(Description, ID));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //Displays the whole page info
        private void DisplayInfo()
        {
            try
            {
                DataSet ds_Case = ClsCase.GetCase(ClsCase.TicketID);
                if (ds_Case.Tables[0].Rows.Count > 0)
                {
                    lbl_LastName.Text = ds_Case.Tables[0].Rows[0]["Lastname"].ToString().Trim();
                    lbl_FirstName.Text = ds_Case.Tables[0].Rows[0]["Firstname"].ToString().Trim();
                    lbl_CaseCount.Text = ds_Case.Tables[0].Rows[0]["CaseCount"].ToString().Trim();
                    lbl_MidNo.Text = ds_Case.Tables[0].Rows[0]["Midnum"].ToString().Trim();


                    //gets Violation Info if it exist
                    if (ClsCaseDetail.TicketViolationID != 0)
                    {
                        //IsNewViolation=false;
                        txt_IsNew.Text = "0";
                        DataSet ds_ViolationDetail = ClsCaseDetail.GetCaseDetailByViolationID(ClsCaseDetail.TicketViolationID);
                        if (ds_ViolationDetail.Tables[0].Rows.Count > 0)
                        {
                            txt_TicketNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["RefCaseNumber"].ToString().Trim();
                            /*if(ds_ViolationDetail.Tables[0].Rows[0]["SequenceNumber"].ToString()=="")
                            {
                                txt_SequenceNo.Text="0";
                            }
                            else
                            {
                                txt_SequenceNo.Text=ds_ViolationDetail.Tables[0].Rows[0]["SequenceNumber"].ToString().Trim();
                            }*/
                            txt_CauseNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CauseNumber"].ToString().Trim();
                            try
                            {
                                lbl_ViolationDescription.Visible = true;
                                lbl_ViolationDescription.Text = ds_ViolationDetail.Tables[0].Rows[0]["ViolationDescription"].ToString().Trim();
                                lbl_ViolationID.Text = ds_ViolationDetail.Tables[0].Rows[0]["ViolationID"].ToString().Trim();
                                TxtValue.Text = "1";
                                ddl_ViolationDescription.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["ViolationID"].ToString().Trim();
                            }
                            catch (Exception)
                            {
                                ddl_ViolationDescription.SelectedValue = "0";
                                TxtValue.Text = "1";
                            }
                            txt_FineAmount.Text = ds_ViolationDetail.Tables[0].Rows[0]["FineAmount"].ToString().Trim();
                            txt_BondAmount.Text = ds_ViolationDetail.Tables[0].Rows[0]["BondAmount"].ToString().Trim();
                            Boolean bondFlag = Convert.ToBoolean(ds_ViolationDetail.Tables[0].Rows[0]["BondFlag"].ToString().Trim());
                            if (bondFlag == true)
                            {
                                chkb_Bond.Checked = true;
                            }
                            else if (bondFlag == false)
                            {
                                chkb_Bond.Checked = false;
                            }
                            //
                            try
                            {
                                ddl_Status.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["VerifiedStatusID"].ToString().Trim();
                                lbl_Status.Text = ddl_Status.SelectedItem.Text;
                            }
                            catch (Exception)
                            { }
                            if (ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString() != "" && ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Substring(0, 10) != "01/01/1900")
                            {
                                txt_Day.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(3, 2);
                                txt_Month.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(0, 2);
                                txt_Year.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(8, 2);
                                try
                                {
                                    ddl_Time.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Substring(11, 7).Trim();
                                }
                                catch (Exception)
                                {
                                    try
                                    {
                                        ddl_Time.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Substring(12, 7).Trim();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                            if (ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString() == "3000" || ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString() == "")
                            {
                                ddl_CourtLocation.SelectedValue = "0";
                                FillPricePlan();
                            }
                            else
                            {
                                txtinactive.Text = ds_ViolationDetail.Tables[0].Rows[0]["inactive"].ToString().Trim();
                                if (txtinactive.Text == "1")
                                {
                                    lblinactiveCourtName.Visible = true;
                                    ddl_CourtLocation.Visible = false;
                                    lblinactiveCourtName.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtName"].ToString().Trim();
                                    lblinactiveCourtID.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString().Trim();
                                    ClsCaseDetail.PlanID = Convert.ToInt32(ds_ViolationDetail.Tables[0].Rows[0]["PlanId"].ToString().Trim());
                                    FillPricePlan();
                                }
                                else
                                {
                                    lblinactiveCourtName.Visible = false;
                                    ddl_CourtLocation.Visible = true;
                                    ddl_CourtLocation.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString().Trim();
                                    ClsCaseDetail.PlanID = Convert.ToInt32(ds_ViolationDetail.Tables[0].Rows[0]["PlanId"].ToString().Trim());
                                    FillPricePlan();
                                }
                            }

                            txt_CourtNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtNumberVerified"].ToString().Trim();

                            int bondMain = Convert.ToInt32(ds_ViolationDetail.Tables[0].Rows[0]["BondFlagMain"].ToString().Trim());
                            if (bondMain == 1)
                            {
                                txt_BondFlag.Text = "1";
                            }
                            else if (bondMain == 0)
                            {
                                txt_BondFlag.Text = "0";
                            }

                            //							lbl_OfficerName.Text=ds_ViolationDetail.Tables[0].Rows[0]["OfficerName"].ToString().Trim();
                            //							lbl_Day.Text=ds_ViolationDetail.Tables[0].Rows[0]["OfficerDay"].ToString().Trim();


                            lbl_AutoStatus.Text = ds_ViolationDetail.Tables[0].Rows[0]["AutoStatus"].ToString().Trim();
                            lbl_AutoStatusID.Text = ds_ViolationDetail.Tables[0].Rows[0]["AutoStatusID"].ToString().Trim();
                            if (ds_ViolationDetail.Tables[0].Rows[0][11].ToString() != "")
                            {
                                lbl_AutoCourtDate.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateAuto"].ToString().Trim().Substring(0, 10);

                                try
                                {
                                    lbl_AutoTime.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateAuto"].ToString().Trim().Substring(11, 7);
                                }
                                catch (Exception)
                                {
                                    lbl_AutoTime.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateAuto"].ToString().Trim().Substring(12, 6);
                                }
                            }
                            lbl_AutoNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtNumberAuto"].ToString().Trim();
                            lbl_ScanStatus.Text = ds_ViolationDetail.Tables[0].Rows[0]["ScanStatus"].ToString().Trim();
                            if (ds_ViolationDetail.Tables[0].Rows[0]["CourtDateScan"].ToString() != "")
                            {
                                lbl_ScanCourtDate.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateScan"].ToString().Trim().Substring(0, 10);

                                try
                                {
                                    lbl_ScanTime.Text = lbl_ScanNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateScan"].ToString().Trim().Substring(11, 7);
                                }
                                catch (Exception)
                                {
                                    lbl_ScanTime.Text = lbl_ScanNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateScan"].ToString().Trim().Substring(12, 6);
                                }
                            }
                            lbl_ScanNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtNumberScan"].ToString().Trim();
                            lbl_MainStatus.Text = ds_ViolationDetail.Tables[0].Rows[0]["VerifiedStatus"].ToString().Trim();
                            if (ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString() != "")
                            {
                                lbl_MainCourtDdate.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(0, 10);

                                try
                                {
                                    lbl_MainTime.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(11, 7);
                                }
                                catch (Exception)
                                {
                                    lbl_MainTime.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(12, 6);
                                }
                            }
                            lbl_MainNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtNumberVerified"].ToString().Trim();
                            txt_FTicketCount.Text = ds_ViolationDetail.Tables[0].Rows[0]["FticketCount"].ToString().Trim();
                        }

                    }

                    else
                    {
                        ClearValues();
                        DataSet ds = ClsCaseDetail.GetMaxTVIDByTicketID(Convert.ToInt32(ClsCase.TicketID));
                        int tvid = Convert.ToInt32(ds.Tables[0].Rows[0]["tvid"].ToString());
                        if (tvid != 0)
                        {
                            DisplayInfoNew(tvid);
                        }
                        //IsNewViolation=true;
                        txt_IsNew.Text = "1";
                    }
                    //fills grid information 
                    DataSet ds_CaseDetail = ClsCaseDetail.GetCaseDetail(ClsCase.TicketID);
                    dgViolationInfo.DataSource = ds_CaseDetail;
                    dgViolationInfo.DataBind();
                    lbldetailcount.Text = ds_CaseDetail.Tables[0].Rows.Count.ToString();
                }
                else
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddl_CourtLocation.SelectedIndexChanged += new System.EventHandler(this.ddl_CourtLocation_SelectedIndexChanged);
            this.lnkb_InsertViolation.Click += new System.EventHandler(this.lnkb_InsertViolation_Click);
            this.dgViolationInfo.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgViolationInfo_ItemCommand);
            this.dgViolationInfo.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgViolationInfo_ItemDataBound);
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            this.btn_Update1.Click += new System.EventHandler(this.btn_Update1_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void dgViolationInfo_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {
                //checked the row whose information is displayed on the page
                foreach (DataGridItem dgItem in dgViolationInfo.Items)
                {
                    CheckBox chkb = (CheckBox)dgItem.FindControl("CheckBox1");
                    LinkButton lnkbtn = (LinkButton)dgItem.FindControl("lbl_tno");
                    Label tvid = (Label)dgItem.FindControl("lblTicketNo");

                    string str = ClsCaseDetail.TicketViolationID.ToString();

                    if (tvid.Text == str)
                    {
                        chkb.Checked = true;
                        chkb.Enabled = false;
                    }
                    else
                    {
                        chkb.Checked = false;
                        chkb.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void btn_Update_Click(object sender, System.EventArgs e)
        {
            try
            {
                UpdateViolationDetail();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                ClsSession.CreateCookie("sIsUpdated", "false", this.Request, this.Response);
                //ViewState["vIsUpdated"]="false";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }


        private void UpdateViolationDetail()
        {
            //in case of new violation
            //if (IsNewViolation==true)
            if (txt_IsNew.Text == "1")
            {
                /*
                    DataSet ds_CheckTNum=ClsCaseDetail.CheckTicketNumberExistance(0,txt_TicketNo.Text.Trim(),txt_SequenceNo.Text.Trim(), ClsCase.TicketID,Convert.ToInt32(ddl_CourtLocation.SelectedValue));
                    
                    //bug#1212 changes by saher
                    /*if(ds_CheckTNum.Tables[0].Rows[0][0].ToString()!="0" && ds_CheckTNum.Tables[0].Rows[0][1].ToString()=="2")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Ticket Number(" + txt_TicketNo.Text + ") already exist in our system. Please Specify a different ticket Number or look for this customer in Non Clients');	</script>");
                        TxtValue.Text="0";
                    }
                    else*/
                /*if(ds_CheckTNum.Tables[0].Rows[0][0].ToString()!="0" && ds_CheckTNum.Tables[0].Rows[0][1].ToString()=="0")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Ticket Number(" + txt_TicketNo.Text + "-" + txt_SequenceNo.Text+ ") already exist in our system. Please Specify a different ticket Number or look for this customer in Quotes');	</script>");
                        TxtValue.Text="0";
                    }
                    else if(ds_CheckTNum.Tables[0].Rows[0][0].ToString()!="0" && ds_CheckTNum.Tables[0].Rows[0][1].ToString()=="1")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Ticket Number(" + txt_TicketNo.Text + "-" + txt_SequenceNo.Text+ ") already exist in our system. Please Specify a different ticket Number or look for this customer in Clients');	</script>");
                        TxtValue.Text="0";
                    }
                    else*/
                if (txt_CauseNo.Text.Trim() != "")
                {
                    DataSet ds_CheckCNum = ClsCaseDetail.CheckCauseNumberExistance(0, txt_CauseNo.Text.Trim(), Convert.ToInt32(ddl_CourtLocation.SelectedValue));
                    if (ds_CheckCNum.Tables[0].Rows[0][0].ToString() != "0")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Cause Number(" + txt_CauseNo.Text + ") already exist in our system. Please Specify a different Cause Number');	</script>");
                        TxtValue.Text = "0";
                    }
                    else
                    {
                        SetValuesForInsertion();
                        //if (ClsCaseDetail.InsertCaseDetail() == true)
                        //{
                            txt_IsNew.Text = "0";
                            ClsSession.CreateCookie("sIsUpdated", "true", this.Request, this.Response);
                            Session["vSearch"] = ViewState["vSearch"];
                            Session["vTicketID"] = ViewState["vTicketID"];
                            //ViewState["vIsUpdated"]="true";
                            HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
                        //}
                    }
                }
                else
                {
                    SetValuesForInsertion();
                    //if (ClsCaseDetail.InsertCaseDetail() == true)
                    //{
                        txt_IsNew.Text = "0";
                        ClsSession.CreateCookie("sIsUpdated", "true", this.Request, this.Response);
                        Session["vSearch"] = ViewState["vSearch"];
                        Session["vTicketID"] = ViewState["vTicketID"];
                        //ViewState["vIsUpdated"]="true";
                        HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
                    //}
                }
            }
            //in case of existing violation
            //else if (IsNewViolation==false)
            else if (txt_IsNew.Text == "0")
            {
                SetValuesForUpdation();
                /*
                    DataSet ds_CheckTNum=ClsCaseDetail.CheckTicketNumberExistance(ClsCaseDetail.TicketViolationID, txt_TicketNo.Text.Trim(),txt_SequenceNo.Text.Trim(),ClsCase.TicketID,Convert.ToInt32(ddl_CourtLocation.SelectedValue));
                    //bug#1212 changes by saher
                    /*if(ds_CheckTNum.Tables[0].Rows[0][0].ToString()!="0" && ds_CheckTNum.Tables[0].Rows[0][1].ToString()=="2")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Ticket Number(" + txt_TicketNo.Text + ") already exist in our system. Please Specify a different ticket Number or look for this customer in Non Clients');	</script>");
                        TxtValue.Text="1";
                    }
                    else */
                /*
                    if(ds_CheckTNum.Tables[0].Rows[0][0].ToString()!="0" && ds_CheckTNum.Tables[0].Rows[0][1].ToString()=="0")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Ticket Number(" + txt_TicketNo.Text + "-" + txt_SequenceNo.Text+ ") already exist in our system. Please Specify a different ticket Number or look for this customer in Quotes');	</script>");
                        TxtValue.Text="1";
                    }
                    else if(ds_CheckTNum.Tables[0].Rows[0][0].ToString()!="0" && ds_CheckTNum.Tables[0].Rows[0][1].ToString()=="1")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Ticket Number(" + txt_TicketNo.Text + "-" + txt_SequenceNo.Text+ ") already exist in our system. Please Specify a different ticket Number or look for this customer in Clients');	</script>");
                        TxtValue.Text="1";
                    }
                    else */
                if (txt_CauseNo.Text.Trim() != "")
                {
                    DataSet ds_CheckCNum = ClsCaseDetail.CheckCauseNumberExistance(ClsCaseDetail.TicketViolationID, txt_CauseNo.Text.Trim(), Convert.ToInt32(ddl_CourtLocation.SelectedValue));
                    if (ds_CheckCNum.Tables[0].Rows[0][0].ToString() != "0")
                    {
                        HttpContext.Current.Response.Write("<script language='javascript'>alert('Cause Number(" + txt_CauseNo.Text + ") already exist in our system. Please Specify a different Cause Number');	</script>");
                        TxtValue.Text = "1";
                    }
                    else
                    {
                        if (ClsCaseDetail.UpdateCaseDetail() == true)
                        {
                            //added by zee for validation & discrepancy report
                            Session["FromUpdate"] = "true";
                            //
                            ClsSession.CreateCookie("sIsUpdated", "true", this.Request, this.Response);
                            Session["vSearch"] = ViewState["vSearch"];
                            Session["vTicketID"] = ViewState["vTicketID"];
                            //ViewState["vIsUpdated"]="true";
                            HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
                        }
                    }
                }
                else
                {
                    if (ClsCaseDetail.UpdateCaseDetail() == true)
                    {
                        //added by zee for validation & discrepancy report
                        Session["FromUpdate"] = "true";
                        //
                        ClsSession.CreateCookie("sIsUpdated", "true", this.Request, this.Response);
                        Session["vSearch"] = ViewState["vSearch"];
                        Session["vTicketID"] = ViewState["vTicketID"];
                        //ViewState["vIsUpdated"]="true";
                        HttpContext.Current.Response.Write("<script language='javascript'> opener.location.reload(); self.close();   </script>");
                    }
                }
            }
        }

        //Sets values for insertion of new violation
        //and also updation of selected violation and main
        private void SetValuesForInsertion()
        {
            try
            {

                ClsCaseDetail.TicketID = ClsCase.TicketID;
                if (ClsSession.GetCookie("sIsChanged", this.Request) == "True")
                {
                    ClsCaseDetail.TicketViolationID = Convert.ToInt32(ViewState["vTicketViolationID"]);
                }
                sTVIDs = "";
                foreach (DataGridItem dgItem in dgViolationInfo.Items)
                {
                    CheckBox chkb = (CheckBox)dgItem.FindControl("CheckBox1");
                    Label lbl = (Label)dgItem.FindControl("lblTicketNo");

                    if (chkb.Checked == true)
                    {
                        if (sTVIDs != String.Empty)
                        {
                            sTVIDs = String.Concat(sTVIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            sTVIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }
                }
                ClsCaseDetail.TicketViolationIDs = sTVIDs;
                ClsCaseDetail.RefCaseNumber = txt_TicketNo.Text;
                ClsCaseDetail.CauseNumber = txt_CauseNo.Text.Trim().Replace(" ", "");
                /*if (txt_SequenceNo.Text!="")
                {
                    ClsCaseDetail.SequencNumber=Convert.ToInt32(txt_SequenceNo.Text);
                }*/
                if (TxtValue.Text == "0")
                {
                    ClsCaseDetail.ViolationNumber = Convert.ToInt32(lbl_ViolationID.Text);
                }
                else
                {
                    ClsCaseDetail.ViolationNumber = Convert.ToInt32(ddl_ViolationDescription.SelectedValue);
                }
                ClsCaseDetail.FineAmount = Convert.ToDouble(txt_FineAmount.Text);
                if (txt_BondAmount.Text != "")
                {
                    ClsCaseDetail.BondAmount = Convert.ToDouble(txt_BondAmount.Text);
                }
                else
                {
                    ClsCaseDetail.BondAmount = 0;
                }
                if (chkb_Bond.Checked == true)
                {
                    ClsCaseDetail.BondFlag = 1;
                }
                else if (chkb_Bond.Checked == false)
                {
                    ClsCaseDetail.BondFlag = 0;
                }
                ClsCaseDetail.PlanID = Convert.ToInt32(ddl_PricePlan.SelectedValue);
                ClsCaseDetail.CourtViolationStatusID = Convert.ToInt32(ddl_Status.SelectedValue);
                ClsCaseDetail.UpdateMain = chkb_Main.Checked;
                //if(ddl_Date_Month.SelectedIndex==0 || ddl_Date_Day.SelectedIndex==0 || ddl_Date_Year.SelectedIndex==0 || ddl_Time.SelectedIndex==0)
                //{
                //    DateTime courtDate=Convert.ToDateTime("01/01/1900");
                //    ClsCaseDetail.CourtDate=courtDate;
                //}

                if (txt_Month.Text == "0" || txt_Month.Text == String.Empty || txt_Day.Text == "0" || txt_Day.Text == String.Empty || txt_Year.Text == "0" || txt_Year.Text == String.Empty)
                {
                    DateTime courtDate = Convert.ToDateTime("01/01/1900");
                    ClsCaseDetail.CourtDate = courtDate;
                }

                else
                {
                    DateTime courtDate = Convert.ToDateTime(String.Concat(txt_Month.Text.ToString() + "/", txt_Day.Text.ToString() + "/", txt_Year.Text.ToString() + " ", ddl_Time.SelectedValue.ToString()));
                    ClsCaseDetail.CourtDate = courtDate;
                }

                ClsCaseDetail.CourtNumber = txt_CourtNo.Text;
                ClsCaseDetail.CourtID = Convert.ToInt32(ddl_CourtLocation.SelectedValue);
                ClsCaseDetail.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //sets values for updation of selected violations and main
        private void SetValuesForUpdation()
        {
            try
            {

                if (ClsSession.GetCookie("sIsChanged", this.Request) == "True")
                {
                    ClsCaseDetail.TicketViolationID = Convert.ToInt32(ViewState["vTicketViolationID"]);
                }
                sTVIDs = "";
                foreach (DataGridItem dgItem in dgViolationInfo.Items)
                {
                    CheckBox chkb = (CheckBox)dgItem.FindControl("CheckBox1");
                    Label lbl = (Label)dgItem.FindControl("lblTicketNo");

                    if (chkb.Checked == true)
                    {
                        if (sTVIDs != String.Empty)
                        {
                            sTVIDs = String.Concat(sTVIDs, lbl.Text.ToString().Trim(), ",");
                        }
                        else
                        {
                            sTVIDs = String.Concat(lbl.Text.ToString().Trim(), ",");
                        }
                    }
                }
                ClsCaseDetail.TicketViolationIDs = sTVIDs;
                ClsCaseDetail.RefCaseNumber = txt_TicketNo.Text;
                ClsCaseDetail.CauseNumber = txt_CauseNo.Text.Trim().Replace(" ", "");
                /*if(txt_SequenceNo.Text.Trim()!="")
                {
                    ClsCaseDetail.SequencNumber=Convert.ToInt32(txt_SequenceNo.Text);
                }*/
                if (TxtValue.Text == "0")
                {
                    ClsCaseDetail.ViolationNumber = Convert.ToInt32(lbl_ViolationID.Text);
                }
                else
                {
                    ClsCaseDetail.ViolationNumber = Convert.ToInt32(ddl_ViolationDescription.SelectedValue);
                }
                ClsCaseDetail.FineAmount = Convert.ToDouble(txt_FineAmount.Text);
                if (txt_BondAmount.Text != "")
                {
                    ClsCaseDetail.BondAmount = Convert.ToDouble(txt_BondAmount.Text);
                }
                else
                {
                    ClsCaseDetail.BondAmount = 0;
                }
                if (chkb_Bond.Checked == true)
                {
                    ClsCaseDetail.BondFlag = 1;
                }
                else if (chkb_Bond.Checked == false)
                {
                    ClsCaseDetail.BondFlag = 0;
                }
                ClsCaseDetail.PlanID = Convert.ToInt32(ddl_PricePlan.SelectedValue);
                ClsCaseDetail.CourtViolationStatusID = Convert.ToInt32(ddl_Status.SelectedValue);

                //if(ddl_Date_Month.SelectedIndex==0 || ddl_Date_Day.SelectedIndex==0 || ddl_Date_Year.SelectedIndex==0 || ddl_Time.SelectedIndex==0)
                if (txt_Month.Text == "0" || txt_Month.Text == String.Empty || txt_Day.Text == "0" || txt_Day.Text == String.Empty || txt_Year.Text == "0" || txt_Year.Text == String.Empty)
                {
                    DateTime courtDate = Convert.ToDateTime("01/01/1900");
                    ClsCaseDetail.CourtDate = courtDate;
                }
                else
                {
                    DateTime courtDate = Convert.ToDateTime(String.Concat(txt_Month.Text.ToString() + "/", txt_Day.Text.ToString() + "/", txt_Year.Text.ToString() + " ", ddl_Time.SelectedValue.ToString()));
                    ClsCaseDetail.CourtDate = courtDate;
                }

                ClsCaseDetail.CourtNumber = txt_CourtNo.Text;
                ClsCaseDetail.UpdateMain = chkb_Main.Checked;
                if (txtinactive.Text == "1")
                {
                    ClsCaseDetail.CourtID = Convert.ToInt32(lblinactiveCourtID.Text);
                }
                else
                {
                    ClsCaseDetail.CourtID = Convert.ToInt32(ddl_CourtLocation.SelectedValue);
                }

                ClsCaseDetail.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }
        //clears the form and remove the checked from the grid
        private void lnkb_InsertViolation_Click(object sender, System.EventArgs e)
        {
            try
            {
                foreach (DataGridItem dgItem in dgViolationInfo.Items)
                {
                    CheckBox chkb = (CheckBox)dgItem.FindControl("CheckBox1");
                    chkb.Checked = false;
                    chkb.Enabled = true;
                }
                ClearValues();
                DataSet ds = ClsCaseDetail.GetMaxTVIDByTicketID(Convert.ToInt32(ClsCase.TicketID));
                int tvid = Convert.ToInt32(ds.Tables[0].Rows[0]["tvid"].ToString());
                if (tvid != 0)
                {
                    DisplayInfoNew(tvid);
                }
                //IsNewViolation=true;
                txt_IsNew.Text = "1";
                txtinactive.Text = "0";
                lblinactiveCourtName.Visible = false;
                ddl_CourtLocation.Visible = true;
                ddl_PricePlan.Enabled = true;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }
        private void DisplayInfoNew(int tvid)
        {
            try
            {
                DataSet ds_ViolationDetail = ClsCaseDetail.GetCaseDetailByViolationID(tvid);
                if (ds_ViolationDetail.Tables[0].Rows.Count > 0)
                {
                    txt_TicketNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["RefCaseNumber"].ToString().Trim();

                    try
                    {
                        ddl_Status.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["VerifiedStatusID"].ToString().Trim();
                        if (ddl_Status.SelectedValue == "80")
                        {
                            ddl_Status.SelectedIndex = 0;
                            lbl_Status.Text = "";
                        }
                        else
                        {
                            lbl_Status.Text = ddl_Status.SelectedItem.Text;
                        }
                    }
                    catch (Exception)
                    { }

                    if (ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString() != "" && ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Substring(0, 10) != "01/01/1900")
                    {
                        txt_Day.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(3, 2);
                        txt_Month.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(0, 2);
                        txt_Year.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Trim().Substring(8, 2);
                        try
                        {
                            ddl_Time.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Substring(11, 7).Trim();
                        }
                        catch (Exception)
                        {
                            try
                            {
                                ddl_Time.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateVerified"].ToString().Substring(12, 7).Trim();
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    if (ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString() == "3000" || ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString() == "")
                    {
                        ddl_CourtLocation.SelectedValue = "0";
                        FillPricePlan();
                    }
                    else
                    {
                        txtinactive.Text = ds_ViolationDetail.Tables[0].Rows[0]["inactive"].ToString().Trim();
                        if (txtinactive.Text == "1")
                        {
                            ddl_CourtLocation.SelectedValue = "0";
                            FillPricePlan();
                        }
                        else
                        {
                            ddl_CourtLocation.SelectedValue = ds_ViolationDetail.Tables[0].Rows[0]["CourtID"].ToString().Trim();
                            ClsCaseDetail.PlanID = Convert.ToInt32(ds_ViolationDetail.Tables[0].Rows[0]["PlanId"].ToString().Trim());
                            FillPricePlan();
                        }
                    }

                    txt_CourtNo.Text = ds_ViolationDetail.Tables[0].Rows[0]["CourtNumberVerified"].ToString().Trim();

                    int bondMain = Convert.ToInt32(ds_ViolationDetail.Tables[0].Rows[0]["BondFlagMain"].ToString().Trim());
                    if (bondMain == 1)
                    {
                        txt_BondFlag.Text = "1";
                    }
                    else if (bondMain == 0)
                    {
                        txt_BondFlag.Text = "0";
                    }

                    //					lbl_OfficerName.Text=ds_ViolationDetail.Tables[0].Rows[0]["OfficerName"].ToString().Trim();
                    //					lbl_Day.Text=ds_ViolationDetail.Tables[0].Rows[0]["OfficerDay"].ToString().Trim();
                }
                else
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = "Information not retrived for the given ticket";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }
        //clears the page for insertion
        private void ClearValues()
        {
            try
            {
                TxtValue.Text = "0";
                txt_TicketNo.Text = String.Empty;
                //txt_SequenceNo.Text=String.Empty;
                txt_CauseNo.Text = String.Empty;
                ddl_ViolationDescription.ClearSelection();
                lbl_ViolationDescription.Text = "";
                lbl_ViolationID.Text = "";
                txt_FineAmount.Text = "0";
                txt_BondAmount.Text = "0";
                chkb_Bond.Checked = false;
                ddl_PricePlan.Items.Clear();
                ddl_Status.ClearSelection();
                //ddl_Date_Month.ClearSelection();
                //ddl_Date_Day.ClearSelection();
                //ddl_Date_Year.ClearSelection();
                ddl_Time.ClearSelection();
                txt_CourtNo.Text = String.Empty;
                ddl_CourtLocation.ClearSelection();
                lbl_AutoCourtDate.Text = String.Empty;
                lbl_AutoNo.Text = String.Empty;
                lbl_AutoStatus.Text = String.Empty;
                lbl_AutoStatusID.Text = String.Empty;
                lbl_AutoTime.Text = String.Empty;
                lbl_ScanCourtDate.Text = String.Empty;
                lbl_ScanNo.Text = String.Empty;
                lbl_ScanStatus.Text = String.Empty;
                lbl_ScanTime.Text = String.Empty;
                lbl_MainCourtDdate.Text = String.Empty;
                lbl_MainNo.Text = String.Empty;
                lbl_MainStatus.Text = String.Empty;
                lbl_MainTime.Text = String.Empty;

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        //Displays the information on the page when ticket number link is selected
        private void dgViolationInfo_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "TicketClick")
                {
                    Label vid = (Label)e.Item.FindControl("lblTicketNo");
                    ClsCaseDetail.TicketViolationID = Convert.ToInt32(vid.Text);
                    //ClsSession.CreateCookie("sTicketViolationID",vid.Text,this.Request, this.Response);
                    ViewState["vTicketViolationID"] = vid.Text;
                    ClsSession.CreateCookie("sIsChanged", "True", this.Request, this.Response);
                    DisplayInfo();
                }
            }
            catch (Exception ex)
            {
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills the Price Plan according to court id selected.
        private void ddl_CourtLocation_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            FillPricePlan();
        }

        private void btn_Update1_Click(object sender, System.EventArgs e)
        {
            try
            {
                UpdateViolationDetail();
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                ClsSession.CreateCookie("sIsUpdated", "false", this.Request, this.Response);
                //ViewState["vIsUpdated"]="false";
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }

        private void CheckCaseStatus()
        {
            bool flag_display = true;

            if (lbl_AutoStatus.Text == lbl_ScanStatus.Text && lbl_AutoStatus.Text == lbl_MainStatus.Text)
                if (lbl_ScanStatus.Text == lbl_MainStatus.Text)
                    flag_display = false;

            if (lbl_AutoCourtDate.Text == lbl_ScanCourtDate.Text && lbl_AutoCourtDate.Text == lbl_MainCourtDdate.Text)
                if (lbl_ScanCourtDate.Text == lbl_MainCourtDdate.Text)
                    flag_display = false;

            if (lbl_AutoTime.Text == lbl_ScanTime.Text && lbl_AutoTime.Text == lbl_MainTime.Text)
                if (lbl_ScanTime.Text == lbl_MainTime.Text)
                    flag_display = false;

            if (lbl_AutoNo.Text == lbl_ScanNo.Text && lbl_AutoNo.Text == lbl_MainNo.Text)
                if (lbl_ScanNo.Text == lbl_MainNo.Text)
                    flag_display = false;


            if (flag_display == false)
            {
                tbl_autoscan.Style["Display"] = "none";
                tbl_autoscan_header.Style["Display"] = "none";
            }
            else
            {
                tbl_autoscan.Style["Display"] = "block";
                tbl_autoscan_header.Style["Display"] = "block";
            }

        }
    }
}

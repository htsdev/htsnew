
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using lntechNew.Components.ClientInfo;
using FrameWorkEnation.Components;
using lntechNew.WebControls;
using lntechNew.Components;
using HTP.WebComponents;
using System.Configuration;
using HTP.Components.ClientInfo;
using HTP.Components;
using System.Globalization;
using System.Text;  //SAEED-7746-05/15/2010, add to use string builder.
using System.IO;    //SAEED-7746-05/15/2010, needed for stream reader.
using System.Data.SqlClient;
using System.Linq;

namespace HTP.ClientInfo
{
    /// <summary>lnk_SendSMS.Visible = true;
    /// Summary description for ViolationFeeNew.Test
    /// </summary>
    public partial class ViolationFeeOld : System.Web.UI.Page
    {
        #region Properties

        //Sabir Khan 6851 12/04/2009 static keyword has been removed...
        private bool AFlag = false;
        private bool CFlag = false;
        private int Speeding = 0;
        private bool ArrStatus = false;
        private bool Conti = false;
        private double IAdj = 0;
        public bool notecallback = false;
        private bool isRedirectedFromPC = false;
        private double FAdj = 0;
        private bool pPlan = false;
        private int CurrentCaseType = CaseType.Traffic;
        private string StrExp = string.Empty;
        private string StrAcsDec = string.Empty;
        private bool PayPlanFlag = false;

        #endregion

        #region Variables

        clsCase ClsCase = new clsCase();
        clsCaseDetail ClsCaseDetail = new clsCaseDetail();
        clsReadNotes notes = new clsReadNotes();
        clsCourts ClsCourts = new clsCourts();
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();
        clsContactType ClsContactType = new clsContactType();
        clsAdSource ClsAdSource = new clsAdSource();
        clsFirms ClsFirms = new clsFirms();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsGeneralMethods general = new clsGeneralMethods();
        //Waqas 5771 04/20/2009
        clsContact ClsContact = new clsContact();
        // Noufil 6138 08/04/2009 Object created
        LanguageService languages = new LanguageService();
        updatePanel upnl = new updatePanel();
        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Check for valid Session if not then redirect to login page	
                //Zeeshan 4036 05/15/2008 Check emp id in cookies if not exist than redirect user to Login page.               
                int Empid = 0;
                int CaseTypeID1 = 1;
                Int32.TryParse(Request.QueryString["casetype"], out CaseTypeID1);
                if (CaseTypeID1 == 1)
                {
                    lblFormName.Text = "Traffic Prospect";
                }
                else if (CaseTypeID1 == 2)
                {
                    lblFormName.Text = "Criminal Prospect";
                }
                else if (CaseTypeID1 == 3)
                {
                    lblFormName.Text = "Civil Prospect";
                }
                else if (CaseTypeID1 == 4)
                {
                    lblFormName.Text = "Family Law Prospect";
                }

                //Waqas 5057 03/17/2009 Checking employee info in session               
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false ||
                        (!int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {

                    //Nasir 5902 06/11/2009 to resolve business logic issue added query string sMenu check
                    if (Request.QueryString["sMenu"] == null)
                    {
                        string URL = Request.Url.AbsoluteUri.ToString();
                        Response.Redirect(URL + "&sMenu=61", false);
                        goto SearchPage;
                    }

                    // Abbas Shahid Khwaja 9435 06/29/2011 font size,bold and name is reset.
                    lbl_Message.CssClass = "FlagMessageNormal";

                    // destroying dataview object from session created on main page...
                    Session["dv_Result"] = null;
                    UpdateViolationSRV1.PageMethod += delegate()
                    {
                        lbl_Message.Text = UpdateViolationSRV1.ErrorMessage;
                        lbl_Message.Visible = true;

                        //Farrukh 9376 06/14/2011 Error message added for same Cause Number
                        test.Text = UpdateViolationSRV1.ErrorMessage;

                        DisplayInfo();
                        //// Noufil 5884 07/08/2009 Refresh SMS body and controls
                        BindSmsControl();


                    };
                    //Waqas 5771 04/14/2009 Page method added
                    ContactInfo1.ContactPageMethod += delegate()
                    {
                        lbl_Message.Text = string.Empty;
                        DisplayInfo();
                    };

                    ContactLookUp.ContactPageMethod += delegate()
                    {
                        lbl_Message.Text = string.Empty;
                        DisplayInfo();
                    };

                    ClsSession.CreateCookie("sIsChanged", "False", this.Request, this.Response);

                    if (UpdateViolationSRV1.ErrorMessage == "" && lbl_Message.Text != "")
                    {
                        lbl_Message.Text = "";
                    }
                    else
                    {
                        lbl_Message.Visible = true;
                    }

                    //Sabir Khan 5009 11/05/2008 Time Stamp set for page refresh ...
                   // txtTimeStamp.Text = System.DateTime.Now.TimeOfDay.ToString();
                   



                    if (!IsPostBack)
                    {

                        int ticketID_Comment = 0;
                        if (ViewState["vTicketID"] != null) { ticketID_Comment = Convert.ToInt32(ViewState["vTicketID"]); } else { ticketID_Comment = 0; }

                        //Zeeshan Ahmed 3820 04/22/2008
                        WCC_GeneralComments.Initialize(ticketID_Comment, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 1, "Label", "clsInputadministration", "clsbutton");
                        WCC_GeneralComments.Enabled = false;
                        WCC_GeneralComments.Button_Visible = false;
                        chkbtn_read.Visible = false;
                        ReadNotes1.Visible = false;

                        //For Calculate button
                        ViewState["vFromCal"] = "false";
                        pPlan = false;
                        //page validation
                        btn_Update.Attributes.Add("OnClick", " return submitForm(4,0);");
                        btn_Calculate.Attributes.Add("OnClick", "return submitForm(4,1);plzwait1();");
                        btn_Lock.Attributes.Add("OnClick", "return submitForm(5,0); plzwait1();");
                        btnNext.Attributes.Add("OnClick", "return submitForm(2,0);");
                        //Insert Violation Link which opens update violation page
                        lnkbtn_Calculation.Attributes.Add("OnClick", "return openwindow1();");
                        //cal_CallBack.SelectedDate = DateTime.Now;   

                        // Means redirected from Client Lookup Report.....
                        // Added by Tahir Ahmed......
                        ///---------------------------------------------------------------------
                        if (Request.QueryString["PC"] == "1")
                            isRedirectedFromPC = true;
                        else
                            isRedirectedFromPC = false;
                        ///---------------------------------------------------------------------

                        //Means Redirected from Main page					
                        // ViewState Bug Fixed By Zeeshan Ahmed On 6/18/2007

                        if (Request.QueryString.Count >= 2)
                        {
                            if (Convert.ToString(Request.QueryString["caseNumber"]) != "" && Convert.ToString(Request.QueryString["search"]) != "" && Request.QueryString["caseNumber"] != null && Request.QueryString["search"] != null)
                            {
                                ViewState["vSearch"] = Request.QueryString["search"];
                                ViewState["vTicketID"] = Request.QueryString["caseNumber"];
                            }
                            else
                            {
                                Response.Redirect("../frmMain.aspx", false);
                                goto SearchPage;
                            }
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }

                        // Abbas Qamar 10114 Filling the State in DropDownList
                        // Rab Nawaz Khan 01/15/2015 code moved to FillBasicData() method to call through single DB roundtrip. . .
                        //DataSet dsDlstate = clsdb.Get_DS_BySP("USP_HTS_Get_DL_State");
                        //ddl_dlState.DataSource = dsDlstate;
                        //ddl_dlState.DataTextField = dsDlstate.Tables[0].Columns[1].ColumnName;
                        //ddl_dlState.DataValueField = dsDlstate.Tables[0].Columns[0].ColumnName;
                        //ddl_dlState.DataBind();

                        //DataSet dsSSN = clsdb.Get_DS_BySP("USP_HTS_Get_SSNumberType");
                        //DDLSSNQuestion.DataSource = dsSSN;
                        //DDLSSNQuestion.DataTextField = dsSSN.Tables[0].Columns[1].ColumnName;
                        //DDLSSNQuestion.DataValueField = dsSSN.Tables[0].Columns[0].ColumnName;
                        //DDLSSNQuestion.DataBind();
                        DataSet dsBasicdata = HttpContext.Current.Cache["DsBasicData"] as DataSet;
                        if (dsBasicdata == null)
                        {
                            dsBasicdata = clsdb.Get_DS_BySP("USP_HTS_FillBasicData");
                            HttpContext.Current.Cache.Insert("DsBasicData", dsBasicdata, null, DateTime.Now.AddDays(7), System.Web.Caching.Cache.NoSlidingExpiration);
                        }
                        FillBasicData(dsBasicdata);

                        // End 10114


                        //modified by ozair 
                        //change the place(placed from the above two checks to here); query string variables are not available
                        //when some other tab links are clicked.
                        //GetCallbackInfo();
                        ibtn_ViolationDataInfo.Attributes.Add("OnClick", "return show_popup('" + Request.QueryString["caseNumber"].ToString() + "')");
                        //modifcations end

                        //Check for User Type 2 means Admin, 1 means application user
                        //price plan will be visible to admin users only
                        if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "2")
                        {
                            //dgViolationInfo.Columns[8].Visible=true;
                            lblAdmin.Text = "1";
                        }
                        else if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "1")
                        {
                            //dgViolationInfo.Columns[8].Visible=false;
                            lblAdmin.Text = "0";
                        }
                        ViewState["empid"] = ClsSession.GetCookie("sEmpID", this.Request).ToString();


                        lblsearchtype.Text = Convert.ToString(ViewState["vSearch"]);//ClsSession.GetCookie("sSearch",this.Request );
                        lblCurrentDate.Text = String.Format("{0:MM/dd/yyyy}", DateTime.Now);

                        //Filling Controls Values                         
                        FillCourtLocation(dsBasicdata.Tables[2]); // Setting Shot court Short Names
                        FillContactType(dsBasicdata.Tables[3]); // setting contact types
                        FillFirms(dsBasicdata.Tables[4]); // setting All Firms
                        FillSpeeding(dsBasicdata.Tables[5]); // setting Speeding info.
                        FillLanguages(dsBasicdata.Tables[6]); // setting languages
                        //Sabir Khan 11509 11/13/2013 Fill How did you find by drop down.
                        FillFindBy(dsBasicdata.Tables[7]); // setting find By info.
                        // Noufil 09/22/2008 Case type added dynamically ... removing hardcode from aspx code
                        //CaseType ctype = new CaseType();
                        //ddl_CaseType.DataSource = ctype.GetAllCaseType();
                        ddl_CaseType.DataSource = dsBasicdata.Tables[8]; // Setting All case Types
                        ddl_CaseType.DataBind();

                        //Zeeshan Ahmed 3979 05/22/2008
                        if (Request.QueryString["casetype"] != null)
                        {
                            int CaseTypeID = 1;
                            Int32.TryParse(Request.QueryString["casetype"], out CaseTypeID);
                            // Sabir Khan 4636 08/27/2008
                            ddl_CaseType.SelectedValue = CaseTypeID.ToString();
                            SetFeeQuestions(CaseTypeID);
                            ViewState["CaseTypeID"] = CaseTypeID;
                        }
                        //Yasir Kamal 7355 02/03/2010 set call back for criminal and family cases.
                        //Fahad 11146 06/11/2013 Set tomorrow (Next Day including Saturday) call back for criminal cases.
                        if (ViewState["CaseTypeID"] != null && (ViewState["CaseTypeID"].ToString() == "2" || ViewState["CaseTypeID"].ToString() == "4"))
                        {
                            switch (DateTime.Today.DayOfWeek)
                            {
                                case DayOfWeek.Friday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(7);
                                    break;
                                case DayOfWeek.Saturday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(2);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(6);
                                    break;
                                case DayOfWeek.Sunday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(5);
                                    break;
                                case DayOfWeek.Monday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(4);
                                    break;
                                case DayOfWeek.Tuesday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(3);
                                    break;
                                case DayOfWeek.Wednesday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(2);
                                    break;
                                case DayOfWeek.Thursday:
                                    cal_cCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                    break;
                            }
                        }
                        else if (DateTime.Now.Date.AddDays(3).DayOfWeek == DayOfWeek.Sunday)
                        {
                            cal_CallBack.SelectedDate = DateTime.Now.Date.AddDays(4);
                            cal_cCallBack.SelectedDate = DateTime.Now.Date.AddDays(1);
                            cal_vCallBack.SelectedDate = DateTime.Now.Date.AddDays(4);
                        }
                        else
                        {
                            cal_CallBack.SelectedDate = DateTime.Now.Date.AddDays(3);
                            cal_cCallBack.SelectedDate = DateTime.Now.Date.AddDays(1);
                            cal_vCallBack.SelectedDate = DateTime.Now.Date.AddDays(3);
                        }


                        chkbtn_read.Attributes.Add("OnClick", "return ShowReadNotes();");
                        //chkbtn_Delete.Attributes.Add("OnClick", "return DeleteReadNotes();");

                        //Aziz 2498 . take care while parsing intergr.
                        int tempTicketId;
                        if (int.TryParse(ViewState["vTicketID"].ToString(), out tempTicketId))
                            ClsCase.TicketID = tempTicketId;
                        else
                            ClsCase.TicketID = 0;

                        GetMailerID(ClsCase.TicketID);
                        //if client or quote
                        //if(ClsSession.GetCookie("sSearch",this.Request)=="0" || ClsSession.GetCookie("sSearch",this.Request)=="1")

                        //Commented it as it looks like some unnecessaray code.
                        //if (ViewState["vSearch"].ToString() == "0" || ViewState["vSearch"].ToString() == "1")
                        //{
                        //    ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketID"]);

                        //}
                        //Aziz end
                        //if New Case
                        if (ClsCase.TicketID == 0)
                        {
                            //Nasir 5902 06/25/2009 remove query string sMenu check to resolve business logic issue
                            if (Request.QueryString.Count == 0)
                            {
                                Response.Redirect("../frmMain.aspx?Menu=16", false);
                            }
                            else
                            {

                                ViewState["vTicketID"] = "0";

                                lblisNew.Text = "1";
                                lnkbtn_InsertViolation.Attributes.Add("OnClick", "return openwindow(" + 0 + "," + 1 + ",'" + UpdateViolationSRV1.ClientID + "');");
                                btn_Calculate.Enabled = false;
                                btn_Lock.Enabled = false;
                                ddl_Speeding.Enabled = false;
                                tr_Speeding.Style["display"] = "none";
                                tr_SpeedingViolations.Style["display"] = "none";
                                lblIsQuoteExist.Text = "false";
                                lbl_InitialAdjustment.Visible = false;
                                txt_InitialAdjustment.Visible = true;
                                txt_FinalAdjustment.Visible = false;
                            }
                        }
                        else
                        {

                            btn_Calculate.Enabled = true;
                            ddl_Speeding.Enabled = true;
                            tr_Speeding.Style["display"] = "table-row";
                            tr_SpeedingViolations.Style["display"] = "table-row";
                            
                            DisplayInfo();
                            //Aziz
                            //CheckReadNotes(Convert.ToInt32(ViewState["vTicketID"]));
                            CheckReadNotes(ClsCase.TicketID);
                            string ssearch = Convert.ToString(ViewState["vSearch"]);
                            if (ssearch != null)
                                lnkbtn_InsertViolation.Attributes.Add("OnClick", "return openwindow(" + 0 + "," + ssearch + ",'" + UpdateViolationSRV1.ClientID + "');");

                            if (ssearch == "0")
                                hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            else if (ssearch == "1")
                                hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                        }

                        // Noufil 5884 07/08/2009 Show Send SMS Linkbutton if client else hide.
                        if (ClsCase.IsClient(ClsCase.TicketID))
                        {
                            lnk_SendSMS.Visible = true;
                            //Waqas 5864 07/16/2009 Pipe Sign
                            lbl_SendSMS.Visible = true;
                            //// Noufil 5884 07/08/2009 bind SMS body and controls
                            BindSmsControl();

                            // Rab Nawaz 9961 01/03/2011 SMS Should not be sent if the cause number contains the NISI associated 
                            hf_smsAlertToNisi.Value = ClsCase.IsNisiAssociatedCase(ClsCase.TicketID) ? "1" : "0";
                        }
                        else
                        {
                            lnk_SendSMS.Visible = false;
                            //Waqas 5864 07/16/2009 Pipe Sign
                            lbl_SendSMS.Visible = false;
                        }
                    }
                    
                    //btn_Update.Attributes.Add("onclick", " return ValidateCommentControls();");
                    //WCC_GeneralComments.btn_AddComments.Attributes.Add("onclick", " return ValidateCommentControls();");
                    //if Client or Quote
                    //This code is outside IsPostBack block so repeated code.
                    //Aziz 2498 . take care while parsing intergr.
                    if (Convert.ToString(ViewState["vSearch"]) == "0" || Convert.ToString(ViewState["vSearch"]) == "1")
                    {
                        int tempTicketId;
                        if (int.TryParse(ViewState["vTicketID"].ToString(), out tempTicketId))
                            ClsCase.TicketID = tempTicketId;
                        else
                            ClsCase.TicketID = 0;
                    }

                }
                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");

                // Modified by kazim for task-2649
                //This bug can be come if viewstate value expired.For resolving this issue i applied a check
                //which assign viewstate["ticketid"]=0 when it does not found viewstate.

                if (ViewState["vTicketID"] == null)
                {
                    ViewState["vTicketID"] = "0";
                }
                txt1.Text = ViewState["vTicketID"].ToString();
                TextBox txt2 = (TextBox)am.FindControl("txtsrch");

                //BindFlags();
                BindFlagsNew();
                // Afaq 8311 09/30/2010 Get the number of violataion in profile having Bond as verified status.
                UpdateViolationSRV1.BondStatusCount = hf_statusCount.Value = Convert.ToString(ClsCase.ViolationCountByStatus(CourtViolationStatusType.BOND));

                //Kazim 2767 2/1/2008 
                //This bug comes if viewstate contains null values and when this null  value convert to string using ToString() method then object
                //reference error comes. For resolving this issue i use convert.tostring function  which converts the null value to 
                //empty string.

                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if ((test.Text.Length > 1) || (lbl_Message.Text.Length > 1 && lbl_Message.Visible))
                    tr_msgsForCIDandCuase.Visible = true;
                //   tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible = false; 
                //tr_msgsForCIDandCuase.Visible=false;

                txt2.Text = Convert.ToString(ViewState["vSearch"]);

                

            SearchPage:
                { }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if ((test.Text.Length > 1) || (lbl_Message.Text.Length > 1 && lbl_Message.Visible))
                    tr_msgsForCIDandCuase.Visible = true;
              //  tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible = false;
               // tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }


        // Rab Nawaz Khan 01/15/2015 Added to reduce the page load time by calling the Sps with DB round trip . . . 
        private void FillBasicData(DataSet dsBasicData)
        {
            ddl_dlState.DataSource = dsBasicData.Tables[0];
            ddl_dlState.DataTextField = dsBasicData.Tables[0].Columns[1].ColumnName;
            ddl_dlState.DataValueField = dsBasicData.Tables[0].Columns[0].ColumnName;
            ddl_dlState.DataBind();

            DDLSSNQuestion.DataSource = dsBasicData.Tables[1];
            DDLSSNQuestion.DataTextField = dsBasicData.Tables[1].Columns[1].ColumnName;
            DDLSSNQuestion.DataValueField = dsBasicData.Tables[1].Columns[0].ColumnName;
            DDLSSNQuestion.DataBind();
        }


        // Noufil 6138 08/04/2009 Method created to fill language
        /// <summary>
        ///     THis method is use to fill language drop down.
        /// </summary>
        private void FillLanguages(DataTable dtTable)
        {
            string lang = string.Empty;
            string dName = String.Empty;
            DataTable dtLanguages;
            if (dtTable.Rows.Count == 0)
                dtLanguages = languages.GetAllLanguages();
            else
                dtLanguages = dtTable;
            
            DataRow[] drLanguages = new DataRow[2];

            for (int i = 0; i < dtLanguages.Rows.Count; i++)
            {
                if (dtLanguages.Rows[i]["LanguageName"].ToString() == "ENGLISH")
                {
                    drLanguages[0] = dtLanguages.NewRow();
                    drLanguages[0]["LanguageID"] = dtLanguages.Rows[i]["LanguageID"];
                    drLanguages[0]["LanguageName"] = dtLanguages.Rows[i]["LanguageName"];
                    dtLanguages.Rows.RemoveAt(i);
                }
                if (dtLanguages.Rows[i]["LanguageName"].ToString() == "SPANISH")
                {
                    drLanguages[1] = dtLanguages.NewRow();
                    drLanguages[1]["LanguageID"] = dtLanguages.Rows[i]["LanguageID"];
                    drLanguages[1]["LanguageName"] = dtLanguages.Rows[i]["LanguageName"];
                    dtLanguages.Rows.RemoveAt(i);
                }
            }
            dtLanguages.Rows.InsertAt(drLanguages[0], 1);
            dtLanguages.Rows.InsertAt(drLanguages[1], 2);

            //ddl_language.DataSource = languages.GetAllLanguages();
            ddl_language.DataSource = dtLanguages;
            ddl_language.DataBind();

            ddl_language.Items.Insert(ddl_language.Items.Count, new ListItem("OTHER", "0"));

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);

            foreach (CultureInfo CI in cultures)
            {
                // IF culture language contains Language with "(" then remove text after "("
                if (CI.DisplayName.Contains("("))
                {
                    dName = CI.DisplayName.Substring(0, CI.DisplayName.IndexOf('(')).Trim();
                }
                else
                {
                    dName = CI.DisplayName.Trim();
                }

                // If Language already added in array then don't add it again.
                if (ddl_language.Items.FindByText(dName.ToUpper()) == null)
                {
                    if (!lang.Contains(dName.ToUpper()))
                    {
                        lang += dName.ToUpper() + "|";
                        //languages.InsertLanguage(dName.ToUpper());
                    }
                }
            }

            if (lang != string.Empty)
            {
                string[] langArray = lang.Substring(0, lang.Length - 1).Split('|');
                Array.Sort(langArray);

                dd_OtherLanguage.DataSource = langArray;
                dd_OtherLanguage.DataBind();
            }
            else
            {
                dd_OtherLanguage.Items.Clear();
            }

            if (dd_OtherLanguage.Items.FindByValue("-1") == null)
                dd_OtherLanguage.Items.Insert(0, new ListItem("-- CHOOSE --", "-1"));

        }
        /// <summary>
        /// //Sabir Khan 11509 11/13/2013 This method is used to fill the how did you find by drop down.
        /// </summary>
        private void FillFindBy(DataTable dtFindBy)
        {
            if (dtFindBy != null && dtFindBy.Rows.Count > 0)
            {
                //DataTable dt = new DataTable();
                //dt = ClsCase.FillFindBy();
                DataRow dr = dtFindBy.NewRow();
                dr["ID"] = -1;
                dr["FindBy"] = "--Choose--";
                dtFindBy.Rows.InsertAt(dr, 0);
                DataRow dr1 = dtFindBy.NewRow();
                dr1["ID"] = 0;
                dr1["FindBy"] = "Other (please specify)";
                dtFindBy.Rows.Add(dr1);
                DataView view = new DataView(dtFindBy);
                DataTable distinctValues = view.ToTable(true, "ID", "FindBy");
                ddlFindBy.DataSource = distinctValues;
                ddlFindBy.DataTextField = "FindBy";
                ddlFindBy.DataValueField = "ID";
                ddlFindBy.DataBind();
            }
        }
        private void btn_Update_Click(object sender, System.EventArgs e)
        {
            try
            {
                // tahir 5325 12/18/2008 commented the code as it was not updating the notes 
                // specifically for multiuser issue...

                ////Sabir Khan 5009 11/05/2008 Check for page refresh ...
                string strReq =  Request.Form["txtTimeStamp"].ToString();
                //if (Session["TimeStamp"] != null && Session["TimeStamp"].ToString() == strReq.ToString())
                //{
                lbl_Message.Text = "";
                //Code Added by Azwer..for modification send on 13 march pointno 4b
                string bflag = "0";
                string cdl = "0";
                string acc = "0";

                //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                if (rdbtn_BondYes.Checked || rdbtn_cBondYes.Checked)
                    bflag = "1";
                if (rdbtn_CDLYes.Checked || rdbtn_cCDLYes.Checked)
                    cdl = "1";
                if (rdbtn_AccidentYes.Checked || rdbtn_cAccidentYes.Checked)
                    acc = "1";

                //Added by Zeeshan Ahmed For Criminal Court Modification 2708
                if (!(lblLockFlag.Text == "True" && ClsCase.IsCriminalCase()))
                {
                    if (ViewState["bflag"] != null)
                    {
                        if (ViewState["bflag"].ToString() != bflag || ViewState["acc"].ToString() != acc || ViewState["cdl"].ToString() != cdl)
                        {
                            lbl_Message.Visible = true;
                            lbl_Message.Text = "Record Updated; You must recalculate the price before processing a payment on the payment info page.";
                        }
                    }
                }

                //Line below Comment by Azee for Modification send on 13 march point no 4a.
                if (lblLockFlag.Text == "False" && lbl_ActiveFlag.Text == "1")
                {
                    lbl_Message.Visible = true;
                    //lbl_Message.Text="Record Updated; You must recalculate the price before processing a payment on the payment info page.";
                }
                try
                {
                    //nasir 5381 01/15/2009 CHecking is comment update from other side

                    bool Check = ClsCase.IsLastGeneralCommentsDateUpdated(hf_LastGerenalcommentsUpdatedate.Value);
                    if (!Check)
                    {
                        Modal_Message.Show();
                    }
                    else
                        WCC_GeneralComments.AddComments();

                    ClsCase.CaseType = Convert.ToInt32(ddl_CaseType.SelectedValue);
                    //Abbas Qamar 10114 04-06-2012 passing the DL Number values in parameter

                    if (RadioButton2.Checked)
                    {
                        ClsCase.DLNumber = "";
                        ClsCase.NoDL = Convert.ToInt32(RadioButton2.Checked);
                    }
                    else
                    {
                        ClsCase.DLStateID = Convert.ToInt32(ddl_dlState.SelectedValue);
                        ClsCase.DLNumber = txt_dlstr.Text;
                        ClsCase.NoDL = 0;

                    }


                    if (RadioButton3.Checked)
                    {

                        ClsCase.SSN = txt_SSN.Text;
                    }
                    else
                        if (RadioButton4.Checked && DDLSSNQuestion.SelectedValue != "6")
                        {
                            ClsCase.SSNTypeID = Convert.ToInt32(DDLSSNQuestion.SelectedValue);
                            ClsCase.SSN = txt_SSNOther.Text;
                        }
                        else
                            if (RadioButton4.Checked && DDLSSNQuestion.SelectedValue == "6")
                            {
                                ClsCase.SSNTypeID = Convert.ToInt32(DDLSSNQuestion.SelectedValue);
                                ClsCase.SSNOtherQuestion = txt_SSNOther.Text;
                            }
                    //Farrukh 11180 06/26/2013 Update Immigration Information Regardless of Language
                    if (rbtn_interestedinworkvisayes.Checked || rbtn_interestedinworkvisano.Checked)
                    {
                        bool isImmigration = false;

                        if (rbtn_interestedinworkvisayes.Checked)
                            isImmigration = true;
                        else if (rbtn_interestedinworkvisano.Checked)
                            isImmigration = false;

                        ClsCase.InsertImmigrationComments(ClsCase.TicketID, txtImmigrationComments.Text, isImmigration);
                        txtImmigrationComments.Text = "";
                    }
                    UpdateCase();                    

                    //// Insert Note to Case History///
                    //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                    if ((td_callback_days.Visible || td_ccallback_days.Visible || td_vcallback_days.Visible)
                        && (rbtn_FollowUp_Yes.Checked || rbtn_cFollowUp_Yes.Checked || rbtn_vFollowUp_Yes.Checked)
                        && (!notecallback)
                        )
                    {
                        switch (mvFeeQuestion.ActiveViewIndex)
                        {
                            //Criminal 
                            case 0: ClsCase.InsertNoteToCaseHistory(cal_cCallBack.SelectedDate); break;
                            //Civil
                            case 1: ClsCase.InsertNoteToCaseHistory(cal_vCallBack.SelectedDate); break;
                            //Traffic
                            default: ClsCase.InsertNoteToCaseHistory(cal_CallBack.SelectedDate); break;
                        }
                        notecallback = true;
                    }                    
                    //Ozair 5381 01/16/2009 Handling for event bubling code refactored
                    //Waqas 6895 11/02/2009 post back just for new case
                    if (Request.QueryString.ToString().IndexOf("newt") >= 0 && Check)
                        Response.Redirect("violationfeeold.aspx?search=0&caseNumber=" + ClsCase.TicketID, false);
                }
                catch (Exception ex)
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = ex.Message;
                    // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                    if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                        tr_msgsForCIDandCuase.Visible = true;
                   // tr_msgsForCIDandCuase.Visible=true;
                    else
                        tr_msgsForCIDandCuase.Visible = false;
                    //tr_msgsForCIDandCuase.Visible=false;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
                // Agha Usman - 4347 - Rebind All flags
                //BindFlags();
                BindFlagsNew();
                //}
                //else
                //{
                //    DisplayInfo();
                // }

                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible = true;
               // tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible = false;
               // tr_msgsForCIDandCuase.Visible=false;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible = true;
               // tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible = false;
                //   tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void btnNext_Click(object sender, System.EventArgs e)
        {
            //ozair 4463 01/29/2009 removed extra try catch
            try
            {
                lbl_Message.Text = "";

                if (lblLockFlag.Text == "False" && lbl_ActiveFlag.Text == "1")
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('Please ReCalculate Price.')</script>");

                //nasir 5381 01/15/2009 CHecking is comment update from other side
                bool Check = ClsCase.IsLastGeneralCommentsDateUpdated(hf_LastGerenalcommentsUpdatedate.Value);
                if (!Check)
                {
                    Modal_Message.Show();
                }
                else
                {
                    WCC_GeneralComments.AddComments();
                }

                UpdateCase();
                //// Insert Note to Case History///
                //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                if (
                    (td_callback_days.Visible || td_ccallback_days.Visible || td_vcallback_days.Visible)
                    && (rbtn_FollowUp_Yes.Checked || rbtn_cFollowUp_Yes.Checked || rbtn_vFollowUp_Yes.Checked)
                   )
                {
                    switch (mvFeeQuestion.ActiveViewIndex)
                    {
                        //Criminal 
                        case 0: ClsCase.InsertNoteToCaseHistory(cal_cCallBack.SelectedDate); break;
                        //Civil
                        case 1: ClsCase.InsertNoteToCaseHistory(cal_vCallBack.SelectedDate); break;
                        //Traffic
                        default: ClsCase.InsertNoteToCaseHistory(cal_CallBack.SelectedDate); break;
                    }
                }
                //nasir 5381 01/15/2009 Check if comments already updated show popup else redirect page
                if (Check)
                {
                    Response.Redirect("GeneralInfo.aspx?casenumber=" + ViewState["vTicketID"].ToString() + "&search=" + Convert.ToString(ViewState["vSearch"]), false);
                }

                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
            }
            //Nasir 4463 01/28/2009  Log Exception
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void btn_Lock_Click(object sender, System.EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                bool bPriceChanged = false;
                lblpriechange.Text = "";
                lblpriechange.Visible = false;
                lbl_Message.Visible = false;

                SetValues();
                UpdateCase();
                foreach (DataGridItem itemX in dgViolationInfo.Items)
                {
                    if (((Label)itemX.FindControl("lblvnpk")).Text == "0")
                        pPlan = true;
                }

                if (pPlan == true)
                {
                    lbl_Message.Text = "Please First Update the Underlying Violation to calculate price.";
                    lbl_Message.Visible = true;
                }
                else
                {
                    //Calculates the Price on the basis of Violations and locks the record means lock flag=1
                    //Added By Zeeshan Ahmed to Implement Late Fee and Over Speeding Fee
                    if (rbtn_lateyes.Checked || rbtn_clateyes.Checked || rbtn_vlateyes.Checked)
                    {
                        ClsCase.LateFee = 1;
                    }
                    else
                    {
                        ClsCase.LateFee = 0;
                    }

                    // tahir 4786 10/08/2008 
                    ClsCase.HasPaymentPlan = rbtn_vplanyes.Checked;

                    // Rab Nawaz Khan 10330 07/16/2012 DayOfArr Flag has been added in the method parameter. . . 
                    bPriceChanged = Convert.ToBoolean(ClsCase.CalculateFee(ClsCase.TicketID, true, dgViolationInfo.Items.Count, AFlag, CFlag, Speeding, Conti, IAdj, FAdj, isRedirectedFromPC, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), false, ClsCase.HasPaymentPlan, ClsCase.DayOfArrFlag));
                    if (bPriceChanged)
                    {
                        ////////////////// Fee Calculation /////////////////
                        DataSet ds_Case = ClsCase.GetCase(ClsCase.TicketID);
                        double Fee = Convert.ToDouble(ds_Case.Tables[0].Rows[0]["calculatedtotalfee"]);
                        double Paid = Convert.ToDouble(ds_Case.Tables[0].Rows[0]["Paid"]);
                        double Owes = Fee - Paid;
                        lbl_CalculatedFee.Text = Fee.ToString("$###0;($###0)");
                        lbl_Paid.Text = Paid.ToString("$###0.##;($###0.##)");
                        lbl_EstimatedOwes.Text = Owes.ToString("$###0.##;($###0.##)");
                        lbl_actualowes.Text = (Convert.ToDouble(ds_Case.Tables[0].Rows[0]["totalfeecharged"]) - Paid).ToString("$###0.##;($###0.##)");
                        //////////////////////////////////////////////
                        this.lblpriechange.Text = "Price has been changed during LOCK process.";
                        this.lbl_Message.Text = "Price has been changed during LOCK process.";
                        lblpriechange.Visible = true;
                        lbl_Message.Visible = true;
                        this.UpdatePanel3.Visible = true;
                        DisplayInfo();

                    }
                    else
                    {
                        //DisplayInfo();//Line below coment by Azee for Modification send on 13 march 2007..                        
                        //Response.Redirect("GeneralInfo.aspx?casenumber=" + ViewState["vTicketID"].ToString() + "&search=" + ViewState["vSearch"].ToString(), false);
                        DataSet ds_Case = ClsCase.GetCase(ClsCase.TicketID);
                        double Fee = Convert.ToDouble(ds_Case.Tables[0].Rows[0]["calculatedtotalfee"]);
                        double Paid = Convert.ToDouble(ds_Case.Tables[0].Rows[0]["Paid"]);
                        double Owes = Fee - Paid;
                        lbl_CalculatedFee.Text = Fee.ToString("$###0;($###0)");
                        lbl_Paid.Text = Paid.ToString("$###0.##;($###0.##)");
                        lbl_EstimatedOwes.Text = Owes.ToString("$###0.##;($###0.##)");
                        lbl_actualowes.Text = (Convert.ToDouble(ds_Case.Tables[0].Rows[0]["totalfeecharged"]) - Paid).ToString("$###0.##;($###0.##)");

                    }
                }
                CheckReadNotes(Convert.ToInt32(ViewState["vTicketID"]));

                //Zeeshan Ahmed 3948 05/14/2008
                //If Price Locked Successfully And No Error Occur Then Redirect To Prevent Event Bubling
                if (lbl_Message.Text == "")
                    Response.Redirect(Request.Url.ToString(), false);

                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                    if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                        tr_msgsForCIDandCuase.Visible=true;
                    else
                        tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void dgViolationInfo_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            try
            {
                //Waqas Javed 4841 12/20/08 Sorting feature
                if (e.Item.ItemType == ListItemType.Header)
                {
                    for (int i = 0; i < e.Item.Cells.Count; i++)
                    {
                        //Waqas Javed 4841 12/22/08 Sorting feature Revised
                        if (e.Item.Cells[i].Controls.Count != 0)
                            ((LinkButton)e.Item.Cells[i].Controls[0]).Attributes.Add("onclick", "ShowSortProgress();");
                    }
                }

                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv != null)
                {
                    //added for Criminal courts
                    if (((HiddenField)e.Item.FindControl("hf_iscriminalcourt")).Value == "1")
                    {
                        dgViolationInfo.Columns[4].Visible = true;
                        // Rab Nawaz Khan 10349 08/30/2013 Jail Time, Jail Type and Max Fine Columns added for the Criminal courts. . .  
                        if (Convert.ToBoolean(((HiddenField)e.Item.FindControl("hf_IsNewClient")).Value))
                        {
                            if (dgViolationInfo.Columns[16].HeaderText.Equals("Jail Time"))
                                dgViolationInfo.Columns[16].Visible = true;
                            if (dgViolationInfo.Columns[17].HeaderText.Equals("Jail Type"))
                                dgViolationInfo.Columns[17].Visible = true;
                            if (dgViolationInfo.Columns[18].HeaderText.Equals("Max Fine"))
                                dgViolationInfo.Columns[18].Visible = true;
                            // Rab Nawaz Khan 10349 09/04/2013 Fixing the length of violation grid columns for new columns. . . 
                            dgViolationInfo.Columns[0].HeaderStyle.Width = 130; // Cause Number width
                            dgViolationInfo.Columns[0].ItemStyle.Width = 130;
                            dgViolationInfo.Columns[1].HeaderStyle.Width = 130; // Cause Number width
                            dgViolationInfo.Columns[1].ItemStyle.Width = 130;
                            dgViolationInfo.Columns[3].HeaderStyle.Width = 160; // Violation Description
                            dgViolationInfo.Columns[3].ItemStyle.Width = 160;
                            dgViolationInfo.Columns[5].HeaderStyle.Width = 30;  // Court Location
                            dgViolationInfo.Columns[5].ItemStyle.Width = 30;
                            dgViolationInfo.Columns[6].HeaderStyle.Width = 60;  // Status
                            dgViolationInfo.Columns[6].ItemStyle.Width = 60;
                            dgViolationInfo.Columns[7].HeaderStyle.Width = 80;
                            dgViolationInfo.Columns[7].ItemStyle.Width = 80;
                            dgViolationInfo.Columns[8].HeaderStyle.Width = 80;
                            dgViolationInfo.Columns[8].ItemStyle.Width = 80;
                            dgViolationInfo.Columns[9].HeaderStyle.Width = 70;
                            dgViolationInfo.Columns[9].ItemStyle.Width = 70;
                            dgViolationInfo.Columns[15].HeaderStyle.Width = 90;
                            dgViolationInfo.Columns[15].ItemStyle.Width = 90;
                            dgViolationInfo.Columns[16].HeaderStyle.Width = 90;
                            dgViolationInfo.Columns[16].ItemStyle.Width = 90;
                            dgViolationInfo.Columns[17].HeaderStyle.Width = 70;
                            dgViolationInfo.Columns[17].ItemStyle.Width = 70;
                            dgViolationInfo.Columns[18].HeaderStyle.Width = 90;
                            dgViolationInfo.Columns[18].ItemStyle.Width = 90;

                            // Rab Nawaz Khan 10349 09/04/2013 If Fine Amount not found then do not display any thing. . .
                            Label lblMaxFine = (Label)e.Item.FindControl("lbl_maxFine");
                            if (Double.Parse(lblMaxFine.Text, NumberStyles.Currency) == 0)
                                lblMaxFine.Text = "";
                        }
                    }
                    hf_lastrow.Value = e.Item.ClientID;

                    HiddenField hf = (HiddenField)e.Item.FindControl("hf_Discrepency");
                    HiddenField arragency = (HiddenField)e.Item.FindControl("hf_ArrestingAgency");

                    if (hf.Value == "1")
                    {
                        Label Verified = (Label)e.Item.FindControl("lblVerified");
                        Label Auto = (Label)e.Item.FindControl("lblAuto");
                        Label Scan = (Label)e.Item.FindControl("lblScan");

                        System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img_status");
                        img.ImageUrl = "../Images/cross.gif";
                        HtmlControl ctrl = (HtmlControl)e.Item.FindControl("div_status");
                        string courtdateauto = "";
                        string courtdatemain = "";
                        string courtdatescan = "";
                        //Ozair 4463 01/29/2009 Code refactored used .lenght instead of .equals
                        //Nasir 4463 01/28/2009 cheking for empty string first before using substring.
                        if (Auto.Text.Length > 0)
                        {
                            courtdateauto = Auto.Text.Substring(2);
                        }

                        if (Verified.Text.Length > 0)
                        {
                            courtdatemain = Verified.Text.Substring(2);
                        }

                        if (Scan.Text.Length > 0)
                        {
                            courtdatescan = Scan.Text.Substring(2);
                        }
                        //end ozair 4463

                        //Kazim 3828 4/24/2008 Add Arresting Agency in tooltip 
                        ctrl.Attributes.Add("Title", "hideselects=[on] offsetx=[-200] offsety=[0] singleclickstop=[on] requireclick=[off] header=[] body=[<table 'width:300px' border='0' cellspacing='0'  class='label' style='BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none'><tr><td class='Label'>Auto:</td><td class='Label'>" + courtdateauto + " </td></tr><tr><td  class='Label'>Verified:</td><td  class='Label' >" + courtdatemain + "</td></tr><tr><td class='Label'>Scan:</td><td  class='Label'>" + courtdatescan + "</td></tr><tr><td class='Label'>Arrested By:</td><td  class='Label'>" + arragency.Value + "</td></tr></table>]");

                    }
                    else
                    {
                        ((System.Web.UI.WebControls.Image)e.Item.FindControl("img_status")).ToolTip = "Arresting Agency: " + arragency.Value;

                    }


                    LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkBtn_CaseNo");
                    LinkButton lnkBtn_cause = (LinkButton)e.Item.FindControl("lnkBtn_CauseNo");
                    //Sabir Khan 7181 12/24/2009 Hyperlink cause number for pasadena municipal court....
                    HiddenField hf_courtID = (HiddenField)e.Item.FindControl("hf_courtid");
                    Label lbl_ticketNo = (Label)e.Item.FindControl("lbl_TicketNo");
                    dgViolationInfo.Columns[1].Visible = true;
                    dgViolationInfo.Columns[0].Visible = false;
                    lnkBtn.Visible = false;
                    lbl_ticketNo.Visible = true;
                    //Asad Ali 8153 09/20/2010 find violation desc
                    Label violationDescription = e.Item.FindControl("Label4") as Label;

                    //Farrukh 10763 06/13/2013 Pre-selection removed for Criminal cases having DWI violation
                    if (Convert.ToString(ViewState["CaseTypeID"]) != "2")
                    {
                        if (System.Text.RegularExpressions.Regex.IsMatch(violationDescription.Text, "DWI", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                        {
                            rBtn_ALRHearingRequiredYes.Checked = true;
                        }
                    }

                    if (hf_courtID.Value == "3004")
                    {
                        if (lbl_ticketNo.Text.Length > 2)
                        {
                            if (lbl_ticketNo.Text.Substring(0, 3).ToUpper() == "PMC")
                            {
                                lbl_ticketNo.Text = "N/A";
                            }
                        }
                        lnkBtn.Visible = false;
                    }
                    //Noufil 4215 07/01/2008 Check SOl flag
                    if (Convert.ToString(ViewState["HasSOLFlag"]) == "1")
                    {
                        lnkBtn.Attributes.Add("onclick", "displaySOLmesaage()");
                        lnkBtn_cause.Attributes.Add("onclick", "displaySOLmesaage()");
                    }
                    else
                    {
                        lnkBtn_cause.Attributes.Add("onClick", "showpopup('" + UpdateViolationSRV1.ClientID + "','" + e.Item.ClientID + "',0);return false;");
                        lnkBtn.Attributes.Add("onClick", "showpopup('" + UpdateViolationSRV1.ClientID + "','" + e.Item.ClientID + "',0);return false;");
                    }

                    Label date = (Label)e.Item.FindControl("lbl_courtdate");
                    Label status = (Label)e.Item.FindControl("lbl_status");
                    //added by ozair for task 1741
                    LinkButton lnkdate = (LinkButton)e.Item.FindControl("lnkbtn_courtdate");
                    lnkdate.OnClientClick = "return openScannedDocket('" + date.Text + "')";
                    int future = Convert.ToDateTime(date.Text).CompareTo(DateTime.Today);
                    if (lbl_ActiveFlag.Text == "1" && future <= 0)
                    {
                        date.Visible = false;
                        lnkdate.Visible = true;
                    }
                    else
                    {
                        date.Visible = true;
                        lnkdate.Visible = false;
                    }
                    //end task 1741
                    if (date.Text == "1/1/1900")
                    {
                        date.Text = "";
                        ((Label)e.Item.FindControl("lbl_time")).Text = "";

                    }

                    if (status.Text == "")
                    {
                        status.Text = "N/A";
                    }

                    Label desc = (Label)e.Item.FindControl("Label4");

                    if (desc.Text.Length > 20)
                    {
                        desc.ToolTip = desc.Text;
                        desc.Text = desc.Text.Substring(0, 20).ToLowerInvariant() + "...";
                    }

                    //Added By Zeeshan Ahmed
                    //string refcasenumber = ((LinkButton)e.Item.FindControl("lnkBtn_CaseNo")).Text;
                    //string cdi = ((HiddenField)e.Item.FindControl("hf_cdi")).Value;
                    //((ImageButton)e.Item.FindControl("img_casesearch")).Attributes.Add("onClick", "openCaseNoSearchPopup('" + cdi + "','" + refcasenumber + "');return false;");

                    //Added by Ozair for Task 2515 on 01/02/2008
                    string courtid = ((HiddenField)e.Item.FindControl("hf_courtid")).Value;
                    ViewState["CourtID"] = courtid.Trim().ToString();
                    //Kazim 3533 4/1/2008 Use hiddenfields to pass these values on courtdetail page.

                    HiddenField hfCourtAddress = (HiddenField)e.Item.FindControl("hfCourtAddress");
                    HiddenField hfClientAddress = (HiddenField)e.Item.FindControl("hfClientAddress");
                    HiddenField hfCriminalCourt = (HiddenField)e.Item.FindControl("hf_iscriminalcourt");
                    if (courtid != "3001" && courtid != "3002" && courtid != "3003" && hfCriminalCourt.Value != "1" && Convert.ToString(ViewState["CaseTypeID"]) != "3")
                    {
                        dgViolationInfo.Columns[14].Visible = true;
                    }
                    // tahir 4072 05/20/2008
                    // fixed the bug related to special promotional pricing.
                    if (courtid == "3001" || courtid == "3002" || courtid == "3003")
                    {
                        if (lbl_ActiveFlag.Text == "0" && hf_isZip7707.Value == "1")
                        {
                            td_SpProm.Style["display"] = "block";
                        }
                    }

                    //Kazim 4079 5/20/2008 pass client address and court address to display google map.
                    //Kazim 4071 05/21/2008 Remove court and client addresses.
                    ((LinkButton)(e.Item.FindControl("lnkBtn_CrtLoc"))).Attributes.Add("OnClick", "ShowCourtDetailPopUp('" + courtid + "','" + ((HiddenField)e.Item.FindControl("hf_ticketid")).Value + "')");


                    //Zeeshan Ahmed 3724 05/14/2008 If Arrest Date is 1/1/1900 than display blank 
                    Label ArrestDate = (Label)e.Item.FindControl("lblArrestDate");

                    if (ArrestDate.Text == "1/1/1900")
                        ArrestDate.Text = "";
                }


            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Added By Zeeshan Ahmed To Implement Violation Updates
        protected void btn_Calculate_Click1(object sender, EventArgs e)
        {
            try
            {
                CalculateFee();
                //lblpriechange.Text = "";
                //lblpriechange.Visible = false;

                //lbl_Message.Text = "";
                //SetValues();
                //ViewState["vFromCal"] = "true";
                //UpdateCase();

                //foreach (DataGridItem itemX in dgViolationInfo.Items)
                //{
                //    if (((Label)itemX.FindControl("lblvnpk")).Text == "0")
                //        pPlan = true;
                //}

                ////DisplayInfo(); Commented vasue it is not required here.
                //if (pPlan == true)
                //{
                //    lbl_Message.Text = "Please First Update the Underlying Violation to calculate price.";
                //    lbl_Message.Visible = true;
                //}
                //else
                //{
                //    lbl_Message.Text = "";
                //    // IF SIGNING UP CLIENT THROUG CLIENT LOOKUP THEN PASS INITAIL ADJUSTMENT
                //    // TO MAKE THE QUOTE AMOUNT SIMILAR TO THE ESTIMATED FEE ON CLIENT LOOKUP REPORT....
                //    if (ClsSession.GetCookie("sPCReport", this.Request) == "1")
                //    {

                //        IAdj = dgViolationInfo.Items.Count * 50;
                //    }
                //    //Calculates the Price on the basis of Violations
                //    if (rbtn_lateyes.Checked || rbtn_clateyes.Checked || rbtn_vlateyes.Checked)
                //        ClsCase.LateFee = 1;
                //    else
                //        ClsCase.LateFee = 0;

                //    ClsCase.HasPaymentPlan = rbtn_vplanyes.Checked;

                //    // Added by Farhan
                //    // Getting count of violations that are of type 'N/H'
                //    int NHcount = 0;

                //    for (int i = 0; i < dgViolationInfo.Items.Count; i++)
                //    {
                //        HiddenField hf_Vl = (HiddenField)dgViolationInfo.Items[i].FindControl("hf_courtviolationstatusid"); //(HiddenField)gvr.FindControl("hf_courtviolationstatusid");
                //        if (hf_Vl != null)
                //            if (hf_Vl.Value == "239")
                //            {
                //                NHcount++;
                //            }
                //    }

                //    ClsCase.CalculateFee(ClsCase.TicketID, false, dgViolationInfo.Items.Count - NHcount, AFlag, CFlag, Speeding, Conti, IAdj, FAdj, isRedirectedFromPC, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), false, ClsCase.HasPaymentPlan);
                //    DisplayInfo();

                //}

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message.ToString();
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                if (ex.Source != "lntechNew")
                {
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }

            }
        }

        protected void lnkbtn_InsertViolation_Click(object sender, EventArgs e)
        {
            // addnewticket();
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            try
            {

                string tel1 = ptel11.Text + ptel12.Text + ptel13.Text + ptel14.Text;
                string tel2 = ptel21.Text + ptel22.Text + ptel23.Text + ptel24.Text;
                string tel3 = ptel31.Text + ptel32.Text + ptel33.Text + ptel34.Text;

                string pno = "", secno1 = "", secno2 = "", type1 = "0", type2 = "0", type3 = "0";

                try
                {
                    if (rb1.Checked)
                    {
                        pno = tel1;
                        type1 = ddl_ptype1.SelectedValue;
                        ddl_ContactType.SelectedValue = ddl_ptype1.SelectedValue;
                        txt_CC11.Text = ptel11.Text;
                        txt_CC12.Text = ptel12.Text;
                        txt_CC13.Text = ptel13.Text;
                        txt_CC14.Text = ptel14.Text;

                        secno1 = tel2;
                        type2 = ddl_ptype2.SelectedValue;
                        secno2 = tel3;
                        type3 = ddl_ptype3.SelectedValue;

                    }
                    else if (rb2.Checked)
                    {
                        pno = tel2;
                        type1 = ddl_ptype2.SelectedValue;
                        ddl_ContactType.SelectedValue = ddl_ptype2.SelectedValue;
                        txt_CC11.Text = ptel21.Text;
                        txt_CC12.Text = ptel22.Text;
                        txt_CC13.Text = ptel23.Text;
                        txt_CC14.Text = ptel24.Text;

                        secno1 = tel1;
                        type2 = ddl_ptype1.SelectedValue;
                        secno2 = tel3;
                        type3 = ddl_ptype3.SelectedValue;
                    }
                    else
                    {
                        pno = tel3;
                        type1 = ddl_ptype3.SelectedValue;
                        ddl_ContactType.SelectedValue = ddl_ptype3.SelectedValue;

                        txt_CC11.Text = ptel31.Text;
                        txt_CC12.Text = ptel32.Text;
                        txt_CC13.Text = ptel33.Text;
                        txt_CC14.Text = ptel34.Text;


                        secno1 = tel1;
                        type2 = ddl_ptype1.SelectedValue;
                        secno2 = tel2;
                        type3 = ddl_ptype1.SelectedValue;
                    }
                }
                ////Nasir 4463 01/28/2009  Log Execption
                catch (Exception ex)
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = ex.Message.ToString();
                    // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                    if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                        tr_msgsForCIDandCuase.Visible=true;
                    else
                        tr_msgsForCIDandCuase.Visible=false;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }

                try
                {
                    string[] keys = { "@TicketID_Pk", "@Contact1", "@Contact2", "@Contact3", "@ContactType1", "@ContactType2", "@ContactType3" };
                    object[] values = { Convert.ToInt32(ViewState["vTicketID"]), pno, secno1, secno2, type1, type2, type3 };
                    clsdb.ExecuteSP("USP_HTS_ADD_TelNo", keys, values);

                    tbl_primaryno.Visible = true;
                    lnkbtn_addphoneno.Visible = false;
                    lbl_Message.Text = "Phone No's Updated";

                    // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                    if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                        tr_msgsForCIDandCuase.Visible=true;
                    else
                        tr_msgsForCIDandCuase.Visible=false;

                }
                catch
                {
                    lbl_Message.Text = "Phone No's Not Added";
                    tbl_primaryno.Visible = false;
                    lnkbtn_addphoneno.Visible = true;
                    // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                    if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                        tr_msgsForCIDandCuase.Visible=true;
                    else
                        tr_msgsForCIDandCuase.Visible=false;
                }

            }
            //Nasir 4463 01/28/2009 Log Execption
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message.ToString();
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_Speeding_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Update_Click1(object sender, EventArgs e)
        {

        }

        protected void ibtn_SPN_Search_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                clsCriminalCase CriminalCase = new clsCriminalCase();
                CriminalCase.SessionID = this.Session.SessionID;
                CriminalCase.TicketID = Convert.ToInt32(ViewState["vTicketID"]);
                CriminalCase.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));

                if (CriminalCase.SaveCriminalCasePDF())
                {
                    Response.Write(CriminalCase.PDFCreatedAlert());
                }
                else
                {
                    Response.Write(CriminalCase.PDFNotCreatedAlert());
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);


            }
        }

        /// <summary>
        /// CheckBox Delete Flag Event (Fahad- 12/24/2007)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkbtn_read_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbtn_read.Checked == true)
            {
                notes.DeleteFlag(Convert.ToInt32(ViewState["vTicketID"]), Convert.ToInt32(ViewState["empid"]));
            }
            CheckReadNotes(Convert.ToInt32(ViewState["vTicketID"]));
            lbl_readnotes.Visible = true;
            lbl_readnotes.Text = "Read Notes Has Been Removed";
            read.Style[HtmlTextWriterStyle.Display] = "none";
        }

        protected void btnfineamount_Click(object sender, EventArgs e)
        {
            try
            {
                //Zeeshan Ahmed 3535 04/15/2008
                MPEViolationAmount.Show();
                lblViolationNotFound.Visible = false;
                btnok.Visible = false;
                string ErrorMessage = "";

                if (ViewState["vTicketID"] != null)
                {
                    DataTable dtViolationAmount = general.GetViolationAmount(hlnk_MidNo.Text, out ErrorMessage);

                    btnViolationAmount.Visible = true;
                    gvViolationAmount.Visible = true;


                    if (dtViolationAmount.Rows.Count > 0)
                    {
                        DataSet CaseViolations = ClsCaseDetail.GetCaseDetail(Convert.ToInt32(ViewState["vTicketID"]));

                        if (CaseViolations.Tables.Count > 0 && CaseViolations.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtViolations = GetDistinctRecords(dtViolationAmount, CaseViolations.Tables[0]);

                            if (dtViolations.Rows.Count > 0)
                            {
                                gvViolationAmount.DataSource = dtViolations;
                                gvViolationAmount.DataBind();
                                return;
                            }
                        }
                        else
                        {
                            gvViolationAmount.DataSource = dtViolationAmount;
                            gvViolationAmount.DataBind();
                            return;
                        }
                    }
                }

                btnViolationAmount.Visible = false;
                gvViolationAmount.Visible = false;

                //Display WebSite Error Message
                if (ErrorMessage != "") lblViolationNotFound.Text = ErrorMessage;

                lblViolationNotFound.Visible = true;
                btnok.Visible = true;
            }
            catch (Exception ex)
            {
                lblViolationNotFound.Visible = true;
                btnok.Visible = true;
                btnViolationAmount.Visible = false;
                gvViolationAmount.Visible = false;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnViolationAmount_Click1(object sender, EventArgs e)
        {
            try
            {
                //Zeeshan Ahmed 3535 04/15/2008
                //Update Violation Fine Amount
                foreach (GridViewRow dr in gvViolationAmount.Rows)
                {
                    if (((CheckBox)dr.FindControl("cbcausenumber")).Checked)
                        ClsCaseDetail.UpdateViolationFineAmount(Convert.ToInt32(ViewState["vTicketID"]), dr.Cells[0].Text, dr.Cells[1].Text);
                }
                DisplayInfo();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = "Violation amount not updated.";
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                clsLogger.ErrorLog(ex);
            }
        }

        // tahir 4841 09/23/2008 added sorting functionality..
        protected void dgViolationInfo_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            //4841 12/20/08 Sorting Feature on Matter Page
            SetAcsDesc(e.SortExpression);
            SortDataGrid(e.SortExpression);

            tblSortProgress.Style.Add("Display", "none");
            dgViolationInfo.Style.Add("Display", "block");
        }

        //Waqas 5771 04/14/2009
        protected void lnkContactID_Click(object sender, EventArgs e)
        {
            if (hdnContactID.Value != null && hdnContactID.Value != string.Empty)
            {
                ContactInfo1.TicketID = ClsCase.TicketID;
                ContactInfo1.BindContactInfo(hdnContactID.Value, MPContactID.ClientID, 1, int.Parse(lbl_ActiveFlag.Text));
                MPContactID.Show();
            }
            else
            {
                string LastName = txt_LastName.Text;
                string FirstName = txt_FirstName.Text;
                string DOB = txt_mm.Text + "/" + txt_dd.Text + "/" + txt_yy.Text;
                ContactLookUp.TicketID = ClsCase.TicketID;
                ContactLookUp.BindContactLookUpGrid(LastName, FirstName, DOB, MPContactLookUp.ClientID);
                MPContactLookUp.Show();
            }

        }

        /// <summary>
        /// Nasir 5381 01/20/2009 handle event bubling
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnokay_Click(object sender, EventArgs e)
        {
            Modal_Message.Hide();
            //Waqas 6895 11/02/2009 do not post back
            //Response.Redirect("violationfeeold.aspx?search=0&caseNumber=" + ClsCase.TicketID, false);
        }

        /// <summary>
        /// Waqas 5771 04/15/2009
        /// To Update IsContactIDConfirmed Flag of the case.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCIDCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                clsContact cContact = new clsContact();
                int Empid = 0;
                int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid);
                cContact.UpdateCIDConfirmationFlag(ClsCase.TicketID, 1, Empid);
                DisplayInfo();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Waqas 5771 04/15/2009
        /// To Update IsContactIDConfirmed Flag of the case.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCIDInCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                clsContact cContact = new clsContact();
                int Empid = 0;
                int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out Empid);
                cContact.UpdateCIDConfirmationFlag(ClsCase.TicketID, 0, Empid);
                DisplayInfo();
                lbl_Message.Text = "This client�s CID has been disassociated. Please select a new CID for this client.";
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        //Waqas 5864 06/25/2009 Sending Email
        /// <summary>
        /// This method is used to send email to attorneys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {

                DataSet ds_CaseDetail = ClsCaseDetail.GetCaseDetail(ClsCase.TicketID);

                NewClsPayments ClsPayments = new NewClsPayments();
                ClsPayments.GetPaymentInfo(ClsCase.TicketID);
                string owes = ClsPayments.Owes.ToString();
                string date = ClsPayments.GetMaxSchedulePaymentDate(ClsCase.TicketID);
                string PaymentMessage = "";
                if (owes == "0")
                {
                    PaymentMessage = "Paid in full.";
                }
                else
                {
                    if (date == "" || Convert.ToDateTime(date).ToString("MM/dd/yyyy").Equals("01/01/1900"))
                    {
                        //Yasir Kamal 7140 12/15/2009 do not display amount.
                        PaymentMessage = "Payment scheduled date is not available.";
                    }
                    else
                    {
                        PaymentMessage = Convert.ToDateTime(date).ToString("MM/dd/yyyy");
                    }
                    //7140 end
                }
                // Abbas Shahid Khwaja 9215 04/30/2011 Concatenate the numbers and put in the SendMail fuction from the database.Contact number 2 and Contact number 3 comes from databse.  
                string contactInformation = string.Empty;
                if (ds_CaseDetail.Tables[0].Rows[0]["Contact1"].ToString() != "")
                {
                    // Rab Nawaz Khan 9215 08/15/2011 Formatted the contact numbers for E-Mail
                    string tempContactInfo = "";
                    tempContactInfo = ds_CaseDetail.Tables[0].Rows[0]["Contact1"].ToString();
                    tempContactInfo = tempContactInfo.Insert(3, "-");
                    tempContactInfo = tempContactInfo.Insert(7, "-");
                    if (tempContactInfo.Length > 12)
                    {
                        tempContactInfo = tempContactInfo.Insert(12, "x");

                    }
                    contactInformation = tempContactInfo;
                    // End 9215
                }
                if (ds_CaseDetail.Tables[0].Rows[0]["Contact2"].ToString() != "")
                {
                    // Rab Nawaz Khan 9215 08/15/2011 Formatted the contact numbers for E-Mail
                    string tempContactInfo = "";
                    tempContactInfo = ds_CaseDetail.Tables[0].Rows[0]["Contact2"].ToString();
                    tempContactInfo = tempContactInfo.Insert(3, "-");
                    tempContactInfo = tempContactInfo.Insert(7, "-");
                    if (tempContactInfo.Length > 12)
                    {
                        tempContactInfo = tempContactInfo.Insert(12, "x");

                    }
                    contactInformation += ", " + tempContactInfo;
                    // End 9215
                }
                if (ds_CaseDetail.Tables[0].Rows[0]["Contact3"].ToString() != "")
                {
                    // Rab Nawaz Khan 9215 08/15/2011 Formatted the contact numbers for E-Mail
                    string tempContactInfo = "";
                    tempContactInfo = ds_CaseDetail.Tables[0].Rows[0]["Contact3"].ToString();
                    tempContactInfo = tempContactInfo.Insert(3, "-");
                    tempContactInfo = tempContactInfo.Insert(7, "-");
                    if (tempContactInfo.Length > 12)
                    {
                        tempContactInfo = tempContactInfo.Insert(12, "x");
                    }
                    contactInformation += ", " + tempContactInfo;
                    // End 9215
                }

                //Nasir 6619 12/02/2009 set replace hidden field with text box txtAttorneyEmail
                SendMail(hf_EmailAttorneyName.Value, txtAttorneyEmail.Text, lblClientEmailName.Text, hf_AppTicketViolationIDs.Value, ds_CaseDetail.Tables[0], txtPriorAttorney.Text, Server.HtmlEncode(txtCaseSummaryComments.Text), PaymentMessage, contactInformation);

                //Nasir 6619 11/25/2009 set updated email in hidden field from email text box and reset email text box and attorney dropdown                
                txtAttorneyEmail.Text = "";
                drpAttorneysOnEmails.SelectedIndex = 0;

                ClsCase.UpdateEmailSentFlag(ClsCase.TicketID);

                imgShowEmailProgress.Style["display"] = "none";
                pnlEmailCase.Style["display"] = "none";
                DivForEmail.Style["display"] = "none";

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                imgShowEmailProgress.Style["display"] = "none";
                pnlEmailCase.Style["display"] = "none";
                DivForEmail.Style["display"] = "none";
            }
        }

        //// Noufil 6138 08/04/2009 Event added
        //protected void lmgbtn_addlanguage_Click(object sender, ImageClickEventArgs e)
        //{
        //    string languagetoinsert = dd_OtherLanguage.SelectedItem.ToString();
        //    if (dd_OtherLanguage.SelectedItem.ToString().Contains("("))
        //        languagetoinsert = languagetoinsert.Substring(0, languagetoinsert.IndexOf("("));

        //    languages.InsertLanguage(languagetoinsert.Trim().ToUpper());
        //    FillLanguages();
        //    ddl_language.SelectedIndex = ddl_language.Items.IndexOf(new ListItem(languagetoinsert.Trim().ToUpper()));
        //}

        //Yasir Kamal 7140 12/22/2009 display ticketnumber if cause number not available
        private void grdEmailCaseSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label causeNumber = (Label)e.Row.FindControl("lbl_CauseNumber");
                    Label refcaseNumber = (Label)e.Row.FindControl("lbl_refcasenumber");

                    if (causeNumber.Text == "N/A")
                    {
                        causeNumber.Text = refcaseNumber.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        #endregion

        #region Methods

        // Noufil 5884 07/14/2009 Bind Controls value
        private void BindSmsControl()
        {
            DataTable dt = ClsCase.GetNearestFutureCourtDate();
            if (dt.Rows.Count > 0)
            {
                if (ClsCase.GetClientInfo(ClsCase.TicketID))
                {
                    if (ClsCase.ContactType1 == 4)
                        smscontrl.MobilePhone1 = ClsCase.Contact1;
                    if (ClsCase.ContactType2 == 4)
                        smscontrl.MobilePhone2 = ClsCase.Contact2;
                    if (ClsCase.ContactType3 == 4)
                        smscontrl.MobilePhone3 = ClsCase.Contact3;

                    // Noufil 6177 08/17/2009 Text body Change.. 
                    // hf_smsmessage.Value = "Sullo & Sullo Alert\nDear " + lbl_FirstName.Text.Trim() + ",\nYou are set for Crt date: " + Convert.ToString(dt.Rows[0]["CourtDateMain"]) + "\nLoc: " + Convert.ToString(dt.Rows[0]["CourtInfo"]) + "\nRM: " + Convert.ToString(dt.Rows[0]["CourtNumbermain"]) + "\nPlease arrive 15 minutes earlier.";
                    //Ozair 6472 08/27/2009 colon added
                    //Sabir Khan 7149 12/16/2009 RM has been changed into Court Room:...
                    //Muhammad Ali 8370 10/26/2010 change SMS Msg.....
                    hf_smsmessage.Value = "Enation Alert:\r\n" + lbl_FirstName.Text.Trim() +
                                          ", you have court on " +
                                          Convert.ToString(dt.Rows[0]["CourtDateMain"]) + " Court Room " +
                                          Convert.ToString(dt.Rows[0]["CourtNumbermain"]) + " @ " +
                                          Convert.ToString(dt.Rows[0]["CourtInfo"]) + ".";
                    if (hf_smsmessage.Value.Length < 128)
                    {
                        hf_smsmessage.Value += " Please arrive 15 minutes early.";
                    }
                    smscontrl.SmsMessageBody = hf_smsmessage.Value;
                    smscontrl.TicketId = ClsCase.TicketID;
                    smscontrl.EmpID = Convert.ToInt32(ViewState["empid"]);
                    // Noufil 6461 08/26/2009 Swap first name and last name as it was wrong before.
                    smscontrl.ClientFirstName = lbl_FirstName.Text.Trim();
                    smscontrl.ClientLastName = lbl_LastName.Text.Trim();
                    smscontrl.BindData();
                }
            }
            else
                hf_smsmessage.Value = "0";
        }


        //Assigns Values for Insertion or Updattion
        private void AssignValues()
        {
            try
            {
                if (lblisNew.Text == "0")
                    ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketID"]);

                ClsCase.FirstName = txt_FirstName.Text.Trim().ToUpper();
                ClsCase.MiddleName = txt_MiddleName.Text.Trim().ToUpper();
                ClsCase.LastName = txt_LastName.Text.Trim().ToUpper();
                ClsCase.DOB = txt_mm.Text.Trim() + "/" + txt_dd.Text.Trim() + "/" + txt_yy.Text.Trim();
                ClsCase.ContactType1 = Convert.ToInt32(ddl_ContactType.SelectedValue);
                ClsCase.FirmID = Convert.ToInt32(ddl_FirmAbbreviation.SelectedValue);
                // Noufil 6138 08/17/2009 If other language selected then add other language in database.
                ClsCase.LanguageSpeak = (ddl_language.SelectedValue == "0") ? dd_OtherLanguage.SelectedItem.ToString() : ddl_language.SelectedItem.ToString();
                ClsCase.Contact1 = String.Concat(txt_CC11.Text, txt_CC12.Text.TrimStart(), txt_CC13.Text, txt_CC14.Text);
                ClsCase.GeneralComments = Convert.ToString(WCC_GeneralComments.Label_Text);

                //Waqas 6599 09/19/2009 Occupation and Employer
                //Farrukh 9925 11/28/2011 Remove Occupation and Employer
                //ClsCase.Occupation = txtOccupation.Text;
                //ClsCase.Employer = txtEmployer.Text;
                //if (chkIsUnempoyed.Checked == true)
                //{
                //    ClsCase.IsUnemployed = true;
                //}
                //else if (chkIsUnempoyed.Checked == false)
                //{
                //    ClsCase.IsUnemployed = false;
                //}

                if (rBtnCommercialVehicle.Checked == true) { ClsCase.VehicleType = 1; }
                else if (rBtnMotorCycle.Checked == true) { ClsCase.VehicleType = 2; }
                else if (rBtnCar.Checked == true) { ClsCase.VehicleType = 3; }
                else { ClsCase.VehicleType = 0; }


                // SARIM 5522 02/12/2009 
                if (hf_LastGerenalcommentsUpdatedate.Value != "")
                {
                    //nasir 5381 01/15/2009 assign last general comment update date
                    ClsCase.LastGerenalcommentsUpdatedate = Convert.ToDateTime(hf_LastGerenalcommentsUpdatedate.Value);
                }
                else
                {
                    ClsCase.LastGerenalcommentsUpdatedate = DateTime.Now;
                }

                //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                //Set Bond Flag 
                ClsCase.BondFlag = ClsCase.BondsRequiredFlag = (rdbtn_BondYes.Checked || rdbtn_cBondYes.Checked) ? 1 : 0;
                //Set Accident Flag
                ClsCase.AccidentFlag = (rdbtn_AccidentYes.Checked || rdbtn_cAccidentYes.Checked) ? 1 : 0;
                //Set CDL Flag
                ClsCase.CDLFlag = (rdbtn_CDLYes.Checked || rdbtn_cCDLYes.Checked) ? 1 : 0;
                //Set Late Fee Flag
                ClsCase.LateFee = (rbtn_lateyes.Checked || rbtn_clateyes.Checked || rbtn_vlateyes.Checked) ? 1 : 0;
                //Set Case Type 
                // Sabir Khan 4636 08/19/2008 Update Case Type from dropdown
                //ClsCase.CaseType = Convert.ToInt32(ViewState["CaseTypeID"]);
                ClsCase.CaseType = Convert.ToInt32(ddl_CaseType.SelectedValue);

                // Rab Nawaz Khan 10330 06/28/2012 Check added for the DayOfArr. . . 
                if (ClsCase.CaseType == 1)
                {
                    if (hf_ArrDayOf.Value == "1")
                        ClsCase.DayOfArrFlag = true;
                }

                // End 10330

                // tahir 4786 10/08/2008
                ClsCase.HasPaymentPlan = rbtn_vplanyes.Checked ? true : false;

                switch (mvFeeQuestion.ActiveViewIndex)
                {
                    case 0:
                        ClsCase.SpeedingID = Convert.ToInt32(ddl_cSpeeding.SelectedValue);
                        break;
                    case 1:
                        ClsCase.SpeedingID = 0;
                        break;
                    default:
                        ClsCase.SpeedingID = Convert.ToInt32(ddl_Speeding.SelectedValue);
                        break;
                }

                ClsCase.AdSourceID = 0;
                ClsCase.ActiveFlag = Convert.ToInt32(lbl_ActiveFlag.Text.ToString().Trim());
                ClsCase.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                ClsCase.FollowUpYN = 1;

                //Waqas 5864 06/26/2009 ALR Criminal Cases.
                //Waqas 6342 08/25/2009 Changes for criminal and family 
                if (rbtnPriorAttorneyYes.Checked == true)
                {
                    ClsCase.HavePriorAttorney = 1;
                    ClsCase.PriorAttorneyType = drpAtt.Items[drpAtt.SelectedIndex].Text;
                    ClsCase.PriorAttorneyName = txtPriorAttorney.Text;
                }
                else if (rbtnPriorAttorneyYes.Checked == false)
                {
                    ClsCase.HavePriorAttorney = 0;
                    ClsCase.PriorAttorneyType = "";
                    ClsCase.PriorAttorneyName = "";
                }

                ClsCase.CaseSummaryComments = txtCaseSummaryComments.Text.Trim();
                //Asad Ali 8153 09/20/2010 ALRIntoxilyzerTakenFlag and ALRHearingRequired moved in pre hire section
                ClsCase.ALRIntoxilyzerTakenFlag = Convert.ToInt32(drp_IntoxilyzerTaken.SelectedValue);
                //Waqas 6342 08/12/2009 ALR Observing officer
                if (rBtn_ALRHearingRequiredYes.Checked == true)
                {
                    ClsCase.IsALRHearingRequired = true;
                }
                else if (rBtn_ALRHearingRequiredNo.Checked == true)
                {
                    ClsCase.IsALRHearingRequired = false;
                }
                else
                {
                    ClsCase.IsALRHearingRequired = null;
                }

                // Rab Nawaz Khan 11473 10/23/2013 setting the caller Id information. . . 
                //ClsCase.CallerID = txt_callerId.Text.Trim();
                //ClsCase.IsUnknownCallerId = chk_unknown.Checked;

                //Sabir Khan 11509 11/12/2013
                ClsCase.Email = txtEmailAddress.Text.Trim();
                ClsCase.EmailNotAvailable = chkEmailAddress.Checked;



                // Hafiz 10288 07/19/2012 commented the below section
                //if (Convert.ToInt32(lbl_ActiveFlag.Text.ToString().Trim()) == 1)
                //{
                //    //Waqas 6342 08/12/2009 ALR Observing officer
                //    ClsCase.ALROfficerName = txtALROfficerName.Text;
                //    ClsCase.ALROfficeBadgeNo = txtALROfficerBadgeNumber.Text;
                //    ClsCase.ALRPrecinct = txtALRPrecinct.Text;
                //    ClsCase.ALROfficerCity = txtALRCity.Text;
                //    ClsCase.ALROfficerAddress = txtALRAddress.Text;
                //    ClsCase.ALROfficerState = Convert.ToInt32(ddl_ALRState.SelectedValue);
                //    ClsCase.ALROfficerZip = txtALRZip.Text;
                //    ClsCase.ALRContactNumber = String.Concat(txt_Attacc11.Text, txt_Attcc12.Text.TrimStart(), txt_Attcc13.Text, txt_Attcc14.Text);
                //    ClsCase.ALRArrestingAgency = txtALRArrestingAgency.Text;
                //    if (txtALROfficerMileage.Text.Trim() != "")
                //    {
                //        ClsCase.ALROfficerMileage = Convert.ToDecimal(txtALROfficerMileage.Text);
                //    }


                //    if (drp_IntoxilyzerTaken.SelectedValue == "1")
                //    {
                //        //Asad Ali 7991 07/14/2010 assign values on IntoxilyzerTaken instead of  Intox Result pass 
                //        //Yasir Kamal 7150 01/01/2010 ALR BTO modified.
                //        if (rbBTOArresting.Checked)
                //        {
                //            ClsCase.ALRBTOLastName = txtALROfficerName.Text;
                //        }
                //        if (rbBTOObserving.Checked)
                //        {
                //            ClsCase.ALRBTOLastName = txtALROBSOfficerName.Text;
                //        }
                //        if (rblBTOName.Checked)
                //        {
                //            ClsCase.ALRBTOLastName = txtBTOName.Text;
                //        }
                //        //ClsCase.ALRBTOFirstName = txtBTOFirstName.Text;
                //        //ClsCase.ALRBTOLastName = txtBTOLastName.Text;
                //        ClsCase.ALRBTSFirstName = txtBTSFirstName.Text;
                //        ClsCase.ALRBTSLastName = txtBTSLastName.Text;

                //        if (rbtn_IntoxResultPass.Checked == true)
                //        {
                //            ClsCase.ALRIntoxilyzerResult = "pass";
                //        }
                //        else if (rbtn_IntoxResultFail.Checked == true)
                //        {
                //            ClsCase.ALRIntoxilyzerResult = "fail";
                //        }

                //        //Waqas 6342 08/24/2009 New changes for all criminal and family cases
                //        //if (rBtn_BtoBtsSameYes.Checked == true)
                //        //{
                //        //    ClsCase.ALRBTOBTSSameFlag = true;
                //        //    //ClsCase.ALRBTOBTSSameFlag = true;
                //        //}
                //        //else if (rBtn_BtoBtsSameNo.Checked == true)
                //        //{
                //        //    ClsCase.ALRBTOBTSSameFlag = false;
                //        //    //ClsCase.ALRBTOBTSSameFlag = false;
                //        //    ClsCase.ALRBTSFirstName = txtBTSFirstName.Text;
                //        //    ClsCase.ALRBTSLastName = txtBTSLastName.Text;
                //        //}
                //        //else
                //        //{
                //        //    ClsCase.ALRBTOBTSSameFlag = null;
                //        //}
                //    }

                //    //7150 end


                //    ClsCase.ALRHearingRequiredAnswer = txtALRHearingRequiredAnswer.Text.Trim();

                //    if (rBtnArrOffObsOffYes.Checked == true)
                //    {
                //        ClsCase.IsALRArrestingObservingSame = true;
                //    }
                //    else if (rBtnArrOffObsOffNo.Checked == true)
                //    {
                //        ClsCase.IsALRArrestingObservingSame = false;
                //    }
                //    else
                //    {
                //        ClsCase.IsALRArrestingObservingSame = null;
                //    }


                //    ClsCase.ALROBSOfficerName = txtALROBSOfficerName.Text;
                //    ClsCase.ALROBSOfficeBadgeNo = txtALROBSOfficerBadgeNumber.Text;
                //    ClsCase.ALROBSPrecinct = txtALROBSPrecinct.Text;
                //    ClsCase.ALROBSOfficerCity = txtALROBSCity.Text;
                //    ClsCase.ALROBSOfficerAddress = txtALROBSAddress.Text;
                //    ClsCase.ALROBSOfficerState = Convert.ToInt32(ddl_ALROBSState.SelectedValue);
                //    ClsCase.ALROBSOfficerZip = txtALROBSZip.Text;
                //    ClsCase.ALROBSContactNumber = String.Concat(txt_OBSAttacc11.Text, txt_OBSAttcc12.Text.TrimStart(), txt_OBSAttcc13.Text, txt_OBSAttcc14.Text);
                //    ClsCase.ALROBSArrestingAgency = txtALROBSArrestingAgency.Text;

                //    if (txtALROBSOfficerMileage.Text.Trim() != "")
                //    {
                //        ClsCase.ALROBSOfficerMileage = Convert.ToDecimal(txtALROBSOfficerMileage.Text);
                //    }



                //}




            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void CalculateFee()
        {
            lblpriechange.Text = "";
            lblpriechange.Visible = false;

            lbl_Message.Text = "";
            SetValues();
            ViewState["vFromCal"] = "true";
            UpdateCase();
            

            foreach (DataGridItem itemX in dgViolationInfo.Items)
            {
                if (((Label)itemX.FindControl("lblvnpk")).Text == "0")
                    pPlan = true;
            }

            //DisplayInfo(); Commented vasue it is not required here.
            if (pPlan == true)
            {
                lbl_Message.Text = "Please First Update the Underlying Violation to calculate price.";
                lbl_Message.Visible = true;
            }
            else
            {
                lbl_Message.Text = "";
                // IF SIGNING UP CLIENT THROUG CLIENT LOOKUP THEN PASS INITAIL ADJUSTMENT
                // TO MAKE THE QUOTE AMOUNT SIMILAR TO THE ESTIMATED FEE ON CLIENT LOOKUP REPORT....
                if (ClsSession.GetCookie("sPCReport", this.Request) == "1")
                {

                    IAdj = dgViolationInfo.Items.Count * 50;
                }
                //Calculates the Price on the basis of Violations
                if (rbtn_lateyes.Checked || rbtn_clateyes.Checked || rbtn_vlateyes.Checked)
                    ClsCase.LateFee = 1;
                else
                    ClsCase.LateFee = 0;

                ClsCase.HasPaymentPlan = rbtn_vplanyes.Checked;

                // Added by Farhan
                // Getting count of violations that are of type 'N/H'
                int NHcount = 0;

                for (int i = 0; i < dgViolationInfo.Items.Count; i++)
                {
                    HiddenField hf_Vl = (HiddenField)dgViolationInfo.Items[i].FindControl("hf_courtviolationstatusid"); //(HiddenField)gvr.FindControl("hf_courtviolationstatusid");
                    if (hf_Vl != null)
                        if (hf_Vl.Value == "239")
                        {
                            NHcount++;
                        }
                }

                // Rab Nawaz Khan 10330 06/28/2012 Provided the DayOfArr paramter to the method . . . 
                ClsCase.CalculateFee(ClsCase.TicketID, false, dgViolationInfo.Items.Count - NHcount, AFlag, CFlag, Speeding, Conti, IAdj, FAdj, isRedirectedFromPC, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), false, ClsCase.HasPaymentPlan, ClsCase.DayOfArrFlag);
                DisplayInfo();

            }

            // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
            if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                tr_msgsForCIDandCuase.Visible=true;
            else
                tr_msgsForCIDandCuase.Visible=false;
        }
        //Waqas 5771 04/17/2009
        /// <summary>
        /// To assign contact information to update it
        /// </summary>
        private void AssignContactValues()
        {
            try
            {
                if (hdnContactID.Value != null && hdnContactID.Value != string.Empty)
                {
                    ClsContact.ContactID = Convert.ToInt32(hdnContactID.Value);
                    ClsContact.MidNumber = hlnk_MidNo.Text;
                    ClsContact.PhoneTypeID1 = Convert.ToInt32(ddl_ContactType.SelectedValue);
                    // Noufil 6138 08/17/2009 If other language selected then add other language in database.
                    ClsContact.Language = (ddl_language.SelectedValue == "0") ? dd_OtherLanguage.SelectedItem.ToString() : ddl_language.SelectedItem.ToString();
                    //ClsContact.Language = ddl_language.SelectedValue;
                    ClsContact.Phone1 = String.Concat(txt_CC11.Text, txt_CC12.Text.TrimStart(), txt_CC13.Text, txt_CC14.Text);
                    ClsContact.CDLFlagID = (rdbtn_CDLYes.Checked || rdbtn_cCDLYes.Checked) ? 1 : 0;
                    ClsContact.MiddleName = txt_MiddleName.Text.Trim().ToUpper();
                }

            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Rab Nawaz Khan 01/14/2015 Added to reduce the page load time.
        private void BindFlagsNew()
        {
            if (ViewState["vTicketID"] != null && Convert.ToInt32(ViewState["vTicketID"]) > 0)
            {
                ClsFlags clsFlags = new ClsFlags();
                clsFlags.TicketID = Convert.ToInt32(ViewState["vTicketID"]);
                int EmpId = 0;
                int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out EmpId);
                DataTable dtFlags = clsFlags.GetFlages(clsFlags.TicketID);
                bool allowHire = false;
                bool motionHirring = false;
                bool isJuvenile = false;
                bool preTrialDiversion = false;
                string msgNoHire = string.Empty;
                string msgAllowHire = string.Empty;
                lblReturnedCourtNotice.Visible = false;
                tr_lblReturnedCourtNotice.Visible=false;
                lblDoNotBond.Visible = false;
                tr_lblDoNotBond.Visible=false;;
                lblBondForfeiture.Visible = false;
                tr_lblBondForfeiture.Visible=false;
                ViewState["HasSOLFlag"] = false.ToString();
                
                if (dtFlags != null && dtFlags.Rows.Count > 0)
                {
                    foreach (DataRow row in dtFlags.Rows)
                    {
                        if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.WrongNumber))
                        {
                            lblbadnumber.Visible = true;
                            tr_lblbadnumber.Visible=true;
                        }
                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.bademail))
                        {
                            lblbademail.Visible = true;
                            tr_lblbademail.Visible=true;
                        }
                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.ProblemClient_NoHire))
                        {
                            msgNoHire = "This is a " + row["Description"].ToString();
                            lblProblemClient.Visible = true;
                            tr_lblProblemClient.Visible=true;;
                        }
                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.ProblemClient_AllowHire))
                        {
                            msgAllowHire = "This is a " + row["Description"].ToString();
                            allowHire = true;
                        }
                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.ReturnedCourtNotice))
                        {
                            lblReturnedCourtNotice.Visible = true;
                            tr_lblReturnedCourtNotice.Visible=true;
                        }

                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.DoNotBond))
                        {
                            lblDoNotBond.Visible = true;
                            tr_lblDoNotBond.Visible=true;
                        }

                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.BondForfeiture))
                        {
                            lblBondForfeiture.Visible = true;
                            tr_lblBondForfeiture.Visible=true;
                        }

                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.JUVENILE))
                            isJuvenile = true;

                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.Motion_Hiring))
                            motionHirring = true;

                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.Pre_Trial_Diversion))
                            preTrialDiversion = true;

                        else if (Convert.ToInt32(row["FlagID"].ToString()) == Convert.ToInt32(FlagType.SOL))
                            ViewState["HasSOLFlag"] = true.ToString();
                    }
                }
                if (lblProblemClient.Visible)
                    lblProblemClient.Text = msgNoHire;
                else
                {
                    if (allowHire)
                    {
                        lblProblemClient.Text = msgAllowHire;
                        lblProblemClient.Visible = true;
                        tr_lblProblemClient.Visible=true;
                        
                    }
                    else
                    {
                        lblProblemClient.Visible = false;
                        tr_lblProblemClient.Visible=false;
                    }
                }
                
                if (lblReturnedCourtNotice.Visible)
                    lblReturnedCourtNotice.Text = "Client�s court notice was returned.  Please update client�s address and resend court notice.  Please remove the 'Returned Court Notice' flag after client's address is updated and a new court notice is sent.";
                
                if (lblDoNotBond.Visible)
                    lblDoNotBond.Text = "This person is not allowed to post future bonds with us.  Please suggest they hire a bonding company and we can represent them after they are bonded.";

                if (lblBondForfeiture.Visible)
                    lblBondForfeiture.Text = "Bond Forfeiture";

                if (isJuvenile)
                {
                    lbl_Message.Text = "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.";
                    lbl_Message.Visible = isJuvenile;
                }
                if (motionHirring)
                {
                    lbl_Message.Visible = motionHirring;
                    lbl_Message.CssClass = "FlagMessageBold";
                    lbl_Message.Text = lbl_Message.Text + "<br/> The client has a Motion Hiring status. This case can't be reschedule";
                }
                if (preTrialDiversion)
                {
                    lbl_Message.Visible = true;
                    lbl_Message.CssClass = "FlagMessageBold";
                    lbl_Message.Text = "The client has a Pre-Trial Diversion status. This case can't be reschedule";
                }
                //Cheack Active/Inactive Court
                DataTable dt_court = new DataTable();
                dt_court = ClsCourts.IsActiveCourtCase(ClsCase.TicketID);
                if (dt_court.Rows.Count > 0)
                {
                    if (dt_court.Rows[0][0].ToString() == "1")
                    {
                        tr_InfoMessages.Visible=true;
                        lblInActiveCourt.Text = "Inactive Court.";
                        lblInActiveCourt.Visible = true;
                        tr_lblInActiveCourt.Visible=true;
                    }
                }
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                if (lblbadnumber.Visible || lblbademail.Visible || lblProblemClient.Visible || lblHmcLubMesg1.Visible || lblHmcLubMesg2.Visible || lblInActiveCourt.Visible)
                {
                    tr_InfoMessages.Visible=true;
                    if(lblbadnumber.Visible)
                        tr_lblbadnumber.Visible=true;
                    if(lblbademail.Visible)
                        tr_lblbademail.Visible=true;
                    if (lblProblemClient.Visible)
                        tr_lblProblemClient.Visible=true;;
                    if (lblHmcLubMesg1.Visible)
                        tr_lblHmcLubMesg1.Visible=true;
                    if (lblHmcLubMesg2.Visible)
                        tr_lblHmcLubMesg2.Visible=true;
                    if (lblInActiveCourt.Visible)
                        tr_lblInActiveCourt.Visible=true;
                }
                else
                   tr_InfoMessages.Visible=false;
            }
        }
        /*
        private void BindFlags()
        {
            if (ViewState["vTicketID"] != null && Convert.ToInt32(ViewState["vTicketID"]) > 0)
            {
                int EmpId = 0;
                int.TryParse(ClsSession.GetCookie("sEmpID", this.Request), out EmpId);
                ClsFlags clsFlags = new ClsFlags();
                clsSession cSession = new clsSession();
                clsFlags.TicketID = Convert.ToInt32(ViewState["vTicketID"]);
                clsFlags.FlagID = Convert.ToInt32(FlagType.WrongNumber);

                clsFlags.EmpID = EmpId;
                lblbadnumber.Visible = (clsFlags.HasFlag() ? true : false);
                // Agha 4004 Implementing Bad email                    
                clsFlags.FlagID = Convert.ToInt32(FlagType.bademail);
                lblbademail.Visible = (clsFlags.HasFlag() ? true : false);
                 
                // Agha 4347 07/21/2008 Implementing Problem Client
                // tahir 4777 09/10/2008 added Problem client (No hire) logic....
                //clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient);
                //lblProblemClient.Visible = (clsFlags.HasFlag() ? true : false);

                clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_NoHire);
                if (clsFlags.HasFlag())
                {
                    lblProblemClient.Text = "This is a " + clsFlags.Description;
                    lblProblemClient.Visible = true;
                }
                else
                {
                    //ozair 4846 11/14/2008 renamed ProblemClient_Becareful to ProblemClient_AllowHire
                    clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_AllowHire);
                    if (clsFlags.HasFlag())
                    {
                        lblProblemClient.Text = "This is a " + clsFlags.Description;
                        lblProblemClient.Visible = true;
                    }
                    else
                    {
                        lblProblemClient.Visible = false;
                    }
                }
                //Sabir Khan 11509 11/05/2013 New flags added
                clsFlags.FlagID = Convert.ToInt32(FlagType.ReturnedCourtNotice);
                if (clsFlags.HasFlag())
                {
                    lblReturnedCourtNotice.Visible = true;
                    lblReturnedCourtNotice.Text = "Client�s court notice was returned.  Please update client�s address and resend court notice.  Please remove the 'Returned Court Notice' flag after client's address is updated and a new court notice is sent.";
                }
                else
                {
                    lblReturnedCourtNotice.Visible = false;
                }

                clsFlags.FlagID = Convert.ToInt32(FlagType.DoNotBond);
                if (clsFlags.HasFlag())
                {
                    lblDoNotBond.Visible = true;
                    lblDoNotBond.Text = "This person is not allowed to post future bonds with us.  Please suggest they hire a bonding company and we can represent them after they are bonded.";
                }
                else
                {
                    lblDoNotBond.Visible = false;
                }

                clsFlags.FlagID = Convert.ToInt32(FlagType.BondForfeiture);
                if (clsFlags.HasFlag())
                {
                    lblBondForfeiture.Visible = true;
                    lblBondForfeiture.Text = "Bond Forfeiture";
                }
                else
                {
                    lblBondForfeiture.Visible = false;
                }


                // end 4777
                //Faique Ali 11491 10/21/2013 Cheack Active/Inactive Court
                DataTable dt_court = new DataTable();
                dt_court = ClsCourts.IsActiveCourtCase(ClsCase.TicketID);
                if (dt_court.Rows.Count > 0)
                {
                    if (dt_court.Rows[0][0].ToString() == "1")
                    {
                        tr_InfoMessages.Visible=true;
                        lblInActiveCourt.Text = "Inactive Court.";
                        lblInActiveCourt.Visible = true;
                    }
                }
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                //Faique Ali 11491 10/21/2013 add Active/Inactive Court label
                if (lblbadnumber.Visible || lblbademail.Visible || lblProblemClient.Visible || lblHmcLubMesg1.Visible || lblHmcLubMesg2.Visible || lblInActiveCourt.Visible)
                    tr_InfoMessages.Visible=true;
                else
                   tr_InfoMessages.Visible=false;
            }
        }
        */
        private void EnableSPNSearch()
        {
            string SPNUser = this.ClsSession.GetCookie("sIsSPNUser", this.Request);
            if (SPNUser.Trim().ToUpper() == "True".ToUpper())
            {
                DataTable dtSPNCourt = ClsCase.GetCourtForSPN(ClsCase.TicketID);

                if (Convert.ToInt32(dtSPNCourt.Rows[0][0]) > 0)
                {
                    dgViolationInfo.Columns[19].Visible = true;
                    //Kazim 3891 4/29/2008 Display Image both for Clients and Quote Clients 
                    ibtn_SPN_Search.Visible = true;
                }
                else
                {
                    ibtn_SPN_Search.Visible = false;
                    dgViolationInfo.Columns[19].Visible = false;
                }
            }

        }

        //fills court location drop down list
        private void FillCourtLocation(DataTable dtShortCourtNames)
        {
            try
            {
                //DataSet ds_Court = ClsCourts.GetShortCourtName();
                //DataSet ds_ActiveCourts = ClsCourts.GetAllActiveCourtName();
                if (dtShortCourtNames != null && dtShortCourtNames.Rows.Count > 0)
                {
                    ddl_CourtLocation.Items.Clear();
                    // Adding Courts Names in Drop Down List 
                    ddl_CourtLocation.DataSource = dtShortCourtNames;
                    ddl_CourtLocation.DataValueField = "courtid";
                    ddl_CourtLocation.DataTextField = "shortcourtname";
                    ddl_CourtLocation.DataBind();
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills Speeding detail drop down list
        private void FillSpeeding(DataTable dtSpeeding)
        {
            try
            {
                //Zeeshan Ahmed 3979 05/16/2008 Add Speeding List For Criminal Case
                //DataSet ds_Speeding = ClsCase.GetAllSpeeding();
                if (dtSpeeding != null && dtSpeeding.Rows.Count > 0)
                {

                    ddl_Speeding.Items.Clear();
                    ddl_cSpeeding.Items.Clear();

                    ddl_Speeding.DataSource = dtSpeeding;
                    ddl_cSpeeding.DataSource = dtSpeeding;

                    ddl_Speeding.DataBind();
                    ddl_cSpeeding.DataBind();

                    ddl_Speeding.Items.Insert(0, new ListItem("--Choose--", "0"));
                    ddl_cSpeeding.Items.Insert(0, new ListItem("--Choose--", "0"));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills contact type drop down list
        private void FillContactType(DataTable dtContactTypes)
        {
            try
            {
                //DataSet ds_Contact = ClsContactType.GetContactType();
                if(dtContactTypes != null && dtContactTypes.Rows.Count > 0)
                {
                    ddl_ContactType.Items.Clear();

                    ddl_ContactType.DataSource = dtContactTypes;
                    ddl_ContactType.DataTextField = "Description";
                    ddl_ContactType.DataValueField = "ID";
                    ddl_ContactType.DataBind();
                    ddl_ContactType.Items.Insert(0, new ListItem("---Choose---", "0"));

                    ddl_ptype1.DataSource = dtContactTypes;
                    ddl_ptype1.DataTextField = "Description";
                    ddl_ptype1.DataValueField = "ID";
                    ddl_ptype1.DataBind();
                    ddl_ptype1.Items.Insert(0, new ListItem("---Choose---", "0"));

                    ddl_ptype2.DataSource = dtContactTypes;
                    ddl_ptype2.DataTextField = "Description";
                    ddl_ptype2.DataValueField = "ID";
                    ddl_ptype2.DataBind();
                    ddl_ptype2.Items.Insert(0, new ListItem("---Choose---", "0"));

                    ddl_ptype3.DataSource = dtContactTypes;
                    ddl_ptype3.DataTextField = "Description";
                    ddl_ptype3.DataValueField = "ID";
                    ddl_ptype3.DataBind();
                    ddl_ptype3.Items.Insert(0, new ListItem("---Choose---", "0"));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //fills Firm Abbreviation drop drop down list
        private void FillFirms(DataTable dtFirms)
        {
            try
            {
                //DataSet ds_Firms = ClsFirms.GetAllFirmInfo();
                if (dtFirms != null && dtFirms.Rows.Count > 0)
                {
                    ddl_FirmAbbreviation.DataSource = dtFirms;
                    ddl_FirmAbbreviation.DataTextField = "FirmAbbreviation";
                    ddl_FirmAbbreviation.DataValueField = "FirmID";
                    ddl_FirmAbbreviation.DataBind();
                    ddl_FirmAbbreviation.SelectedValue = "3000";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Waqas 5771 04/15/2009 
        /// <summary>
        /// To make client information editable
        /// </summary>
        /// <param name="check"></param>
        private void EditClientInformation(bool check)
        {
            if (check)
            {
                txt_LastName.Style["display"] = "block";
                lbltxt_Lastname.Style["display"] = "none";

                td_txtName.Style["display"] = "block";
                td_lblName.Style["display"] = "none";

                td_txtDate.Style["display"] = "block";
                td_lbldate.Style["display"] = "none";
            }
            else
            {
                txt_LastName.Style["display"] = "none";
                lbltxt_Lastname.Style["display"] = "block";

                td_txtName.Style["display"] = "none";
                td_lblName.Style["display"] = "block";

                td_txtDate.Style["display"] = "none";
                td_lbldate.Style["display"] = "block";
            }
        }

        //Displays all the information on the page
        private void DisplayInfo()
        {
            try
            {
                // Rab Nawaz 9961 01/03/2011 SMS Should not be sent if the cause number contains the NISI associated 
                hf_smsAlertToNisi.Value = ClsCase.IsNisiAssociatedCase(ClsCase.TicketID) ? "1" : "0";

                string DOB = "";
                //Get case info to display
                DataSet ds_Case = ClsCase.GetCase(ClsCase.TicketID);
                if (ds_Case.Tables.Count > 0)
                {
                    if (ds_Case.Tables[0].Rows.Count > 0)
                    {

                        DataRow dRow = ds_Case.Tables[0].Rows[0];                        
                        /////////////////////////////////////////////////////////////////////////
                        // Abbas Qamar 10093 03-28-2012 hiding the Phone number lable if it exist in non client database.
                        // Rab Nawaz Khan 10195 04/20/2012 Contact from Court field Removed . . . 
                        //int recordid = Convert.ToInt32(ds_Case.Tables[0].Rows[0]["RecordId"]);
                        //int caseTypeId = Convert.ToInt32(ds_Case.Tables[0].Rows[0]["CaseTypeId"]);
                        //string[] key1 = { "@RecordID", "@CaseType" };
                        //object[] value1 = { recordid, caseTypeId };

                        //DataTable dtPhoneNumbers = clsdb.Get_DT_BySPArr("USP_HTP_Get_AllPhoneNumber", key1, value1);

                        //if (dtPhoneNumbers.Rows.Count > 0)
                        //{
                        //    lblContact.Text = "";
                        //    foreach (DataRow dr in dtPhoneNumbers.Rows)
                        //    {
                        //        if (Convert.ToString(dr["PhoneNumber"]) != "" && Convert.ToString(dr["NotinFamily"]) == "1")
                        //        {
                        //            lblContact.Text += "Home : " + Convert.ToString(dr["PhoneNumber"]);
                        //            tbl_primaryno.Visible = true;
                        //            lnkbtn_addphoneno.Visible = false;
                        //        }
                        //        else if (Convert.ToString(dr["PhoneNumber"]) != "" && Convert.ToString(dr["NotinFamily"]) == "0")
                        //        {
                        //            lblContact.Text += "Other : " + Convert.ToString(dr["PhoneNumber"]);
                        //            tbl_primaryno.Visible = true;
                        //            lnkbtn_addphoneno.Visible = false;
                        //        }
                        //        if ((Convert.ToString(dr["WorkPhoneNumber"]) != "") && (Convert.ToString(dr["PhoneNumber"]) != ""))
                        //        {
                        //            lblContact.Text += ", Work : " + Convert.ToString(dr["WorkPhoneNumber"]);
                        //            tbl_primaryno.Visible = true;
                        //            lnkbtn_addphoneno.Visible = false;
                        //        }
                        //        else if ((Convert.ToString(dr["WorkPhoneNumber"]) != "") && (Convert.ToString(dr["PhoneNumber"]) == ""))
                        //        {
                        //            lblContact.Text += " Work : " + Convert.ToString(dr["WorkPhoneNumber"]);
                        //            tbl_primaryno.Visible = true;
                        //            lnkbtn_addphoneno.Visible = false;
                        //        }
                        //        if ((Convert.ToString(dr["WorkPhoneNumber"]) == "") && (Convert.ToString(dr["PhoneNumber"]) == ""))
                        //        {
                        //            tdContact.Visible = false;
                        //            lnkbtn_addphoneno.Visible = true;
                        //            tbl_primaryno.Visible = false;
                        //        }
                        //    }
                        //    txt_CC11.Text = txt_CC12.Text = txt_CC13.Text = txt_CC14.Text = "";


                        //}
                        //else
                        //{
                        //    tdContact.Visible = false;
                        //}
                        /////////////////////////////////////////////////////////////////////////  
                        //Fahad 5807 04/29/2009 Add ViewState to Save Recordid
                        ViewState["RecordID"] = Convert.ToInt32(dRow["recordid"].ToString());
                        //Sabir Khan 5449 01/23/2009 Get promotional message on zipcode...
                        lbl_SpProm.Text = ClsCase.GetPromotionalMessage();

                        txtEmailAddress.Text = dRow["email"].ToString();
                        chkEmailAddress.Checked = Convert.ToBoolean(dRow["EmailNotAvailable"].ToString());


                        if (lbl_SpProm.Text.Length > 0)
                        {
                            hf_isZip7707.Value = "1";
                            td_SpProm.Style["display"] = "block";
                        }
                        else
                        {
                            hf_isZip7707.Value = "0";
                            td_SpProm.Style["display"] = "none";
                        }
                        //Sabir Khan 11460 10/04/2013 Promo client logic has been implemented.
                        //Sabir Khan 11560 New promo lagic has been implemented.
                        DataTable dtPromo = ClsCase.IsPromoClient();
                        if (Convert.ToBoolean(dtPromo.Rows[0]["IsPromo"].ToString()))
                        {
                            td_SpProm.Style["display"] = "block";
                            lbl_SpProm.Text = dtPromo.Rows[0]["PromoMessageForHTP"].ToString();
                        }
                        else
                        {
                            td_SpProm.Style["display"] = "none";
                            lbl_SpProm.Text = "";
                        }
                        ////////{
                        ////////    

                        ////////    //Sabir Khan 5266 11/28/2008 Check for zipcodes...
                        ////////    //Sabir Khan 5449 01/23/2009 set tooltip window for zipcodes...
                        ////////    hf_SpProm.Value = "Special promotional pricing offered for zip codes:";
                        ////////    hf_SpProm.Value = hf_SpProm.Value + ClsCase.GetPromotionalZipCodes();
                        ////////    string ControlName = hf_SpProm.ClientID;
                        ////////    lbl_SpProm.Attributes.Add("Title", "hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='100%'><tr><td width='100%' align='right'><img src='/Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='100%'><tr><td width='100%'><textarea id='txt_zipcodeMsg' name='txt_zipcodeMsg' cols='37' rows='10'></textarea></td></tr></table>] ");
                        ////////    lbl_SpProm.Attributes.Add("OnMouseOver", "zipcodePoupup('" + ControlName + "');");
                        ////////    lbl_SpProm.Attributes.Add("OnMouseOut", "CursorIcon2()");
                        ////////    lbl_SpProm.Attributes.Add("onclick", "copyToClipboard('" + hf_SpProm.Value + "');");
                        ////////}
                        ////////else
                        ////////{
                        ////////    hf_isZip7707.Value = "0";
                        ////////}
                        //end ozair 3717
                        //Sarim 2664 06/10/2008 
                        //lbl_DayB4Date.Text = dRow["DayB4Date"].ToString().Trim();
                        lblLockFlag.Text = dRow["LockFlag"].ToString().Trim();
                        lbl_ActiveFlag.Text = dRow["Activeflag"].ToString().Trim();
                        lbl_LastName.Text = dRow["Lastname"].ToString().Trim().ToUpper();
                        lbl_FirstName.Text = dRow["Firstname"].ToString().Trim().ToUpper();


                        //Waqas 6599 09/19/2009 Occupations List
                        //waqas 6666 10/22/2009 disable javascript suggestion
                        //hf_OccupationList.Value = ClsCase.GetCaseOccupation();
                        //hf_Employer.Value = ClsCase.GetCaseEmployer();

                        //Farrukh 9925 11/28/2011 Remove Occupation and Employer
                        //txtOccupation.Text = dRow["Occupation"].ToString().Trim();

                        //txtEmployer.Text = dRow["Employer"].ToString().Trim();

                        //if (Convert.ToInt32(dRow["IsUnemployed"]) == 0)
                        //{
                        //    chkIsUnempoyed.Checked = false;
                        //    tr_Employer.Style["display"] = "block";
                        //}
                        //else
                        //{
                        //    chkIsUnempoyed.Checked = true;
                        //    txtOccupation.Text = "";
                        //    txtEmployer.Text = "";
                        //    tr_Employer.Style["display"] = "none";
                        //}

                        rBtnCommercialVehicle.Checked = false;
                        rBtnMotorCycle.Checked = false;
                        rBtnCar.Checked = false;

                        if (Convert.ToInt32(dRow["VehicleType"]) == 1)
                        { rBtnCommercialVehicle.Checked = true; }
                        else if (Convert.ToInt32(dRow["VehicleType"]) == 2)
                        { rBtnMotorCycle.Checked = true; }
                        else if (Convert.ToInt32(dRow["VehicleType"]) == 3)
                        { rBtnCar.Checked = true; }



                        //Waqas 5771 04/14/2009 check on contact id

                        if (dRow["ContactID"] == DBNull.Value || dRow["ContactID"].ToString() == "0")
                        {
                            lnkContactID.Text = "CID Lookup";
                            hdnContactID.Value = string.Empty;
                            lnkContactID.Attributes.Add("OnClick", " return CheckPreReqForCID();");
                            EditClientInformation(true);
                        }
                        else
                        {
                            lnkContactID.Text = dRow["ContactID"].ToString();
                            hdnContactID.Value = dRow["ContactID"].ToString();
                            lnkContactID.Attributes.Add("OnClick", " return CheckPreReqForCID();");
                            EditClientInformation(false);
                        }

                        if (Convert.ToInt32(dRow["Activeflag"]) == 0
                            && lnkContactID.Text.ToUpper() != "CID Lookup".ToUpper()
                            && Convert.ToInt32(dRow["IsContactIDConfirmed"]) == 0
                            && Convert.ToInt32(dRow["recordid"].ToString()) > 0
                            && (ClsCase.IsInsideCourtCase(ClsCase.TicketID)))
                        {
                            tbl_CIDConfirmation.Style["display"] = "block";
                        }
                        else
                        {
                            tbl_CIDConfirmation.Style["display"] = "none";
                        }

                        lbl_CaseCount.Text = dRow["CaseCount"].ToString().Trim();
                        hlnk_MidNo.Text = dRow["Midnum"].ToString().Trim();
                        //Fahad 5807 05/08/2009 Add ViewState to save MidNumber
                        ViewState["Midnum"] = dRow["Midnum"].ToString().Trim();
                        txt_FirstName.Text = dRow["Firstname"].ToString().Trim().ToUpper();
                        txt_MiddleName.Text = dRow["MiddleName"].ToString().Trim().ToUpper();
                        txt_LastName.Text = dRow["Lastname"].ToString().Trim().ToUpper();
                        //Waqas 5771 04/15/2009 for contact matter changes
                        lbltxt_Lastname.Text = dRow["Lastname"].ToString().Trim().ToUpper();
                        lbltxt_FirstName.Text = dRow["Firstname"].ToString().Trim().ToUpper();
                        lbltxt_MiddleName.Text = dRow["MiddleName"].ToString().Trim().ToUpper();
                        //nasir 5381 01/15/2009 assign latest date 
                        hf_LastGerenalcommentsUpdatedate.Value = ds_Case.Tables[0].Rows[0]["LastGerenalcommentsUpdatedate"].ToString().Trim();
                        DOB = dRow["DOB"].ToString().Trim();

                        if (DOB != "" && DOB.Length == 10)
                        {
                            txt_mm.Text = DOB.Substring(0, 2);
                            txt_dd.Text = DOB.Substring(3, 2);
                            txt_yy.Text = DOB.Substring(6);
                            //Waqas 5771 04/15/2009
                            lbltxt_mm.Text = DOB.Substring(0, 2);
                            lbltxt_dd.Text = DOB.Substring(3, 2);
                            lbltxt_yy.Text = DOB.Substring(6);
                        }

                        ddl_CourtLocation.SelectedValue = dRow["CurrentCourtloc"].ToString().Trim();
                        //Sabir Khan 4636 08/19/2008  set the selected value of case type drop down to CaseTypeID
                        ddl_CaseType.SelectedValue = dRow["CaseTypeID"].ToString().Trim();
                        // Noufil 5807 06/22/2009 Case type check added to show or hide New hire bond client link.
                        if (Convert.ToInt32(dRow["CaseTypeID"].ToString().Trim()) == 1)
                        //Waqas 5864 07/16/2009 Pipe Sign
                        {
                            lnkbtn_NewHireBondClient.Visible = true;
                            lbl_NewHireBondClient.Visible = true;
                        }
                        else
                        {
                            lnkbtn_NewHireBondClient.Visible = false;
                            lbl_NewHireBondClient.Visible = false;
                        }
                        EnableSPNSearch();

                        ddl_FirmAbbreviation.SelectedValue = dRow["FirmID"].ToString().Trim();
                        lbl_ofirm.Text = getFirmName(Convert.ToInt32(dRow["FirmID"]));
                        Txt_CourtNo.Text = dRow["CurrentCourtNum"].ToString().Trim();
                        //lbl_CourtNo.Text=dRow["CurrentCourtNum"].ToString().Trim();
                        //khalid
                        //Comment By Zeeshan Not Used in the page
                        //if (dRow["CurrentCourtloc"].ToString().Trim() == "3037") showworldimage = true;
                        string contact1 = dRow["Contact1"].ToString().Trim();
                        if (contact1.Length > 0)
                        {
                            // Comment By Zeeshan Ahmed..//code below uncoment by Azee for modification send on 13 march 2007 point no 2.
                            // Agha Usman 4274 07/22/2008 - Try Catch Remove - Do error measures
                            if (contact1.Length >= 3)
                                txt_CC11.Text = contact1.Substring(0, 3);
                            if (contact1.Length >= 6)
                                txt_CC12.Text = contact1.Substring(3, 3);
                            if (contact1.Length >= 10)
                                txt_CC13.Text = contact1.Substring(6, 4);

                            if (contact1.Length > 10)
                                txt_CC14.Text = contact1.Substring(10);


                            tbl_primaryno.Visible = true;
                            lnkbtn_addphoneno.Visible = false;
                            pnl_telnos.Visible = false;

                        }
                        else
                        {
                            // Rab Nawaz Khan 10195 04/20/2012 Contact from Court field Removed . . . 
                            //if (tdContact.Visible)
                            //{
                            //    tbl_primaryno.Visible = true;
                            //    lnkbtn_addphoneno.Visible = false;
                            //}
                            //else
                            //{
                            //    tbl_primaryno.Visible = false;
                            //    lnkbtn_addphoneno.Visible = true;
                            //    //lnkbtn_addphoneno.Attributes.Add("click", "return showPhonePanel();");
                            //    lnkbtn_addphoneno.OnClientClick = "return showPhonePanel();";
                            //    //pnl_telnos.Attributes.Add("Display", "none");
                            //}

                            tbl_primaryno.Visible = false;
                            lnkbtn_addphoneno.Visible = true;
                            //lnkbtn_addphoneno.Attributes.Add("click", "return showPhonePanel();");
                            lnkbtn_addphoneno.OnClientClick = "return showPhonePanel();";
                            //pnl_telnos.Attributes.Add("Display", "none");
                        }

                        // Selecting Contact Type
                        ddl_ContactType.SelectedValue = dRow["ContactType1"].ToString().Trim();
                        // Selecting Client Language
                        //Farrukh 9925 12/26/2011 exception handled
                        if (dRow["LanguageSpeak"].ToString().Trim() == "-1")
                            ddl_language.SelectedIndex = 0;
                        else
                            ddl_language.SelectedValue = dRow["LanguageSpeak"].ToString().Trim();
                        //Zeeshan Ahmed 3979 05/15/2008 Modify Code For Criminal And Civil Cases
                        CurrentCaseType = (int)dRow["CaseTypeId"];
                        ViewState["CaseTypeID"] = dRow["CaseTypeId"];
                        string acflag = lbl_ActiveFlag.Text;
                        SetFeeQuestions(CurrentCaseType);

                        //Set Bond Flag
                        int bond = Convert.ToInt32(dRow["BondFlag"].ToString().Trim());

                        if (bond != 3)
                        {
                            ViewState["bflag"] = bond;

                            switch (CurrentCaseType)
                            {
                                case CaseType.Civil:
                                    break;
                                case CaseType.Criminal:
                                    SetFlag(rdbtn_cBondYes, rdbtn_cBondNo, bond);
                                    break;

                                // Noufil 4782 09/23/2008 Family law case added
                                case CaseType.FamilyLaw:
                                    break;
                                default:
                                    SetFlag(rdbtn_BondYes, rdbtn_BondNo, bond);
                                    break;
                            }
                        }
                        //Set Accident Flag & CDL
                        int accident = Convert.ToInt32(dRow["AccidentFlag"].ToString().Trim());
                        int cdl = Convert.ToInt32(dRow["CDLFlag"].ToString().Trim());

                        // Set need payment plan question...
                        int PayPlan = (dRow["HasPaymentPlan"].ToString().ToUpper().Equals("TRUE")) ? 1 : 0;

                        ViewState["acc"] = accident.ToString();
                        ViewState["cdl"] = accident;

                        switch (CurrentCaseType)
                        {
                            case CaseType.Civil:
                                //SetFlag(rdbtn_vAccidentYes, rdbtn_vAccidentNo, accident);
                                //Sabir Khan 6020 08/07/2009 set is payment plan needed flag...
                                SetFlag(rbtn_vplanyes, rbtn_vplanno, PayPlan);
                                break;
                            // Noufil 4782 09/23/2008 Family law case added
                            case CaseType.FamilyLaw:
                                //SetFlag(rdbtn_vAccidentYes, rdbtn_vAccidentNo, accident);
                                // tahir 4786 10/08/2008
                                SetFlag(rbtn_vplanyes, rbtn_vplanno, PayPlan);
                                break;
                            case CaseType.Criminal:
                                SetFlag(rdbtn_cAccidentYes, rdbtn_cAccidentNo, accident);
                                SetFlag(rdbtn_cCDLYes, rdbtn_cCDLNo, cdl);
                                break;
                            default:
                                SetFlag(rdbtn_AccidentYes, rdbtn_AccidentNo, accident);
                                SetFlag(rdbtn_CDLYes, rdbtn_CDLNo, cdl);
                                //Haris Ahmed 10381 08/30/2012 Show hide immigration comments
                                SetImmigration();
                                break;
                        }

                        //Zeeshan Ahmed 3535 04/10/2008
                        lblManualClient.Visible = (Convert.ToInt32(dRow["RecordID"]) == 0) ? true : false;

                        // initializing comment control in edit mode
                        //Nasir 6098 08/21/2009 general comment initialize overload method

                        WCC_GeneralComments.Initialize(Convert.ToInt32(ViewState["vTicketID"]), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 1, "Label", "clsinputadministration", "clsbutton", "Label", true);
                        WCC_GeneralComments.Enabled = true;
                        WCC_GeneralComments.Button_Visible = false;


                        if (WCC_GeneralComments.Label_Text.Length > 0)
                        {
                            // Re-initializing comment control in default mode
                            WCC_GeneralComments.Initialize(Convert.ToInt32(ViewState["vTicketID"]), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 1, "Label", "clsinputadministration", "clsbutton", false);
                            WCC_GeneralComments.Enabled = true;
                            WCC_GeneralComments.Button_Visible = false;
                        }

                        chkbtn_read.Visible = true;
                        ReadNotes1.Visible = true;
                        lbl_Fee.Text = Convert.ToDouble(dRow["BaseFeeCharge"]).ToString("$###0;($###0)");

                        if (Convert.ToBoolean(dRow["ContinuanceFlag"]))
                        {
                            lblCont.Visible = true;
                            lbl_Cont.Visible = true;
                            lblCont.Text = Convert.ToDouble(dRow["ContinuanceAmount"]).ToString("$###0;($###0)");
                            lbl_Cont.Text = "Cont.";
                        }
                        else
                        {
                            lblCont.Visible = false;
                            lbl_Cont.Visible = false;
                        }

                        double tfc = Convert.ToDouble(dRow["TotalFeeCharged"]);
                        //Waqas 4654 03/07/2009 InitialAdjustmentInitial is returned '' before initial Adjustment, so we checking it
                        int iAdji = dRow["InitialAdjustmentInitial"].ToString().Length;
                        if (lblLockFlag.Text == "False" && tfc == 0 && iAdji == 0)
                        {
                            lblTFC.Text = "0";
                            txt_InitialAdjustment.Visible = true;
                            lbl_InitialAdjustment.Visible = false;
                            txt_FinalAdjustment.Visible = false;
                            txt_FinalAdjustment.Text = dRow["Adjustment"].ToString().Trim();
                            if (dRow["InitialAdjustment"].ToString() == "0")
                            {
                                txt_InitialAdjustment.Text = "0";
                            }
                            else
                            {
                                txt_InitialAdjustment.Text = Convert.ToDouble(dRow["InitialAdjustment"]).ToString("###0;-###0").Trim();
                            }
                        }
                        else if (lblLockFlag.Text == "True" || tfc != 0 || iAdji != 0) //Waqas 4654 03/07/2009 Checking for final adjustment call
                        {
                            lblTFC.Text = "1";
                            txt_InitialAdjustment.Visible = false;
                            lbl_InitialAdjustment.Visible = true;
                            txt_FinalAdjustment.Visible = true;
                            lbl_InitialAdjustment.Text = String.Format("{0}{1}", Convert.ToDouble(dRow["InitialAdjustment"]).ToString("$###0;($###0)"), dRow["InitialAdjustmentInitial"]);
                            txt_FinalAdjustment.Text = Convert.ToDouble(dRow["Adjustment"]).ToString("###0;-###0").Trim();
                            if (dRow["InitialAdjustment"].ToString() == "0")
                            {
                                //txt_InitialAdjustment.Text="0";
                                IAdj = 0;
                                txt_iadj.Text = "0";
                            }
                            else
                            {
                                //txt_InitialAdjustment.Text=Convert.ToDouble(dRow["InitialAdjustment"]).ToString("###0;-###0").Trim();
                                IAdj = Convert.ToDouble(dRow["InitialAdjustment"]);
                                txt_iadj.Text = Convert.ToDouble(dRow["InitialAdjustment"]).ToString("###0;-###0").Trim();
                            }
                        }
                        double Fee = Convert.ToDouble(dRow["calculatedtotalfee"]);
                        double Paid = Convert.ToDouble(dRow["Paid"]);
                        double Owes = Fee - Paid;
                        lbl_CalculatedFee.Text = Fee.ToString("$###0;($###0)");
                        lbl_Paid.Text = Paid.ToString("$###0.##;($###0.##)");
                        lbl_EstimatedOwes.Text = Owes.ToString("$###0.##;($###0.##)");
                        lbl_actualowes.Text = (Convert.ToDouble(dRow["totalfeecharged"]) - Paid).ToString("$###0.##;($###0.##)");

                        // Rab Nawaz Khan Moved the below code to the BindFlageNew() to reduce the page load time. . . 
                        //Noufil 4215 07/01/2008 Check SOl flag
                        //ClsFlags cflag = new ClsFlags();
                        //cflag.FlagID = Convert.ToInt32(FlagType.SOL);
                        //cflag.TicketID = Convert.ToInt32(ClsCase.TicketID);
                        //ViewState["HasSOLFlag"] = Convert.ToInt32(cflag.HasFlag()).ToString();
                        //Sabir Khan 8623 12/13/2010 display a message on top if pre trial diviersion flag is active for the case.
                        //cflag.FlagID = Convert.ToInt32(FlagType.Pre_Trial_Diversion);
                        //if (cflag.HasFlag())
                        //{
                        //    //Abbas Shahid Khwaja 9435 06/27/2011 font size,bold and name is set. 
                        //    lbl_Message.Visible = true;
                        //    lbl_Message.CssClass = "FlagMessageBold";
                        //    lbl_Message.Text = "The client has a Pre-Trial Diversion status. This case can't be reschedule";
                        //}
                        //else
                        //{
                        //    lbl_Message.Visible = false;
                        //}
                        //cflag.FlagID = Convert.ToInt32(FlagType.Motion_Hiring);
                        //if (cflag.HasFlag())
                        //{
                        //    if (lbl_Message.Visible)
                        //    {
                        //        //Abbas Shahid Khwaja 9435 06/28/2011 font size,bold and name is set. 
                        //        lbl_Message.CssClass = "FlagMessageBold";
                        //        lbl_Message.Text = lbl_Message.Text + "<br/> The client has a Motion Hiring status. This case can't be reschedule";
                        //    }
                        //    else
                        //    {
                        //        //Abbas Shahid Khwaja 9435 06/28/2011 font size,bold and name is set. 
                        //        lbl_Message.Visible = true;
                        //        lbl_Message.CssClass = "FlagMessageBold";
                        //        lbl_Message.Text = "The client has a Motion Hiring status. This case can't be reschedule";
                        //    }


                        //}

                        ////Sabir Khan 8862 02/15/2011 Message for JUVNILE Flag...
                        //cflag.FlagID = Convert.ToInt32(FlagType.JUVENILE);
                        //if (cflag.HasFlag())
                        //{
                        //    if (lbl_Message.Visible)
                        //    {
                        //        lbl_Message.Text = "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.";
                        //    }
                        //    else
                        //    {
                        //        lbl_Message.Visible = true;
                        //        lbl_Message.Text = "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.";
                        //    }
                        //}

                        // Rab Nawaz Khan 10353 07/03/2012 Red message added for the CLIENTS/QUOTE Clients BEAUMONT
                        if (dRow["Zip"].ToString().Trim() != "" || dRow["Zip"].ToString().Trim() != null)
                        {
                            if (dRow["Zip"].ToString().Trim().Length >= 3)
                            {
                                string zipcheck = dRow["Zip"].ToString().Trim().Substring(0, 3);
                                if (zipcheck == "777")
                                {
                                    lbl_Message.Text = "CLIENT LIVES IN BEAUMONT. PLEASE QUOTE CLIENT AND THEN SPEAK WITH ANDREW AND REY IMMEDIATLEY ABOUT CASE.";
                                    lbl_Message.Visible = true;
                                }
                            }
                        }
                        // End 10353

                        //Displays Data Grid Detail Info
                        DataSet ds_CaseDetail = ClsCaseDetail.GetCaseDetail(ClsCase.TicketID);
                        hf_lastrow.Value = ds_CaseDetail.Tables[0].Rows.Count.ToString();
                        dgViolationInfo.Columns[4].Visible = false;

                        //Ozair 2515 01/02/2008
                        dgViolationInfo.Columns[14].Visible = false;

                        //Ozair 3717 04/12/2008
                        //td_SpProm.Style["display"] = "none";

                        // tahir 4841 09/23/2008 add sorting on violation grid...
                        Session["dvViol"] = ds_CaseDetail.Tables[0].DefaultView;

                        dgViolationInfo.DataSource = ds_CaseDetail;
                        dgViolationInfo.DataBind();

                        // Rab Nawaz Khan 10330 06/26/2012 Show the Additional Amount in the Calculation Grid. . . 
                        DataSet ds_clientPaymentsInfo = ClsCase.GetClientPaymentInfo(ClsCase.TicketID);
                        bool isClientPaymentFound = Convert.ToBoolean(ds_clientPaymentsInfo.Tables[0].Rows[0]["isPaymentFound"]);
                        if (isClientPaymentFound || Convert.ToInt32(dRow["Activeflag"].ToString().Trim()) == 0)
                        {
                            if (Convert.ToInt32(dRow["CaseTypeID"].ToString()) == 1)
                            {

                                if (rdbtn_cAccidentYes.Checked || rdbtn_cAccidentNo.Checked || rdbtn_AccidentNo.Checked ||
                                    rdbtn_AccidentYes.Checked)
                                {
                                    if (Convert.ToBoolean(dRow["AccidentFlag"]) == true)
                                    {
                                        td_accident.Style["Display"] = "block";
                                        td_accidentValue.Style["Display"] = "block";
                                    }
                                    else
                                    {
                                        td_accident.Style["Display"] = "none";
                                        td_accidentValue.Style["Display"] = "none";
                                    }
                                }

                                if (rdbtn_cCDLYes.Checked || rdbtn_cCDLNo.Checked || rdbtn_CDLYes.Checked || rdbtn_CDLNo.Checked)
                                {

                                    if (Convert.ToBoolean(dRow["CDLFlag"]) == true)
                                    {
                                        td_cdl.Style["Display"] = "block";
                                        td_cdlValue.Style["Display"] = "block";
                                    }
                                    else
                                    {
                                        td_cdl.Style["Display"] = "none";
                                        td_cdlValue.Style["Display"] = "none";
                                    }
                                }
                            }
                        }

                        // END 10330

                        //Waqas 5864 06/23/2009 Databinding to Email popup.
                        //Hafiz 10288 07/19/2012 commented the below section
                        //DataSet ds_dlstate = clsdb.Get_DS_BySP("USP_HTS_Get_State");

                        //ddl_ALRState.DataSource = ds_dlstate;
                        //ddl_ALRState.DataTextField = ds_dlstate.Tables[0].Columns[1].ColumnName;
                        //ddl_ALRState.DataValueField = ds_dlstate.Tables[0].Columns[0].ColumnName;
                        //ddl_ALRState.DataBind();

                        ////Waqas 6342 08/12/2009 ALR Required
                        //ddl_ALROBSState.DataSource = ds_dlstate;
                        //ddl_ALROBSState.DataTextField = ds_dlstate.Tables[0].Columns[1].ColumnName;
                        //ddl_ALROBSState.DataValueField = ds_dlstate.Tables[0].Columns[0].ColumnName;
                        //ddl_ALROBSState.DataBind();


                        lblClientEmailName.Text = lbl_FirstName.Text + " " + lbl_LastName.Text;

                        DataTable dtAtt = ClsCaseDetail.GetAttorneysByTickets(ClsCase.TicketID);

                        drpAttorneysOnEmails.DataSource = dtAtt;
                        drpAttorneysOnEmails.DataTextField = "AttorneyName";
                        drpAttorneysOnEmails.DataValueField = "Email";

                        drpAttorneysOnEmails.DataBind();

                        drpAttorneysOnEmails.Items.Insert(0, new ListItem("---- Choose ----", ""));

                        drpAttorneysOnEmails.Attributes.Add("onchange", "return SetEmailAttorney();");

                        if (dtAtt.Rows.Count == 1)
                        {
                            tr_DrpAttonerneyName.Style["display"] = "none";
                            tr_lblAttorneyName.Style["display"] = "block";

                            lblEmailAttorneyName.Text = dtAtt.Rows[0]["AttorneyName"].ToString();
                            hf_EmailAttorneyName.Value = dtAtt.Rows[0]["AttorneyName"].ToString();
                            //Nasir 6619 11/25/2009 set Email in text Box
                            txtAttorneyEmail.Enabled = true;
                            txtAttorneyEmail.Text = dtAtt.Rows[0]["Email"].ToString();

                        }
                        else
                        {
                            tr_DrpAttonerneyName.Style["display"] = "block";
                            tr_lblAttorneyName.Style["display"] = "none";
                        }

                        if (ds_CaseDetail.Tables[0].Rows.Count > 6)
                        {
                            pnlViolationEmailScrolPanel.Height = 155;
                        }

                        //For ALR only
                        //DataTable dtALRViolations = ds_CaseDetail.Tables[0].Copy();
                        //for (int i = 0; i < dtALRViolations.Rows.Count; i++)
                        //{
                        //    DataRow dr = dtALRViolations.Rows[i];
                        //    if (dr["ViolationNumber_PK"].ToString() != "16159")
                        //    {
                        //        dtALRViolations.Rows.Remove(dr);
                        //        i++;

                        //    }
                        //}
                        int ALRCount = 0;
                        //Yasir Kamal 7398 02/15/2010 display mesg in red if time not b/w 08:00 AM and 12:00 PM for HMC(Lubbock) Judge Trial
                        bool HmcLubJudgeAlert1 = false;
                        //Yasir Kamal 7453 03/01/2010 display mesg in red if time b/w 08:00 AM and 12:00 PM for HMC(Lubbock) Judge Trial
                        bool HmcLubJudgeAlert2 = false;

                        //Waqas 6342 08/24/2009 for criminal and family cases except ALR.
                        bool CheckedALRQuestion = false;
                        DataTable dtALRViolations = ds_CaseDetail.Tables[0].Copy();

                        for (int i = 0; i < ds_CaseDetail.Tables[0].Rows.Count; i++)
                        {
                            DataRow dr = ds_CaseDetail.Tables[0].Rows[i];
                            if (dr["ViolationNumber_PK"].ToString() == "16159")
                            {
                                dtALRViolations.Rows.RemoveAt(i - ALRCount);
                                ALRCount++;
                            }
                            // Sabir Khan 7398 02/11/2010 Display message if HMC(Lublock) Judge Trial having court time not b/w 8:00AM and 12:00PM.
                            if (dr["CourtID"].ToString() == "3001")
                            {
                                DateTime dt = DateTime.Parse(dr["Courtdatemain"].ToString());
                                int hours = dt.Hour;
                                int min = dt.Minute;

                                if (dr["CategoryID"].ToString() == "5")
                                {
                                    if ((hours < 8) || (hours == 12 && min > 0) || (hours > 12))
                                    {
                                        HmcLubJudgeAlert1 = true;
                                    }
                                    if ((hours == 8) || (hours == 12 && min == 0) || (hours < 12))
                                    {
                                        HmcLubJudgeAlert2 = true;
                                    }
                                }

                            }
                            //Asad Ali 8153 09/09/2010 if Any court is Criminal Court then set IsCriminal Flag true
                            if (dr["IsCriminalCourt"] != null && Convert.ToBoolean(dr["IsCriminalCourt"]) == true)
                            {
                                hf_ISCriminalCourtForALR.Value = "1";
                                // Rab Nawaz Khan 10349 09/04/2013 Page Length Dynamically increased for the new Criminal cases. . . 
                                if (dr["isNewClient"] != null && Convert.ToBoolean(dr["isNewClient"]))
                                {
                                    //TableMain.Style["width"] = "1000px";
                                    //td_violationBox.Style["width"] = "1000px";
                                    TableSub.Style["width"] = "1000px";
                                    txtCaseSummaryComments.Width = 1000;
                                    WCC_GeneralComments.Width = 1000;
                                    WCC_GeneralComments.TextBox_CSS = "textBoxWidth";

                                }
                            }
                            ////Asad Ali 8153 09/09/2010 if any violation contain DWI and Court Is Criminal then Show ALR Hearing Question 
                            if (System.Text.RegularExpressions.Regex.IsMatch(
                                dr["violationDescription"].ToString(),
                                "DWI",
                                System.Text.RegularExpressions.RegexOptions.IgnoreCase
                                )
                                )
                            {
                                CheckedALRQuestion = true;
                            }



                        }


                        if (HmcLubJudgeAlert1)
                        {
                            lblHmcLubMesg1.Visible = true;
                            tr_lblHmcLubMesg1.Visible=true;
                        }
                        else
                        {
                            lblHmcLubMesg1.Visible = false;
                           tr_lblHmcLubMesg1.Visible=false;
                        }
                        if (HmcLubJudgeAlert2)
                        {
                            lblHmcLubMesg2.Visible = true;
                            tr_lblHmcLubMesg2.Visible=true;
                        }
                        else
                        {
                            lblHmcLubMesg2.Visible = false;
                           tr_lblHmcLubMesg2.Visible=false;
                        }
                        ////Yasir Kamal 7398 end
                        grdEmailCaseSummary.DataSource = dtALRViolations;
                        grdEmailCaseSummary.DataBind();

                        //Zeeshan Ahmed 3724 05/13/2008 If criminal case than display arrest date column and hide fine amount column
                        if (ClsCase.IsCriminalCase())
                        {
                            dgViolationInfo.Columns[15].Visible = true;
                            dgViolationInfo.Columns[13].Visible = false;
                        }
                        else
                        {
                            dgViolationInfo.Columns[15].Visible = false;
                            dgViolationInfo.Columns[13].Visible = true;
                        }

                        if (CaseType.Civil == CurrentCaseType)
                            dgViolationInfo.Columns[13].Visible = false;

                        if (CaseType.FamilyLaw == CurrentCaseType)
                            dgViolationInfo.Columns[13].Visible = false;

                        //Zeeshan Ahmed 3535 04/08/2008
                        hfIsInsideCase.Value = Convert.ToInt32(ClsCase.IsInsideCourtCase(ClsCase.TicketID)).ToString();
                        hfHasFTAViolations.Value = Convert.ToInt32(ClsCase.HasFTAViolations(ClsCase.TicketID)).ToString();

                        //Zeeshan Ahmed 3534 04/16/2008
                        //Show Fine Amount Button If Case Has FTA Violation
                        if (hfHasFTAViolations.Value == "1" && hfIsInsideCase.Value == "1")
                            btnfineamount.Visible = true;
                        else
                            btnfineamount.Visible = false;

                        //Set Late Fee And Call Back Flag
                        switch (CurrentCaseType)
                        {
                            //Yasir Kamal 7355 01/29/2010 set call back date on friday for criminal & familiy
                            case CaseType.Civil:
                                SetCallBack(Convert.ToInt32(dRow["Activeflag"]), Convert.ToInt32(dRow["Followupflag"]), Convert.ToDateTime(dRow["Callbackdate"]), rbtn_vFollowUp_Yes, rbtn_vFollowUp_No, td_vcallback_days, Convert.ToInt32(dRow["CaseTypeID"]));
                                SetLateFlag(Convert.ToInt32(dRow["LateFlag"]), ds_CaseDetail.Tables[0], rbtn_vlateyes, rbtn_vlateno);
                                break;
                            // Noufil 4782 09/23/2008 Family law case added
                            case CaseType.FamilyLaw:
                                SetCallBack(Convert.ToInt32(dRow["Activeflag"]), Convert.ToInt32(dRow["Followupflag"]), Convert.ToDateTime(dRow["Callbackdate"]), rbtn_vFollowUp_Yes, rbtn_vFollowUp_No, td_vcallback_days, Convert.ToInt32(dRow["CaseTypeID"]));
                                SetLateFlag(Convert.ToInt32(dRow["LateFlag"]), ds_CaseDetail.Tables[0], rbtn_vlateyes, rbtn_vlateno);
                                break;
                            case CaseType.Criminal:
                                SetCallBack(Convert.ToInt32(dRow["Activeflag"]), Convert.ToInt32(dRow["Followupflag"]), Convert.ToDateTime(dRow["Callbackdate"]), rbtn_cFollowUp_Yes, rbtn_cFollowUp_No, td_ccallback_days, Convert.ToInt32(dRow["CaseTypeID"]));
                                SetLateFlag(Convert.ToInt32(dRow["LateFlag"]), ds_CaseDetail.Tables[0], rbtn_clateyes, rbtn_clateno);
                                break;
                            default:
                                SetCallBack(Convert.ToInt32(dRow["Activeflag"]), Convert.ToInt32(dRow["Followupflag"]), Convert.ToDateTime(dRow["Callbackdate"]), rbtn_FollowUp_Yes, rbtn_FollowUp_No, td_callback_days, Convert.ToInt32(dRow["CaseTypeID"]));
                                SetLateFlag(Convert.ToInt32(dRow["LateFlag"]), ds_CaseDetail.Tables[0], rbtn_lateyes, rbtn_lateno);
                                break;
                        }

                        #region "ozair 4274 06/19/2008"
                        /* comented as it is not used any more; related to updateviolation page which is not used now                        
                        try
                        {

                            if (ClsCaseDetail.TicketViolationID != 0)
                            {
                                //IsNewViolation=false;
                                //txt_IsNew.Text = "0";
                                DataSet ds_ViolationDetail = ClsCaseDetail.GetCaseDetailByViolationID(ClsCaseDetail.TicketViolationID);
                                if (ds_ViolationDetail.Tables[0].Rows.Count > 0)
                                {

                                    //hf_AutoStatusID.Text = ds_ViolationDetail.Tables[0].Rows[0]["AutoStatus"].ToString().Trim();
                                    hf_AutoStatusID.Value = ds_ViolationDetail.Tables[0].Rows[0]["AutoStatusID"].ToString().Trim();
                                    if (ds_ViolationDetail.Tables[0].Rows[0][11].ToString() != "")
                                    {
                                        hf_AutoCourtDate.Value = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateAuto"].ToString().Trim().Substring(0, 10);

                                        try
                                        {
                                            hf_AutoTime.Value = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateAuto"].ToString().Trim().Substring(11, 7);
                                        }
                                        catch (Exception)
                                        {
                                            hf_AutoTime.Value = ds_ViolationDetail.Tables[0].Rows[0]["CourtDateAuto"].ToString().Trim().Substring(12, 6);
                                        }
                                    }
                                    hf_AutoNo.Value = ds_ViolationDetail.Tables[0].Rows[0]["CourtNumberAuto"].ToString().Trim();

                                }
                            }
                        }
                        catch { }
                        */
                        #endregion


                        //Waqas 6342 08/24/2009 New changes for all criminal and family cases


                        hf_IsPreHire.Value = "0";
                        hf_IsALROnly.Value = "0";
                        //Waqas 5864 06/15/2009 For Criminal ALR.                        

                        if (Convert.ToInt32(dRow["isALR"]) == 1)
                        {
                            hf_IsALROnly.Value = "1";
                        }
                        if (CurrentCaseType == CaseType.Criminal || CurrentCaseType == CaseType.FamilyLaw)
                        {
                            lnkEmailCaseSummary.OnClientClick = "return ShowCaseEmailPanel();";
                            hf_IsPreHire.Value = "1";


                            
                            tr_PreHireQuestion.Style["display"] = "block";
                            tr_QuestionBar.Style["display"] = "block";
                            tr_FeeQuestion.Style["display"] = "none";

                            tbBodyCriminalALR.Style["display"] = "block";
                            tbBodyCaseSummary.Style["display"] = "block";

                            //Waqas 6342 08/24/2009 New changes for all criminal and family cases
                            if (dRow["HavePriorAttorney"].ToString() != "")
                            {
                                if (dRow["HavePriorAttorney"].ToString().ToLower().Equals("true"))
                                {
                                    rbtnPriorAttorneyYes.Checked = true;
                                    rbtnPriorAttorneyNo.Checked = false;

                                    td_priorattorneytype.Style["display"] = "block";
                                    if (dRow["PriorAttorneyType"].ToString().ToLower().Equals("court appointed attorney"))
                                    {
                                        drpAtt.SelectedIndex = 1;
                                    }
                                    else if (dRow["PriorAttorneyType"].ToString().ToLower().Equals("hired defense attorney"))
                                    {
                                        drpAtt.SelectedIndex = 2;
                                    }
                                    else
                                    {
                                        drpAtt.SelectedIndex = 0;
                                    }

                                    td_priorattorney.Style["display"] = "block";
                                    txtPriorAttorney.Text = dRow["PriorAttorneyName"].ToString();

                                }
                                else if (dRow["HavePriorAttorney"].ToString().ToLower().Equals("false"))
                                {
                                    rbtnPriorAttorneyYes.Checked = false;
                                    rbtnPriorAttorneyNo.Checked = true;
                                }
                            }
                            else
                            {
                                rbtnPriorAttorneyYes.Checked = false;
                                rbtnPriorAttorneyNo.Checked = false;
                            }
                            //Asad Ali 8153 10/27/2010 fix issue not setting intoxilyzer drop down value 
                            drp_IntoxilyzerTaken.SelectedValue = dRow["ALRIsIntoxilyzerTakenFlag"].ToString();
                            txtCaseSummaryComments.Text = dRow["CaseSummaryComments"].ToString();
                            lblCaseSummaryCommentsonEmails.Text = Server.HtmlEncode(dRow["CaseSummaryComments"].ToString());

                            NewClsPayments ClsPayments = new NewClsPayments();
                            ClsPayments.GetPaymentInfo(ClsCase.TicketID);
                            string owes = ClsPayments.Owes.ToString();
                            string date = ClsPayments.GetMaxSchedulePaymentDate(ClsCase.TicketID);
                            string PaymentMessage = "";
                            if (owes == "0")
                            {
                                PaymentMessage = "Paid in full.";
                            }
                            else
                            {
                                if (date == "" || Convert.ToDateTime(date).ToString("MM/dd/yyyy").Equals("01/01/1900"))
                                {
                                    //Yasir Kamal 7140 12/15/2009 do not display amount.
                                    PaymentMessage = "Payment scheduled date is not available.";
                                }
                                else
                                {
                                    PaymentMessage = Convert.ToDateTime(date).ToString("MM/dd/yyyy");
                                }
                                //7140 end
                            }


                            lblPaidPaymentMessage.Text = PaymentMessage;
                            //Asad Ali 8153 09/03/2010 Show For All Criminal COurts
                            if (hf_ISCriminalCourtForALR.Value == "1")
                            {
                                tr_ALRhearingRequired.Style[HtmlTextWriterStyle.Display] = "block";
                                //Asad Ali 8153 09/03/2010 if IsAlrHearing Required then show inxt taken dropdown
                                if (dRow["isALRhearingRequired"] != DBNull.Value && Convert.ToInt32(dRow["isALRhearingRequired"]) == 1)
                                {
                                    tr_IntoxilyzerTaken.Style[HtmlTextWriterStyle.Display] = "table-row";
                                }
                                else
                                {
                                    tr_IntoxilyzerTaken.Style[HtmlTextWriterStyle.Display] = "none";
                                }
                            }
                            //Asad Ali 8153 09/20/2010 checked true if any violation have DWI vioalation 

                            //Farrukh 10763 06/13/2013 Pre-selection removed for Criminal cases having DWI violation
                            if (dRow["IsALRHearingRequired"] == DBNull.Value)
                            {
                                if (Convert.ToString(ViewState["CaseTypeID"]) != "2")
                                {
                                    rBtn_ALRHearingRequiredYes.Checked = (CheckedALRQuestion == true ? true : false);
                                    tr_IntoxilyzerTaken.Style[HtmlTextWriterStyle.Display] = (CheckedALRQuestion == true ? "table-row" : "none");
                                    rBtn_ALRHearingRequiredNo.Checked = false;
                                }
                            }
                            else if (dRow["IsALRHearingRequired"] != DBNull.Value && Convert.ToBoolean(dRow["IsALRHearingRequired"]) == false)
                            {
                                rBtn_ALRHearingRequiredYes.Checked = false;
                                rBtn_ALRHearingRequiredNo.Checked = true;
                            }
                            else
                            {
                                rBtn_ALRHearingRequiredYes.Checked = true;
                                rBtn_ALRHearingRequiredNo.Checked = false;
                            }
                            
                            if (Convert.ToInt32(dRow["Activeflag"]) == 0)
                            {
                                //Hafiz 10288 07/19/2012 commented the below section
                                //tr_postHireQuestions.Style[HtmlTextWriterStyle.Display] = "none";
                                //End
                                //Waqas 6342 08/12/2009 ALR required
                                //tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "none";
                                //Asad Ali 8153 09/09/2010 move this question in pre hire section 
                                //tr_ALRhearingRequired.Style[HtmlTextWriterStyle.Display] = "none";

                                lnkEmailCaseSummary.Visible = false;
                                lblEmailCaseSummary.Visible = false;
                            }
                            else
                            {
                                //tbBodyCriminalALRQA.Disabled = true;
                                //txtCaseSummaryComments.Enabled = false;


                                //Waqas 6342 08/24/2009 For All criminal 

                                lnkEmailCaseSummary.Visible = true;
                                lblEmailCaseSummary.Visible = true;

                                //If all violations are of ALR it will return true
                                if (Convert.ToInt32(dRow["isALLALR"]) == 1)
                                {

                                    lnkEmailCaseSummary.Visible = false;
                                    lblEmailCaseSummary.Visible = false;
                                }



                                if (Convert.ToInt32(dRow["isALR"]) == 1)
                                {
                                    //Hafiz 10288 07/19/2012 commented the below section

                                    //tr_postHireQuestions.Style[HtmlTextWriterStyle.Display] = "block";
                                    ////tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "block";
                                    ////Asad Ali 8153 09/09/2010 Move to PreHire Section
                                    ////Waqas 6342 08/12/2009 ALR required

                                    ////Yasir Kamal 7150 01/01/2010 display County of Arrest.
                                    //if (dRow["CountyOfArrest"].ToString().Trim().ToLower() != "")
                                    //{
                                    //    lblCountyOfArrest.Text = dRow["CountyOfArrest"].ToString();
                                    //    HLkCounty.Style[HtmlTextWriterStyle.Display] = "none";
                                    //    HLkCounty.NavigateUrl = "";
                                    //    lblCountyOfArrest.Style[HtmlTextWriterStyle.Display] = "block";
                                    //}
                                    //else
                                    //{
                                    //    HLkCounty.Style[HtmlTextWriterStyle.Display] = "block";
                                    //    HLkCounty.NavigateUrl = "~/backroom/Courts.aspx?ALRCourt=" + dRow["ALRCourt"].ToString();
                                    //    lblCountyOfArrest.Style[HtmlTextWriterStyle.Display] = "none";
                                    //}
                                    ////7150 end
                                    ////Asad Ali 8153 09/20/2010 Move AlR Hearing Required in Pre Hire Section 
                                    ////Waqas 6342 08/12/2009 ALR observing officer required
                                    //if (dRow["IsALRHearingRequired"].ToString().ToLower() == "false")
                                    //{
                                    //    tr_ALRRequiredWhy.Style[HtmlTextWriterStyle.Display] = "block";
                                    //    tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "none";
                                    //    txtALRHearingRequiredAnswer.Text = dRow["ALRHearingRequiredAnswer"].ToString();
                                    //}
                                    //else
                                    //{

                                    //    tr_ALRRequiredWhy.Style[HtmlTextWriterStyle.Display] = "none";
                                    //    tr_postHireQuestions2.Style[HtmlTextWriterStyle.Display] = "block";
                                    //    txtALROfficerName.Text = dRow["ALROfficerName"].ToString();
                                    //    txtALROfficerBadgeNumber.Text = dRow["ALROfficerbadge"].ToString();
                                    //    txtALRPrecinct.Text = dRow["ALRPrecinct"].ToString();
                                    //    txtALRAddress.Text = dRow["ALROfficerAddress"].ToString();
                                    //    txtALRCity.Text = dRow["ALROfficerCity"].ToString();
                                    //    ddl_ALRState.SelectedValue = dRow["ALROfficerStateID"].ToString();
                                    //    txtALRZip.Text = dRow["ALROfficerZip"].ToString();

                                    //    string contact11 = dRow["ALROfficerContactNumber"].ToString().Trim();
                                    //    if (contact11.Length > 0)
                                    //    {
                                    //        if (contact11.Length >= 3)
                                    //            txt_Attacc11.Text = contact11.Substring(0, 3);
                                    //        if (contact11.Length >= 6)
                                    //            txt_Attcc12.Text = contact11.Substring(3, 3);
                                    //        if (contact11.Length >= 10)
                                    //            txt_Attcc13.Text = contact11.Substring(6, 4);

                                    //        if (contact11.Length > 10)
                                    //            txt_Attcc14.Text = contact11.Substring(10);

                                    //    }
                                    //    txtALRArrestingAgency.Text = dRow["ALRArrestingAgency"].ToString();

                                    //    //-----
                                    //    if (dRow["ALRMileage"].ToString().Trim() != "0")
                                    //    {
                                    //        txtALROfficerMileage.Text = dRow["ALRMileage"].ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        txtALROfficerMileage.Text = "";
                                    //    }


                                    //    //Waqas 6342 08/13/2009 observing officer
                                    //    if (dRow["IsALRArrestingObservingSame"].ToString() == "")
                                    //    {
                                    //        rBtnArrOffObsOffYes.Checked = false;
                                    //        rBtnArrOffObsOffNo.Checked = false;
                                    //    }
                                    //    else if (dRow["IsALRArrestingObservingSame"].ToString().ToLower() == "false")
                                    //    {
                                    //        rBtnArrOffObsOffYes.Checked = false;
                                    //        rBtnArrOffObsOffNo.Checked = true;

                                    //        tr_ObservingOfficer.Style[HtmlTextWriterStyle.Display] = "block";

                                    //        txtALROBSOfficerName.Text = dRow["ALRObservingOfficerName"].ToString();
                                    //        txtALROBSOfficerBadgeNumber.Text = dRow["ALRObservingOfficerBadgeNo"].ToString();
                                    //        txtALROBSPrecinct.Text = dRow["ALRObservingOfficerPrecinct"].ToString();
                                    //        txtALROBSAddress.Text = dRow["ALRObservingOfficerAddress"].ToString();
                                    //        txtALROBSCity.Text = dRow["ALRObservingOfficerCity"].ToString();
                                    //        ddl_ALROBSState.SelectedValue = dRow["ALRObservingOfficerState"].ToString();
                                    //        txtALROBSZip.Text = dRow["ALRObservingOfficerZip"].ToString();

                                    //        string OBScontact11 = dRow["ALRObservingOfficerContact"].ToString().Trim();
                                    //        if (OBScontact11.Length > 0)
                                    //        {
                                    //            if (OBScontact11.Length >= 3)
                                    //                txt_OBSAttacc11.Text = OBScontact11.Substring(0, 3);
                                    //            if (OBScontact11.Length >= 6)
                                    //                txt_OBSAttcc12.Text = OBScontact11.Substring(3, 3);
                                    //            if (OBScontact11.Length >= 10)
                                    //                txt_OBSAttcc13.Text = OBScontact11.Substring(6, 4);

                                    //            if (OBScontact11.Length > 10)
                                    //                txt_OBSAttcc14.Text = OBScontact11.Substring(10);

                                    //        }
                                    //        txtALROBSArrestingAgency.Text = dRow["ALRObservingOfficerArrestingAgency"].ToString();

                                    //        if (dRow["ALRObservingOfficerMileage"].ToString().Trim() != "0")
                                    //        {
                                    //            txtALROBSOfficerMileage.Text = dRow["ALRObservingOfficerMileage"].ToString();
                                    //        }
                                    //        else
                                    //        {
                                    //            txtALROBSOfficerMileage.Text = "";
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        rBtnArrOffObsOffYes.Checked = true;
                                    //        rBtnArrOffObsOffNo.Checked = false;

                                    //        tr_ObservingOfficer.Style[HtmlTextWriterStyle.Display] = "none";
                                    //    }




                                    //    if (dRow["ALRIsIntoxilyzerTakenFlag"].ToString().Trim() == "1") //Yes
                                    //    {
                                    //        tr_IntoxilyzerResults.Style[HtmlTextWriterStyle.Display] = "block";

                                    //        if (dRow["ALRIntoxilyzerResult"].ToString().Trim().ToLower() == "pass")
                                    //        {
                                    //            rbtn_IntoxResultPass.Checked = true;
                                    //            rbtn_IntoxResultFail.Checked = false;




                                    //            //txtBTOFirstName.Text = dRow["ALRBTOFirstName"].ToString();
                                    //            //txtBTOLastName.Text = dRow["ALRBTOLastName"].ToString();



                                    //        }
                                    //        else if (dRow["ALRIntoxilyzerResult"].ToString().Trim().ToLower() == "fail")
                                    //        {
                                    //            rbtn_IntoxResultPass.Checked = false;
                                    //            rbtn_IntoxResultFail.Checked = true;
                                    //            //Asad Ali 7991 06/14/2010 We need to display BTO officer name and BTS Officer name question show up even when INtoxilyzer result is 'Fail' or it's 'Pass' 
                                    //            tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "block";
                                    //            tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "block";
                                    //        }
                                    //        else
                                    //        {
                                    //            rbtn_IntoxResultPass.Checked = false;
                                    //            rbtn_IntoxResultFail.Checked = false;
                                    //            tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "none";
                                    //            tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "none";
                                    //        }

                                    //        //Asad Ali 7991 07/31/2010 fix issue when pass check not displaying values
                                    //        tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "block";
                                    //        //Yasir Kamal 7150 01/01/2010 ALR BTS, BTO modified.
                                    //        tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "block";
                                    //        // tr_BreathTestSame.Style[HtmlTextWriterStyle.Display] = "block";



                                    //        if (dRow["ALROfficerName"].ToString().Trim().ToLower() == dRow["ALRBTOLastName"].ToString().Trim().ToLower())
                                    //        {
                                    //            rbBTOArresting.Checked = true;
                                    //            rbBTOObserving.Checked = false;

                                    //        }
                                    //        else if (dRow["ALRObservingOfficerName"].ToString().Trim().ToLower() == dRow["ALRBTOLastName"].ToString().Trim().ToLower())
                                    //        {
                                    //            rbBTOArresting.Checked = false;
                                    //            rbBTOObserving.Checked = true;
                                    //            txtBTOName.Enabled = false;
                                    //        }
                                    //        else if (dRow["ALRBTOLastName"].ToString().Trim() == "")
                                    //        {
                                    //            rblBTOName.Checked = true;
                                    //            txtBTOName.Enabled = true;
                                    //        }
                                    //        else
                                    //        {
                                    //            rblBTOName.Checked = true;
                                    //            txtBTOName.Text = dRow["ALRBTOLastName"].ToString();
                                    //            txtBTOName.Enabled = true;
                                    //        }
                                    //        if (dRow["ALRBTSFirstName"].ToString().Trim() != string.Empty)
                                    //        {
                                    //            txtBTSFirstName.Text = dRow["ALRBTSFirstName"].ToString();
                                    //        }
                                    //        if (dRow["ALRBTSLastName"].ToString().Trim() != string.Empty)
                                    //        {
                                    //            txtBTSLastName.Text = dRow["ALRBTSLastName"].ToString().Trim();
                                    //        }


                                    //    }
                                    //    else
                                    //    {
                                    //        tr_IntoxilyzerResults.Style[HtmlTextWriterStyle.Display] = "none";
                                    //        tr_BreathTestOperator.Style[HtmlTextWriterStyle.Display] = "none";
                                    //        tr_BreathTestSupervisor.Style[HtmlTextWriterStyle.Display] = "none";
                                    //    }
                                    //    //7150 end
                                    //}
                                }
                                else
                                {
                                    //Hafiz 10288 07/19/2012 commented the below section
                                    //tr_postHireQuestions.Style[HtmlTextWriterStyle.Display] = "none";
                                    //End
                                    //Asad Ali 8153 09/23/2010  move in pre hire section 
                                    //tr_ALRhearingRequired.Style[HtmlTextWriterStyle.Display] = "none";


                                    lnkEmailCaseSummary.Visible = true;
                                    lblEmailCaseSummary.Visible = true;
                                }
                            }
                        }

                        // Agha Usman 07/22/2008 - Remove try catch.

                        lbldetailcount.Text = ds_CaseDetail.Tables[0].Rows.Count.ToString();

                        // GETTING & SETTING SPEEDING INFORMATION.....
                        bool bSpeed = false;
                        foreach (DataGridItem ItemX in dgViolationInfo.Items)
                        {
                            if (((Label)(ItemX.FindControl("Label4"))).Text.ToUpper().IndexOf("SPEED") > -1)
                            {
                                bSpeed = true;
                            }
                            // Rab Nawaz Khan 10330 07/03/2012 Checking the Traffic Cases. . . 
                            if (isClientPaymentFound || Convert.ToInt32(dRow["Activeflag"].ToString().Trim()) == 0)
                            {
                                if (Convert.ToInt32(dRow["CaseTypeID"].ToString()) == 1)
                                {
                                    // Rab Nawaz Khan 10330 07/03/2012 Display the Arr Day Of column in Calculation grid if status is Arraignment and court date is current date
                                    if (((Label)(ItemX.FindControl("lbl_status"))).Text.ToUpper().IndexOf("ARR") > -1)
                                    {
                                        if (
                                            Convert.ToDateTime(((Label)(ItemX.FindControl("lbl_courtdate"))).Text).
                                                ToShortDateString() == DateTime.Now.ToShortDateString())
                                        {
                                            ArrStatus = true;
                                            hf_ArrDayOf.Value = "1";
                                        }
                                        else
                                        {
                                            ArrStatus = false;
                                            hf_ArrDayOf.Value = "0";
                                        }
                                    }
                                }
                            }


                        }
                        // Rab Nawaz Khan 10330 07/03/2012 Checking the Traffic Cases. . . 
                        if (isClientPaymentFound || Convert.ToInt32(dRow["Activeflag"].ToString().Trim()) == 0)
                        {
                            if (Convert.ToInt32(dRow["CaseTypeID"].ToString()) == 1)
                            {
                                if (ArrStatus || Convert.ToBoolean(dRow["ArraignmentStatusFlag"]))
                                {
                                    hf_ArrDayOf.Value = "1";
                                    td_dayOfArr.Style["Display"] = "block";
                                    td_dayOfArrValue.Style["Display"] = "block";
                                }
                                else
                                {
                                    hf_ArrDayOf.Value = "0";
                                    td_dayOfArr.Style["Display"] = "none";
                                    td_dayOfArrValue.Style["Display"] = "none";
                                }
                                if (bSpeed == true)
                                {
                                    // Rab Nawaz Khan 10330
                                    if (Convert.ToInt32(dRow["SpeedingID"]) == 6)
                                    {
                                        td_speeding.Style["Display"] = "block";
                                        td_speedingValue.Style["Display"] = "block";
                                        tr_Speeding.Style["display"] = "table-row";

                                    }
                                    else
                                    {
                                        td_speeding.Style["Display"] = "none";
                                        td_speedingValue.Style["Display"] = "none";
                                        tr_Speeding.Style["display"] = "none";
                                    }
                                    // End 10330
                                    ddl_Speeding.Enabled = ddl_cSpeeding.Enabled = true;
                                    tr_Speeding.Style["display"] = "table-row";
                                    tr_SpeedingViolations.Style["display"] = "table-row";
                                }
                                else
                                {
                                    ddl_Speeding.Enabled = ddl_cSpeeding.Enabled = false;
                                    td_speeding.Style["Display"] = "none";
                                    td_speedingValue.Style["Display"] = "none";
                                    tr_Speeding.Style["display"] = "none";
                                    tr_SpeedingViolations.Style["display"] = "none";
                                }
                            }
                        }
                        //Sabir Khan 5160 11/20/2008 Find Speeding ID in dropdown value if exist then set else 0 value as been set to dropdown...
                        if (ddl_cSpeeding.Items.FindByValue(dRow["SpeedingID"].ToString().Trim()) != null)
                        {
                            ddl_Speeding.SelectedValue = ddl_cSpeeding.SelectedValue = dRow["SpeedingID"].ToString().Trim();
                        }
                        else
                        {
                            ddl_Speeding.SelectedValue = ddl_cSpeeding.SelectedValue = "0";
                        }



                        lblSpeed.InnerText = ddl_Speeding.SelectedItem.Text;
                        txtSpeedVal.Text = "1";
                        if (ddl_Speeding.SelectedValue == "0" || ddl_cSpeeding.SelectedValue == "0")
                        {
                            txtSpeedVal.Text = "0";
                            int SpeedingId = ClsCase.GetSpeedingId(ClsCase.TicketID);
                            if (SpeedingId == -1)
                            {
                                ddl_Speeding.SelectedValue = ddl_cSpeeding.SelectedValue = "0";
                            }
                            else
                            {
                                ddl_Speeding.SelectedValue = ddl_cSpeeding.SelectedValue = SpeedingId.ToString();
                            }
                        }
                        // Abbas Qamar 10114 04-04-2012 Checking HCCC,Fortbend,Montgeomry Criminal Courts
                        int isCriminal = Convert.ToInt32(ds_Case.Tables[0].Rows[0]["IsCriminalCourt"]);
                        if (isCriminal > 0)
                        {

                            trDL.Visible = true;
                            trSSN.Visible = true;
                            txt_dlstr.Text = ds_Case.Tables[0].Rows[0]["DLNumber"].ToString();
                            //if(ds_Case.Tables[0].Rows[0]["SSNumber"].)
                            string ssn = ds_Case.Tables[0].Rows[0]["SSNumber"].ToString();
                            string ssnTypeId = ds_Case.Tables[0].Rows[0]["SSNTypeId"].ToString();
                            string ssnQuestionOther = ds_Case.Tables[0].Rows[0]["SSNOtherQuestion"].ToString();
                            int DlState = Convert.ToInt32(ds_Case.Tables[0].Rows[0]["DLState"]);

                            //RadioButton1.Checked = true;
                            if (txt_dlstr.Text != "")
                            {

                                RadioButton1.Checked = true;
                                // Rab Nawaz Khan 11388 09/05/2013 Fix the Exception of "ddl_dlState has a SelectedValue Whisi is invalid because it does not exist in the list of items". . . 
                                if (DlState == 0)
                                    ddl_dlState.SelectedValue = "45";
                                else
                                    ddl_dlState.SelectedValue = Convert.ToString(DlState);
                                tdDrivingLicense.Style.Add("display", "block");


                            }
                            else
                            {
                                tdDrivingLicense.Style.Add("display", "none");
                                RadioButton2.Checked = true;
                            }

                            if (ssnTypeId == "0" || ssnTypeId == "")
                            {

                                RadioButton3.Checked = true;
                                txt_SSN.Text = ssn;
                                // Hafiz 10288 07/19/2012  commented the below line
                                //tdSSN.Style.Add("display", "block");
                                tdDDLSSN.Style.Add("display", "none");
                                tdOther.Style.Add("display", "none");




                            }
                            else if (ssnTypeId.Length > 0 && ssnTypeId != "6")
                            {
                                RadioButton4.Checked = true;
                                tdDDLSSN.Style.Add("display", "block");
                                tdSSN.Style.Add("display", "none");
                                txt_SSN.Text = "";
                                DDLSSNQuestion.SelectedValue = ssnTypeId;
                                tdOther.Style.Add("display", "none");


                            }
                            else if (ssnTypeId == "6")
                            {
                                tdDDLSSN.Style.Add("display", "block");
                                tdOther.Style.Add("display", "block");
                                RadioButton4.Checked = true;
                                tdSSN.Style.Add("display", "none");
                                txt_SSNOther.Text = ssnQuestionOther;
                                DDLSSNQuestion.SelectedValue = ssnTypeId;


                            }
                            //else if (ssn.Length > 0 && ssnTypeId == "6")
                            //{
                            //    tdDDLSSN.Style.Add("display", "block");
                            //    RadioButton3.Checked = true;
                            //    txt_SSN.Text = ssn;
                            //    tdSSN.Style.Add("display", "none");
                            //    DDLSSNQuestion.SelectedValue = ssnTypeId;

                            //}

                        }
                        else
                        {
                            trSSN.Visible = false;
                            trDL.Visible = false;
                        }
                        // End 10114 

                        // Rab Nawaz Khan 11473 10/23/2013 Added to display the Caller Id info. . . 
                        //txt_callerId.Text = dRow["CallerId"].ToString().Trim();
                        //chk_unknown.Checked = Convert.ToBoolean(dRow["isUnknown"].ToString().Trim());
                        //if (chk_unknown.Checked)
                        //    txt_callerId.Enabled = false;
                        //else
                        //    txt_callerId.Enabled = true;

                    }
                    else
                    {
                        lbl_Message.Visible = true;
                        lbl_Message.Text = "Information not retrived for the given ticket";
                    }
                }
                else
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = "Information not retrived for the given ticket";
                }

                CheckReadNotes(Convert.ToInt32(ClsCase.TicketID));

                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if ((test.Text.Length > 1) || (lbl_Message.Text.Length > 1 && lbl_Message.Visible))
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;

                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                if (lblbadnumber.Visible || lblbademail.Visible || lblProblemClient.Visible || lblHmcLubMesg1.Visible || lblHmcLubMesg2.Visible)
                {
                    tr_InfoMessages.Visible=true;
                    if (lblbadnumber.Visible)
                        tr_lblbadnumber.Visible=true;
                    if (lblbademail.Visible)
                        tr_lblbademail.Visible=true;
                    if (lblProblemClient.Visible)
                        tr_lblProblemClient.Visible=true;;
                    if (lblHmcLubMesg1.Visible)
                        tr_lblHmcLubMesg1.Visible=true;
                    if (lblHmcLubMesg2.Visible)
                        tr_lblHmcLubMesg2.Visible=true;
                }
                else
                   tr_InfoMessages.Visible=false;


                DataTable dtFindBy = ClsCase.GetFindBy();
                if (dtFindBy != null && dtFindBy.Rows.Count > 0)
                {
                    string FindByValue = dtFindBy.Rows[0]["FindByID"].ToString();
                    string FindbyText = dtFindBy.Rows[0]["FindBy"].ToString();
                    if (ddlFindBy.Items.FindByValue(FindByValue) == null)
                    {
                        ddlFindBy.SelectedValue = "0";
                        tdOtherFindBy.Style.Add("display", "block");
                        txtOtherFindBy.Text = FindbyText;
                    }
                    else
                    {
                        ddlFindBy.SelectedValue = FindByValue;
                        tdOtherFindBy.Style.Add("display", "none");
                    }
                }


            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                tr_msgsForCIDandCuase.Visible=true;

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Updates or Insert case info
        private void UpdateCase()
        {
            try
            {

                if (lblisNew.Text == "0")
                {
                    AssignValues();

                    // Noufil 6138 08/17/2009 Update language and add in database
                    if (ddl_language.SelectedValue == "0")
                    {
                        languages.InsertLanguage(dd_OtherLanguage.SelectedItem.ToString().ToUpper().Trim());
                        FillLanguages(new DataTable());
                    }

                    if (ClsCase.UpdateCase() == true)
                    {
                        //Sabir Khan 11509 11/13/2013 Save how did you find by value in DB.
                        if (ddlFindBy.SelectedItem.Value != "-1")
                        {
                            if (ddlFindBy.SelectedItem.Value == "0")
                            {
                                ClsCase.SaveFindBy(Convert.ToInt32(ddlFindBy.SelectedItem.Value), txtOtherFindBy.Text);
                            }
                            else
                            {
                                ClsCase.SaveFindBy(Convert.ToInt32(ddlFindBy.SelectedItem.Value), ddlFindBy.SelectedItem.Text);
                            }
                        }
                        // Rab Nawaz Khan 11473 10/23/2013 Added for Caller ID information . . . 
                        //ClsCase.UpdateCallerIdInfo();
                        //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                        if ((rbtn_FollowUp_Yes.Enabled || rbtn_cFollowUp_Yes.Enabled || rbtn_vFollowUp_Yes.Enabled)
                             && (rbtn_FollowUp_No.Enabled || rbtn_cFollowUp_No.Enabled || rbtn_vFollowUp_No.Enabled)
                             && (cal_CallBack.Enabled || cal_cCallBack.Enabled || cal_vCallBack.Enabled)
                            )
                        {
                            if ((rbtn_FollowUp_Yes.Checked || rbtn_cFollowUp_Yes.Checked || rbtn_vFollowUp_Yes.Checked)
                                || (rbtn_FollowUp_No.Checked || rbtn_cFollowUp_No.Checked || rbtn_vFollowUp_No.Checked)
                                )
                            {
                                InsertOrUpdateFollowUpInfo();
                            }
                        }

                        if (Convert.ToInt32(ViewState["CaseTypeID"]) != ClsCase.CaseType)
                            UpdateViolationSRV1.ResetControl();


                        if (ViewState["vFromCal"].ToString() == "false")
                            DisplayInfo();
                    }
                    //Waqas 5771 04/20/2009
                    AssignContactValues();
                    ClsContact.UpdateMatterInfoToContact();
                }
                else if (lblisNew.Text == "1")
                {
                    AssignValues();
                    //insert case Info
                    ViewState["vTicketID"] = ClsCase.AddCase();
                    ClsCase.TicketID = Convert.ToInt32(ViewState["vTicketID"]);
                    txtTTM.Text = ViewState["vTicketID"].ToString();//Added by Azee..
                    if (ClsCase.TicketID != 0)
                    {
                        ViewState["vTicketID"] = ClsCase.TicketID.ToString();
                        ViewState["vSearch"] = "1";
                        //
                        ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                        TextBox txt1 = (TextBox)am.FindControl("txtid");
                        txt1.Text = ViewState["vTicketID"].ToString();
                        TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                        txt2.Text = Convert.ToString(ViewState["vSearch"]);
                        //
                        lblisNew.Text = "0";

                        //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                        if ((rbtn_FollowUp_Yes.Enabled || rbtn_cFollowUp_Yes.Enabled || rbtn_vFollowUp_Yes.Enabled)
                             && (rbtn_FollowUp_No.Enabled || rbtn_cFollowUp_No.Enabled || rbtn_vFollowUp_No.Enabled)
                             && (cal_CallBack.Enabled || cal_cCallBack.Enabled || cal_vCallBack.Enabled)
                            )
                        {
                            if ((rbtn_FollowUp_Yes.Checked || rbtn_cFollowUp_Yes.Checked || rbtn_vFollowUp_Yes.Checked)
                                || (rbtn_FollowUp_No.Checked || rbtn_cFollowUp_No.Checked || rbtn_vFollowUp_No.Checked)
                                )
                            {
                                InsertOrUpdateFollowUpInfo();
                            }
                        }
                        // Rab Nawaz Khan 11473 10/23/2013 Added for Caller ID information . . . 
                        //ClsCase.UpdateCallerIdInfo();

                        //Sabir Khan 11509 11/13/2013 Save how did you find by value in DB. 
                        if (ddlFindBy.SelectedItem.Value != "-1")
                        {
                            if (ddlFindBy.SelectedItem.Value == "0")
                            {
                                ClsCase.SaveFindBy(Convert.ToInt32(ddlFindBy.SelectedItem.Value), txtOtherFindBy.Text);
                            }
                            else
                            {
                                ClsCase.SaveFindBy(Convert.ToInt32(ddlFindBy.SelectedItem.Value), ddlFindBy.SelectedItem.Text);

                            }
                        }

                        lnkbtn_InsertViolation.Attributes.Add("OnClick", "return openwindow('0','1','" + UpdateViolationSRV1.ClientID + "');");

                        if (ViewState["vFromCal"].ToString() == "false")
                            DisplayInfo();

                    }
                }



                ViewState["vFromCal"] = "false";
                btn_Calculate.Enabled = true;
                btn_Lock.Enabled = true;
                hf_contacts.Value = "1";//added by azee for Javascript function called on body load..

                //Waqas 6895 11/02/2009 show progress on update
                tblProgressUpdatePage.Style["Display"] = "none";
               
            }
            catch (Exception ex)
            {
                lbl_Message.Visible = true;
                lbl_Message.Text = ex.Message;
                ViewState["vFromCal"] = "false";
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                tr_msgsForCIDandCuase.Visible=true;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetValues()
        {
            if (rdbtn_AccidentYes.Checked || rdbtn_cAccidentYes.Checked)
                AFlag = true;
            else
                AFlag = false;

            if (rdbtn_CDLYes.Checked || rdbtn_cCDLYes.Checked)
                CFlag = true;
            else
                CFlag = false;

            switch (mvFeeQuestion.ActiveViewIndex)
            {
                case 0: Speeding = Convert.ToInt32(ddl_cSpeeding.SelectedValue); break;
                case 1: Speeding = 0; break;
                default: Speeding = Convert.ToInt32(ddl_Speeding.SelectedValue); break;
            }

            if (txt_InitialAdjustment.Visible == true)
            {
                if (txt_InitialAdjustment.Text == "")
                    IAdj = 0;
                else
                    IAdj = Convert.ToDouble(txt_InitialAdjustment.Text);
            }
            // Fix By Zeeshan ( Bug Assigned By Faraz )
            //else if(lbl_InitialAdjustment.Visible==true)
            else if (lbl_InitialAdjustment.Visible == false)
            {
                IAdj = Convert.ToDouble(txt_iadj.Text);
            }
            //Sabir Khan 6851 12/04/2009 getting initial adjustment value...
            else if (lbl_InitialAdjustment.Visible == true)
            {

                IAdj = Convert.ToDouble(general.CurrencyToDouble(lbl_InitialAdjustment.Text.Remove(lbl_InitialAdjustment.Text.IndexOf('-'))));
            }
            FAdj = 0;
            if (txt_FinalAdjustment.Visible == true)
            {
                if (txt_FinalAdjustment.Text == "")
                {
                    FAdj = 0;
                }
                else
                {
                    FAdj = Convert.ToDouble(txt_FinalAdjustment.Text);
                }
            }

            // tahir 4786 10/08/2008
            PayPlanFlag = rbtn_vplanyes.Checked;
        }

        private void addnewticket()
        {

            DataSet ds = ClsCaseDetail.GetMaxTVIDByTicketID(Convert.ToInt32(ClsCase.TicketID));
            int tvid = Convert.ToInt32(ds.Tables[0].Rows[0]["tvid"].ToString());
            if (tvid != 0)
            {
                //DisplayInfoNew(tvid);
            }
            else
            {
                lbl_Message.Text = "Ticket Information Not Retrive";
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
            }
        }

        // Update Panel Simplified Procedure
        //Nasir 4463 01/28/2009 Correct Name from courtID to FirmID
        //ozair 4463 01/29/2009 refactored the code
        /// <summary>
        /// Get the firm name by provided Firm ID if SulloLaw then return Empty
        /// </summary>
        /// <param name="FirmID"></param>
        /// <returns></returns>
        private string getFirmName(int FirmID)
        {
            string result = String.Empty;
            if (FirmID > 3000)
            {
                try
                {
                    DataSet ds = ClsFirms.GetFirmInfo(FirmID);
                    result = "\"" + ds.Tables[0].Rows[0]["FirmName"].ToString().ToUpper() + "\" CLIENT";
                }
                catch (Exception ex)
                {
                    lbl_Message.Visible = true;
                    lbl_Message.Text = ex.Message;
                    // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                    tr_msgsForCIDandCuase.Visible=true;
                    bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }

            return result;
        }

        private void GetMailerID(int TicketID)
        {
            string[] keys = { "@TicketID" };
            object[] values = { TicketID };
            DataTable dt = clsdb.Get_DT_BySPArr("usp_hts_get_mailer_id", keys, values);
            int letterid;
            if (dt.Rows.Count > 0)
                letterid = Convert.ToInt32(dt.Rows[0]["MailerID"]);
            else
                letterid = 0;

            if (letterid == 0)
            {
                lbl_LetterID.Visible = false;
            }
            else
            {
                lbl_LetterID.Text = " Letter ID: " + letterid.ToString();
                lbl_LetterID.Visible = true;
            }
        }

        private void InsertOrUpdateFollowUpInfo()
        {
            try
            {
                DateTime FollowUpdate;
                int Followup = 0;
                //Zeeshan Ahmed 3979 05/16/2008 Refactor Code For Criminal And Civil Cases
                switch (mvFeeQuestion.ActiveViewIndex)
                {
                    //Criminal Followup
                    case 0:
                        if (rbtn_cFollowUp_Yes.Checked)
                            Followup = 1;

                        FollowUpdate = cal_cCallBack.SelectedDate;
                        break;
                    //Civil Followup
                    case 1:
                        if (rbtn_vFollowUp_Yes.Checked)
                            Followup = 1;

                        FollowUpdate = cal_vCallBack.SelectedDate;
                        break;
                    //Traffic Folloup
                    default:
                        if (rbtn_FollowUp_Yes.Checked)
                            Followup = 1;

                        FollowUpdate = cal_CallBack.SelectedDate;
                        break;
                }

                string[] keys = { "@TicketID", "@CallbackDate", "@followup" };
                object[] values = { ClsCase.TicketID, FollowUpdate, Followup };
                clsdb.ExecuteSP("USP_HTS_INSERTORUPDATE_FOLLOWUP", keys, values);

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                tr_msgsForCIDandCuase.Visible=true;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void GetCallbackInfo()
        {
            try
            {
                string[] keys = { "@TicketID" };
                object[] values = { Request.QueryString["caseNumber"].ToString() };
                DataTable dt = clsdb.Get_DT_BySPArr("usp_get_callback_status", keys, values);
                if (Convert.ToInt16(dt.Rows[0]["ViolationQuote"]) == 0)
                {
                    rbtn_FollowUp_Yes.Enabled = true;
                    rbtn_FollowUp_No.Enabled = true;
                    ddl_ContactType.Enabled = true;
                    td_callback_days.Style[HtmlTextWriterStyle.Display] = "none";
                    //ddl_FollowUp_Days.Style[HtmlTextWriterStyle.Display] = "none";
                    //label_123.Style[HtmlTextWriterStyle.Display] = "none";
                }
                else
                {
                    rbtn_FollowUp_Yes.Enabled = false;
                    rbtn_FollowUp_No.Enabled = false;
                    //ddl_FollowUp_Days.Enabled = false;
                    cal_CallBack.Enabled = false;
                    td_callback_days.Style[HtmlTextWriterStyle.Display] = "none";
                    //ddl_FollowUp_Days.Style[HtmlTextWriterStyle.Display] = "none";
                    //label_123.Style[HtmlTextWriterStyle.Display] = "none";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                tr_msgsForCIDandCuase.Visible=true;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Check Weather Read Note Flag Exist or Not
        /// </summary>
        /// <param name="ticketid"></param>
        private void CheckReadNotes(int ticketid)
        {
            bool checkflag = false;
            checkflag = notes.CheckReadNoteComments(ticketid);
            if (checkflag == true)
            {
                chkbtn_read.Checked = true;
                chkbtn_Delete.Visible = true;
                chkbtn_read.Visible = false;
                read.Style[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                chkbtn_read.Visible = true;
                chkbtn_Delete.Visible = false;
                chkbtn_read.Checked = false;
                read.Style[HtmlTextWriterStyle.Display] = "none";
            }
        }

        private DataTable GetDistinctRecords(DataTable dtCourtViolation, DataTable dtCaseViolation)
        {

            DataTable dtTemp = dtCourtViolation.Clone();
            for (int i = 0; i < dtCourtViolation.Rows.Count; i++)
            {
                for (int j = 0; j < dtCaseViolation.Rows.Count; j++)
                {
                    if (Convert.ToString(dtCourtViolation.Rows[i]["CauseNumber"]) == Convert.ToString(dtCaseViolation.Rows[j]["CauseNumber"]))
                    {
                        DataRow dr = dtTemp.NewRow();
                        dr[0] = dtCourtViolation.Rows[i][0];
                        dr[1] = dtCourtViolation.Rows[i][1];
                        dtTemp.Rows.Add(dr);
                        break;
                    }
                }
            }
            return dtTemp;

        }

        //Zeeshan Ahmed 3979 05/15/2008 Set Fee Question Panel Based On Case Type
        private void SetFeeQuestions(int caseType)
        {
            // Noufil 4447 08/18/2008 active flag added to decide whether case is for prospect (activeflag 0) or client (activeflag 1)
            string txtflag = (Convert.ToInt32(lbl_ActiveFlag.Text) == 0 ? "Prospect " : "Client");
            switch (caseType)
            {
                case CaseType.Civil:
                    dgViolationInfo.Columns[0].Visible = true;
                    dgViolationInfo.Columns[2].Visible = true;
                    dgViolationInfo.Columns[1].Visible = false;
                    mvFeeQuestion.ActiveViewIndex = 1;                    
                    dgViolationInfo.Columns[13].Visible = false;
                    dgViolationInfo.Columns[14].Visible = false;
                    lblClientType.Text = "Civil " + txtflag;
                    break;
                case CaseType.Criminal:
                    // Noufil 4369 07/17/2008 Hiding Ticket number column and moving hyperlink to Cause Number
                    dgViolationInfo.Columns[0].Visible = false;
                    dgViolationInfo.Columns[2].Visible = false;
                    dgViolationInfo.Columns[1].Visible = true;
                    lblClientType.Text = "Criminal " + txtflag;
                    mvFeeQuestion.ActiveViewIndex = 0;                    
                    break;

                // Noufil
                case CaseType.FamilyLaw:
                    //Waqas 5864 07/30/2009 Changes For Family
                    dgViolationInfo.Columns[0].Visible = false;
                    dgViolationInfo.Columns[2].Visible = false;
                    dgViolationInfo.Columns[1].Visible = true;
                    mvFeeQuestion.ActiveViewIndex = 1;                    
                    dgViolationInfo.Columns[13].Visible = false;
                    dgViolationInfo.Columns[14].Visible = false;
                    lblClientType.Text = "Family Law " + txtflag;
                    break;

                default:
                    dgViolationInfo.Columns[0].Visible = true;
                    dgViolationInfo.Columns[2].Visible = true;
                    dgViolationInfo.Columns[1].Visible = false;
                    lblClientType.Text = "Traffic " + txtflag;
                    mvFeeQuestion.ActiveViewIndex = 2;                    
                    break;
            }
        }

        //Zeeshan Ahmed 3979 05/15/2008 Used To Set Accident CDL & BondFlag
        private void SetFlag(RadioButton rbtnYes, RadioButton rbtnNo, int flagValue)
        {
            if (flagValue == 1)
            {
                rbtnNo.Checked = false;
                rbtnYes.Checked = true;
            }
            else if (flagValue == 0)
            {
                rbtnNo.Checked = true;
                rbtnYes.Checked = false;
            }
        }

        //Zeeshan Ahmed 3979 05/15/2008 Used To Set Call Back Information
        //Yasir Kamal 7355 01/29/2010 set call back date on friday for criminal & familiy
        private void SetCallBack(int ActiveFlag, int FollowUpFlag, DateTime CallBackDate, RadioButton btnYes, RadioButton btnNo, HtmlControl tdCallBackdays,  int caseTypeId)
        {
            if (ActiveFlag == 0)
            {
                btnYes.Enabled = true;
                btnNo.Enabled = true;
                cal_vCallBack.Enabled = true;
                //Sabir Khan 4636 08/19/2008  set the Case type enable property to true for prospect
                ddl_CaseType.Enabled = true;

                if (FollowUpFlag == 0)
                {
                    btnNo.Checked = true;
                    btnYes.Checked = false;
                    tdCallBackdays.Style[HtmlTextWriterStyle.Display] = "none";
                }
                else if (FollowUpFlag == 1)
                {
                    btnYes.Checked = true;
                    btnNo.Checked = false;
                    cal_vCallBack.SelectedDate = Convert.ToDateTime(CallBackDate);
                    tdCallBackdays.Style[HtmlTextWriterStyle.Display] = "block";

                }
                else
                {
                    btnYes.Checked = true;
                    btnNo.Checked = false;
                    tdCallBackdays.Style[HtmlTextWriterStyle.Display] = "block";
                    //Yasir Kamal 7355 02/03/2010 set call back for criminal and family cases.
                    //Fahad 11146 06/11/2013 Set tomorrow (Next Day including Saturday) call back for criminal cases.
                    if (caseTypeId == 2)
                    {
                        switch (DateTime.Today.DayOfWeek)
                        {
                            case DayOfWeek.Friday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                            case DayOfWeek.Saturday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(2);
                                break;
                            case DayOfWeek.Sunday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                            case DayOfWeek.Monday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                            case DayOfWeek.Tuesday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                            case DayOfWeek.Wednesday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                            case DayOfWeek.Thursday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                        }
                    }
                    else if (caseTypeId == 4)
                    {
                        switch (DateTime.Today.DayOfWeek)
                        {
                            case DayOfWeek.Friday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(7);
                                break;
                            case DayOfWeek.Saturday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(6);
                                break;
                            case DayOfWeek.Sunday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(5);
                                break;
                            case DayOfWeek.Monday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(4);
                                break;
                            case DayOfWeek.Tuesday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(3);
                                break;
                            case DayOfWeek.Wednesday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(2);
                                break;
                            case DayOfWeek.Thursday:
                                cal_vCallBack.SelectedDate = DateTime.Now.AddDays(1);
                                break;
                        }
                    }
                    else if (DateTime.Now.Date.AddDays(3).DayOfWeek == DayOfWeek.Sunday)
                        cal_vCallBack.SelectedDate = DateTime.Now.Date.AddDays(4);
                    else
                        cal_vCallBack.SelectedDate = DateTime.Now.Date.AddDays(3);
                }
            }
            else
            {
                btnYes.Enabled = false;
                btnNo.Enabled = false;
                cal_vCallBack.Enabled = false;
                //Sabir Khan 4636 08/19/2008  set the Case type enable property to false for Clients
                ddl_CaseType.Enabled = false;
            }

        }

        //Zeeshan Ahmed 3979 05/15/2008 Used To Set Late Fee Flag
        private void SetLateFlag(int LateFlag, DataTable violations, RadioButton rbtnYes, RadioButton rbtnNo)
        {
            if (LateFlag != 1)
            {
                // Check for Late Fee 
                DateTime tempdate = DateTime.Parse("1/1/1900");

                foreach (DataRow dr in violations.Rows)
                {
                    DateTime dt = (DateTime)dr["CourtDateMain"];

                    if (dt > DateTime.Now)
                    {
                        if (tempdate.ToShortDateString() != "1/1/1900")
                        {
                            if (tempdate > dt) tempdate = dt;
                        }
                        else
                        {
                            tempdate = dt;
                        }

                    }
                    else if (dt.ToShortDateString() == DateTime.Now.ToShortDateString())
                    {
                        tempdate = dt;
                    }

                }

                if (tempdate.ToShortDateString() == "1/1/1900")
                {
                    rbtnYes.Checked = false;
                    rbtnNo.Checked = true;
                    hf_latefee.Value = "0";

                }
                else
                {
                    if (tempdate.ToShortDateString() == DateTime.Now.ToShortDateString() || DateTime.Now.AddDays(1).ToShortDateString() == tempdate.ToShortDateString())
                    {
                        rbtnYes.Checked = true;
                        rbtnNo.Checked = false;
                        hf_latefee.Value = "1";
                    }

                    else
                    {
                        rbtnNo.Checked = true;
                        rbtnYes.Checked = false;
                        hf_latefee.Value = "0";
                    }
                }
            }

            else
            {
                if (LateFlag == 1)
                {
                    rbtnYes.Checked = true;
                    rbtnNo.Checked = false;
                    //Yasir Kamal 5801 04/24/2009 Fee calculation bug fixed.
                    hf_latefee.Value = "1";
                    //5801 end
                }
                else
                {
                    rbtnNo.Checked = true;
                    rbtnYes.Checked = false;
                    hf_latefee.Value = "0";
                }
            }
        }

        // tahir 4841 09/23/2008 added sorting functionality..
        private void SetAcsDesc(string Val)
        {

            //Nasir 4463 01/28/2009 Checking for existance of viewstate before getting value from it
            if (ViewState["StrExp"] != null)
            {
                StrExp = ViewState["StrExp"].ToString();
            }
            if (ViewState["StrAcsDec"] != null)
            {
                StrAcsDec = ViewState["StrAcsDec"].ToString();
            }

            try
            {
                if (StrExp == Val)
                {
                    if (StrAcsDec == "ASC")
                    {
                        StrAcsDec = "DESC";
                        ViewState["StrAcsDec"] = StrAcsDec;
                    }
                    else
                    {
                        StrAcsDec = "ASC";
                        ViewState["StrAcsDec"] = StrAcsDec;
                    }
                }
                else
                {
                    StrExp = Val;
                    StrAcsDec = "ASC";
                    ViewState["StrExp"] = StrExp;
                    ViewState["StrAcsDec"] = StrAcsDec;

                }

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                clsLogger.ErrorLog(ex);
            }
        }

        // tahir 4841 09/23/2008 added sorting functionality..
        private void SortDataGrid(string Sortnm)
        {
            try
            {
                // Setting dataview for sorting....
                DataView dv = (DataView)Session["dvViol"];
                dv.Sort = Sortnm + " " + StrAcsDec;

                dgViolationInfo.DataSource = dv;
                dgViolationInfo.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                clsLogger.ErrorLog(ex);
            }

        }
        //Abbas Shahid Khwaja 9215 04/26/2011 Add Contact Number parameter 
        //Waqas 5864 06/25/2009 Sending Email
        /// <summary>
        /// This method is used to send email to attorneys and save in docstorage in msg format.
        /// </summary>
        /// <param name="AttorneyName"></param>
        /// <param name="AttorneyEmailAddress"></param>
        /// <param name="ClientName"></param>
        /// <param name="TicketViolationIDs"></param>
        /// <param name="dt"></param>
        /// <param name="PreAttorneyName"></param>
        /// <param name="CaseSummary"></param>
        /// <param name="PaymentMessage"></param>
        /// <param name="ContactNumber"></param>
        private void SendMail(string AttorneyName, string AttorneyEmailAddress, string ClientName, string TicketViolationIDs, DataTable dt, string PreAttorneyName, string CaseSummary, string PaymentMessage, string ContactNumber)
        {
            try
            {
                string subject = "Case Summary";
                string message = GetEmailCaseBody(AttorneyName, AttorneyEmailAddress, ClientName, TicketViolationIDs, dt, PreAttorneyName, CaseSummary, PaymentMessage, ContactNumber);
                //message = lntechNew.Components.MailClass.GetEmailBody(message);   //SAEED-7746-05/15/2010, no need to call this method, cause all message is build inside 'GetEmailCaseBody' method.
                string toUser = AttorneyEmailAddress;
                lntechNew.Components.MailClass.SendMailToAttorneys(message, subject, "", toUser, "");

                string fileName = "ALRCaseEmailSummary" + Session.SessionID + DateTime.Now.ToFileTime().ToString() + ".pdf";

                clsCriminalCase CriminalCase = new clsCriminalCase();
                CriminalCase.TicketID = ClsCase.TicketID;
                CriminalCase.EmpID = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                CriminalCase.SaveCaseEmailSummaryPDF(fileName, message, AttorneyName);
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                // Rab Nawaz Khan 10349 09/13/2013 Added to Remove the extra white space. . . 
                if (lbl_Message.Text.Length > 1 && lbl_Message.Visible)
                    tr_msgsForCIDandCuase.Visible=true;
                else
                    tr_msgsForCIDandCuase.Visible=false;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        /// <summary>
        /// Abbas Shahid Khwaja 9215 04/26/2011 Add Contact Number parameter 
        /// SAEED-7746-05/15/2010
        /// Message body is generated inside this method. It reads an email template from 'EmailTemplates/CaseSummary.htm'. 
        /// Replace variables definded in the template with actual values.
        /// </summary>
        /// <param name="AttorneyName"></param>
        /// <param name="AttorneyEmailAddress"></param>
        /// <param name="ClientName"></param>
        /// <param name="TicketViolationIDs"></param>
        /// <param name="dt"></param>
        /// <param name="PreAttorneyName"></param>
        /// <param name="CaseSummary"></param>
        /// <param name="PaymentMessage"></param>
        /// <param name="ContactNumber"></param>
        /// <returns></returns>
        private string GetEmailCaseBody(string AttorneyName, string AttorneyEmailAddress, string ClientName, string TicketViolationIDs, DataTable dt, string PreAttorneyName, string CaseSummary, string PaymentMessage, string ContactNumber)
        {
            TextReader reader = null;
            StringBuilder sbMessage = new StringBuilder();
            StringBuilder sbViolationRow = new StringBuilder();
            StringBuilder sbAllViolations = new StringBuilder();
            string singleRow = string.Empty;
            string tvID = TicketViolationIDs.Substring(0, TicketViolationIDs.IndexOf(","));
            string caseNum = string.Empty;

            sbViolationRow.Append("<tr>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_VIOLATION_DESC</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_CASE_NUMBER</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_LOCATION</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_NUMBER</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_DATE</span></td>");
            sbViolationRow.Append("<td><span class=\"Label\">VAR_COURT_TIME</span></td>");
            sbViolationRow.Append("</tr>");

            try
            {
                reader = new StreamReader(Server.MapPath("../EmailTemplates/CaseSummary.htm"));
                if (reader != null)
                {
                    sbMessage.Append(reader.ReadToEnd());
                    reader.Close();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (TicketViolationIDs != "")
                        {
                            caseNum = "";
                            if (dr["TicketsViolationID"].ToString() == tvID)
                            {
                                if (dr["CauseNumber"].ToString() == "N/A") caseNum = dr["refcasenumber"].ToString();
                                else caseNum = dr["CauseNumber"].ToString();

                                singleRow = string.Empty;
                                singleRow = sbViolationRow.ToString();
                                singleRow = singleRow.Replace("VAR_VIOLATION_DESC", dr["violationDescription"].ToString());
                                singleRow = singleRow.Replace("VAR_CASE_NUMBER", caseNum);
                                singleRow = singleRow.Replace("VAR_COURT_LOCATION", dr["ShortName"].ToString());
                                singleRow = singleRow.Replace("VAR_COURT_NUMBER", (dr["CourtNumber"].ToString() == "" ? "0" : dr["CourtNumber"].ToString()));
                                singleRow = singleRow.Replace("VAR_COURT_DATE", (Convert.ToDateTime(dr["CourtDateMain"].ToString())).ToString("MM/dd/yyyy"));
                                singleRow = singleRow.Replace("VAR_COURT_TIME", (Convert.ToDateTime(dr["CourtDateMain"].ToString())).ToShortTimeString());
                                sbAllViolations.Append(singleRow);
                                TicketViolationIDs = TicketViolationIDs.Substring(TicketViolationIDs.IndexOf(",") + 1, TicketViolationIDs.Length - 1 - TicketViolationIDs.IndexOf(","));
                                if (TicketViolationIDs != "")
                                    tvID = TicketViolationIDs.Substring(0, TicketViolationIDs.IndexOf(","));

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            //Abbas Shahid Khwaja 9215 04/26/2011 Add Contact Number 
            sbMessage.Replace("VAR_CLIENT_NAME", ClientName);
            sbMessage.Replace("VAR_ATTORNEY_NAME", AttorneyName);
            sbMessage.Replace("VAR_PAYMENT_MESSAGE", PaymentMessage);
            sbMessage.Replace("VAR_CONTACT_NUMBER", ContactNumber);
            sbMessage.Replace("VAR_CASE_SUMMARY", CaseSummary);

            if (sbAllViolations != null) sbMessage.Replace("VAR_VIOLATION_ROWS", sbAllViolations.ToString());

            return sbMessage.ToString();
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgViolationInfo.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgViolationInfo_ItemDataBound);
            this.btn_Lock.Click += new System.EventHandler(this.btn_Lock_Click);
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            this.btnfineamount.Click += new EventHandler(btnfineamount_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.dgViolationInfo.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgViolationInfo_SortCommand);
            //Yasir Kamal 7140 12/22/2009 display ticketnumber if cause number not available
            this.grdEmailCaseSummary.RowDataBound += new GridViewRowEventHandler(this.grdEmailCaseSummary_RowDataBound);

        }
        #endregion

        /// <summary>
        /// Fahad 5807 05/14/2009
        /// Check in record set that either FTA is avaliable in Client Profile then ADD manually 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtn_NewHireBondClient_Click(object sender, EventArgs e)
        {
            try
            {
                int TicketID = Convert.ToInt32(ViewState["vTicketID"]);
                int RecordID = Convert.ToInt32(ViewState["RecordID"]);
                string courtid = (ViewState["CourtID"]).ToString();
                string AddAlpha = "A";
                string MidNumber = ViewState["Midnum"].ToString();
                string NewCauseNo = string.Empty;
                DataTable LastRow = new DataTable();
                LastRow = ClsCase.GetLastViolation(Convert.ToInt32(ViewState["vTicketID"]));
                DataRow lastrow = LastRow.Rows[0];
                string CauseNo = lastrow["casenumassignedbycourt"].ToString().Trim();
                if (CauseNo.Trim() == string.Empty)
                {
                    CauseNo = lastrow["RefCaseNumber"].ToString().Trim();
                }
                int TicktViolId = Convert.ToInt32(lastrow["TicketsViolationId"].ToString());
                char last = (Convert.ToChar((CauseNo[CauseNo.Length - 1].ToString())));
                int value = Convert.ToInt32(last);
                if (value != 90)
                {
                    if (value >= 65 && value < 90)
                    {
                        value = value + 1;
                        char newchar = (char)value;
                        AddAlpha = Convert.ToString(newchar);
                        NewCauseNo = CauseNo.Substring(0, CauseNo.Length - 1) + AddAlpha;
                    }
                    else if (!(value >= 65 && value <= 90))
                    {
                        NewCauseNo = CauseNo + AddAlpha;
                    }
                    ClsCase.AddFTAViolationsNonClient(RecordID, MidNumber, NewCauseNo, TicketID, 0, CourtViolationStatusType.BOND, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)));
                }

                ClsCase.UpdateCaseStatus(TicketID, CourtViolationStatusType.BOND, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)));
                if (courtid == "3001" || courtid == "3002" || courtid == "3003")
                {
                    ClsCase.UpdateBondforHMC(TicketID);
                }

                ClsCase.IsBondFlagUpdated(TicketID);
                ClsCase.UnderLineBondFlagUpdated(TicketID);
                // Noufil 5807 06/17/2009 Check bond flag to true.
                rdbtn_BondYes.Checked = true;
                DisplayInfo();
                CalculateFee();
                // Afaq 8311 12/09/2010 Assign bond violation count value to the hidden field in UpdateViolationSRV1 Control.
                HiddenField statusCount = (HiddenField)UpdateViolationSRV1.FindControl("hf_statusCount");
                statusCount.Value = Convert.ToString(ClsCase.ViolationCountByStatus(CourtViolationStatusType.BOND));

            }
            catch (Exception ex)
            {
                clsLogger.ErrorLog(ex);
            }


        }

        //Haris Ahmed 10381 08/30/2012 Show hide immigration comments
        private void SetImmigration()
        {
            DataSet ds = ClsCase.CanBePotentialVisaClients(ClsCase.TicketID);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //Farrukh 11180 06/26/2013 Display Immigration Question for Both "SPANISH" & "ENGLISH" clients
                    trImmigration.Style.Add("display", "block");
                    rbtn_interestedinworkvisayes.Enabled = true;
                    rbtn_interestedinworkvisano.Enabled = true;
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["IsImmigration"].ToString()))
                    {
                        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsImmigration"].ToString()) == true)
                            rbtn_interestedinworkvisayes.Checked = true;
                        else
                            rbtn_interestedinworkvisano.Checked = true;
                    }
                    if (ds.Tables[0].Rows[0]["Comments"].ToString().Trim() != "" && txtImmigrationComments.Text == "")
                    {                        
                        trImmigrationComments.Style.Add("display", "block");
                        txtImmigrationComments.Text = ds.Tables[0].Rows[0]["Comments"].ToString().Trim();
                    }
                    hfCanBePotentialVisaClients.Value = "1";
                }
                else
                {
                    trImmigration.Style.Add("display", "none");
                    trImmigrationComments.Style.Add("display", "none");
                    rbtn_interestedinworkvisayes.Enabled = false;
                    rbtn_interestedinworkvisano.Enabled = false;
                    rbtn_interestedinworkvisayes.Checked = false;
                    rbtn_interestedinworkvisano.Checked = false;
                    hfCanBePotentialVisaClients.Value = "0";
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static VoilationModel ControlLoad(int courtid, int casetype, int hf_IsDispositionpage)
        {
            var obj = new VoilationModel();
            obj.PVDESCModelList = new System.Collections.Generic.List<PVDESCModel>();
            obj.StatusModelList = new System.Collections.Generic.List<StatusModel>();
            clsViolations ClsViolations = new clsViolations();
            DataSet ds_Violation = ClsViolations.GetAllViolations(casetype, courtid);

            if (ds_Violation.Tables.Count > 0)
            {
                if (ds_Violation.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds_Violation.Tables[0].Rows)
                    {
                        var single = new PVDESCModel();
                        single.Id = Convert.ToInt32(dr["ID"]);
                        single.Name = dr["Description"].ToString();
                        obj.PVDESCModelList.Add(single);
                    }
                }

            }

            DataSet ds_Status = null;
            updatePanel upnl = new updatePanel();
            clsCaseStatus ClsCaseStatus = new clsCaseStatus();
            bool iscasedisposition = hf_IsDispositionpage == 0 ? false : true;
            //ozair 4442 07/22/2008 ALR Courts(Houston, Fort bend, Conroe)
            //Nasir 5310 12/24/2008 checking ALR Courts by adding IsALRProcess
            if (upnl.IsALRProcess(courtid))
                ds_Status = ClsCaseStatus.GetALRCourtCaseStatus(courtid, iscasedisposition);
            else if (courtid == 0)
                ds_Status = ClsCaseStatus.GetAllCourtCaseStatus();
            else
                ds_Status = ClsCaseStatus.GetAllCourtCaseStatus(courtid, iscasedisposition);

            if (ds_Status.Tables.Count > 0)
            {
                if (ds_Status.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds_Status.Tables[0].Rows)
                    {
                        var single = new StatusModel();
                        single.Id = Convert.ToInt32(dr["id"]);
                        single.Name = dr["description"].ToString();
                        obj.StatusModelList.Add(single);
                    }
                }

            }

            //if (obj != null)
            //{
            //    if (obj.PVDESCModelList.Count > 1000) obj.PVDESCModelList = obj.PVDESCModelList.Take(1000).ToList();
            //    if (obj.StatusModelList.Count > 0) obj.StatusModelList = obj.StatusModelList.Take(200).ToList();
            //}


            return obj;
        }
        public class VoilationModel
        {
            public System.Collections.Generic.List<PVDESCModel> PVDESCModelList { get; set; }
            public System.Collections.Generic.List<StatusModel> StatusModelList { get; set; }
        }
        public class PVDESCModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public class StatusModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using HTP.Components;
using HTP.WebComponents;

namespace HTP.ClientInfo
{
    /// <summary>
    /// Summary description for CaseDisposition.
    /// </summary>
    /// 
    //Aziz 1974 05/14/08 Changed to use base page.
    public partial class CaseDisposition : MatterBasePage
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsCase ClsCase = new clsCase();
        clsFirms ClsFirms = new clsFirms();
        clsReadNotes notes = new clsReadNotes();
        clsGeneralMethods ClsGeneralMethod = new clsGeneralMethods();

        protected System.Web.UI.WebControls.DropDownList ddl_Coverage;
        protected System.Web.UI.WebControls.TextBox txt_GeneralComments;
        protected System.Web.UI.WebControls.TextBox txt_TrialComments;
        protected System.Web.UI.WebControls.Button btn_UpdateDetails;
        clsCaseDetail ClsDetail = new clsCaseDetail();
        clsCaseStatus ClsCaseStatus = new clsCaseStatus();

        //Aziz 1974 05/14/08 Changed to use base page ticket id.
        //Int32	intTicketID ;
        Int32 iCount = 0;
        DataSet ds = new DataSet();
        clsLogger bugTracker = new clsLogger();
        protected System.Web.UI.WebControls.DataGrid dg_Details;
        protected System.Web.UI.WebControls.Label test;
        protected System.Web.UI.WebControls.DropDownList ddl_hid;
        protected System.Web.UI.HtmlControls.HtmlGenericControl CtrlEnd;
        string[,] arrMonth = new string[14, 2];
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtHidden;
        protected System.Web.UI.WebControls.TextBox lbl_Court;
        protected System.Web.UI.WebControls.Label lbl_CaseStatus;
        protected System.Web.UI.WebControls.Label lbl_CrtDate;
        protected System.Web.UI.WebControls.Label lbl_Crttime;
        protected System.Web.UI.WebControls.Label lbl_CrtNo;
        protected System.Web.UI.WebControls.Label lblMessage;
        protected System.Web.UI.WebControls.Label lb_LastName;
        protected System.Web.UI.WebControls.Label lb_firstName;
        protected System.Web.UI.WebControls.Label lbl_CaseCount;
        protected System.Web.UI.WebControls.HyperLink hlk_MIDNo;
        protected System.Web.UI.WebControls.Button btn_UpdateInfo;
        protected System.Web.UI.WebControls.Button btnNext;
        protected System.Web.UI.WebControls.Label lblAdmin;
        clsSession ClsSession = new clsSession();
        clsCourts ClsCourts = new clsCourts();

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Added By Zeeshan
                // Update Violation Control Delegate
                //Zeeshan Ahmed 3979 05/20/2008 Add New Update Violation Control
                // Abbas Shahid Khwaja 9435 06/29/2011 font size,bold and name is reset.
                lbl_Message.CssClass = "FlagMessageNormal";
                UpdateViolationSRV1.PageMethod += delegate()
                {
                    test.Text = UpdateViolationSRV1.ErrorMessage;
                    PopulateRecords();
                    FillDateDropDowns();
                    PopulateRecords();
                    FillinGrid();
                    //Sameeullah 8623 12/31/2010 after Violation updation Displays  message(s) on top if pre trial diviersion flag/motion hiring  are active for the case.
                    SetPreTrailFlags();
                    //Sabir Khan 8862 02/15/2011 after Violation updation Displays  message(s) on top if JUVENILE flag is active for the case.
                    //SetJUVENILEFlags();
                };

                if (!IsPostBack)
                {
                    if (Request.QueryString["caseNumber"] != null && Request.QueryString["caseNumber"].ToString() == "0")
                        Response.Redirect("ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=0", false);
                    if (Request.QueryString.Count >= 2)
                    {
                        ////Aziz 1974 05/14/08 Changed to use base page ticket id.
                        //this.TicketId = Request.QueryString["casenumber"];
                        ViewState["vSearch"] = Request.QueryString["search"];
                    }
                    else
                    {
                        Response.Redirect("../frmMain.aspx", false);
                        goto SearchPage;
                    }

                    test.Text = "";

                    //Aziz 1974 05/14/08 Changed to use base page properties.
                    //this.EmpId = ClsSession.GetCookie("sEmpID", this.Request);
                    //intTicketID = this.TicketId;
                    //if (intTicketID == 0)
                    //    Response.Redirect("ViolationFeeold.aspx?newt=1", false);

                    ClsCase.TicketID = this.TicketId;

                    //Added by ozair
                    //Check for User Type 2 means Admin, 1 means application user
                    if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "2")
                    {
                        lblAdmin.Text = "1";
                    }
                    else if (ClsSession.GetCookie("sAccessType", this.Request).ToString() == "1")
                    {
                        lblAdmin.Text = "0";
                    }


                    //
                    #region "Code on Load page "
                    FillDateDropDowns();
                    PopulateRecords();
                    FillinGrid();

                    btn_UpdateInfo.Attributes.Add("OnClick", "return ValidateInput();");
                    btn_UpdateDetails.Attributes.Add("OnClick", "return ValidateInput();plzwait('TableComment','plz_wait2');");

                    btnNext.Attributes.Add("OnClick", "return ValidateInput();");
                    txtHidden.Value = dg_Details.Items.Count.ToString();
                    CheckReadNotes(this.TicketId);



                }

                // Babar Ahmad 8623 05/25/2011 Changed position of the SetPreTrailFlags function to view the message. 
                //Sabir Khan 8862 02/15/2011 after Violation updation Displays  message(s) on top if JUVENILE flag is active for the case.
                //SetJUVENILEFlags();
                //Sameeullah 8623 12/31/2010 Displays  message(s) on top if pre trial diviersion flag/motion hiring  are active for the case.
                SetPreTrailFlags();

                    #endregion


                ActiveMenu am = (ActiveMenu)this.FindControl("ActiveMenu1");
                TextBox txt1 = (TextBox)am.FindControl("txtid");
                txt1.Text = this.TicketId.ToString();
                TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                txt2.Text = Convert.ToString(ViewState["vSearch"]);
            SearchPage:
                { }
            }
            catch (Exception ex)
            {
                test.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        //Sabir Khan 8623 12/13/2010 Displays  message(s) on top if pre trial diviersion flag/motion hiring  are active for the case.

        /// <summary>
        /// Displays  message(s) on top if pre trial diviersion flag/motion hiring  are active for the case.
        /// </summary>
        private void SetPreTrailFlags()
        {
            ClsFlags cflag = new ClsFlags();
            //cflag.TicketID = Convert.ToInt32(this.TicketId);
            bool isPreTrial_Diversion = false;
            bool isMotion_Hiring = false;
            bool isJUVENILE = false;

            DataTable dtFlags = cflag.GetFlages(this.TicketId);
            if (dtFlags != null && dtFlags.Rows.Count > 0)
            {
                for (int loopCounter = 0; loopCounter < dtFlags.Rows.Count; loopCounter++)
                {
                    if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.Pre_Trial_Diversion))
                    {
                        lbl_Message.Visible = true;
                        lbl_Message.CssClass = "FlagMessageBold";
                        lbl_Message.Text = "The client has a Pre-Trial Diversion status. This case can't be reschedule";
                        isPreTrial_Diversion = true;
                    }
                }

                if (!isPreTrial_Diversion)
                {
                    if (lbl_Message.Text == "" || lbl_Message.Text == "The client has a Pre-Trial Diversion status. This case can't be reschedule<br/> The client has a Motion Hiring status. This case can't be reschedule" || lbl_Message.Text == "The client has a Pre-Trial Diversion status. This case can't be reschedule" || lbl_Message.Text == "The client has a Motion Hiring status. This case can't be reschedule")
                    { lbl_Message.Visible = false; }
                }

                for (int loopCounter = 0; loopCounter < dtFlags.Rows.Count; loopCounter++)
                {
                    if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.Motion_Hiring))
                    {
                        if (lbl_Message.Visible)
                        {
                            lbl_Message.CssClass = "FlagMessageBold";
                            lbl_Message.Text = lbl_Message.Text + "<br/> The client has a Motion Hiring status. This case can't be reschedule";
                            isMotion_Hiring = true;
                        }
                        else
                        {
                            lbl_Message.Visible = true;
                            lbl_Message.CssClass = "FlagMessageBold";
                            lbl_Message.Text = "The client has a Motion Hiring status. This case can't be reschedule";
                        }
                    }
                }

                for (int loopCounter = 0; loopCounter < dtFlags.Rows.Count; loopCounter++)
                {
                    if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.JUVENILE))
                    {
                        if (lbl_Message.Visible)
                        {
                            lbl_Message.Visible = true;
                            lbl_Message.Text = "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.";
                            isJUVENILE = true;
                        }
                    }
                }
                if (!isJUVENILE)
                {
                    if (lbl_Message.Text == "" || lbl_Message.Text == "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.")
                    { lbl_Message.Visible = false; }
                }

                //cflag.FlagID = Convert.ToInt32(FlagType.Pre_Trial_Diversion);
                //if (cflag.HasFlag())
                //{
                //    //Abbas Shahid Khwaja 9435 06/27/2011 font size,bold and name is set. 
                //    lbl_Message.Visible = true;
                //    lbl_Message.CssClass = "FlagMessageBold";
                //    lbl_Message.Text = "The client has a Pre-Trial Diversion status. This case can't be reschedule";
                //}
                //else
                //{
                //    if (lbl_Message.Text == "" || lbl_Message.Text == "The client has a Pre-Trial Diversion status. This case can't be reschedule<br/> The client has a Motion Hiring status. This case can't be reschedule" || lbl_Message.Text == "The client has a Pre-Trial Diversion status. This case can't be reschedule" || lbl_Message.Text == "The client has a Motion Hiring status. This case can't be reschedule")
                //    { lbl_Message.Visible = false; }
                //}
                //cflag.FlagID = Convert.ToInt32(FlagType.Motion_Hiring);
                //if (cflag.HasFlag())
                //{
                //    if (lbl_Message.Visible)
                //    {
                //        //Abbas Shahid Khwaja 9435 06/28/2011 font size,bold and name is set. 
                //        lbl_Message.CssClass = "FlagMessageBold";
                //        lbl_Message.Text = lbl_Message.Text + "<br/> The client has a Motion Hiring status. This case can't be reschedule";
                //    }
                //    else
                //    {
                //        //Abbas Shahid Khwaja 9435 06/28/2011 font size,bold and name is set.
                //        lbl_Message.Visible = true;
                //        lbl_Message.CssClass = "FlagMessageBold";
                //        lbl_Message.Text = "The client has a Motion Hiring status. This case can't be reschedule";
                //    }
                //}
            }
        }

        //Sabir Khan 8862 02/15/2011 Displays  message(s) on top if JUVENILE flag are active for the case.
        /// <summary>
        /// Displays  message(s) on top if JUVENILE flag are active for the case.
        /// </summary>
        //private void SetJUVENILEFlags()
        //{
        //    ClsFlags cflag = new ClsFlags();
        //    cflag.TicketID = Convert.ToInt32(this.TicketId);
        //    cflag.FlagID = Convert.ToInt32(FlagType.JUVENILE);
        //    if (cflag.HasFlag())
        //    {
        //        lbl_Message.Visible = true;
        //        lbl_Message.Text = "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.";
        //    }
        //    else
        //    {
        //        if (lbl_Message.Text == "" || lbl_Message.Text == "A JUVENILE CASE CANNOT BE RESET. CLIENT MUST BE PRESENT AND NOT ALLOW AGENTS TO CHANGE STATUS OF CASE. ALL OUR JUVENILE CASES MUST BE PRESENT IN COURT.")
        //        { lbl_Message.Visible = false; }
        //    }
        //}

        #region Web Form Designer generated code
        /// <summary>
        /// init event of page
        /// </summary>
        /// <param name="e"></param>
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_UpdateInfo.Click += new System.EventHandler(this.btn_UpdateInfo_Click);
            this.btn_UpdateDetails.Click += new System.EventHandler(this.btn_UpdateInfo_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
        // Fill Case Disposition Grid
        private void FillinGrid()
        {
            try
            {
                #region  "  Filling Violation Outcomes"
                string[,] arrPending = new string[10, 2];

                // Filling ddl_Pending in datagrid
                //Change by Ajmal
                //SqlDataReader rdr_Pending = ClsDb.Get_DR_BySP("usp_HTS_Get_CaseDispositionStatus");
                IDataReader rdr_Pending = ClsDb.Get_DR_BySP("usp_HTS_Get_CaseDispositionStatus");
                arrPending = new string[20, 2];
                iCount = 0;
                while (rdr_Pending.Read())
                {
                    arrPending[iCount, 0] = (rdr_Pending["Dcr"].ToString());
                    arrPending[iCount, 1] = rdr_Pending["CaseStatusID_Pk"].ToString();
                    iCount++;
                }
                rdr_Pending.Close();
                #endregion

                #region "Filling Firms"
                //----------code Added by M.Azwar Alam on 27 August 2007 for Task no 1126-------------------
                DataSet ds_Firms = ClsFirms.GetActiveFirms(1);
                //------------------------------------------------------------------------------------------

                #endregion

                Int32 i = 0;
                foreach (DataGridItem items in dg_Details.Items)
                {
                    #region "  Fill Drop Downs"
                    for (i = 0; i <= (iCount - 1); i++)
                    {
                        ((DropDownList)items.FindControl("ddl_pending")).Items.Add(arrPending[i, 0]);//"CaseStatusID_Pk";
                        ((DropDownList)items.FindControl("ddl_pending")).Items[i].Value = (arrPending[i, 1]);//"CaseStatusID_Pk";
                    }
                    //----------code Added by M.Azwar Alam on 27 August 2007 for Task no 1126-------------------
                    if (ds_Firms.Tables[0].Rows.Count > 0)
                    {
                        ((DropDownList)items.FindControl("ddlcoveringfirms")).Items.Clear();
                        for (int j = 0; j < ds_Firms.Tables[0].Rows.Count; j++)
                        {
                            string Description = ds_Firms.Tables[0].Rows[j]["FirmAbbreviation"].ToString().Trim();
                            string ID = ds_Firms.Tables[0].Rows[j]["FirmID"].ToString().Trim();
                            ((DropDownList)items.FindControl("ddlcoveringfirms")).Items.Add(new ListItem(Description, ID));
                        }


                    }
                    //------------------------------------------------------------------------------------------

                    for (i = 0; i <= 12; i++)
                    {
                        ((DropDownList)items.FindControl("ddl_PAMonth")).Items.Add(arrMonth[i, 0]);
                        ((DropDownList)items.FindControl("ddl_PAMonth")).Items[i].Value = (arrMonth[i, 1]);
                        ((DropDownList)items.FindControl("ddl_PTMonth")).Items.Add(arrMonth[i, 0]);
                        ((DropDownList)items.FindControl("ddl_PTMonth")).Items[i].Value = (arrMonth[i, 1]);
                        ((DropDownList)items.FindControl("ddl_CauseMonth")).Items.Add(arrMonth[i, 0]);
                        ((DropDownList)items.FindControl("ddl_CauseMonth")).Items[i].Value = (arrMonth[i, 1]);
                        ((DropDownList)items.FindControl("ddl_ShowMonth")).Items.Add(arrMonth[i, 0]);
                        ((DropDownList)items.FindControl("ddl_ShowMonth")).Items[i].Value = (arrMonth[i, 1]);
                    }
                    ((DropDownList)items.FindControl("ddl_PAMonth")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_PTMonth")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_CauseMonth")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_ShowMonth")).SelectedIndex = 0;

                    ((DropDownList)items.FindControl("ddl_PTDay")).Items.Add("--");
                    ((DropDownList)items.FindControl("ddl_PADay")).Items.Add("--");
                    ((DropDownList)items.FindControl("ddl_CauseDay")).Items.Add("--");
                    ((DropDownList)items.FindControl("ddl_ShowDay")).Items.Add("--");
                    for (i = 1; i <= 31; i++)
                    {
                        ((DropDownList)items.FindControl("ddl_PTDay")).Items.Add(Convert.ToString(i));
                        ((DropDownList)items.FindControl("ddl_PADay")).Items.Add(Convert.ToString(i));
                        ((DropDownList)items.FindControl("ddl_CauseDay")).Items.Add(Convert.ToString(i));
                        ((DropDownList)items.FindControl("ddl_ShowDay")).Items.Add(Convert.ToString(i));
                    }
                    ((DropDownList)items.FindControl("ddl_PTDay")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_PADay")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_CauseDay")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_ShowDay")).SelectedIndex = 0;

                    ((DropDownList)items.FindControl("ddl_PTYear")).Items.Add("----");
                    ((DropDownList)items.FindControl("ddl_PAYear")).Items.Add("----");
                    ((DropDownList)items.FindControl("ddl_CauseYear")).Items.Add("----");
                    ((DropDownList)items.FindControl("ddl_ShowYear")).Items.Add("----");
                    int dYear = Convert.ToInt32(System.DateTime.Today.Year.ToString());
                    dYear = dYear + 10;
                    for (i = 1996; i <= dYear; i++)
                    {
                        ((DropDownList)items.FindControl("ddl_PTYear")).Items.Add(Convert.ToString(i));
                        ((DropDownList)items.FindControl("ddl_PAYear")).Items.Add(Convert.ToString(i));
                        ((DropDownList)items.FindControl("ddl_CauseYear")).Items.Add(Convert.ToString(i));
                        ((DropDownList)items.FindControl("ddl_ShowYear")).Items.Add(Convert.ToString(i));
                    }
                    ((DropDownList)items.FindControl("ddl_PTYear")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_PAYear")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_CauseYear")).SelectedIndex = 0;
                    ((DropDownList)items.FindControl("ddl_ShowYear")).SelectedIndex = 0;

                    //Time
                    FillTimeDDL(((DropDownList)items.FindControl("ddl_CauseTime")));
                    FillTimeDDL(((DropDownList)items.FindControl("ddl_ShowTime")));
                    #endregion

                    #region "   setGridContents"

                    ((Label)(items.FindControl("lbl_Ulinks"))).Text = "updateviolation.aspx?ticketsViolationID=" + ((Label)(items.FindControl("lb_TicketsViolationID"))).Text + "&ticketid=" + this.TicketId.ToString() + "&Dispo=1";

                    Label lbl = (Label)items.FindControl("Label1");
                    DropDownList ddl = (DropDownList)items.FindControl("ddl_pending");
                    ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(lbl.Text.ToString()));

                    Label lblDate = (Label)items.FindControl("lb_HidAmountDate");

                    if ((lblDate.Text != null) || (lblDate.Text != ""))
                    {
                        DropDownList DdlMonLst = (DropDownList)items.FindControl("ddl_PAMonth");
                        DropDownList DdlDayLst = (DropDownList)items.FindControl("ddl_PADay");
                        DropDownList DdlYearLst = (DropDownList)items.FindControl("ddl_PAYear");

                        string RsltMonth = ClsGeneralMethod.ParseDate("M", (DateTime)Convert.ChangeType(lblDate.Text, typeof(DateTime)));
                        string RsltDay = ClsGeneralMethod.ParseDate("D", (DateTime)Convert.ChangeType(lblDate.Text, typeof(DateTime)));
                        string RsltYear = ClsGeneralMethod.ParseDate("Y", (DateTime)Convert.ChangeType(lblDate.Text, typeof(DateTime)));

                        if (RsltYear != "1900")
                        {
                            DdlMonLst.SelectedIndex = DdlMonLst.Items.IndexOf(DdlMonLst.Items.FindByValue(RsltMonth.ToString()));
                            DdlDayLst.SelectedIndex = DdlDayLst.Items.IndexOf(DdlDayLst.Items.FindByValue(RsltDay.ToString()));
                            DdlYearLst.SelectedIndex = DdlYearLst.Items.IndexOf(DdlYearLst.Items.FindByValue(RsltYear.ToString()));
                        }
                        else
                        {
                            DdlMonLst.SelectedIndex = 0;
                            DdlDayLst.SelectedIndex = 0;
                            DdlYearLst.SelectedIndex = 0;
                        }

                    }

                    Label lblDatePay = (Label)items.FindControl("lb_HidPayDate");

                    if ((lblDatePay.Text != null) || (lblDatePay.Text != ""))
                    {

                        DropDownList DdlMonPayLst = (DropDownList)items.FindControl("ddl_PTMonth");
                        DropDownList DdlDayPayLst = (DropDownList)items.FindControl("ddl_PTDay");
                        DropDownList DdlYearPayLst = (DropDownList)items.FindControl("ddl_PTYear");

                        string RsltMonthPay = ClsGeneralMethod.ParseDate("M", (DateTime)Convert.ChangeType(lblDatePay.Text, typeof(DateTime)));
                        string RsltDayPay = ClsGeneralMethod.ParseDate("D", (DateTime)Convert.ChangeType(lblDatePay.Text, typeof(DateTime)));
                        string RsltYearPay = ClsGeneralMethod.ParseDate("Y", (DateTime)Convert.ChangeType(lblDatePay.Text, typeof(DateTime)));

                        if (RsltYearPay != "1900")
                        {
                            DdlMonPayLst.SelectedIndex = DdlMonPayLst.Items.IndexOf(DdlMonPayLst.Items.FindByValue(RsltMonthPay.ToString()));
                            DdlDayPayLst.SelectedIndex = DdlDayPayLst.Items.IndexOf(DdlDayPayLst.Items.FindByValue(RsltDayPay.ToString()));
                            DdlYearPayLst.SelectedIndex = DdlYearPayLst.Items.IndexOf(DdlYearPayLst.Items.FindByValue(RsltYearPay.ToString()));
                        }
                        else
                        {
                            DdlMonPayLst.SelectedIndex = 0;
                            DdlDayPayLst.SelectedIndex = 0;
                            DdlYearPayLst.SelectedIndex = 0;
                        }
                    }

                    Label lblDateCause = (Label)items.FindControl("lb_HidCauseDate");
                    if (lblDateCause.Text != "")
                    {
                        DropDownList DdlMonCauseLst = (DropDownList)items.FindControl("ddl_CauseMonth");
                        DropDownList DdlDayCauseLst = (DropDownList)items.FindControl("ddl_CauseDay");
                        DropDownList DdlYearCauseLst = (DropDownList)items.FindControl("ddl_CauseYear");
                        DropDownList DdlTimeCauseLst = (DropDownList)items.FindControl("ddl_CauseTime");


                        string RsltMonthCause = ClsGeneralMethod.ParseDate("M", (DateTime)Convert.ChangeType(lblDateCause.Text, typeof(DateTime)));
                        string RsltDayCause = ClsGeneralMethod.ParseDate("D", (DateTime)Convert.ChangeType(lblDateCause.Text, typeof(DateTime)));
                        string RsltYearCause = ClsGeneralMethod.ParseDate("Y", (DateTime)Convert.ChangeType(lblDateCause.Text, typeof(DateTime)));
                        string RsltTimeCause = ClsGeneralMethod.ParseDate("T", (DateTime)Convert.ChangeType(lblDateCause.Text, typeof(DateTime)));

                        if (RsltYearCause != "1900")
                        {
                            DdlMonCauseLst.SelectedIndex = DdlMonCauseLst.Items.IndexOf(DdlMonCauseLst.Items.FindByValue(RsltMonthCause.ToString()));
                            DdlDayCauseLst.SelectedIndex = DdlDayCauseLst.Items.IndexOf(DdlDayCauseLst.Items.FindByValue(RsltDayCause.ToString()));
                            DdlYearCauseLst.SelectedIndex = DdlYearCauseLst.Items.IndexOf(DdlYearCauseLst.Items.FindByValue(RsltYearCause.ToString()));
                            DdlTimeCauseLst.SelectedIndex = DdlTimeCauseLst.Items.IndexOf(DdlTimeCauseLst.Items.FindByValue(RsltTimeCause.ToString()));
                        }
                        else
                        {
                            DdlMonCauseLst.SelectedIndex = 0;
                            DdlDayCauseLst.SelectedIndex = 0;
                            DdlYearCauseLst.SelectedIndex = 0;
                            DdlTimeCauseLst.SelectedIndex = 0;
                        }
                    }


                    Label lblShowDate = (Label)items.FindControl("lbl_HidShowDate");
                    if (lblShowDate.Text != "")
                    {
                        DropDownList DdlMonShowLst = (DropDownList)items.FindControl("ddl_ShowMonth");
                        DropDownList DdlDayShowLst = (DropDownList)items.FindControl("ddl_ShowDay");
                        DropDownList DdlYearShowLst = (DropDownList)items.FindControl("ddl_ShowYear");
                        DropDownList DdlTimeShowLst = (DropDownList)items.FindControl("ddl_ShowTime");

                        string RsltMonthShow = ClsGeneralMethod.ParseDate("M", (DateTime)Convert.ChangeType(lblShowDate.Text, typeof(DateTime)));
                        string RsltDayShow = ClsGeneralMethod.ParseDate("D", (DateTime)Convert.ChangeType(lblShowDate.Text, typeof(DateTime)));
                        string RsltYearShow = ClsGeneralMethod.ParseDate("Y", (DateTime)Convert.ChangeType(lblShowDate.Text, typeof(DateTime)));
                        string RsltTimeShow = ClsGeneralMethod.ParseDate("T", (DateTime)Convert.ChangeType(lblShowDate.Text, typeof(DateTime)));

                        if (RsltYearShow != "1900")
                        {
                            DdlMonShowLst.SelectedIndex = DdlMonShowLst.Items.IndexOf(DdlMonShowLst.Items.FindByValue(RsltMonthShow.ToString()));
                            DdlDayShowLst.SelectedIndex = DdlDayShowLst.Items.IndexOf(DdlDayShowLst.Items.FindByValue(RsltDayShow.ToString()));
                            DdlYearShowLst.SelectedIndex = DdlYearShowLst.Items.IndexOf(DdlYearShowLst.Items.FindByValue(RsltYearShow.ToString()));
                            DdlTimeShowLst.SelectedIndex = DdlTimeShowLst.Items.IndexOf(DdlTimeShowLst.Items.FindByValue(RsltTimeShow.ToString()));
                        }
                        else
                        {
                            DdlMonShowLst.SelectedIndex = 0;
                            DdlDayShowLst.SelectedIndex = 0;
                            DdlYearShowLst.SelectedIndex = 0;
                            DdlTimeShowLst.SelectedIndex = 0;
                        }
                    }

                    //-----------------Added by Azwar Alam for Task no 1126--------------------------
                    //If firm is disable then show label and arrow image button otherwise show firm drop down..
                    DropDownList ddlCoveringFirm = (DropDownList)items.FindControl("ddlcoveringfirms");
                    HiddenField HFFirmID = (HiddenField)items.FindControl("HFCoveringFirmID");



                    if (HFFirmID.Value != "0")
                        ddlCoveringFirm.SelectedValue = HFFirmID.Value;
                    else
                        ddlCoveringFirm.SelectedValue = "3000";



                    if (ddlCoveringFirm.Items.FindByValue(HFFirmID.Value) == null)
                    {
                        ((Label)items.FindControl("lblfirmname")).Text = ClsFirms.GetFirmAbbreviation(Convert.ToInt32(HFFirmID.Value));
                        ddlCoveringFirm.Style["display"] = "none";
                        ((System.Web.UI.HtmlControls.HtmlImage)items.FindControl("Imgtoggle")).Visible = true;
                        ((Label)items.FindControl("lblfirmname")).Visible = true;

                    }
                    else
                    {
                        ddlCoveringFirm.Style["display"] = "table-row";
                        ddlCoveringFirm.SelectedValue = HFFirmID.Value;
                        ((System.Web.UI.HtmlControls.HtmlImage)items.FindControl("Imgtoggle")).Visible = false;
                        ((Label)items.FindControl("lblfirmname")).Visible = false;
                    }
                    //--------------------------------------------------------------------------------------
                    #endregion

                    #region  "  setjavascript()"
                    // Noufil 4369 07/22/2008 If case is criminal then hide ticket number column and make hyperlink to case number
                    int casetype = Convert.ToInt32(((HiddenField)items.FindControl("hf_casetype")).Value);
                    if (casetype == 2)
                    {
                        items.FindControl("td_ticket").Visible = false;
                        items.FindControl("td_causeno").Visible = false;
                        td_ticketname.Visible = false;
                        items.FindControl("td_lcausenumber").Visible = true;
                        ((System.Web.UI.HtmlControls.HtmlTable)items.FindControl("Table2")).Width = "780px";
                        ((System.Web.UI.HtmlControls.HtmlTableCell)items.FindControl("td_lcausenumber")).Width = "170px";
                        ((System.Web.UI.HtmlControls.HtmlTableCell)items.FindControl("td_status")).Width = "200px";
                        ((System.Web.UI.HtmlControls.HtmlTableCell)items.FindControl("td_desc")).Width = "150px";

                        table_name.Rows[0].Cells[1].Width = "220px";
                        table_name.Rows[0].Cells[2].Width = "250px";
                        table_name.Rows[0].Cells[3].Width = "250px";
                        table_name.Rows[0].Cells[4].Width = "150px";

                        ((LinkButton)(items.FindControl("lnkcausenumber"))).Attributes.Add("onClick", "return showpopup('" + UpdateViolationSRV1.ClientID + "','" + items.ClientID + "',0);");
                    }
                    else
                    {
                        items.FindControl("td_ticket").Visible = true;
                        items.FindControl("td_causeno").Visible = true;
                        td_ticketname.Visible = true;
                        items.FindControl("td_lcausenumber").Visible = false;
                        //((System.Web.UI.HtmlControls.HtmlTable)items.FindControl("Table2")).Width = "780px";
                        //table_name.Width = "780px";
                        ((LinkButton)(items.FindControl("lnkbtn_TicketNo"))).Attributes.Add("onClick", "return showpopup('" + UpdateViolationSRV1.ClientID + "','" + items.ClientID + "',0);");
                    }

                    //lnkBtn.Attributes.Add("onClick", "showpopup('" + e.Item.ClientID + "',0);return false;");


                    ((DropDownList)items.FindControl("ddl_pending")).Attributes.Add("onchange", "javascript: setTableRows('" + ((DropDownList)items.FindControl("ddl_pending")).ClientID + "');");

                    ((CheckBox)items.FindControl("chk_Ret")).Attributes.Add("onclick", "javascript: setTableRowCauseDate_2('" + ((CheckBox)items.FindControl("chk_Ret")).ClientID + "');");
                    ((CheckBox)items.FindControl("chk_DSC")).Attributes.Add("onclick", "javascript: setTableRowDSC_2('" + ((CheckBox)items.FindControl("chk_DSC")).ClientID + "');");
                    ((CheckBox)items.FindControl("chk_ShowCause")).Attributes.Add("onclick", "javascript: setTableRowShowCause_2('" + ((CheckBox)items.FindControl("chk_ShowCause")).ClientID + "');");
                    ((CheckBox)items.FindControl("chk_Misc")).Attributes.Add("onclick", "javascript: setTableRowMiscComments_2('" + ((CheckBox)items.FindControl("chk_Misc")).ClientID + "');");

                    // Added By Zeeshan Ahmed On 7th August 2007 For DSC Modification
                    // Getting CourtDate

                    //Kazim 3252 2/25/2008
                    //First Parse the Court Date, If it is not parsed correctly then assign Court Date='1/1/1900'

                    DateTime dt;
                    string temp = ((HiddenField)items.FindControl("hf_courtdate")).Value;
                    if (!string.IsNullOrEmpty(temp))
                    {
                        dt = DateTime.Parse(((HiddenField)items.FindControl("hf_courtdate")).Value);

                    }
                    else
                    {
                        dt = Convert.ToDateTime("1/1/1900");
                    }


                    string day = dt.Day.ToString(), month = dt.Month.ToString(), year = dt.Year.ToString();

                    ((TextBox)items.FindControl("txt_TDuration")).Attributes.Add("onBlur", "javascript: setTableRowDate('" + ((TextBox)items.FindControl("txt_TDuration")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PAMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PADay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PAYear")).ClientID + "',''," + day + "," + month + "," + year + ");");

                    ((TextBox)items.FindControl("txt_PDuration")).Attributes.Add("onBlur", "javascript: setTableRowDate('" + ((TextBox)items.FindControl("txt_PDuration")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTDay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTYear")).ClientID + "',''," + day + "," + month + "," + year + ");");

                    ((DropDownList)items.FindControl("ddl_PAMonth")).Attributes.Add("onchange", "javascript: setDurText('" + ((DropDownList)items.FindControl("ddl_PAMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PADay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PAYear")).ClientID + "','" + ((TextBox)items.FindControl("txt_TDuration")).ClientID + "');");

                    ((DropDownList)items.FindControl("ddl_PTMonth")).Attributes.Add("onchange", "javascript: setDurText('" + ((DropDownList)items.FindControl("ddl_PTMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTDay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTYear")).ClientID + "','" + ((TextBox)items.FindControl("txt_PDuration")).ClientID + "');");

                    ((DropDownList)items.FindControl("ddl_PADay")).Attributes.Add("onchange", "javascript: setDurText('" + ((DropDownList)items.FindControl("ddl_PAMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PADay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PAYear")).ClientID + "','" + ((TextBox)items.FindControl("txt_TDuration")).ClientID + "');");

                    ((DropDownList)items.FindControl("ddl_PTDay")).Attributes.Add("onchange", "javascript: setDurText('" + ((DropDownList)items.FindControl("ddl_PTMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTDay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTYear")).ClientID + "','" + ((TextBox)items.FindControl("txt_PDuration")).ClientID + "');");

                    ((DropDownList)items.FindControl("ddl_PAYear")).Attributes.Add("onchange", "javascript: setDurText('" + ((DropDownList)items.FindControl("ddl_PAMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PADay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PAYear")).ClientID + "','" + ((TextBox)items.FindControl("txt_TDuration")).ClientID + "');");

                    ((DropDownList)items.FindControl("ddl_PTYear")).Attributes.Add("onchange", "javascript: setDurText('" + ((DropDownList)items.FindControl("ddl_PTMonth")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTDay")).ClientID + "' , '" + ((DropDownList)items.FindControl("ddl_PTYear")).ClientID + "','" + ((TextBox)items.FindControl("txt_PDuration")).ClientID + "');");
                    //-----Added by M.Azwar Alam on 27 August 2007----------------------
                    ((HtmlImage)items.FindControl("Imgtoggle")).Attributes.Add("onclick", "return DisplayTogglePa('" + ((DropDownList)items.FindControl("ddlcoveringfirms")).ClientID + "','" + ((HtmlImage)items.FindControl("Imgtoggle")).ClientID + "','" + ((Label)items.FindControl("lblfirmname")).ClientID + "');");
                    //------------------------------------------------------------------

                    #endregion
                }
            }
            catch (Exception ex)
            {
                test.Text = ex.Message;

                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }
        }


        // Update Case Information
        private void btn_UpdateInfo_Click(object sender, System.EventArgs e)
        {
            try
            {
                
                test.Text = "";
                //IDbTransaction trans = ClsDb.DBBeginAndReturnTransaction();
                ClsDb.DBBeginTransaction();
                //if  (UpdateCaseDispositionInfo(trans) == true)
                if (UpdateCaseDispositionInfo() == true)
                {
                    //if (UpdateCaseDispositionDetails(trans) == true)
                    if (UpdateCaseDispositionDetails() == true)
                    {
                        ClsDb.DBTransactionCommit();

                        FillDateDropDowns();
                        PopulateRecords();
                        FillinGrid();
                    }
                    //Aziz 1974 06/20/08 Update all violations in the calender
                    SharePointWebWrapper.UpdateAllViolationsInCalendar(this.TicketId);
                }
                //Faique Ali 10069 09/02/2013 to maintain ddl_pending dropdown state
                //Fahad 10069 06/21/2013 Fixed the issue... 
                //PopulateRecords();
            }
            catch (Exception ex)
            {
                ClsDb.DBTransactionRollback();
                test.Text = ex.Message;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //private bool UpdateCaseDispositionInfo(IDbTransaction trans)
        private bool UpdateCaseDispositionInfo()
        {
            try
            {
                ClsCase.TicketID = this.TicketId;
                //ClsCase.GeneralComments = txt_GeneralComments.Text ;
                //ClsCase.TrialComments = txt_TrialComments.Text ;
                ClsCase.FirmID = Convert.ToInt32(HFMainFirmID.Value);
                ClsCase.EmpID = this.EmpId;
                //if (ClsCase.UpdateDispositionInfo(trans) == true)


                if (ClsCase.UpdateDispositionInfo() == true && SaveComments() == true)
                {
                    return true;
                }
                else
                {
                    return false;

                }

            }
            catch (Exception e_updateinfo)
            {
                test.Text = e_updateinfo.Message;
                bugTracker.ErrorLog(e_updateinfo.Message, e_updateinfo.Source, e_updateinfo.TargetSite.ToString(), e_updateinfo.StackTrace);

                return false;
            }
        }
        //Save comments
        private bool SaveComments()
        {
            try
            {
                WCC_GeneralComments.AddComments();
                WCC_TrialComments.AddComments();
                return true;
            }
            catch (Exception e_updateinfo)
            {
                test.Text = e_updateinfo.Message;
                bugTracker.ErrorLog(e_updateinfo.Message, e_updateinfo.Source, e_updateinfo.TargetSite.ToString(), e_updateinfo.StackTrace);

                return false;
            }
        }
        //private bool UpdateCaseDispositionDetails(IDbTransaction trans)
        private bool UpdateCaseDispositionDetails()
        {
            try
            {
                SaveComments();
                foreach (DataGridItem items in dg_Details.Items)
                {
                    ClsDetail.EmpID = this.EmpId;

                    ClsDetail.ViolationStatusID = Convert.ToInt32(((DropDownList)items.FindControl("ddl_Pending")).SelectedValue);

                    if (ClsDetail.ViolationStatusID.ToString() == "18")
                    {
                        ClsDetail.TicketViolationIDs = ((Label)items.FindControl("lb_TicketsViolationID")).Text;

                        ClsDetail.IsRETSelected = (retvalueCheckbox(((CheckBox)items.FindControl("chk_Ret"))));
                        ClsDetail.IsDSCSelected = (retvalueCheckbox(((CheckBox)items.FindControl("chk_Dsc"))));
                        ClsDetail.IsInsSelected = (retvalueCheckbox(((CheckBox)items.FindControl("chk_Ins"))));
                        ClsDetail.IsShowCauseSelected = (retvalueCheckbox(((CheckBox)items.FindControl("chk_ShowCause"))));
                        ClsDetail.IsMISCSelected = (retvalueCheckbox(((CheckBox)items.FindControl("chk_Misc"))));

                        if (ClsDetail.IsRETSelected == 1)
                        {
                            String dtRet = (((DropDownList)items.FindControl("ddl_CauseMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_CauseDay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_CauseYear")).SelectedValue);
                            if (dtRet == "---/--/----")
                                dtRet = "01/01/1900";

                            String tmRet = ((DropDownList)items.FindControl("ddl_CauseTime")).SelectedValue;
                            if (tmRet == "00:0")
                                tmRet = "00:00";
                            ClsDetail.RetDate = calDate(Convert.ToDateTime(dtRet), tmRet);
                            ClsDetail.RetCourtNumber = Convert.ToInt32(((TextBox)items.FindControl("txt_CauseCrtNo")).Text.ToString());
                            //	ClsDetail.RecDate = Convert.ToDateTime(((DropDownList) items.FindControl("ddl_CauseMonth")).SelectedValue + "/" + ((DropDownList) items.FindControl("ddl_CauseDay")).SelectedValue + "/" + ((DropDownList) items.FindControl("ddl_CauseYear")).SelectedValue )  ;
                        }
                        else
                        {
                            DateTime dtC = new DateTime(1900, 01, 01, 00, 00, 00);
                            ClsDetail.RetDate = dtC;
                            ClsDetail.RetCourtNumber = 0;
                        }

                        if (ClsDetail.IsShowCauseSelected == 1)
                        {
                            string dtShow = (((DropDownList)items.FindControl("ddl_ShowMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_ShowDay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_ShowYear")).SelectedValue);
                            if (dtShow == "---/--/----")
                                dtShow = "01/01/1900";

                            string tmShow = ((DropDownList)items.FindControl("ddl_ShowTime")).SelectedValue;
                            if (tmShow == "00:0")
                                tmShow = "00:00";
                            ClsDetail.ShowCauseDate = calDate(Convert.ToDateTime(dtShow), tmShow);
                            ClsDetail.ShowCauseCourtNumber = Convert.ToInt32(((TextBox)items.FindControl("txt_ShowCrtNo")).Text.ToString());
                            //ClsDetail.ShowCauseDate = Convert.ToDateTime(((DropDownList) items.FindControl("ddl_ShowMonth")).SelectedValue + "/" + ((DropDownList) items.FindControl("ddl_ShowDay")).SelectedValue + "/" + ((DropDownList) items.FindControl("ddl_ShowYear")).SelectedValue )  ;
                        }
                        else
                        {
                            DateTime dtC = new DateTime(1900, 01, 01, 00, 00, 00);
                            ClsDetail.ShowCauseDate = dtC;
                            ClsDetail.ShowCauseCourtNumber = 0;

                        }

                        if (ClsDetail.IsDSCSelected == 1)
                        {
                            ClsDetail.CourseCompletionDays = Convert.ToInt32(((TextBox)items.FindControl("txt_DSCDay1")).Text);
                            ClsDetail.DrivingRecordsDays = Convert.ToInt32(((TextBox)items.FindControl("txt_DSCDay2")).Text);
                        }
                        else
                        {
                            ClsDetail.CourseCompletionDays = 0;
                            ClsDetail.DrivingRecordsDays = 0;
                        }

                        if (ClsDetail.IsMISCSelected == 1)
                        {
                            ClsDetail.MISCComments = ((TextBox)items.FindControl("txt_Misc")).Text;
                        }
                        else
                        {
                            ClsDetail.MISCComments = "";
                        }
                        //ClsDetail.IsDSCSelected  = (retvalueCheckbox(((CheckBox) items.FindControl("chk_Ret")))) ; 		


                        string sTimeToPayDate = ((DropDownList)items.FindControl("ddl_PTMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PTDay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PTYear")).SelectedValue;
                        if (sTimeToPayDate == "---/--/----")
                            sTimeToPayDate = "01/01/1900";
                        ClsDetail.TimeToPayDate = Convert.ToDateTime(sTimeToPayDate);


                        ClsDetail.TimeToPay = Convert.ToInt32(((TextBox)items.FindControl("txt_PDuration")).Text);

                        string sDSCProbationDate = ((DropDownList)items.FindControl("ddl_PAMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PADay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PAYear")).SelectedValue;
                        if (sDSCProbationDate == "---/--/----")
                            sDSCProbationDate = "01/01/1900";

                        ClsDetail.DSCProbationDate = Convert.ToDateTime(sDSCProbationDate);
                        ClsDetail.DSCProbationTime = Convert.ToInt32(((TextBox)items.FindControl("txt_TDuration")).Text);
                        ClsDetail.DSCProbationAmount = Convert.ToDouble(((TextBox)items.FindControl("txt_TAmount")).Text);

                    }
                    else if (ClsDetail.ViolationStatusID.ToString() == "19")
                    {
                        ClsDetail.TicketViolationIDs = ((Label)items.FindControl("lb_TicketsViolationID")).Text;
                        ClsDetail.MISCComments = "";

                        ClsDetail.IsDSCSelected = 0;

                        DateTime dtC = new DateTime(1900, 01, 01, 00, 00, 00);
                        ClsDetail.ShowCauseDate = dtC;

                        ClsDetail.IsInsSelected = 0;
                        ClsDetail.IsShowCauseSelected = 0;
                        ClsDetail.RetDate = dtC;

                        ClsDetail.IsMISCSelected = 0;
                        ClsDetail.IsRETSelected = 0;

                        ClsDetail.TimeToPayDate = dtC;
                        ClsDetail.TimeToPay = 0;
                        string sDSCProbationDate = ((DropDownList)items.FindControl("ddl_PAMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PADay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PAYear")).SelectedValue;
                        if (sDSCProbationDate == "---/--/----")
                            sDSCProbationDate = "01/01/1900";

                        ClsDetail.DSCProbationDate = Convert.ToDateTime(sDSCProbationDate);
                        ClsDetail.DSCProbationTime = Convert.ToInt32(((TextBox)items.FindControl("txt_TDuration")).Text);
                        ClsDetail.DSCProbationAmount = Convert.ToDouble(((TextBox)items.FindControl("txt_TAmount")).Text);
                        ClsDetail.CourseCompletionDays = 0;
                        ClsDetail.DrivingRecordsDays = 0;
                        ClsDetail.ShowCauseCourtNumber = 0;
                        ClsDetail.RetCourtNumber = 0;
                    }
                    else if (ClsDetail.ViolationStatusID.ToString() == "9")
                    {
                        ClsDetail.TicketViolationIDs = ((Label)items.FindControl("lb_TicketsViolationID")).Text;
                        ClsDetail.MISCComments = "";

                        ClsDetail.IsDSCSelected = 0;
                        DateTime dtC = new DateTime(1900, 01, 01, 00, 00, 00);
                        ClsDetail.ShowCauseDate = dtC;
                        ClsDetail.IsInsSelected = 0;
                        ClsDetail.IsShowCauseSelected = 0;
                        ClsDetail.RetDate = dtC;
                        ClsDetail.IsMISCSelected = 0;
                        ClsDetail.IsRETSelected = 0;


                        string sTimeToPayDate = ((DropDownList)items.FindControl("ddl_PTMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PTDay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PTYear")).SelectedValue;
                        if (sTimeToPayDate == "---/--/----")
                            sTimeToPayDate = "01/01/1900";
                        ClsDetail.TimeToPayDate = Convert.ToDateTime(sTimeToPayDate);
                        ClsDetail.TimeToPay = Convert.ToInt32(((TextBox)items.FindControl("txt_PDuration")).Text);
                        string sDSCProbationDate = ((DropDownList)items.FindControl("ddl_PAMonth")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PADay")).SelectedValue + "/" + ((DropDownList)items.FindControl("ddl_PAYear")).SelectedValue;
                        if (sDSCProbationDate == "---/--/----")
                            sDSCProbationDate = "01/01/1900";
                        ClsDetail.DSCProbationDate = Convert.ToDateTime(sDSCProbationDate);
                        ClsDetail.DSCProbationTime = Convert.ToInt32(((TextBox)items.FindControl("txt_TDuration")).Text);
                        ClsDetail.DSCProbationAmount = Convert.ToDouble(((TextBox)items.FindControl("txt_TAmount")).Text);
                        ClsDetail.CourseCompletionDays = 0;
                        ClsDetail.DrivingRecordsDays = 0;
                        ClsDetail.ShowCauseCourtNumber = 0;
                        ClsDetail.RetCourtNumber = 0;
                    }
                    else
                    {
                        ClsDetail.TicketViolationIDs = ((Label)items.FindControl("lb_TicketsViolationID")).Text;
                        ClsDetail.MISCComments = "";

                        ClsDetail.IsDSCSelected = 0;
                        DateTime dtC = new DateTime(1900, 01, 01, 00, 00, 00);
                        ClsDetail.ShowCauseDate = dtC;
                        ClsDetail.IsInsSelected = 0;
                        ClsDetail.IsShowCauseSelected = 0;
                        ClsDetail.RetDate = dtC;
                        ClsDetail.IsMISCSelected = 0;
                        ClsDetail.IsRETSelected = 0;

                        ClsDetail.TimeToPayDate = dtC;
                        ClsDetail.TimeToPay = 0;
                        ClsDetail.DSCProbationDate = dtC;
                        ClsDetail.DSCProbationTime = 0;
                        ClsDetail.DSCProbationAmount = 0;
                        ClsDetail.CourseCompletionDays = 0;
                        ClsDetail.DrivingRecordsDays = 0;
                        ClsDetail.ShowCauseCourtNumber = 0;
                        ClsDetail.RetCourtNumber = 0;
                    }
                    //------Added by M.Azwar Alam on 27 August 2007 for task no 1016---------------------
                    //----------Updating Coverage Firm against each violation----------------------------
                    //----------if Firm is disable then update previous firm otherwise update from dropdown------------



                    ClsDetail.FirmID = Convert.ToInt32(((HiddenField)items.FindControl("HFCoveringFirmID")).Value);

                    //---------------------------------------------------------------------------------- 
                    //if (ClsDetail.UpdateDispositionInfoDetail(trans) != true)
                    if (ClsDetail.UpdateDispositionInfoDetail() != true)
                    {
                        return false;
                    }
                }
                return true;
                
            }
            catch (Exception ex)
            {
                test.Text = ex.Message.ToString();
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }


        }

        private int retvalueCheckbox(CheckBox chk)
        {
            if (chk.Checked == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        private DateTime calDate(DateTime sDate, string strTime)
        {
            string[] strTimeH = strTime.Split(':');
            //double dHours = (double) (Convert.ChangeType(strTime.Substring(0,2),typeof(double))) ;
            //double dMinutes = (double) (Convert.ChangeType(strTime.Substring(3,2),typeof(double))) ;
            double dHours = (double)(Convert.ChangeType(strTimeH[0], typeof(double)));
            double dMinutes = (double)(Convert.ChangeType(strTimeH[1], typeof(double)));
            sDate = sDate.AddHours(dHours);
            sDate = sDate.AddMinutes(dMinutes);


            return sDate;
        }

        private void PopulateRecords()
        {
            //Getting information in the Labels 

            DataSet ds_DispositionInfo = ClsCase.GetDispositionInfo(this.TicketId);

            if (ds_DispositionInfo == null)
            {
                lb_LastName.Text = "Unable to get case record from database";
                //lblMessage.Visible = true;
                return;
            }
            lbl_CaseCount.Text = ds_DispositionInfo.Tables[0].Rows[0]["CaseCount"].ToString().Trim();
            lb_LastName.Text = ds_DispositionInfo.Tables[0].Rows[0]["LastName"].ToString().Trim();
            lb_firstName.Text = ds_DispositionInfo.Tables[0].Rows[0]["FirstName"].ToString().Trim();
            lbl_CaseStatus.Text = ds_DispositionInfo.Tables[0].Rows[0]["description"].ToString().Trim();
            hlk_MIDNo.Text = ds_DispositionInfo.Tables[0].Rows[0]["Midnum"].ToString().Trim();
            //hlk_MIDNo.NavigateUrl = "../frmmain.aspx?search=" + ClsSession.GetCookie("sSearch",this.Request) + "&lstcriteriaValue3=" + ds_DispositionInfo.Tables[0].Rows[0]["MidNum"].ToString().Trim() + "&lstcriteria3=3";
            hlk_MIDNo.NavigateUrl = "../frmmain.aspx?search=" + Request.QueryString["search"].ToString() + "&lstcriteriaValue3=" + ds_DispositionInfo.Tables[0].Rows[0]["MidNum"].ToString().Trim() + "&lstcriteria3=3";



            lbl_Court.Text = ds_DispositionInfo.Tables[0].Rows[0]["CurrentCourtloc"].ToString().Trim();
            DateTime Dt_Data = new DateTime();
            Dt_Data = Convert.ToDateTime(ds_DispositionInfo.Tables[0].Rows[0]["CurrentDateSet"].ToString().Trim());
            lbl_CrtDate.Text = Dt_Data.Month.ToString() + "/" + Dt_Data.Day.ToString().Trim() + "/" + Dt_Data.Year.ToString().Trim();
            lbl_Crttime.Text = Dt_Data.TimeOfDay.ToString().Trim().Substring(0, 5) + " " + ds_DispositionInfo.Tables[0].Rows[0]["CurrentDateSet"].ToString().Substring(ds_DispositionInfo.Tables[0].Rows[0]["CurrentDateSet"].ToString().Length - 2);
            lbl_CrtNo.Text = ds_DispositionInfo.Tables[0].Rows[0]["CurrentCourtNum"].ToString().Trim();
            //Faique Ali 10069 06/19/2013 Setting both comment control in same order..
            WCC_TrialComments.Initialize(this.TicketId, this.EmpId, 4, "Label", "clsinputadministration", "clsbutton", true);
            WCC_GeneralComments.Initialize(this.TicketId, this.EmpId, 1, "Label", "clsinputadministration", "clsbutton", "Label", true);
            WCC_GeneralComments.btn_AddComments.Visible = false;
            WCC_GeneralComments.Button_Visible = true;
            

            WCC_TrialComments.btn_AddComments.Visible = false;
            WCC_TrialComments.Button_Visible = false;
            //Nasir 6098 08/18/2009 visifalse check box for trial comments
            WCC_TrialComments.chk_Complaint.Visible = false;
            WCC_TrialComments.lbl_Check_Complaint.Visible = false;

            // Getting Detail Information in DataGrid.
            DataSet ds = ClsDetail.GetDispositionInfoDetail(this.TicketId);
            dg_Details.DataSource = ds;
            dg_Details.DataBind();

            // Noufil  3479 03/25/2008 
            // Getting AutoHistory record in grid
            DataTable dt = ClsCase.GetAutodisp(this.TicketId);
            dg_autohistory.DataSource = dt;
            dg_autohistory.DataBind();



        }

        private DateTime calDate(string strDate, string strTime)
        {
            DateTime tempdt = Convert.ToDateTime(strDate);
            string newSubDate = tempdt.Date.ToString();
            String newDate;
            newDate = newSubDate + " " + strTime.ToString();

            DateTime dtTime;
            //DateTime dtDate ;
            dtTime = (DateTime)(Convert.ChangeType(newDate, typeof(System.DateTime)));


            return dtTime;
        }

        private void FillDateDropDowns()
        {

            arrMonth[0, 0] = ("---");
            arrMonth[1, 0] = ("Jan");
            arrMonth[1, 1] = "1";
            arrMonth[2, 0] = ("Feb");
            arrMonth[2, 1] = "2";
            arrMonth[3, 0] = ("Mar");
            arrMonth[3, 1] = "3";
            arrMonth[4, 0] = ("Apr");
            arrMonth[4, 1] = "4";
            arrMonth[5, 0] = ("May");
            arrMonth[5, 1] = "5";
            arrMonth[6, 0] = ("Jun");
            arrMonth[6, 1] = "6";
            arrMonth[7, 0] = ("Jul");
            arrMonth[7, 1] = "7";
            arrMonth[8, 0] = ("Aug");
            arrMonth[8, 1] = "8";
            arrMonth[9, 0] = ("Sep");
            arrMonth[9, 1] = "9";
            arrMonth[10, 0] = ("Oct");
            arrMonth[10, 1] = "10";
            arrMonth[11, 0] = ("Nov");
            arrMonth[11, 1] = "11";
            arrMonth[12, 0] = ("Dec");
            arrMonth[12, 1] = "12";





        }

        private void CheckReadNotes(int ticketid)
        {
            bool checkflag = false;
            checkflag = notes.CheckReadNoteComments(ticketid);
            if (checkflag == true)
            {
                read.Visible = true;
               // read.Style[HtmlTextWriterStyle.Display] = "table-row";

            }
            else
            {
                read.Visible = false;
                // read.Style[HtmlTextWriterStyle.Display] = "none";
            }


        }

        private void FillTimeDDL(DropDownList ddl_CrtTime)
        {
            Int32 i;
            ddl_CrtTime.Items.Add("--------");
            ddl_CrtTime.Items[0].Value = "00:0";

            ddl_CrtTime.Items.Add("8:00 am");
            ddl_CrtTime.Items[1].Value = ("8:0");
            ddl_CrtTime.Items.Add("8:15 am");
            ddl_CrtTime.Items[2].Value = ("8:15");
            ddl_CrtTime.Items.Add("8:30 am");
            ddl_CrtTime.Items[3].Value = ("8:30");
            ddl_CrtTime.Items.Add("8:45 am");
            ddl_CrtTime.Items[4].Value = ("8:45");

            ddl_CrtTime.Items.Add("9:00 am");
            ddl_CrtTime.Items[5].Value = ("9:0");
            ddl_CrtTime.Items.Add("9:15 am");
            ddl_CrtTime.Items[6].Value = ("9:15");
            ddl_CrtTime.Items.Add("9:30 am");
            ddl_CrtTime.Items[7].Value = ("9:30");
            ddl_CrtTime.Items.Add("9:45 am");
            ddl_CrtTime.Items[8].Value = ("9:45");

            Int32 iTime = 9;
            for (i = 10; i < 12; i++)
            {
                ddl_CrtTime.Items.Add(i.ToString() + ":00 am");
                ddl_CrtTime.Items[iTime].Value = (i.ToString() + ":0");
                ddl_CrtTime.Items.Add(i.ToString() + ":15 am");
                ddl_CrtTime.Items[iTime += 1].Value = (i.ToString() + ":15");
                ddl_CrtTime.Items.Add(i.ToString() + ":30 am");
                ddl_CrtTime.Items[iTime += 1].Value = (i.ToString() + ":30");
                ddl_CrtTime.Items.Add(i.ToString() + ":45 am");
                ddl_CrtTime.Items[iTime += 1].Value = (i.ToString() + ":45");
                iTime += 1;
            }


            ddl_CrtTime.Items.Add("12:00 pm");
            ddl_CrtTime.Items[iTime].Value = ("12:0");
            ddl_CrtTime.Items.Add("12:15 pm");
            ddl_CrtTime.Items[iTime += 1].Value = ("12:15");
            ddl_CrtTime.Items.Add("12:30 pm");
            ddl_CrtTime.Items[iTime += 1].Value = ("12:30");
            ddl_CrtTime.Items.Add("12:45 pm");
            ddl_CrtTime.Items[iTime += 1].Value = ("12:45");
            iTime += 1;



            Int32 iTimeClock = 12;
            for (i = 1; i < 9; i++)
            {
                ddl_CrtTime.Items.Add(i.ToString() + ":00 pm");
                iTimeClock += 1;
                ddl_CrtTime.Items[iTime].Value = (iTimeClock.ToString() + ":0");
                ddl_CrtTime.Items.Add(i.ToString() + ":15 pm");
                ddl_CrtTime.Items[iTime += 1].Value = (iTimeClock.ToString() + ":15");
                ddl_CrtTime.Items.Add(i.ToString() + ":30 pm");
                ddl_CrtTime.Items[iTime += 1].Value = (iTimeClock.ToString() + ":30");
                ddl_CrtTime.Items.Add(i.ToString() + ":45 pm");
                ddl_CrtTime.Items[iTime += 1].Value = (iTimeClock.ToString() + ":45");
                iTime += 1;
            }
            ddl_CrtTime.Items.Add("9:00 pm");
            iTimeClock += 1;
            ddl_CrtTime.Items[iTime].Value = (iTimeClock.ToString() + ":00");



            ddl_CrtTime.SelectedIndex = 0;

        }

    }
}





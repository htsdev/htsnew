﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using MSXML2;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using WebSupergoo.ABCpdf6;
using System.IO;

namespace HTP.ClientInfo
{
    // Noufil 5807 05/29/2009 Created
    public partial class BondInformationFromCourt : System.Web.UI.Page
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsCase ClsCase = new clsCase();
        System.Text.UTF8Encoding str = new System.Text.UTF8Encoding();
        Doc theDoc = new Doc();
        Doc doc1 = new Doc();
        Doc doc2 = new Doc();
        //Yasir kamal 7058 07/12/2009 save voilation description.
        string ResponseBondHTML = string.Empty;
        string ResponseViolHTML = string.Empty;
        string ResponseHTML = string.Empty;
        string[] ResponseTextArray;
        int ticketid = 0;
        int Empid = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                //if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["TicketNumber"])))
                // {
                string url = "http://www.jp.hctx.net/PublicAccess/SearchValidator";
                ticketid = Convert.ToInt32(Request.QueryString["Ticketid"].ToString().Substring(0, Request.QueryString["Ticketid"].ToString().IndexOf('-')));
                Empid = Convert.ToInt32(Request.QueryString["Empid"]);

                if (ticketid > 0 && Empid > 0)
                {

                    DataTable dtpaymnet = new DataTable();
                    dtpaymnet = ClsCase.GetTicketNumber(ticketid);
                    ResponseTextArray = new string[dtpaymnet.Rows.Count];
                    int count = 0;

                    foreach (DataRow dr in dtpaymnet.Rows)
                    {
                        string ticketnumber = (Convert.ToString(dr["TicketNumber"]).Trim() == "N/A") ? "" : Convert.ToString(dr["TicketNumber"]);
                        count = count + 1;
                        XMLHTTP ObjHttp = new XMLHTTP();
                        ObjHttp.open("Post", url, false, null, null);
                        //ObjHttp.send(null);
                        ObjHttp.send("caseNumber=" + ticketnumber + "&dl=&lname=&dob=&court=00&language=en&Submit=Search for Cases");

                        if (ObjHttp.status.ToString() == "200")
                        {
                            ResponseHTML = ObjHttp.responseText;
                            ResponseBondHTML = ResponseViolHTML= ResponseHTML;
                            
                            ObjHttp.abort();
                            ResponseTextArray[count - 1] = ResponseHTML;

                            if (!ResponseHTML.Contains("NOTE: Criminal cases filed before January 1, 2003 are not displayed by this search."))
                            {
                                if (ResponseBondHTML.Contains("Bail:"))
                                    ResponseBondHTML = ResponseBondHTML.Substring(ResponseBondHTML.IndexOf("Bail:") + 5);
                                if (ResponseBondHTML.Contains("<td>"))
                                    ResponseBondHTML = ResponseBondHTML.Replace("<td>", "");
                                if (ResponseBondHTML.Contains("</b>"))
                                    ResponseBondHTML = ResponseBondHTML.Replace("</b>", "");
                                if (ResponseBondHTML.Contains("\t"))
                                    ResponseBondHTML = ResponseBondHTML.Replace("\t", "");
                                if (ResponseBondHTML.Contains("</td>"))
                                    ResponseBondHTML = ResponseBondHTML.Replace("</td>", "");
                                if (ResponseBondHTML.Contains("$"))
                                    ResponseBondHTML = ResponseBondHTML.Replace("$", "");
                                if (ResponseBondHTML.Contains("</tr>"))
                                    ResponseBondHTML = ResponseBondHTML.Substring(0, ResponseBondHTML.IndexOf("</tr>"));

                                if (ResponseViolHTML.Contains("Offense:"))
                                    ResponseViolHTML = ResponseViolHTML.Substring(ResponseViolHTML.IndexOf("Offense:") + 5);
                                if (ResponseViolHTML.Contains("<td>"))
                                    ResponseViolHTML = ResponseViolHTML.Replace("<td>", "");
                                if (ResponseViolHTML.Contains("</b>"))
                                    ResponseViolHTML = ResponseViolHTML.Replace("</b>", "");
                                if (ResponseViolHTML.Contains("\t"))
                                    ResponseViolHTML = ResponseViolHTML.Replace("\t", "");
                                if (ResponseViolHTML.Contains("</td>"))
                                    ResponseViolHTML = ResponseViolHTML.Replace("</td>", "");
                                if (ResponseViolHTML.Contains("$"))
                                    ResponseViolHTML = ResponseViolHTML.Replace("$", "");
                                if (ResponseViolHTML.Contains("se:"))
                                    ResponseViolHTML = ResponseViolHTML.Replace("se:", "");
                                if (ResponseViolHTML.Contains("</tr>"))
                                    ResponseViolHTML = ResponseViolHTML.Substring(0, ResponseViolHTML.IndexOf("</tr>"));

                                try
                                {
                                    if ( Convert.ToDouble(ResponseBondHTML.Trim()) > 0)
                                    {
                                        string[] key = { "@ticketid_pk", "@casenumber", "@bondamount","@violDesp" };
                                        object[] value = { ticketid, Convert.ToString(dr["TicketNumber"]), Convert.ToDouble(ResponseBondHTML.Trim()), ResponseViolHTML };
                                        clsdb.ExecuteSP("USP_HTP_Update_BondAmount", key, value);
                                    }
                                    Literal1.Text += Convert.ToString(dr["TicketNumber"]) + ":" + Convert.ToString(Convert.ToDouble(ResponseBondHTML.Trim())) + "$ ";
                                }
                                catch
                                {
                                    Literal1.Text += Convert.ToString(dr["TicketNumber"]) + ":0$ ";
                                }
                            }
                            else
                                Literal1.Text += Convert.ToString(dr["TicketNumber"]) + ":0$ ";
                        }
                        else
                        {
                            Literal1.Text = "Error: Error getting response from the web site.";
                        }
                    }
                    Literal1.Text += "Done";
                    SaveHTMLasPDF(ResponseTextArray);
                }
                else
                    Literal1.Text = "Error: Ticketid Not Found";
            }
            catch (Exception ex)
            {
                Literal1.Text = "Error: " + ex.Message;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            ////string url = "http://www.jp.hctx.net/PublicAccess/welcome.jsp";
            ////string url = "http://www.yahoo.com";
            //string url = "http://www.jp.hctx.net/PublicAccess/SearchValidator";
            //XMLHTTP ObjHttp = new XMLHTTP();
            //ObjHttp.open("Post", url, false, null, null);
            ////ObjHttp.send("language=en");
            ////ObjHttp.send(null);
            //ObjHttp.send("caseNumber=BC12C0232872&dl=&lname=&dob=&court=00&language=en&Submit=Search for Cases");

            //if (ObjHttp.status.ToString() == "200")
            //{
            //    Literal1.Text = "Ticketnumber :" + txt_ticketnumber.Text + "<br/>" + ObjHttp.responseText;
            //}
            //else
            //{
            //    Literal1.Text = "Error getting response from the web site.";
            //}
        }


        private bool SaveHTMLasPDF(string[] Text)
        {
            theDoc.Rect.Inset(35,40);
            int theID = 0;

            foreach (string strr in Text)
            {
                theDoc.Page = theDoc.AddPage();
                //theDoc.AddHtml(strr);
                //Fahad 6202 08/04/2009 Used AddImageHtml as Clone Copy of web page
                theDoc.AddImageHtml(strr, true,1000, true);
                //theDoc.AddImageHtml(strr);                
            }
            //theID = theDoc.AddImageUrl("http://www.yahoo.com/");
            while (true)
            {
                //theDoc.FrameRect(); // add a black border
                if (!theDoc.Chainable(theID))
                    break;
                theDoc.Page = theDoc.AddPage();
                theID = theDoc.AddImageToChain(theID);
            }
            for (int i = 1; i <= theDoc.PageCount; i++)
            {
                theDoc.PageNumber = i;
                theDoc.Flatten();
            }

            string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
            object[] value1 = { DateTime.Now, "PDF", "HCJP Court Website snapshot Saved", "Other", 0, Empid, ticketid, theDoc.PageCount, 0, "Upload", "" };
            //call sP and get the book ID back from sP
            string picName = clsdb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
            string picDestination = Convert.ToString(ConfigurationManager.AppSettings["NTPATHScanImage"]) + picName + ".pdf";
            string localfilepath = "C://Temp//" + DateTime.Today.ToFileTime() + ".pdf";
            theDoc.Save(localfilepath);
            theDoc.Clear();
            File.Copy(localfilepath, picDestination);
            return true;
        }
    }
}

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingSignature.aspx.cs" Inherits="lntechNew.ClientInfo.PendingSignature" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
<head>
    <title>Pending Signature</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />  
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>


    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
            <tr>
                <td>
                  <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" colSpan="6" height="11">
                </td>
            </tr>
            <tr>
                <td  colspan="6" height="11" style="text-align: center">
                    <asp:Label ID="lblMessage" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                        ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34px">&nbsp;Pending Signatures</td>
            </tr>
            <tr>
                
            </tr>
            <tr>
                <td >
                    <asp:GridView ID="gv_UnsignedDocuments" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable" Width="780px">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_UnsignedSelect" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Document Type">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_DocumentName" runat="server" CssClass="Labelfrmmain" Text="<%# Container.DataItem %>" ></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pages">
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td >
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Button ID="btn_DocUSignViewer" runat="server" Text="Doc-U-Sign Viewer" CssClass="clsbutton" />
                    <asp:Button ID="btn_Delete" runat="server" Text="Delete" CssClass="clsbutton" /></td>
            </tr>
            <tr>
                <td height=16 background="../../images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td class="clssubhead" background="../Images/subhead_bg.gif" colSpan="2" height="34px">
                    Signed Documents</td>
            </tr>
            <tr>
                <td style="height: 16px"><asp:GridView ID="gv_SignedDocuments" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable" Width="780px">
                    <Columns>
                        <asp:TemplateField HeaderText="Document Type">
                            <ItemStyle Width="60%" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_SignedDocumentName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lettername") %>' CssClass="Labelfrmmain"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Signature Date &amp; Time">
                            <ItemTemplate>
                                <asp:Label ID="lbl_SignatureDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Printdate") %>' CssClass="Labelfrmmain"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Staff">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Staff" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserAbb") %>' CssClass="Labelfrmmain"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pages">
                            <ItemStyle Width="10%" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_SignedPages" runat="server" CssClass="Labelfrmmain"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="height: 16px" background="../../images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td >
                    <uc4:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

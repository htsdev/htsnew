using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using lntechNew.Components;
using HTP.WebComponents;
using HTP.Components;
using HTP.Components.ClientInfo;
using Configuration.Client;
using Configuration.Helper;
using System.Net;


//ozair 3613 on 04/02/2008
namespace HTP.ClientInfo
{
    public partial class NewPaymentInfo : MatterBasePage
    {

        //Fahad 6054 07/23/2009 Declare Object of OpenServiceTickets class named ServiceTicket
        //Farrukh 9925 11/30/2011 Resharper executed
        OpenServiceTickets ServiceTicket = new OpenServiceTickets();
        clsCrsytalComponent Cr = new clsCrsytalComponent();
        NewClsPayments ClsPayments = new NewClsPayments();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        clsCase ClsCase = new clsCase();
        clsSession ClsSession = new clsSession();
        clsGeneralMethods general = new clsGeneralMethods();
        clsGeneralMethods clsGMethod = new clsGeneralMethods();
        private clsENationWebComponents cls_db = new clsENationWebComponents();
        clsReadNotes notes = new clsReadNotes();
        clsNOS nos = new clsNOS();
        ClsFlags clsFlags = new ClsFlags();
        clsCaseDetail CaseDetails = new clsCaseDetail();
        //Sabir Khan 6423 08/27/2009 added for sharepoint event.
        updatePanel updatepanel = new updatePanel();
        // Noufil 6052 09/17/2009 object created
        clsFirms cfirm = new clsFirms();
        clsCourts clsCourt = new clsCourts();
        IConfigurationClient Client = ConfigurationClientFactory.GetConfigurationClient();
        string name = "";

        Documents TrialNotRestrict = new Documents();//code Added by Azwer...

        //int TicketID;
        string Firstname;
        string Lastname;
        string paymentcomments;
        string ticketnumber;
        double ActualOwes;
        double totalfee;
        string paymenthistory;
        double totaldue;
        string _localmachinename;

        //Nasir 6181 07/28/2009  remove IsLORBatchActive property
        protected bool IsLORBatchActive
        {
            get
            {
                return (ViewState["IsLORBatchActive"] == null ? false : Convert.ToBoolean(ViewState["IsLORBatchActive"]));
            }
            set
            {
                ViewState["IsLORBatchActive"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Adil 9165 04/12/2011 Turned off ESignature feature.
                btn_ESignature.Visible = false;

                lblPasadenaJudge.Text = "";
                //Waqas 5771 04/16/2009 Checking for session
                int Empid = 0;
                if (cSession.IsValidSession(Request, Response, Session) == false ||
                        (!int.TryParse(cSession.GetCookie("sEmpID", Request), out Empid)))
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if(Request.QueryString["caseNumber"]!=null && Request.QueryString["caseNumber"].ToString() == "0")
                        Response.Redirect("ViolationFeeold.aspx?sMenu=61&search=0&caseNumber=0", false);

                    txt_creditcard.Attributes.Add("onblur", "GetFocus();return false;");
                    imgbtn_creditcard.Attributes.Add("onclick", "ShowReaderPopup();return false;");
                    txt_creditcard.Attributes.Add("onKeyPress", "return VerifyCard();");
                    btn_popupCancel.Attributes.Add("OnClick", "closereaderpopup(" + 1 + ");return false;");
                    btn_popupok.Attributes.Add("OnClick", "FillCreditInfo();return false;");

                    ClsPayments.TransType = clsGMethod.SetTransactionMode(Request);



                    lblMessage.Text = "";
                    // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                    tr_errorMessages.Visible = false;
                    //  ClsPayments = new NewClsPayments(this.Request);

                    btn_trialLetter.Attributes.Add("onClick", "return JuryTrialAlert();");
                    btn_Receipt.Attributes.Add("onclick", " return PrintLetter(" + 3 + ");");
                    btn_bond.Attributes.Add("onclick", " return PrintLetter(" + 4 + ");");
                    btn_contuance.Attributes.Add("onclick", " return PrintLetter(" + 5 + ");");
                    btn_letterofRep.Attributes.Add("onclick", " return PrintLetter(" + 6 + ");");
                    btn_emailrep.Attributes.Add("onclick", " return PrintLetter(" + 8 + ");");
                    btn_EmailTrial.Attributes.Add("onclick", " return PrintLetter(" + 9 + ");");
                    btn_Limine.Attributes.Add("onclick", " return PrintLetter(" + 11 + ");");
                    btn_Discovery.Attributes.Add("onclick", " return PrintLetter(" + 12 + ");");
                    btn_TrialSuspensionDL.Attributes.Add("onclick", " return PrintLetter(" + 16 + ");");
                    btn_TrialOccupationDL.Attributes.Add("onclick", " return PrintLetter(" + 17 + ");");

                    txtTimeStemp.Text = DateTime.Now.TimeOfDay.ToString();


                    if (!IsPostBack)
                    {
                        // Sabir Khan 10920 05/27/2013 Display branch name of the logged in user.
                        lbl_BranchName.Text = "Branch - " + ClsSession.GetCookie("BranchName", this.Request);
                        hf_CheckPaymentType.Value = ClsPayments.GetSpecificPaymentType(TicketId).ToString();  //Fahad 6054 07/24/2009 Checking Client having Friend,Partener Firm and Attorney credit.
                        hf_CheckClientonSameDate.Value = ClsPayments.GetExistingClientByCourtDateandLocation(TicketId).ToString();  //Fahad 6054 07/24/2009 Checking More Client having Same CourtDate and CourtLocation
                        lblPasadenaJudge.Text = "";


                        if (Request.QueryString.Count >= 2)
                        {
                            //this.TicketId = Request.QueryString["casenumber"];
                            ViewState["vSearch"] = Request.QueryString["search"];
                        }
                        else
                        {
                            Response.Redirect("../frmMain.aspx", false);
                            goto SearchPage;
                        }

                        // Noufil 5931 05/21/2009code commented.
                        //Fahad 5908 05/16/2009 Set Method on Client-side to open E-signature Exe
                        //btn_ESignature.Attributes.Add("OnClick", "OpenESignature();");
                        //5908

                        // Noufil 3498 04/05/2008 activte or inactive bad number label
                        clsFlags.TicketID = TicketId;
                        clsFlags.FlagID = Convert.ToInt32(FlagType.WrongNumber);

                        //Yasir Kamal 6109 07/17/2009 do not allow hiring if no court is assigned to one or more violations
                        hf_IsNoCourtAssigned.Value = ClsCase.IsNoCourtAssigned(TicketId).ToString();

                        //Yasir Kamal 6194 07/23/2009 do not allow hiring if court date in past for criminal court
                        ClsCase.TicketID = TicketId;
                        hf_isCriminalCase.Value = ClsCase.IsCriminalCase().ToString();
                        // Noufil 6164 08/11/2009 Get Case Type of Client
                        hf_casetype.Value = ClsCase.GetCaseType(TicketId).ToString();

                        //TODO: put comments
                        // clsFlags.EmpID = Convert.ToInt32(cSession.GetCookie("sEmpID", this.Request));
                        //if (!clsFlags.HasFlag())
                        //{
                        //    lblbadnumber.Visible = false;
                        //}
                        //else
                        //    lblbadnumber.Visible = true;

                        //Kazim 3768 4/18/2008 Use Empid property of Baseclass 
                        CheckFlags(TicketId);
                        clsFlags.EmpID = EmpId;
                        //lblbadnumber.Visible = (clsFlags.HasFlag() ? true : false);
                        //// Noufil  bad email label added
                        //clsFlags.FlagID = Convert.ToInt32(FlagType.bademail);
                        //lblbademail.Visible = (clsFlags.HasFlag() ? true : false);

                        //// Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                        //tr_InfoMessages.Style["display"] = lblbadnumber.Visible ? "block" : "none";
                        //tr_InfoMessages.Style["display"] = lblbademail.Visible ? "block" : "none";

                        // Noufil 5819 05/15/2009 Check if client has null arrest date with ALR violation.
                        hf_checkALRnullArrestdate.Value = ClsCase.CheckALRWithNullArrestDate(TicketId).ToString();

                        // tahir 4777 09/10/2008 added Problem client (No hire) logic....

                        // Agha 4347 07/21/2008 Implementing Problem Client
                        //clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient);
                        //lblProblemClient.Visible = (clsFlags.HasFlag() ? true : false);

                        // Noufil 6052 09/17/2009 Get Attorney info for crminal case type
                        //if (Convert.ToBoolean(hf_isCriminalCase.Value))
                        //{
                        //    GetAttorneypaidHistoryforCriminalCase();
                        //}

                        //clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_NoHire);
                        //if (clsFlags.HasFlag())
                        //{
                        //    lblProblemClient.Text = "This is a " + clsFlags.Description;
                        //    lblProblemClient.Visible = true;
                        //}
                        //else
                        //{

                        //    //ozair 4846 11/14/2008 renamed ProblemClient_Becareful to ProblemClient_AllowHire
                        //    clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_AllowHire);
                        //    if (clsFlags.HasFlag())
                        //    {
                        //        lblProblemClient.Text = "This is a " + clsFlags.Description;
                        //        lblProblemClient.Visible = true;
                        //    }
                        //    else
                        //    {
                        //        lblProblemClient.Visible = false;
                        //    }
                        //}

                        // end 4777

                        //Waqas 5864 06/23/2009 Check for arresting date to be in 15 days from today.
                        hf_checkALRArrestdateRange.Value = ClsCase.CheckALRWithArrestDateInDaysRange(TicketId).ToString();

                        ViewState["vNTPATHCaseSummary"] = ConfigurationManager.AppSettings["NTPATHCaseSummary"];
                        Session["TimeStemp"] = txtTimeStemp.Text;
                        //TicketID = Convert.ToInt32(ViewState["vTicketId"]);
                        ClsPayments.EmpID = EmpId;
                        //	TicketID=11103;
                        //	ClsPayments.EmpID=3992;
                        if (TicketId == 0)
                        {
                            Response.Redirect("ViolationFeeold.aspx?newt=1", false);
                        }
                        //for oscar court
                        string[] key_1 = { "@ticketid" };
                        object[] value_1 = { TicketId };
                        DataSet ds_os = cls_db.Get_DS_BySPArr("usp_hts_check_activeflag", key_1, value_1);
                        if (ds_os != null && ds_os.Tables[0] != null && ds_os.Tables[0].Rows.Count > 0)
                        {
                            lbl_OscarActiveFlag.Text = ds_os.Tables[0].Rows[0]["activeflag"].ToString();
                            lbl_OscarCourtID.Text = ds_os.Tables[0].Rows[0]["courtid"].ToString();
                            lbl_lockflag.Text = ds_os.Tables[0].Rows[0]["lockflag"].ToString();
                            hf_court.Value = ds_os.Tables[0].Rows[0]["courtid"].ToString();
                            lbl_courtid.Text = ds_os.Tables[0].Rows[0]["courtid"].ToString();
                            //Sabir Khan 9941 12/14/2011 Check for PMC LOR Dates. 
                            if (Convert.ToInt32(hf_court.Value) == Convert.ToInt32(clsCourts.CourtsName.Pasadena))
                            {
                                // Zeeshan Haider 11306 08/12/2013 Parameterize the below method for PMC Jury Trial Cases.
                                hf_IsLORDateInPastOrFull.Value = Convert.ToString(clsCourt.CheckLORDatesForPMC(TicketId));
                                // Zeeshan Haider 10699 04/18/2013 PMC Cases for LOR
                                hf_IsLORDateInFuture.Value = clsCourt.CompareCourtDateLORDatesPMC(TicketId);
                            }
                            else
                            {
                                hf_IsLORDateInPastOrFull.Value = "0";
                                // Zeeshan Haider 10699 04/18/2013 PMC Cases for LOR
                                hf_IsLORDateInFuture.Value = "1";
                            }
                            //Sabir Khan 9243 11/29/2011 set hidden field for active.inactive court.
                            hf_ActiveInactiveCourt.Value = ds_os.Tables[0].Rows[0]["InActiveFlag"].ToString();

                            if (lbl_lockflag.Text == "False")
                            {
                                lbl_flag.Text = "Please 'Lock' the current estimated owed amount or make an adjustment on the Matter page";
                            }
                            //6036 Fahad 06/16/2009 For Pasadena Judge show the message(If clause added only)
                            if (ClsCase.HasViolationByCourID(TicketId, 3004, 103))
                            {
                                lblPasadenaJudge.Text = "Please inform the client that the case will be reset to 'PreTrial'";
                            }
                            else
                            {
                                lbl_flag.Text = "";
                            }
                        }
                        else
                        {
                            lbl_OscarActiveFlag.Text = "";
                            lbl_OscarCourtID.Text = "";
                        }

                        // Added by Asghar For checking the No Check Flag during payment process
                        if (ds_os.Tables[1].Rows.Count > 0)
                        {
                            hf_nocheck.Value = "True";
                            lblNoCheckflag.Visible = true;
                        }
                        else
                        {
                            hf_nocheck.Value = "False";
                            lblNoCheckflag.Visible = false;
                        }

                        //
                        ClsPayments.TicketID = this.TicketId;

                        //---------Added By Zeeshan Ahmed------------//
                        //------ Get Unpaid Continuance Amount-------//
                        string[] key = { "@TicketId" };
                        object[] value1 = { this.TicketId };
                        //Asad Ali 8236 11/19/2010  Remove bug adding 1 to actual count
                        hf_continuancecount.Value = ((((int)cls_db.ExecuteScalerBySp("USP_HTS_Get_Unpaid_ContinuanceCount_By_TicketID", key, value1)))).ToString();
                        //------------------------------------------//


                        //Method to check courtid null if any of violation courtid is null then restrict payment..
                        if (ClsPayments.CourtIDNullCheck())
                            HF_Courtid.Value = "1";
                        else
                            HF_Courtid.Value = "0";

                        cSession.CreateCookie("FromSchedule", "", Request, Response);
                        //In order to redirect to frmMain when midnumber is clicked
                        //string casetype=cSession.GetCookie("sSearch",this.Request);
                        ViewState["vSearch"] = Request.QueryString["search"];
                        DisplayInfo();
                        FillYearDDL();
                        FillHowHiredDDL();
                        //Added by ozair to allow check payments if First app date is in past or after 5 business days.
                        //should be call before BindPaymentMethod 
                        AllowCheckPayments(TicketId);
                        //
                        BindPaymentMethod();
                        //Fahad 6054 07/20/2009 Add following two(2) Methods BindUser and BindOutsideFirm
                        BindUser();
                        BindOutsideFirm();
                        GetPayments();
                        //Sabir Khan 4636 08/22/2008
                        GetCaseTypeAssociation();

                        //Check for records in BatchPrint
                        IsAlreadyInBatchPrint();

                        // Abid Ali 5359 1/1/2009 Check LOR records in Batch print
                        IsLORAlreadyInBatchPrint();

                        //Check if Court Status is Bond and execute when salesrep try to print bond.
                        // added by shekhani
                        getBondflagConfirmedStatus(TicketId);


                        //Check for split cases 
                        CheckSpilitCases();

                        //Nasir 6483 10/06/2009 set value for bond feild
                        if (ClsSession.GetCookie("sAccessType", Request) != "2")
                        {
                            hf_CheckBondReportForSecUser.Value = CaseDetails.SetForBondReport(TicketId);
                        }


                        //code added by Azwer when case has NO Letters flag then restrict trial notificiation to print..
                        RestrictTrialNotification();
                        //
                        //Added by Azwar when first time payment then check status and active flag if flag=0 and status is in jury or pretrial then dont sent trial notification letter..
                        CheckForNoTrialLetterSent();

                        CheckReadNotes(TicketId);

                        //Code Added By Fahad To Restrict payment if Cause number is unavailable ( Fahad - 1/4/2008 )
                        //Check Contition If Court ID Exist
                        //Zeeshan Ahmed 3541 Modify Logic For NOS Flag and HCJP Cause No validation 

                        //Kazim 3451 3/25/2008
                        //Check the validity of HCJP Court Cause Numbers
                        hf_InvalidCauseNo.Value = Convert.ToString(Convert.ToInt32(ClsCase.IsValidHCJPCauseNumbers(TicketId)));

                        //ozair 3613 on 04/02/2008
                        if (ClsCase.IsHCJPCourt(TicketId))
                        {
                            hf_nos.Value = Convert.ToString(nos.CheckNOS(TicketId)).ToUpper();

                            if (hf_nos.Value == "FALSE" && hf_InvalidCauseNo.Value == "0")
                            {
                                lbl_cause.Text = "Please enter a 'Cause Number' for HCJP court that starts with 'TR', 'BC' or 'CR' or select 'Not On System' flag if cause number is unavailable";
                            }
                        }


                        //Sabir Khan 5763 04/16/2009 Get LOR Subpoena Flag and LOR Motion of discovery flag...
                        if (btn_letterofRep.Visible == true)
                        {
                            DataSet dscourt = clsCourt.GetCourtInfo(Convert.ToInt32(lbl_courtid.Text));
                            hf_IsLORSubpoena.Value = (Convert.ToBoolean(dscourt.Tables[0].Rows[0]["IsLORSubpoena"]) ? "1" : "0");
                            hf_IsLORMOD.Value = (Convert.ToBoolean(dscourt.Tables[0].Rows[0]["IsLORMOD"]) ? "1" : "0");
                            //Sabir Khan 5763 04/16/2009 check if a client different court date...
                            hf_HasSameCourtdate.Value = (CaseDetails.HasSameCourtDate(TicketId) ? "1" : "0");
                            //Sabir Khan 5763 04/16/2009 check if a client has more then one speeding violations...
                            hf_HasMoreSpeedingviol.Value = (CaseDetails.HasMoreSpeedingViolation(TicketId) <= 1 ? "1" : "0");
                        }

                        //Yasir Kamal 5948 06/04/2009 Check all violations are disposed or not.
                        hf_IsnotDisposed.Value = Convert.ToString(Convert.ToInt32(ClsCase.HasDisposedViolations(TicketId)));
                        // Afaq 8213 11/01/10 Display the payment identifier drop down to client cases only.
                        if (lbl_OscarActiveFlag.Text == "0")
                        {
                            //  td_lbl_PaymentIdentifier.Style["display"] = "none";
                            td_PaymentIdentifier.Style["display"] = "none";
                        }
                        else
                        {
                            //  td_lbl_PaymentIdentifier.Style["display"] = "";
                            td_PaymentIdentifier.Style["display"] = "";
                        }

                        // Farrukh 9398 09/15/2011 initializing continuance popup
                        WCC_ContinuanceComments.Initialize(TicketId, EmpId, 6, "Label", "clsinputadministration", "clsbutton", true);
                        WCC_VoidGeneralComments.Button_Visible = false;
                    }

                    //When coming from Schedule Window
                    //Method To Call Function(s)										 
                    //if (Session["FromSchedule"].ToString()=="yes")
                    if (cSession.GetCookie("FromSchedule", Request) == "yes")
                    {
                        //Session["FromSchedule"]="no";
                        cSession.CreateCookie("FromSchedule", "no", Request, Response);
                        // GetPayments();
                    }
                    //In order to redirect to frmMain when midnumber is clicked

                    ddlPaymentMethod.Attributes.Add("onclick", "HideShow();");
                    btnProcess.Attributes.Add("onclick", "return ProcessAmount();");


                    ActiveMenu am = (ActiveMenu)FindControl("ActiveMenu1");
                    TextBox txt1 = (TextBox)am.FindControl("txtid");
                    //Aziz Null TicketId problem removed.
                    txt1.Text = Convert.ToString(TicketId);
                    TextBox txt2 = (TextBox)am.FindControl("txtsrch");
                    txt2.Text = ViewState["vSearch"].ToString();
                    SearchPage:
                    { }

                    //// Check transaction mode in webconfig///               
                    NewClsCreditCard clscrd = new NewClsCreditCard();
                    if (ClsPayments.TransType)
                    {
                        lbl_transactionmode.Text = "Application running in test mode.";
                        hf_transactionmode.Value = "1";
                    }
                    else
                    {
                        lbl_transactionmode.Text = "";
                        hf_transactionmode.Value = "0";
                    }
                    if (lbl_activeflag.Text == "1")
                    {
                        imgclienttrue.Visible = true;
                        imgclientfalse.Visible = false;
                    }
                    else
                    {
                        imgclienttrue.Visible = false;
                        imgclientfalse.Visible = true;

                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        //Sabir Khan 4636 08/22/2008 set the association value to hf_AssociatedCaseType hidden field.
        private void GetCaseTypeAssociation()
        {
            string[] key = { "@ticketid_pk" };
            object[] value1 = { TicketId };
            DataSet ds = cls_db.Get_DS_BySPArr("USP_HTP_GET_AllUnMatchCourtIDs", key, value1);
            if (ds.Tables[0].Rows.Count > 0)
                hf_AssociatedCaseType.Value = Convert.ToString(ds.Tables[0].Rows[0][0]);
        }
        #region BindPaymentMethodDDl
        private void BindPaymentMethod()
        {
            try
            {
                DataSet ds_paymenttype = ClsPayments.GetPaymentType();
                ddlPaymentMethod.DataSource = ds_paymenttype;
                ddlPaymentMethod.DataTextField = ds_paymenttype.Tables[0].Columns[1].ColumnName;
                ddlPaymentMethod.DataValueField = ds_paymenttype.Tables[0].Columns[0].ColumnName;
                ddlPaymentMethod.DataBind();
                //Afaq 9398 06/21/2011 popuplate payment identifier dropdown
                ddl_PaymentIdentifier.DataSource = ClsPayments.GetPaymentIdentifierType();
                ddl_PaymentIdentifier.DataTextField = "Name";
                ddl_PaymentIdentifier.DataValueField = "Id";
                ddl_PaymentIdentifier.DataBind();
                //CardType DropDown hidden in html..
                DataSet ds_cardtype = ClsPayments.GetCardType();
                ddl_cardtype.DataSource = ds_cardtype;
                ddl_cardtype.DataTextField = ds_cardtype.Tables[0].Columns[1].ColumnName;
                ddl_cardtype.DataValueField = ds_cardtype.Tables[0].Columns[0].ColumnName;
                ddl_cardtype.DataBind();
                ddl_cardtype.Items.Insert(0, new ListItem("--Choose--", "0"));
                //Yasir Kamal 7748 04/28/2010 M.O./CahierCheck validation modified.
                if (hf_AllowChecks.Value == "0" || hf_nocheck.Value == "True")
                {
                    //Remove Check
                    ddlPaymentMethod.Items.Remove(ddlPaymentMethod.Items.FindByValue("2"));

                }
                //if (hf_nocheck.Value == "True")
                //{
                //    //Remove M.O./CashierCheck
                //    ddlPaymentMethod.Items.Remove(ddlPaymentMethod.Items.FindByValue("4"));
                //}
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion

        //Fahad 6054 07/20/2009 populate Outside firm DropDown list by adding all outside firms
        private void BindOutsideFirm()
        {
            clsFirms objClsFirms = new clsFirms();
            DataSet dsFirms = objClsFirms.GetActiveFirms(0);
            ddlOutsideFirm.DataSource = dsFirms;
            ddlOutsideFirm.DataTextField = dsFirms.Tables[0].Columns["FirmAbbreviation"].ToString();
            ddlOutsideFirm.DataValueField = dsFirms.Tables[0].Columns["FirmID"].ToString();
            ddlOutsideFirm.DataBind();
            ddlOutsideFirm.Items.Insert(0, new ListItem("--Choose--", "0"));
            ddlOutsideFirm.SelectedValue = objClsFirms.GetFirmByTicketId(TicketId).ToString();

        }
        //Fahad 6054 07/20/2009 populate user DropDown list by adding all Users 
        private void BindUser()
        {
            clsUser objClsUsers = new clsUser();
            DataSet dsUser = objClsUsers.getAllUsersList();
            ddlRepsName.DataSource = dsUser;
            ddlRepsName.DataTextField = dsUser.Tables[0].Columns["UserName"].ToString();
            ddlRepsName.DataValueField = dsUser.Tables[0].Columns["Employeeid"].ToString();
            ddlRepsName.DataBind();
            ddlRepsName.Items.Insert(0, new ListItem("--Choose--", "0"));
        }
        private void getBondflagConfirmedStatus(int TicketID)
        {
            string[] key = { "@Ticketid_pk" };
            object[] value1 = { TicketID };
            DataSet ds = cls_db.Get_DS_BySPArr("USP_HTS_Get_BondFlagConfirmedFlag", key, value1);

            if (ds.Tables[0].Rows.Count > 0)
                lblBondConfirmed.Text = "1";
            else
                lblBondConfirmed.Text = "0";
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //Modified by ozair 3180 on 16/02/2008
                string[] key = { "@PaymentStatusID", "@tiketid" };
                object[] value1 = { Convert.ToInt32(ddlPaymentType.SelectedValue), TicketId };
                cls_db.ExecuteSP("usp_hts_update_PaymentStaus", key, value1);
                //added by khalid for control replacing 7-1-08
                WCC_GeneralComments.AddComments();


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        private void AllowCheckPayments(int ticketid)
        {
            bool IsAdmin = false;

            if (ClsSession.GetCookie("sAccessType", Request) == "2")
                IsAdmin = true;

            string[] key = { "@TicketId" };
            object[] value1 = { TicketId };
            DataSet ds = cls_db.Get_DS_BySPArr("usp_hts_AllowChecksPayment", key, value1);

            if (IsAdmin)
            {
                hf_AllowChecks.Value = "1";
            }
            else if (ds.Tables[0].Rows.Count > 0)
            {

                hf_AllowChecks.Value = ds.Tables[0].Rows[0]["allowChecks"].ToString();
            }
            else
            {
                hf_AllowChecks.Value = "0";
            }
        }


        protected void dg_PaymentDetail_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    LinkButton lnkb_void = ((LinkButton)e.Item.FindControl("lnkb_edit"));
                    Label lbl_paydate = ((Label)e.Item.FindControl("lbl_paydate"));
                    Label lbl_voidflag = ((Label)e.Item.FindControl("lbl_vflag"));

                    //Kazim 3451 3/19/2008 Use hidden field to convert pay date.

                    HiddenField paydate = (HiddenField)e.Item.FindControl("hf_paydate");
                    DateTime chkdt = Convert.ToDateTime(paydate.Value);

                    //Modified By Zeeshan Ahmed
                    string paymentType = ((Label)e.Item.FindControl("lbl_paymethod")).Text;
                    //Fahad 6054 07/21/2009 Below if and elseif cases added according to dropdown selection
                    if (ddlPaymentMethod.SelectedValue == "9")
                    {
                        paymentType = paymentType + " [" + txtBarCardNumber.Text + "]";

                    }

                    else if (ddlPaymentMethod.SelectedValue == "11")
                    {
                        paymentType = paymentType + " [C/O " + ddlRepsName.SelectedValue + "]";
                    }
                    else if (ddlPaymentMethod.SelectedValue == "104")
                    {
                        paymentType = paymentType + " [" + ddlOutsideFirm.SelectedValue + "]";
                    }


                    //If refund then mark as red
                    if (paymentType == "Refund" || paymentType == "Bounce Check")
                    {
                        ((Label)e.Item.FindControl("lbl_emp")).ForeColor = System.Drawing.Color.Red;
                        ((Label)e.Item.FindControl("lbl_amount")).ForeColor = System.Drawing.Color.Red;
                        ((Label)e.Item.FindControl("lbl_paydate")).ForeColor = System.Drawing.Color.Red;
                        ((Label)e.Item.FindControl("lbl_paymethod")).ForeColor = System.Drawing.Color.Red;
                    }

                    //When trans is done on Todays Date then Enable the void button				
                    int vflag = Convert.ToInt32(lbl_voidflag.Text);
                    if (vflag == 1)
                    {
                        lnkb_void.Enabled = false;
                    }
                    else
                    {
                        lnkb_void.Text = "";
                    }
                    //Fahad 6054 08/19/2009 Prevent disable Void option in following three payment types
                    if (vflag == 0 && (paymentType.Contains("Attorney Credit") || paymentType.Contains("Friend Credit") || paymentType.Contains("Partner Firm Credit")))
                    {
                        lnkb_void.Enabled = true;
                        lnkb_void.Text = "Void";
                    }
                    else if (DateTime.Now.Date == chkdt.Date && vflag == 0)
                    {
                        lnkb_void.Enabled = true;
                        lnkb_void.Text = "Void";
                    }
                    if (lnkb_void.Enabled)
                    {
                        //ozair 3856 on 04/25/2008 getting the invoice number and payment method for that perticular payment
                        int invoicenum = Convert.ToInt32(((Label)e.Item.FindControl("lbl_invoicenum")).Text);
                        string Paymentmethod = ((Label)e.Item.FindControl("lbl_paymethod")).Text;
                        //passign the invoice number and payment method to promptvoid method
                        //it'll enable us to send these to the click event of Void comments popup update button
                        lnkb_void.Attributes.Add("onclick", "return PromptVoid(" + invoicenum + ",'" + Paymentmethod + "');");

                        //end ozair 3856
                    }

                    if (!string.IsNullOrEmpty(lnkb_void.Text) && !lnkb_void.Enabled)
                        lnkb_void.Attributes.Add("class", "btn disabled");

                    if (!string.IsNullOrEmpty(lnkb_void.Text) && lnkb_void.Enabled)
                        lnkb_void.Attributes.Add("class", "btn btn-primary");

                    string lbl_amount = ((Label)e.Item.FindControl("lbl_amount")).Text;
                    if (lbl_amount.StartsWith("-"))
                    {
                        lbl_amount = lbl_amount.Substring(1);
                        ((Label)e.Item.FindControl("lbl_amount")).Text = lbl_amount;
                    }
                    //Transaction num
                    string s_trans = ((Label)e.Item.FindControl("lbl_transnum")).Text;
                    if (s_trans != "")// && s_trans!="0")
                    {
                        Label lbl_trans = ((Label)e.Item.FindControl("lbl_transnum"));
                        lbl_trans.Text = "(" + s_trans + ")";
                    }
                    //visible credit card info columns if card type is credit card..
                    string cardtype = ((Label)e.Item.FindControl("lbl_paymethod")).Text;
                    if (cardtype == "Credit Card" || cardtype == "Internet")
                    {
                        dg_PaymentDetail.Columns[4].Visible = true;
                        dg_PaymentDetail.Columns[5].Visible = true;
                        dg_PaymentDetail.Columns[6].Visible = true;
                        dg_PaymentDetail.Columns[7].Visible = true;
                        dg_PaymentDetail.Columns[8].Visible = true;
                        dg_PaymentDetail.Columns[9].Visible = true;
                        dg_PaymentDetail.Columns[10].Visible = true;
                    }

                }
                DisplayInfo();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void DisplayInfo()
        {
            try
            {
                string[] key = { "@TicketID_PK" };
                object[] value = { TicketId };
                DataSet DS = cls_db.Get_DS_BySPArr("USP_HTS_Get_ClientInfo", key, value);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    lbl_FirstName.Text = DS.Tables[0].Rows[0]["Firstname"].ToString();
                    lbl_LastName.Text = DS.Tables[0].Rows[0]["Lastname"].ToString();
                    lbl_CaseCount.Text = DS.Tables[0].Rows[0]["CaseCount"].ToString();
                    hlnk_MidNo.Text = DS.Tables[0].Rows[0]["Midnum"].ToString();

                    if (Convert.ToInt32(DS.Tables[0].Rows[0]["Activeflag"]) == 0)
                    {
                        tdHowHired.Visible = true;
                        // tdHowHiredLabel.Visible = true;
                    }
                    else
                    {
                        tdHowHired.Visible = false;
                        // tdHowHiredLabel.Visible = false;
                    }

                    //Faique Ali 11422 09/23/2013 getting speacking language 
                    //ViewState["languagespeak"] = DS.Tables[0].Rows[0]["languagespeak"].ToString();

                    //Waqas 5771 04/17/2009
                    int Contact_ID = 0;
                    if (DS.Tables[0].Rows[0]["ContactID"] == DBNull.Value || DS.Tables[0].Rows[0]["ContactID"].ToString() == "0")
                    {
                        Contact_ID = 0;
                    }
                    else
                    {
                        Contact_ID = Convert.ToInt32(DS.Tables[0].Rows[0]["ContactID"]);
                    }
                    //Asad Ali 8153 09/21/2010 set active flag in hidden field 
                    lbl_OscarActiveFlag.Text = DS.Tables[0].Rows[0]["Activeflag"].ToString();
                    // to diplay CID confimation on top
                    if (Convert.ToInt32(DS.Tables[0].Rows[0]["Activeflag"]) == 0
                            && Contact_ID > 0
                            && Convert.ToInt32(DS.Tables[0].Rows[0]["IsContactIDConfirmed"]) == 0
                            && Convert.ToInt32(DS.Tables[0].Rows[0]["recordid"]) > 0
                            && (ClsCase.IsInsideCourtCase(TicketId)))
                    {
                        tbl_CIDConfirmation.Style["display"] = "";
                    }
                    else
                    {
                        tbl_CIDConfirmation.Style["display"] = "none";
                    }

                    //Allow Process Payment
                    if (Convert.ToInt32(DS.Tables[0].Rows[0]["IsContactIDConfirmed"]) == 0
                           && Convert.ToInt32(DS.Tables[0].Rows[0]["recordid"]) > 0
                            && Convert.ToInt32(DS.Tables[0].Rows[0]["Activeflag"]) == 0
                            && (ClsCase.IsInsideCourtCase(TicketId)))
                    {
                        //Display Alert
                        hf_DisplayConfirmedMsg.Value = "1";
                    }
                    else
                    {
                        hf_DisplayConfirmedMsg.Value = "0";
                    }


                    // Flag to check CIS has been Associated or not
                    if (Contact_ID > 0)
                    {
                        hf_DisplayCIDMsg.Value = "0";
                    }
                    else
                    {
                        //Display Alert
                        hf_DisplayCIDMsg.Value = "1";
                    }

                    key[0] = "@TicketID";
                    DataSet DS_Case = cls_db.Get_DS_BySPArr("USP_HTS_GetCaseInfo", key, value);
                    //Asad Ali 8502 11/04/2010 Remove validation as per requirement
                    //Sabir Khan 7181 12/24/2009 Set hidden field for Invalid PMC Ticket Number....
                    //Muhammad Muneer 8276 9/22/2010 Calling the new SP for validation by using components
                    //DataSet DS_Case_Validation = ClsCase.PMCValidation(this.TicketId);
                    //hf_isInvalidPMCTicket.Value = DS_Case_Validation.Tables[0].Rows[0]["IsInvalidPMCTicket"].ToString();
                    //hf_IsInvalidPMCCausenumber.Value = DS_Case_Validation.Tables[0].Rows[0]["IsInvalidPMCCausenumber"].ToString();
                    //Sabir Khan 7110 12/09/2009 Get information for Oscar case...                                      
                    hf_CoveringFirm.Value = DS_Case.Tables[0].Rows[0]["IsOscarClient"].ToString();
                    double Fee = Convert.ToDouble(DS_Case.Tables[0].Rows[0]["calculatedtotalfee"]);
                    double Paid = Convert.ToDouble(DS_Case.Tables[0].Rows[0]["Paid"]);
                    double Owes = Fee - Paid;
                    ActualOwes = Convert.ToDouble(DS_Case.Tables[0].Rows[0]["totalfeecharged"]) - Paid;

                    if (
                        (DS_Case.Tables[0].Rows[0]["DOB"].ToString().Trim() == "" || DS_Case.Tables[0].Rows[0]["DOB"].ToString().Trim() == "01/01/1900" || DS_Case.Tables[0].Rows[0]["DOB"].ToString().Trim() == "1/1/1900")
                        ||
                        (hf_DisplayCIDMsg.Value == "1")
                      )
                    {
                        lblDobMessage.Visible = true;
                        lblDobMessage.Text = "Please enter in a valid DOB and/or CID number on the matter page before processing payment.";
                    }
                    else
                    {
                        lblDobMessage.Visible = false;
                        lblDobMessage.Text = "";
                    }
                    hf_DOB.Value = DS_Case.Tables[0].Rows[0]["DOB"].ToString().Trim();

                    if (ActualOwes != Owes)
                    {
                        hf_owesdiff.Value = "1";
                    }
                    else
                    {
                        hf_owesdiff.Value = "0";
                    }


                    string casetype = ViewState["vSearch"].ToString();

                    switch (casetype)
                    {
                        case "0":
                            hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=0&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            break;
                        case "1":
                            hlnk_MidNo.NavigateUrl = "../frmMain.aspx?search=1&lstcriteriaValue3=" + hlnk_MidNo.Text + "&lstcriteria3=3";
                            break;
                    }
                    //added by ozair
                    ddlPaymentType.SelectedValue = DS_Case.Tables[0].Rows[0]["paymentstatus"].ToString();

                    //Asad Ali 8153 09/20/2010 set values of No DL and IS ALR Hearing Required
                    hf_NoDL.Value = DS_Case.Tables[0].Rows[0]["NoDL"].ToString();
                    hf_IsALRHearingRequired.Value = (DS_Case.Tables[0].Rows[0]["IsALRHearingRequired"] != DBNull.Value ? DS_Case.Tables[0].Rows[0]["IsALRHearingRequired"].ToString().ToLower() : "false");


                    //Zeeshan Ahmed 3535 04/08/2008
                    ViewState["BondFlag"] = Convert.ToInt32(DS_Case.Tables[0].Rows[0]["BondFlag"]);
                    ViewState["HasFTATickets"] = Convert.ToInt32(ClsCase.HasFTAViolations(TicketId));
                    ViewState["IsInsideCase"] = Convert.ToInt32(ClsCase.IsInsideCourtCase(TicketId));

                    //Sabir Khan 11509 11/05/2013 Social Media tracking
                    #region Social Media
                    DataTable dtSocialMedia = ClsCase.GetSocialMedia(TicketId);
                    if (dtSocialMedia.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtSocialMedia.Rows)
                        {
                            switch (dr["MediaID"].ToString())
                            {
                                case "1":
                                    {
                                        chkFacebook.Checked = true;
                                        break;
                                    }
                                case "2":
                                    {
                                        chkGooglePlus.Checked = true;
                                        break;
                                    }
                                case "3":
                                    {
                                        chkLinkedIn.Checked = true;
                                        break;
                                    }
                                case "4":
                                    {
                                        chkTwitter.Checked = true;
                                        break;
                                    }
                                case "5":
                                    {
                                        chkYelp.Checked = true;
                                        break;
                                    }
                                case "6":
                                    {
                                        chkNotAMember.Checked = true;
                                        break;
                                    }
                                case "7":
                                    {
                                        chkDidNotAnswer.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        chkOther.Checked = true;
                                        tdOtherSocialMedia.Style["Display"] = "block";
                                        txtOtherSocialMedia.Text = dr["DESCRIPTION"].ToString();
                                        break;
                                    }
                            }
                        }
                    }
                    #endregion                  
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void FillYearDDL()
        {
            try
            {
                //Populating year Combo
                int lbound = DateTime.Now.Date.Year;
                string lb = lbound.ToString();
                int ubound = DateTime.Now.AddYears(13).Date.Year;
                string ub = ubound.ToString();
                lb = lb.Substring(2);
                ub = ub.Substring(2);

                for (int i = Convert.ToInt32(lb); i <= Convert.ToInt32(ub); i++)
                {
                    if (i < 10)
                    {
                        //string year =""i.ToString()
                        ddlYear.Items.Add(String.Format("0{0}", i));
                    }
                    else
                    {
                        ddlYear.Items.Add(i.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void FillHowHiredDDL()
        {
            DataTable dtHowHired = ClsCase.FillHowHiredDropDown();
            DataRow dr = dtHowHired.NewRow();
            dr["ID"] = 0;
            dr["HiredVia"] = "--Choose--";
            dtHowHired.Rows.InsertAt(dr, 0);
            if (dtHowHired != null && dtHowHired.Rows.Count > 0)
            {
                ddlHowHired.DataSource = dtHowHired;
                ddlHowHired.DataTextField = "HiredVia";
                ddlHowHired.DataValueField = "ID";
                ddlHowHired.DataBind();
            }
        }

        private void GetPayments()
        {
            try
            {
                //int datetypeflag=0;	
                ClsPayments.TicketID = TicketId;
                ClsPayments.EmpID = EmpId;
                if (ClsPayments.GetPaymentInfo(TicketId))
                {
                    lbl_ToProcess.Text = ClsPayments.ToProcess.ToString();
                    //
                    lblFTACase.Text = ClsPayments.Status;
                    lbl_Fee.Text = ClsPayments.Totalfee.ToString();
                    lbl_Paid.Text = ClsPayments.ChargeAmount.ToString();
                    lbl_owes.Text = ClsPayments.Owes.ToString();
                    lblOwe.Text = ClsPayments.Owes.ToString();
                    HFOwes.Value = ClsPayments.Owes.ToString();
                    HFFee.Value = ClsPayments.Totalfee.ToString();
                    lbl_WalkFlag.Text = ClsPayments.WalkInClientFlag;
                    // Noufil 5618 02/04/2009 Add followupdate for payment
                    lblFollowupdate.Text = ClsPayments.PaymentFollowUpdate;
                }
                else
                {
                    lblMessage.Text = "Information Not Retrived";
                    // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                    tr_errorMessages.Visible = true;
                }

                string[] key = { "@TicketID" };
                object[] value = { TicketId };
                DataSet DS = cls_db.Get_DS_BySPArr("usp_hts_get_NewPaymentDetails", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    dg_PaymentDetail.DataSource = DS;
                    dg_PaymentDetail.DataBind();

                }
                else
                {
                    dg_PaymentDetail.DataSource = DS;
                    dg_PaymentDetail.DataBind();
                }

                DataSet ds_schedulepayment = ClsPayments.GetSchedulePaymentInfo(TicketId);
                dg_Schedule.DataSource = ds_schedulepayment;
                dg_Schedule.DataBind();
                lbl_schcount.Text = dg_Schedule.Items.Count.ToString();

                //To get the sum of schedule amount
                if (dg_Schedule.Items.Count > 0)
                {
                    double sumamt = ScheduleAmountSum();
                    txt_schedulesum.Text = sumamt.ToString();
                }
                else
                {
                    txt_schedulesum.Text = "0";
                }

                DataSet ds_case = ClsCase.GetCase(TicketId);
                if (ds_case.Tables[0].Rows.Count > 0)
                {
                    DataRow dRowCase = ds_case.Tables[0].Rows[0];


                    //When Schedule Payment is clicked
                    double owes = Convert.ToDouble(lbl_owes.Text) - Convert.ToDouble(txt_schedulesum.Text);
                    int schdid = 0;
                    lnkb_Viol.Attributes.Add("onclick", "return OpenSchedule(" + owes + "," + schdid + "," + dRowCase["ActiveFlag"] + ");");
                    //When To Edit SchedulePayment
                    EditSchedulePayment(owes);
                    //setting flags

                    lbl_FirstName.Text = dRowCase["FirstName"].ToString().Trim();
                    lbl_LastName.Text = dRowCase["Lastname"].ToString().Trim();
                    hlnk_MidNo.Text = dRowCase["midnum"].ToString().Trim();
                    lbl_CaseCount.Text = dRowCase["Casecount"].ToString().Trim();
                    //Checking flags for displaying report buttons
                    lbl_activeflag.Text = dRowCase["activeflag"].ToString();

                    if (lbl_activeflag.Text == "1")
                    {
                        imgclienttrue.Visible = true;
                        imgclientfalse.Visible = false;
                    }
                    else
                    {
                        imgclienttrue.Visible = false;
                        imgclientfalse.Visible = true;
                    }
                    //for oscar
                    lbl_OscarActiveFlag.Text = dRowCase["activeflag"].ToString();
                    //
                    lbl_dlstate.Text = dRowCase["dlstate"].ToString();
                    lbl_lockflag.Text = dRowCase["LockFlag"].ToString();
                    lbl_addresscheck.Text = dRowCase["Addressconfirmflag"].ToString();
                    //by khalid 1-1-08 control replacing task.
                    //Nasir 6098 08/21/2009 general comment initialize overload method
                    WCC_GeneralComments.Initialize(TicketId, EmpId, 1, "Label", "clsinputadministration", "clsbutton", "Label");
                    WCC_GeneralComments.btn_AddComments.Visible = true;
                    //In order to prompt for conflict
                    //				lbl_trialbuttonflag.Text=ClsCase.CheckCourtStatus(TicketID).ToString();	
                    //Checking if bondflag=1 then client must have three contact numbers

                    //ozair 3856 on 04/25/2008 intializing the general comments for payment void pop up
                    WCC_VoidGeneralComments.Initialize(TicketId, EmpId, 1, "Label", "clsinputadministration", "clsbutton", true);
                    WCC_VoidGeneralComments.Button_Visible = false;
                    //end ozair 3856

                    //  Added By Zeeshan Jur
                    lbl_yds.Text = dRowCase["YDS"].ToString();
                    //Newly add if lock then get owed anount in amount textbox
                    if (lbl_lockflag.Text == "True")
                    {
                        txtAMT.Text = lbl_owes.Text;
                        txtCardholder.Text = lbl_FirstName.Text + " " + lbl_LastName.Text;
                    }

                    if (Convert.ToInt32(dRowCase["Bondflag"]) == 1) //main
                    {
                        lbl_contactcheck.Text = ClsCase.CheckClientContact(TicketId) ? "True" : "False";
                    }
                    else //Farrukh 9925 11/29/2011 Changed validation for at least 2 contact numbers
                    {
                        lbl_contactcheck.Text = ClsCase.CheckNonBondClientContact(TicketId) ? "True" : "False";
                    }


                    //for Buttons

                    if (Convert.ToInt32(dRowCase["ActiveFlag"]) == 1)
                    {
                        //Only active flag should be 1 
                        btn_coversheet.Visible = true;
                        btn_Receipt.Visible = true;
                        //Zeeshan Ahmed 4202 06/09/2008 Display Trial Letter Button
                        btn_trialLetter.Visible = true;
                        //Zeeshan Ahmed 3262 02/28/2008 
                        //Display Bond Button If Bond Flag Is Yes 
                        btn_bond.Visible = (Convert.ToInt32(dRowCase["Bondflag"]) == 1) ? true : false;

                        //Button Display new logic
                        DataSet ds_underlying = ClsCase.GetUnderlyingCaseInfo(TicketId);

                        // Abid Ali 5505 2/6/2009 Optimizing body of for loop
                        DataRow dRow;
                        int caseTypeID;
                        int CategoryID;
                        int CourtID;
                        int violationCategoryId;
                        //Ozair 5661 04/01/2009 Warnings Removed

                        for (int i = 0; i < ds_underlying.Tables[0].Rows.Count; i++)
                        {
                            // Abid Ali 5505 2/6/2009 Optimizing because in every iteration memory re-created which effect on performance
                            //Zeeshan Ahmed 3948 05/14/2008
                            dRow = ds_underlying.Tables[0].Rows[i];

                            //Ozair 5473 02/02/2009 
                            // Abid Ali 5505 2/6/2009  get violation category id instead of violationnumber_pk all printed letter will depend on it
                            violationCategoryId = (dRow["ViolationCategoryID"] == DBNull.Value) ? 0 : Convert.ToInt32(dRow["ViolationCategoryID"]);
                            caseTypeID = (dRow["caseTypeID"] == DBNull.Value) ? 0 : Convert.ToInt32(dRow["caseTypeID"]);
                            CategoryID = Convert.ToInt32(dRow["categoryid"]);
                            CourtID = Convert.ToInt32(dRow["CourtID"]);


                            //Fahad 5098 11/28/2008//  For AG Contract
                            //Ozair 5743 02/02/2009 case type checked instead of courtid
                            if (caseTypeID == 4)
                            {
                                btnAGContract.Visible = true;
                                btnAGContract.Attributes.Remove("onClick");
                                btnAGContract.Attributes.Add("onclick", " return PrintLetter(18);");

                            }
                            // Ozair 5473 02/02/2009 GetCaseType Method replaced with casetypeid got from procedure
                            else if (caseTypeID == 2)
                            {
                                //Ali 9949 12/30/2011 No need to display trial letter button for criminal cases and also change recipt button text to Criminal Receipt.
                                btn_trialLetter.Visible = false;
                                btn_Receipt.Text = "Criminal Contract";
                                // Abid Ali 5505 2/6/2009 print letter based on Violation Category ID
                                if (violationCategoryId == 28)         //For ALR Contract
                                {
                                    btnALRContract.Visible = true;
                                    btnALRContract.Attributes.Remove("onClick");
                                    btnALRContract.Attributes.Add("onclick", " return PrintLetter(" + 19 + ");");


                                    ////Waqas 5864 07/02/2009 ALR Letters
                                    btn_ALRCheckRequest.Visible = true;
                                    btn_ALRContinuance.Visible = true;
                                    btn_ALRHearingRequest.Visible = true;
                                    btn_ALRMandatoryContinuance.Visible = true;
                                    btn_ALRMotionToRequestBTOBTS.Visible = true;
                                    btn_ALRSubpoena.Visible = true;


                                    DataSet ds_Case = ClsCase.GetALRCaseQuestionsInfo(TicketId);
                                    if (ds_Case.Tables.Count > 0)
                                    {
                                        if (ds_Case.Tables[0].Rows.Count > 0)
                                        {
                                            DataRow dRow1 = ds_Case.Tables[0].Rows[0];
                                            if (dRow1["AttorenyFirstName"].ToString().Trim() == "" || dRow1["AttorneyLastName"].ToString().Trim() == "")
                                            {
                                                hf_ALRAttorneyNameAlert.Value = "1";
                                            }

                                            if (dRow1["AttorneyBarCardNumber"].ToString().Trim() == "")
                                            {
                                                hf_ALRAttorneyBarCardAlert.Value = "1";
                                            }

                                            if (dRow1["ALRArrestingAgency"].ToString().Trim() == "")
                                            {
                                                hf_ALRArrestingAgencyAlert.Value = "1";
                                            }

                                            if (dRow1["ALROfficerName"].ToString().Trim() == "")
                                            {
                                                hf_ALROfficerNameAlert.Value = "1";
                                            }

                                            //Waqas 6342 08/13/2009 officer address added
                                            if (dRow1["ALROfficerAddress"].ToString().Trim() == "")
                                            {
                                                hf_ALROfficerAddressAlert.Value = "1";
                                            }

                                            if (dRow1["ALROfficerCity"].ToString().Trim() == "")
                                            {
                                                hf_ALROfficerCityAlert.Value = "1";
                                            }

                                            //Waqas 6342 08/13/2009 officer state added
                                            if (dRow1["ALROfficerStateID"].ToString().Trim() == "" || dRow1["ALROfficerStateID"].ToString().Trim() == "0")
                                            {
                                                hf_ALROfficerStateAlert.Value = "1";
                                            }

                                            if (dRow1["ALROfficerZip"].ToString().Trim() == "")
                                            {
                                                hf_ALROfficerZipAlert.Value = "1";
                                            }

                                            if (dRow1["ALRIsIntoxilyzerTakenFlag"].ToString().Trim() != "1")
                                            {
                                                hf_ALRIntoxilizerAlert.Value = "1";
                                            }

                                            if (dRow1["ALRIntoxilyzerResult"].ToString().Trim().ToLower() == "fail")
                                            {
                                                hf_ALRIntoxilizerAlert2.Value = "1";
                                            }
                                            //Waqas 6342 08/13/2009 for observing officer


                                            if (dRow1["IsALRArrestingObservingSame"].ToString().Trim().ToUpper() == "FALSE")
                                            {
                                                if (dRow1["ALRObservingOfficerName"].ToString().Trim() == "")
                                                {
                                                    hf_ALROBSOfficerNameAlert.Value = "1";
                                                }

                                                if (dRow1["ALRObservingOfficerAddress"].ToString().Trim() == "")
                                                {
                                                    hf_ALROBSOfficerAddressAlert.Value = "1";
                                                }

                                                if (dRow1["ALRObservingOfficerCity"].ToString().Trim() == "")
                                                {
                                                    hf_ALROBSOfficerCityAlert.Value = "1";
                                                }

                                                if (dRow1["ALRObservingOfficerState"].ToString().Trim() == "" || dRow1["ALRObservingOfficerState"].ToString().Trim() == "0")
                                                {
                                                    hf_ALROBSOfficerStateAlert.Value = "1";
                                                }

                                                if (dRow1["ALRObservingOfficerZip"].ToString().Trim() == "")
                                                {
                                                    hf_ALROBSOfficerZipAlert.Value = "1";
                                                }
                                            }
                                        }
                                    }

                                }
                                // ozair 5098 01/13/2009 DWI violation no updated as per id at live
                                else if (violationCategoryId == 27)    //For DWI Contract
                                {
                                    btnDWIContract.Visible = true;
                                    btnDWIContract.Attributes.Remove("onClick");
                                    btnDWIContract.Attributes.Add("onclick", " return PrintLetter(20);");
                                }
                                else if (violationCategoryId == 30)    //For DLSuspension Contract
                                {
                                    btnDLSuspensionContract.Visible = true;
                                    btnDLSuspensionContract.Attributes.Remove("onClick");
                                    btnDLSuspensionContract.Attributes.Add("onclick", " return PrintLetter(21);");
                                }
                                else if (violationCategoryId == 29)    //For Occupational Contract
                                {
                                    btnOccupLicContract.Visible = true;
                                    btnOccupLicContract.Attributes.Remove("onClick");
                                    btnOccupLicContract.Attributes.Add("onclick", " return PrintLetter(22);");
                                }
                                // Abid Ali 2/6/2009 Optimizig conditions into if-else if
                                else
                                {
                                    //Ali 12/30/2011 9949 No need of pre trial contract button.
                                    //if (CategoryID == 3)               //For Pre-trial Contract
                                    //{

                                    //    btnPreTrialContract.Visible = true;
                                    //    btnPreTrialContract.Attributes.Remove("onClick");
                                    //    btnPreTrialContract.Attributes.Add("onclick", " return PrintLetter(23);");
                                    //}
                                    //else 
                                    if (CategoryID == 4)    //For Trial Contract
                                    {
                                        btnTrialContract.Visible = true;
                                        btnTrialContract.Attributes.Remove("onClick");
                                        btnTrialContract.Attributes.Add("onclick", " return PrintLetter(24);");
                                    }
                                    //Fahad 5098 12/26/2008
                                    else if (CategoryID == 5)    //For Trial Contract
                                    {
                                        btnTrialContract.Visible = true;
                                        btnTrialContract.Attributes.Remove("onClick");
                                        btnTrialContract.Attributes.Add("onclick", " return PrintLetter(25);");
                                    }
                                }

                            }



                            //checking for BOND button //Bond flag set to 1 & status in Waiting or Arr
                            //Zeeshan Ahmed 3948 05/14/2008 Refactoring 


                            // tahir 4477 07/29/2008
                            // added validation on bond letter printing &
                            // fixed fixed the bug.
                            if (btn_bond.Visible && (CategoryID == 1 || CategoryID == 2 || CategoryID == 12))
                            {
                                //Zeeshan Ahmed 3262 02/28/2008 
                                //btn_bond.Visible = true;
                                hf_AllowBondLetter.Value = "1";
                            }

                            lbl_courtid.Text = CourtID.ToString();
                            // end 4477

                            //checking for TRIAL BUTTON 
                            //Outside court and status in Arr,Pre,Jury,Judge today or future courtdate
                            //Inside court and status in Jury today or future courtdate
                            DateTime dcourt = Convert.ToDateTime(dRow["CourtDateMain"]);
                            //ozair 4967 10/15/2008 included check for Traffic Case Type
                            // Ozair 5473 02/02/2009 GetCaseType Method replaced with casetypeid got from procedure
                            if (caseTypeID != 1)
                            {
                                btn_trialLetter.Attributes.Remove("onClick");
                                btn_trialLetter.Attributes.Add("onClick", "return TrialCaseTypeAlert();");
                            }
                            //Yasir Kamal 7074 12/02/2009 do not allow to print trial letter for arraignment status.
                            //Sabir Khan 7074 12/03/2009 Allow printing trial letter for HMC Judge trial cases
                            else if (
                                ((dcourt >= DateTime.Now.Date) && (CategoryID == 3 || CategoryID == 4 || CategoryID == 5)
                                && (CourtID != 3001 && CourtID != 3002 && CourtID != 3003))
                                ||
                                ((dcourt >= DateTime.Now.Date) && (CategoryID == 4 || CategoryID == 5) && (CourtID == 3001 || CourtID == 3002 || CourtID == 3003))
                                ||
                                ((Convert.ToString(ClsCase.IsHCJPCourt(TicketId)) == "True") && ((dRow["ViolationStatus"].ToString() == "139") || (dRow["ViolationStatus"].ToString() == "256") || (dRow["ViolationStatus"].ToString() == "257") || (dRow["ViolationStatus"].ToString() == "258") || (dRow["ViolationStatus"].ToString() == "259"))) //Sabir Khan 8862 02/22/2010 check for Juivnila cases.
                                )
                            {
                                ViewState["emailid"] = "";
                                //Sabir Khan 7074 12/03/2009 No need to check court room number for trial letter...
                                //if (ClsCase.ValidateTrialLetterPrintOption(this.TicketId))
                                //{
                                if (dRowCase["email"].ToString() != "")
                                    ViewState["emailid"] = dRowCase["email"].ToString();

                                btn_trialLetter.Attributes.Remove("onClick");
                                btn_trialLetter.Attributes.Add("onclick", "return PrintLetter(2);");
                                //}
                            }
                            // Checking for LETTER OF REP //status in Waiting,Arr,Waiting2nd,Bond and court not to be Lubbok/Mykawa

                            //if ((CategoryID == 1 || CategoryID == 2 || CategoryID == 11 || CategoryID == 12) && (CourtID != 3001 && CourtID != 3002 && CourtID != 3003))
                            if (CourtID != 3001 && CourtID != 3002 && CourtID != 3003)
                            {
                                //Nasir 6181 07/25/2009 Court FAX method count
                                int MyCount = clsCourts.GetLORMethodByCase(TicketId);
                                hfLORMethodCount.Value = MyCount.ToString();
                                btn_letterofRep.Visible = true;
                                bool faxFlag = clsFlags.HasFlag(Convert.ToInt32(FlagType.AllowFaxingLOR));
                                hffaxFlag.Value = faxFlag.ToString();
                                hfIsHCJP.Value = Convert.ToString(ClsCase.IsHCJPCourt(TicketId));

                                //Nasir 6181 07/25/2009 Court FAX method count
                                //Ozair 6460 08/25/2009 included check for bond flag
                                //Yasir Kamal 6480 08/28/2009 Allow Faxing LOR even if bond is true. 
                                if (!(MyCount == 0) && (Convert.ToInt32(dRowCase["Bondflag"]) == 0 || Convert.ToInt32(dRowCase["Bondflag"]) == 1 && faxFlag == true && Convert.ToBoolean(ClsCase.IsHCJPCourt(this.TicketId))))
                                {
                                    btn_letterofRep.Text = "Fax LOR";
                                }
                                else
                                {
                                    btn_letterofRep.Text = "Batch LOR";
                                }
                                if (CourtID == 3060)
                                {
                                    btn_Discovery.Visible = true;
                                    btn_Limine.Visible = true;
                                }

                                //Nasir 6181 07/28/2009  remove code of setting IsLORBatchActive
                            }
                            //Faique Ali 11422 09/23/20013 show continuance btn only for client languge is English 
                            //bool con = false;
                            //con = (Convert.ToInt32(dRow["ContinuanceAmount"]) > 0) ? true : false;
                            //if (ViewState["languagespeak"]!=null)
                            //{
                            //    if (con && ViewState["languagespeak"].ToString()== "ENGLISH")
                            //        btn_contuance.Visible = true;
                            //    else
                            //        btn_contuance.Visible = false;

                            //}
                            btn_contuance.Visible = (Convert.ToInt32(dRow["ContinuanceAmount"]) > 0) ? true : false;

                            // Abid Ali 5138 visible button on case
                            if ((dcourt >= DateTime.Now.Date) && (CategoryID == 2 || CategoryID == 3 || CategoryID == 4 || CategoryID == 5) && caseTypeID == 2)
                            {
                                // Abid Ali 5505 2/6/2009 print letter based on Violation Category ID instead of violationnumber_pk
                                if (violationCategoryId == 30) // DL Suspension 
                                    btn_TrialSuspensionDL.Visible = true;
                                else if (violationCategoryId == 29) // Occupational
                                    btn_TrialOccupationDL.Visible = true;

                                btn_TrialSuspensionDL.Attributes.Remove("onClick");
                                btn_TrialSuspensionDL.Attributes.Add("onclick", " return PrintLetter(16);");

                                btn_TrialOccupationDL.Attributes.Remove("onClick");
                                btn_TrialOccupationDL.Attributes.Add("onclick", " return PrintLetter(17);");
                            }
                            else
                            {
                                btn_TrialSuspensionDL.Attributes.Remove("onClick");
                                btn_TrialSuspensionDL.Attributes.Add("onclick", " return JuryTrialAlert();");

                                btn_TrialOccupationDL.Attributes.Remove("onClick");
                                btn_TrialOccupationDL.Attributes.Add("onclick", " return JuryTrialAlert();");
                            }

                            //if(dRow["Email"].ToString()!="")
                            //{
                            //    //../reports/payment_receipt_contract.asp?TicketID=<%=intticketid 
                            //    btn_emailrep.Visible=true;
                            //}						
                        }
                    }
                    else
                    {
                        btn_bond.Visible = false;
                        btn_contuance.Visible = false;
                        btn_coversheet.Visible = false;
                        btn_emailrep.Visible = false;
                        btn_EmailTrial.Visible = false;
                        btn_letterofRep.Visible = false;
                        btn_Receipt.Visible = false;
                        btn_Limine.Visible = false;
                        btn_Discovery.Visible = false;
                        btn_trialLetter.Visible = false;
                        // Abid Ali 5138 
                        btn_TrialOccupationDL.Visible = false;
                        btn_TrialSuspensionDL.Visible = false;
                        //Ozair 5359 02/12/2009 hiding contracts button if active flag=0 (not a client)
                        btnAGContract.Visible = false;
                        btnALRContract.Visible = false;
                        btnDLSuspensionContract.Visible = false;
                        btnDWIContract.Visible = false;
                        btnOccupLicContract.Visible = false;
                        btnPreTrialContract.Visible = false;
                        btnTrialContract.Visible = false;
                        //Waqas 5864 07/02/2009 Hiding ALR Letters
                        btn_ALRCheckRequest.Visible = false;
                        btn_ALRContinuance.Visible = false;
                        btn_ALRHearingRequest.Visible = false;
                        btn_ALRMandatoryContinuance.Visible = false;
                        btn_ALRMotionToRequestBTOBTS.Visible = false;
                        btn_ALRSubpoena.Visible = false;
                        //Sabir Khan 5338 12/26/2008 Past court date ...
                        hf_CourtDateMain.Value = ClsCase.HasFutureCourtdate(TicketId);


                        //Waqas 6342 08/17/2009 checking alr hearing Process
                        DataSet ds_underlying = ClsCase.GetUnderlyingCaseInfo(TicketId);
                        for (int i = 0; i < ds_underlying.Tables[0].Rows.Count; i++)
                        {
                            DataRow dRow = ds_underlying.Tables[0].Rows[i];

                            int violationCategoryId = (dRow["ViolationCategoryID"] == DBNull.Value) ? 0 : Convert.ToInt32(dRow["ViolationCategoryID"]);
                            int caseTypeID = (dRow["caseTypeID"] == DBNull.Value) ? 0 : Convert.ToInt32(dRow["caseTypeID"]);

                            if (violationCategoryId == 28 && caseTypeID == 2)
                            {
                                hf_ALRProcess.Value = "1";
                            }
                        }



                    }

                    ////---Adil
                    //if (lbl_activeflag.Text == "1")
                    //{

                    //    btn_ESignature.Enabled = true;
                    //    //Fahad 5908 05/15/2009 Comment Below Line
                    //    string SessionedID = "", ExePath = "";
                    //    DataTable dt_Config = new DataTable();
                    //    dt_Config = cls_db.Get_DS_BySP("USP_HTS_ESignature_Get_Configuration").Tables[0];
                    //    ExePath = dt_Config.Rows[0]["ExePath"].ToString();
                    //    //ExePath = ExePath.Replace("\\", "\\\\");
                    //    ExePath = ExePath.Replace("\\", "_");
                    //    //ExePath=ExePath.Replace(
                    //    SessionedID = Session.SessionID + System.DateTime.Now;
                    //    SessionedID = SessionedID.Replace("/", "");
                    //    SessionedID = SessionedID.Replace(":", "");
                    //    SessionedID = SessionedID.Replace(" ", "");
                    //    // Noufil 5931 05/21/2009 set parameter on onclick event
                    //    // tahir 7899 07/21/2010 changed the parameter delimeter;
                    //    btn_ESignature.Attributes.Add("OnClick", "return OpenDocumentSelector('" + Request.QueryString["casenumber"] + "|" + AccessType + "|" + EmpId + "','" + ExePath + "','" + SessionedID + "');");
                    //}
                    //else
                    //{
                    //    btn_ESignature.Enabled = false;
                    //}
                    ////---Adil

                    if (dg_Schedule.Items.Count > 0)//for difference ammount and plan amount..
                    {
                        string ScheduleAmount = "";
                        double schamount = 0;
                        double diff = 0;
                        foreach (DataGridItem itemx in dg_Schedule.Items)
                        {
                            ScheduleAmount = ((Label)itemx.FindControl("lnkb_amount")).Text;
                            schamount += Convert.ToDouble(ScheduleAmount);
                            ((Label)itemx.FindControl("lnkb_amount")).Text = "$" + ((Label)itemx.FindControl("lnkb_amount")).Text;

                        }
                        lblplan.Text = schamount.ToString();
                        diff = Convert.ToDouble(lblOwe.Text) - Convert.ToDouble(lblplan.Text);
                        lbldifference.Text = diff.ToString();
                    }
                    else
                    {
                        lblplan.Text = "0";
                        lbldifference.Text = "0";
                    }

                }//concatenating dollar sign..
                lbl_Fee.Text = "$" + lbl_Fee.Text;
                lbl_Paid.Text = "$" + lbl_Paid.Text;
                lbl_owes.Text = "$" + lbl_owes.Text;
                lblOwe.Text = "$" + lblOwe.Text;
                lblplan.Text = "$" + lblplan.Text;
                lbldifference.Text = "$" + lbldifference.Text;



            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private double ScheduleAmountSum()
        {
            try
            {
                double sum = 0;
                foreach (DataGridItem ItemX in dg_Schedule.Items)
                {
                    sum += Convert.ToDouble(((Label)(ItemX.FindControl("lnkb_amount"))).Text);
                }
                return sum;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return 0;
            }
        }

        //In order to Edit the schedule payment
        private void EditSchedulePayment(double owes)
        {
            try
            {
                int schdid = 0;
                double amount = 0;
                foreach (DataGridItem ItemX in dg_Schedule.Items)
                {
                    //binding scheduleid 
                    schdid = Convert.ToInt32(((Label)ItemX.FindControl("lbl_schd_id")).Text);
                    amount = Convert.ToDouble(((Label)ItemX.FindControl("lnkb_amount")).Text);
                    //Function to open window to Edit Schedule Amt
                    ((LinkButton)ItemX.FindControl("lnkb_editschedule")).Attributes.Add("OnClick", "return OpenSchedule(" + owes + " , " + schdid + ");");
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void dg_Schedule_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Delete")
                {
                    int schdid = 0;
                    schdid = Convert.ToInt32(((Label)e.Item.FindControl("lbl_schd_id")).Text);
                    ClsPayments.InactiveSchedulePayment(schdid);
                    //filling Schedule Grid
                    DataSet ds_schedulepayment = ClsPayments.GetSchedulePaymentInfo(TicketId);
                    dg_Schedule.DataSource = ds_schedulepayment;
                    dg_Schedule.DataBind();
                    GetPayments();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                PaymentProcess();

            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        //Fahad 6054 07/23/2009 Place this payment process in to a new method
        private void PaymentProcess()
        {
            string owesformail = "";
            string paidforemail = "";
            string strReq = Request.Form["txtTimeStemp"];
            //Fahad 5807 05/13/2009 Add Session
            Session["OldIsClient"] = ClsCase.IsClient(TicketId);
            if (Session["TimeStemp"] != null && Session["TimeStemp"].ToString() == strReq)
            {

                if (ddlPaymentMethod.SelectedValue == "8")
                {
                    SendMail(3);

                }

                string[] keys1 = { "@TicketID", "@Amount" };
                object[] values1 = { TicketId, txtAMT.Text };
                DataTable dt = cls_db.Get_DT_BySPArr("USP_HTS_PAYMENT_CHECK_AMOUNT", keys1, values1);

                //Added By Zeeshan Ahmed
                //Bounce Check Modification
                if (Convert.ToInt16(dt.Rows[0]["ProcessPayment"]) == 0 && (Convert.ToInt32(ddlPaymentMethod.SelectedValue) != 8 && Convert.ToInt32(ddlPaymentMethod.SelectedValue) != 103))
                {
                    lblMessage.Text = "Invalid payment amount. The amount paid exceeds the total amount.";
                    // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                    tr_errorMessages.Visible = true;
                }
                else
                {
                    if (lblMessage.Text == "Invalid payment amount. The amount paid exceeds the total amount.")
                    {
                        lblMessage.Text = "";
                        // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                        tr_errorMessages.Visible = false;
                    }



                    txtTimeStemp.Text = DateTime.Now.TimeOfDay.ToString();
                    Session["TimeStemp"] = txtTimeStemp.Text;
                    //mandatory	
                    ClsPayments.TicketID = TicketId;
                    ClsPayments.EmpID = EmpId;
                    ClsPayments.ChargeAmount = Convert.ToDouble(txtAMT.Text);
                    ClsPayments.RecDate = DateTime.Now;//date_payment.SelectedDate;
                    ClsPayments.PaymentType = Convert.ToInt32(ddlPaymentMethod.SelectedValue);
                    //if credit card selected
                    ClsPayments.CardHolderName = txtCardholder.Text.ToUpper();
                    ClsPayments.CardNumber = txtCardNumber.Text;
                    ClsPayments.CardType = ddl_cardtype.SelectedValue;
                    ClsPayments.Cin = txtCIN.Text;
                    //ClsPayments.PaymentsComments = txt_paycomments.Text.Trim();
                    //ClsPayments.CertifiedMail = txt_certified.Text.Trim();
                    ClsPayments.CardExpirationDate = ddl_expmonth.SelectedValue + "/" + ddlYear.SelectedValue;
                    ClsPayments.PaymentStatus = Convert.ToInt32(ddlPaymentType.SelectedValue);

                    if (lbl_scheduleid.Text == "")
                    {
                        lbl_scheduleid.Text = "0";
                    }
                    ClsPayments.Scheduleid = Convert.ToInt32(lbl_scheduleid.Text);
                    //Settings parameters for process flag
                    //if 'payment type is refund' or 'not processing a schedule amt' or 'no schedule payments' 
                    if (ClsPayments.PaymentType == 8 || ClsPayments.Scheduleid != 0 || dg_Schedule.Items.Count == 0)
                    {
                        ClsPayments.Processflag = 0;
                    }
                    else
                    {
                        ClsPayments.Processflag = 1;
                    }

                    //Calling Method to process payment
                    try
                    {

                        //Kazim 3184 2/28/2008 Validate Credit Card Number at server side

                        if (ClsPayments.CardType == "0" && ClsPayments.PaymentType == 5)
                        {
                            lblMessage.Text = "Invalid Credit Card Number";
                            // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                            tr_errorMessages.Visible = true;
                            return;
                        }

                        //Fahad 6054 07/21/2009 Check If any other Client is exist related to same date and same Courthouse
                        if (ddlPaymentMethod.SelectedValue == "9")
                        {
                            ClsPayments.PaymentReference = txtBarCardNumber.Text;
                        }

                        else if (ddlPaymentMethod.SelectedValue == "11" || ddlPaymentMethod.SelectedValue == "102")
                        {
                            ClsPayments.PaymentReference = ddlRepsName.SelectedValue;
                        }

                        else if (ddlPaymentMethod.SelectedValue == "104")
                        {
                            ClsPayments.PaymentReference = ddlOutsideFirm.SelectedValue;
                        }
                        ClsPayments.PaymentIdentifier = Convert.ToInt32(ddl_PaymentIdentifier.SelectedValue);

                        // Sabir Khan 10920 05/27/2013 Need to get local machine name and ip 
                        _localmachinename = ClsSession.GetCookie("LocalMachineName", Request);
                        string branchName = ClsSession.GetCookie("BranchName", this.Request);
                        ClsPayments.BranchID = ClsPayments.GetBranchByName(branchName);
                        if (lbl_activeflag.Text == "0")
                        {
                            ClsPayments.HowHired = Convert.ToInt32(ddlHowHired.SelectedValue);
                        }
                        else
                        {
                            ClsPayments.HowHired = 0;
                        }

                        if (ClsPayments.ProcessPayment())
                        {
                            //Sabir Khan 11/05/2013 Social Media tracking...
                            ClsCase.SaveSocialMedia(1, TicketId, EmpId, "Facebook", chkFacebook.Checked);
                            ClsCase.SaveSocialMedia(2, TicketId, EmpId, "Google Plus", chkGooglePlus.Checked);
                            ClsCase.SaveSocialMedia(3, TicketId, EmpId, "LinkedIn", chkLinkedIn.Checked);
                            ClsCase.SaveSocialMedia(4, TicketId, EmpId, "Twitter", chkTwitter.Checked);
                            ClsCase.SaveSocialMedia(5, TicketId, EmpId, "Yelp!", chkYelp.Checked);
                            ClsCase.SaveSocialMedia(6, TicketId, EmpId, "Not A Member", chkNotAMember.Checked);
                            ClsCase.SaveSocialMedia(7, TicketId, EmpId, "Did Not Answer", chkDidNotAnswer.Checked);
                            ClsCase.SaveSocialMedia(0, TicketId, EmpId, txtOtherSocialMedia.Text.Trim(), chkOther.Checked);

                            //Fahad 6054 07/21/2009 Check If any other Client is exist related to same date and same Courthouse
                            if (ddlPaymentMethod.SelectedValue == "9" || ddlPaymentMethod.SelectedValue == "11" || ddlPaymentMethod.SelectedValue == "104" || ddlPaymentMethod.SelectedValue == "102")
                            {
                                if (ddlPaymentMethod.SelectedValue == "9")
                                {
                                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Attorney Credit");
                                }
                                else if (ddlPaymentMethod.SelectedValue == "11")
                                {
                                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Friend Credit");
                                }
                                // Abbas Qamar 9957 03-01-2012 Adding condition for Fee Wavied.
                                //Faique Ali 1051 05/17/2013 condition added for inserting trial note only when amount greater than 1$
                                else if (ddlPaymentMethod.SelectedValue == "102" && double.Parse(txtAMT.Text) > 1.00)
                                {
                                    //Faique ali 11051 05/17/2013 Change trial note text 
                                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Fee Waived");
                                }
                                // End 
                                else if (ddlPaymentMethod.SelectedValue == "104")
                                {
                                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Partner Firm Credit");
                                    clsFlags.TicketID = TicketId;
                                    clsFlags.FlagID = Convert.ToInt32(FlagType.OutsideFirmClient);
                                    bool checkflag = clsFlags.HasFlag();
                                    if (!(checkflag))
                                    {
                                        clsFlags.AddNewFlag();
                                    }
                                }
                                //Fahad 6054 07/23/2009 Add New Service Ticket
                                // Afaq 8213 11/01/10 replace code for add new service ticket to method.
                                //24 = Refund Review
                                AddServiceTicket("24");
                            }


                            double ContinuanceAmount = Convert.ToDouble(hf_continuancecount.Value);
                            if (ContinuanceAmount > 0)
                            {

                                double owesamount = Convert.ToDouble(lbl_owes.Text.Substring(1));
                                owesformail = Convert.ToString(owesamount);
                                double curr_paidamount = Convert.ToDouble(txtAMT.Text);
                                paidforemail = Convert.ToString(curr_paidamount);

                                //Asad Ali 8236 11/19/2010  Remove bug of paid amount compare with Continuance Flag count
                                //if ((curr_paidamount >= ContinuanceAmount) || (owesamount - curr_paidamount == 0))
                                if (curr_paidamount < owesamount || (owesamount - curr_paidamount == 0))
                                {
                                    string[] keys = { "@TicketID" };
                                    object[] values = { TicketId };
                                    cls_db.ExecuteSP("USP_HTS_Update_ContinuanceFlag_By_TicketID", keys, values);
                                }

                            }
                            txtAMT.Text = "";
                            txtAMT.Enabled = true;
                            lbl_scheduleid.Text = "0";
                            ddlPaymentMethod.SelectedValue = "0";
                            //btn_process. ="clsbutton";
                            //Initializing credit card section
                            txtCardholder.Text = "";
                            txtCardNumber.Text = "";
                            ddl_cardtype.SelectedIndex = 0;
                            txtCIN.Text = "";
                            ddl_expmonth.SelectedValue = "01";
                            //for oscar court
                            string[] key_1 = { "@ticketid" };
                            object[] value_1 = { TicketId };
                            DataSet ds_os = cls_db.Get_DS_BySPArr("usp_hts_check_activeflag", key_1, value_1);
                            if (ds_os.Tables[0].Rows.Count > 0)
                            {
                                //if (lbl_OscarCourtID.Text == "3061" & lbl_OscarActiveFlag.Text == "0" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1" & ds_os.Tables[0].Rows[0]["courtid"].ToString() == "3061")
                                if (lbl_OscarActiveFlag.Text == "0" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1")
                                {

                                    //+ "-CoverSheet-

                                    DateTime dtnow = DateTime.Today;
                                    string st = dtnow.Month + "-" + dtnow.Day + "-" + dtnow.Year;


                                    name = TicketId + "-CoverSheet-" + DateTime.Now.ToFileTime() + ".pdf";
                                    string path = "http://" + Request.ServerVariables["SERVER_NAME"] + "/clientinfo/CaseSummaryNew.aspx?cdate=" + DateTime.Now.ToFileTime() + "+&pdf=1&casenumber=" + this.TicketId.ToString() + "&search=" + ViewState["vSearch"].ToString();

                                    string[] keys = { "@TicketID_FK", "@BatchDate", "@PrintDate", "@IsPrinted", "@LetterID_FK", "@EmpID", "@DocPath" };
                                    object[] values = { TicketId.ToString(), DateTime.Now.Date, DateTime.Now.Date, 1, 9, EmpId, name };
                                    cls_db.InsertBySPArr("USP_HTS_Insert_BatchPrintLetter", keys, values);

                                    CaseSummary();
                                    //Sabir Khan 5084 11/10/2008 Send Message to text message number...                                        
                                    SendTextMessage();

                                    //ozair 3613 on 04/02/2008
                                    if (lbl_OscarActiveFlag.Text == "0" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1" & ClsCase.IsOscarCourt(this.TicketId))
                                    {
                                        SendMail(1);
                                    }

                                    //Sabir Khan 6025 06/11/2009 No need to assign Jackie Miller as attorney to AG Cases by default ...
                                    //-------------------------------------------------------------------------------------------------
                                    // Noufil 4747 09/09/2008 If courtid is 3077 then coevrage firm must be set to Jackie miller
                                    //if (ClsCase.IsHarrisCountyTitleCourt(this.TicketId))
                                    //    CaseDetails.UpdateCoverageFirm(Convert.ToInt32(clsCourts.CourtsName.HarrisCountyTitle4D), this.TicketId);
                                    //-------------------------------------------------------------------------------------------------
                                }

                                //Waqas Javed 5173 12/26/08 AddViolation is being done in UpdateViolation already, Calling once is enough. CastType checking only 2,3,4. No need to check
                                //We have allowed all cases whose covering firm is not sullo&sullo
                                //Sabir Khan 6248 07/31/2009 update event only for App, Arr, Pre, Jury and Judge statuses...
                                //Sabir Khan 6423 08/24/2009 commented becuase this need to execute in separate thread at the end of this method.
                                // updatepanel.ManageSharepointViolationEvents(this.TicketId, -1);
                                //SharePointWebWrapper.UpdateAllViolationsInCalendar(this.TicketId);    



                                //Aziz 2468 Email either hired or paid.                                    
                                //ozair 3613 on 04/02/2008

                                //Sabir Khan 4662 09/12/2008
                                //if (ClsCase.IsHCCCourt(this.TicketId) & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1")
                                if (ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "2" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1")
                                {
                                    string toUser = ConfigurationManager.AppSettings["HCCHiring"];
                                    string ccUser = ConfigurationManager.AppSettings["HCCHiringCC"];

                                    if (lbl_OscarActiveFlag.Text == "0")
                                    {
                                        SendMailtoall("Criminal", owesformail, paidforemail, "Criminal Case Hired", toUser, ccUser);
                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Signup notification email sent to " + toUser, "Signup notification email sent to " + toUser, TicketId);
                                        //SendMailHCC(owesformail, paidforemail, true);
                                        //Aziz 1974 06/20/08 Add calender Items in case of new criminal case.
                                        //SharePointWebWrapper.AddViolationsToCalendar(this.TicketId);
                                    }
                                    else
                                    {
                                        SendMailtoall("Criminal", owesformail, paidforemail, "Criminal Case Paid", toUser, ccUser);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Payment notification email sent to " + toUser, "Payment notification email sent to " + toUser, TicketId);

                                        //SendMailHCC(owesformail, paidforemail, false);
                                        // tahir 4631 08/16/2008 
                                        // update owes amount for all violations in share point criminal calendar
                                        //SharePointWebWrapper.UpdateAllViolationsInCalendar(this.TicketId);
                                    }
                                }

                                // Sarim 4646 08/20/2008
                                // This will send Email on Client First Payment
                                if (ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "3" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1")
                                {
                                    string toemail = ConfigurationManager.AppSettings["CivilHiring"];
                                    string ccemail = ConfigurationManager.AppSettings["CivilHiringCC"];

                                    if (lbl_OscarActiveFlag.Text == "0")
                                    {
                                        // Noufil 5003 10/21/2008 Using Generic method for email 
                                        SendMailtoall("Civil", owesformail, paidforemail, "Civil Case Hired", toemail, ccemail);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Signup notification email sent to " + toemail, "Signup notification email sent to " + toemail, TicketId);
                                        //SendMailCivil(owesformail, paidforemail, true);
                                        //Sabir Khan 4662 09/12/2008
                                        //SharePointWebWrapper.AddViolationsToCalendar(this.TicketId);
                                    }
                                    else
                                    {
                                        SendMailtoall("Civil", owesformail, paidforemail, "Civil Case Paid", toemail, ccemail);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Payment notification email sent to " + toemail, "Payment notification email sent to " + toemail, TicketId);
                                        //Sabir Khan 4662 09/12/2008
                                        //SendMailCivil(owesformail, paidforemail, false);
                                        //SharePointWebWrapper.UpdateAllViolationsInCalendar(this.TicketId);
                                    }
                                }

                                // Noufil 4782 09/25/2008  Send Email For family law
                                if (ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "4" & ds_os.Tables[0].Rows[0]["activeflag"].ToString() == "1")
                                {
                                    string toUser = ConfigurationManager.AppSettings["CivilHiring"];
                                    string ccUser = ConfigurationManager.AppSettings["CivilHiringCC"];

                                    if (lbl_OscarActiveFlag.Text == "0")
                                    {
                                        //Yasir Kamal 6114 07/30/2009 for Family law cases set covering firm to sull
                                        //Yasir Kamal 6430 11/12/2009 code moved in trigger and covering firm bug fixed.
                                        //CaseDetails.UpdateCoverageFirm(Convert.ToInt32(3000), this.TicketId,this.EmpId);

                                        SendMailtoall("Family Law", owesformail, paidforemail, "Family Law Case Hired", toUser, ccUser);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Signup notification email sent to " + toUser, "Signup notification email sent to " + toUser, TicketId);
                                        //SendMailFamilyLaw(owesformail, paidforemail, true);
                                        //SharePointWebWrapper.AddViolationsToCalendar(this.TicketId);
                                    }
                                    else
                                    {
                                        SendMailtoall("Family Law", owesformail, paidforemail, "Family Law Case Paid", toUser, ccUser);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Payment notification email sent to " + toUser, "Payment notification email sent to " + toUser, TicketId);
                                        //SendMailFamilyLaw(owesformail, paidforemail, false);
                                        //SharePointWebWrapper.UpdateAllViolationsInCalendar(this.TicketId);
                                    }
                                }

                                // Yasir Kamal 5558 02/20/2009 Send mail when hiring a case taking jury trial for HMC court 
                                if (lbl_OscarActiveFlag.Text == "0" && ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "1" && (ClsCase.IsInsideCourtCase(this.TicketId) == true))
                                {
                                    bool violationstatus = false;
                                    bool dayscount = false;
                                    int days;
                                    string shortname = string.Empty;
                                    clsCaseDetail ccasedetail = new clsCaseDetail();
                                    DataSet ds = ccasedetail.GetCaseDetail(TicketId);
                                    foreach (DataRow drr in ds.Tables[0].Rows)
                                    {
                                        if (Convert.ToInt32(drr["CourtViolationStatusIDmain"]) == 26)
                                        {
                                            violationstatus = true;
                                            days = (Convert.ToDateTime(drr["CourtDateMain"]) - DateTime.Today).Days;
                                            dayscount = (days >= 0 && days < 7) ? true : dayscount;
                                            shortname = Convert.ToString(drr["ShortName"]);
                                            if (dayscount)
                                            {
                                                break;
                                            }
                                        }

                                    }
                                    if (violationstatus && dayscount)
                                    {
                                        string toUser = ConfigurationManager.AppSettings["TrafficClientHireWithinWeek"];
                                        string ccUser = ConfigurationManager.AppSettings["TrafficClientHireWithinWeekCC"];

                                        string subj = "Traffic " + shortname + " - Jury Trial Case Hired";
                                        SendMailtoall("Traffic", owesformail, paidforemail, subj, toUser, ccUser);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Signup notification email sent to " + toUser, "Signup notification email sent to " + toUser, TicketId);
                                    }
                                }
                                //Zeeshan Haider 10699 04/26/2013 Validate PMC LOR Dates with Case Court Date
                                bool PMCLORDateValidation = true;
                                if (hf_IsLORDateInPastOrFull.Value == "1")
                                {
                                    PMCLORDateValidation = false;
                                    SendPmcPreTrialLorDateIntimation();
                                    HttpContext.Current.Response.Write("<script language='javascript'> alert('All available LOR dates are in the past or full, Please request the Administrator to change the LOR dates.'); </script>");
                                }
                                else if (hf_IsLORDateInFuture.Value == "0")
                                {
                                    PMCLORDateValidation = false;
                                    SendPmcPreTrialLorDateIntimation(TicketId);
                                    HttpContext.Current.Response.Write("<script language='javascript'> alert('The system is unable to generate and send a LOR because there are no available court setting dates. Please request the system administrator to update the LOR setting request date. This case will remain in its current status until a LOR is sent.'); </script>");
                                }

                                //Fahad 9022 03/14/2011 Check Added for Outside courts and capable for LOR
                                if (lbl_OscarActiveFlag.Text == "0" && ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "1" && (ClsCase.IsInsideCourtCase(this.TicketId) == false) && PMCLORDateValidation)
                                {
                                    clsCaseDetail ccasedetail = new clsCaseDetail();
                                    DataSet ds = ccasedetail.GetCaseDetail(TicketId);
                                    //Sabir Khan 9031 03/15/2011 Fixed LOR issue for PMC and all other courts having Fax as LOR Method on Court interface.
                                    bool isLORSentToBatch = false;
                                    //Babar Ahmad 9031 03/22/2011 Added functionality to check bond flag.
                                    //Babar Ahmad 9031 04/12/2011 Message Box added.
                                    foreach (DataRow drr in ds.Tables[0].Rows)
                                    {
                                        // Haris Ahmed 9931 12/01/2011 Add Pre-Trail Status
                                        // Rab Nawaz 10005 01/19/2012 Added Judge and Jury Trail Status
                                        if ((Convert.ToInt32(drr["CategoryID"]) == 1 || Convert.ToInt32(drr["CategoryID"]) == 12 || Convert.ToInt32(drr["CategoryID"]) == 2 || Convert.ToInt32(drr["CategoryID"]) == 3 || Convert.ToInt32(drr["CategoryID"]) == 4 || Convert.ToInt32(drr["CategoryID"]) == 5) && ((drr["LOR"].ToString() != "Fax") || (Convert.ToBoolean(ClsCase.GetBondFlag(this.TicketId)))) && (isLORSentToBatch == false))
                                        {
                                            clsBatch objClsBatch = new clsBatch();
                                            objClsBatch.SendLORToBatch(TicketId, EmpId, 6, true);
                                            isLORSentToBatch = true;
                                            HttpContext.Current.Response.Write("<script language='javascript'> alert('Letter of Rep�sent to batch successfully'); </script>");

                                        }

                                    }

                                }

                                //Sabir Khan 8058 07/23/2010 Shoot an email when PMC Pre Trial Cases has max out for selected court date.
                                if (lbl_OscarActiveFlag.Text == "0" && ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "1" && ds_os.Tables[0].Rows[0]["courtid"].ToString() == "3004")
                                {
                                    clsCaseDetail ccasedetail = new clsCaseDetail();
                                    DataSet ds = ccasedetail.GetCaseDetail(TicketId);
                                    int counter = 0;
                                    foreach (DataRow drr in ds.Tables[0].Rows)
                                    {
                                        if (Convert.ToInt32(drr["CourtViolationStatusIDmain"]) == 101 && Convert.ToInt32(drr["CourtID"]) == 3004)
                                        {
                                            counter = counter + 1;
                                            if (counter == 1)
                                            {
                                                DateTime courtDate = Convert.ToDateTime(drr["CourtDateMain"]);
                                                updatepanel.CourtDate = courtDate;
                                                updatepanel.CourtLocation = Convert.ToInt32(drr["CourtID"]);
                                                updatepanel.CheckPMCPreTrialMaxOutClient();

                                            }
                                        }

                                    }

                                }

                                // Ozair 5003 10/21/2008 Send outside court mail for traffic
                                // Zeeshan Haider 10276 07/23/2012 Below condition commented for 10276, removing InsideCourtCase Check
                                // if (lbl_OscarActiveFlag.Text == "0" && ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "1" && !(ClsCase.IsInsideCourtCase(this.TicketId)))

                                // Zeeshan Haider 10276 07/23/2012 Email for Late hires
                                if (lbl_OscarActiveFlag.Text == "0" && ds_os.Tables[0].Rows[0]["casetypeid"].ToString() == "1")
                                {
                                    bool violationstatus = false;
                                    bool dayscount = false;
                                    int days;
                                    string shortname = string.Empty;
                                    clsCaseDetail ccasedetail = new clsCaseDetail();
                                    DataSet ds = ccasedetail.GetCaseDetail(TicketId);
                                    foreach (DataRow drr in ds.Tables[0].Rows)
                                    {
                                        if (((Convert.ToInt32(drr["CourtViolationStatusIDmain"]) == 26) || (Convert.ToInt32(drr["CourtViolationStatusIDmain"]) == 101) || (Convert.ToInt32(drr["CourtViolationStatusIDmain"]) == 103)))
                                        {
                                            violationstatus = true;
                                            days = (Convert.ToDateTime(drr["CourtDateMain"]) - DateTime.Today).Days;
                                            dayscount = (days >= 0 && days < 15) ? true : dayscount;
                                            shortname = Convert.ToString(drr["ShortName"]);
                                            if (dayscount)
                                            {
                                                break;
                                            }
                                        }

                                    }
                                    if (violationstatus && dayscount)
                                    {
                                        // Zeeshan Haider 10276 07/23/2012 Below line commented for 10276, 
                                        // string toUser = ConfigurationManager.AppSettings["TrafficClientHireWithin2Week"];

                                        // Zeeshan Haider 10276 07/23/2012 Email For Late Hires
                                        string toUser = ConfigurationManager.AppSettings["EmailForLateHires"];
                                        string ccUser = ConfigurationManager.AppSettings["TrafficClientHireWithin2WeekCC"];
                                        string subj = "Traffic - " + shortname + " - LATE HIRE";
                                        SendLateHireEmail("Traffic", owesformail, paidforemail, subj, toUser, ccUser, true);

                                        //Sabir Khan 5578 02/25/2009 add note into case history...
                                        clog.AddNote(EmpId, "Signup notification email sent to " + toUser, "Signup notification email sent to " + toUser, TicketId);
                                    }
                                }

                                //Added By Zeeshan Ahmed
                                // Save PDF For The Document Page If Case Is Criminal 
                                ClsCase.TicketID = TicketId;
                                if (Convert.ToInt32(lbl_OscarActiveFlag.Text) == 0 && ClsCase.GetActiveFlag() == 1 && ClsCase.IsCriminalCase())
                                {
                                    clsCriminalCase CriminalCase = new clsCriminalCase();
                                    CriminalCase.TicketID = TicketId;
                                    CriminalCase.EmpID = EmpId;
                                    if (CriminalCase.SaveCriminalCasePDF() == false)
                                    {
                                        Response.Write(CriminalCase.PDFNotCreatedAlert());
                                    }
                                }

                                lbl_OscarActiveFlag.Text = ds_os.Tables[0].Rows[0]["activeflag"].ToString();
                                lbl_OscarCourtID.Text = ds_os.Tables[0].Rows[0]["courtid"].ToString();
                                //Fahad 5807 05/14/2009 If check Added to check Bond Flag and Court should be HCJP then Process
                                if (Convert.ToBoolean(ClsCase.GetBondFlag(TicketId)) && Convert.ToBoolean(ClsCase.IsHCJPCourt(TicketId)))
                                {
                                    Session["NewIsClient"] = ClsCase.IsClient(TicketId);
                                    if (Convert.ToBoolean(Session["OldIsClient"]) == false && Convert.ToBoolean(Session["NewIsClient"]))
                                    {
                                        tr_Process.Style.Add("display", "");
                                        trPaymentMsg.Style.Add("display", "none");
                                        tr_nextbutton.Style.Add("display", "");
                                        trgv_Payments.Style.Add("display", "none");
                                        tr_pleasewait.Style.Add("display", "none");
                                        gv_Payments.Visible = true;
                                        DataTable dtpaymnet = new DataTable();
                                        dtpaymnet = ClsCase.GetTicketNumber(TicketId);
                                        gv_Payments.DataSource = dtpaymnet;
                                        gv_Payments.DataBind();
                                        mp_msg.Show();
                                    }
                                }

                            }
                            else
                            {
                                lbl_OscarActiveFlag.Text = "";
                                lbl_OscarCourtID.Text = "";
                            }

                            GetPayments();

                            // Noufil 6126 07/23/2009 Update new hire status for client
                            ClsCase.UpdateClientNewHireStatus(TicketId, EmpId, 1, true);

                            //Nasir 6483 10/06/2009 set value for bond feild
                            if (ClsSession.GetCookie("sAccessType", Request) != "2")
                            {
                                hf_CheckBondReportForSecUser.Value = CaseDetails.SetForBondReport(TicketId);
                            }

                            //Sabir Khan 6423 08/21/2009 Call sharepoint method in a separate thread...
                            // Rab Nawaz Khan 10197 04/26/2012 Cases Information Update has been stoped on Matter Calender . . . 
                            //updatepanel.ManageSharepointViolation(TicketId, -1);
                            //Afaq 8213 11/01/2010 Add service ticket and continuance flag when continuance is selected from payment identifier drop down.
                            if (ddl_PaymentIdentifier.SelectedValue == "1")
                            {
                                WCC_ContinuanceComments.AddComments();
                                ServiceTicket.Category = "4";
                                ServiceTicket.TicketID = TicketId;
                                //Fahad 8213 05/30/2011 Removed check to add service ticket only one time
                                //Sabir Khan 9349 06/03/2011 Fixed Already exisitng service ticket issue
                                int ticketCount = ServiceTicket.CheckServiceTicket();
                                if (ticketCount == 0)
                                    AddServiceTicket("4");
                                else
                                    HttpContext.Current.Response.Write("<script language='javascript'> alert('Service Ticket cannot be added automatically as it already exist. Please manually add service ticket from contact page, if required.'); </script>");
                            }
                            //Afaq 8213 11/01/2010 Add service ticket and Probation Request flag when Deferred is selected from payment identifier drop down.
                            else if (ddl_PaymentIdentifier.SelectedValue == "2")
                            {
                                clsFlags.TicketID = TicketId;
                                clsFlags.EmpID = EmpId;
                                clsFlags.FlagID = 20;
                                if (!clsFlags.HasFlag())
                                    clsFlags.AddNewFlag();
                                ServiceTicket.Category = "34";
                                ServiceTicket.TicketID = TicketId;
                                //Fahad 8213 05/30/2011 Removed check to add service ticket only one time
                                //Sabir Khan 9349 06/03/2011 Fixed Already exisitng service ticket issue
                                int ticketCount = ServiceTicket.CheckServiceTicket();
                                if (ticketCount == 0)
                                    AddServiceTicket("34");
                                else
                                    HttpContext.Current.Response.Write("<script language='javascript'> alert('Service Ticket cannot be added automatically as it already exist. Please manually add service ticket from contact page, if required.'); </script>");
                            }
                            if (td_PaymentIdentifier.Style["display"] == "none")
                            {
                                td_PaymentIdentifier.Style["display"] = "";
                                //td_lbl_PaymentIdentifier.Style["display"] = "";
                            }

                            lblMessagesuccess.Text = "This transaction has been approved.";
                            // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                            PlcHSuccesmsg.Visible = true;
                            tr_errorMessages.Visible = false;

                        }
                        else
                        {
                            lblMessage.Text = "Payment cannot be processed";
                            // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                            tr_errorMessages.Visible = true;
                            PlcHSuccesmsg.Visible = false;

                        }
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = ex.Message;
                        // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                        tr_errorMessages.Visible = true;
                        PlcHSuccesmsg.Visible = false;
                        clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                        GetPayments();
                    }
                }
            }
            else
            {
                GetPayments();

            }
        }
        // Afaq 8213 11/01/2010 Method for adding new service ticket.
        private void AddServiceTicket(string catId)
        {
            ServiceTicket.TicketID = Convert.ToInt32(TicketId);
            ServiceTicket.EmpID = Convert.ToInt32(EmpId);
            ServiceTicket.Percentage = "0";
            ServiceTicket.Category = catId;
            ServiceTicket.Priority = "2"; //Sabir Khan 9349 06/03/2011 priority has been changed from low to High.
            ServiceTicket.ShowOnTrialDocket = 0;
            ServiceTicket.ShowServiceTicketInstruction = false;
            ServiceTicket.ServiceInstruction = string.Empty;
            ServiceTicket.AssignTo = 0;
            ServiceTicket.ShowGeneralComments = false;
            if (catId == "4")
            { ServiceTicket.ContinuanceOption = 1; } ////Sabir Khan 9349 06/03/2011 Pending has been set as Continuance Option for Continuance Service Ticket
            else
            { ServiceTicket.ContinuanceOption = 0; }
            ServiceTicket.ServiceTicketFollowUpdate = Convert.ToString(DateTime.Now);
            ServiceTicket.AddServiceTicket();
        }
        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                PaymentProcess();
            }
            catch (Exception ex)
            {

                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SendMail(int type)
        {
            try
            {
                DataTable dt = new DataTable();
                //TicketID = Convert.ToInt32(Request.QueryString["casenumber"]);
                string[] keys = { "@ticketid_pk" };
                object[] Values = { TicketId };
                //date 17-1-08, bug 2624
                ////commented by khalid //DS = cls_db.Get_DS_BySPArr("usp_hts_paymentemail", keys, Values);
                //Firstname = DS.Tables[0].Rows[0]["Firstname"].ToString();
                //Lastname = DS.Tables[0].Rows[0]["Lastname"].ToString();
                //paymentcomments = DS.Tables[0].Rows[0]["PymtComments"].ToString();
                //ticketnumber = DS.Tables[0].Rows[0]["refcasenumber"].ToString();
                //added by khalid bug 2624,17-10-08
                dt = cls_db.Get_DT_BySPArr("usp_hts_paymentemail", keys, Values);
                if (dt.Rows.Count > 0)
                {
                    Firstname = dt.Rows[0]["Firstname"].ToString();
                    Lastname = dt.Rows[0]["Lastname"].ToString();
                    paymentcomments = dt.Rows[0]["PymtComments"].ToString();
                    ticketnumber = dt.Rows[0]["refcasenumber"].ToString();
                }
                else
                {
                    lblMessage.Text = "Void Transaction Email,Client info not found";
                    // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                    tr_errorMessages.Visible = true;
                }

                if (type == 1)
                {
                    string toUser = ConfigurationManager.AppSettings["OsEmailTo"];
                    string ccUser = ConfigurationManager.AppSettings["OsEmailCC"];
                    MailClass.SendMailToAttorneys("This is a system generated email.", "An Oscar client hired.", Convert.ToString(ViewState["vFullPath"]), toUser, ccUser);
                    //Sabir Khan 5578 02/25/2009 add note into case history...
                    clog.AddNote(EmpId, "Signup notification email sent to " + toUser, "Signup notification email sent to " + toUser, TicketId);
                }
                if (type == 2)
                {
                    //khalid 2988 2/13/08 formated email for void payment
                    ServiceTicketComponent voidPay = new ServiceTicketComponent();
                    DataTable violations = voidPay.GetViolationInfoForEmail(ClsPayments.TicketID);
                    MailClass.SendPaymentNotificationEmail(ClsPayments.TicketID, Request.Url.Authority, Convert.ToString(cSession.GetCookie("sEmpName", this.Request)), violations);
                    //Sabir Khan 5578 02/25/2009 add note into case history...
                    clog.AddNote(EmpId, "Payment void notification email sent to " + ConfigurationManager.AppSettings["paymentEmail"].ToString(), "Payment void notification email sent to " + ConfigurationManager.AppSettings["paymentEmail"].ToString(), TicketId);


                }

                if (type == 3)
                {

                    string name = "<a href=" + Request.Url + ">" + Lastname + " " + Firstname + "</a>";
                    string casenumbers = ticketnumber;
                    string subject = "PaymentDetail----Refund � " + Lastname + "," + Firstname + " - " + casenumbers;

                    string toUser = ConfigurationManager.AppSettings["paymentEmail"];
                    string ccUser = ConfigurationManager.AppSettings["paymentEmailCC"];

                    //Modified By Zeeshan Ahmed On 12/12/2007
                    //Add Rep Name & General Comment in Payment Refund Notification Email.
                    //changed by khalid for control changing 1-1-08
                    MailClass.SendPaymentEmail("<table>\n<tr><td align=center class=label><h3><b>Payment Details - Refund</b></h3></td></tr>\n</table><table width=450 border=0 class=label >\n<tr><td width=173><b>Associated Case Numbers : </b> </td> <td width=267> " + casenumbers + "</td></tr><tr><td><b>Client Name : </td><td>" + name + "</td></tr><tr><td><b>Rep : </td><td>" + Convert.ToString(cSession.GetCookie("sEmpName", this.Request)).ToUpper() + "</td></tr><tr><td><b>General Comments : </td></tr><tr><td colspan=2>\n" + Convert.ToString(WCC_GeneralComments.Label_Text) + "</td></tr>\n</table>", subject, toUser, ccUser);
                    //txtGeneralComments.Text
                    //Sabir Khan 5578 02/25/2009 add note into case history...
                    clog.AddNote(EmpId, "Payment refund notification email sent to " + toUser, "Payment refund notification email sent to " + toUser, TicketId);
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = "Email not sent.";
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Noufil 5003 10/21/2008 Generic method for mail to each case type
        private void SendMailtoall(string casetype, string owes, string paid, string subject, string toUser, string ccUser)
        {
            DataSet DS = new DataSet();
            string[] keys = { "@ticketid_pk" };
            object[] Values = { TicketId };
            DS = cls_db.Get_DS_BySPArr("usp_hts_paymentemail", keys, Values);
            Firstname = DS.Tables[0].Rows[0]["Firstname"].ToString();
            Lastname = DS.Tables[0].Rows[0]["Lastname"].ToString();
            paymentcomments = DS.Tables[0].Rows[0]["PymtComments"].ToString();
            ticketnumber = DS.Tables[0].Rows[0]["refcasenumber"].ToString();
            string rep = cSession.GetCookie("sEmpName", Request).ToUpper();
            totalfee = Convert.ToDouble(DS.Tables[0].Rows[0]["TotalFeeCharged"]);
            paymenthistory = (DS.Tables[0].Rows[0]["PaymentHistory"].ToString()).Replace("PAYMENT:", "");
            totaldue = Convert.ToDouble(DS.Tables[0].Rows[0]["TotalDue"]);

            string name = "\n<a href=" + Request.Url + ">" + Lastname + " " + Firstname + "</a>";
            string casenumbers = ticketnumber;
            subject += " - " + (Lastname + ", " + Firstname);
            DataSet DSet = new DataSet();

            DSet = cls_db.Get_DS_BySPByOneParmameter("USP_HTS_GetCaseInfoDetail", "@ticketid", TicketId);

            string detail = @"<center><b><h3>Case Information</h3></b></center>";

            detail += "<style type=\"text/css\">.Labelwithborder{ FONT-WEIGHT: normal;   COLOR: #000000;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 8.5pt;  }</style>";


            detail += "<table align=center width='456' border=1 cellpedding=0 cellspacing =0><tr><td width='111' class=Labelwithborder><b>Ticket#</b></td><td width='220' class=Labelwithborder ><b>Matter Desc</b></td><td width='98'class=Labelwithborder ><b>Court date</b></td><td width='99' class=Labelwithborder><b>Time</b></td><td width='34'class=Labelwithborder><b>Crt#</b></td><td width='54'class=Labelwithborder><b>Status</b></td>";
            if (casetype.Trim() == "Criminal")
                detail += "<td width='20'class=Labelwithborder><b>Lvl</b></td></tr>";
            else
                detail += "</tr>";

            foreach (DataRow dr in DSet.Tables[0].Rows)
            {
                //Sabir Khan 5558 03/11/2009 Add ticket No if Cause Number is not exist...
                ArrayList stringArray = new ArrayList();
                stringArray.Add("N/A");
                stringArray.Add("n/a");
                stringArray.Add("");
                if (stringArray.Contains(dr["CauseNumber"].ToString()) || dr["CauseNumber"] == null)

                    detail += "<tr><td class=Labelwithborder>" + dr["RefCaseNumber"] + "</td><td class=Labelwithborder>" + dr["violationDescription"] + "</td><td class=Labelwithborder>" + Convert.ToDateTime(dr["courtdatemain"]).ToString("MM/dd/yyyy") + "</td>" + "<td class=Labelwithborder>&nbsp;" + Convert.ToDateTime(dr["courtdatemain"]).ToString("HH:mm tt") + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["CourtNumbermain"] + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["AutoCourtDesc"] + "</td>";
                else
                    detail += "<tr><td class=Labelwithborder>" + dr["CauseNumber"] + "</td><td class=Labelwithborder>" + dr["violationDescription"] + "</td><td class=Labelwithborder>" + Convert.ToDateTime(dr["courtdatemain"]).ToString("MM/dd/yyyy") + "</td>" + "<td class=Labelwithborder>&nbsp;" + Convert.ToDateTime(dr["courtdatemain"]).ToString("HH:mm tt") + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["CourtNumbermain"] + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["AutoCourtDesc"] + "</td>";

                if (casetype.Trim() == "Criminal")
                    detail += "<td class=Labelwithborder>" + dr["LevelCode"] + "</td>\n</tr>\n";
                else
                    detail += "\n</tr>\n";
            }
            // Noufil 5003 10/21/2008 cause number remove on Sarim bhai's Request
            detail += "</table>";
            //ozair 5003 10/24/2008 if case is hired then email body heading shold have Hired else Payment
            string paymentType = "PAYMENT";
            if (lbl_OscarActiveFlag.Text == "0")
            {
                paymentType = "HIRED";
            }
            MailClass.SendPaymentEmail("<table border=1 align=\"center\" class=\"label\"><tr><td align=\"center\" class=\"label\" style=\"height: 14px\"><h3><b>" + casetype.ToUpper() + " CASE " + paymentType + " </b></h3></td></tr></table><br /><br /><br /><table class=\"label\" align=\"center\" style=\"width: 446px\"><tr><td align=\"left\"><table width=\"100%\" class=\"label\"><tr><td style=\"width: 130px\"><b>CLIENT NAME : </b></td><td>" + name + "</td>\n</tr><tr><td style=\"width: 130px\"><b>TOTAL FEE : </b></td><td>$" + totalfee.ToString() + "</td>\n</tr><tr><td valign=\"top\"><b>PAYMENT:</b></td><td>" + paymenthistory + "</td>\n</tr><tr><td style=\"width: 130px\"><b>TOTAL DUES:</b></td><td>$" + totaldue.ToString() + "</td>\n</tr></table></td></tr></table>" + detail, subject, toUser, ccUser);

        }

        // Zeeshan Haider 10276 07/31/2012 LATE HIRE EMAIL
        private void SendLateHireEmail(string casetype, string owes, string paid, string subject, string toUser, string ccUser, bool isLateHire)
        {
            DataSet DS = new DataSet();
            string[] keys = { "@ticketid_pk" };
            object[] Values = { TicketId };
            DS = cls_db.Get_DS_BySPArr("usp_hts_paymentemail", keys, Values);
            Firstname = DS.Tables[0].Rows[0]["Firstname"].ToString();
            Lastname = DS.Tables[0].Rows[0]["Lastname"].ToString();
            paymentcomments = DS.Tables[0].Rows[0]["PymtComments"].ToString();
            ticketnumber = DS.Tables[0].Rows[0]["refcasenumber"].ToString();
            string rep = cSession.GetCookie("sEmpName", Request).ToUpper();
            totalfee = Convert.ToDouble(DS.Tables[0].Rows[0]["TotalFeeCharged"]);
            paymenthistory = (DS.Tables[0].Rows[0]["PaymentHistory"].ToString()).Replace("PAYMENT:", "");
            totaldue = Convert.ToDouble(DS.Tables[0].Rows[0]["TotalDue"]);

            string name = "\n<a href=" + Request.Url + ">" + Lastname + " " + Firstname + "</a>";
            string casenumbers = ticketnumber;
            subject += " - " + (Lastname + ", " + Firstname);

            DataSet DSet = new DataSet();

            DSet = cls_db.Get_DS_BySPByOneParmameter("USP_HTS_GetCaseInfoDetail", "@ticketid", TicketId);

            string detail = @"<center><b><h3>Case Information</h3></b></center>";

            detail += "<style type=\"text/css\">.Labelwithborder{ FONT-WEIGHT: normal;   COLOR: #000000;  FONT-STYLE: normal;  FONT-FAMILY: Verdana; FONT-SIZE: 8.5pt;  }</style>";


            detail += "<table align=center width='456' border=1 cellpedding=0 cellspacing =0><tr><td width='111' class=Labelwithborder><b>Ticket#</b></td><td width='220' class=Labelwithborder ><b>Matter Desc</b></td><td width='98'class=Labelwithborder ><b>Court date</b></td><td width='99' class=Labelwithborder><b>Time</b></td><td width='34'class=Labelwithborder><b>Crt#</b></td><td width='54'class=Labelwithborder><b>Status</b></td>";
            if (casetype.Trim() == "Criminal")
                detail += "<td width='20'class=Labelwithborder><b>Lvl</b></td></tr>";
            else
                detail += "</tr>";

            foreach (DataRow dr in DSet.Tables[0].Rows)
            {
                // Only JUR,PRE,JUD Cases are included
                if (((Convert.ToInt32(dr["CourtViolationStatusIDmain"]) == 26) || (Convert.ToInt32(dr["CourtViolationStatusIDmain"]) == 101) || (Convert.ToInt32(dr["CourtViolationStatusIDmain"]) == 103)))
                {
                    // Add ticket No if Cause Number is not exist
                    ArrayList stringArray = new ArrayList();
                    stringArray.Add("N/A");
                    stringArray.Add("n/a");
                    stringArray.Add("");
                    if (stringArray.Contains(dr["CauseNumber"].ToString()) || dr["CauseNumber"] == null)

                        detail += "<tr><td class=Labelwithborder>" + dr["RefCaseNumber"] + "</td><td class=Labelwithborder>" + dr["violationDescription"] + "</td><td class=Labelwithborder>" + Convert.ToDateTime(dr["courtdatemain"]).ToString("MM/dd/yyyy") + "</td>" + "<td class=Labelwithborder>&nbsp;" + Convert.ToDateTime(dr["courtdatemain"]).ToString("HH:mm tt") + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["CourtNumbermain"] + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["AutoCourtDesc"] + "</td>";
                    else
                        detail += "<tr><td class=Labelwithborder>" + dr["CauseNumber"] + "</td><td class=Labelwithborder>" + dr["violationDescription"] + "</td><td class=Labelwithborder>" + Convert.ToDateTime(dr["courtdatemain"]).ToString("MM/dd/yyyy") + "</td>" + "<td class=Labelwithborder>&nbsp;" + Convert.ToDateTime(dr["courtdatemain"]).ToString("HH:mm tt") + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["CourtNumbermain"] + "</td><td class=Labelwithborder>&nbsp;&nbsp;" + dr["AutoCourtDesc"] + "</td>";

                    if (casetype.Trim() == "Criminal")
                        detail += "<td class=Labelwithborder>" + dr["LevelCode"] + "</td>\n</tr>\n";
                    else
                        detail += "\n</tr>\n";
                }
            }

            detail += "</table>";

            // If case is hired then email body heading shold have Hired else Payment
            string paymentType = "PAYMENT";
            if (lbl_OscarActiveFlag.Text == "0")
            {
                paymentType = "HIRED";
            }
            MailClass.SendPaymentEmail("<table border=1 align=\"center\" class=\"label\"><tr><td align=\"center\" class=\"label\" style=\"height: 14px\"><h3><b>" + casetype.ToUpper() + " CASE " + paymentType + " </b></h3></td></tr></table><br /><br /><br /><table class=\"label\" align=\"center\" style=\"width: 446px\"><tr><td align=\"left\"><table width=\"100%\" class=\"label\"><tr><td style=\"width: 130px\"><b>CLIENT NAME : </b></td><td>" + name + "</td>\n</tr><tr><td style=\"width: 130px\"><b>TOTAL FEE : </b></td><td>$" + totalfee.ToString() + "</td>\n</tr><tr><td valign=\"top\"><b>PAYMENT:</b></td><td>" + paymenthistory + "</td>\n</tr><tr><td style=\"width: 130px\"><b>TOTAL DUES:</b></td><td>$" + totaldue.ToString() + "</td>\n</tr></table></td></tr></table>" + detail, subject, toUser, ccUser);

        }

        protected void dg_Schedule_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                //to prompt
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ImageButton btnButton = (ImageButton)e.Item.FindControl("ImgDelete");
                    btnButton.Attributes.Add("onclick", "return PromptDelete();");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void IsAlreadyInBatchPrint()
        {
            try
            {
                lbl_court.Text = Request.QueryString["casenumber"];
                string[] keys = { "@TicketID" };
                object[] values = { Request.QueryString["casenumber"] };
                DataTable dtBatchPrint = new DataTable();
                dtBatchPrint = cls_db.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
                if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                    lbl_IsAlreadyInBatchPrint.Text = "1";
                else
                    lbl_IsAlreadyInBatchPrint.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Abid Ali 5359 1/1/2009 Check LOR Already in batch print
        /// <summary>
        /// Check LOR already in batch print       
        /// </summary>
        private void IsLORAlreadyInBatchPrint()
        {
            try
            {
                string[] keys = { "@TicketID", "@LetterType" };
                object[] values = { Request.QueryString["casenumber"], 6 };
                DataTable dtBatchPrint = new DataTable();
                dtBatchPrint = cls_db.Get_DT_BySPArr("USP_HTS_GET_BATCHLETTER_CHECK_BATCHPRINT", keys, values);
                if (Convert.ToInt32(dtBatchPrint.Rows[0][0].ToString()) > 0)
                    lbl_IsLORAlreadyInBatchPrint.Text = "1";
                else
                    lbl_IsLORAlreadyInBatchPrint.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void CheckSpilitCases()
        {
            try
            {
                string[] keys = { "@ticketid" };
                object[] values = { Request.QueryString["casenumber"] };
                DataTable dtSplit = new DataTable();
                dtSplit = cls_db.Get_DT_BySPArr("USP_HTS_PAYMENTINFO_GET_SPLITCaseCount", keys, values);

                if (Convert.ToInt32(dtSplit.Rows[0][0].ToString()) > 0)
                    lbl_IsSplit.Text = "1";
                else
                    lbl_IsSplit.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btn_coversheet_Click1(object sender, EventArgs e)
        {
            try
            {

                Response.Redirect("CaseSummaryNew.aspx?casenumber=" + TicketId + "&search=" + ViewState["vSearch"], false);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void RestrictTrialNotification()
        {
            try
            {

                object value = TicketId;

                TrialNotRestrict.Value = value;
                DataSet DS = TrialNotRestrict.RestrictTrialLetterOnPaymentInfo();

                if (DS.Tables[0].Rows.Count > 0)
                {
                    int count = Convert.ToInt32(DS.Tables[0].Rows[0]["flagcount"]);
                    if (count > 0)
                        lblTrailNotFlag.Text = "0";
                    else
                        lblTrailNotFlag.Text = "1";

                }
                else
                {
                    lblTrailNotFlag.Text = "1";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Nasir 6013 07/16/2009 remove LOR click event 
        //Nasir 6181 07/25/2009 Add LOR click event 
        protected void btn_letterofRep_Click(object sender, EventArgs e)
        {
            try
            {
                //Sabir Khan 9941 12/14/2011 Send PMC Intimation for LOR Dates.
                if (hf_IsLORDateInPastOrFull.Value == "1")
                {
                    SendPmcPreTrialLorDateIntimation();
                }
                // Zeeshan Haider 10699 04/18/2013 PMC Cases for LOR 
                else if (hf_IsLORDateInFuture.Value == "0")
                {
                    SendPmcPreTrialLorDateIntimation(TicketId);
                }
                else
                {
                    Button btn = sender as Button;
                    if (cSession.GetCookie("sEmpID", Request) != "")
                    {
                        if (btn != null && btn.Text.Trim().ToLower() == "fax lor")
                        {
                            clsCrsytalComponent Cr = new clsCrsytalComponent();
                            Cr.CreateApprovalReportLOR(TicketId,
                                                       Convert.ToInt32(ClsSession.GetCookie("sEmpID", Request)), 6,
                                                       Server.MapPath("../Reports") + "\\Wordreport.rpt", Session,
                                                       Response);
                            //CreateApprovalReport(Convert.ToInt32(ViewState["vTicketId"]), Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), 6);
                            Faxcontrol1.Ticketid = TicketId;
                            Faxcontrol1.Empid = Convert.ToInt32((ClsSession.GetCookie("sEmpID", Request)));
                            Faxcontrol1.Attachment = Session["reportpath"].ToString();
                            Faxcontrol1.GetCaseInformation();
                            ModalPopupExtender1.Show();

                        }

                    }

                    else
                    {
                        Response.Redirect("../frmlogin.aspx", false);
                    }

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }

        private void CheckForNoTrialLetterSent()
        {
            try
            {
                int ActiveFlag = 0;
                int StatusCategoryID = 0;

                object value = TicketId;
                TrialNotRestrict.Value = value;
                DataSet DS = TrialNotRestrict.CheckNoTrialLetterFlag();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ActiveFlag = Convert.ToInt32(DS.Tables[0].Rows[0]["ActiveFlag"]);
                    StatusCategoryID = Convert.ToInt32(DS.Tables[0].Rows[0]["categoryid"]);
                    if (ActiveFlag == 0)
                    {
                        if (StatusCategoryID == 4 || StatusCategoryID == 3)
                            HFNoTrialLetter.Value = "1";
                    }
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        private void CheckReadNotes(int ticketid)
        {
            bool checkflag = false;
            checkflag = notes.CheckReadNoteComments(ticketid);
            if (checkflag)
            {
                read.Visible = true;
            }
            else
            {
                read.Visible = false;
            }
            //  read.Style[HtmlTextWriterStyle.Display] = checkflag ? "" : "none";
        }


        private void CaseSummary()
        {
            try
            {

                DataSet ds;

                string RptSetting = "123456";

                String[] reportname = new[] { "Notes.rpt", "Matters.rpt" };

                string filename = Server.MapPath("\\Reports") + "\\CaseSummary.rpt";


                string[] keyssub = { "@TicketID" };
                object[] valuesub = { TicketId.ToString() };
                ds = cls_db.Get_DS_BySPArr("USP_HTS_GET_SubReports", keyssub, valuesub);


                string path = ViewState["vNTPATHCaseSummary"].ToString();
                string[] key = { "@TicketId_pk", "@Sections" };
                object[] value1 = { TicketId.ToString(), RptSetting };

                Cr.CreateSubReports(filename, "usp_hts_get_casesummary_report_data", key, value1, reportname, ds, 1, Session, Response, path, TicketId.ToString(), false, name);
                ViewState["vFullPath"] = path + name;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
            }
        }

        protected void btn_VoidUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ClsPayments.TicketID = TicketId;
                ClsPayments.EmpID = EmpId;

                //getting invoice number and payment method from hidden fields            
                int invoicenum = Convert.ToInt32(hf_InvNum.Value);
                string Paymentmethod = hf_PayMethod.Value;
                //voidings the selected payment
                ClsPayments.VoidPayment(invoicenum, Paymentmethod);
                //adding general comments for payment void from control
                WCC_VoidGeneralComments.TextBox_Text = "Payment Void Reason: " + WCC_VoidGeneralComments.TextBox_Text;
                WCC_VoidGeneralComments.AddComments();
                //populating page to display these changes.
                GetPayments();
                //sending email.          
                SendMail(2);

                // tahir 4631 08/16/2008 
                // update owes amount for all violations in share point criminal calendar
                //Sabir Khan 6248 07/31/2009 update event only for App, Arr, Pre, Jury and Judge statuses...                 
                //updatepanel.ManageSharepointViolationEvents(this.TicketId, -1);

                //SharePointWebWrapper.UpdateAllViolationsInCalendar(this.TicketId);

                // if total paid amount is zero. means it is no more a client...
                // then remove all the events from the calendar....
                //Sabir Khan 6423 08/21/2009 code commented...
                //if (Convert.ToInt32(lbl_Paid.Text.Substring(1)) == 0)
                //{
                //    SharePointWebWrapper.DeleteAllViolationsInCalendar(this.TicketId);
                //}
                //Fahad 6054 07/23/2009 Insert Comments in Trial Comments in case of payment Voided
                if (Paymentmethod.Contains("Attorney Credit"))
                {
                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Attorney Credit Voided");
                }
                else if (Paymentmethod.Contains("Friend Credit"))
                {
                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Friend Credit Voided");
                }

                else if (Paymentmethod.Contains("Partner Firm Credit"))
                {
                    ClsPayments.InsertPaymentTrialComments(TicketId, EmpId, 4, "Partner Firm Credit Voided");
                }

                //Nasir 6483 10/06/2009 set value for bond feild
                if (ClsSession.GetCookie("sAccessType", Request) != "2")
                {
                    hf_CheckBondReportForSecUser.Value = CaseDetails.SetForBondReport(TicketId);
                }


                //Sabir Khan 6423 08/21/2009 Need to call sharepoint event method in separate thread...
                // Rab Nawaz Khan 10197 04/26/2012 Cases Information Update has been stoped on Matter Calender . . . 
                // updatepanel.ManageSharepointViolation(TicketId, -1);
                // Afaq 8213 11/01/2010 hide drop down of payment identifier if all payments are void (non client).
                if (lbl_OscarActiveFlag.Text == "0")
                {
                    //td_lbl_PaymentIdentifier.Style["display"] = "none";
                    td_PaymentIdentifier.Style["display"] = "none";
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Sabir Khan 5084 11/11/2008 Sending text message...
        private void SendTextMessage()
        {
            clsFirms objFirm = new clsFirms();
            DataTable dtFirmMessage = objFirm.GetFirmTextMessageInfo(TicketId);
            if (dtFirmMessage.Rows.Count > 0)
            {
                if (Convert.ToInt32(dtFirmMessage.Rows[0]["sendtextmessage"]) != 0)
                {
                    int totalday = general.GetTotalDaysFromBusinessDays(Convert.ToInt32(dtFirmMessage.Rows[0]["noticeperioddays"]));
                    if (Convert.ToDateTime(dtFirmMessage.Rows[0]["CourtDateMain"]).Date < DateTime.Today.AddDays(totalday) || Convert.ToInt32(dtFirmMessage.Rows[0]["noticeperioddays"]) == -1)
                    {
                        string Subject = lbl_LastName.Text + "," + lbl_FirstName.Text + " " + Convert.ToString(dtFirmMessage.Rows[0]["CourtDateMain"]) + ", #" + Convert.ToString(dtFirmMessage.Rows[0]["CourtNumberMain"]) + " - please confirm";
                        string Body = lbl_LastName.Text + "," + lbl_FirstName.Text + " " + Convert.ToString(dtFirmMessage.Rows[0]["CourtDateMain"]) + ", #" + Convert.ToString(dtFirmMessage.Rows[0]["CourtNumberMain"]) + " - please confirm";
                        //sabir Khan 5298 12/03/2008 sender address has been added...
                        //MailClass.SendMailToAttorneys(Body, Subject, "", Convert.ToString(dtFirmMessage.Rows[0]["textmessagenumber"]), "");
                        MailClass.SendMail(Convert.ToString(dtFirmMessage.Rows[0]["SenderEmail"]), Convert.ToString(dtFirmMessage.Rows[0]["textmessagenumber"]), Subject, Body, "");

                        string HistorySubject = "Hiring Notification text message sent to " + Convert.ToString(dtFirmMessage.Rows[0]["textmessagenumber"]) + ".";
                        clog.AddNote(EmpId, HistorySubject, HistorySubject, TicketId);
                    }

                }
            }
        }

        /// <summary>
        /// Waqas 5771 04/15/2009
        /// To Update IsContactIDConfirmed Flag of the case.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCIDCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                clsContact cContact = new clsContact();
                cContact.UpdateCIDConfirmationFlag(TicketId, 1, EmpId);
                DisplayInfo();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// Waqas 5771 04/15/2009
        /// To Update IsContactIDConfirmed Flag of the case.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCIDInCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                clsContact cContact = new clsContact();
                cContact.UpdateCIDConfirmationFlag(TicketId, 0, EmpId);
                DisplayInfo();
                lblMessage.Text = "This client�s CID has been disassociated. Please select a new CID for this client.";
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
                tr_errorMessages.Visible = true;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Noufil 6052 09/17/2009 add grid row command event
        protected void gv_AttorneyPayouthistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                GridViewRow gv_row = gv_AttorneyPayouthistory.Rows[Convert.ToInt32(e.CommandArgument)];
                int ticketviolationid = Convert.ToInt32(((HiddenField)(gv_row.FindControl("hf_ticketviolationid"))).Value);
                string amount = ((Label)(gv_row.FindControl("lbl_Pamount"))).Text.Replace(" ", "");
                cfirm.UpdateAttorneyPaidAmount(0, 0.0, DateTime.Now, ticketviolationid);
                GetAttorneypaidHistoryforCriminalCase();
                clog.AddNote(EmpId, "Attorney paid amount of " + amount + " removed from the profile.", "Attorney paid amount of " + amount + " removed from the profile.", TicketId);
                Response.Redirect(Request.Url.AbsoluteUri, false);
            }
        }

        private void GetAttorneypaidHistoryforCriminalCase()
        {
            // Noufil 6052 09/17/2009 show attorney paid information if attorney paid letter is printed for criminal case.

            DataTable dt_attorneypaid = new DataTable();
            dt_attorneypaid = cfirm.GetAttorneyPaidInfoByTicketid(TicketId);

            if (dt_attorneypaid != null && dt_attorneypaid.Rows.Count > 0)
            {
                tr_attorneygrid.Style[HtmlTextWriterStyle.Display] = "";
                /// tr_attorneyseperator.Style[HtmlTextWriterStyle.Display] = "";
                // tr_attorneysubhead.Style[HtmlTextWriterStyle.Display] = "";
                gv_AttorneyPayouthistory.DataSource = dt_attorneypaid;
                gv_AttorneyPayouthistory.DataBind();

                // Allow only admin user to delete the Attorney paid amount
                gv_AttorneyPayouthistory.Columns[7].Visible = cSession.GetCookie("sAccessType", Request) == "2";
            }
            else
            {
                tr_attorneygrid.Style[HtmlTextWriterStyle.Display] = "none";
                //  tr_attorneyseperator.Style[HtmlTextWriterStyle.Display] = "none";
                //  tr_attorneysubhead.Style[HtmlTextWriterStyle.Display] = "none";
            }

        }

        // Noufil 6052 09/17/2009 add row deleting event.
        protected void gv_AttorneyPayouthistory_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //  Please don't delete this event it will cause error on removal for attorney pay out grid.
        }

        // Noufil 6052 09/17/2009 add row databound event.
        protected void gv_AttorneyPayouthistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ((ImageButton)e.Row.FindControl("img_delete")).CommandArgument = Convert.ToString(e.Row.RowIndex);
            }
        }

        // Rab Nawaz Khan 04/10/015 Added to check all the flages through single DB trip. . . 
        private void CheckFlags(int ticketId)
        {

            DataTable dtFlags = clsFlags.GetFlages(ticketId);
            clsFlags.FlagID = Convert.ToInt32(FlagType.WrongNumber);
            bool isProblemClient = false;
            bool isProblemClientAlloHire = true;
            clsFlags.EmpID = EmpId;

            if (dtFlags != null && dtFlags.Rows.Count > 0)
            {
                for (int loopCounter = 0; loopCounter < dtFlags.Rows.Count; loopCounter++)
                {
                    if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.bademail))
                    {
                        lblbademail.Visible = true;
                    }
                    else if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.WrongNumber))
                    {
                        lblbadnumber.Visible = true;
                    }
                    else if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.ProblemClient_NoHire))
                    {
                        isProblemClient = true;
                        lblProblemClient1.Text = "This is a " + clsFlags.Description;
                        lblProblemClient.Visible = true;
                    }
                }

                if (!isProblemClient)
                {
                    for (int loopCounter = 0; loopCounter < dtFlags.Rows.Count; loopCounter++)
                    {
                        if (Convert.ToInt32(dtFlags.Rows[loopCounter]["FlagID"].ToString()) == Convert.ToInt32(FlagType.ProblemClient_AllowHire))
                        {
                            lblProblemClient1.Text = "This is a " + clsFlags.Description;
                            lblProblemClient.Visible = true;
                            isProblemClientAlloHire = false;
                        }
                    }
                    if (isProblemClientAlloHire)
                        lblProblemClient.Visible = false;
                }
            }

            tr_InfoMessages.Style["display"] = lblbadnumber.Visible ? "block" : "none";
            tr_InfoMessages.Style["display"] = lblbademail.Visible ? "block" : "none";

            //lblbadnumber.Visible = (clsFlags.HasFlag() ? true : false);
            //// Noufil  bad email label added
            //clsFlags.FlagID = Convert.ToInt32(FlagType.bademail);
            //lblbademail.Visible = (clsFlags.HasFlag() ? true : false);

            //// Rab Nawaz Khan 10349 08/30/2013 Added to remove the extra white Space after Menue. . .  
            //tr_InfoMessages.Style["display"] = lblbadnumber.Visible ? "block" : "none";
            //tr_InfoMessages.Style["display"] = lblbademail.Visible ? "block" : "none";

            //clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_NoHire);
            //if (clsFlags.HasFlag())
            //{
            //    lblProblemClient.Text = "This is a " + clsFlags.Description;
            //    lblProblemClient.Visible = true;
            //}
            //else
            //{

            //    //ozair 4846 11/14/2008 renamed ProblemClient_Becareful to ProblemClient_AllowHire
            //    clsFlags.FlagID = Convert.ToInt32(FlagType.ProblemClient_AllowHire);
            //    if (clsFlags.HasFlag())
            //    {
            //        lblProblemClient.Text = "This is a " + clsFlags.Description;
            //        lblProblemClient.Visible = true;
            //    }
            //    else
            //    {
            //        lblProblemClient.Visible = false;
            //    }
            //}
        }

        //Sabir Khan 9941 12/14/2011 Send PMC Max Out or Past Date intomation.
        private void SendPmcPreTrialLorDateIntimation()
        {

            string pmcMaxOutTo = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.EMAIL_TO_ADDRESS);
            string fromAddress = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.EMAIL_FROM_ADDRESS);
            string PMCMaxOutSubject = "PMC Pre Trial LOR Dates Alert";
            string PMCMaxOutBody = "All available LOR dates are in the past or full, Please request the Administrator to change the LOR dates.";
            LNHelper.MailClass.SendMailWithPriority(fromAddress, pmcMaxOutTo, PMCMaxOutSubject, PMCMaxOutBody, "", "", "", System.Net.Mail.MailPriority.High);
        }

        // Zeeshan Haider 10699 04/18/2013 PMC Cases for LOR
        private void SendPmcPreTrialLorDateIntimation(int TicketId)
        {
            string[] key = { "@TicketID_PK" };
            object[] value = { TicketId };
            DataSet DS = cls_db.Get_DS_BySPArr("USP_HTS_Get_ClientInfo", key, value);
            string TicketNumber = DS.Tables[0].Rows[0]["TicketNumber"].ToString();
            TicketNumber = TicketNumber.Split(',').Length > 0 ? TicketNumber.Split(',')[0] : TicketNumber;

            string toAddress = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.EMAIL_TO_ADDRESS);
            string fromAddress = Client.GetValueOfKey(Division.HOUSTON, Module.TRAFFIC_PROGRAM, SubModule.Pasadena, Key.EMAIL_FROM_ADDRESS);
            string emailSubject = "PMC Pre Trial LOR Dates Alert";
            string emailBody = "Please update the setting request date for Pasadena and send a LOR for case number: " + TicketNumber;
            LNHelper.MailClass.SendMailWithPriority(fromAddress, toAddress, emailSubject, emailBody, "", "", "", System.Net.Mail.MailPriority.High);
        }
    }
}
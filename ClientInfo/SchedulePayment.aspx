<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ClientInfo.SchedulePayment"
    CodeBehind="SchedulePayment.aspx.cs" %>


<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>Schedule Payment</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #TableDates {
            width: 776px;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>


    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>

   <%-- <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>--%>

    <script language="javascript" type="text/javascript">

        function ValidateForm() {
            var active = 0;
            active = document.getElementById("lbl_activeflag").innerText;

            if (active == '0') {
                doyou = confirm("Are you sure you want to schedule a payment for a client that has no initial down payment. Please click OK for Yes and Cancel for No");
                if (doyou == true) {
                    if (check() == false) {
                        return false;
                    }
                    else if (document.getElementById("txt_amount").value != (document.getElementById("lbl_owes").innerText) * 1) {
                        alert('Scheduled amount is less than owed amount, Please schedule the full owed amount');
                    }
                }
                else {
                    window.close();
                    return false;
                }
            }
            else
                if (check() == false)
                    return false;

            // Noufil 5802 05/21/2009 Check Date for Business Days
            if (CheckScheduledate() == false)
                return false;
        }
        function CloseForm()
        {
            window.close();
            return false;
        }
        function check() {
            var intamount = 0;
            intamount = document.getElementById("txt_amount").value;

            if (isNaN(intamount) == false && intamount != "" && intamount > 0) {
                if ((document.getElementById("lbl_owes").innerText) * 1 < intamount) {
                    alert("Incorrect payment amount. A client cannot pay more than he owes");
                    document.getElementById("txt_amount").focus();
                    return false;
                }
            }
            else {
                alert("Incorrect payment amount.");
                document.getElementById("txt_amount").focus();
                return false;
            }

            // Noufil 5802 05/21/2009 code comment

            //			var date =document.getElementById("date_payment").value;		
            //			if (date=="")
            //			{
            //				alert ("Please specify payment date");			
            //				return false;
            //			}	
        }

        // Noufil 5802 05/21/2009 Check Date for Business Days
        function CheckScheduledate() {
            var scheduledate = document.getElementById("date_payment").value;

            if (scheduledate == "") {
                alert("Please specify payment date");
                return false;
            }

            var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
            var newseldatestr = formatDate((Date.parseInvariant(scheduledate).getMonth() + 1) + "/" + Date.parseInvariant(scheduledate).getDate() + "/" + Date.parseInvariant(scheduledate).getFullYear(), "MM/dd/yyyy");
            newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
            if ((weekday[newseldate.getDay()] == "Sunday")) {
                alert("Please select any business day.");
                return false;
            }
        }



        function changeBackTo(myWidth, myHeight) {
            window.resizeTo(myWidth, myHeight);
        }

        function ValidiatePayment()
        {
            var pmnt = document.getElementById('txt_amount').value;
            var regex = /^(\d*\.?\d*)$/;
            if (!regex.test(pmnt) || pmnt == '') {
                alert("Incorrect payment amount");
                return false;
            }
            return true;
        }

    </script>


<body  onresize="javascript:changeBackTo(500,450)">
  

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <%--<script src="../Scripts/cBoxes.js" type="text/javascript"></script>--%>

    <script type="text/javascript" language="javascript"></script>
    <form id="Form1" method="post" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <%--<aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />--%>
            </Scripts>
        </aspnew:ScriptManager>
        <section id="main-content-popup" class="addNewLeadPopup">
                <section class="wrapper main-wrapper row" style="">
                     <div class="col-xs-12">
                         <div class="page-title" style="display: none;">                
                              <h1 class="title">Schedule Payment</h1>
                        </div>

                      </div>
                    <div class="clearfix"></div>
                     <div class="col-xs-12">
                         <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Schedule Payment</h2>

                            <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                      </header>
                        <div class="content-body">
                           <div class="row form-inline">
                           <div class="col-md-3">
                                    <label class="form-label" style="width:148px;">Schedule Upto </label>
                                       $<asp:Label ID="lbl_owes" runat="server"  ForeColor="Black"></asp:Label>
                                                              
                                </div>
                           <div class="well-sm"></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-label" style="width:135px;">Schedule Amount($)</label>
                                    <span class="desc"></span>
                                   
                                        <asp:TextBox ID="txt_amount" runat="server" CssClass="form-control inline-textbox" Width="174px" MaxLength="5"></asp:TextBox>

                                </div>
                            </div>
                            <div class="well-sm"></div>
                            <div class="col-md-3 col-sm-6 col-xs-6" style="display:-webkit-inline-box;" >
                                    <label class="form-label" style="width:155px;">Payment Date</label>
                                    <picker:datepicker id="date_payment_new" runat="server"  Width="90px" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
    
                                    <%--<span class="desc"></span>
                                        <ew:CalendarPopup ID="date_payment" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="-80" ToolTip="Payment Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
                                ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Nullable="True">
                                <TextboxLabelStyle Width="250px" CssClass="form-control inline-textbox"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>--%>
                                                              
                            </div>
                             </div>
                            
                            
                            <hr />
                            <div class="clearfix"></div>
                            <div class="row">
                            <div class="col-md-12">
                            <div class="pull-right">
                                <asp:Button ID="btn_submit" runat="server" CssClass="btn btn-primary" Text="Save"  OnClientClick="return ValidiatePayment();"></asp:Button>
                            <%--<asp:Button ID="btn_submit" runat="server" class="btn btn-primary" Text="Submit" OnClientClick="return ValidateDepuyContactUs();"
                                        OnClick="btn_submit_Click"  />
                                    <asp:Button ID="btn_Clear" runat="server" class="btn btn-primary" Text="Clear" 
                                        OnClick="btn_Clear_Click" />--%>
                                    <asp:Button ID="btn_Close" runat="server" class="btn btn-primary"
                                        TabIndex="9" Text="Close"  />
                           </div>             
                          </div>         
                          </div>
                          
                          
                          </div>
                                
                                
                </section>
                 </section>
            </section>

    </form>









    <%--    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" cellspacing="0" cellpadding="0" width="350" align="center"
        border="0">
        <tr>
            <td background="../../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <table class="clsLeftPaddingTable" id="tblsub" height="20" cellspacing="0" cellpadding="0"
                    width="350" align="center" border="1" bordercolor="#ffffff">
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            <strong>Schedule Payment</strong>
                        </td>
                        <td valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                        </td>
                        <td valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            Schedule Upto
                        </td>
                        <td valign="top">
                            $
                            <asp:Label ID="lbl_owes" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            Schedule Amount($)
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="txt_amount" runat="server" CssClass="clsinputadministration" Width="48px"
                                MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" valign="top">
                            Payment&nbsp;Date
                        </td>
                        <td valign="top">
                            <ew:CalendarPopup ID="date_payment" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="-80" ToolTip="Payment Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
                                ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Nullable="True">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                        <td align="right">
                            <asp:Button ID="btn_submit" runat="server" CssClass="clsbutton" Text="Save"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
            <tr>
                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                </td>
            </tr>
            <tr>
                <td background="../../images/separator_repeat.gif" colspan="2" height="11">
                </td>
            </tr>
            <tr>
                <td style="visibility: hidden">
                    <asp:Label ID="lbl_activeflag" runat="server"></asp:Label>
                </td>
            </tr>
    </table>
    </form>--%>
    <asp:Label ID="lbl_activeflag" runat="server" Visible="false"></asp:Label>
     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <script language="javascript" type="text/javascript">

        var time = new Date();
        var date = time.getDate();
        var year = time.getYear();
        if (year < 2000)    // Y2K Fix, Isaac Powell
            year = year + 1900; // http://onyx.idbsu.edu/~ipowell
        //var d1 = new dateObj(document.Form1.dDay,document.Form1.dMonth, document.Form1.dYear);
        //var d1 = new dateObj(document.Form1.dMonth, document.Form1.dDay, document.Form1.dYear);
        //initDates(2002, year, '--', '--', '--', d1);

        //var d2 = new dateObj(document.Form1.ToMonth, document.Form1.ToDay, document.Form1.ToYear);
        //initDates(2002, year, '--', '--', '--', d2);

        //ShowHidePageNavigation();

        //document.getElementById("Navigation1_CallBackDate_div").style.zIndex = 100003;
        //document.getElementById("Navigation1_CallBackDate_monthYear").style.zIndex = 100004;

    </script>
</body>
</html>

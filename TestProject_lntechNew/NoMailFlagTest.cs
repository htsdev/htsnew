﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using lntechNew.Components;
namespace TestProject_lntechNew
{
    /// <summary>
    ///This is a test class for lntechNew.Components.NoMailFlag and is intended
    ///to contain all lntechNew.Components.NoMailFlag Unit Tests
    ///</summary>
    [TestClass()]
    public class NoMailFlagTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion




        /// <summary>
        ///A test for UpdateByTrafficDetails ()
        ///</summary>
        [TestMethod()]
        public void UpdateByTrafficDetailsTest()
        {
            /* Test cases for UpdateByTrafficDetails
             * UpdateByTrafficDetail() returns an int value, and that value indicates the result
             * The following are the values & their descriptions 
             * 0 = No such record exists
             * 1 = Record Updated
             * 2 = Address could not be verified
             * 3 = Record already exists
             */


            #region Test 1:Trying to update do not mail flag when the record does'nt exist
            string FName = "noname"; 
            string LName = "noname"; 
            string Addr = "noaddress"; 
            string Cty = "nocity"; 
            int State_ID = 0; 
            string StateName = "--"; 
            string Zip = "32424"; 

            NoMailFlag target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            
            int expected = 0;
            int actual;
            actual = target.UpdateByTrafficDetails();

            Assert.AreEqual(expected, actual, "The result should have been 0");
            #endregion

            #region Test 2:Trying to update do not mail flag when record exists
            FName = "BRYANT";
            LName = "SCOGIN";
            Addr = "7035 LEADER";
            Cty = "HOUSTON"; 
            State_ID = 45;
            StateName = "TX";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            actual = target.UpdateByTrafficDetails();
            expected = 1;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " +expected.ToString());

            #endregion

            #region Test 3(a):Trying to update do not mail flag when Firstname is invalid
            FName = "asdasd";
            LName = "SCOGIN";
            Addr = "7035 LEADER";
            Cty = "HOUSTON";
            State_ID = 45;
            StateName = "TX";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 0;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());

            #endregion

            #region Test 3(b):Trying to update do not mail flag when Lastname is invalid
            FName = "BRYANT";
            LName = "dsfsdf";
            Addr = "7035 LEADER";
            Cty = "HOUSTON";
            State_ID = 45;
            StateName = "TX";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 0;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());

            #endregion

            #region Test 3(c):Trying to update do not mail flag when Address is invalid
            FName = "BRYANT";
            LName = "SCOGIN";
            Addr = "dsfsfsfd";
            Cty = "HOUSTON";
            State_ID = 45;
            StateName = "TX";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 0;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());

            #endregion

            #region Test 3(d):Trying to update do not mail flag when City is invalid
            FName = "BRYANT";
            LName = "SCOGIN";
            Addr = "7035 LEADER";
            Cty = "asdasd";
            State_ID = 45;
            StateName = "TX";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 0;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());


            #endregion

            #region Test 3(e):Trying to update do not mail flag when State is invalid
            FName = "BRYANT";
            LName = "SCOGIN";
            Addr = "7035 LEADER";
            Cty = "asdasd";
            State_ID = 52;
            StateName = "WY";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 0;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());


            #endregion

            #region Test 3(f):Trying to update do not mail flag when ZipCode is invalid
            FName = "BRYANT";
            LName = "SCOGIN";
            Addr = "7035 LEADER";
            Cty = "asdasd";
            State_ID = 45;
            StateName = "TX";
            Zip = "77074";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 0;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());


            #endregion

            #region Test 4:Trying to insert a record that is already in DoNotMailFlag list

            FName = "GEORGIA";
            LName = "BANKS";
            Addr = "8150 SAINT LO RD";
            Cty = "HOUSTON";
            State_ID = 45;
            StateName = "TX";
            Zip = "77033";

            target = new NoMailFlag(FName, LName, Addr, Cty, State_ID, StateName, Zip);
            expected = 3;
            actual = target.UpdateByTrafficDetails();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());

            #endregion
        }

        [TestMethod()]
        public void UpdateByTrafficMailerIDTest()
        {
            /* Test cases for UpdateByTrafficLetterID
            * UpdateByTrafficLetterID() returns an int value, and that value indicates the result
            * The following are the values & their descriptions 
            * 0 = No such record exists
            * 1 = Record Updated
            * 2 = Address could not be verified
            * 3 = Record already exists
            */

            #region Test 1: Updating by LetterID when LetterID does not exist
            int LetterID = -1;
            NoMailFlag target = new NoMailFlag(LetterID);
            int expected = 0;
            int actual;
            actual = target.UpdateByTrafficLetterID();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());
            #endregion

            #region Test 2: Updating by LetterID that exists
            LetterID = 2380405;
            target = new NoMailFlag(LetterID);
            expected = 1;
            actual = target.UpdateByTrafficLetterID();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());
            #endregion

            #region Test 3: Updating a record that is already in Do Not Mail list
            LetterID = 2380405;
            target = new NoMailFlag(LetterID);
            expected = 3;
            actual = target.UpdateByTrafficLetterID();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());
            #endregion


        }

        [TestMethod()]
        public void UpdateByTicketNumberTest()
        {
            /* Test cases for UpdateByTicketNumber
            * UpdateByTicketNumber() returns an int value, and that value indicates the result
            * The following are the values & their descriptions 
            * 0 = No such record exists
            * 1 = Record Updated
            * 2 = Address could not be verified
            * 3 = Record already exists
            */


            #region Test 1: Updating by TicketNumber which does'nt exits
            string TicketNumber = "ererr";
            NoMailFlag target = new NoMailFlag(TicketNumber);
            int expected = 0;
            int actual = target.UpdateByTicketNumber();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());
            #endregion

            #region Test 2: Updating by TicketNumber that exists
            TicketNumber = "M004519231";
            target = new NoMailFlag(TicketNumber);
            expected = 1;
            actual = target.UpdateByTicketNumber();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());
            #endregion

            #region Test 3: Updating by TicketNumber a record that is already in Do Not Mail list
            TicketNumber = "M004401484";
            target = new NoMailFlag(TicketNumber);
            expected = 3;
            actual = target.UpdateByTicketNumber();
            Assert.AreEqual(expected, actual, "The result should have been " + expected.ToString());
            #endregion
        }

  
    }


}

﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;
using System;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for AutoDialerTest and is intended
    ///to contain all AutoDialerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AutoDialerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SettingDateRange
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void SettingDateRangeTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            target.SettingDateRange = expected;
            actual = target.SettingDateRange;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EventTypeName
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EventTypeNameTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.EventTypeName = expected;
            actual = target.EventTypeName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EventTypeID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EventTypeIDTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            target.EventTypeID = expected;
            actual = target.EventTypeID;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EventState
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EventStateTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.EventState = expected;
            actual = target.EventState;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EmpID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EmpIDTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.EmpID = expected;
            actual = target.EmpID;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DialTime
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void DialTimeTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DateTime expected = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime actual;
            target.DialTime = expected;
            actual = target.DialTime;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CourtID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void CourtIDTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.CourtID = expected;
            actual = target.CourtID;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CallDays
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void CallDaysTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            target.CallDays = expected;
            actual = target.CallDays;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UpdateEventConfigSettingsByID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void UpdateEventConfigSettingsByIDTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            target.UpdateEventConfigSettingsByID();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for UpdateAllEventState
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void UpdateAllEventStateTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            target.UpdateAllEventState();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetEventConfigSettingsHistory
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void GetEventConfigSettingsHistoryTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetEventConfigSettingsHistory();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetEventConfigSettingsByID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void GetEventConfigSettingsByIDTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetEventConfigSettingsByID();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AutoDialer Constructor
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void AutoDialerConstructorTest()
        {
            AutoDialer target = new AutoDialer();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for GetEventConfigSettings
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void GetEventConfigSettingsTest()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetEventConfigSettings();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SettingDateRange
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void SettingDateRangeTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            target.SettingDateRange = expected;
            actual = target.SettingDateRange;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EventTypeName
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EventTypeNameTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.EventTypeName = expected;
            actual = target.EventTypeName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EventTypeID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EventTypeIDTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            target.EventTypeID = expected;
            actual = target.EventTypeID;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EventState
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EventStateTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.EventState = expected;
            actual = target.EventState;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EmpID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void EmpIDTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.EmpID = expected;
            actual = target.EmpID;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DialTime
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void DialTimeTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DateTime expected = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime actual;
            target.DialTime = expected;
            actual = target.DialTime;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CourtID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void CourtIDTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.CourtID = expected;
            actual = target.CourtID;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CallDays
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void CallDaysTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            target.CallDays = expected;
            actual = target.CallDays;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UpdateEventConfigSettingsByID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void UpdateEventConfigSettingsByIDTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            target.UpdateEventConfigSettingsByID();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for UpdateAllEventState
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void UpdateAllEventStateTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            target.UpdateAllEventState();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetEventConfigSettingsHistory
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void GetEventConfigSettingsHistoryTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetEventConfigSettingsHistory();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetEventConfigSettingsByID
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void GetEventConfigSettingsByIDTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetEventConfigSettingsByID();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetEventConfigSettings
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void GetEventConfigSettingsTest1()
        {
            AutoDialer target = new AutoDialer(); // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetEventConfigSettings();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AutoDialer Constructor
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Ozair\\Ozair's Team Projects\\HoustonTraffic", "/")]
        [UrlToTest("http://localhost:17617/")]
        public void AutoDialerConstructorTest1()
        {
            AutoDialer target = new AutoDialer();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}

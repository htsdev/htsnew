﻿using lntechNew.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for MailClassTest and is intended
    ///to contain all MailClassTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MailClassTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SendMailWithtAttchment
        ///</summary>
        [TestMethod()]
       
        public void SendMailWithtAttchmentTest()
        {
            string From = string.Empty; // TODO: Initialize to an appropriate value
            string To = string.Empty; // TODO: Initialize to an appropriate value
            string Subject = string.Empty; // TODO: Initialize to an appropriate value
            string Body = string.Empty; // TODO: Initialize to an appropriate value
            string attachment = string.Empty; // TODO: Initialize to an appropriate value
            MailClass.SendMailWithtAttchment(From, To, Subject, Body, attachment);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}

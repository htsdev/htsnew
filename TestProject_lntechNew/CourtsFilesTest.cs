﻿using HTP.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for CourtsFilesTest and is intended
    ///to contain all CourtsFilesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CourtsFilesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DeleteAllCourtFiles
        ///</summary>
        [TestMethod()]
    
        public void DeleteAllCourtFilesTest()
        {
            CourtsFiles target = new CourtsFiles();
            
            //Test for passing courtid whose file has been uploaded 
            
            int count=0;
            target.CourtId = 3048; // TODO: Initialize courtid whose file has already been uploaded
            count=target.DeleteAllCourtFiles();
            Assert.IsTrue(count>0);

            //Test for passing courtid whose file has not been uploaded at current time 

            count = 0;
            target.CourtId=3048; //TODO: Initialize courtid whose file has not uploaded at current time
            count=target.DeleteAllCourtFiles ();
            Assert.AreEqual (0,count);
        }

        /// <summary>
        ///A test for DeleteFile
        ///</summary>
        [TestMethod()]
                       
        public void DeleteFileTest()
        {
            CourtsFiles target = new CourtsFiles(); // TODO: Initialize to an appropriate value
            
            int id = 1; // TODO: Initialize id to id field of CourtFiles table
            int count=0;
            
            //Test for already exisitng id in CoutFiles table

            count=target.DeleteFile(id);
            Assert.IsTrue(count>0);


            //Test for id not exising in database
            count = 0;
            id = -1; //TODO: Initialize id to a value not exist in CourtFiles table  
            count=target.DeleteFile(id);
            Assert.AreEqual(0,count); 
        }

        /// <summary>
        ///A test for GetUploadedFiles
        ///</summary>
        [TestMethod()]
       
        public void GetUploadedFilesTest()
        {
            CourtsFiles target = new CourtsFiles(); 
            DataTable actual;

            //Test for passing a courtid whose file has been uploaded at curren time     

            target.CourtId = 3048; //TODO: Initialize to Appropriate value
            actual = target.GetUploadedFiles();
            Assert.IsTrue(actual .Rows.Count>0);

            //Test for passing a courtid whose file has not been uploaded at curent time
            
            target.CourtId = 7; //Initialize to court id whose file has not been uploaded at current time  
            actual = target.GetUploadedFiles();
            Assert.AreEqual(0,actual.Rows .Count);
        }

        /// <summary>
        ///A test for ViewFilepath
        ///</summary>
        [TestMethod()]

        public void ViewFilepathTest()
        {
            CourtsFiles target = new CourtsFiles(); 
            
            int id = 4; // TODO: Initialize id to id field of CourtFiles table
            string expected = "\\\\SRV-WEB\\DOCSTORAGE\\CourtFilesPath\\a-configurable-rt-os.pdf"; //Here assign the path against the id field 
            string actual;
            
            //Test for passing a valid file id exist in CourtFile Table
            
            actual = target.ViewFilepath(id);
            Assert.AreEqual(expected, actual);

            //Test for passing a file id not currently exist in CourtFile Table 
            id = -1;
            expected = null;
            actual = target.ViewFilepath(id);
            Assert.AreEqual(expected ,actual);
        }
    }
}

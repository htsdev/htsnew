﻿using lntechNew.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for faxclassTest and is intended
    ///to contain all faxclassTest Unit Tests
    ///</summary>
    [TestClass()]
    public class faxclassTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetJpAppearancereport
        ///</summary>
        [TestMethod()]        
        public void GetJpAppearancereportTest()
        {
            faxclass target = new faxclass(); 
            int expected = 0; 
            DataTable actual;
            actual = target.GetJpAppearancereport();
            if (actual.Rows.Count == 0)
            {
                Assert.AreEqual(expected, actual.Rows.Count);
            }
            else
                Assert.AreNotEqual(expected, actual.Rows.Count);
            
        }

        /// <summary>
        ///A test for GetFaxReport
        ///</summary>
        [TestMethod()]      
        public void GetFaxReportTest()
        {
            faxclass target = new faxclass();
            int status = -1; 
            DateTime fromdate = DateTime.Now;
            DateTime todate = DateTime.Now;
            int expected = 0;
            DataTable actual;
            actual = target.GetFaxReport(status, fromdate, todate);
            if (actual.Rows.Count == 0)
            {
                Assert.AreEqual(expected, actual.Rows.Count);
            }
            else
                Assert.AreNotEqual(expected, actual.Rows.Count);
            
        }

        
        

        /// <summary>
        ///A test for addlfaxletter
        ///</summary>
        [TestMethod()]       
        public void addlfaxletterTest()
        {
            faxclass target = new faxclass(); 
            int ticketid = 0; 
            int empid = 0;
            string ename = "Noufil";
            string eemail = "noufil@lntech.com";
            string faxno = "123-3221";
            string subject = "dsad";
            string body = "dasdassadadas";
            string status = "status"; 
            DateTime faxdate = DateTime.Now; 
            string docpath = "c:/path"; 
            bool expected = true; 
            bool actual;
            actual = false;//target.addlfaxletter(ticketid, empid, ename, eemail, faxno, subject, body, status, faxdate, docpath);
            Assert.AreEqual(expected, actual);
           
        }

        /// <summary>
        ///A test for getfaxcount
        ///</summary>
        [TestMethod()]
        
        public void getfaxcountTest()
        {
            faxclass target = new faxclass();
            int ticketid = 8723; 
            string expected = string.Empty;
            string actual;
            actual = target.getfaxcount(ticketid);
            Assert.AreNotEqual(expected, actual);
           
        }
    }
}

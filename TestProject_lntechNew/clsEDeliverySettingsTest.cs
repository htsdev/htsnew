﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for clsEDeliverySettingsTest and is intended
    ///to contain all clsEDeliverySettingsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class clsEDeliverySettingsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for getdelconfirmation
        ///</summary>
        [TestMethod()]
     
        public void getdelconfirmationTest()
        {
            clsEDeliverySettings target = new clsEDeliverySettings(); // TODO: Initialize to an appropriate value
            int status = 0; 
            int  expected = 0;
            
            DataTable actual;
            actual = target.getdelconfirmation(status);
            if (actual.Rows.Count != 0) 
            {
                Assert.AreNotEqual(expected, actual.Rows.Count);       
            }
            else
            {
                Assert.AreEqual(expected, actual.Rows.Count);       
            }

             status = 1;
             expected = 0;

           // DataTable actual;
            actual = target.getdelconfirmation(status);
            if (actual.Rows.Count != 0)
            {
                Assert.AreNotEqual(expected, actual.Rows.Count);
            }
            else
            {
                Assert.AreEqual(expected, actual.Rows.Count);
            }

            status = -1;
            expected = 0;

            // DataTable actual;
            actual = target.getdelconfirmation(status);
            if (actual.Rows.Count != 0)
            {
                Assert.AreNotEqual(expected, actual.Rows.Count);
            }
            else
            {
                Assert.AreEqual(expected, actual.Rows.Count);
            }
        }
    }
}

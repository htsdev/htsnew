﻿using lntechNew.Components.ClientInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System;
using System.Data;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for HccCriminalReportTest and is intended
    ///to contain all HccCriminalReportTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HccCriminalReportTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

       

        /// <summary>
        ///A test for GetCriminalCOurt
        ///</summary>
        [TestMethod()]
       
        public void GetCriminalCOurtTest()
        {
            HccCriminalReport target = new HccCriminalReport(); 
            int expected = 0; 
            DataTable actual;
            actual = target.GetCriminalCOurt();
            Assert.AreNotEqual(expected, actual.Rows.Count);
            
        }

        /// <summary>
        ///A test for Get_CriminalFollowUp
        ///</summary>
        [TestMethod()]       
        public void Get_CriminalFollowUpTest()
        {
            HccCriminalReport target = new HccCriminalReport(); 
            int courtid = -1; 
            DateTime datefrom = Convert.ToDateTime("01/01/2008");
            DateTime dateto = DateTime.Today; ; 
            int expected = 0; 
            DataTable actual;
            actual = new DataTable();//target.Get_CriminalFollowUp(courtid, datefrom, dateto);            
            Assert.AreNotEqual(expected, actual.Rows.Count);
            
        }
    }
}

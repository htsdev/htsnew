﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data;

namespace TestProject_lntechNew
{
    
    
    /// <summary>
    ///This is a test class for BussinessLogicTest and is intended
    ///to contain all BussinessLogicTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BussinessLogicTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetUploadedDocument
        ///</summary>
        [TestMethod()]
        public void GetUploadedDocumentTest()
        {
            string id = "67";
            BussinessLogic target = new BussinessLogic();
            DataTable expected = null;
            DataTable actual;
            actual = target.GetUploadedDocument(id);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for UploadNewDocument
        ///</summary>
        [TestMethod()]
        public void UploadNewDocumentTest()
        {
            BussinessLogic target = new BussinessLogic(); // TODO: Initialize to an appropriate value
            target.UploadNewDocument();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}

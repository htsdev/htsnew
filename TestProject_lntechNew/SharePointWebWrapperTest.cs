﻿using HTP.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Xml;
using System;

namespace TestProject_lntechNew
{


    /// <summary>
    ///This is a test class for SharePointWebWrapperTest and is intended
    ///to contain all SharePointWebWrapperTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SharePointWebWrapperTest
    {
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }  
      
        [TestMethod()]
        public void AddNewViolationToCalendarTest_1()
        {
            int TicketsViolationID = 1381337;
            int expected = 1; 
            int actual;
            actual = SharePointWebWrapper.AddNewViolationToCalendar(TicketsViolationID);
            Assert.AreEqual(expected, actual);
        }

        public void AddNewViolationToCalendarTest_2()
        {
            int TicketsViolationID = 1381337;
            int expected = 1;
            int actual;
            actual = SharePointWebWrapper.AddNewViolationToCalendar(TicketsViolationID);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddViolationsToCalendarTest()
        {
            int ticketId = 196108;
            int expected = 9;
            int actual;
            actual = SharePointWebWrapper.AddViolationsToCalendar(ticketId);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void UpdateAllViolationsInCalendarTest()
        {
            int pTicketId = 196108; // TODO: Initialize to an appropriate value
            int expected = 9; // TODO: Initialize to an appropriate value
            int actual;
            actual = SharePointWebWrapper.UpdateAllViolationsInCalendar(pTicketId);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void UpdateSingleViolationInCalendarTest()
        {
            int pTicketsViolationID = 1381337; // TODO: Initialize to an appropriate value
            int expected = 1; // TODO: Initialize to an appropriate value
            int actual;
            actual = SharePointWebWrapper.UpdateSingleViolationInCalendar(pTicketsViolationID);
            Assert.AreEqual(expected, actual);
        }        
    }
}

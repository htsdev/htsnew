using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew
{
	/// <summary>
	/// Summary description for Bugtracker.
	/// </summary>
	public partial class Bugtracker : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dg_bug;
		protected System.Web.UI.WebControls.DropDownList cmbPageNo;
		protected System.Web.UI.WebControls.Label lblGoto;
		protected System.Web.UI.WebControls.Label lblPNo;
		protected System.Web.UI.WebControls.Label lblCurrPage;
		protected System.Web.UI.WebControls.Label lblMessage;
		clsENationWebComponents clsdb = new clsENationWebComponents();
		clsLogger bugTracker = new clsLogger();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{				
				if(!IsPostBack)
					FillGrid();			
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		private void FillGrid()
		{
			try
			{
				dg_bug.DataSource=clsdb.Get_DS_BySP("usp_hts_get_errorlog");
				dg_bug.DataBind();
				FillPageList();
				SetNavigation();
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		#region Navigation logic
		//----------------------------------Navigational LOGIC-----------------------------------------//
		
		// Procedure for filling page numbers in page number combo........
		private void FillPageList()
		{
			Int16 idx;

			cmbPageNo.Items.Clear();
			for (idx=1; idx<=dg_bug.PageCount;idx++)
			{
				cmbPageNo.Items.Add ("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value=idx.ToString();
			}
		}	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(dg_bug.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;

				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();

				cmbPageNo.SelectedIndex =dg_bug.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message ;
				bugTracker.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}
		//Event fired When page changed from drop down list
		private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{				
				dg_bug.CurrentPageIndex= cmbPageNo.SelectedIndex ;						
				FillGrid();								
			}
			catch (Exception ex)
			{
				lblMessage.Text=ex.Message;
			}		
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_bug.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_bug_PageIndexChanged);
			this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dg_bug_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg_bug.CurrentPageIndex=e.NewPageIndex;		
			FillGrid();
		}
	}
}

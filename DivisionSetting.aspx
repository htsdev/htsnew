﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DivisionSetting.aspx.cs"
    Inherits="HTP.DivisionSetting" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Division Setting</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable">
                    <table style="width: 100%">
                        <tr>
                            <td align="center" valign="middle">
                                <asp:Label ID="lblDivision" runat="server" CssClass="clssubhead" Text="Division :"></asp:Label>
                                &nbsp;
                                <asp:TextBox ID="txtDivision" runat="server" CssClass="clsInputadministration" Width="200px"></asp:TextBox>&nbsp;
                                <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" CssClass="clsLeftPaddingTable" />
                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                    Text="Add" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td>
                    <table width="819px" height="34px" background="../../Images/subhead_bg.gif" class="clssubhead">
                        <tr>
                            <td align="left" style="width=50%" class="clssubhead">
                                &nbsp;Division &nbsp;&nbsp;
                            </td>
                            <td align="right" style="width=50%" class="clssubhead">
                                <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New Record</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnTrial" runat="server" Text="Button" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 819px">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:GridView ID="gvDivision" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        CellPadding="3" CellSpacing="0" OnRowCommand="gvDivision_RowCommand" OnRowDataBound="gvDivision_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblSno" runat="server" CssClass="clssubhead" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Division Type">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnValue" runat="server" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'
                                        CommandName="lnkbutton"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Active">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" CssClass="clsLabel" Text='<%# (Convert.ToInt32(Eval("IsActive")) == 0) ? "No" : "Yes" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/remove2.gif" CommandName="ImgDelete" />
                                    <asp:HiddenField ID="hfValueId" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:HiddenField ID="hfcases" runat="server" Value='<%# Eval("AssociatedCase") %>' />
                                    <asp:HiddenField ID="hfIsactive" runat="server" Value='<%# Eval("IsActive") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPending.aspx.cs" Inherits="HTP.WebScanII.NewPending" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pending</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    
    function ShowMorePopup() 
		{
	      document.getElementById("PopupPanel").style.display = 'block';
          document.getElementById("PopupPanel").style.top = 355;			
	      
	      document.getElementById("DisableDive").style.height = document.body.offsetHeight * 2 ;
		  document.getElementById("DisableDive").style.width = document.body.offsetWidth;
		  document.getElementById("DisableDive").style.display = 'block';
		  document.getElementById("txt_MoreCause").value="";
		  document.getElementById("txt_MoreCause").focus();  
		  return false;
		}
		
		function CloseMorePopUp(w)
		{
		  document.getElementById("PopupPanel").style.display = 'none';
		  document.getElementById("DisableDive").style.display = 'none';
		  if(w=="0")
		  {
		    document.getElementById("txt_MoreCause").value="";
		    document.getElementById("ddlstatus").focus();
		  }
		  else if(w == "1")
		  {
		    var causeno=document.getElementById("txtCauseNo");
		    if(causeno.value.lenght==0 || causeno.value=="")
		    {
		        causeno.value=document.getElementById("txt_MoreCause").value;	
		    }
		    else
		    {
		        causeno.value=causeno.value+","+document.getElementById("txt_MoreCause").value;		
		    }
		    document.getElementById("ddlstatus").focus();
		  }
		  return false;
		}
	    //ozair 4400 07/12/2008
	    function ShowHCJP51Popup() 
		{
	      document.getElementById("PopupPanel_HCJP51").style.display = 'block';
          document.getElementById("PopupPanel_HCJP51").style.top = 355;			
	      
	      document.getElementById("DisableDive").style.height = document.body.offsetHeight * 2 ;
		  document.getElementById("DisableDive").style.width = document.body.offsetWidth;
		  document.getElementById("DisableDive").style.display = 'block';
		  document.getElementById("hf_isUpdateable").value="0";		  
		  return false;
		}
		
	    function CloseHCJP51PopUp(w)
		{
		  document.getElementById("PopupPanel_HCJP51").style.display = 'none';
		  document.getElementById("DisableDive").style.display = 'none';
		  if(w=="0")
		  {
		    document.getElementById("hf_isUpdateable").value="0";
		    document.getElementById("ddlstatus").focus();
		    return false;
		  }
		  else if(w == "1")
		  {
		     document.getElementById("hf_isUpdateable").value="1";
		     return true;;
		  }		  
		}			
        //end ozair 4400
    function openW()
    {
        window.open("previewpdf.aspx");
        return false;
    }
    
     function Validation()
     { 
       //Farrukh 9451 07/04/2011 replaced month textbox with dropdown 
       var txtcauseno=document.getElementById("txtCauseNo").value;
       var ddlmm=document.getElementById("ddlmm").value;
       var txtdd=document.getElementById("txtdd").value;
       var txtyy=document.getElementById("txtyy").value;
       var ddlStatus=document.getElementById("ddlstatus").value;
       var ddltime=document.getElementById("ddl_Time").value;
      
       
       if(txtcauseno == "")
       { 
        alert("PLease Enter Cause Number");
        document.getElementById("txtCauseNo").focus();
        return false;
       } 
       if(ddlStatus=="0")
       {
        alert("Please Select Status");
        document.getElementById("ddlstatus").focus();
        return false;
       }
        if(ddlStatus!="104")
        {
            if(ddlmm!="" && txtdd!="" && txtyy!="")
			{
				var dob=ddlmm+"/"+txtdd+"/"+txtyy				
				if (!MMDDYYYYDate(dob))
				{	
				
				document.getElementById("ddlmm").focus(); 
				return(false);
				}			   
			}
			else
			{
			    alert("Please Enter CourtDate");
			   document.getElementById("ddlmm").focus(); 
			   return(false);
			}
	    }
	    
	     document.getElementById("hf_height").value=  document.body.offsetHeight * 2 ;
         document.getElementById("hf_width").value= document.body.offsetWidth;
     }
     
     function DeleteConfirm()
    {
        var isDelete=confirm("Are You Sure You Want To Delete This Record");
        if(isDelete== true)
        {
           
        }
        
        else
        {
            return false;
        }
    }
    
    function checkStatus()
    {
        //Farrukh 9451 07/04/2011 replaced month textbox with dropdown
        var Status=document.getElementById("ddlstatus").value;
        var mm=document.getElementById("ddlmm");
        var dd=document.getElementById("txtdd");
        var yy=document.getElementById("txtyy");       
        var time=document.getElementById("ddl_Time");
        
        if(Status=="104")
        {
            mm.disabled=true;
            dd.disabled=true;
            yy.disabled=true;
            time.disabled=true;
        }
        else
        {
            mm.disabled=false;
            dd.disabled=false;
            yy.disabled=false;
            time.disabled=false;
        }
    }
     
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div runat="server" id="DisableDive" style="display: none; position: absolute; left: 1;
                top: 1; height: 1px; background-color: Silver; filter: alpha(opacity=50)">
                <table width="100%" height="100%">
                    <tr>
                        <td style="width: 100%; height: 100%">
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
            <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
        </Triggers>
    </aspnew:UpdatePanel>
    <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="850">
        <tr>
            <td colspan="2">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="clssubhead" height="34" background="../Images/subhead_bg.gif">
                            &nbsp;Pending
                        </td>
                        <td align="right" class="clssubhead" height="34" style="font-weight: bold; font-size: 9pt;"
                            background="../Images/subhead_bg.gif">
                            &nbsp;<asp:HyperLink ID="search" runat="server" NavigateUrl="~/WebScanII/NewSearch.aspx">Search</asp:HyperLink>
                            <asp:Label ID="lblsp" runat="server" Text="|"></asp:Label>
                            <asp:HyperLink ID="scan" runat="server" NavigateUrl="~/WebScanII/Default.aspx">Scan</asp:HyperLink>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="clsLeftPaddingTable" colspan="2">
            </td>
        </tr>
        <tr>
            <td align="center" class="clsLeftPaddingTable" colspan="2">
                <aspnew:UpdatePanel ID="panel5" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />                        
                        <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
                        <aspnew:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                        <aspnew:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                        <aspnew:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                        <aspnew:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                        <aspnew:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                    </Triggers>
                </aspnew:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <table id="tbl1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="100%">
                            <aspnew:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gv_pending" runat="server" AutoGenerateColumns="False" PageSize="15"
                                        Width="100%" OnRowDataBound="gv_pending_RowDataBound">
                                        <AlternatingRowStyle BackColor="#EEEEEE" />
                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Batch" SortExpression="batchid">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblbatchid" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Count" SortExpression="imagecount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCount" runat="server" CssClass="label" Text='<%# bind("Count") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblScanType" runat="server" CssClass="label" Text='<%# bind("scantype") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" CssClass="label" Text='<%# bind("ScanDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Verified">
                                                <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="verified" runat="server" ForeColor="RoyalBlue" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Problem" SortExpression="NoCauseCount">
                                                <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="nocause" ForeColor="RoyalBlue" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Queued">
                                                <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="queued" runat="server" ForeColor="RoyalBlue" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discrepancy">
                                                <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="discrepancy" runat="server" ForeColor="RoyalBlue" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pending">
                                                <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPending" runat="server" Text='<%# bind("PendingCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" style="height: 11px">
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <aspnew:UpdatePanel ID="panel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="clsLeftPaddingTable" width="12%">
                                                Scan Type
                                            </td>
                                            <td width="12%" class="clsLeftPaddingTable">
                                                Crt Loc
                                            </td>
                                            <td width="18%" class="clsLeftPaddingTable">
                                                Cause Number
                                            </td>
                                            <td width="13%" class="clsLeftPaddingTable">
                                                Status
                                            </td>
                                            <td width="20%" class="clsLeftPaddingTable">
                                                Crt Date
                                            </td>
                                            <td width="10%" class="clsLeftPaddingTable">
                                                Crt Time
                                            </td>
                                            <td class="clsLeftPaddingTable">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 2%">
                                            <td colspan="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="clsLeftPaddingTable">
                                                <asp:Label ID="lbl_ScanType" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td class="clsLeftPaddingTable">
                                                <asp:Label ID="lbl_CrtLoc" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td class="clsLeftPaddingTable">
                                                <asp:TextBox ID="txtCauseNo" runat="server" CssClass="clstextarea"></asp:TextBox>&nbsp;&nbsp;<asp:LinkButton
                                                    ID="lnkbtn_More" runat="server" OnClientClick="return ShowMorePopup();">More</asp:LinkButton>
                                            </td>
                                            <td class="clsLeftPaddingTable">
                                                <asp:DropDownList ID="ddlstatus" runat="server" CssClass="clsinputcombo" OnChange="checkStatus();">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="47">JURY TRIAL</asp:ListItem>
                                                    <asp:ListItem Value="101">PRETRIAL</asp:ListItem>
                                                    <asp:ListItem Value="103">JUDGE</asp:ListItem>
                                                    <asp:ListItem Value="3">ARRAIGNMENT</asp:ListItem>
                                                    <asp:ListItem Value="104">WAITING</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                           <td class="clsLeftPaddingTable" style="width: 220px">
                                                <asp:DropDownList ID="ddlmm" Width="65px" runat="server" CssClass="clsInputCombo">
                                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Jan(01)" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="Feb(02)" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="Mar(03)" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="Apr(04)" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="May(05)" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="Jun(06)" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="Jul(07)" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="Aug(08)" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="Sep(09)" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="Oct(10)" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="Nov(11)" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="Dec(12)" Value="12"></asp:ListItem>
                                                    </asp:DropDownList>/
                                                <asp:TextBox ID="txtdd" runat="server" Width="25px" CssClass="clstextarea" MaxLength="2"
                                                    onkeyup="return autoTab(this, 2, event)"></asp:TextBox>/
                                                <asp:TextBox ID="txtyy" runat="server" Width="33px" CssClass="clstextarea" MaxLength="4"
                                                    onkeyup="return autoTab(this, 4, event)"></asp:TextBox>
                                            </td>
                                            <td class="clsLeftPaddingTable">
                                                <asp:DropDownList ID="ddl_Time" runat="server" CssClass="clsinputcombo">
                                                    <asp:ListItem Value="7:00 AM">7:00 AM</asp:ListItem>
                                                    <asp:ListItem Value="7:15 AM">7:15 AM</asp:ListItem>
                                                    <asp:ListItem Value="7:30 AM">7:30 AM</asp:ListItem>
                                                    <asp:ListItem Value="7:45 AM">7:45 AM</asp:ListItem>
                                                    <asp:ListItem Value="8:00 AM">8:00 AM</asp:ListItem>
                                                    <asp:ListItem Value="8:15 AM">8:15 AM</asp:ListItem>
                                                    <asp:ListItem Value="8:30 AM">8:30 AM</asp:ListItem>
                                                    <asp:ListItem Value="8:45 AM">8:45 AM</asp:ListItem>
                                                    <asp:ListItem Value="9:00 AM">9:00 AM</asp:ListItem>
                                                    <asp:ListItem Value="9:15 AM">9:15 AM</asp:ListItem>
                                                    <asp:ListItem Value="9:30 AM">9:30 AM</asp:ListItem>
                                                    <asp:ListItem Value="9:45 AM">9:45 AM</asp:ListItem>
                                                    <asp:ListItem Value="10:00 AM">10:00 AM</asp:ListItem>
                                                    <asp:ListItem Value="10:15 AM">10:15 AM</asp:ListItem>
                                                    <asp:ListItem Value="10:30 AM">10:30 AM</asp:ListItem>
                                                    <asp:ListItem Value="10:45 AM">10:45 AM</asp:ListItem>
                                                    <asp:ListItem Value="11:00 AM">11:00 AM</asp:ListItem>
                                                    <asp:ListItem Value="11:15 AM">11:15 AM</asp:ListItem>
                                                    <asp:ListItem Value="11:30 AM">11:30 AM</asp:ListItem>
                                                    <asp:ListItem Value="11:45 AM">11:45 AM</asp:ListItem>
                                                    <asp:ListItem Value="12:00 PM">12:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="12:15 PM">12:15 AM</asp:ListItem>
                                                    <asp:ListItem Value="12:30 PM">12:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="12:45 PM">12:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="1:00 PM">1:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="1:15 PM">1:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="1:30 PM">1:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="1:45 PM">1:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="2:00 PM">2:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="2:15 PM">2:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="2:30 PM">2:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="2:45 PM">2:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="3:00 PM">3:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="3:15 PM">3:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="3:30 PM">3:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="3:45 PM">3:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="4:00 PM">4:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="4:15 PM">4:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="4:30 PM">4:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="4:45 PM">4:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="5:00 PM">5:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="5:15 PM">5:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="5:30 PM">5:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="5:45 PM">5:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="6:00 PM">6:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="6:15 PM">6:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="6:30 PM">6:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="6:45 PM">6:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="7:00 PM">7:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="7:15 PM">7:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="7:30 PM">7:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="7:45 PM">7:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="8:00 PM">8:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="8:15 PM">8:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="8:30 PM">8:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="8:45 PM">8:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="9:00 PM">9:00 PM</asp:ListItem>
                                                    <asp:ListItem Value="9:15 PM">9:15 PM</asp:ListItem>
                                                    <asp:ListItem Value="9:30 PM">9:30 PM</asp:ListItem>
                                                    <asp:ListItem Value="9:45 PM">9:45 PM</asp:ListItem>
                                                    <asp:ListItem Value="10:00 PM">10:00 PM</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="clsLeftPaddingTable">
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" Text="Update" OnClick="btnUpdate_Click" />
                                                <asp:HiddenField ID="hf_isUpdateable" runat="server" />
                                                <asp:HiddenField ID="hf_height" runat="server" />
                                                <asp:HiddenField ID="hf_width" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" style="height: 11px">
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <aspnew:UpdatePanel ID="panel4" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left" width="33%">
                                                <asp:ImageButton ID="ImgMoveFirst" runat="server" ImageUrl="~/Images/MoveFirst.gif"
                                                    ToolTip="Move First" OnClick="ImgMoveFirst_Click" />&nbsp;
                                                <asp:ImageButton ID="ImgMovePrev" runat="server" ImageUrl="~/Images/MovePrevious.gif"
                                                    ToolTip="Previous" OnClick="ImgMovePrev_Click" />
                                            </td>
                                            <td width="33%">
                                                <asp:Label ID="lblpageno" runat="server" CssClass="clslabel"></asp:Label>&nbsp;
                                                of &nbsp;
                                                <asp:Label ID="lblCount" runat="server" CssClass="clslabel"></asp:Label>&nbsp;<asp:DropDownList
                                                    ID="ddlImgsize" runat="server" OnSelectedIndexChanged="ddlImgsize_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="500">500%</asp:ListItem>
                                                    <asp:ListItem Value="400">400%</asp:ListItem>
                                                    <asp:ListItem Value="200">200%</asp:ListItem>
                                                    <asp:ListItem Value="175">175%</asp:ListItem>
                                                    <asp:ListItem Value="150" Selected="True">150%</asp:ListItem>
                                                    <asp:ListItem Value="100">100%</asp:ListItem>
                                                    <asp:ListItem Value="75">75%</asp:ListItem>
                                                    <asp:ListItem Value="50">50%</asp:ListItem>
                                                    <asp:ListItem Value="25">25%</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="8%">
                                                <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" OnClick="img_delete_Click"
                                                    ToolTip="Delete Record" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="img_Print" OnClientClick="return openW();" runat="server" ImageUrl="~/Images/preview.gif" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="img_flip" runat="server" ImageUrl="~/Images/Rotate.bmp" OnClick="img_flip_Click"
                                                    ToolTip="Flip Image" />
                                            </td>
                                            <td align="right" width="33%">
                                                <asp:ImageButton ID="ImgMoveNext" runat="server" ImageUrl="~/Images/MoveNext.gif"
                                                    ToolTip="Next" OnClick="ImgMoveNext_Click" />&nbsp;
                                                <asp:ImageButton ID="ImgMoveLast" runat="server" ImageUrl="~/Images/MoveLast.gif"
                                                    ToolTip="Move Last" OnClick="ImgMoveLast_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <aspnew:UpdatePanel ID="panel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="width: 780px; height: 450px; overflow: auto; text-align: center; vertical-align: middle">
                                                    <asp:Image ID="img_docs" runat="server" Height="840px" Width="798px" /></div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="ddlImgsize" EventName="SelectedIndexChanged" />
                                                <aspnew:AsyncPostBackTrigger ControlID="img_delete" EventName="Click" />
                                                <aspnew:AsyncPostBackTrigger ControlID="img_flip" EventName="Click" />
                                            </Triggers>
                                        </aspnew:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" style="height: 11px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            <asp:Panel ID="PopupPanel" runat="server" Height="50px" Width="125px" Style="position: absolute;
                                top: 1000; left: 500px; display: none">
                                <table cellpadding="0" cellspacing="1" style="width: 200px; background-color: White;
                                    border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                    border-bottom: black thin solid;" border="0">
                                    <tr>
                                        <td class="clsLeftPaddingTable" colspan="2" style="height: 34px; background-image: url(../Images/subhead_bg.gif);"
                                            background="../images/headbar_headerextend.gif" valign="middle">
                                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td class="clssubhead">
                                                        <strong>Add Cause No </strong>
                                                    </td>
                                                    <td align="right">
                                                        &nbsp;<asp:LinkButton ID="lnkbtnpclose" runat="server" OnClientClick='return CloseMorePopUp("0");'>X</asp:LinkButton>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                            Cause No :
                                        </td>
                                        <td class="clsLeftPaddingTable">
                                            <asp:TextBox ID="txt_MoreCause" MaxLength="20" CssClass="clstextarea" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                        </td>
                                        <td class="clsLeftPaddingTable">
                                            <asp:Button ID="btn_popupAdd" runat="server" Text="Add" CssClass="clsbutton" Width="50px"
                                                OnClientClick='return CloseMorePopUp("1");' />&nbsp;
                                            <asp:Button ID="btn_popupCancel" runat="server" Text="Cancel" CssClass="clsbutton"
                                                Width="50px" OnClientClick='return CloseMorePopUp("0");' />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel ID="PopupPanel_HCJP51" runat="server" Height="150px" Width="500px" Style="position: absolute;
                                        top: 1000; left: 350px; display: none">
                                        <table id="tbl_Alert" runat="server" cellpadding="0" cellspacing="0" style="width: 100%;
                                            background-color: White; border-right: black thin solid; border-top: black thin solid;
                                            border-left: black thin solid; border-bottom: black thin solid;" border="0">
                                            <tr>
                                                <td class="clsLeftPaddingTable" valign="middle" style="height: 34px; background-image: url(../Images/subhead_bg.gif);">
                                                    &nbsp;<b>Alert</b>&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" align="center">
                                                    <asp:Label CssClass="clsLabel" ForeColor="Red" Font-Size="X-Small" ID="lbl_HCJP51Alert"
                                                        runat="server" Text="This cause number is associated with HCJP 5-1. HCJP 5-1 has only Jury or Pre trial settings. Please change your status selection."></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="clsLeftPaddingTable" height="11">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" align="center">
                                                    <asp:Button CssClass="clsbutton" ID="btn_HCJP51Ok" runat="server" Text="Ok" OnClientClick='return CloseHCJP51PopUp("0");'
                                                        Width="50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" height="8">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tbl_Confirm" runat="server" cellpadding="0" cellspacing="0" style="width: 100%;
                                            background-color: White; border-right: black thin solid; border-top: black thin solid;
                                            border-left: black thin solid; border-bottom: black thin solid;" border="0">
                                            <tr>
                                                <td class="clsLeftPaddingTable" valign="middle" style="height: 34px; background-image: url(../Images/subhead_bg.gif);">
                                                    &nbsp;<b>Confirm</b>&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" align="center">
                                                    <asp:Label CssClass="clsLabel" Font-Size="X-Small" ID="lbl_HCJP51Confirm" runat="server"
                                                        Text="This cause number is associated with HCJP 5-1. HCJP 5-1 allows Pre Trial settings but generally the settings are for Jury Trials. Please confirm the reset says ‘Pretrial’."></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" height="11">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" align="center">
                                                    <asp:Button CssClass="clsbutton" ID="btn_HCJP51Yes" runat="server" Text="Yes" OnClientClick='return CloseHCJP51PopUp("1");'
                                                        Width="50px" onclick="btn_HCJP51Yes_Click" />&nbsp;<asp:Button ID="btn_HCJP51No" CssClass="clsbutton" runat="server"
                                                            Text="No" OnClientClick='return CloseHCJP51PopUp("0");' Width="51px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="clsLeftPaddingTable" height="8">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                                    <aspnew:AsyncPostBackTrigger ControlID="btn_HCJP51Yes" EventName="Click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

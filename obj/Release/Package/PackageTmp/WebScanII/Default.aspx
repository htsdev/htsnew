<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="lntechNew.WebScanII._Default" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Default</title>
    	<LINK href="../Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript">
    function hide()
    { 
        var td=document.getElementById("tdProcess");
        td.style.display='none';
        
    }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body >
    <form id="form1" runat="server">
     <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0"></aspnew:ScriptManager>
        <div>
             
			<TABLE id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
			    <TR>
					<TD  ><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></TD>
					
				</TR>
				<TR>
				<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<TD class="clssubhead" background="../Images/subhead_bg.gif"  height="34">
                        &nbsp;Scan/Search Documents</TD>
				    <TD class="clssubhead" background="../Images/subhead_bg.gif"  height="34" align="right">
                        <asp:HyperLink ID="hpSearch" runat="server" NavigateUrl="~/WebScanII/NewSearch.aspx">Search</asp:HyperLink>&nbsp;&nbsp;&nbsp;</TD>
                  </tr>
                  </table>      
                 </td>       
				</TR>
				<tr>
				    <td colspan="2" class="clsLeftPaddingTable" ></td>
				</tr>
                <tr>
                    <td class="clsLeftPaddingTable" colspan="2" width=100%>
                    <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                   
                    <tr>
                        <td width="14%" class="clsLeftPaddingTable">
                            <strong> Scan Type</strong></td>
                        <td width="10%">
                            &nbsp;<asp:DropDownList ID="ddl_ScanType" runat="server" Width="90px" CssClass="clsinputcombo">
                                            <asp:ListItem Value="888" Selected="True">Resets</asp:ListItem>                                            
                                            <asp:ListItem Value="999">Document Id's</asp:ListItem>
                        </asp:DropDownList></td>
                    <td width="15%" class="clsLeftPaddingTable"  >
                        <strong>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Court House</strong></td>
                    <td width="15%">
                        &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlCourtLoc" runat="server" Width="99px" CssClass="clsinputcombo">
                            <asp:ListItem Value="1">HCJP / Others</asp:ListItem>
                        </asp:DropDownList></td>
                        <td class="clsLeftPaddingTable"  width="15%">
                            <strong>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;Scan
                        Date</strong></td>
                        <td  width="17%">
                            &nbsp;<asp:Label ID="cal_scandate" runat="server" CssClass="label"></asp:Label></td>
                        <td  id="btnScan">
                            
                            <asp:Button ID="btnScan" runat="server" CssClass="clsbutton" Text="Scan " OnClick="btnScan_Click" Width="58px" />
                        </td> 
                        
                    </tr>
                   
                            
                    
                    
                    </table>
                    
                    </td>
                </tr>
				<tr>
					<td align="center" colspan="2" class="clsLeftPaddingTable">
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
				</Tr>
				<tr>
					<td align="center" colspan="2" >
					    	    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
					<table id="tbl_plzwait1"   style="display: none" width="800">
                          <tr>
                          <td class="clssubhead" valign="middle" align="center">
                           <img src="../Images/plzwait.gif" />
                             Please wait while scanning and OCR is in process..
                             </td>
                                                                     
                             </tr>
                                                                    
                                                                    
                          </table>
                          </ContentTemplate>
                          <Triggers>
                            <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                          </Triggers>
                          </aspnew:UpdatePanel>
                        </td>
				</Tr>
				<TR>
					<TD vAlign="top" colSpan="2" style="height: 144px">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td width="100%" >
								    <aspnew:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                    <asp:GridView ID="gv_Scan" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_Scan_PageIndexChanging" OnSorting="gv_Scan_Sorting" PageSize="15" OnRowDataBound="gv_Scan_RowDataBound">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="BatchID" SortExpression="batchid">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBatchID" CssClass="label" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Images" SortExpression="ImagesCount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblimages" CssClass="label" runat="server" Text='<%# bind("ImagesCount") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblScanType" CssClass="label" runat="server" Text='<%# bind("scantype") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# bind("ScanDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle ForeColor="#006699" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Verified" SortExpression="verifiedCount">
                                             <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="verified"  ForeColor="RoyalBlue" runat="server" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Problem" SortExpression="NoCauseCount">
                                             <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="nocause" ForeColor="RoyalBlue" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Queued" SortExpression="QueuedCount">
                                             <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="queued" ForeColor="RoyalBlue" runat="server" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discrepancy" SortExpression="DiscrepancyCount">
                                            <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="discrepancy" ForeColor="RoyalBlue" runat="server" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pending" SortExpression="PendingCount">
                                            <HeaderStyle ForeColor="#006699" />
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="pending" ForeColor="RoyalBlue" runat="server" Text='<%# bind("PendingCount") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                    </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="btnScan" EventName="Click" />
                                </Triggers>
                                </aspnew:UpdatePanel>
                                        
                                </td>
							</tr>
							<tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
							<tr><td>
							    
                               <uc1:footer id="Footer1" runat="server"></uc1:footer>
                                </td></tr>
							<tr>
							<td style="visibility:hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox></td>
							</tr>
						</TABLE>
                        
					</TD>
				</TR>
			</TABLE>
    
    </div>
       
    </form>
    <script type="text/javascript">
        function StartScan()
        {
            var ddlcrtloc=document.getElementById("ddlCourtLoc").value  ;
            var td=document.getElementById("tdProcess"); 
            var sid=document.getElementById("txtsessionid").value;
            var eid=document.getElementById("txtempid").value;
            var sSrv=document.getElementById("txtSrv").value;
            var type='network';
            var path = "<%=ViewState["vNTPATHScanTemp"]%>";
            
            if(ddlcrtloc == 0)
            {
               alert("PLease Select Court House") ;
               document.getElementById("ddlCourtLoc").focus(); 
               return false;
            }
            var sel=OZTwain1.SelectScanner();
	        if(sel=="Success")
	        {
               document.getElementById("tbl_plzwait1").style.display = "block";
		       document.getElementById("tbl_plzwait1").focus();
                OZTwain1.Acquire(sid,eid,path,type,-1);
            }
            else if(sel=="Cancel")
            {
                alert("Operation canceled by user!");
                return false;
            }
            else 
            {
                alert("Scanner not installed.");
	            return false;
            }
        }
    </script>
    <%=Session["objTwain"].ToString()%>
    
</body>
</html>

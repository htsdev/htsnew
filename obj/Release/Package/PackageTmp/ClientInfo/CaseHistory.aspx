﻿<%@ Register TagPrefix="uc1" TagName="Footer" Src="~/WebControls/Footer.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ClientInfo.CaseHistory"
    SmartNavigation="False" CodeBehind="CaseHistory.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>History</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
		
			function DoValidate()
			{
				var txt = document.getElementById("txtSubject");
				if (txt.value == '')
					{
						alert("Please enter some note.");
						txt.focus();
						return false;
					}
			}
		
			function ChangeBGColor(pCell, pMouseStatus)
			{
				if (pMouseStatus == 'in')
					pCell.style.backgroundColor = '#FFF2D9';
				else
					pCell.style.backgroundColor = '#EFF4FB';
			}
			
			
			
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form2" method="post" runat="server">
        
    <div class="page-container row-fluid container-fluid">

        <asp35:ScriptManager ID="ScriptManager" runat="server"></asp35:ScriptManager>

    <asp35:UpdatePanel ID="updatepnlpaging" runat="server">
        <ContentTemplate>

            <%--<table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tbody>
                    <tr>
                        <td>
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    </tbody>
                    </table>--%>
            <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>

    <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="TableGrid" style="">

            <div class="col-xs-12">
                    <div class="page-title">

                <div class="pull-left">
                    <!-- PAGE HEADING TAG - START --><h1 class="title">MATTER --> History</h1><!-- PAGE HEADING TAG - END -->                           

                </div>
                <div class="pull-right hidden-xs">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                
                                                    <tr>
                                                        <td style="height: 35px">
                                                            &nbsp;
                                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="form-label"></asp:Label>,
                                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="form-label"></asp:Label>&nbsp;(
                                                            <asp:Label ID="lbl_CaseCount" runat="server" CssClass="form-label"></asp:Label>),
                                                            <asp:HyperLink ID="hlnk_MidNo" runat="server" CssClass="form-label"></asp:HyperLink>
                                                        
                                                        </td>
                                                   
                                                    </tr>
                        <tr>
                                    <td>
                                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                
                                                </table>

                    </div>

                </div>
                </div>

            <div class="clearfix"></div>

             <section class="box" id="" style="display:none">
                    <%-- <header class="panel_header">
                         <h2 class="title pull-left">Scan/Upload</h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>--%>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <table id="TableNote" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td align="center" bgcolor="#eff4fb">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td bgcolor="aliceblue">
                                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control inline-textbox" Width="320px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btnInsertNote" runat="server" CssClass="btn btn-primary" Width="80px" Font-Bold="True"
                                                    Text="Insert Note"></asp:Button>
                                            </td>
                                            <td>
                                                &nbsp;<br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

            <section class="box" id="" style="">
                     <header class="panel_header">

                         <h2 class="title pull-left">
                             User Activities:
                            <%-- <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="User Activities:" Width="170px"></asp:Label>--%>

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:DataGrid ID="dg_caseHistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                    BorderStyle="Solid" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped">
                                                    <Columns>
                                                        <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyleBig"
                                                            HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                            DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}" HeaderStyle-Width="25%">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="User" DataField="lastname" ItemStyle-CssClass="GridItemStyleBig"
                                                            HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Description" DataField="subject" ItemStyle-CssClass="GridItemStyleBig"
                                                            HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                   </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

             <section class="box" id="" style="">
                     <header class="panel_header">

                         <h2 class="title pull-left">
                             Auto Info History:
                        

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                    <asp:DataGrid ID="dg_autohistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                                BorderStyle="Solid" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped">
                                                                <Columns>
                                                                    <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                        DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}"></asp:BoundColumn>
                                                                    <%--Yasir Kamal 6029 06/17/2009 show user lastname and court location--%>
                                                                    <asp:BoundColumn HeaderText="User" DataField="lastname" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Cause Number" DataField="casenumassignedbycourt" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Ticket No" DataField="refcasenumber" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Auto Status" DataField="Description" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Court Date" DataField="NewAutoDate" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                        DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Court Time" DataField="NewAutoDate" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                        DataFormatString="{0:t}"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Court Number" DataField="NewAutoRoom" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Court Location" DataField="ShortName" ItemStyle-CssClass="GridItemStyleBig"
                                                                        HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center">
                                                                    </asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                   </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>


             <section class="box"   >
             
				<div class="table-responsive" data-pattern="priority-columns">
             
                 <table id="forprimaryuser" runat="server" style="display: none" border="0" cellpadding="0" cellspacing="0" >
                     <tr>
                         <td>

                         

                     <header class="panel_header">

                         <h2 class="title pull-left">
                             Status History:
                        

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>

                 <div class="content-body" >
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group" id="" >
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                    <asp:DataGrid ID="dg_statushistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                                            BorderStyle="Solid" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped">
                                                                            <Columns>
                                                                                <asp:BoundColumn HeaderText="Date & Time" DataField="recdate" ItemStyle-CssClass="GridItemStyleBig"
                                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                                    DataFormatString="{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}" ItemStyle-Font-Size="7pt">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="Ticket No" DataField="refcasenumber" ItemStyle-CssClass="GridItemStyleBig"
                                                                                    HeaderStyle-CssClass="clsaspcolumnheader" HeaderStyle-HorizontalAlign="center"
                                                                                    ItemStyle-Font-Size="7pt"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="New Ver. Status">
                                                                                    <HeaderStyle Width="13%" HorizontalAlign="left" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                        VerticalAlign="Top"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <itemstyle wrap="False" />
                                                                                        <asp:Label ID="lb_verifiedstatus" Font-Size="7pt" runat="server" CssClass="Label"
                                                                                            Text='<%# Eval("shortname") +" " + Eval("newverifiedstatus") +" "+ Eval("newverifiedDate","{0:d}")+" "+ Eval("newverifiedDate","{0:t}") + " " + Eval("NewverifiedRoom") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Old Ver. Status">
                                                                                    <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                        VerticalAlign="Top"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbllastname" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("oldverifiedstatus") +" "+ Eval("oldverifiedDate","{0:d}")+" "+ Eval("oldverifiedDate","{0:t}") + " " + Eval("oldverifiedRoom")  %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="New Scan Status">
                                                                                    <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                        VerticalAlign="Top"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lb_note" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("newscanstatus") +" "+ Eval("newscandate","{0:d}")+" "+ Eval("newscandate","{0:t}")  + " " + Eval("NewscanRoom")  %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Old Scan Status">
                                                                                    <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                        VerticalAlign="Top"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lb_note" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("oldscanstatus") +" "+ Eval("oldscandate","{0:d}")+" "+ Eval("oldscandate","{0:t}")  + " " + Eval("oldscanRoom") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="New Auto Status">
                                                                                    <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                        VerticalAlign="Top"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lb_note" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("newautostatus") +" "+ Eval("newautodate","{0:d}")+" "+ Eval("newautodate","{0:t}")  + " " + Eval("NewautoRoom") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Old Auto Status">
                                                                                    <HeaderStyle HorizontalAlign="left" Width="13%" Font-Size="8pt" CssClass="clsaspcolumnheader"
                                                                                        VerticalAlign="Top"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lb_note1" Font-Size="7pt" runat="server" CssClass="Label" Text='<%# Eval("shortname") +" " + Eval("oldautostatus") +" "+ Eval("oldautodate","{0:d}")+" "+ Eval("oldautodate","{0:t}")   + " " + Eval("oldautoRoom") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                   </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                 </td>
                     </tr>
                 </table>
                 
				</div>
                 
                 </section>

            <div class="clearfix"></div>

            <section class="box" id="">
                     <header class="panel_header">

                         <h2 class="title pull-left">
                             Court Activities:
                             <%--<asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Court Activities:" Width="162px"></asp:Label>--%>
                        

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">

                                          <asp:DataGrid ID="dg_SystemHistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                    BorderStyle="Solid" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped"
                                                    OnItemDataBound="dg_SystemHistory_ItemDataBound">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Date &amp; Time">
                                                            <HeaderStyle Width="25%" HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top">
                                                            </HeaderStyle>
                                                            <ItemTemplate>
                                                                <itemstyle wrap="False" />
                                                                &nbsp;
                                                                <asp:Label ID="lb_Date" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.recdate") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="User">
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top">
                                                            </HeaderStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbllastname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Description">
                                                            <ItemTemplate>
                                                                &nbsp;
                                                                <asp:Label ID="lb_note" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.subject") %>'>
                                                                </asp:Label>&nbsp;
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="75%" CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                               
                                   </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

            <section class="box" id="">
                     <header class="panel_header">

                         <h2 class="title pull-left">
                             Tickler Court History:
                             <%--<asp:Label ID="lblTicklerHistory" runat="server" Font-Bold="True" Text="Tickler Court History:"
                                                    Width="162px"></asp:Label>--%>
                        

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">

                                         <asp:GridView ID="gv_tickler" runat="server" Width="100%" BorderColor="DarkGray"
                                                    BorderStyle="Solid" AllowSorting="true" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped"
                                                    OnSorting="gv_tickler_Sorting">
                                                    <Columns>
                                                        <asp:TemplateField SortExpression="updatedate" HeaderText="<u>Date &amp; Time</u>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_Date" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.updatedate","{0:MM/dd/yyyy @ HH:mm:ss tt}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="casenumassignedbycourt" HeaderText="<u>Cause Number</u>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_CauseNo" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.casenumassignedbycourt") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="ticklerevent" HeaderText="<u>Tickler Event</u>">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_ticklerevent" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ticklerevent") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                     
                               
                                   </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

             <section class="box" id="">
                     <header class="panel_header">

                         <h2 class="title pull-left">
                             SMS History:
                            <%-- <asp:Label ID="lblSMSHistory" runat="server" Font-Bold="True" Text="SMS History:"
                                                    Width="162px"></asp:Label>--%>
                        

                         </h2>
                         <div class="actions panel_actions pull-right">
                      
                             <a class="box_toggle fa fa-chevron-down"></a>
                    
                    </div>
                </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">

                                         <asp:GridView ID="GV_SMSHistory" runat="server" Width="100%" BorderColor="DarkGray"
                                                    BorderStyle="Solid" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Date &amp; Time">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_SMSDate" runat="server" CssClass="form-label" Text='<%# Eval("recdate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="70px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_SMSTicketNo" runat="server" CssClass="form-label" Text='<%# Eval("TicketNumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Mobile Number">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_SMSMnumber" runat="server" CssClass="form-label" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Message">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_SMSMessage" runat="server" CssClass="form-label" Text='<%# Eval("SentInfo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Alert Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_SmsAlertType" runat="server" CssClass="form-label" Text='<%# Eval("SmsCallType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sent/Received">
                                                            <ItemTemplate>
                                                                <asp:Label ID="LblSendReceived" runat="server" CssClass="form-label" Text='<%# Eval("SendRecieved") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                     
                               
                                   </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

             <div class="clearfix"></div>


            <section class="box" >
            
            <div class="content-body" style="display:none">
                <div class="row">
                      <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:HiddenField ID="hf_CheckBondReportForSecUser" Value="0" runat="server" />
                                     <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                    </div>
                                                                </div>
                          </div>
                    </div>
                </div>
                </section>


















            </section>
        </section>


            </ContentTemplate>
        </asp35:UpdatePanel>

    </div>

    </form>
     <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

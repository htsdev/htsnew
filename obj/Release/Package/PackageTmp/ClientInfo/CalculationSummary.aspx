<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.ClientInfo.CalculationSummary" Codebehind="CalculationSummary.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CalculationSummary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<base target="_self">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			function SetGridRows()
			{
				//a;
				var AddTotal = document.getElementById("lblAddTotal").innerText;
				var tblAdd = document.getElementById("tblAdditional");

				var Speed = document.getElementById("lblSpeeding").innerText;
				var trSpeeding =  document.getElementById("trSpeeding");
				
				var Accident = document.getElementById("lblAccident").innerText;
				var trAccident =  document.getElementById("trAccident");

				var CDL = document.getElementById("lblCDL").innerText;
				var trCDL =  document.getElementById("trCDL");

				var Round = document.getElementById("lblRound").innerText;
				var trRound =  document.getElementById("trRound");

				var Cont = document.getElementById("lblContinuance").innerText;
				var trCont =  document.getElementById("trCont");

				var Init = document.getElementById("lblInitialAdj").innerText;
				var trInitial =  document.getElementById("trIAdj");

				var Final = document.getElementById("lblFinalAdj").innerText;
				var trFinal =  document.getElementById("trFAdj");
                    
                var latefee = document.getElementById("lbllatefee").innerText;
				var trlatefee =  document.getElementById("trlatefee");
                
                var planfee = document.getElementById("lblplanfee").innerText;
                var trplanfee = document.getElementById("trplanfee");

                // Rab Nawaz Khan 10330 07/16/2012 Added for the Day Of Arr . . .
                var dayOfArrFee = document.getElementById("lbl_ArrDayOf").innerText;
                var tr_dayOfArrFee = document.getElementById("tr_ArrDayOf");

				if (AddTotal == '' || AddTotal == '0')
					{
					tblAdd.style.display = 'none';
					return;
					}
				else
					{
					tblAdd.style.display = 'block';
					//return;
					}
					
				if (Speed == '' || Speed == '0')
					trSpeeding.style.display = 'none';
				else
					trSpeeding.style.display = 'block';
					
				if (Accident == '' || Accident == '0')
					trAccident.style.display = 'none';
				else
					trAccident.style.display = 'block';
					
					
				if (CDL == '' || CDL == '0')
					trCDL.style.display = 'none';
				else
					trCDL.style.display = 'block';
					

				if (Round == '' || Round == '0')
					trRound.style.display = 'none';
				else
					trRound.style.display = 'block';
					
				if (Cont == '' || Cont == '0')
					trCont.style.display = 'none';
				else
					trCont.style.display = 'block';

				if (Init == '' || Init == '0')
					trInitial.style.display = 'none';
				else
					trInitial.style.display = 'block';

				if (Final == '' || Final == '0')
					trFinal.style.display = 'none';
				else
					trFinal.style.display = 'block';

                if (latefee == '' || latefee == '0')
					trlatefee.style.display = 'none';
				else
					trlatefee.style.display = 'block';
                
				// tahir 4786 10/08/2008
                if (planfee == '' || planfee == '0')
					trplanfee.style.display = 'none';
				else
				    trplanfee.style.display = 'block';

				// Rab Nawaz Khan 10330 07/16/2012 Added for the Day Of Arr . . .
				if (dayOfArrFee == '' || dayOfArrFee == '0')
				    tr_dayOfArrFee.style.display = 'none';
				else
				    tr_dayOfArrFee.style.display = 'block';
	
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="500" align="left" border="0">
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><IMG height="17" src="../images/head_icon.gif" width="30">&nbsp;<STRONG>Calculation 
										Summary</STRONG></td>
							</tr>
							<tr>
								<td background="../../images/separator_repeat.gif" height="11"></td>
							</tr>
							<tr>
								<td background="../../images/headbar_headerextend.gif" height="5"></td>
							</tr>
							<tr>
								<td class="clssubhead" background="../../images/headbar_midextend.gif" height="18"></td>
							</tr>
							<tr>
								<td background="../../images/headbar_footerextend.gif" height="10"></td>
							</tr>
						</TABLE>
					</td>
				</tr>
				<TR>
					<td>
						<asp:datagrid id="dgResult" runat="server" CssClass="clsleftpaddingtable" Width="500px" AutoGenerateColumns="False"
							PageSize="20" BorderColor="White" BorderStyle="None">
							<Columns>
								<asp:TemplateColumn>
									<ItemTemplate>
										<TABLE class="clsleftpaddingtable" id="tblItem" cellSpacing="1" cellPadding="0" width="100%"
											border="0">
											<TR>
												<TD colSpan="4">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR id="trCaseNo" runat="server">
															<TD class="clslabel" width="15%">Case Number
															</TD>
															<TD width="85%">&nbsp;
																<asp:Label id=lblCaseNumber runat="server" CssClass="grdlabel" Font-Bold="True" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
																</asp:Label></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR id="trViolation" runat="server">
												<TD width="5%">&nbsp;</TD>
												<TD class="clslabel" width="30%">Violation Description
												</TD>
												<TD width="50%">
													<asp:Label id=lblViolDesc runat="server" Width="205px" CssClass="grdlabel" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'>
													</asp:Label></TD>
												<TD width="15%">&nbsp;</TD>
											</TR>
											<TR>
												<TD>&nbsp;</TD>
												<TD class="clslabel">Price Type
												</TD>
												<TD>
													<asp:Label id=lblPriceType runat="server" Width="197px" CssClass="grdlabel" Text='<%# DataBinder.Eval(Container, "DataItem.PriceType") %>'>
													</asp:Label></TD>
												<TD>&nbsp;</TD>
											</TR>
											<TR>
												<TD>&nbsp;</TD>
												<TD class="clslabel">Calculation Description
												</TD>
												<TD>
													<asp:Label id=lblCalcDesc runat="server" Width="197px" CssClass="grdlabel" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationDescription") %>'>
													</asp:Label></TD>
												<TD>&nbsp;</TD>
											</TR>
											<TR>
												<TD>&nbsp;</TD>
												<TD class="clslabel">Amount</TD>
												<TD>&nbsp;</TD>
												<TD align="right">
													<asp:Label id=lblAmount runat="server" CssClass="grdlabel" Font-Bold="True" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "{0:C0}") %>'>
													</asp:Label>&nbsp;</TD>
											</TR>
											<TR>
												<TD background="../../images/separator_repeat.gif" colSpan="4" height="11"></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center"></PagerStyle>
						</asp:datagrid>
					</td>
				</TR>
				<tr>
					<td><TABLE runat="server" id="tblAdditional" WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0"
							class="clsleftpaddingtable">
							<TR>
								<TD colspan="4" class="grdlabel" style="FONT-WEIGHT: bold">&nbsp;Additional Charges 
									/ Adjustments</TD>
							</TR>
							<TR id="trSpeeding">
								<TD width="5%" style="HEIGHT: 17px"></TD>
								<TD width="40%" class="clslabel">Speeding</TD>
								<TD align="right">
									<asp:Label id="lblSpeeding" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR id="trAccident">
								<TD></TD>
								<TD class="clslabel">Accident</TD>
								<TD align="right">
									<asp:Label id="lblAccident" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR id="trCDL">
								<TD></TD>
								<TD class="clslabel">CDL</TD>
								<TD align="right">
									<asp:Label id="lblCDL" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR id="trlatefee">
								<TD></TD>
								<TD class="clslabel">Late Fee</TD>
								<TD align="right">
									<asp:Label id="lbllatefee" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR id="trplanfee">
								<TD>&nbsp;</TD>
								<TD class="clslabel">Payment Plan Fee</TD>
								<TD align="right">
									<asp:Label id="lblPlanFee" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD>&nbsp;</TD>
							</TR>
							<TR id="trRound">
								<TD></TD>
								<TD class="clslabel" align="right">Rounding Off</TD>
								<TD align="right"><asp:Label id="lblRound" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD align="right" width="35%"></TD>
							</TR>
							<TR id="trCont">
								<TD></TD>
								<TD class="clslabel" align="right">Continuance Amount</TD>
								<TD align="right" width="20%">
									<asp:Label id="lblContinuance" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD align="right" width="35%"></TD>
							</TR>
							<TR id="trIAdj">
								<TD></TD>
								<TD class="clslabel" align="right">Initial Adjustment</TD>
								<TD align="right" width="20%">
									<asp:Label id="lblInitialAdj" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD align="right" width="35%"></TD>
							</TR>
							<TR id="trFAdj">
								<TD></TD>
								<TD class="clslabel" align="right">Final Adjustment</TD>
								<TD align="right" width="20%">
									<asp:Label id="lblFinalAdj" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD align="right" width="35%"></TD>
							</TR>
							<TR id="tr_ArrDayOf">
								<TD></TD>
								<TD class="clslabel" align="right">Day of Arr</TD>
								<TD align="right" width="20%">
									<asp:Label id="lbl_ArrDayOf" runat="server" CssClass="grdlabel"></asp:Label></TD>
								<TD align="right" width="35%"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 14px"></TD>
								<TD class="clslabel" align="right" style="HEIGHT: 14px"></TD>
								<TD width="20%" class="clslabel" style="HEIGHT: 14px">Total</TD>
								<TD width="35%" align="right" style="HEIGHT: 14px">
									<asp:Label id="lblAddTotal" runat="server" CssClass="grdlabel" Font-Bold="True"></asp:Label>&nbsp;</TD>
							</TR>
							<TR>
								<TD background="../../images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td align="right" class="GRDLABEL" style="FONT-SIZE: 12pt; height: 22px;">
						<asp:Label id="lbl_Total" runat="server" Font-Size="Larger">TOTAL:</asp:Label>&nbsp;
						<asp:Label id="lblGrandTotal" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label>&nbsp;</td>
					
				</tr>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<tr>
					<td colSpan="2">
						<asp:label id="lblMessage" runat="server" Width="272px" Font-Bold="True" ForeColor="Red" Font-Names="Verdana"
							Font-Size="XX-Small"></asp:label></td>
				</tr>
			</TABLE>
			<script language="javascript"> SetGridRows();</script>
		</form>
	</body>
</HTML>

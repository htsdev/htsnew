<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.ClientInfo.WebForm1" Codebehind="FrmCreditCardInfo.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Insert Comments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
	   function closewin()
	    {
	      opener.location.reload();	      	     
	      self.close();	   
	    }
	   
	    
		</script>
	</HEAD>
	<body onunload="closewin();" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" width="350" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
				<TR>
					<td colSpan="2">
						<Table id="tblsub" width="350" class="clsLeftPaddingTable" height="20" border="0" align="center"
							cellpadding="0" cellspacing="1">
							<tr>
								<TD vAlign="top">
									<P><STRONG><FONT color="#3366cc">Credit Card Info</FONT></STRONG></P>
								</TD>
								<TD vAlign="top"></TD>
							</tr>
							<TR>
								<TD class="clsleftpaddingtable" vAlign="top">Name</TD>
								<TD vAlign="top">
									<asp:TextBox id="TextBox1" runat="server" CssClass="clsinputadministration" Width="176px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="clsleftpaddingtable" vAlign="top">Card Number</TD>
								<TD vAlign="top">
									<asp:TextBox id="TextBox2" runat="server" CssClass="clsinputadministration" Width="176px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="clsleftpaddingtable" vAlign="top">Card Type</TD>
								<TD vAlign="top" style="HEIGHT: 17px">
									<asp:DropDownList id="DropDownList1" runat="server" CssClass="clsinputcombo" Width="176px">
										<asp:ListItem Value="MasterCard" Selected="True">MasterCard</asp:ListItem>
										<asp:ListItem Value="AmericanExpress">AmericanExpress</asp:ListItem>
										<asp:ListItem Value="Visa">Visa</asp:ListItem>
									</asp:DropDownList></TD>
							</TR>
							<tr>
								<TD class="clsleftpaddingtable" valign="top">Cin</TD>
								<TD>
									<asp:TextBox id="TextBox3" runat="server" CssClass="clsinputadministration" Width="56px"></asp:TextBox></TD>
							</tr>
							<TR>
								<TD class="clsleftpaddingtable" valign="top">Expiration Date</TD>
								<TD>
									<asp:DropDownList id="DropDownList2" runat="server" Width="48px" CssClass="clsinputcombo">
										<asp:ListItem Value="01" Selected="True">01</asp:ListItem>
										<asp:ListItem Value="02">02</asp:ListItem>
										<asp:ListItem Value="03">03</asp:ListItem>
									</asp:DropDownList>/
									<asp:DropDownList id="DropDownList3" runat="server" Width="51px" CssClass="clsinputcombo">
										<asp:ListItem Value="06" Selected="True">06</asp:ListItem>
										<asp:ListItem Value="07">07</asp:ListItem>
										<asp:ListItem Value="08">08</asp:ListItem>
									</asp:DropDownList></TD>
							</TR>
							<TR>
								<TD align="right"></TD>
								<TD align="right">
									<asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Update"></asp:button></TD>
							</TR>
						</Table>
					</td>
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

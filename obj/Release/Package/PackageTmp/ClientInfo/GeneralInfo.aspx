﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.ClientInfo.GeneralInfo"
    AspCompat="True" EnableViewState="True" SmartNavigation="False" CodeBehind="GeneralInfo.aspx.cs"
    ValidateRequest="false" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<%@ Register Src="../WebControls/ReadNotes.ascx" TagName="ReadNotes" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc" TagName="AddPopUpComment" Src="../WebControls/AddPopUpComment.ascx" %>
<%@ Register TagName="ContactInfo" TagPrefix="Contact" Src="~/WebControls/ContactInfo.ascx" %>
<%@ Register TagName="LookUp" TagPrefix="ContactID" Src="~/WebControls/ContactIDLookUp.ascx" %>
<%@ Register TagName="PolmControl" TagPrefix="uc1" Src="~/WebControls/PolmControl.ascx" %>
<%@ Register TagName="VerifyEmailControl" TagPrefix="VerifyEmail" Src="~/WebControls/VerifyEmail.ascx" %>
<%@ Register TagPrefix="uc3" TagName="UpdateEmailAddress" Src="../WebControls/UpdateEmailAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Contact Page</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>


    <script src="../Scripts/boxover.js" type="text/javascript"></script>

    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>
    <meta content="True" name="vs_snapToGrid" />
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Scripts/flags.js"></script>

    <%=Convert.ToString(Session["objTwain"])%>

    <script type="text/javascript">
        //Farrukh iftikhar 9332 07/30/2011 popup window close after update
        function refreshContent(){            
            document.getElementById("tdData").style.display = 'none';
            __doPostBack('lbRefresh','');
        }
		 
        //Farrukh 9925 11/30/2011 Returns alert message for validation and code redundancy removed		
        function AlertForValidation(obj,msg)
        {
            debugger;
            $("#txtErrorMessage").text(msg);
            $("#errorAlert").modal();
          //  alert(msg);
            obj.focus(); 
            return false;	
        }
		
        // Rab Nawaz Khan 11473 10/24/2013 Added for Add new Lead Control. . . 
        function openAddNewLeadWindow(url,width,height) {
            var languageSpeak = document.getElementById('hf_languageSpk').value;
            var completeName = document.getElementById('hf_FirstNameLastName').value;
            var contactNum = document.getElementById('hf_ContactNum').value;
            var email = document.getElementById('hf_emailadd').value;
            var ticketId = document.getElementById('hf_TicketIdForLead').value;
            var left = parseInt((screen.availWidth/2) - (width/2));
            var top = parseInt((screen.availHeight/2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + 
                ",status,left=" + left + ",top=" + top + 
                "screenX=" + left + ",screenY=" + top + ",scrollbars=yes";
            window.open(url + languageSpeak +'&completeName='+completeName+'&contactNum='+contactNum+'&email='+email+'&caseNum='+ticketId, "", windowFeatures);
            return false;
        }     
		
        function showhideComments(i)
        {
		
            if(i==1)
                document.getElementById('td_free_con').style.display='block';
            if(i==0)
                document.getElementById('td_free_con').style.display='none';
        }
        //====================Added By Zeeshan Ahmed for Continuance==============//
		
        function DisplayToggleP(controlid)
        {		   
		 
            var temp =   document.getElementById(controlid + "_ddl_FirmAbbreviation").style.display;
            document.getElementById(controlid +"_ddl_FirmAbbreviation").style.display = document.getElementById(controlid +"_lbl_FirmAbbreviation").style.display;
            document.getElementById(controlid +"_lbl_FirmAbbreviation").style.display = temp;
        }
		
		
        function DisplayToggle()
        {	          		            		            		        
            var ddl =   document.getElementById("ddl_continuancedate").style.display;
            var lbl =	document.getElementById("lbl_continunacedate").style.display;
			
            document.getElementById("lbl_continunacedate").style.display = ddl;
            document.getElementById("ddl_continuancedate").style.display = lbl;
    
        }
        
        	 
        function addContinuance(ctrl,prevdate)
        {
					
            //Continuance can only be added in these statuses
            if(document.getElementById("hf_Status").value=="0" & document.getElementById("ddl_flags").value=="9")
            {
                $("#txtErrorMessage").text("Sorry You cannot add continuance flag for this case because continuances can only be filed on cases that are in Jury, Judge or Pre Trial Status.");
                $("#errorAlert").modal();
//                alert("Sorry You cannot add continuance flag for this case because continuances can only be filed on cases that are in Jury, Judge or Pre Trial Status.");
                document.getElementById("ddl_flags").selectedIndex = 0;
                return false;
            }
            //End	
			
            var curdate = document.getElementById(ctrl).value;
            var ctrlname = document.getElementById(ctrl).name;
            var cdate= document.getElementById("hf_continuancedate").value;		
        			
            if ( curdate != prevdate && prevdate !='' && curdate !="0")
            {
			
                var pdate = Date.parse(prevdate);
                var cdate = Date.parse(curdate);
				
                if ( (cdate - pdate) < 0 )
                {
                    $("#txtErrorMessage").text("Please Enter Continuance date greater than previous continuance date.");
                    $("#errorAlert").modal();
                  //  alert("Please Enter Continuance date greater than previous continuance date.");
                    document.getElementById(ctrl).value = prevdate;
                    return false;
                }
		        
                if ( document.getElementById("hf_unpaid").value == "0")
                {
                    if ( false == confirm("It simply update the continuance date for the case. Click ok if you want to add a new continuance or click cancel if you want to update the continuance date."))
                    {
                        document.getElementById("hf_updatecontinuance").value="1";
                        setTimeout('__doPostBack(\'' + ctrlname + '\',\'\')', 0);
                        return true                                        
                    }
                    else
                    {
                        document.getElementById("hf_updatecontinuance").value="0";
                    }                   
                }
                    
                if ( document.getElementById("hf_unpaid").value == "1")
                {
                    if ( true == confirm("It simply update the continuance date for the case. Click ok if you want to update new continuance or if you want add a new continuance for the case click cancel and pay your previous continuance."))
                    {
                        document.getElementById("hf_updatecontinuance").value="1";
                        setTimeout('__doPostBack(\'' + ctrlname + '\',\'\')', 0);
                        return true                                        
                    }
                    document.getElementById(ctrl).value = prevdate;
                    return false;
                }  
		        	       	
                // Verify Continuance Amount to be added
                var chk =  confirm("A $50 fee will automatically be added (and locked) to this client's fee. Press 'OK' to continue.");
                
                if ( chk==true){
                    setTimeout('__doPostBack(\'' + ctrlname + '\',\'\')', 0);
                    return true;
                }
                else {
                    document.getElementById(ctrl).value = prevdate;
                    return false;
                }
		
            }
            else if ( prevdate  == "")
                return false;
		
            //		else if ( curdate == "0");
            //		{
            //		alert("Please select valid court date.");
            //		document.getElementById(ctrl).value = prevdate;
            //		return false;
            //		}   

		
            document.getElementById(ctrl).value = prevdate;
            return false;		
			
		
        }
		 
        //Muhammad Ali 7725 06/11/2010 --Change Test() to FlagValidation()
        function FlagValidation()
        {
            //-- Muhammad Ali(Night-Shift) '7725'  01-06-2010 --PreTrailDiversion(Monks Billing) Flag set to allow only Criminal Cases only--
            if(document.getElementById("hf_casetype").value!="2" && document.getElementById("ddl_flags").value=="41")
            {
                $("#txtErrorMessage").text("Invalid Flag. This flag can only be added for Criminal cases");
                $("#errorAlert").modal();
            //    alert("Invalid Flag. This flag can only be added for Criminal cases");
                document.getElementById("ddl_flags").selectedIndex = 0;
              //  //document.getElementById("td_showwaiting").style.display = "none";
                document.getElementById("btn_update2").disabled = false;
                document.getElementById("btn_next").disabled = false;
                return false;
            }
            //--END Muhammad Ali(Night-Shift)--		 
		 	 
            //----khalid 17-9-07 ----start --adding new service ticket---
            var flag=document.getElementById("ddl_flags");
            var percentage =document.getElementById("hf_checkServiceTicket").value;
            var iscrim=document.getElementById("hf_casetype").value;
            var courtstatus=document.getElementById("Hf_courtstatus").value;
            // Noufil 7157 21/12/2009 show plz wait image and disable button
          //  //document.getElementById("td_showwaiting").style.display = "";
            document.getElementById("btn_update2").disabled = true;
            document.getElementById("btn_next").disabled = true;        
                
            // Agha Usman 4165 06/03/2008
            if (flag.value=="33") 
            {
                //alert(confirm("Bad Phone number flag will move the ticket to batch print press [OK] to continue or [CANCEL] to stop the process"));
                var obj = confirm("Bad Phone number letter will be sent to batch print. Press [OK] to continue or [CANCEL] to stop the process");
                if (obj == false) 
                {
                    document.getElementById("ddl_flags").selectedIndex = 0;
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                   // //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                    return false;
                }
            }
        
            // Abid Ali 4846
            // updating changes and refactorin code
            if (flag.value == "36" || flag.value == "13") 
            {        
                var selectedText = flag.options[flag.selectedIndex].text;
                var obj = confirm("This will add '" + selectedText + " Flag' to all the profile of this defendent. Press [OK] to continue or [CANCEL] to stop the process");
                if (obj == false) 
                {
                    flag.selectedIndex = 0;                
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                  //  //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                               
                    return false;
                }
                else if (flag.value == "36" && obj == true)
                {
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                  //  //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                
                    //return true;
                }                   
            }
        
            //noufil 4215 06/16/2008 check if client case is on criminal court and court status is waiting
            var isactive = document.getElementById("hf_isactive").value;
            if (flag.value=="35" && iscrim=="1" && courtstatus=="1" && isactive=="1")
            {
                                                                
                var hiredate = new Date(document.getElementById("hf_courtdateyear").value);
                //Add 2 Year In Hire Date 
                var addyear= new Date(hiredate.getFullYear()+2,hiredate.getMonth(),hiredate.getDate());
                var today = new Date();                                       
           
                //If Greater Than 2 Year
                if (addyear.getTime() > today.getTime())
                {  
                              
                    if (!confirm("This case has not been with us for 2 years.\r\n- By activating this flag, the system will automatically close out all associated cases and \r\n  send a letter to the client instructing the client that the cases have been closed.\r\n  Are you sure you want to dispose the cases?"))
                    {
                        document.getElementById("ddl_flags").selectedIndex = 0;
                        // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                      //  //document.getElementById("td_showwaiting").style.display = "none";
                        document.getElementById("btn_update2").disabled = false;
                        document.getElementById("btn_next").disabled = false;
                        return false;
                    }
                }
                else
                {
                    if (!confirm("By activating this flag, the system will automatically close out all associated cases and \r\n  send a letter to the client instructing the client that the cases have been closed.\r\n Are you sure you want to activate the flag? "))
                    {
                        document.getElementById("ddl_flags").selectedIndex = 0;
                        // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                        ////document.getElementById("td_showwaiting").style.display = "none";
                        document.getElementById("btn_update2").disabled = false;
                        document.getElementById("btn_next").disabled = false;
                        return false;
                    }
                }
            }
            //else
            if (flag.value=="35" && (iscrim=="0"|| courtstatus=="0" ||isactive=="0"))
            {
                if (isactive=="0"){
                    $("#txtErrorMessage").text("Sorry this flag can only be added for client cases.");
                    $("#errorAlert").modal();
                   // alert("Sorry this flag can only be added for client cases.");
                }
                else if(iscrim!="1"){
                    $("#txtErrorMessage").text("This Flag Cannot be activated with this court house.");
                    $("#errorAlert").modal();
                    //  alert("This Flag Cannot be activated with this court house.");
                }
                else if (courtstatus == "0"){
                    $("#txtErrorMessage").text("This flag can only be activated when associated Court is HMC and status is Arraignment Waiting or Court is HCJP and status is Waiting.");
                    $("#errorAlert").modal();
                  //  alert("This flag can only be activated when associated Court is HMC and status is Arraignment Waiting or Court is HCJP and status is Waiting.");
                }
                     
                document.getElementById("ddl_flags").selectedIndex = 0;
                // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                //document.getElementById("td_showwaiting").style.display = "none";
                document.getElementById("btn_update2").disabled = false;
                document.getElementById("btn_next").disabled = false;
                return false;
            }
        
    
            
            //Added by khalid 
            if(flag.value=="27" && '0'== '<%= ViewState["ALRDocument"] %>')
            {
                $("#txtErrorMessage").text("Sorry ALR Request Confirmation flag  Can't be added directly, To add Please Scan Document in Document Section");
                $("#errorAlert").modal();
                //alert("Sorry ALR Request Confirmation flag  Can't be added directly, To add Please Scan Document in Document Section");
                document.getElementById("ddl_flags").selectedIndex = 0;
                // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                //document.getElementById("td_showwaiting").style.display = "none";
                document.getElementById("btn_update2").disabled = false;
                document.getElementById("btn_next").disabled = false;
                return false;
            }
       
       
            //Agha Usman 07/02/2008 4271 - Comment the code which is responsible of restricting two multiple service ticket
       
            //        if( flag.value==23 && percentage<=100)
            //		{ alert("New service Ticket cannot be added if previous service ticket is not completed to 100%");
            //		  flag.value=0;
            //		  return false;
            //		  
            //		}
            if ( flag.value == 23 )
            {
                //            setpopuplocation();
                //            flag.selectedIndex =0;
                //            document.getElementById("ddl_category2").focus();
                //            document.getElementById("ddl_category2").selectedIndex=0;
                //            document.getElementById("ddl_priority").selectedIndex=0;
                //             document.body.scrollTop = 0;
             
                //Aziz 2382 12/19/2007
                //Adding of service ticket option 
                var w = 1100;
                var h = 750;
                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                var top = ((height / 2) - (h / 2)) + dualScreenTop;
                var ticketid = <%=this.TicketId%>;
                var path="../Reports/GeneralCommentsPopup.aspx?ticketid="+ticketid;
                //Sabir Khan 5711 04/01/2009 Height of comment control has been changed.


                window.open(path,'',"fullscreen=no,toolbar=no,left=" + left +",top=" +top + ",status=no,menubar=no,resizable=no,width="+w+",height="+h+",scrollbars=yes");
                document.getElementById("ddl_flags").selectedIndex = 0;
                // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                //document.getElementById("td_showwaiting").style.display = "none";
                document.getElementById("btn_update2").disabled = false;
                document.getElementById("btn_next").disabled = false;
                return false;         
            } 
            ///Added By Fahad For Read Note Flag (Fahad- 12/24/2007)
            if(flag.value == 31)
            {  
                flag.selectedIndex =0;
                var ticketid = <%=this.TicketId%>;
                var empid = <%=ViewState["empid"]%>;
                var path="../Reports/ReadComments.aspx?ticketid="+ticketid + "&empid=" + empid;
                window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no,width=390,height=215,scrollbars=no");
                document.getElementById("ddl_flags").selectedIndex = 0;
                // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                //document.getElementById("td_showwaiting").style.display = "none";
                document.getElementById("btn_update2").disabled = false;
                document.getElementById("btn_next").disabled = false;
                return false;         
        
            } 
            //-------end ----khalid
                
            //Continuance can only be added in these statuses
            if(document.getElementById("hf_Status").value=="0" && document.getElementById("ddl_flags").value=="9")
            {
                $("#txtErrorMessage").text("Sorry, You cannot add continuance flag for this case because continuances can only be filed on cases that are in Jury, Judge or Pre Trial Status.");
                $("#errorAlert").modal();
                //alert("Sorry, You cannot add continuance flag for this case because continuances can only be filed on cases that are in Jury, Judge or Pre Trial Status.");
                document.getElementById("ddl_flags").selectedIndex = 0;
                // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                //document.getElementById("td_showwaiting").style.display = "none";
                document.getElementById("btn_update2").disabled = false;
                document.getElementById("btn_next").disabled = false;
                return false;
            }
		    
            //Sabir Khan 5260 11/27/2008 Checks for HCJP , HMC  , IDTicket and Not on system flag ...
            //----------------------------------------------------------
            if(document.getElementById("ddl_flags").value == "18")
            {		
                <%HasNotonSystem();%>;        
                if(((document.getElementById("hf_isHCJPcase").value == "1") || (document.getElementById("hf_isHMCcase").value == "1")) && (document.getElementById("hf_HasIDTicket").value =="1") && (document.getElementById("hf_HasNotonSystem").value =="1"))
                {
                    $("#txtErrorMessage").text("Please enter the correct Ticket Number for client cases. System doesn't allow cases with ID Ticket Number.\nIf court still has not inputted the ticket number then first set the Not on System flag. ");
                    $("#errorAlert").modal();
                    //alert("Please enter the correct Ticket Number for client cases. System doesn't allow cases with ID Ticket Number.\nIf court still has not inputted the ticket number then first set the Not on System flag. ")
                    document.getElementById("ddl_flags").value="0";
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                    //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                    return false;
                }
		        
            }
            //-----------------------------------------------------------		    
            
            var v = document.getElementById("ddl_flags").value;
                
            if ( v!="9"  && v!="0" )
            {
                setTimeout('__doPostBack(\'ddl_flags\',\'\')', 0);
                		        
            }
                
                	       
            if ( v=="9")
            {
                if ( document.getElementById("hf_unpaid").value == "1")
                {
                    $("#txtErrorMessage").text("Please first pay your previous continuance amount.");
                    $("#errorAlert").modal();
                    //alert("Please first pay your previous continuance amount.");
                    document.getElementById("ddl_flags").selectedIndex = 0;
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                    //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                    return false;                        
                }
                    
                if ( document.getElementById("hf_continuancedate").value == "1/1/1900")
                {
                    $("#txtErrorMessage").text("Sorry You cannot add continuance flag for this case because you do not have a continuance date for this case.");
                    $("#errorAlert").modal();
                   // alert("Sorry You cannot add continuance flag for this case because you do not have a continuance date for this case.");
                    document.getElementById("ddl_flags").selectedIndex = 0;
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                    //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                    return false;
		                
                }     
                                                                   
                var chk =  confirm("A $50 fee will automatically be added (and locked) to this client's fee. Press 'OK' to continue.");
                if ( chk==true) {		            
                    setTimeout('__doPostBack(\'ddl_flags\',\'\')', 0);
                }
                else {
                    // Noufil 7157 21/12/2009 Hide plz wait image and enable button
                    //document.getElementById("td_showwaiting").style.display = "none";
                    document.getElementById("btn_update2").disabled = false;
                    document.getElementById("btn_next").disabled = false;
                    return false;		        
                }
            }          
            return false;
        }
        
        //Nasir 6098 08/31/2009 complaint flag cannot remove
        function Complaint()
        {        
            $("#txtErrorMessage").text("This flag cannot be removed.");
            $("#errorAlert").modal();
          //  alert("This flag cannot be removed.")                   
            return false;                            
        }
        
        //added by Ozair for Continuance Modifications
        function checkConSD()
        {
		
            var ConStatus = document.getElementById('<%=  Convert.ToString(ViewState["ddl_ContinuanceStatus"]) %>_ddl_continuancestatus');
            var ConDate = document.getElementById('<%=  Convert.ToString(ViewState["ddl_ContinuanceDate"]) %>_ddl_continuancedate');
            if(ConStatus == null || ConDate==null)
            {
                return true;
            }
            else if ( ConStatus.value == "0")
            {
                $("#txtErrorMessage").text("Please Select Continuance Status.");
                $("#errorAlert").modal();
              //  alert("Please Select Continuance Status.");
                ConStatus.focus();		    
                return false;
            }
            else if ( ConDate.value == "0")
            {
                $("#txtErrorMessage").text("Please Select Continuance Date.");
                $("#errorAlert").modal();
               // alert("Please Select Continuance Date.");
                ConDate.focus();
                return false;
            }
            return true;
        }
        //END
        function checkFirm()
        {
            var update = document.getElementById("hf_updateaddress");		    		    
            var chkfirm = document.getElementById("hf_checkFirm");
            var firm = document.getElementById('<%=  Convert.ToString(ViewState["ddl_FirmAbbreviation"]) %>_ddl_FirmAbbreviation');
		   		    
            //		    if ( chkfirm.value == "1" && firm !="3000")
            //		    {
            //		        var test= confirm("Please note any contact information for this client will be overwritten with the contact information from the firm selected. Would you like to overwrite this information.");
            //                
            //                if ( test == true)
            //                update.value = "1"
            //                
            //                else
            //                update.value = "0";
            //                
            //                return true;
            //		    }

            if(firm == null)
                return(true);
            if ( firm.style.display == "block")
            {		        		   	    
                if ( firm.value == "3000")
                {
                    $("#txtErrorMessage").text("To assign this firm please first remove the outside flag or select diferent firm");
                    $("#errorAlert").modal();
                   // alert("To assign this firm please first remove the outside flag or select diferent firm");		    
                    return false;
                }
                document.getElementById("hf_prevfirm").value =  document.getElementById('<%=Convert.ToString(ViewState["ddl_FirmAbbreviation"]) %>_ddl_FirmAbbreviation').value;
            }
            return true;
        }
		
        function checkContinuance(chk)
        {
            // Check for Continuance Flag
            //var activeflag =  document.getElementById("hf_activeflag").value;	
		    		    	    
            if ( chk == "True" || chk == "1" )
            {
                $("#txtErrorMessage").text("Sorry You Cannot change continuance flag after payment.");
                $("#errorAlert").modal();
               // alert("Sorry You Cannot change continuance flag after payment.");
                return false;
            }
		    
            var chk =  confirm("Continuance flag will be removed. A $50 fee will automatically be subtracted (and locked) from this client's fee. Press 'OK' to continue.");
            return chk;
		    		
        }
		
		
        function setContinuance()
        {	
            if ( document.getElementById("cb_continuance").checked)
            {
                document.getElementById("tr_continuance").style.display = "block";
                document.getElementById("tbl_continuance").style.display = "block";
			        
            }
            else
            {
                document.getElementById("tr_continuance").style.display = "none";
                document.getElementById("tbl_continuance").style.display = "none";
            }
        }
		
        function setFirm()
        {
		
            if ( document.getElementById("cb_outside").checked)
            {
                document.getElementById("ddl_FirmAbbreviation").disabled = false;
		          			        
            }
            else
            {
                document.getElementById("ddl_FirmAbbreviation").disabled = true;
		           
            }
		
        }
		
		
        //========================================================================//
		
		
        function EnableDisabeEmail()
        {
            if (document.frmgeneralinfo.chkEmail.checked == true )
            {
                $("#txtErrorMessage").text("Are you sure the client doesn�t have an email address? This information can be used to send trial notification letters and other related documents to clients. For Processing It without email address, click 'OK� or �CANCEL� to type in the email address.");
                $("#errorAlert").modal();
                //var chk1 = confirm ("Are you sure the client doesn�t have an email address? This information can be used to send trial notification letters and other related documents to clients. For Processing It without email address, click 'OK� or �CANCEL� to type in the email address.");
                if(chk1==true)
                {
                    document.frmgeneralinfo.txt_email.value = '';
                    document.frmgeneralinfo.txt_email.disabled = true;
                    return false;
                }
                else 
                {
                    document.frmgeneralinfo.chkEmail.checked = false;
                    return false;
                }
            }
            else
            {
		        
                document.frmgeneralinfo.chkEmail.checked = false;
                document.frmgeneralinfo.txt_email.disabled = false;
                return false;
		        
            }
		        
        }
		
        function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
        {
            //window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
            window.open ("../paperless/PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
            return false;
        }
		
		
		
        // IF OTHER OPTION IS SELECTED THAN SHOW THE TEXT BOX FOR DETAILS OF OHTERS......
	
        function ShowHideOtherDetail()
        {
            //a;           
            var td=document.getElementById("tdhidden");
            var txt=document.getElementById("txt_occupation");	//other		
            if(document.frmgeneralinfo.ddl_occupation.value=="16")	
            {					    
                td.style.display = 'block';
                txt.value ='';								
            }
            else
            {			   
                td.style.display ='none';			      	
            }
        }
		
        //In order to select TX when DL num is entered		
        function SelectState()
        {
            if(document.getElementById("txt_dlstr").value=="")
            {						
                var ddl_dlstate=document.getElementById("ddl_dlState");	
                ddl_dlstate.value=45;
            }
        }
		
        //added by kazim to validate priority
        // Valiate Open Service Ticket Flag
        function checkOpenServiceTicket()
        {				    
		  
            var id = '<%=Convert.ToString(ViewState["ValidateOSTflag"]) %>';
		    
        if ( id != "" && document.getElementById(id + "_ddl_priority") != null)
        {		          	          
            // Check Priority
            if ( document.getElementById(id + "_ddl_priority").value == "-1")//  .checked == false &&  document.getElementById(id + "_rblPriority_1").checked == false && document.getElementById(id + "_rblPriority_2").checked == false ) 
            {		          
                $("#txtErrorMessage").text("Please select priority for open service ticket.");
                $("#errorAlert").modal();
             //   alert("Please select priority for open service ticket.");
                return false;
            }
		         
        }
        if ( id != "")
        {		          	          
            // Check Category
            var ddl_cat = document.getElementById(id + "_ddlServiceTicketCategory");
            if(ddl_cat != null)
            {
                if ( ddl_cat.value == '0' ) 
                {		  
                    $("#txtErrorMessage").text("Please select category for open service ticket.");
                    $("#errorAlert").modal();
                   // alert("Please select category for open service ticket.");
                    return false;
                }
            }
		         
        }
        else
            return true;	    
		    
    }
		
		
    //Waqas Javed 5771 04/16/2009
    function CheckPreReqForCID()
    {
		    
		    
			
        if(document.frmgeneralinfo.txt_lname.value=='')
        {
            alert ("Please enter LastName.");
            document.frmgeneralinfo.txt_lname.focus(); 
            return(false);			   
        }
			
        if(document.frmgeneralinfo.txt_fname.value=='')
        {
            alert ("Please enter FirstName.");
            document.frmgeneralinfo.txt_fname.focus(); 
            return(false);			   
        }
			
        var mon =document.frmgeneralinfo.txt_dob_m.value;
        var day =document.frmgeneralinfo.txt_dob_d.value;
        var year=document.frmgeneralinfo.txt_dob_y.value;
			
        if(mon!="" && day!="" && year!="")
        {
            var dob=mon+"/"+day+"/"+year				
            if (!MMDDYYYYDate(dob))
            {			  
                document.frmgeneralinfo.txt_dob_m.focus(); 
                return(false);
            }			   
        }
        else
        {
            $("#txtErrorMessage").text("Please Enter DOB");
            $("#errorAlert").modal();
         //   alert("Please Enter DOB")			   
            document.frmgeneralinfo.txt_dob_m.focus(); 
            return(false);			  
        }
		
		 	
        var today=new Date();
        var dob=new Date();
        dob.setFullYear(year);
        dob.setMonth(mon-1);
        dob.setDate(day);
         
        if(dob>today)
        {
            $("#txtErrorMessage").text("Please Enter Correct Date of Birth, Date of Birth is not Greater than Current Date.");
            $("#errorAlert").modal();
           // alert("Please Enter Correct Date of Birth, Date of Birth is not Greater than Current Date.");
            document.getElementById ("txt_dob_d").focus();
            return false;
        }
		    
		    
        if(document.frmgeneralinfo.txt_lname.value != lbltxt_lname.innerHTML)
        {
            $("#txtErrorMessage").text("Last Name has been changed, Please update client information before CID Lookup");
            $("#errorAlert").modal();
          //  alert("Last Name has been changed, Please update client information before CID Lookup");
            document.frmgeneralinfo.txt_lname.focus();
            return false;
        }
		    
        if(document.frmgeneralinfo.txt_fname.value != lbltxt_fname.innerHTML)
        {
            $("#txtErrorMessage").text("First Name has been changed, Please update client information before CID Lookup");
            $("#errorAlert").modal();
          //  alert("First Name has been changed, Please update client information before CID Lookup");
            document.frmgeneralinfo.txt_fname.focus();
            return false;
        }
		    
		    
        if(document.frmgeneralinfo.txt_dob_m.value != lbltxt_dob_m.innerHTML)
        {
            $("#txtErrorMessage").text("DOB has been changed, Please update client information before CID Lookup");
            $("#errorAlert").modal();
           // alert("DOB has been changed, Please update client information before CID Lookup");
            document.frmgeneralinfo.txt_dob_m.focus();
            return false;
        }
			
        if(document.frmgeneralinfo.txt_dob_d.value != lbltxt_dob_d.innerHTML)
        {
            $("#txtErrorMessage").text("DOB has been changed, Please update client information before CID Lookup");
            $("#errorAlert").modal();
           // alert("DOB has been changed, Please update client information before CID Lookup");
            document.frmgeneralinfo.txt_dob_d.focus();
            return false;
        }
			
        if(document.frmgeneralinfo.txt_dob_y.value != lbltxt_dob_y.innerHTML)
        {
            $("#txtErrorMessage").text("DOB has been changed, Please update client information before CID Lookup");
            $("#errorAlert").modal();
           // alert("DOB has been changed, Please update client information before CID Lookup");
            document.frmgeneralinfo.txt_dob_y.focus();
            return false;
        }
			
        document.getElementById("tblCIDProg").style.display = 'block';
        document.getElementById("lnkContactID").style.display = 'none';
		    
    }
		
		
    function ValidateControls()
    {		  
        //Sabir Khan 5009 11/05/2008 set session for page refresh ...
        <%Session["TimeStamp"]=txtTimeStamp.Text;%>;
        //Comments Textboxes
	      
        var dateLenght = 0;
        var newLenght = 0;
		  
        //          newLenght = document.frmgeneralinfo.WCC_GeneralComments_txt_Comments.value.length
        //		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

        //		  if (document.frmgeneralinfo.WCC_GeneralComments_txt_Comments.value.length + document.getElementById("WCC_GeneralComments_lbl_comments").innerHTML.length > (5000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
        //		  {
        //		    alert("Sorry You cannot type in more than 5000 characters in General comments box")
        //			return false;		  
        //		  }
        //if (document.frmgeneralinfo.txt_settingnotes.value!=document.frmgeneralinfo.txt_oldset.value)
        //{
        //ozair 4846 11/14/2008 commented settign note check
        //          if (document.frmgeneralinfo.WCC_SettingComments_txt_Comments.style.display=="block")
        //          {
        //              newLenght = document.frmgeneralinfo.WCC_SettingComments_txt_Comments.value.length
        //		      if(newLenght > 0){dateLenght =  27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time
        //    		  
        //		      if (document.frmgeneralinfo.WCC_SettingComments_txt_Comments.value.length + document.getElementById("WCC_SettingComments_lbl_Comments").innerHTML.length > (100 - dateLenght) )
        //		      {
        //		       alert("Please Enter Setting Notes less than 100 characters")
        //			    return false;		  
        //		      }
        //		  }
        //}
        //Nasir 6098 08/18/2009 remove comments validation

        //Waqas 6895 11/02/2009 Fixed issue while live deployment. Continuance Comment is now at comments page.

        //		  if(document.getElementById("hf_continuanceflag").value=='1')
        //		  {
        //	          newLenght = document.frmgeneralinfo.WCC_ContinuanceComments_txt_Comments.value.length
        //	          if(newLenght > 0){dateLenght =  27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

        //	          if (document.frmgeneralinfo.WCC_ContinuanceComments_txt_Comments.value.length + document.getElementById("WCC_ContinuanceComments_lbl_Comments").innerHTML.length >2000 - dateLenght)
        //	          {
        //	            alert("Sorry You cannot type in more than 2000 characters in Continuance comments box")
        //		        return false;		  
        //	          }
        //		  }

        //Mohammad Ali 9996 01/12/2012 validation on SSN Enter numbers 0-9
        // Hafiz 10288 07/12/2012 commented the below section add for the  criminal
        if($get("hf_casetype").value!=="2"){
            var ssn = document.frmgeneralinfo.txt_SSN.value;
            if(ssn.trim().length > 0)
            {	
                var v = ssn.replace( /[^0-9]/g , "");
                if(ssn==v )
                {
                    if(ssn.length !=4)
                    {
                        $("#txtErrorMessage").text("Please enter Last 4 Digits of your SSN");
                        $("#errorAlert").modal();
                       // alert("Please enter Last 4 Digits of your SSN");	
                        document.frmgeneralinfo.txt_SSN.focus();
                        return false;
                    }	
                }
                else
                {
                    $("#txtErrorMessage").text("Please enter numbers 0-9");
                    $("#errorAlert").modal();
                   // alert("Please enter numbers 0-9");
                    document.frmgeneralinfo.txt_SSN.focus();
                    return false;
                }
            }
        }
        // End of 9996

        if(document.frmgeneralinfo.txt_fname.value=='')
        {
            alert ("Please enter FirstName.");
            document.frmgeneralinfo.txt_fname.focus(); 
            return(false);			   
        }
			
        if(document.frmgeneralinfo.txt_lname.value=='')
        {
            alert ("Please enter LastName.");
            document.frmgeneralinfo.txt_lname.focus(); 
            return(false);			   
        }
        //zeeshan
			
        var mon =document.frmgeneralinfo.txt_dob_m.value;
        var day =document.frmgeneralinfo.txt_dob_d.value;
        var year=document.frmgeneralinfo.txt_dob_y.value;
			
        if(mon!="" && day!="" && year!="")
        {
            var dob=mon+"/"+day+"/"+year				
            if (!MMDDYYYYDate(dob))
            {			  
                document.frmgeneralinfo.txt_dob_m.focus(); 
                return(false);
            }			   
        }
        else
        {
            $("#txtErrorMessage").text("Please Enter DOB");
            $("#errorAlert").modal();
           // alert("Please Enter DOB");			   
            document.frmgeneralinfo.txt_dob_m.focus(); 
            return(false);			  
        }
        //Added by kazim task id:2536
        //The code is written to resolve the problem that Dob should not be greater than Today Date. 
		 	
        var today=new Date();
        var dob=new Date();
        dob.setFullYear(year);
        dob.setMonth(mon-1);
        dob.setDate(day);
         
        if(dob>today)
        {
            $("#txtErrorMessage").text("Please Enter Correct Date of Birth, Date of Birth is not Greater than Current Date.");
            $("#errorAlert").modal();
           // alert("Please Enter Correct Date of Birth, Date of Birth is not Greater than Current Date.");
            document.getElementById ("txt_dob_d").focus();
            return false;
        }
		    
        //Validation for when nothing enter in DL textbox for no DL
        try
        {

            var dltxt = document.getElementById("txt_dlstr").value;
            var ddlDL = document.getElementById("ddl_dlState").value;
            if (dltxt == "")
            {
                if (document.getElementById("chk_NoLicense").checked == false)
                {
                    $("#txtErrorMessage").text("If Driving License No not available then checkmark the NO DL");
                    $("#errorAlert").modal();
                   // alert("If Driving License No not available then checkmark the NO DL");
                    document.getElementById("chk_NoLicense").focus();
                    return (false);
                }
            }
            if (dltxt != "")
            {
                if (ddlDL == 0)
                {
                    $("#txtErrorMessage").text("Please Specify DL from DropDown");
                    $("#errorAlert").modal();
                   // alert("Please Specify DL from DropDown");
                    document.getElementById("ddl_dlState").focus();
                    return (false);

                }
            }
            if (dltxt != "")
            {
                if (document.getElementById("chk_NoLicense").checked == true)
                {
                    $("#txtErrorMessage").text("PLease UnCheck No DL if you have DL Number");
                    $("#errorAlert").modal();
                   // alert("PLease UnCheck No DL if you have DL Number");
                    document.getElementById("chk_NoLicense").focus();
                    return (false);
                }

            }
		   
		  
            if(document.getElementById('rbtn_PR_Yes').checked == false && document.getElementById('rbtn_PR_No').checked == false && document.getElementById('rbtn_PR_NA').checked == false )
            {
                $("#txtErrorMessage").text("Please select whether the client in a US Citizen/Perm Resident");
                $("#errorAlert").modal();
               // alert('Please select whether the client in a US Citizen/Perm Resident');
                document.getElementById('rbtn_PR_Yes').focus();
                return(false);
            }
			
            var dlstate=document.frmgeneralinfo.ddl_dlState.value;
            var dlnum=document.frmgeneralinfo.txt_dlstr.value;
						
            /*if(dlstate=='0')
            {
               alert ("Please select DL State.");
               document.frmgeneralinfo.ddl_dlState.focus(); 
               return(false);			   
            }			
            //a;
            if (dlnum=="")
            {
                    if( dlstate!='55' && dlstate!='54')// && dlnum=="" )
                    {
                        alert ("Please enter DL Number .");
                        document.frmgeneralinfo.txt_dlstr.focus(); 
                        return(false);					
                    }				
            }*/			
			
            if(dlstate=='45'|| dlstate=='14') //should be numeric
            {
                if (isNaN(dlnum))
                {
                    alert ("Please Enter DL in Numeric");
                    document.frmgeneralinfo.txt_dlstr.focus();
                    return false;
                }					
            }
        }
        catch(err)
        {
            //Handle errors here
        }
        //////////////////////////////////////////////
			
			
        /*if(document.frmgeneralinfo.ddl_occupation.value=='0')
        {
           alert ("Please select Occupation.");
           document.frmgeneralinfo.ddl_occupation.focus(); 
           return(false);			   
        }*/
        //zee
        /*
        if(document.frmgeneralinfo.ddl_occupation.value=='Other')
        {
         var txt=document.getElementById("txt_occupation").value;	
         if (txt=="")
         {
           alert ("Please Specify Occupation.");
           document.frmgeneralinfo.txt_occupation.focus(); 
           return(false);			   
         }
        }
        */
        //Hafiz 10288 07/12/2012
        if($get("hf_casetype").value!=="2"){
            if(document.frmgeneralinfo.txt_heightft.value!='')
            {			
                if(isNaN(document.frmgeneralinfo.txt_heightft.value)==true || document.frmgeneralinfo.txt_heightft.value==0)
                {
                    alert ("Sorry, Invalid Height.");
                    document.frmgeneralinfo.txt_heightft.focus(); 
                    return(false);			   
                }
            }
            if(document.frmgeneralinfo.txt_heightin.value!='')
            {			
                var heightin=document.getElementById("txt_heightin").value;
                if (heightin>11)
                {//added by khalid bug 2531 7-1-08 
                    $("#txtErrorMessage").text("Please Enter Inches part less than 12");
                    $("#errorAlert").modal();
                   // alert("Please Enter Inches part less than 12");
                    return(false);
                }
                if(isNaN(document.frmgeneralinfo.txt_heightin.value)==true)
                {
                    $("#txtErrorMessage").text("Sorry, Invalid Height.");
                    $("#errorAlert").modal();
                   // alert ("Sorry, Invalid Height.");
                    document.frmgeneralinfo.txt_heightin.focus(); 
                    return(false);			   
                }
            }
            if(document.frmgeneralinfo.txt_weight.value!='')
            {			 
			
                if(isNaN(document.frmgeneralinfo.txt_weight.value)==true || document.frmgeneralinfo.txt_weight.value==0)
                {
                    $("#txtErrorMessage").text("Sorry, Invalid Weight.");
                    $("#errorAlert").modal();
                   // alert ("Sorry, Invalid Weight.");
                    document.frmgeneralinfo.txt_weight.focus(); 
                    return(false);			   
                }
            } 
        }

        /*
        if(document.frmgeneralinfo.txt_ssn.value!='')
        {			   
            if(isNaN(document.frmgeneralinfo.txt_ssn.value)==true)
            {
            alert ("Invalid Social Security Number.");
            document.frmgeneralinfo.txt_ssn.focus(); 
            return(false);			   
            }
        }
        */
        //a;
        // Sabir Khan 11509 12/18/2013 remove walk in flag
        //			if (document.frmgeneralinfo.rdbtn_Yes.checked == false && document.frmgeneralinfo.rdbtn_No.checked == false )
        //	        {
        //	            alert("Please select walk in client flag.");	
        //				document.frmgeneralinfo.rdbtn_Yes.focus();
        //				return false;
        //	        }
						
        if(document.frmgeneralinfo.chkb_addresscheck.checked==false &&  document.getElementById("hf_prevfirm").value=="3000")
        {
            $("#txtErrorMessage").text("Please verify the Address Check Box");
            $("#errorAlert").modal();
           // alert("Please verify the Address Check Box");	
            document.frmgeneralinfo.chkb_addresscheck.focus();
            return false;		
        }						
			
        if(document.frmgeneralinfo.txt_add1.value=='')
        {
            $("#txtErrorMessage").text("Please enter Address1, it must be less than or equal to 150 character.");
            $("#errorAlert").modal();
          //  alert ("Please enter Address1, it must be less than or equal to 150 character.");
            document.frmgeneralinfo.txt_add1.focus(); 
            return(false);			   
        }
			
			    
        if(document.getElementById("chk_aparmentno").checked==true)
        { 
            var add2 = document.getElementById("txt_add2").value;
            add2 = add2.toUpperCase();
            if(add2.length == 0)
            { 
                $("#txtErrorMessage").text("This is an Apartment address. Please make sure that you get the Apartment number from the client.");
                $("#errorAlert").modal();
               // alert("This is an Apartment address. Please make sure that you get the Apartment number from the client.");
                document.getElementById("txt_add2").focus();
                return false;
            }
        }
			
        if(document.frmgeneralinfo.txt_city.value=='')
        {
            $("#txtErrorMessage").text("Please enter City.");
            $("#errorAlert").modal();
           // alert ("Please enter City.");
            document.frmgeneralinfo.txt_city.focus(); 
            return(false);			   
        }
        if(document.frmgeneralinfo.ddl_state.selectedIndex=='0')
        {
            $("#txtErrorMessage").text("Please Select State.");
            $("#errorAlert").modal();
          //  alert ("Please Select State.");
            document.frmgeneralinfo.ddl_state.focus(); 
            return(false);			   
        }
        if(document.frmgeneralinfo.txt_zip.value=='')
        {
            $("#txtErrorMessage").text("Please specify ZipCode.");
            $("#errorAlert").modal();
           // alert ("Please specify ZipCode.");
            document.frmgeneralinfo.txt_zip.focus(); 
            return(false);			   
        }
			
        if (document.frmgeneralinfo.chkEmail.checked == true )
            document.frmgeneralinfo.txt_email.value = "";
			
        if(document.frmgeneralinfo.txt_email.value!="" )
        {			
            if( isEmail(document.frmgeneralinfo.txt_email.value)== false)
            {
                $("#txtErrorMessage").text("Please enter Email Address in Correct format.");
                $("#errorAlert").modal();
               // alert ("Please enter Email Address in Correct format.");
                document.frmgeneralinfo.txt_email.focus(); 
                return false;			   
            }
        }	
        else
        {
            if (document.frmgeneralinfo.chkEmail.checked == false) 			
            {
                $("#txtErrorMessage").text("If email address is not available then" + '\n' + "Please check mark the 'Email Address not available' option.");
                $("#errorAlert").modal();
               // alert ("If email address is not available then" + '\n' + "Please check mark the 'Email Address not available' option.");
                document.frmgeneralinfo.chkEmail.focus(); 
                return false;			   
            }
        }		
		   
        //		    var chkFirmChange = document.getElementById('<%= Convert.ToString(ViewState["ddl_FirmAbbreviation"]) %>_ddl_FirmAbbreviation');
        //		    
        //		    if ( chkFirmChange !=null)
        //		    {
        //		        if ( chkFirmChange.value != document.getElementById("hf_prevfirm").value) 		        	    
        //		        {
        //		            document.getElementById("hf_checkfirm").value = "1";
        //		        }
        //		    }		    		    		    	    		  
		    
        //		    if (document.getElementById("hf_checkfirm").value == "1")
        //		    {
        //added by Ozair for Continuance Modifications
        if ( checkConSD() == false)
        {
            return false;		    
        }
        //END
		       
        if ( checkFirm() == false)
            return false;	
		       
        if( checkOpenServiceTicket()==false)
        {
            return false;
        }    
        //		    }
		  
		  
        /*if ( document.getElementById("ddl_continuancedate").style.display !="none")
        {
            
            if ( document.getElementById("ddl_continuancedate").value == null)
             {
             alert("You does not have a valid continuance date. Please unchecked the continuance check box.");
             return false;
             }
        }*/
		   
        /*
       // Check for Continuance Flag
         var activeflag =  document.getElementById("hf_activeflag").value;	
         var contflagprev = document.getElementById("hf_continuanceflag").value;
         var contflagcur = document.getElementById("cb_continuance").checked;
         // alert("Active Flag :" + activeflag + " CFlagP :" + contflagprev + "CFlagC :" + contflagcur);
                 
         if ( activeflag == "1" && contflagprev == "1" &&  contflagcur==false)
         {
             alert("You Cannot change continuance flag after payment.");
             document.getElementById("cb_continuance").checked = !(document.getElementById("cb_continuance").checked);
             setContinuance();
             return false;
         }
         
         if ( contflagprev == "0" && contflagcur == true)
         {
             var chk =  confirm("A $50 fee will automatically be added ( and locked) to this client's fee.Press ok to continue");
             document.getElementById("cb_continuance").checked = chk;
             setContinuance();
             return chk;
             
         }*/
		 
		 
		 
		 
        //Checking Bond flag for more validation
        //  alert(document.getElementById("lbl_bondflag").innerHTML);
		   
        if(document.getElementById("lbl_bondflag").innerHTML==1)		
        {
            if(CheckForBondFlag()==false)
                return false;
            // Noufil 7938 07/02/2010 commented code because return true call at the end of function.
            // return true;
        }
        else
        {			
            //Checking ph numbers
            var c11=document.frmgeneralinfo.txt_cc11.value;
            var c12=document.frmgeneralinfo.txt_cc12.value;
            var c13=document.frmgeneralinfo.txt_cc13.value;		
            var c1=c11.length+c12.length+c13.length;
            var c21=document.frmgeneralinfo.txt_cc21.value;
            var c22=document.frmgeneralinfo.txt_cc22.value;
            var c23=document.frmgeneralinfo.txt_cc23.value;		
            var c2=c21.length+c22.length+c23.length;
            var c31=document.frmgeneralinfo.txt_cc31.value;
            var c32=document.frmgeneralinfo.txt_cc32.value;
            var c33=document.frmgeneralinfo.txt_cc33.value;		
            var c3=c31.length+c32.length+c33.length;		
			
            //Checking if numeric or not zeeshan
            //a;
			
            if (  isNaN(document.frmgeneralinfo.txt_cc11.value)==true || isNaN(document.frmgeneralinfo.txt_cc12.value)==true ||isNaN(document.frmgeneralinfo.txt_cc13.value)==true)// || isNaN(document.frmgeneralinfo.txt_cc14.value) == true )	
            {
                return AlertForValidation(document.frmgeneralinfo.txt_cc11, "Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");	                
            }
            if (  isNaN(document.frmgeneralinfo.txt_cc21.value)==true || isNaN(document.frmgeneralinfo.txt_cc22.value)==true ||isNaN(document.frmgeneralinfo.txt_cc23.value)==true)// || isNaN(document.frmgeneralinfo.txt_cc24.value) == true )	
            {
                return AlertForValidation(document.frmgeneralinfo.txt_cc21, "Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");	                
            }
            if (  isNaN(document.frmgeneralinfo.txt_cc31.value)==true || isNaN(document.frmgeneralinfo.txt_cc32.value)==true ||isNaN(document.frmgeneralinfo.txt_cc33.value)==true)// || isNaN(document.frmgeneralinfo.txt_cc34.value) == true )	
            {
                return AlertForValidation(document.frmgeneralinfo.txt_cc31, "Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");	                
            }			
            //
            //Farrukh 9925 11/29/2011 Changed validation for at least 2 contact numbers
            if((c1==0) && (c2==0) && (c3==0))
            {			
                return AlertForValidation(document.frmgeneralinfo.txt_cc11, "Please Enter at least 2 contact Numbers for this client."); 
            }
            else if((c1==0) && (c2==0))
            {
                return AlertForValidation(document.frmgeneralinfo.txt_cc11, "Please Enter at least 2 contact Numbers for this client."); 
            }
            else if((c1==0) && (c3==0))
            {
                return AlertForValidation(document.frmgeneralinfo.txt_cc11, "Please Enter at least 2 contact Numbers for this client."); 
            }
            else if((c2==0) && (c3==0))
            {
                return AlertForValidation(document.frmgeneralinfo.txt_cc11, "Please Enter at least 2 contact Numbers for this client."); 
            }
            else
            {				
                if((c1==10) ||(c2==10)||(c3==10))
                {
                    //Farrukh 9925 11/30/2011 Code redundancy removed
                    if(c1==10)
                    {										
                        if(document.frmgeneralinfo.ddl_contact1.selectedIndex=='0')
                        {
                            return AlertForValidation(document.frmgeneralinfo.ddl_contact1, "Please specify Contact Type.");						
                        }			
					
                    }
                    if(c2==10)
                    {				
                        if(document.frmgeneralinfo.ddl_contact2.selectedIndex=='0')
                        {
                            return AlertForValidation(document.frmgeneralinfo.ddl_contact2, "Please specify Contact Type.");
                        }				
                    }
                    if(c3==10)
                    {				
                        if(document.frmgeneralinfo.ddl_contact3.selectedIndex=='0')
                        {
                            return AlertForValidation(document.frmgeneralinfo.ddl_contact3, "Please specify Contact Type.");
                        }				
                    }		               		   
                }
                else
                {	 
                    alert ("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");			  
                    return false;	
                }
            }
            if( (c1>0&&c1<10) || (c2>0&&c2<10) || (c3>0&&c3<10) )
            {
                $("#txtErrorMessage").text("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");
                $("#errorAlert").modal();
               // alert("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");				
                return false;
            }	
            // Noufil 6177 08/06/2009 Show confirm box if no check box is checked and "Mobile" is selected from drop down
            if( document.frmgeneralinfo.ddl_contact1.value == '4' && document.getElementById("chk_SmsRequired1").checked == false)			
                var allowsmsflag1 = true;
			    
            if( document.frmgeneralinfo.ddl_contact2.value == '4' && document.getElementById("chk_SmsRequired2").checked == false)
                var allowsmsflag2 = true;
			    
            if( document.frmgeneralinfo.ddl_contact3.value == '4' && document.getElementById("chk_SmsRequired3").checked == false)
                var allowsmsflag3 = true;
			
            if (allowsmsflag1 || allowsmsflag2 || allowsmsflag3)
            {
                if (confirm("Would you like to receive reminder text messages with your court date?"))
                {
                    if (allowsmsflag1)
                        document.getElementById("chk_SmsRequired1").checked = true;
                    if (allowsmsflag2)
                        document.getElementById("chk_SmsRequired2").checked = true;
                    if (allowsmsflag3)
                        document.getElementById("chk_SmsRequired3").checked = true;			        
                }
            }
        }		  
		
        //Ozair 6766 03/25/2010 Hide consultation section.
        if(document.getElementById("tr_consultation").style.display!="none")
        {             
            if(document.getElementById('rbtn_FreeCon_Yes').checked==false && document.getElementById('rbtn_FreeCon_No').checked==false)
            {
                //Ozair 6766 04/23/2010 typo error resolved 
                $("#txtErrorMessage").text("Please select whether the client wants a consultation or not");
                $("#errorAlert").modal();
              //  alert('Please select whether the client wants a consultation or not');
                document.getElementById('rbtn_FreeCon_Yes').focus();
                return(false);
            }
           
            if(document.getElementById('rbtn_FreeCon_Yes').checked == true)
            {
                //var comm = document.getElementById('txt_ConsultationComments');
                //var comm = document.getElementById('WCC_ConsultationComments_txt_Comments')
                if(document.getElementById('WCC_ConsultationComments_txt_Comments').value + document.getElementById("WCC_ConsultationComments_lbl_comments").innerHTML == '')
                {
                    $("#txtErrorMessage").text("Please enter some comments for consultation");
                    $("#errorAlert").modal();
                   // alert('Please enter some comments for consultation');
                    return(false);
                }
            }
        }
        //            // Afaq 7937 06/29/2010 alert for wrong email address.            
        //            var imgfalse = document.getElementById("emailVerifier_imageFalse");
        //            // Afaq7937 07/06/2010 dispaly alert if flase image display and email text box is not empty.
        //            if (imgfalse != null && imgfalse.style.display == "" && document.frmgeneralinfo.txt_email.value != "")
        //                alert("Please Verify Email Address from client");


        //Hafiz 10288 07/18/2012 validations for Post Hire question
        var divSplit=$get("divClient").innerHTML.split('~');
            
        // if ($get("divClient").innerHTML !== "0") {
        if(divSplit.length >1){
            if (divSplit[0] !== "0" && (divSplit[1] === "2")) { //for criminal
                return ALRValidators();
            }
        }
    		            

    }
		
		
		
    function CheckForBondFlag()
    {	
        if(document.frmgeneralinfo.ddl_gender.value=='--Choose--')
        {
            $("#txtErrorMessage").text("Please select Gender");
            $("#errorAlert").modal();
           // alert ("Please select Gender.");
            document.frmgeneralinfo.ddl_gender.focus(); 
            return(false);			   
        }
			
        if(document.frmgeneralinfo.txt_race.value=='')
        {
            $("#txtErrorMessage").text("Please enter Race.");
            $("#errorAlert").modal();
          //  alert ("Please enter Race.");
            document.frmgeneralinfo.txt_race.focus(); 
            return(false);			   
        }		
        //Hafiz 10288 07/12/2012
			
        var divCriminal=$get("divClient").innerHTML.split('~');
                   
        if(divCriminal.length >1){
            if(divCriminal[1] !== "2" ){
          
                if(document.frmgeneralinfo.txt_heightft.value=='')
                {
                    alert ("Please specify Height in Feet.");
                    document.frmgeneralinfo.txt_heightft.focus(); 
                    return(false);			   
                }
                if(isNaN(document.frmgeneralinfo.txt_heightft.value)==true || document.frmgeneralinfo.txt_heightft.value==0)
                {
                    alert ("Sorry, Invalid Height.");
                    document.frmgeneralinfo.txt_heightft.focus(); 
                    return(false);			   
                }
						
                if(document.frmgeneralinfo.txt_heightin.value=='')
                {
                    alert ("Please specify Height in Inches.");
                    document.frmgeneralinfo.txt_heightin.focus(); 
                    return(false);			   
                }
                if(isNaN(document.frmgeneralinfo.txt_heightin.value)==true)
                {
                    alert ("Sorry, Invalid Height.");
                    document.frmgeneralinfo.txt_heightin.focus(); 
                    return(false);			   
                }
			
                if(document.frmgeneralinfo.txt_weight.value=='')
                {
                    alert ("Please specify Weight.");
                    document.frmgeneralinfo.txt_weight.focus(); 
                    return(false);			   
                }
		
		
                if(isNaN(document.frmgeneralinfo.txt_weight.value)==true || document.frmgeneralinfo.txt_weight.value==0 )
                {
                    alert ("Sorry, Invalid Weight.");
                    document.frmgeneralinfo.txt_weight.focus(); 
                    return(false);			   
                }
			
                if(document.frmgeneralinfo.ddl_haircol.selectedIndex=='0')
                {
                    alert ("Please select HairColor.");
                    document.frmgeneralinfo.ddl_haircol.focus(); 
                    return(false);			   
                }
			
                if(document.frmgeneralinfo.ddl_eyecol.selectedIndex=='0')
                {
                    alert ("Please select EyeColor.");
                    document.frmgeneralinfo.ddl_eyecol.focus(); 
                    return(false);			   
                }
            }
        }	
			
        /*
        if(document.frmgeneralinfo.txt_ssn.value=='')
        {
           alert ("Please specify Social Security Number.");
           document.frmgeneralinfo.txt_ssn.focus(); 
           return(false);			   
        }	  
        if(isNaN(document.frmgeneralinfo.txt_ssn.value)==true)
        {
           alert ("Invalid Social Security Number.");
           document.frmgeneralinfo.txt_ssn.focus(); 
           return(false);			   
        }
        */
        //Mandatory 3 ph# numbers
			
        //Checking ph numbers
        var c11=document.frmgeneralinfo.txt_cc11.value;
        var c12=document.frmgeneralinfo.txt_cc12.value;
        var c13=document.frmgeneralinfo.txt_cc13.value;		
        var c1=c11.length+c12.length+c13.length;
        var c21=document.frmgeneralinfo.txt_cc21.value;
        var c22=document.frmgeneralinfo.txt_cc22.value;
        var c23=document.frmgeneralinfo.txt_cc23.value;		
        var c2=c21.length+c22.length+c23.length;
        var c31=document.frmgeneralinfo.txt_cc31.value;
        var c32=document.frmgeneralinfo.txt_cc32.value;
        var c33=document.frmgeneralinfo.txt_cc33.value;		
        var c3=c31.length+c32.length+c33.length;				
			
        if((c1==0) &&(c2==0)&&(c3==0))
        {
            $("#txtErrorMessage").text("Please Enter at least 3 contact Numbers for this client.");
            $("#errorAlert").modal();
           // alert("Please Enter at least 3 contact Numbers for this client.");
            document.frmgeneralinfo.txt_cc11.focus;				
            return false;			
        }
        else
        {		
            //checking for numeric
            if (  isNaN(document.frmgeneralinfo.txt_cc11.value)==true || isNaN(document.frmgeneralinfo.txt_cc12.value)==true ||isNaN(document.frmgeneralinfo.txt_cc13.value)==true)// || isNaN(document.frmgeneralinfo.txt_cc14.value) == true )	
            {
                $("#txtErrorMessage").text("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");
                $("#errorAlert").modal();
              //  alert ("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");			  
                document.frmgeneralinfo.txt_cc11.focus();
                return false;	
            }
            if (  isNaN(document.frmgeneralinfo.txt_cc21.value)==true || isNaN(document.frmgeneralinfo.txt_cc22.value)==true ||isNaN(document.frmgeneralinfo.txt_cc23.value)==true)// || isNaN(document.frmgeneralinfo.txt_cc24.value) == true )	
            {
                $("#txtErrorMessage").text("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");
                $("#errorAlert").modal();
             //   alert ("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");			  
                document.frmgeneralinfo.txt_cc21.focus();
                return false;	
            }
            if (  isNaN(document.frmgeneralinfo.txt_cc31.value)==true || isNaN(document.frmgeneralinfo.txt_cc32.value)==true ||isNaN(document.frmgeneralinfo.txt_cc33.value)==true)// || isNaN(document.frmgeneralinfo.txt_cc34.value) == true )	
            {
                $("#txtErrorMessage").text("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");
                $("#errorAlert").modal();
               // alert ("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");			  
                document.frmgeneralinfo.txt_cc21.focus();
                return false;	
            }
			  
			  	
            /////////////////////////////////// Entered Complete number
            if((c1==10)&&(c2==10)&&(c3==10))
            {									
                if(document.frmgeneralinfo.ddl_contact1.selectedIndex=='0')
                {
                    $("#txtErrorMessage").text("Please specify Contact Type.");
                    $("#errorAlert").modal();
                  //  alert ("Please specify Contact Type.");
                    document.frmgeneralinfo.ddl_contact1.focus(); 
                    return(false);			   
                }						
							
                if(document.frmgeneralinfo.ddl_contact2.selectedIndex=='0')
                {
                    $("#txtErrorMessage").text("Please specify Contact Type.");
                    $("#errorAlert").modal();
                  //  alert ("Please specify Contact Type.");
                    document.frmgeneralinfo.ddl_contact2.focus(); 
                    return(false);			   
                }	
			
                if(document.frmgeneralinfo.ddl_contact3.selectedIndex=='0')
                {
                    $("#txtErrorMessage").text("Please specify Contact Type.");
                    $("#errorAlert").modal();
                    //alert ("Please specify Contact Type.");
                    document.frmgeneralinfo.ddl_contact3.focus(); 
                    return(false);		
                }			
            }
            else
            {
                if( (c1>0&&c1<10) || (c2>0&&c2<10) || (c3>0&&c3<10) )
                {
                    $("#txtErrorMessage").text("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");
                    $("#errorAlert").modal();
                   // alert("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");				
                    return false;
                }
                else
                {
                    $("#txtErrorMessage").text("Please Enter at least 3 contact Numbers for this client.");
                    $("#errorAlert").modal();
                 //   alert("Please Enter at least 3 contact Numbers for this client.");		  			   
                    return false;
                }				  			 
            }		    		
        }
        // Ozair 5000 11/04/2008 Javascript for making consultation required or not option mandatory
        if(document.getElementById('rbtn_FreeCon_Yes').checked==false && document.getElementById('rbtn_FreeCon_No').checked==false)
        {
            //Ozair 6766 04/23/2010 typo error resolved
            $("#txtErrorMessage").text("Please select whether the client wants a consultation or not");
            $("#errorAlert").modal();
         //   alert('Please select whether the client wants a consultation or not');
            document.getElementById('rbtn_FreeCon_Yes').focus();
            return(false);
        }
		   
		   
        // Noufil 5000 10/24/2008 Javascript for making consultation comments mandatory on yes check
        if(document.getElementById('rbtn_FreeCon_Yes').checked == true)
        { 
            // Noufil 7937 07/02/2010 Check null value of Comments textbox and label.
            var comments = document.getElementById('WCC_ConsultationComments_txt_Comments');
            var label = document.getElementById("WCC_ConsultationComments_lbl_comments");
            // Noufil 7937 07/02/2010 Show alert message if comments and label are available.
            if (comments!=null && label !=null)
            {
                if(document.getElementById('WCC_ConsultationComments_txt_Comments').value + document.getElementById("WCC_ConsultationComments_lbl_comments").innerHTML == '')
                {               
                    $("#txtErrorMessage").text("Please enter some comments for consultation");
                    $("#errorAlert").modal();
                 //   alert('Please enter some comments for consultation');
                    return(false);
                }
            }
        }	 
    }
		
    // Noufil 5884 07/02/2009 Show hide Sms Option checkbox
    function Check(id)
    {		
        var selectedvalue= document.getElementById(id).value;
		     
        if (id == "ddl_contact1")
        {
            if (selectedvalue == "4")
                document.getElementById("td_chk_SMS1").style.display="block";
            else
            {
                document.getElementById("chk_SmsRequired1").checked =false;
                document.getElementById("td_chk_SMS1").style.display="none";
            }
        }
		        
        if (id == "ddl_contact2")
        {
            if (selectedvalue == "4")
                document.getElementById("td_chk_SMS2").style.display="block";
            else
            {
                document.getElementById("chk_SmsRequired2").checked =false;
                document.getElementById("td_chk_SMS2").style.display="none";
            }
        }
		    		       
        if (id == "ddl_contact3")
        {
            if (selectedvalue == "4")		        
                document.getElementById("td_chk_SMS3").style.display="block";
            else
            {
                document.getElementById("chk_SmsRequired3").checked =false;
                document.getElementById("td_chk_SMS3").style.display="none";
            }
        }
		        
        return false;
    }
		
    // Noufil 6766 02/10/2010 Show modal popup
    function ShowModalPopup(popupId,radiobuttonid)
    {
        var id = document.getElementById(radiobuttonid);
        if (id !=null)
            var ischeck = id.checked
        if (id ==null || (id !=null && id.type == "radio" && ischeck))
        {
            var modalPopupBehavior = $find(popupId);
            modalPopupBehavior.show();
            if (id !=null)
                id.checked = ischeck;
        }
        return false;		
    }	
		
    //Hafiz 10288 07/18/2102
    function ShowHideObserveOfficer()
    {
        if(document.getElementById('rBtnArrOffObsOffYes').checked == true)
        {
            document.getElementById('tr_ObservingOfficer').style.display='none';
		        
            document.getElementById('txtALROBSOfficerName').value = '';
            document.getElementById('txtALROBSOfficerBadgeNumber').value = '';
            document.getElementById('txtALROBSPrecinct').value = '';
            document.getElementById('txtALROBSAddress').value = '';
            document.getElementById('txtALROBSCity').value = '';
            document.getElementById('ddl_ALROBSState').selectedIndex = 0;
            document.getElementById('txtALROBSZip').value = ''	;	        
            document.getElementById('txt_OBSAttacc11').value = '';
            document.getElementById('txt_OBSAttcc12').value = '';
            document.getElementById('txt_OBSAttcc13').value = '';
            document.getElementById('txt_OBSAttcc14').value = '';
            document.getElementById('txtALROBSArrestingAgency').value = '';
            document.getElementById('txtALROBSOfficerMileage').value = '';
        }
		    
        if(document.getElementById('rBtnArrOffObsOffNo').checked == true)
        {
            document.getElementById('tr_ObservingOfficer').style.display='table-row';
        }
		    
        else
        {
            document.getElementById('tr_ObservingOfficer').style.display='none';
		        
            document.getElementById('txtALROBSOfficerName').value = '';
            document.getElementById('txtALROBSOfficerBadgeNumber').value = '';
            document.getElementById('txtALROBSPrecinct').value = '';
            document.getElementById('txtALROBSAddress').value = '';
            document.getElementById('txtALROBSCity').value = '';
            document.getElementById('ddl_ALROBSState').selectedIndex = 0;
            document.getElementById('txtALROBSZip').value = ''	;	        
            document.getElementById('txt_OBSAttacc11').value = '';
            document.getElementById('txt_OBSAttcc12').value = '';
            document.getElementById('txt_OBSAttcc13').value = '';
            document.getElementById('txt_OBSAttcc14').value = '';
            document.getElementById('txtALROBSArrestingAgency').value = '';
            document.getElementById('txtALROBSOfficerMileage').value = '';
        }
		    
    }
		
    function ShowBTOName() {
        if (document.getElementById('rblBTOName').checked) {
            document.getElementById('txtBTOName').disabled = false;

        }
        else {
            document.getElementById('txtBTOName').disabled = true;

        }

    }

    function ShowBTOObserving() {
        if (document.getElementById('rblBTOName').checked) {
            document.getElementById('txtBTOName').disabled = false;

        }
        else {
            document.getElementById('txtBTOName').disabled = true;
            document.getElementById('txtBTOName').value = '';

        }

    }

    function ShowBTOArresting() {
        if (document.getElementById('rblBTOName').checked) {
            document.getElementById('txtBTOName').disabled = false;

        }
        else {
            document.getElementById('txtBTOName').disabled = true;
            document.getElementById('txtBTOName').value = '';
        }

    }
    function ALRValidators() {
        debugger;
        var inValidChars = "0123456789";
        var flag=false;
        //Check for Officer name
        if ($get('tr_ALRRequiredWhy').style.display === "none") {
            var Offname = document.getElementById('txtALROfficerName').value;
            for (j = 0; j < Offname.length; j++) {
                flag=true;
                Char = Offname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtALROfficerName'), "Officer name can not have any numbers");
                }
            }


            //Waqas 5864 07/17/2009 Check for City
            var OffCity = document.getElementById('txtALRCity').value;
            for (j = 0; j < OffCity.length; j++) {
                flag=true;
                Char = OffCity.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtALRCity'), "Officer's City can not have any numbers");
                }
            }



            //Edit by Hafiz
            if (isNaN($get('txtALRZip').value)) {
                flag=true;
                return AlertForValidation($get('txtALRZip'), "Officer's Zip Code must be numeric.");
            }

            //Waqas 5864 07/30/2009 Check for Zip
            //Waqas 6342 08/12/2009 ALR required
            if (frmgeneralinfo.ddl_ALRState.value == "") {
                flag=true;
                return AlertForValidation(frmgeneralinfo.ddl_ALRState, "Please specify Officer's state");
            }

            // Waqas 5864 07/17/2009 Contact No Check
            var intphonenum1 = frmgeneralinfo.txt_Attacc11.value;
            var intphonenum2 = frmgeneralinfo.txt_Attcc12.value;
            var intphonenum3 = frmgeneralinfo.txt_Attcc13.value;
            var intphonenum4 = frmgeneralinfo.txt_Attcc14.value;

            if ((isNaN(intphonenum1) == true)) {
                flag=true;
                return AlertForValidation(frmgeneralinfo.txt_Attacc11, "Invalid Phone Number. Please don't use any dashes or space");
            }
            if ((isNaN(intphonenum2) == true)) {
                flag=true;
                return AlertForValidation(frmgeneralinfo.txt_Attcc12, "Invalid Phone Number. Please don't use any dashes or space");
            }
            if ((isNaN(intphonenum3) == true)) {
                flag=true;
                return AlertForValidation(frmgeneralinfo.txt_Attcc13, "Invalid Phone Number. Please don't use any dashes or space");
            }

            //Waqas 5864 07/17/2009 Check for Arresting Agency name
            var ArrestingAgencyname = document.getElementById('txtALRArrestingAgency').value;
            for (j = 0; j < ArrestingAgencyname.length; j++) {
                flag=true;
                Char = ArrestingAgencyname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtALRArrestingAgency'), "Arresting agency name can not have any numbers");
                }
            }

            if (isNaN(frmgeneralinfo.txtALROfficerMileage.value)) {
                flag=true;
                return AlertForValidation(frmgeneralinfo.txtALROfficerMileage, "Officer mileage must be numeric.");
            }
        
            if (document.getElementById('rBtnArrOffObsOffYes').checked == true || document.getElementById('rBtnArrOffObsOffNo').checked == true) {
                //return AlertForValidation(frmgeneralinfo.rBtnArrOffObsOffYes, "Please specify 'Is the Arresting Officer the Observing Officer? ' (Yes/No)");
                flag=true;
            }
            //Asad Ali 8153 remove validation b/c this ALR hearing Required Question is Moved question move in Pre hire section    
            if (flag ===true && document.getElementById('rBtnArrOffObsOffYes').checked == false && document.getElementById('rBtnArrOffObsOffNo').checked == false) {
                return AlertForValidation(frmgeneralinfo.rBtnArrOffObsOffYes, "Please specify 'Is the Arresting Officer the Observing Officer? ' (Yes/No)");
            }

            //Asad Ali 7991 07/14/2010 Req#4 If "Is the Arresting Officer the Observing Officer" is set to No then we need to enter "Observing Officer Name".
            if (document.getElementById('rBtnArrOffObsOffNo').checked == true && frmgeneralinfo.txtALROBSOfficerName.value == "" && document.getElementById('rBtnArrOffObsOffNo').checked == true) {
                return AlertForValidation(frmgeneralinfo.txtALROBSOfficerName, "Please specify Observing Officer Name");
            }
            if (document.getElementById('rbBTOArresting').checked == true && document.getElementById('txtALROfficerName').value == '') {
                return AlertForValidation(frmgeneralinfo.txtALROfficerName, "First specify Arresting Officer Name");
            }

            if (document.getElementById('rBtnArrOffObsOffYes').checked == true && document.getElementById('txtALROfficerName').value == '') {
                return AlertForValidation(frmgeneralinfo.txtALROfficerName, "First specify Arresting Officer Name");
            }


            //Waqas 6342 08/12/2009 check for observing officer.
            var OBSOffname = document.getElementById('txtALROBSOfficerName').value;
            for (j = 0; j < OBSOffname.length; j++) {
                Char = OBSOffname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtALROBSOfficerName'), "Observing officer name can not have any numbers");
                }
            }


            var OBSOffCity = document.getElementById('txtALROBSCity').value;
            for (j = 0; j < OBSOffCity.length; j++) {
                Char = OBSOffCity.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtALROBSCity'), "Observing officer's city can not have any numbers");
                }
            }



            if (isNaN(frmgeneralinfo.txtALROBSZip.value)) {
                return AlertForValidation(frmgeneralinfo.txtALROBSZip, "Observing officer's zip code must be numeric.");
            }

            if (frmgeneralinfo.ddl_ALROBSState.value == "") {
                return AlertForValidation(frmgeneralinfo.ddl_ALROBSState, "Please specify observing officer's state");
            }

            var intOBSphonenum1 = frmgeneralinfo.txt_OBSAttacc11.value;
            var intOBSphonenum2 = frmgeneralinfo.txt_OBSAttcc12.value;
            var intOBSphonenum3 = frmgeneralinfo.txt_OBSAttcc13.value;
            var intOBSphonenum4 = frmgeneralinfo.txt_OBSAttcc14.value;

            if ((isNaN(intOBSphonenum1) == true)) {
                return AlertForValidation(frmgeneralinfo.txt_OBSAttacc11, "Invalid Phone Number. Please don't use any dashes or space");
            }
            if ((isNaN(intOBSphonenum2) == true)) {
                return AlertForValidation(frmgeneralinfo.txt_OBSAttcc12, "Invalid Phone Number. Please don't use any dashes or space");
            }
            if ((isNaN(intOBSphonenum3) == true)) {
                return AlertForValidation(frmgeneralinfo.txt_OBSAttcc13, "Invalid Phone Number. Please don't use any dashes or space");
            }


            var OBSArrestingAgencyname = document.getElementById('txtALROBSArrestingAgency').value;
            for (j = 0; j < OBSArrestingAgencyname.length; j++) {
                Char = OBSArrestingAgencyname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtALROBSArrestingAgency'), "Observing arresting agency name can not have any numbers");
                }
            }


            if (isNaN(frmgeneralinfo.txtALROBSOfficerMileage.value)) {
                return AlertForValidation(frmgeneralinfo.txtALROBSOfficerMileage, "Observing officer mileage must be numeric.");
            }
        }
        //Intoxilyzer Result Testing
        if (document.getElementById('tr_IntoxilyzerResults').style.display == "table-row" && $get('tr_ALRRequiredWhy').style.display!=="table-row") {
            if ( document.getElementById('rbtn_IntoxResultPass').checked == true || document.getElementById('rbtn_IntoxResultFail').checked == true) {
                //return AlertForValidation(frmgeneralinfo.rbtn_IntoxResultPass, "Please specify Intoxilyzer Results (Pass/Fail)");
                flag=true;
            }
            //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency 
            if ( flag===true && document.getElementById('rbtn_IntoxResultPass').checked == false && document.getElementById('rbtn_IntoxResultFail').checked == false) {
                return AlertForValidation(frmgeneralinfo.rbtn_IntoxResultPass, "Please specify Intoxilyzer Results (Pass/Fail)");
            }
        

            if (document.getElementById('rblBTOName').checked == true && document.getElementById('txtBTOName').value == '') {
                return AlertForValidation(frmgeneralinfo.txtBTOName, "Please Enter BTO Name");
            }

            var BTOname = document.getElementById('txtBTOName').value;
            for (j = 0; j < BTOname.length; j++) {
                Char = BTOname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtBTOName'), "BTO name can not have any numbers");
                }
            }


            if (document.getElementById('rbBTOArresting').checked == true && document.getElementById('txtALROfficerName').value == '') {
                return AlertForValidation(frmgeneralinfo.rbBTOArresting, "First specify Arresting Officer Name");
            }
            if (document.getElementById('rBtnArrOffObsOffYes').checked == true && document.getElementById('txtALROfficerName').value == '') {
                return AlertForValidation(frmgeneralinfo.rbBTOArresting, "First specify Arresting Officer Name");
            }
            if (document.getElementById('rbBTOObserving').checked == true && document.getElementById('txtALROBSOfficerName').value == '' && document.getElementById('rBtnArrOffObsOffNo').checked == true) {
                return AlertForValidation(frmgeneralinfo.rbBTOObserving, "First specify Observing Officer Name");
            }

            //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency
            if (flag===true && document.getElementById('txtBTSFirstName').value == '') {
                return AlertForValidation(frmgeneralinfo.txtBTSFirstName, "Please specify BTS First Name");
            }

            var BTSFname = document.getElementById('txtBTSFirstName').value;
            for (j = 0; j < BTSFname.length; j++) {
                Char = BTSFname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtBTSFirstName'), "BTS first name can not have any numbers");
                }
            }
            //Asad Ali 8153 09/09/2010 AlR Hearing Question moved in pre Hire Section so remove dependency
            if (flag===true && document.getElementById('txtBTSLastName').value == '' ) {
                return AlertForValidation(frmgeneralinfo.txtBTSLastName, "Please specify BTS Last Name");
            }

            var BTSLname = document.getElementById('txtBTSLastName').value;
            for (j = 0; j < BTSLname.length; j++) {
                Char = BTSLname.charAt(j);
                if (inValidChars.indexOf(Char) > -1) {
                    return AlertForValidation(document.getElementById('txtBTSLastName'), "BTS last name can not have any numbers");
                }
            }
        }
    }

    </script>

    <style type="text/css">
        .style1
        {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            width: 27%;
            padding-left: 5px;
            background-color: #EFF4FB;
        }
        .style2
        {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            height: 20px;
            width: 27%;
            padding-left: 5px;
            background-color: #EFF4FB;
        }
        .clsLeadsGridItems
        {
	        font-size: 8pt;
	        color: #123160;
	        font-family: Tahoma;
	        background-color: #EFF4FB;
        }
    </style>
</head>
<body>
    <div id="Disable" style="z-index: 1; display: none; position: absolute; left: 1; top: 1; height: 1px; background-color: Silver; filter: alpha(opacity=50)">
    </div>
    <form id="frmgeneralinfo" method="post" runat="server">
        <div class="page-container row-fluid container-fluid" >
            <aspnew:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="600" runat="server">
                <Scripts>
                    <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
                </Scripts>
            </aspnew:ScriptManager>
            <aspnew:UpdatePanel ID="UpdPnlActiveMenu" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>

                    <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                    <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                    <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                </Triggers>
            </aspnew:UpdatePanel>
       

        <section id="main-content" class="" id="TableSub">
    <section class="wrapper main-wrapper row" style="">
        <div class="col-xs-12">
            <div ID="lblbademail" runat="server" class="alert alert-danger alert-dismissable fade in" Visible="False" style="margin:30px 0 0 0;">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <asp:Label Id="lbl" runat="server" >Bad Email</asp:Label>
                 </div>
             <div ID="lblbadnumber" runat="server"  class="alert alert-danger alert-dismissable fade in" Visible="False" style="margin:30px 0 0 0;">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl1" runat="server" >Telephone Number Alert : Please update client contact information.
                                    </asp:Label>
                                 </div>  
            <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" EnableViewState="true">
                                        <ContentTemplate>
                                            <div class="alert alert-danger alert-dismissable fade in" Id="dvMessage" runat="server" style="margin:30px 0 0 0;">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   <asp:Label ID="lblMessage" runat="server"  ></asp:Label>
                </div>         
                                           
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="btnCIDInCorrect" EventName="click"></aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="ContactLookUp$btnAssociate" EventName="click">
                                            </aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                                        </Triggers>
                                    </aspnew:UpdatePanel>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblClientType" runat="server" Visible="false" 
                                       >Traffic Client</asp:Label>
                 </div>
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <asp:Label ID="lbl_nocalls" runat="server" CssClass="Label" ForeColor="Red" Font-Bold="True"></asp:Label>
                </div>
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        <asp:Label ID="lbl_NoLetters" runat="server" CssClass="Label" ForeColor="Red" Font-Bold="True"></asp:Label>
                </div>
        </div>
        <div class="col-xs-12">
            <div class="page-title">
                 <div class="pull-left">
                     <!-- PAGE HEADING TAG - START --><h1 class="title">Contact</h1><!-- PAGE HEADING TAG - END -->                           
                     </div>

                
                     <div class="pull-right hidden-xs">
                         <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td align="left" style="height: 20px">
                                            <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
                                            <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>(
                                            <asp:Label ID="lbl_CaseCount" runat="server" CssClass="Label"></asp:Label>)
                                            <asp:HyperLink ID="hlnk_MidNo" runat="server"></asp:HyperLink><a href="#"></a>
                                            <asp:HiddenField ID = "hf_languageSpk" runat = "server" value = "0" />
                                            <asp:HiddenField ID = "hf_FirstNameLastName" runat = "server" />
                                            <asp:HiddenField ID = "hf_ContactNum" runat = "server" />
                                            <asp:HiddenField ID = "hf_emailadd" runat = "server" />
                                            <asp:HiddenField ID = "hf_TicketIdForLead" runat = "server" value = "0" />
                                        </td>
                                        <td align="right"style="height: 20px">
                                            <asp:Button ID="btn_update1" runat="server" CssClass="btn btn-primary" Visible="False"
                                                Text="Update"></asp:Button>
                                            
                                        </td>
                                        <td align="right" width="6%" style="height: 20px">
                                        </td>
                                    </tr>
                                </table>
                         </div>
                     
                </div>
            </div>
         <div style="text-align: left">
                                    
                                </div>
                                <table id = "tbl_messages" runat = "server" style="display:none;" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td>
                                <div style="text-align: left">
                                    
                                    <br />
                                    <aspnew:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table runat="server" style="display: none" id="tbl_CIDConfirmation" cellspacing="0"
                                                cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td style="height: 30px">
                                                        <asp:Label ID="lblCIDCorrectConfirmation" runat="server" ForeColor="Red" Font-Names="Verdana"
                                                            Font-Bold="True">Please confirm that the following CID information is 
                                                        correct?</asp:Label>
                                                        <asp:Button ID="btnCIDCorrect" runat="server" CssClass="clsbutton" Text="Correct"
                                                            OnClick="btnCIDCorrect_Click"></asp:Button>
                                                        &nbsp;<asp:Button ID="btnCIDInCorrect" runat="server" CssClass="clsbutton" Text="Incorrect"
                                                            OnClick="btnCIDInCorrect_Click"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%-- <aspnew:AsyncPostBackTrigger ControlID="ContactLookUp$btnAssociate" EventName="click">
                                            </aspnew:AsyncPostBackTrigger>--%>
                                            <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                                            <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="ContactInfo1$btnDisAssociate" EventName="click">
                                            </aspnew:AsyncPostBackTrigger>--%>
                                        </Triggers>
                                    </aspnew:UpdatePanel>
                                    <br />
                                    <asp:Label ID="lblProblemClient" runat="server" ForeColor="Red" Font-Names="Verdana"
                                        Font-Size="X-Small" Visible="False" Font-Bold="True">This is a problem client.</asp:Label>
                                </div>
                                        </td>
                                    </tr>
                                </table>


    
    <div class="clearfix"></div>

          <div class="col-xs-12">
   
               <section class="box ">
        <header class="panel_header">
                <h2 class="title pull-left">Client Information</h2>

                <div class="actions panel_actions pull-right">
                    
                	<a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
        <div class="content-body">
            <div class="row">
                <aspnew:UpdatePanel ID="uppnContactID" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="col-md-3">
                                                        <div class="form-group">
                            <label class="form-label">Last Name</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:TextBox ID="txt_lname" runat="server" Width="100px" CssClass="form-control inline-textbox"
                                                        MaxLength="20"></asp:TextBox><asp:Label ID="lbltxt_lname" runat="server" CssClass="form-label"
                                                            Style="display: none;"></asp:Label>
                            </div>
                        </div>
                                                    </div>
                                        <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-label">First Name , MI :</label>
                                                            <span class="desc"></span>
                                                             <div class="controls">

                                                                 <table class="clsLabel" cellspacing="1" cellpadding="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr class="">
                                                                <td id="td_txtName" width="200px" runat="server" align="left" style="display: inline;">
                                                                    <asp:TextBox ID="txt_fname" runat="server" Width="77%" CssClass="form-control inline-textbox"
                                                                        MaxLength="20"></asp:TextBox>,
                                                                    <asp:TextBox ID="txt_mname" runat="server" Width="18%" CssClass="form-control inline-textbox"
                                                                        MaxLength="1"></asp:TextBox>
                                                                </td>
                                                                <td id="td_lblName" width="100%" runat="server" align="left" style="display: none;">
                                                                    <asp:Label ID="lbltxt_fname" runat="server" CssClass="form-label" Text=" "></asp:Label>,
                                                                    <asp:Label ID="lbltxt_mname" runat="server" Width="17px" CssClass="form-label" Text=" "></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                                 </div>
                                                             </div>
                        </div>
                                         <div class="col-md-3">
                                                        <div class="form-group">
                                                             <label class="form-label">DOB</label>
                            <span class="desc"></span>
                            <div class="controls">

                                 <table cellspacing="0" cellpadding="0" align="left" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td id="td_txtDate" width="100%" runat="server" align="left">
                                                                    <asp:TextBox ID="txt_dob_m" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                                        align="left" Width="50px" CssClass="form-control inline-textbox" MaxLength="2"></asp:TextBox><asp:Label
                                                                            ID="Label2" runat="server" CssClass="form-label" align="left" Text="/"></asp:Label>
                                                                    <asp:TextBox
                                                                                ID="txt_dob_d" onkeyup="return autoTab(this, 2, event)" runat="server" align="left"
                                                                                Width="50px" CssClass="form-control inline-textbox" MaxLength="2"></asp:TextBox>
                                                                    <asp:Label
                                                                                    ID="Label5" runat="server" CssClass="form-label" align="left" Text="/"></asp:Label>
                                                                    <asp:TextBox
                                                                                        ID="txt_dob_y" runat="server" Width="60px" CssClass="form-control inline-textbox"
                                                                                        align="left" MaxLength="4"></asp:TextBox>
                                                                </td>
                                                                <td id="td_lbldate" runat="server" align="left" style="display: none;">
                                                                    <asp:Label ID="lbltxt_dob_m" runat="server" CssClass="form-label" Text=""></asp:Label>
                                                                    <asp:Label
                                                                        ID="Label3" runat="server" CssClass="form-label" Text="/"></asp:Label>
                                                                    <asp:Label ID="lbltxt_dob_d"
                                                                            runat="server" CssClass="form-label" Text=""></asp:Label>
                                                                    <asp:Label ID="Label6" runat="server"
                                                                                CssClass="form-label" Text="/"></asp:Label>
                                                                    <asp:Label ID="lbltxt_dob_y" runat="server"
                                                                                    CssClass="form-label" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                </div>
   </div>
   </div>
                                        <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-label">CID</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">

                                                                 <asp:LinkButton ID="lnkContactID" runat="server" OnClick="lnkContactID_Click" />
                                                    <asp:HiddenField ID="hdnContactID" runat="server" />

                                                                <table style="display: none" id="tblCIDProg" width="100" align="left">
                                                        <tbody>
                                                            <tr>
                                                                <td style="height: 13px" class="clssubhead" valign="middle" align="left">
                                                                    <img src="../Images/plzwait.gif" />
                                                                    Please Wait.
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                                </div>

                                                            </div>
                                            </div>
                                    </ContentTemplate>
                                    <Triggers>
                                      
                                        <aspnew:PostBackTrigger ControlID="ContactInfo1$btnDisAssociate" />
                                        
                                        <aspnew:PostBackTrigger ControlID="ContactLookUp$btnAssociate" />
                                        <aspnew:AsyncPostBackTrigger ControlID="btnCIDCorrect" EventName="click"></aspnew:AsyncPostBackTrigger>
                                        
                                        <aspnew:PostBackTrigger ControlID="btnCIDInCorrect" />
                                    </Triggers>
                                </aspnew:UpdatePanel>
                </div>
            </div>
        </section>
              <div class="clearfix"></div>

              <section class="box " id="tbl_clientinfo">
        <header class="panel_header">
                <h2 class="title pull-left">Client Information Details</h2>

                <div class="actions panel_actions pull-right">
                    
                	<a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                  <div class="content-body">
            <div class="row">

                <div class="col-md-6" id="tdDrivingLicense" runat="server">
                                                        <div class="form-group">
                            <label class="form-label">DL / ST</label>
                            <span class="desc"></span>
                            <div class="controls">
                                <asp:LinkButton ID="lbtn_ViewOCR" runat="server" CausesValidation="False">View</asp:LinkButton>
                                <asp:LinkButton ID="lbtn_Scan" runat="server" OnClick="lbtn_Scan_Click">Scan</asp:LinkButton>
                                <asp:LinkButton ID="lbtn_OCR" runat="server" OnClick="lbtn_OCR_Click">OCR</asp:LinkButton>
                                
                                <asp:TextBox ID="txt_dlstr" runat="server"  CssClass="form-control inline-textbox"
                                                            MaxLength="25"></asp:TextBox>&nbsp; /
                                                        <asp:DropDownList ID="ddl_dlState" runat="server"  CssClass="form-control inline-textbox">
                                                        </asp:DropDownList>
                                                        &nbsp; &nbsp;<asp:CheckBox ID="chk_NoLicense" runat="server" CssClass="checkbox-custom"
                                                            Text="No DL" />

                                </div>
                                                            </div>
                    </div>

                   <div class="col-md-6" id="tr_spn" runat="server">
                                                        <div class="form-group cidCustom" style="margin-top: 20px;">
                            <label id="lbl_spn" runat="server" class="form-label">SPN</label>
                                                             <span class="desc"></span>
                                                            <div class="controls">

                                                            <asp:TextBox ID="txt_spn" runat="server" CssClass="form-control inline-textbox"></asp:TextBox>
                                                        <asp:HiddenField ID="hf_court" runat="server" />
 </div>
                </div>
                      </div>

                 <div class="clearfix"></div>

                 <div class="col-md-6" id="trSSN" runat="server">
                                                        <div class="form-group cidCustom">
                                                            <label class="form-label">Last 4 Digits of your SSN </label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                             <asp:TextBox ID="txt_SSN" MaxLength="4" CssClass="form-control inline-textbox" runat="server" Width="172px"></asp:TextBox>

</div>
                            </div>
                     </div>

                 <div class="col-md-6" runat="server" id="tdCitizen">
                                                        <div class="form-group cidCustom">
                                                            <label class="form-label">US Citizen/Perm Resident?</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                             <asp:RadioButton ID="rbtn_PR_Yes" runat="server" style="margin-left:0px !important;" CssClass="checkbox-custom" GroupName="Citizen_PR"
                                                            Text="Yes" />
                                                            <asp:RadioButton ID="rbtn_PR_No" runat="server" CssClass="checkbox-custom"
                                                                GroupName="Citizen_PR" Text="No" />
                                                        <asp:RadioButton ID="rbtn_PR_NA" runat="server" CssClass="checkbox-custom" GroupName="Citizen_PR"
                                                            Text="N/A" />

</div>
                            </div>
                     </div>
					 
					 <div class="clearfix"></div>

                <div class="col-md-6">
                                                        <div class="form-group cidCustom">
                                                            <label class="form-label">Gender / Race</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <asp:DropDownList ID="ddl_gender" runat="server" Width="150px" CssClass="form-control inline-textbox">
                                                            <asp:ListItem Value="--Choose--" Selected="True">--Choose-- </asp:ListItem>
                                                            <asp:ListItem Value="Male">Male</asp:ListItem>
                                                            <asp:ListItem Value="Female">Female</asp:ListItem>
                                                        </asp:DropDownList>
                                                             /
                                                        <asp:TextBox ID="txt_race" runat="server" Width="70px" CssClass="form-control inline-textbox"></asp:TextBox>

</div>
                            </div>
                     </div>

                 <div class="clearfix"></div>
                
                  <asp:Panel ID="tblBodyParts" runat="server">
                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="form-label">Height</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                             <asp:TextBox ID="txt_heightft" onkeyup="return autoTab(this, 1, event)" runat="server"
                                                            Width="50px" CssClass="form-control inline-textbox" MaxLength="2"></asp:TextBox>
                                                        <asp:TextBox ID="txt_heightin" onkeyup="return autoTab(this, 2, event)" runat="server"
                                                            Width="50px" CssClass="form-control inline-textbox" MaxLength="2"></asp:TextBox>
                                                            </div>
                        </div>
                        </div>
                     
                   <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-label">Weight</label>
                                                            <span class="desc"></span>
                                                               <div class="controls">
                                                            <asp:TextBox ID="txt_weight" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                            Width="60px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>&nbsp;Lbs

                                                             </div>
                                                            </div>
                        </div>

                <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-label">Hair Color</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <asp:DropDownList ID="ddl_haircol" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="--Choose--" Selected="True">--Choose--</asp:ListItem>
                                                            <asp:ListItem Value="Black">Black</asp:ListItem>
                                                            <asp:ListItem Value="Blonde">Blonde</asp:ListItem>
                                                            <asp:ListItem Value="Red">Red</asp:ListItem>
                                                            <asp:ListItem Value="Brown">Brown</asp:ListItem>
                                                            <asp:ListItem Value="Other">Other</asp:ListItem>
                                                        </asp:DropDownList>

                                                             </div>
                                                            </div>
                        </div>

                <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="form-label">Eye Color</label>
                                                            <span class="desc"></span>
                                                             <div class="controls">
                                                           <asp:DropDownList ID="ddl_eyecol" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="--Choose--" Selected="True">--Choose--</asp:ListItem>
                                                            <asp:ListItem Value="Black">Black</asp:ListItem>
                                                            <asp:ListItem Value="Blue">Blue</asp:ListItem>
                                                            <asp:ListItem Value="Green">Green</asp:ListItem>
                                                            <asp:ListItem Value="Hazel">Hazel</asp:ListItem>
                                                            <asp:ListItem Value="Brown">Brown</asp:ListItem>
                                                            <asp:ListItem Value="Other">Other</asp:ListItem>
                                                        </asp:DropDownList>
</div>
                                                             
                                                            </div>
                        </div>
                      </asp:Panel>

                 </div>
                </div>
                  </section>
              <div class="clearfix"></div>

               <section class="box " id="tbl_contact">
                   <header class="panel_header">
                <h2 class="title pull-left">Contact Information</h2>

                <div class="actions panel_actions pull-right">
                    <asp:CheckBox ID="chkb_addresscheck" runat="server" CssClass="checkbox-custom" Text="Address Check">
                                </asp:CheckBox>&nbsp;
                                <asp:Image ID="img_right" runat="server" Visible="False" ImageUrl="../Images/right.gif"
                                    ToolTip="Correct Address"></asp:Image>
                                <asp:Image ID="img_cross" runat="server" Visible="False"
                                        ImageUrl="../Images/cross.gif" ToolTip="Incorrect Address"></asp:Image>
                	<a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                   <div class="content-body">
            <div class="row">
                 <div class="col-md-4">
                                                        <div class="form-group">

                                                            <label class="form-label">Address1</label>
                                                            <span class="desc"></span>
                                                             <div class="controls">
                                                            <asp:TextBox ID="txt_add1" runat="server" Width="100%" CssClass="form-control inline-textbox"
                                                            MaxLength="150"></asp:TextBox>
</div>
                                                            </div>
                     </div>

                 <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-label">Address2</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <asp:TextBox ID="txt_add2" runat="server" Width="100%" CssClass="form-control inline-textbox"
                                                            MaxLength="150"></asp:TextBox>
</div>
                                                            </div>
                     </div>

                 <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="form-label"></label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <asp:CheckBox ID="chk_aparmentno" runat="server" Text="Is Apartment Number" CssClass="checkbox-custom"/>
</div>
                                                            </div>
                     </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                                                        <div class="form-group cityStateZipCustom">
                                                            <label class="form-label">City/State/Zip</label>
                                                            <span class="desc"></span>
                                                             <div class="controls">
                                                             <table cellspacing="1" cellpadding="1" align="center" border="0" style="float: left;">
                                                            <tr>
                                                                <td align="left" style="height: 20px">
                                                                    <asp:TextBox ID="txt_city" runat="server" CssClass="form-control inline-textbox"
                                                                        MaxLength="30"></asp:TextBox>
                                                                </td>
                                                                <td style="height: 20px">
                                                                    <asp:DropDownList ID="ddl_state" runat="server" Width="80px" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="height: 20px">
                                                                    <asp:TextBox ID="txt_zip" runat="server" Width="70px" CssClass="form-control inline-textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
 </div>
                                                            </div>
                     </div>

                <div class="clearfix"></div>

                

                 <div class="col-md-12 contact1and2Field">
                 <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label">Contact 1</label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <table>
                                                            <tr>
                                                                <td>
                                                                      <asp:DropDownList ID="ddl_contact1" runat="server" CssClass="form-control inline-textbox" onchange="return Check('ddl_contact1');">
                                                            <asp:ListItem Value="0" Selected="True">--Choose--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txt_cc11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="txt_cc12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="txt_cc13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                 Width="70px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox><span class="contact3FieldX">x</span><asp:TextBox ID="txt_cc14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox"></asp:TextBox>
                                                                </td>
                                                                <td id="td_chk_SMS1" runat="server" class="" style="display: block">
                                                                    <asp:CheckBox ID="chk_SmsRequired1" runat="server" Text="SMS Alert" CssClass="checkbox-custom"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                            </div>
                                                            </div>
                     </div>
                 <div class="col-md-6">
                 <div class="form-group">
                                                            <label class="form-label">
                                                               Contact 2
                                                                </label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                             <table>
                                                            <tr>
                                                                <td>
                                                                     <asp:DropDownList ID="ddl_contact2" runat="server" CssClass="form-control inline-textbox" onchange="return Check('ddl_contact2');">
                                                            <asp:ListItem Value="0">--Choose--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txt_cc21" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="txt_cc22" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="txt_cc23" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox><span class="contact3FieldX">x</span><asp:TextBox ID="txt_cc24" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox"></asp:TextBox>
                                                                </td>
                                                                <td id="td_chk_SMS2" runat="server" style="display: none" class="">
                                                                    <asp:CheckBox ID="chk_SmsRequired2" runat="server" Text="SMS Alert" CssClass="checkbox-custom"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                            </div>
                                                            </div>
                     </div>
                     </div>
                   <div class="clearfix"></div>
                <div class="col-md-12 contact3Field">
                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label">
                                                                Contact 3
                                                                </label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                             <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddl_contact3" runat="server" CssClass="form-control inline-textbox" onchange="return Check('ddl_contact3');">
                                                            <asp:ListItem Value="0" Selected="True">--Choose--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txt_cc31" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="txt_cc32" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>
                                                                    <asp:TextBox ID="txt_cc33" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox><span class="contact3FieldX">x</span><asp:TextBox ID="txt_cc34" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                        Width="70px" CssClass="form-control inline-textbox"></asp:TextBox>
                                                                </td>
                                                                <td id="td_chk_SMS3" runat="server" class="" style="display: none">
                                                                    <asp:CheckBox ID="chk_SmsRequired3" runat="server" Text="SMS Alert" CssClass="checkbox-custom"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                            </div>
                                                            </div>
                    </div>
                    
                <div class="col-md-6">
                
                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-label">Email
                                                                </label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txt_email" runat="server" CssClass="form-control inline-textbox"
                                                                        Width="200px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkbtn_VerifyEmail" Text="Verify Email" runat="server" />
                                                                   
                                                                </td>
                                                            </tr>
                                                        </table>
                                                            </div>
                                                            </div>
                     </div>
                     
<div class="col-md-6">
                                                        <div class="form-group">
                                                           
                      <label class="form-label">
                                                                </label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                     <asp:CheckBox ID="chkEmail" runat="server" onclick="EnableDisabeEmail();" CssClass="checkbox-custom"
                                                            Text="Email Address not available"></asp:CheckBox>
                                                             </div>
                                                             
                                                            </div>
                     </div>
                
                    </div>
                    
                    </div>
                
                 <div class="clearfix"></div>

                 

                 


 </div>
                </div>
                       

                   </section>
              <div class="clearfix"></div>

                <section class="box " id="tr_consultation" runat="server" >
                   <header class="panel_header" id="tr_consultationHeader" runat="server">
                       <h2 class="title pull-left">Consultation</h2>

                <div class="actions panel_actions pull-right">
                    <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                       </header>

                    <div class="content-body">
            <div class="row">

                 <div class="col-md-12" id="">
                                                        <div class="form-group">
                                                            <label class="form-label"></label>
                                                            <span class="desc"></span>
                                                            <div class="controls">
                                                            <table style="width: 100%" class="">
                                    <tr>
                                        <td style="width: 99%" class="form-label">
                                            &nbsp; Would you like to schedule a consultation regarding any other legal matter?
                                            <asp:RadioButton ID="rbtn_FreeCon_Yes" runat="server" onclick="return openAddNewLeadWindow('../clientinfo/addnewlead.aspx?languageSpk=',400,420);"
                                                CssClass="checkbox-custom" GroupName="FreeCon" Text="Yes" /><asp:RadioButton ID="rbtn_FreeCon_No"
                                                    runat="server" CssClass="checkbox-custom" GroupName="FreeCon" Text="No" />
                                           
                                        </td>
                                    </tr>
                                    
                                </table>

                                                            </div>
                     </div>
                     </div>

                </div>
                        </div>
                    </section>
              <div class="clearfix"></div>

              <section class="box " id="trConsultationHistorHead" runat="server" >
                   <header class="panel_header" id="">
                       <h2 class="title pull-left">Consultation History</h2>
                       <div class="actions panel_actions pull-right">
                           <asp:LinkButton ID="lnkaddnewpolm" CssClass="btn btn-primary" runat="server" Text="Add New Lead" OnClientClick="return openAddNewLeadWindow('../clientinfo/addnewlead.aspx?languageSpk=',1000,550);"></asp:LinkButton>&nbsp;
                    <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                       
                       </header>
                  <div class="content-body">
            <div class="row">
                 <div class="col-md-12" id="trConsultationHistory" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="form-label"></label>

 <span class="desc"></span>
                            <div class="controls">
                                                            <asp:GridView ID="gvConsultationHistory" runat="server" AutoGenerateColumns="False"
                                    CssClass="clsLeftPaddingTable" OnRowDataBound="gvConsultationHistory_RowDataBound"
                                    CellPadding="0" CellSpacing="0" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="20px" />
                                            <ItemStyle HorizontalAlign="Center" CssClass="clsLeadsGridItems" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowNum" CssClass="form-label" runat="server" Text='<%# Eval("RowNum") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" HeaderStyle-Width ="70">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLeadDate" CssClass="form-label" runat="server" Text='<%# Eval("LeadDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Caller ID" HeaderStyle-Width ="80">
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCallerId" CssClass="form-label" runat="server" Text='<%# Eval("CallerId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name" HeaderStyle-Width ="130">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPersonName" CssClass="form-label" runat="server" Text='<%# Eval("PersonName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Language" HeaderStyle-Width ="60">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLanguage" CssClass="form-label" runat="server" Text='<%# Eval("LanguageSpk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-Width ="140">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" CssClass="form-label" runat="server" Text='<%# Eval("EmailAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone"  HeaderStyle-Width ="80">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPhone" CssClass="form-label" runat="server" Text='<%# Eval("Phone") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Practice Area" HeaderStyle-Width ="150">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPracticeArea" CssClass="form-label" runat="server" Text='<%# Eval("PracticeArea") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rep" HeaderStyle-Width ="80">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="clsLeadsGridItems" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRepname" CssClass="form-label" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Norecord" CssClass="form-label" runat="server" Text="No Records Found"></asp:Label>
                                                            
                                                            </div>
                                                            </div>
                     </div>
                
                </div>
                      </div>
                  </section>
              <div class="clearfix"></div>

               <section class="box " id="trfirminformation"  runat="server">
                   <header class="panel_header" id="">
                       <h2 class="title pull-left">Firm Information</h2>
                       <div class="actions panel_actions pull-right">
                           
                    <a class="box_toggle fa fa-chevron-down"></a>
                    </div>
                       </header>
                   <div class="content-body">
            <div class="row">
                <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">Address1</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:Label ID="lblfaddress1" CssClass="form-label" runat="server"></asp:Label>
                                    
                                    </div>
                                                                </div>
                         </div>
                 <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">Address2</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:Label ID="lblfaddress2" CssClass="form-label" runat="server"></asp:Label>
                                    
                                    </div>
                                                                </div>
                         </div>
                 <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">ZipCode</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:Label ID="lblfzipcode" runat="server" CssClass="form-label" ></asp:Label>
                                    
                                    </div>
                                                                </div>
                         </div>
                <div class="clearfix"></div>
                <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">City</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:Label ID="lblfcity" runat="server" Text="" CssClass="form-label"></asp:Label>
                                    
                                    </div>
                                                                </div>
                         </div>
                 <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">State</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblfstate" runat="server" CssClass="form-label"></asp:Label>
                                    </div>
                                                                </div>
                         </div>
                 <div class="col-md-3">
                                                            <div class="form-group">
                                <label class="form-label">Phone #</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblfphone" runat="server" CssClass="form-label"></asp:Label>
                                    </div>
                                                                </div>
                         </div>

                   <div class="col-md-3" id="">
                                                        <div class="form-group">
                                                            <label class="form-label"></label>

                                                            
 <span class="desc"></span>
                            <div class="controls">
                                                            <div id="divClient" runat="server" style="display: none">
                        </div>
                                                            </div>
                       </div>
                </div>

                <div class="clearfix"></div>

                </div>
                       </div>

                   </section>
              <div class="clearfix"></div>


              
 <div class="clearfix"></div> 
              <section class="box " id="tr_postHireQuestions"  runat="server">
                   <header class="panel_header" id="">
                       <h2 class="title pull-left">Post Hire Questions</h2>
                       </header>
                  <div class="content-body">
            <div class="row">
                <div class="col-md-12" id="tr_ALRRequiredWhy" style="display:none">
                                                        <div class="form-group">
                                                            <label class="form-label">Why?</label>
                                                            
 <span class="desc"></span>
                            <div class="controls">
                                                             <asp:TextBox ID="txtALRHearingRequiredAnswer" Width="99%" CssClass="form-control inline-textbox"
                                                    runat="server" Height="42px" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                            </div>
                    </div>
                <div id="tr_postHireQuestions2"  runat="server" style="display:none">
                      <div class="table-responsive" data-pattern="priority-columns">
                <table style="width: 100%;" id="Table2" cellspacing="1" cellpadding="0" width="100%" class="table table-small-font table-bordered table-striped">
                                    <tbody>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                County of Arrest :
                                            </td>
                                            <td style="height: 25px" class="Label">
                                                <asp:Label Style="display: none" CssClass="form-label" ID="lblCountyOfArrest" Width="200px" runat="server"></asp:Label>
                                                <asp:HyperLink Style="display: none" Width="200px" Font-Underline="true" ID="HLkCounty"
                                                    runat="server">Set County</asp:HyperLink>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Officer Name :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALROfficerName" Width="200px" CssClass=" form-control inline-textbox"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Officer Badge Number :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALROfficerBadgeNumber" Width="200px" CssClass=" form-control inline-textbox"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Precinct:
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALRPrecinct" Width="200px" CssClass=" form-control inline-textbox"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Address :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALRAddress" Width="400px" CssClass=" form-control inline-textbox" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                City/State/Zip :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALRCity" Width="75px" CssClass=" form-control inline-textbox" runat="server"></asp:TextBox>
                                                <asp:DropDownList ID="ddl_ALRState" runat="server" Width="50px" CssClass="clsInputCombo">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtALRZip" Width="65px" CssClass=" form-control inline-textbox" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Contact Number :
                                            </td>
                                            <td style="height: 25px">
                                                <table style="width: 100%;" id="Table4" cellspacing="1" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td class="clssubhead" align="left">
                                                                <asp:TextBox ID="txt_Attacc11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                    Width="40px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>&nbsp;
                                                                <asp:TextBox ID="txt_Attcc12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                    Width="40px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox>&nbsp;
                                                                <asp:TextBox ID="txt_Attcc13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                    Width="40px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox>
                                                                &nbsp;x
                                                                <asp:TextBox ID="txt_Attcc14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                    Width="40px" CssClass="form-control inline-textbox"></asp:TextBox>&nbsp;
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Arresting Agency :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALRArrestingAgency" Width="200px"  form-control inline-textbox
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="" style="display: table-row;">
                                            <td style="width: 50%" class="clsLabelNew">
                                                Officer Mileage to Court house :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:TextBox ID="txtALROfficerMileage" Width="200px" CssClass="form-control inline-textbox"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr1" runat="server" class="" style="display: table-row">
                                            <td style="width: 50%;" class="clsLabelNew">
                                                Is the Arresting Officer the Observing Officer?
                                            </td>
                                            <td style="height: 25px">
                                                <asp:RadioButton ID="rBtnArrOffObsOffYes" runat="server" CssClass="clsLabelNew" GroupName="ArrOffObsOff"
                                                    onclick="return ShowHideObserveOfficer();" Text="Yes"></asp:RadioButton>
                                                <asp:RadioButton ID="rBtnArrOffObsOffNo" runat="server" CssClass="clsLabelNew" GroupName="ArrOffObsOff"
                                                    onclick="return ShowHideObserveOfficer();" Text="No"></asp:RadioButton>
                                            </td>
                                        </tr>
                                        <tr id="tr_ObservingOfficer" runat="server" style="display: none">
                                            <td colspan="2">
                                                <table style="width: 100%;" id="Table11" cellspacing="1" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr class="">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Name :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSOfficerName" Width="200px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Badge Number :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSOfficerBadgeNumber" Width="200px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="" style="display: table-row;">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Precinct:
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSPrecinct" Width="200px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="" style="display: table-row;">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Address :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSAddress" Width="400px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="" style="display: table-row;">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer City/State/Zip :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSCity" Width="75px" CssClass="form-control inline-textbox" runat="server"></asp:TextBox>
                                                                <asp:DropDownList ID="ddl_ALROBSState" runat="server" Width="50px" CssClass="form-control inline-textbox">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtALROBSZip" Width="65px" CssClass="form-control inline-textbox" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="" style="display: table-row;">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Contact Number :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <table style="width: 100%;" id="Table9" cellspacing="1" cellpadding="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="clssubhead" align="left">
                                                                                <asp:TextBox ID="txt_OBSAttacc11" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                    Width="40px" CssClass="form-control inline-textbox" MaxLength="3"></asp:TextBox>&nbsp;
                                                                                <asp:TextBox ID="txt_OBSAttcc12" onkeyup="return autoTab(this, 3, event)" runat="server"
                                                                                    Width="40px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox>&nbsp;
                                                                                <asp:TextBox ID="txt_OBSAttcc13" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                    Width="40px" CssClass="form-control inline-textbox" MaxLength="4"></asp:TextBox>
                                                                                &nbsp;x
                                                                                <asp:TextBox ID="txt_OBSAttcc14" onkeyup="return autoTab(this, 4, event)" runat="server"
                                                                                    Width="40px" CssClass="form-control inline-textbox"></asp:TextBox>&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="" style="display: table-row;">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Arresting Agency :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSArrestingAgency" Width="200px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="" style="display: table-row;">
                                                            <td style="width: 50%" class="clsLabelNew">
                                                                Observing Officer Mileage to Court house :
                                                            </td>
                                                            <td style="height: 25px">
                                                                <asp:TextBox ID="txtALROBSOfficerMileage" Width="200px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                       
                                        <tr id="tr_IntoxilyzerResults" runat="server" class="" style="display: none">
                                            <td style="width: 50%;" class="clsLabelNew">
                                                Intoxilyzer Results :
                                            </td>
                                            <td style="height: 25px">
                                                <asp:RadioButton ID="rbtn_IntoxResultPass" runat="server" CssClass="clsLabelNew"
                                                    GroupName="PassFail" Text="Pass"></asp:RadioButton>
                                                <asp:RadioButton ID="rbtn_IntoxResultFail" runat="server" CssClass="clsLabelNew"
                                                    GroupName="PassFail" Text="Fail"></asp:RadioButton>
                                            </td>
                                        </tr>
                                        <tr id="tr_BreathTestOperator" runat="server" class="" style="display: none">
                                            <td style="width: 50%;" class="clsLabelNew">
                                                Breath Test Operator (BTO):
                                            </td>
                                            <td style="height: 25px" class="clsLabelNew">
                                                <table style="width: 100%;" id="Table3" cellspacing="1" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td class="clsLabelNew">
                                                                <asp:RadioButton ID="rbBTOArresting" onclick=" return ShowBTOArresting();" runat="server"
                                                                    CssClass="clsLabelNew" GroupName="BTO" Text="Same as Arresting Officer"></asp:RadioButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="clsLabelNew">
                                                                <asp:RadioButton ID="rbBTOObserving" onclick=" return ShowBTOObserving();" runat="server"
                                                                    CssClass="clsLabelNew" GroupName="BTO" Text="Same as Observing Officer"></asp:RadioButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="clsLabelNew">
                                                                <asp:RadioButton ID="rblBTOName" onclick=" return ShowBTOName();" runat="server"
                                                                    CssClass="clsLabelNew" GroupName="BTO"></asp:RadioButton>
                                                                <asp:TextBox ID="txtBTOName" Width="200px" Enabled="false" CssClass="clsInputadministration"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            </tr>
                                            
                                        <tr id="tr_BreathTestSupervisor" runat="server" class="" style="display: none">
                                            <td style="width: 50%;" class="clsLabelNew">
                                                Breath Test Supervisor (BTS):
                                            </td>
                                            <td style="height: 25px" class="clsLabelNew">
                                                <table style="width: 100%;" id="Table5" cellspacing="1" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td class="clsLabelNew">
                                                                First Name :
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtBTSFirstName" Width="100px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="clsLabelNew">
                                                                Last Name :
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtBTSLastName" Width="100px" CssClass="form-control inline-textbox"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                          </div>
                    </div>
                </div>
                      </div>
                  </section>
              
 <div class="clearfix"></div>

              <section class="box " id="FlagsE"  >
                   <header class="panel_header" >
                       <h2 class="title pull-left">Flags/Events</h2>
                        <div class="actions panel_actions pull-right">
                            <div style="display:inline">
                             <aspnew:UpdatePanel ID="updatePanel1" runat="server" >
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_flags" runat="server" CssClass="form-control inline-textbox" onChange=" return FlagValidation();" Width="100%" >
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                            </div>
                            <%--<a class="box_toggle fa fa-chevron-down"></a>--%>

                            </div>
                       </header>
                  <div class="content-body">
            <div class="row"  >
                  
   <aspnew:UpdatePanel ID="updatePanelFlagsEvents" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlFlagsEvents" runat="server">
                                                        <uc:AddPopUpComment ID="addPopUpComment_Trial" runat="server" Message="Please add trial note to describe the need of this flag."
                                                            EmptyErrorMessage="Please add trial notes to add this flag in client profile otherwise press Cancel to exit.">
                                                        </uc:AddPopUpComment>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>

        
                     
                     

                <%--<div class="clearfix"></div>--%>

                <div class="col-md-12" id="tbl_flagdetail">
    
    
                            <div class="table-responsive" data-pattern="priority-columns">
                                <asp:DataList ID="DataL_flags" runat="server" OnItemDataBound="DataL_flags_ItemDataBound"
                                                RepeatColumns="1" Width="100%" CssClass="table table-small-font table-bordered table-striped">
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%;" class="">
                                                        <tr>
                                                            <td style="height: 25px;">
                                                                <asp:LinkButton ID="lnk_flags" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'
                                                                    CommandName="FlagClicked" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.fid") %>'></asp:LinkButton>
                                                            </td>
                                                            <td align="right" style="height: 25px">
                                                                <table id="tbl_continuance" runat="server" visible="false">
                                                                    <tr>
                                                                        <td>&nbsp;
                                                                            
                                                                        </td>
                                                                        <td align="right">
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:DropDownList ID="ddl_continuancedate" runat="server" CssClass="clsInputCombo"
                                                                                Width="117px" OnSelectedIndexChanged="DdlContinuancedateSelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                            <asp:Label ID="lbl_continunacedate" runat="server" CssClass="Label"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddl_continuancestatus" runat="server" CssClass="clsInputCombo">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table id="tblfirms" runat="server" visible="false">
                                                                    <tr>
                                                                        <td>
                                                                            <img alt="" src="../Images/Rightarrow.gif" id="img_firmtoggle" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddl_FirmAbbreviation" runat="server" CssClass="clsInputCombo"
                                                                                Style="display: none;" Font-Size="XX-Small">
                                                                            </asp:DropDownList>
                                                                            <asp:Label ID="lbl_FirmAbbreviation" runat="server" CssClass="Label" Style="display: block;"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table id="tblnos" runat="server" visible="false">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Label ID="Lbl2" runat="server" CssClass="Label" Text="Follow Up Date : "></asp:Label>
                                                                            <asp:DropDownList ID="ddl_nos" runat="server" CssClass="clsInputCombo" Width="117px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image ID="img_open" runat="server" Height="23px" Style="cursor: hand;" ImageUrl="../images/open.png"
                                                                                Visible="False" Width="22px" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label1" runat="server" CssClass="Label"></asp:Label>
                                                                            <asp:Label ID="lblPercentage" runat="server" CssClass="Label"></asp:Label>
                                                                            <asp:Label ID="lblContinuanceOption" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.Options") %>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lbl_ServiceTicketCategory" CssClass="Label" Visible="False" runat="server"
                                                                                Text="Category: "></asp:Label>
                                                                            <asp:Label ID="lblServiceTicketCategory" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.category") %>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblPriority" runat="server" CssClass="Label" Text="  " Visible="False"></asp:Label>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_priority" runat="server" CssClass="Label"></asp:Label>
                                                                            </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:HiddenField ID="hf_ispaid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.isPaid") %>' />
                                                    <asp:HiddenField ID="hf_continuancedate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ContinuanceDate","{0:d}") %>' />
                                                    <asp:HiddenField ID="hf_continuancestatus" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ContinuanceStatus") %>' />
                                                    <asp:HiddenField ID="hf_flagid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FlagID") %>' />
                                                    <asp:HiddenField ID="hf_priority" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Priority") %>' />
                                                    <asp:HiddenField ID="hf_ServiceTicketCategory" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ServiceTicketCategory") %>' />
                                                    <asp:HiddenField ID="hf_ContinuanceOption" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ContinuanceStatus") %>' />
                                                </ItemTemplate>
                                            </asp:DataList>
                                </div>
                                            <div id="pnl_serviceticket" style="display: none; z-index: 3; position: absolute;
                                                background-color: transparent">
                                                <asp:HiddenField ID="hf_checkServiceTicket" Value="102" runat="server" />
                                                <table>
                                                    <tr>
                                                        <td id="read" runat="server">
                                                            <uc2:ReadNotes ID="ReadNotes1" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                 
                                </div>
        
                    
                <div class="clearfix"></div>


                </div>
                      </div>
                      </div>


                  </section>
              <div class="clearfix"></div>

              <section class="box " id=""  >
                   <header class="panel_header" >
                       <h2 class="title pull-left">Activities</h2>
                        <div class="actions panel_actions pull-right">
                            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                       </header>
                   <div class="content-body">
            <div class="row"  >
                 
                 <div class="col-md-6" id="">
    <div class="form-group">
    <label class="form-label"></label>
    
 <span class="desc"></span>
                            <div class="controls">
                                 <asp:Label ID="lblError" runat="server" Width="320px" CssClass="form-label" ForeColor="Red"
                                    EnableViewState="False"></asp:Label>
                                </div>
        </div>
                      </div>
                <div class="clearfix"></div>
                 <div class="col-md-12" id="">
    <div class="form-group">
    <label class="form-label"></label>
    
 <span class="desc"></span>
                            <div class="controls" id="tdData">
                                 <asp:GridView ID="gv_Activites" runat="server" AllowSorting="false" AllowPaging="true"
                                                PageSize="20" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped" OnPageIndexChanging="gv_Activites_PageIndexChanging"
                                                OnRowDataBound="gv_Activites_RowDataBound" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_RowID" runat="server" Text="" Visible="false" />
                                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# Eval("CourtDateMain","{0:d}") %>'
                                                                Visible="false" />
                                                            <asp:Label ID="lbl_TicketID" runat="server" Text='<%# Eval("TicketID") %>' Visible="false" />
                                                            <asp:Label ID="lbl_TicketViolationID" runat="server" Text='<%# Eval("TicketViolationIds") %>'
                                                                Visible="false" />
                                                            <asp:Label ID="lbl_EventTypeID" runat="server" Text='<%# Eval("EventTypeID") %>'
                                                                Visible="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Call Date">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnk_Activitity" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CallID") %>'
                                                                CommandName="ActivitityClicked" Text='<%# Eval("CallDateTime","{0:d}") %>' Style="text-decoration: none"></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Response Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblActivitiesRespType" runat="server" CssClass="Label" Text='<%# Eval("ResponseType") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Event">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" CssClass="Label" Text='<%# Eval("EventTypeName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CallBack Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label4" runat="server" CssClass="Label" Text='<%# Eval("CallBackStatus") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Contact Info">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label3" runat="server" CssClass="Label" Text='<%# Eval("ContactNumber") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                    Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                            </asp:GridView>
                                            <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                </div>
        </div>
                      </div>

                </div>
                       </div>
                  </section>
              <div class="clearfix"></div>

               <section class="box ">
            
        <div class="content-body">
            <div class="row">
                 <asp:Button ID="btn_update2" runat="server" CssClass="btn btn-primary" Text="Update"></asp:Button>
                                    <asp:Button ID="btn_next" runat="server" Width="60px" CssClass="btn btn-primary" Text="Next">
                                            </asp:Button>
            </div>
        </div>
        </section>

              




              </div>
    </section>
                </section>
            
            <table>
                  <tr>
                           <td style="visibility: hidden; height: 206px;" align="right" colspan="2">
                                <asp:TextBox ID="txtTimeStamp" runat="server" Width="89px"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox ID="txtsessionid"
                                    runat="server"></asp:TextBox><asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox><asp:TextBox
                                        ID="txtSrv" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txt_oldset" runat="server"></asp:TextBox>
                                &nbsp;&nbsp;
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_checkFirm" runat="server" Value="0" />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_lastConsultationComments" runat="server" Value='' />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_activeflag" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_updateaddress" runat="server" />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_checkcontinuance" runat="server" Value="0" />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_continuanceflag" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_Status" runat="server" Value="0" />
                                            <asp:HiddenField ID="hf_LanguageSpeak" runat="server" />
                                            <asp:HiddenField ID="CourtLocation" runat="server" />
                                            <asp:HiddenField ID="hf_casetype" runat="server" />
                                            <asp:HiddenField ID="Hf_courtstatus" runat="server" />
                                            <asp:HiddenField ID="hf_courtdateyear" runat="server" />
                                            <asp:HiddenField ID="hf_isactive" runat="server" value="1"/>
                                            <asp:HiddenField ID="CourtRoom" runat="server" Value="1" />
                                            <%--<aspnew:AsyncPostBackTrigger ControlID="ContactLookUp$btnAssociate" EventName="click">
                                            </aspnew:AsyncPostBackTrigger>--%>
                                            <asp:HiddenField ID="hf_isHCJPcase" runat="server" />
                                            <asp:HiddenField ID="hf_isHMCcase" runat="server" />
                                            <asp:HiddenField ID="hf_HasIDTicket" runat="server" />
                                            <asp:HiddenField ID="hf_HasNotonSystem" runat="server" />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_unpaid" runat="server" Value="0" />
                                            <asp:HiddenField ID="hf_continuancedate" runat="server" />
                                            <asp:HiddenField ID="hf_updatecontinuance" runat="server" Value="0" />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:HiddenField ID="hf_prevfirm" runat="server" />
                                            <asp:HiddenField ID="hf_prevcontinuancedate" runat="server" />
                                            <asp:HiddenField ID="hf_LastGerenalcommentsUpdatedate" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                <asp:Label ID="lbl_bondflag" runat="server"></asp:Label>
                                <table style="width: 100%">
                                    <tr id="tbl_firminfo" runat="server">
                                        <td class="Label">
                                            Outside Firm
                                        </td>
                                        <td align="right" style="width: 50%">
                                            <asp:DropDownList ID="ddl_FirmAbbreviation" runat="server" CssClass="clsInputCombo">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tbl_continuanceinfo" runat="server">
                                        <td class="Label">
                                            Continuance Date &amp; Status
                                        </td>
                                        <td align="right">
                                            <table id="tbl_continuance" runat="server">
                                                <tr>
                                                    <td>
                                                        <img id="img_toggle" runat="server" alt="" onclick="DisplayToggle()" src="../Images/Rightarrow.gif" />
                                                    </td>
                                                    <td align="right">
                                                        <asp:DropDownList ID="ddl_continuancedate" runat="server" CssClass="clsInputCombo"
                                                            Width="117px">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lbl_continunacedate" runat="server" CssClass="Label"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddl_continuancestatus" runat="server" CssClass="clsInputCombo">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                  </tr>
              </table>
            
        <aspnew:UpdatePanel ID="updatePanelPopUpFlagsEvents" runat="server">
        <ContentTemplate>
            <ajaxToolkit:ModalPopupExtender ID="modalPopupExtenderFlagsEvents" runat="server"
                TargetControlID="Button1" PopupControlID="pnlFlagsEvents" BackgroundCssClass="modalBackground"
                HideDropDownList="false">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
        </ContentTemplate>
    </aspnew:UpdatePanel>

         <aspnew:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
            <ajaxToolkit:ModalPopupExtender ID="Modal_Message" runat="server" TargetControlID="btncancel"
                PopupControlID="Panel1" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btncancel" runat="server" Text="Yes" Style="display: none" />
            <asp:Panel ID="Panel1" runat="server">
                <table id="tablepopup" style="border-top: black thin solid; border-left: black thin solid;
                    border-bottom: black thin solid; border-right: black thin solid" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                            style="width: 353px">
                            <table style="width: 407px" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td class="clssubhead" align="left" style="height: 16px">
                                            &nbsp;Alert Box
                                        </td>
                                        <td align="right" style="height: 16px">
                                            &nbsp;<asp:LinkButton ID="lnkbtncancelpopup" runat="server">X</asp:LinkButton>&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#eff4fb">
                        <td align="center">
                            <table width="353px" cellspacing="1" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblmessageshow" runat="server" BackColor="#eff4fb" ForeColor="#3366CC"
                                            Text="Please Enter General Comments Again because some one already updated the comments"
                                            Width="353px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                        <asp:Button ID="btnok" runat="server" Text="Ok" CssClass="clsbutton" Height="20px"
                                            Width="60px" OnClick="btnok_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 10px">
                        <td bgcolor="#eff4fb">
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <aspnew:PostBackTrigger ControlID="btncancel" />
        </Triggers>
    </aspnew:UpdatePanel>

        <aspnew:UpdatePanel ID="upnlContact" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlViewContactInfo" runat="server" Width="340px">
                <Contact:ContactInfo ID="ContactInfo1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="PnlContactLookUp" runat="server" Width="340px">
                <ContactID:LookUp ID="ContactLookUp" runat="server" />
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MPContactID" runat="server" TargetControlID="btnContactID"
                PopupControlID="pnlViewContactInfo" BackgroundCssClass="modalBackground" DropShadow="false">
            </ajaxToolkit:ModalPopupExtender>
            <ajaxToolkit:ModalPopupExtender ID="MPContactLookUp" runat="server" TargetControlID="btnContactLookUp"
                PopupControlID="PnlContactLookUp" BackgroundCssClass="modalBackground" DropShadow="false">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button runat="server" ID="btnContactID" Text="More" Style="display: none;" />
            <asp:Button runat="server" ID="btnContactLookUp" Text="More" Style="display: none;" />
        </ContentTemplate>
    </aspnew:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="mpeShowPolm" runat="server" TargetControlID="polmHiddenButton"
        PopupControlID="pnlPolm" BackgroundCssClass="modalBackground" DropShadow="false">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Button runat="server" ID="polmHiddenButton" Text="More" Style="display: none;" />
    <asp:Panel ID="pnlPolm" runat="server">
        <table id="table1" style="border-top: black thin solid; border-left: black thin solid;
            border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
            cellspacing="0" class="clsLeftPaddingTable">
            <tr>
                <td align="left" valign="bottom" background="../Images/subhead_bg.gif" colspan="2">
                    <table width="100%" border="0">
                        <tr>
                            <td style="height: 26px" class="clssubhead">
                                <asp:Label runat="server" ID="lbl_title" Text="Consultation Request"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:LinkButton ID="lbtn_close2" runat="server" OnClick="lbtn_close2_Click">X</asp:LinkButton>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <uc1:PolmControl ID="polmControl1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <uc3:UpdateEmailAddress ID="UpdateEmailAddress1" runat="server" />
    <aspnew:UpdatePanel ID="UpdateNoBadAddress" runat="server">
        <ContentTemplate>
            <ajaxToolkit:ModalPopupExtender ID="Modal_NoBadAddress" runat="server" TargetControlID="btnCancelNoBadAddress"
                PopupControlID="UpdateNoBadAddress" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnCancelNoBadAddress" runat="server" Text="Yes" Style="display: none" />
            <asp:Panel ID="pNoBadAddress" runat="server">
                <table id="tblNoBadAddress" style="border-top: black thin solid; border-left: black thin solid;
                    border-bottom: black thin solid; border-right: black thin solid" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                            style="width: 353px">
                            <table style="width: 407px" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td class="clssubhead" align="left" style="height: 16px">
                                            &nbsp;Alert Box
                                        </td>
                                        <td align="right" style="height: 16px">
                                            &nbsp;<asp:LinkButton ID="LinkButton1" runat="server">X</asp:LinkButton>&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#eff4fb">
                        <td align="center">
                            <table width="353px" cellspacing="1" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNoBadAddress" runat="server" BackColor="#EFF4FB" ForeColor="#3366CC"
                                            Text="Physical Address validation for this client has failed. Please verify from the client that his/her address is correct. Click 'Yes' to accept the address or Click 'No' to cancel"
                                            Width="353px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="clsbutton" Height="20px"
                                            Width="60px" OnClick="btnYes_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="clsbutton" Height="20px"
                                            Width="60px" OnClick="btnNo_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 10px">
                        <td bgcolor="#eff4fb">
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <aspnew:PostBackTrigger ControlID="btnCancelNoBadAddress" />
        </Triggers>
    </aspnew:UpdatePanel>
             </div>

        

       
    </form>
    <div id="ErrorString">
    </div>
    <img height="1" src="" width="1" name="Img1" />

    <script type="text/javascript">


        //setContinuance();
        //setFirm();

        function StartScan() {
            var sid = document.getElementById("txtsessionid").value;

            var eid = document.getElementById("txtempid").value;

            var path = '<%=ViewState["vNTPATHScanTemp"]%>';
            var Type = 'network';
            var AutoFeed = 1;
            var sel = OZTwain1.SelectScanner();
            if (sel == "Success") {
                OZTwain1.Acquire(sid, eid, path, Type, AutoFeed);
            }
            else if (sel == "Cancel") {
                $("#txtErrorMessage").text("Operation canceled by user!");
                $("#errorAlert").modal();
               // alert("Operation canceled by user!");
                return false;
            }
            else {
                $("#txtErrorMessage").text("Scanner not installed.");
                $("#errorAlert").modal();
             //   alert("Scanner not installed.");
                return false;
            }

        }
    </script>
    

   <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="lntechNew.ClientInfo.test" %>
<%@ Register Src="../WebControls/UpdateViolationSRV.ascx" TagName="UpdateViolation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-color :Lime">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        </aspnew:ScriptManager>
        &nbsp; &nbsp;
        </div>
      &nbsp;&nbsp;&nbsp;
        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
        
            <ContentTemplate>
              <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
        <asp:DataGrid ID="dgViolationInfo" runat="server" AutoGenerateColumns="False" BorderColor="Gray"
            BorderStyle="Solid" CssClass="clsleftpaddingtable" OnItemDataBound="dgViolationInfo_ItemDataBound"
            Width="800px">
            <Columns>
                <asp:TemplateColumn HeaderText="Cause Number">
                    <ItemTemplate>
                        <asp:Label ID="lbl_CauseNo" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'>
																	</asp:Label>
                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>' />
                        <asp:HiddenField ID="hf_bondamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.Bondamount","{0:F0}") %>' />
                        <asp:HiddenField ID="hf_fineamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.FineAmount","{0:F0}") %>' />
                        <asp:HiddenField ID="hf_courtloc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.shortname") %>' />
                        <asp:HiddenField ID="hf_priceplan" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' />
                        <asp:HiddenField ID="hf_bondflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' />
                        <asp:HiddenField ID="hf_violationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>' />
                        <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>' />
                        <asp:HiddenField ID="hf_courtviolationstatusid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtViolationStatusidmain") %>' />
                        <asp:HiddenField ID="hf_categoryid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.categoryid") %>' />
                        <asp:HiddenField ID="hf_ticketsviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketsviolationid") %>' />
                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' />
                        <asp:HiddenField ID="hf_OscareCourt" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.OscarCourtDetail") %>' />
                        <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refcasenumber") %>' />
                        <asp:HiddenField ID="hf_courtdate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtdatemain","{0:d}") %>' />
                        <asp:HiddenField ID="hf_crm" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' />
                        <asp:HiddenField ID="hf_ctime" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>' />
                        <asp:HiddenField ID="hf_VDesc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>' />
                        <asp:HiddenField ID="hf_courtstatus" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.VerCourtDesc") %>' />
                        <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ticketid_pk") %>' />
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="15%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Ticket Number">
                    <ItemTemplate>
                        <asp:Label ID="lblTicketID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>'
                            Visible="False">
																	</asp:Label>
                        <asp:LinkButton ID="lnkBtn_CaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
																	</asp:LinkButton>
                        <asp:Label ID="lbl_ulstatus" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ulstatus") %>'
                            Visible="False">
																	</asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="15%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Violation Description">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>'>
																	</asp:Label>
                        <asp:Label ID="lblvnpk" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>'
                            Visible="False">
																	</asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="20%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Crt Loc">
                    <ItemTemplate>
                        <asp:Label ID="lbl_court" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'>
																	</asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="8%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lbl_status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.VerCourtDesc") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="8%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Crt Date">
                    <ItemTemplate>
                        <asp:Label ID="lbl_courtdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="10%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Crt Time">
                    <ItemTemplate>
                        <asp:Label ID="lbl_time" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="10%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Crt">
                    <ItemTemplate>
                        <asp:Label ID="lbl_room" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtNumberMain") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="5%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=" Court Info" Visible="False">
                    <ItemTemplate>
                        <table id="court" align="left" cellpadding="0" cellspacing="0" width="191">
                            <tr>
                                <td id="vdate" align="left" style="height: 16px">
                                    <asp:Label ID="lblVerified" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedDate") %>'
                                        Width="198px">
																				</asp:Label>
                                    <asp:Label ID="lbl_VStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedStatus") %>'
                                        Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblAuto" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.AutoDate") %>'
                                        Width="194px">
																				</asp:Label>
                                    <asp:Label ID="lbl_AStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AutoStatus") %>'
                                        Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblScan" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ScanDate") %>'
                                        Width="195px">
																				</asp:Label>
                                    <asp:Label ID="lbl_SStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScanStatus") %>'
                                        Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="25%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Bond" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lbl_bond" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
																	</asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="4%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Price Plan" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lbl_plan" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.PlanDescription") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="8%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Fine">
                    <ItemTemplate>
                        <asp:Label ID="lbl_fineamount" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.FineAmount","{0:C0}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="5%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Bond">
                    <ItemTemplate>
                        <asp:Label ID="lbl_bondamount" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Bondamount","{0:C0}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="clsaspcolumnheader" Width="5%" />
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <div id="div_status" runat="server">
                            <asp:Image ID="img_status" runat="server" ImageUrl="../Images/right.gif" />
                            <asp:HiddenField ID="hf_Discrepency" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.Discrepency") %>' />
                        </div>
                        <asp:Label ID="lbl_cds" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateScan","{0:d}") %>'
                            Visible="false"></asp:Label>
                        <asp:Label ID="lbl_cdm" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:d}") %>'
                            Visible="false"></asp:Label>
                        <asp:Label ID="lbl_cda" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate","{0:d}") %>'
                            Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        
        <TABLE style="DISPLAY: none" id="tbl_plzwait1" width=800><TBODY><TR><TD class="clssubhead" vAlign=middle align=center style="width: 785px"><IMG src="../Images/plzwait.gif" /> Please wait While we update your Violation 
information. </TD></TR></TBODY></TABLE>
         <uc1:UpdateViolation id="UpdateViolation1" runat="server" IsCaseDisposition="false" GridID="dgViolationInfo" DivID="tbl_plzwait1" GridField="hf_ticketviolationid">
        </uc1:UpdateViolation>
            </ContentTemplate>
            
            <Triggers>
            <aspnew:AsyncPostBackTrigger ControlID="UpdateViolation1$btn_popup" EventName="click" />
            
            </Triggers>
              </aspnew:UpdatePanel>
    </form>
</body>
</html>

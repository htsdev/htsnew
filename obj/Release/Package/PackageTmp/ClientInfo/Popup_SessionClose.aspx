<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Popup_SessionClose.aspx.cs" Inherits="lntechNew.ClientInfo.Popup_SessionClose" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Alert</title>
    <base target="_self">
    <script type="text/javascript">    
    
    var t;
    function StartTimer()
    {
        t = window.setTimeout('CloseSession(0);', 60000);
    }    
    
    function CloseSession(flag)
    {
        if(flag==1)
        {
            window.clearTimeout(t);
            window.returnValue = 1;
            window.close();
        }
        else
        {
            window.clearTimeout(t);
            window.returnValue = 0;
            window.close();
        }
    }
    
    </script>
</head>
<body onload="javascript:StartTimer();">
    <form id="form1" runat="server">
    <div>
        <br />
        <br />
        <table style="text-align: center">
            <tr>
                <td>
                    <strong>You have been idle for 5 minutes. Would you like to continue with this session?</strong></td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btn_Yes" runat="server" Text=" Yes " />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btn_No" runat="server" Text="  No " /></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddressCIDValidationReport.aspx.cs"
    Inherits="HTP.Reports.AddressCIDValidationReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>

<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Address Validation Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0" />
        
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
        <table cellspacing="0" cellpadding="0" width="950" align="center" border="0">
            <tr>
                <td align="center">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable">
                
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 70px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkShowAll" runat="server" Text="Show All"  />
                                </span>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btnSubmit_Click"/>
                            </td>
                        </tr>
                    </table>
                
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Address Validation Report
                            </td>
                            <td align="right" class="clssubhead">
                                
                                        <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="Label" ForeColor="Red" Text="Label"></asp:Label>
                </td>
            </tr>            
            <tr>
                <td width="100%">
                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult" >
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text=" Please Wait ......"
                                CssClass="clsLabel"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress>
                    
                    <%--<aspnew:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="updatepnlpaging">
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lblUp" runat="server" Text=" Please Wait ......"
                                CssClass="clsLabel"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress>--%>
                            
                            <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                Width="100%" OnPageIndexChanging="gv_Data_PageIndexChanging" AllowPaging="True"
                                OnRowDataBound="gv_Data_RowDataBound" OnRowCommand="gv_Data_RowCommand"
                                AllowSorting="False" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_SNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CID" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ContactID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlnk_LastName" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DOB">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DOB" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FI">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstNameInitial" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FirstNameInitial") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Matter Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Address" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ContactAddress" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ContactAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" />
                                    </asp:TemplateField>                                    
                                     <asp:TemplateField HeaderText="Follow Up Date" SortExpression="FollowUpDate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FollowUpDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("FollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="img_Add" runat="server" CommandName="AddFollowupDate" ImageUrl="../Images/add.gif" />
                                            <asp:HiddenField ID="hf_Fname" runat="server" Value='<%#Eval("FirstName") %>' />
                                            <asp:HiddenField ID="hf_Lname" runat="server" Value='<%#Eval("LastName") %>' />
                                            <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("TicketID_PK") %>' />
                                            <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("CauseNumber") %>' />
                                            <asp:HiddenField ID="hf_refCaseNumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                            <asp:HiddenField ID="hf_ContactID" runat="server" Value='<%#Eval("ContactID") %>' />
                                        </ItemTemplate>
                                     </asp:TemplateField>                                     
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                    
                </td>
            </tr>
            <tr>
                <td align="center" background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            
            <tr>
                            <td>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            
                                            <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" />
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                 
                            </td>
                        </tr>
             
            <tr>
            
                <td>
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
        </ContentTemplate>
             </aspnew:UpdatePanel>
    </div>
    </form>
    
    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>
    
</body>
</html>

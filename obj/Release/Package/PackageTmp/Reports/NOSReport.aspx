﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NOSReport.aspx.cs" Inherits="HTP.Reports.NOSReport"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Not on System Report</title>
    
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    
    
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

</head>


<body class=" ">

    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>                        
        </aspnew:ScriptManager>
        
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->

                    <asp:Label ID="lbl_message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>

                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Not on System Report</h2>
                                <div class="actions panel_actions pull-right">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                            <ContentTemplate>
                                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" 
                                                    AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                                                    CssClass="table" OnRowCommand="gv_records_RowCommand" OnRowDataBound="gv_records_RowDataBound"
                                                    AllowSorting="True" OnSorting="gv_records_Sorting">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S.No">
                                                            <ItemTemplate>
                                                                &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Last Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_firstname" runat="server" CssClass="form-label" Text='<%# Bind("lastname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="First Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_lastname" runat="server" CssClass="form-label" Text='<%# Bind("firstname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cause No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_causenum" runat="server" CssClass="form-label" Text='<%# Bind("causenumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ticket No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ticketno" runat="server" CssClass="form-label" Text='<%# Bind("refcasenumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Court Loc</u>" SortExpression="shortname">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_crtloc" runat="server" CssClass="form-label" Text='<%# Bind("shortname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Hire Date</u>" SortExpression="hiredatesort">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_hiredate" runat="server" CssClass="form-label" Text='<%# Bind("HireDate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>FollowUpDate</u>" SortExpression="followupdatesort">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_followup" runat="server" CssClass="form-label" Text='<%# Bind("followupdate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                                <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("firstname") %>' />
                                                                <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("lastname") %>' />
                                                                <asp:HiddenField ID="hf_ticketid_pk" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                                <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                                <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("CourtId") %>' />
                                                                <asp:HiddenField ID="hf_casetypeid" runat="server" Value='<%#Eval("CaseTypeId") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </aspnew:UpdatePanel>
                                        <ajaxToolkit:UpdatePanelAnimationExtender TargetControlID="upnlResult" runat="server">
                                        <Animations>
                                                
                                        </Animations>
                                        </ajaxToolkit:UpdatePanelAnimationExtender>

                                    </div>

                                    <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                        <ProgressTemplate>
                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                CssClass="clsLabel"></asp:Label>
                                        </ProgressTemplate>
                                    </aspnew:UpdateProgress>
                                    <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlFollowup" runat="server">
                                                <div class="col-xs-12">
                                                    <uc5:UpdateFollowUpInfo ID="UpdateFollowUpInfo" runat="server" Title="LOR Follow-up" />
                                                    </asp:Panel>
                                                </div>
                                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                PopupControlID="pnlFollowup" TargetControlID="btn">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <asp:Button ID="btn" runat="server" Style="display: none;" />
                                            <ajaxToolkit:AnimationExtender BehaviorID="AEFadeIn" runat="server" Enabled="True"
                                                TargetControlID="hfFadeIn">
                                                <Animations>
                                                    <OnClick>
                                                        <Sequence AnimationTarget="pnlFollowup">                     
                                                            <FadeIn Duration=".3" MinimumOpacity="0" MaximumOpacity="1" />
                                                            <ReSize AnimationTarget="pnlFollowup" height="124px" width="430px" duration ="0" fps="0" unit="px" />
                                                                        
                                                        </Sequence>
                                                    </OnClick>                                                                
                                                </Animations>
                                            </ajaxToolkit:AnimationExtender>
                                            <asp:HiddenField runat="server" ID="hfFadeIn" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </div>
                            </div>
                        </section>
                    </div>
                
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </form>


    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>























<%--<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>                        
        </aspnew:ScriptManager>
        
                        
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="text-align: right">
                                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lbl_message" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                            
                                <td>
                                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td colspan="5" width="780">
                                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                                                            CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand" OnRowDataBound="gv_records_RowDataBound"
                                                            AllowSorting="True" OnSorting="gv_records_Sorting">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S.No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Last Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="Label" Text='<%# Bind("lastname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="Label" Text='<%# Bind("firstname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cause No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_causenum" runat="server" CssClass="Label" Text='<%# Bind("causenumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_ticketno" runat="server" CssClass="label" Text='<%# Bind("refcasenumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Court Loc</u>" SortExpression="shortname">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_crtloc" runat="server" CssClass="Label" Text='<%# Bind("shortname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Hire Date</u>" SortExpression="hiredatesort">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_hiredate" runat="server" CssClass="Label" Text='<%# Bind("HireDate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>FollowUpDate</u>" SortExpression="followupdatesort">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="Label" Text='<%# Bind("followupdate") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                                        <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("firstname") %>' />
                                                                        <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("lastname") %>' />
                                                                        <asp:HiddenField ID="hf_ticketid_pk" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("CourtId") %>' />
                                                                        <asp:HiddenField ID="hf_casetypeid" runat="server" Value='<%#Eval("CaseTypeId") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                                <ajaxToolkit:UpdatePanelAnimationExtender TargetControlID="upnlResult" runat=server>
                                                <Animations>
                                                
                                                </Animations>
                                                </ajaxToolkit:UpdatePanelAnimationExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                                    <ProgressTemplate>
                                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                            CssClass="clsLabel"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlFollowup" runat="server">
                                                            <uc5:UpdateFollowUpInfo ID="UpdateFollowUpInfo" runat="server" Title="LOR Follow-up" />
                                                        </asp:Panel>
                                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                        <ajaxToolkit:AnimationExtender BehaviorID="AEFadeIn" runat="server" Enabled="True"
                                                            TargetControlID="hfFadeIn">
                                                            <Animations>
                                                                <OnClick>
                                                                    <Sequence AnimationTarget="pnlFollowup">                     
                                                                        <FadeIn Duration=".3" MinimumOpacity="0" MaximumOpacity="1" />
                                                                        <ReSize AnimationTarget="pnlFollowup" height="124px" width="430px" duration ="0" fps="0" unit="px" />
                                                                        
                                                                    </Sequence>
                                                                </OnClick>                                                                
                                                            </Animations>
                                                        </ajaxToolkit:AnimationExtender>
                                                        <asp:HiddenField runat="server" ID="hfFadeIn" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:Footer ID="Footer1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="CheckHMC" runat="server" />
    <asp:Panel ID="divDisable" runat="server" Style="z-index: 1; display: none; position: absolute;
        left: 1; top: 1; height: 1px; background-color: Silver; filter: alpha(opacity=50)">
    </asp:Panel>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>--%>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="false" Inherits="HTP.Reports.NonupdatedResetCases"
    MaintainScrollPositionOnPostback="true" CodeBehind="NonupdatedResetCases.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Non-Updated Waiting Cases</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>

    <script language="javascript" type="text/javascript">
	      function OpenEditWin(violationid)
	      {
              var PDFWin
		      PDFWin = window.open("FrmComments.aspx?ticketID="+violationid,"","fullscreen=no,toolbar=no,width=380,height=200,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	  
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" method="post" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="800"
                    align="center" border="0">
                    <tbody>
                        <tr>
                            <td style="height: 14px" colspan="4">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table id="TableSub" cellspacing="0" cellpadding="0" width="800" border="0">
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="right" valign="middle">
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" align="center">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <td align="center" style="width: 100%">
                                            <asp:Label ID="lbl_message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <asp:GridView ID="gv_NonupdatedResetCases" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                CssClass="clsLeftPaddingTable" OnPageIndexChanging="gv_NonupdatedResetCases_PageIndexChanging"
                                                OnRowCommand="gv_NonupdatedResetCases_RowCommand" PageSize="20" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            &nbsp;<asp:HyperLink ID="HLTicketno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                            <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                            <asp:HiddenField ID="hf_TicketViolationID" runat="server" Value="<%# Bind('TicketsViolationID') %>" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Cause No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Causeno" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container.DataItem, "CauseNumber") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ticket No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_RefCaseNo" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Last Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_lastname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="First Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_firstname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt. Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_courtstatus" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document Uploaded">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_followupdate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentUploaded") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderStyle Font-Bold="true" ForeColor="black" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgRemove" runat="server" CommandName="btnRemove" ImageUrl="../Images/cross.gif" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'
                                                                ToolTip="Mark as No Split" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                    Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../images/separator_repeat.gif" height="11" width="780">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc1:Footer ID="Footer1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ALRHearingAlerts.aspx.cs"
    Inherits="HTP.Reports.ALRHearingAlerts" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ALR Alerts</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                    border="0">
                    <tr>
                        <td style="width: 896px; height: 118px">
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                            <table>
                                <tr>
                                    <td colspan="2" style="text-align: right;" valign="middle">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                        CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="Gv_ALR" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="20" OnPageIndexChanging="Gv_ALR_PageIndexChanging"
                                AllowSorting="True" OnSorting="Gv_ALR_Sorting" CellPadding="0" CellSpacing="0"
                                OnRowCommand="Gv_ALR_RowCommand" OnRowDataBound="Gv_ALR_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                Text='<%# Eval("sno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cause Number">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="11%" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CauseNumber" runat="server" Text='<%# Eval("casenumassignedbycourt") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="First Name">
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FirstName" runat="server" Text='<%# Eval("Firstname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LastName" runat="server" Text='<%# Eval("Lastname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Court Date & Time">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtDate" runat="server" Text='<%# Eval("CourtDatemain") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Crt Time">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="left" Width="9%" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_courtnumber" runat="server" Text='<%# Eval("courttime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Room #">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="5%" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CourtNum" runat="server" Text='<%# Eval("CourtNumbermain") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alert Type">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_status" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<u>Follow-up</u>" SortExpression="Followupdate">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="8%" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="left" CssClass="clsLeftPaddingTable" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_follow" runat="server" Text='<%# Eval("Followupdate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add" CommandName="ShowFollowupdate"
                                                runat="server" />
                                            <asp:HiddenField ID="hf_Subdoctypeid" runat="server" Value='<%#Eval("subdoctypeid") %>' />
                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%#Eval("TicketID_PK") %>' />
                                            <asp:HiddenField ID="hf_MaxDays" runat="server" Value='<%#Eval("ALRMaxBusinessDay") %>' />
                                            <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumber") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" background="../Images/separator_repeat.gif" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Past Court Date HMC Follow Up Date" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                            PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                    </ContentTemplate>
                </aspnew:UpdatePanel>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </aspnew:UpdatePanel>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

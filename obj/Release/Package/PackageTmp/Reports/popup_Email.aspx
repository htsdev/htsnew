<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="popup_Email.aspx.cs" Inherits="HCCCTrialDocket.QuickEntry.popup_Email" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Email</title>
    <base target="_self" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>
    <script type="text/javascript">
    function submitform()
    {
   
    var values = document.getElementById('txt_Emails').value;
    if(multiEmail(values))
    {
    window.returnValue = values;
    window.close();
    }
    }
    
    function cancel()
    {
    
    window.returnValue=-1;
    window.close();
    }
    
    function multiEmail(email_field)
		{
			var email = email_field.split(',');
			for(var i = 0; i < email.length; i++) 
			{
				if (!isEmail(email[i])) 
					{
						alert('one or more email addresses entered is invalid');
						return false;
					}
			}
			return true;
		} 		
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="550px">
    <tr>
    <td width="100%" class="clssubhead"  background="../Images/subhead_bg.gif" colSpan="3" height="34px" >
        Email</td>
    </tr>
    <tr>
    <td style="width: 546px" class="label">
        &nbsp;<strong>To :</strong>
        <asp:TextBox ID="txt_Emails" runat="server" CssClass="clsinputadministration" Width="502px"></asp:TextBox></td>
    </tr>
        <tr>
            <td align="right" style="width: 546px">
                <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClientClick="return submitform();" Text="Submit" />&nbsp;<asp:Button
                    ID="btn_Cancel" runat="server" CssClass="clsbutton" Text="Cancel" OnClientClick="cancel();" />
                &nbsp;
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

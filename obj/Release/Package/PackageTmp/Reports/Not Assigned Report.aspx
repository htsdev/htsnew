﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Not Assigned Report.aspx.cs"
    Inherits="HTP.Reports.Not_Assigned_Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Not Assigned Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
        function ArraignmentDocketPopUp(flag)
		{		   
		    window.open("../Reports/ArraignmentDocket.aspx?day=99&Flag="+flag,'',"toolbar=no,status=no,menubar=no,resizable=yes");
		    return false;
		}
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="width: 920px">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td align="right">
                    <%--<asp:ImageButton ID="ImageButton2" OnClick="SendMail" runat="server" ImageUrl="../Images/Email.jpg" />
                    &nbsp;--%>
                   <%-- <asp:ImageButton ID="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg"
                        OnClick="imgPrint_Ds_to_Printer_Click1" ToolTip="Print" />&nbsp;--%>
                  <%--  <asp:ImageButton ID="img_pdfLong" runat="server" ImageUrl="../Images/pdf.jpg" OnClick="ImageButton2_Click"
                        ToolTip="Print Single" />--%>
                   <%-- <asp:ImageButton ID="img_PdfAll" runat="server" ImageUrl="../Images/pdfall.jpg" OnClick="img_PdfAll_Click"
                        ToolTip="Print All" />--%>
                    &nbsp;<asp:ImageButton ID="ImgCrystalSingle" runat="server" ImageUrl="../Images/pdf.jpg"
                        ToolTip="Print Single" OnClientClick="return ArraignmentDocketPopUp(1);" />
                   <%-- <asp:ImageButton ID="ImgCrystalAll" runat="server" ImageUrl="../Images/Cr.jpg" ToolTip="Print All"
                        OnClientClick="return ArraignmentDocketPopUp(2);" />--%>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 32px">
                    <table style="width: 100%">
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: right">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 850px">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="dg_Report" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        Width="100%" AllowPaging="true" PageSize="30" AllowSorting="True" OnPageIndexChanging="dg_Report_PageIndexChanging"
                        OnSorting="dg_Report_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="<u>SNO</u>" SortExpression="sno">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>                                        
                                    </asp:Label>
                                    <asp:HyperLink ID="hl_SNo" Target="_self" runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                        Text='<%# DataBinder.Eval(Container.DataItem, "sno") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Flags</u>" SortExpression="BondFlag">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label14" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Case #</u>" SortExpression="TicketNumber_PK">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="hlnkCaseNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber_PK") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Seq" Visible="False">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label15" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="Lastname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label16" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>First Name</u>" SortExpression="Firstname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label17" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DL</u>" SortExpression="DLNumber">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label18" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DOB</u>" SortExpression="DOB">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_dob" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>MID</u>" SortExpression="MID">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label20" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>Set Date</u>" SortExpression="TrialDateTime">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_arrdate" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                        BackColor="White" ForeColor="Black" AllowPaging="true" PageSize="30" AllowSorting="True"
                        OnPageIndexChanging="dg_Report_PageIndexChanging" OnSorting="dg_Report_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="<u>SNO</u>" SortExpression="sno">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>                                        
                                    </asp:Label>
                                    <asp:HyperLink ID="hl_SNo" Target="_self" ForeColor="Black" runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                        Text='<%# DataBinder.Eval(Container.DataItem, "sno") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Flags</u>" SortExpression="BondFlag">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Case #</u>" SortExpression="TicketNumber_PK">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="hlnkCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber_PK") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Seq" Visible="False">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="Lastname">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>First Name</u>" SortExpression="Firstname">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DL</u>" SortExpression="DLNumber">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label18" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DOB</u>" SortExpression="DOB">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_dob" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>MID</u>" SortExpression="MID">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label20" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>Set Date</u>" SortExpression="TrialDateTime">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_arrdate" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td id="Td1" style="height: 16px">
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 32px">
                    <table style="width: 100%">
                        <tr>
                            <td align="left">
                                <strong class="clssubhead">Outside Trials</strong>
                            </td>
                            <td style="text-align: right">
                                <uc3:PagingControl ID="PagingControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="ostgrd" style="height: 9px" width="100%" colspan="5" height="9">
                    <asp:GridView ID="dg_ReportOT" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        Width="100%" AllowPaging="true" PageSize="30" OnPageIndexChanging="dg_ReportOT_PageIndexChanging"
                        AllowSorting="true" OnSorting="dg_ReportOT_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="<u>SNo</u>" SortExpression="sno">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
                                    </asp:Label>
                                    <asp:HyperLink ID="hl_SNo1" Target="_self" runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>'
                                        Text='<%# DataBinder.Eval(Container.DataItem, "sno") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>Flags</u>" SortExpression="BondFlag">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label142" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>Case #</u>" SortExpression="TicketNumber_PK">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="hlnkCaseNo2" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber_PK") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Seq" Visible="false">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label152" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<U>Last Name</u>" SortExpression="Lastname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label162" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="Firstname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label172" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DL</u>" SortExpression="DLNumber">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="Label182" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>'>
                                    </asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>DOB</u>" SortExpression="DOB">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_dob" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<u>Court Information</U>" SortExpression="CurrentDateSet">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CCDate" CssClass="clsLabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lbl_CCNum" CssClass="clsLabel" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="HF_ArraignmentDocket" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11" style="width: 850px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

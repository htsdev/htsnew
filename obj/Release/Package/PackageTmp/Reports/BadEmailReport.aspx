<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BadEmailReport.aspx.cs"
    Inherits="HTP.Reports.BadEmailReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc5" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfoEmailAddress.ascx" TagName="UpdateFollowUpInfoEmailAddress"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bad Email Address</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    
     //Fahad 7167 12/23/2009 To check Or Validate the From and End Date
     //SAEED 7844 06/03/2010, Comment out this function, not using it now.
//     function CheckDateValidation()
//        {
//            if (IsDatesEqualOrGrater(document.form1.cal_FromDate.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
//            {
//			    alert("Please enter valid date, Start Date must be greater than or equal to 1/1/1900");
//				return false;
//			}
//			
//			if (IsDatesEqualOrGrater(document.form1.cal_ToDate.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
//            {
//			    alert("Please enter valid date, End Date must be greater than or equal to 1/1/1900");
//				return false;
//			}
//            if (IsDatesEqualOrGrater(document.form1.cal_ToDate.value,'MM/dd/yyyy',document.form1.cal_FromDate.value,'MM/dd/yyyy')==false)
//            {
//			    alert("Please enter valid date, End Date must be greater than or equal to Start Date");
//				return false;
//			}
//			
//        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 70px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkFamilyShowAll" runat="server" Text="Show All" />
                                </span>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnFamilySubmit" runat="server" Text="Submit" CssClass="clsbutton"
                                    OnClick="btnFamilySubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Bad Email Address
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                    width="780">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <td align="center" colspan="2" valign="top">
                                        <aspnew:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                    CssClass="clsLabel"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" OnRowDataBound="gv_Records_RowDataBound" AllowPaging="True"
                                            AllowSorting="true" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="20"
                                            OnRowCommand="gv_Records_RowCommand" OnSorting="GridView1_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Existing Email" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Email" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Rep Name</u>" HeaderStyle-CssClass="clssubhead"
                                                    SortExpression="RepLastName" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_replastname" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RepLastName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Court Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CourtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Hire Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_HireDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.HireDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Follow Up Date" SortExpression="FollowUpDate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FollowUpDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="AddFollowupDate" ImageUrl="../Images/add.gif" />
                                                        <asp:HiddenField ID="hf_Fname" runat="server" Value='<%#Eval("FirstName") %>' />
                                                        <asp:HiddenField ID="hf_Lname" runat="server" Value='<%#Eval("LastName") %>' />
                                                        <asp:HiddenField ID="hf_Email" runat="server" Value='<%#Eval("Email") %>' />
                                                        <asp:HiddenField ID="hf_Courtdate" runat="server" Value='<%#Eval("CourtDate") %>' />
                                                        <asp:HiddenField ID="hf_HireDate" runat="server" Value='<%#Eval("HireDate") %>' />
                                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%#Eval("TicketID_PK") %>' />
                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("CauseNumber") %>' />
                                                        <asp:HiddenField ID="hf_refCaseNumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                        <asp:HiddenField ID="hf_comments" runat="server" Value='<%#Eval("GeneralComments") %>' />
                                                        <asp:HiddenField ID="hf_Contact1" runat="server" Value='<%#Eval("Contact1") %>' />
                                                        <asp:HiddenField ID="hf_Contact2" runat="server" Value='<%#Eval("Contact2") %>' />
                                                        <asp:HiddenField ID="hf_Contact3" runat="server" Value='<%#Eval("Contact3") %>' />
                                                        <asp:HiddenField ID="hf_reviewedEmailStatus" runat="server" Value='<%#Eval("ReviewedEmailStatus") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                        width: 780px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            <uc3:UpdateFollowUpInfoEmailAddress ID="UpdateFollowUpInfo2" runat="server" />
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc5:Footer ID="Footer" runat="server"></uc5:Footer>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

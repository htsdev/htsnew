<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocketCloseOutNew.aspx.cs" Inherits="lntechNew.Reports.DocketCloseOutNew" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Docket Close Out</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    function poorman_toggle(id)
{
	var tr = document.getElementById(id);
	if (tr==null) { return; }
	var bExpand = tr.style.display == '';
	tr.style.display = (bExpand ? 'none' : '');
}
function poorman_changeimage(id, sMinus, sPlus)
{
	var img = document.getElementById(id);
	if (img!=null)
	{
	    
	     var bExpand = img.src.indexOf(sPlus.substring(3,sPlus.length -3)) >= 0;
		if (!bExpand)
			img.src = sPlus;
		else
			img.src = sMinus;
	}
}
function closepopup(){	    
		   
		    document.getElementById("pnl_Update").style.display = 'none';
	        //document.getElementById("Disable").style.display = 'none';	
	      
	    }

    </script>
    <style type="text/css">
    .treetable {
    }
    
    .treetable th {
      
       FONT-SIZE: 17pt;
     
    
    }
    
    .treetable td {
      
    
    FONT-SIZE: 7pt;
    
    FONT-FAMILY: Tahoma;
    border-color:3366cc
    
    }
    
    
    a {
      text-decoration:none;
      color:#090;
    }
    
   
  </style>
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
                    
                    </aspnew:ScriptManager>
    <div>
    <table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
    <tr>
    <td width="100%" background="../Images/separator_repeat.gif"  height="11"  >
    </td>
    </tr>
    <tr>
    <td> <ew:CalendarPopup ID="cal_Date" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif" Width="86px">
                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Gray" />
                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                            ForeColor="Black" />
                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                            Font-Size="XX-Small" ForeColor="Black" />
                    </ew:CalendarPopup>
        &nbsp;
        <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClick="btn_Submit_Click"
            Text="Submit" /></td>
    </tr>
    <tr>
    <td width="100%" background="../Images/separator_repeat.gif"  height="11"  >
    </td>
    </tr>
        <tr>
            <td>
            <asp:Repeater ID="rptrtable" runat="server" OnItemDataBound="rptOrder_ItemDataBound" EnableViewState="false" OnItemCommand="rptrtable_ItemCommand">
    <HeaderTemplate>
        <table width="780" id="table2" class="treetable" border=1 style="BORDER-COLLAPSE: collapse" bordercolor="#3366cc">
    </HeaderTemplate>
    <ItemTemplate>
        <tr runat="server" id="rowGroupHeader" valign="middle">
            <td colspan="15" valign="middle">
                <span runat="server" id="idClickable" >
                    <asp:Image ID="idImage" runat="server"  CssClass="button" Width="16" Height="16" />
                    <asp:Label ID="lblGroupName" runat="server" Font-Bold="true"></asp:Label></span></td>
        </tr>
        <tr runat="server" id="rowItem">
            <td >&nbsp;<%# DataBinder.Eval(Container.DataItem, "blankspace")%></td>
            <td >&nbsp; 
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/norgie_closed_dna.gif" />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" TargetControlID="ImageButton1" PopupControlID="pnl_Update" OkControlID="btn_Update" CancelControlID="lbtn_close" runat="server">
                </cc1:ModalPopupExtender>
                <cc1:DragPanelExtender ID="DragPanelExtender1" DragHandleID="Panel2" TargetControlID="pnl_Update" runat="server">
                </cc1:DragPanelExtender>
                </td>
            <td >&nbsp;<%# DataBinder.Eval(Container.DataItem, "LastName") %></td>
            <td >&nbsp;<%# DataBinder.Eval(Container.DataItem, "FirstName") %></td>
            <td >&nbsp;<%# DataBinder.Eval(Container.DataItem, "CourtDateMain", "{0:t}")%></td>
            <td >&nbsp;<%# DataBinder.Eval(Container.DataItem, "CourtNumber") %> </td>
            <td >&nbsp;<%# DataBinder.Eval(Container.DataItem, "ShortDescription") %></td>
            </tr>
        <tr runat="server" id="RowRoomFooter">
            <td bgcolor="gray" colspan="15">
                &nbsp;</td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
            </td>
        </tr>
        <tr>
            <td style="height: 16px">
                <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
        </tr>
    </table>  
    
                <asp:Panel ID="pnl_Update" runat="server" Height="105px" Width="546px" >
                    <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px">Drag Me
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" Height="50px" Width="125px"> 
                    <table width="100%">
                <tr>
                <td width="100%" align="right" background="../Images/subhead_bg.gif" height="34" class ="clssubhead" >
                    <asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>&nbsp;</td>
                </tr>
                <tr>
                <td>
                <table width="100%">
                <tr>
                <td align="center">
                    <asp:RadioButton ID="rb_Disposed" Text = "DISPOSED" runat="server" GroupName="group1" />
                </td>
                <td align="center">
                    <asp:RadioButton ID="rb_Missed" Text = "MISSED" runat="server" GroupName="group1" />
                </td>
                <td align="center">
                    <asp:RadioButton ID="rb_Reset" Text = "RESET" runat="server" GroupName="group1" />
                </td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                <td>
                <table id="tbl_Reset" width="100%">
                <tr>
                <td style="height: 50px" align="center">
                    <asp:DropDownList ID="ddl_Status" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 50px" align="center">
                    <asp:TextBox ID="txt_MM" runat="server" Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_DD" runat="server" Width="25px"></asp:TextBox>/<asp:TextBox ID="txt_YY" runat="server" Width="25px"></asp:TextBox>
                </td>
                <td style="height: 50px" align="center">
                    <asp:DropDownList ID="ddl_Time" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 50px" align="center">
                    <asp:TextBox ID="txt_CourtNumber" runat="server" Width="28px"></asp:TextBox>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                <tr><td align="center" width="100%">
                <asp:Button id="btn_Update" runat="server" Text="Update" CssClass="clsbutton"></asp:Button>
                </td>
                </tr>
                </table>
                    </asp:Panel>
               
                </asp:Panel>
           
    </div>
      
    </form>
</body>
</html>

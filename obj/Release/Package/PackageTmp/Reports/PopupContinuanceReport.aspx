<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopupContinuanceReport.aspx.cs" Inherits="lntechNew.Reports.PopupContinuanceReport" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    <script language="javascript">
        function PopUpShowPreviewPDF(DocID,refType,DocNum,DocExt)
			{
				//window.open ("PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType );
				window.open ("../PaperLess/PreviewMain.aspx?DocID="+ DocID + "&RecType=" + refType + "&DocNum=" + DocNum + "&DocExt=" + DocExt);
				return false;
			}
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="550" align="center" border="0">
				<tr>
					<td background="../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
           <tr>
               <td colspan="2">
               </td>
           </tr>
				<TR>
					<td colSpan="2" style="height: 226px">
						<TABLE class="clsLeftPaddingTable" id="tblsub" borderColor="#ffffff" height="20" cellSpacing="1"
							cellPadding="0" width="100%" align="center" border="1">
							<tr>
							  <td align="center" style="height: 30px; font-size:medium" class="clssubhead">
                                <asp:Label ID="Label1" runat="server" Text="Scan Documents" CssClass="TopHeading"></asp:Label>
                              </td>    
							</tr>
							<TR>
							    <td style="height: 136px">
                                    <asp:GridView ID="GV_ContinuanceDOC" runat="server" AutoGenerateColumns="False" Width="100%" OnRowDataBound="GV_ContinuanceDOC_RowDataBound" CssClass="clsleftpaddingtable">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Date">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldate" runat="server" Text='<%# bind("UPDATEDATETIME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sub Type">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsubtype" runat="server" Text='<%# bind("subdoctype") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldescription" runat="server" Text='<%# bind("ResetDesc") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnview" runat="server" ImageUrl="~/Images/preview.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                             <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_ID") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                          
                                                                            <asp:Label ID="lbldid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.did") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblocrid" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ocrid") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                            <asp:Label ID="lblDocNum" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DOC_Num") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblRecType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.RecordType") %>'
                                                                                Visible="False" Width="10px" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblDocExtension" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.DocExtension") %>'
                                                                                Visible="False" Font-Size="Smaller"></asp:Label>
                                                                                <asp:Label ID="lblEvent" Visible="false" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Events") %>' Font-Size="Smaller"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" Font-Size="Smaller"  />
                                                           </asp:TemplateField>       
                                        </Columns>
                                    </asp:GridView>
							    </td>
							</TR>							
                            <tr>
                                <td align="right">
                                    <asp:Button id="btnClose" runat="server" Width="60px" CssClass="clsbutton" Text="Close" OnClick="btnClose_Click" OnClientClick="window.close();"></asp:Button>
                                </td>
                            </tr>
						</TABLE>
					</td>
				   </TR>
				  
				<tr>
					<td background="../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
			</TABLE>
    </div>
    </form>
</body>
</html>

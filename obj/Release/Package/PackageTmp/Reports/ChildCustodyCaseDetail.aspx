<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ChildCustodyCaseDetail.aspx.cs"
    Inherits="HTP.Reports.ChildCustodyCaseDetail" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <title>Child Custody Case Information</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblmain" runat="server" Visible="False" Font-Bold="True" ForeColor="Red"
                Text="Information Not Found" CssClass="label"></asp:Label>
            <table id="TableMain" runat="server" align="left" border="0" cellpadding="0" cellspacing="0"
                width="750">
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="width: 763px;
                        height: 35px">
                        &nbsp;Case Information</td>
                </tr>
                <tr>
                    <td class="label" colspan="2" style="width: 762px">
                        <table id="tblCaseInformation" runat="server" border="0" cellpadding="1" cellspacing="1" style="width: 100%" class="clsleftpaddingtable">
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Style</td>
                                <td colspan="3">
                                    <asp:Label ID="lblstyle" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Case Number</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lblcasenumber" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    Result</td>
                                <td>
                                    <asp:Label ID="lblresult" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px">
                                    Party Request</td>
                                <td style="width: 290px; height: 16px">
                                    <asp:Label ID="lblpartyrequest" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px; height: 16px">
                                    Setting Date</td>
                                <td style="height: 16px">
                                    <asp:Label ID="lblsettingdate" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    File Location</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lblfilelocation" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    Current Court</td>
                                <td>
                                    <asp:Label ID="lblcurrentcourt" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Case Status</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lblcasestatus" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    File Court</td>
                                <td>
                                    <asp:Label ID="lblfilecourt" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Case Type</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lblcasetype" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    Next Setting Date</td>
                                <td>
                                    <asp:Label ID="lblnextsettingdate" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Setting Type</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lblsettingtype" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    Volume</td>
                                <td>
                                    <asp:Label ID="lblvolume" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Court Location</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lblcourtlocation" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    Plaintiffs</td>
                                <td>
                                    <asp:Label ID="lblplaintiffs" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px">
                                    Rec Load Date</td>
                                <td style="width: 290px; height: 16px">
                                    <asp:Label ID="lblrecloaddate" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px; height: 16px">
                                    Defandents</td>
                                <td style="height: 16px">
                                    <asp:Label ID="lbldefendants" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="clssubhead" style="width: 104px; height: 15px;">
                                    Judgement For</td>
                                <td style="width: 290px">
                                    <asp:Label ID="lbljudgementfor" runat="server" CssClass="label"></asp:Label></td>
                                <td class="clssubhead" style="width: 110px">
                                    Judgement Date</td>
                                <td>
                                    <asp:Label ID="lbljudgementdate" runat="server" CssClass="label"></asp:Label></td>
                            </tr>
                        </table>
                        <asp:Label ID="lblError1" runat="server" Font-Bold="True" ForeColor="Red" Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" colspan="4" style="width: 763px;
                        height: 35px">
                        &nbsp;Connected Parties Information</td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 762px">
                        <asp:GridView ID="gvParties" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                            Width="100%" Visible="False">
                            <Columns>
                                <asp:BoundField HeaderText="Name" DataField="Name" HeaderStyle-CssClass="clssubhead"
                                    ItemStyle-CssClass="GridItemStyleBig"></asp:BoundField>
                                <asp:BoundField HeaderText="Status" DataField="PartyStatus">
                                    <HeaderStyle CssClass="clssubhead" />
                                    <ItemStyle CssClass="GridItemStyleBig" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Associated Attorney" DataField="AssociatedAttorney" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Address" DataField="Address" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="City" DataField="City" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="State" DataField="State" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Zip" DataField="ZipCode" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Address Date" DataField="AddressDate" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Connection" DataField="ConnectionName" ItemStyle-CssClass="GridItemStyleBig">
                                    <HeaderStyle CssClass="clssubhead" />
                                </asp:BoundField>
                                <asp:BoundField />
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblerror2" runat="server" CssClass="label" Font-Bold="True" ForeColor="Red"
                            Text="Information Not Found"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" background="../Images/separator_repeat.gif" colspan="2" style="width: 762px;
                        height: 10px" valign="top">
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="width: 762px" valign="top">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" OnClientClick="return self.close();"
                            Text="Close This Window" Width="119px" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

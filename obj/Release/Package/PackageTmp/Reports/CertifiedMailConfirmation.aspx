﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CertifiedMailConfirmation.aspx.cs"
    Inherits="HTP.Reports.CertifiedMailConfirmation" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>No Certified Mail Confirmation</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/cBoxes.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function EnabledControls()
        {
            document.form1.fromdate.disabled = document.form1.chkShowall.checked ;
            document.form1.todate.disabled =document.form1.chkShowall.checked ;
            //Sarim 5665 03/19/2009
           // document.form1.ddlStatus.disabled =document.form1.chkShowall.checked ;
               
        } // end of EnabledFamilyFilterControls
        
        function CheckDateValidation()
        {
            if (!document.form1.chkShowall.checked && IsDatesEqualOrGrater(document.form1.todate.value,'MM/dd/yyyy',document.form1.fromdate.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, To-Date must be grater then or equal to From-Date");
				document.form1.todate.focus(); 
				return false;
			}
			else
			{
			    return true;
			}
        }
        
        function OpenReport(filePath)
        {
            
            window.open( filePath ,"");
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" method="post" runat="server">
    <table id="tblmain" cellspacing="1" cellpadding="1" align="center" border="0" style="width: 780px">
        <tr>
            <td style="width: 100%">
                <table id="tblsub" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td style="width: 100%">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="10" style="width: 668px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%">
                            <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%;" valign="middle" rowspan="">
                            <strong>
                                <table id="tblmerg" border="0" cellpadding="0" cellspacing="0" width="100%" class="clsleftpaddingtable">
                                    <tr>
                                        <td align="left" valign="middle" class="clsleftpaddingtable" style="height: 34px">
                                            <table id="tbldate" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="width: 65px; height: 21px">
                                                        <strong><span class="clssubhead">Start Date&nbsp; </span></strong>
                                                    </td>
                                                    <td align="left" style="width: 120px; height: 21px">
                                                        <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                            ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                                            LowerBoundDate="1900-01-01">
                                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                BackColor="White"></WeekdayStyle>
                                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                BackColor="LightGray"></WeekendStyle>
                                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                                BackColor="White"></HolidayStyle>
                                                        </ew:CalendarPopup>
                                                    </td>
                                                    <td align="left" style="width: 58px; height: 21px">
                                                        <strong><span class="clssubhead">End Date&nbsp; </span></strong>
                                                    </td>
                                                    <td align="left" style="width: 120px; height: 21px">
                                                        <ew:CalendarPopup ID="todate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                            UpperBoundDate="12/31/9999 23:59:00" Width="90px" LowerBoundDate="1900-01-01">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                    <td align="left" style="width: 58px; height: 21px">
                                                        <strong><span class="clssubhead">Status&nbsp; </span></strong>
                                                    </td>
                                                    <td align="left" style="width: 80px; height: 21px">
                                                        <asp:DropDownList ID="ddlStatus" runat="server">
                                                            <asp:ListItem Value="2" Text="All" />
                                                            <asp:ListItem Value="0" Selected="True" Text="Pending" />
                                                            <asp:ListItem Value="1" Text="Confirmed" />
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 70px; height: 21px">
                                                        <span style="font-size: XX-Small;">
                                                            <asp:CheckBox ID="chkShowall" runat="server" Text="Show All" Checked="true" />
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" Width="70px"
                                                            OnClick="btnSearch_Click" OnClientClick="return CheckDateValidation();" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td background="../Images/separator_repeat.gif" height="10" style="width: 668px">
                                        </td>
                                    </tr>
                                </table>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" colspan="5">
                            <table id="tblgridtrial" cellspacing="0" cellpadding="0" border="0" style="width: 100%">
                                <tr id="rowtrialdetail">
                                    <td style="width: 100%;" valign="top" align="center">
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="false" AllowPaging="false" PageSize="30"
                                            OnRowDataBound="gv_Records_RowDataBound">
                                            <Columns>
                                                <asp:BoundField HeaderText="Client's Full Name" HeaderStyle-CssClass="clssubhead"
                                                    DataField="FullName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Cause No" HeaderStyle-CssClass="clssubhead" DataField="casenumassignedbycourt"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Ticket No" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                    ControlStyle-Width="200px" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnkTicketId" CssClass="Label" runat="server" Text='<%#Eval("refcasenumber") %>'
                                                            NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=1&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:HyperLink>
                                                        <asp:HiddenField ID="hf_TicketId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivery Status" HeaderStyle-CssClass="clssubhead"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("DeliveryStatus") %>' Style="display: none"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sent Date & Time" HeaderStyle-CssClass="clssubhead"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DocPath") %>' Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblCmail" runat="server" Text='<%#Eval("USPSTrackingNumber") %>' Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblPrintDate" runat="server" Text='<%#Eval("ConfirmDate") %>' Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblRep" runat="server" Text='<%#Eval("Rep") %>' Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblBatchDate" runat="server" Text='<%#Eval("PrintDate") %>' Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblPrintRep" runat="server" Text='<%#Eval("PrintRep") %>' Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblEDeliveryFlag" runat="server" Text='<%#Eval("EDeliveryFlag") %>' Style="display: none"></asp:Label>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rep" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Printed" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Upload Date & Time" HeaderStyle-CssClass="clssubhead"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rep" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivery Confirmation" HeaderStyle-CssClass="clssubhead">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblManualUpdate" runat="server" Text='<%#Eval("ManualUpdate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/separator_repeat.gif" style="width: 900px; height: 10px;">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 760px" align="left">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>

    <script language="javascript" type="text/javascript">
        EnabledControls();
    </script>

</body>
</html>

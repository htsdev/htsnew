﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NisiTrackerReport.aspx.cs"
    Inherits="HTP.Reports.NisiTrackerReport" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc5" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="~/WebControls/UpdateNisiContactInfo.ascx" TagName="UpdateNisiContactInfo"  TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>NISI Tracker Report</title>
    <style type ="text/css">
        .clsLeftPaddingTable
        {
            padding-left: 5px;
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
            text-align: center;
        }
        
        .clsLeftPaddingLable
        {
            padding-left: 3px;
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
            text-align: left;
        }
        
        .clsRightPaddingLable
        {
            padding-Right: 3px;
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            background-color: #EFF4FB;
            text-align: Right;
        }
        .clsInputadministration
        {
            border-right: #3366cc 1px solid;
            border-top: #3366cc 1px solid;
            font-size: 8pt;
            border-left: #3366cc 1px solid;
            width: 90px;
            color: #123160;
            border-bottom: #3366cc 1px solid;
            font-family: Tahoma;
            background-color: white;
            text-align: left;
        }
        .clscomboBox
        {
            border-right: 1px solid;
            border-top: 1px solid;
            font-weight: normal;
            list-style-position: outside;
            font-size: 8pt;
            border-left: 1px solid;
            color: #123160;
            border-bottom: 1px solid;
            font-family: Tahoma;
            list-style-type: square;
            background-color: white;
        }
        .labelRight
        {
	       font-family: Tahoma;
	       font-size: 8pt;
	       color: #123160;
	       border-bottom-width: 0;
	       border-left-width: 0;
	       border-right-width: 0;
	       border-top-width: 0;
	       text-align: right;
        }


        .hideGridColumn
        {
        	display:none;
        }

        .clsbutton
        {
            font-weight: bold;
            font-size: 8pt;
            border-left-color: #ffcc66;
            border-bottom-color: #ffcc66;
            cursor: hand;
            color: #3366cc;
            border-top-color: #ffcc66;
            font-family: Tahoma;
            background-color: #ffcc66;
            text-align: center;
            border-right-color: #ffcc66;
        }
       
    </style>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    
    // Used to Select/Deselect all the check boxes from Grid
    function select_deselectAll(chkVal, idVal) 
	{ 
	    var frm = document.forms[0];
	    // Loop through all elements
	    for (i=0; i<frm.length; i++) 
	    {
		    if(frm.elements[i].name.substring(0,2)=='gv' )
		    {
			    // Look for our Header Template's Checkbox
			    if (idVal.indexOf ('ChkPrintAnswerHeader') != -1) 
			    {
				    // Check if main checkbox is checked, then select or deselect Gridview checkboxes 					
				    if(chkVal == true) 
					    {			
						    frm.elements[i].checked = true;										    		
					    } 
					    else 
					    {		
		    		        frm.elements[i].checked = false;
					    }
				 }
			 }      
		 }
	}
	
	// This Function is used to check weather letters are selected to print the letters. . . 
	function checkForPrintAnswerLetters ()
	{
	    var frm = document.forms[0]; // Getting Form Id
	    var notfoundchecked = true;
	    // Loop through all elements
	    for (i=0; i<frm.length; i++) 
	    {
		    if(frm.elements[i].name.substring(0,2)=='gv' )
		    {   // If header checkbox is checked then break the loop
		        if (document.getElementById('gv_Records_ctl01_ChkPrintAnswerHeader').checked == true)
		        {
			        notfoundchecked = false;
			        break;
			    }
			    else
			    {
			        
			        var chkControlName = frm.elements[i].name;
                    if (chkControlName.lastIndexOf("ChkPrintAnser") != -1)  // checking if row control contains the checkbox name
                    {
                        if (frm.elements[i].checked == true) // if checkbox is not checked
                        {
                            notfoundchecked = false;
                        }
                    }
			    }
			 }      
		 }
		 if (notfoundchecked)
		 {
		    alert ("Please select record to print the answer");
		    return false;
		 }
	}
	
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager" runat="server" AsyncPostBackTimeout="3600" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class ="labelRight"> NISI Status : </td>
                            <td><asp:DropDownList ID = "ddl_nisiStatus" CssClass = "clscomboBox"  runat = "server" AutoPostBack= "false" /></td>
                            <td class ="labelRight"> Underlying Violation Status : </td>
                            <td> <asp:DropDownList ID = "ddl_UnderlyingViolationsStatus" CssClass = "clscomboBox" runat = "server" AutoPostBack= "false" /></td>
                            <td>  </td>
                            <td> <asp:Button ID = "btn_Submit" Text = "Submit" runat = "server" CssClass ="clsbutton" onclick="btn_Submit_Click" /> </td>
                            <td id = "td_printAnswers" runat = "server" class ="labelRight">
                            <asp:Button ID = "Btn_PrintAnswers" Text = "Print Answer" runat = "server" OnClientClick = "return checkForPrintAnswerLetters()" CssClass ="clsbutton" OnClick = "Btn_PrintAnswers_Click" /> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                NISI Tracker Report
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                    width="780">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <td align="center" colspan="2" valign="top">
                                    <div id="divgrid" runat="server" style="min-height: 300px; height: auto !important;
                                            height: 600px; width: 980px; overflow: auto;">
                                        <aspnew:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                    CssClass="clsLabel"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                        
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            OnRowDataBound="gv_Records_RowDataBound" AllowPaging="True" BackColor = "#EFF4FB"
                                            AllowSorting="true" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="20" RowStyle-HorizontalAlign = "Center"
                                            OnRowCommand="gv_Records_RowCommand" OnSorting="GridView1_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                 <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.TicketID")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="RowId" Visible="false">
                                                 <ItemTemplate>
                                                    <asp:Label ID="LblRowId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RowId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                </asp:TemplateField>
                                                
                                                 <asp:TemplateField HeaderText="NISI Generated Date" SortExpression="NISIGeneratedDateSortable" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "120px" HeaderStyle-HorizontalAlign="Center">
                                                  <ItemTemplate>
                                                        <asp:Label ID="LblNisiGeneratedDate" Width = "120px"  runat="server" CssClass="clsRightPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.NISIGeneratedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Last Name" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "120px" HeaderStyle-HorizontalAlign="Center">
                                                 <ItemTemplate>
                                                        <asp:Label ID="LblLastName" Width ="120px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="First Name" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "120px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFirstName" Width = "120px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Mid Name" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "50px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblMidName" Width = "50px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.MidName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Bond Amount" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "80px" HeaderStyle-HorizontalAlign="Center">
                                                     <ItemTemplate>
                                                        <asp:Label ID="LblBondAmount" Width ="80px" runat="server" CssClass="clsRightPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.BondAmount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="NISI #" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "120px" HeaderStyle-HorizontalAlign="Center">
                                                 <ItemTemplate>
                                                        <asp:Label ID="LblNisiCause" Width ="120px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.NISICause") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="NISI Status - Date, Court Room @ Time" HeaderStyle-CssClass="clssubhead" SortExpression="NISIStatus" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "220px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblNisiStatus" Width ="220px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.NISIStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                 <asp:TemplateField HeaderText="Underlying Violation Case #" SortExpression = "UnderlyingViolationCase" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "120px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblUnderlyingViolationCase" Width = "120px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.UnderlyingViolationCase") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Violation Description" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "170px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblViolationDescription" Width = "170px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField HeaderText="Violation Case Status - Date, Court Room @ Time" SortExpression="ViolationCaseStatusInfo"  HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "170px" HeaderStyle-HorizontalAlign="Center" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblViolationCaseStatusDateCourtRoomTime" Width = "170px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationCaseStatusInfo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Print Answer" HeaderStyle-CssClass="clssubhead" SortExpression = "PrintAnswerSortable" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "100px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID = "LnkBtnPrintAnswer" Width = "100px" runat = "server" Text ='<%# DataBinder.Eval(Container, "DataItem.PrintAnswer") %>'></asp:LinkButton>
                                                        <%--<asp:Label ID="LblPrintAnswer" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.PrintAnswer") %>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField HeaderText="Contact Status" HeaderStyle-CssClass="clssubhead" SortExpression="ContactStatus" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "140px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                    <asp:Label ID = "LblImgAdd" Width ="140px" runat = "server" Visible = "false" CssClass="clsLeftPaddingLable" Text = "*****" />
                                                    <asp:LinkButton ID="ImgAdd" Width = "140px" runat="server" CssClass ="clsleftpaddinglable" CommandName="AddFollowupDate" Text='<%# DataBinder.Eval(Container, "DataItem.ContactStatus") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Notes" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "170px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblNotes" Width ="170px" Visible = "false" runat="server" CssClass="clsLeftPaddingLable" Text= "*****"></asp:Label>
                                                        <asp:TextBox ID="TxtNotes" Width = "170px" runat="server" Visible = "true" TextMode = "MultiLine" ReadOnly = "true" Height = "35px" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                 <asp:TemplateField HeaderText="Contact Numbers" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "170px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblContactNumbers" Width = "170px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.ContactNumbers") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField HeaderText="Language" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "170px" HeaderStyle-HorizontalAlign="Center" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblLanguage" Width = "170px" runat="server" CssClass="clsLeftPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.Language") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Email Address" HeaderStyle-CssClass="clssubhead" ItemStyle-HorizontalAlign = "Center"  HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID = "LblBtnEmail"  runat = "server" CssClass="clsLeftPaddingLable" Text = "*****" Visible = "false" />
                                                        <asp:HyperLink ID = "HyprlnkEmail" NavigateUrl = '<%# String.Format( "mailto:{0}", DataBinder.Eval(Container, "DataItem.EmailAddress"))  %>' runat = "server" Text = '<%# DataBinder.Eval(Container, "DataItem.EmailAddress") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                               
                                                <asp:TemplateField HeaderText="Follow Up Date" SortExpression="FollowUpDateSortable" ItemStyle-HorizontalAlign = "Center"  ControlStyle-Width = "100px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFollowUpDate" Width = "100px" runat="server" CssClass="clsRightPaddingLable" Text='<%# DataBinder.Eval(Container, "DataItem.FollowUpDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderTemplate> 
                                                        <asp:CheckBox ID = "ChkPrintAnswerHeader" onclick="return select_deselectAll(this.checked, this.id);" runat = "server" /> 
                                                    </HeaderTemplate>
                                                   <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID = "ChkPrintAnser" runat = "server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ControlStyle-CssClass ="hideGridColumn" >
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="HfFname" runat="server" Value='<%#Eval("FirstName") %>' />
                                                        <asp:HiddenField ID="HfLname" runat="server" Value='<%#Eval("LastName") %>' />
                                                        <asp:HiddenField ID="HfEmail" runat="server" Value='<%#Eval("EmailAddress") %>' />
                                                        <asp:HiddenField ID="HfTicketId" runat="server" Value='<%#Eval("TicketID") %>' />
                                                        <asp:HiddenField ID="HfNisiCauseno" runat="server" Value='<%#Eval("NISICause") %>' />
                                                        <asp:HiddenField ID="HfUnderlyingCause" runat="server" Value='<%#Eval("TicketNumber") %>' />
                                                        <asp:HiddenField ID="HfComments" runat="server" Value='<%#Eval("Notes") %>' />
                                                        <asp:HiddenField ID="HfContactType" runat="server" Value='<%#Eval("ContactTypeId") %>' />
                                                        <asp:HiddenField ID="HfNisiGeneratedDate" runat="server" Value='<%#Eval("NISIGeneratedDate") %>' />
                                                        <asp:HiddenField ID="HfFollowUpDateFreezed" runat="server" Value='<%#Eval("IsNisiFolloupDateFreezed") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                     </div>   
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                        width: 780px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            <uc3:UpdateNisiContactInfo ID="UpdateFollowUpInfo2" runat="server" />
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc5:Footer ID="Footer" runat="server"></uc5:Footer>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 10003;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="AuthorizeNetReport.aspx.cs"
    Inherits="lntechNew.Reports.AuthorizeNetReport" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Authorize.Net Report</title>
    

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />


    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        
        
    <link href="../assets/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" media="screen"/>

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <!-- CORE CSS TEMPLATE - END -->

    
    
    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
    
    function SelectionChanged()
    {//a;
//        var ddl_db = document.getElementById('ddl_Database');
//        var ddl_src = document.getElementById('ddl_Source');
//        if(ddl_db.value == '2')
//        {
//            ddl_src.value = '1';
//            ddl_src.disabled = true;
//        }
//        else
//        {
//            ddl_src.disabled = false;
//        }
    }
    
    
    function CheckTransactionType()
    {
        //Zeeshan Ahmed 3931 05/27/2008
        var ddl_db = document.getElementById('ddl_Database');
        var ddl_src = document.getElementById('ddl_Source');
    
        if(ddl_db.value == '2')
        {
            if ( ddl_src.value == '2')
            {
                alert("Manual CC transactions not exists for Dallas.")
                return false;
            }
        }
        return true;
    }
   
    </script>
</head>

    <body onload="SelectionChanged()">

        <form id="form1" runat="server">
        

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>


            <!-- START CONTENT -->
        <section id="main-content" class=" ">
            <section class="wrapper main-wrapper row" style=''>

                <div class='col-xs-12'>
                    <div class="page-title">

                        <div class="pull-left">

                            <!-- PAGE HEADING TAG - START -->
                            <h1 class="title">Authorized.Net Report</h1>
                            <!-- PAGE HEADING TAG - END -->    

                        </div>
                                
                    </div>
                </div>

                <div class="clearfix">

                </div>
                <!-- MAIN CONTENT AREA STARTS -->
    
                <div class="col-xs-12">
                    <section class="box ">

                        <header class="panel_header">
                            <h2 class="title pull-left">Search</h2>
                        </header>

                        <div class="content-body">

                            <div class="row">

                                <div class="col-md-2 col-sm-3 col-xs-4">

                                    <div class="form-group">
                                        <div class="controls">
                                            <ew:CalendarPopup ID="cal_startdate" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif">
                                                <TextboxLabelStyle CssClass="form-control" />
                                            </ew:CalendarPopup>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-2 col-sm-3 col-xs-4">

                        <div class="form-group">
                            <div class="controls">
                                <ew:CalendarPopup ID="cal_enddate" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif">
                                    <TextboxLabelStyle CssClass="form-control" />
                                </ew:CalendarPopup>
                            </div>
                        </div>

                    </div>

                                <div class="col-md-2 col-sm-3 col-xs-4">

                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:DropDownList ID="ddl_Database" runat="server" CssClass="form-control m-bot15" onChange = "SelectionChanged()">
                                                <asp:ListItem Selected="True" Value="1">Houston</asp:ListItem>
                                                <asp:ListItem Value="2">Dallas</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-2 col-sm-3 col-xs-4">

                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:DropDownList
                                                ID="ddl_source" runat="server" CssClass="clsinputcombo">
                                                <asp:ListItem Value="-1">All Sources</asp:ListItem>
                                                <asp:ListItem Value="0">Public Site</asp:ListItem>
                                                <asp:ListItem Value="1">Traffic System</asp:ListItem>
                                                <asp:ListItem Value="2">Manual CC</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-2 col-sm-3 col-xs-4">

                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:DropDownList
                                                ID="ddl_transtype" runat="server" CssClass="clsinputcombo">
                                                <asp:ListItem Value="-1">All Sources</asp:ListItem>
                                                <asp:ListItem Value="0">Public Site</asp:ListItem>
                                                <asp:ListItem Value="1">Traffic System</asp:ListItem>
                                                <asp:ListItem Value="2">Manual CC</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:Button ID="btn_Submit" runat="server" CssClass="btn btn-primary pull-right" Text="Submit" OnClick="btn_Submit_Click" OnClientClick="return CheckTransactionType();" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>

                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Result</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <asp:CheckBox ID="chk_ShowTestTrans" runat="server" Checked="True" OnCheckedChanged="chk_ShowTestTrans_CheckedChanged" Text="Show Test Transactions" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">

                                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="table" Width="100%" AllowSorting="True" OnSorting="gv_Records_Sorting" OnRowDataBound="gv_Records_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Trans ID" SortExpression="TransID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TransID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TransID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ticket Number" SortExpression="TicketNumber">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hl_TicketNumber" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:HyperLink><asp:HiddenField
                                                            ID="hf_ticketid" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Invoice Number" SortExpression="InvoiceNumber">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_InvoiceNumber" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trans Status" SortExpression="TransStatus">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TransStatus" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TransStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Submit Date" SortExpression="SubmitDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SubmitDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer" SortExpression="Customer">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Customer" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Customer") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Card" SortExpression="Card">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Card" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Card") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sales Rep" SortExpression="SalesRep">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SalesRep" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.SalesRep") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Method" SortExpression="PaymentMethod">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PaymentMethod" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentMethod") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Amount" SortExpression="Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_PaymentAmount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Amount","{0:C}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Void" SortExpression="isVoid">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Void" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.isVoid") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Appr." SortExpression="isApproved">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_approved" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.isApproved") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Source" SortExpression="Source">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Source" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Source") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                    <%--<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Mark</td>
                                                <td>Otto</td>
                                                <td>@mdo</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Jacob</td>
                                                <td>Thornton</td>
                                                <td>@fat</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Larry</td>
                                                <td>the Bird</td>
                                                <td>@twitter</td>
                                            </tr>
                                        </tbody>
                                    </table>--%>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->

            </section>
        </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->
    </form>

    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>
































<%--<body onload="SelectionChanged()">
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td align="left" style="height: 16px">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 140px; height: 23px;">
                                    <asp:Label ID="Label1" runat="server" CssClass="clslabel" Text="Date: "></asp:Label>
                                    <ew:CalendarPopup ID="cal_startdate" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif"
                                        Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clsinputadministration" />
                                    </ew:CalendarPopup>
                                </td>
                                <td style="height: 23px; width: 107px;">
                                    <ew:CalendarPopup ID="cal_enddate" runat="server" ControlDisplay="TextBoxImage" ImageUrl="../Images/calendar.gif"
                                        Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clsinputadministration" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td style="width: 130px; height: 23px;">
                                    <asp:Label ID="Label2" runat="server" CssClass="clslabel" Text="Database: "></asp:Label>
                                    <asp:DropDownList ID="ddl_Database" runat="server" CssClass="clsinputcombo" onChange = "SelectionChanged()">
                                        <asp:ListItem Selected="True" Value="1">Houston</asp:ListItem>
                                        <asp:ListItem Value="2">Dallas</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="width: 141px; height: 23px;">
                                    <asp:Label ID="Label3" runat="server" CssClass="clslabel" Text="Source: "></asp:Label><asp:DropDownList
                                        ID="ddl_source" runat="server" CssClass="clsinputcombo">
                                        <asp:ListItem Value="-1">All Sources</asp:ListItem>
                                        <asp:ListItem Value="0">Public Site</asp:ListItem>
                                        <asp:ListItem Value="1">Traffic System</asp:ListItem>
                                        <asp:ListItem Value="2">Manual CC</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="width: 185px; height: 23px;">
                                    <asp:Label ID="Label4" runat="server" CssClass="clslabel" Text="Transaction Type: "></asp:Label><asp:DropDownList
                                        ID="ddl_transtype" runat="server" CssClass="clsinputcombo">
                                        <asp:ListItem Selected="True" Value="-1">All</asp:ListItem>
                                        <asp:ListItem Value="1">Approved</asp:ListItem>
                                        <asp:ListItem Value="2">Void</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="height: 23px">
                                    <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_Submit_Click" OnClientClick="return CheckTransactionType();" /></td>
                            </tr>
                        </table>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" background="../Images/separator_repeat.gif" style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 16px" align="left">
                        <asp:CheckBox ID="chk_ShowTestTrans" runat="server" Checked="True"
                            OnCheckedChanged="chk_ShowTestTrans_CheckedChanged" Text="Show Test Transactions"
                            CssClass="clslabelnew" /></td>
                </tr>
                <tr>
                    <td align="center">
                        &nbsp;<asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center" style="height: 16px">
                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                            Width="100%" AllowSorting="True" OnSorting="gv_Records_Sorting" OnRowDataBound="gv_Records_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Trans ID" SortExpression="TransID">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TransID" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.TransID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket Number" SortExpression="TicketNumber">
                                    <ItemTemplate>
                                        &nbsp;<asp:HyperLink ID="hl_TicketNumber" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>'></asp:HyperLink><asp:HiddenField
                                                ID="hf_ticketid" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Invoice Number" SortExpression="InvoiceNumber">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_InvoiceNumber" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trans Status" SortExpression="TransStatus">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_TransStatus" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.TransStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Submit Date" SortExpression="SubmitDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_SubmitDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.SubmitDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer" SortExpression="Customer">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Customer" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Customer") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Card" SortExpression="Card">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Card" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Card") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Rep" SortExpression="SalesRep">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_SalesRep" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.SalesRep") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment Method" SortExpression="PaymentMethod">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PaymentMethod" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentMethod") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment Amount" SortExpression="Amount">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PaymentAmount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Amount","{0:C}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Void" SortExpression="isVoid">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Void" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.isVoid") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Appr." SortExpression="isApproved">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_approved" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.isApproved") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Source" SortExpression="Source">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Source" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Source") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>--%>
</html>

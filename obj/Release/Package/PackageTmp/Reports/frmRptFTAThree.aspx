<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.frmRptFTAThree" Codebehind="frmRptFTAThree.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>FTA Three Analysis</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
			var dtfrom=document.getElementById("dtFrom").value;
			var dtTo=document.getElementById("dtTo").value;
			if(dtfrom > dtTo){
				alert("Please Specify Valid Future Date!");
				return false;
			}
			
		}
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780" align="center"
				border="0">
				<TR>
					<TD align="left">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<tr>
					<td background="../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD style="width: 4%">&nbsp;From: </TD>
								<TD style="width: 19%" align="left">&nbsp;
									<ew:calendarpopup id="dtFrom" runat="server" Font-Size="8pt" Font-Names="Tahoma" DisplayOffsetY="20"
										ToolTip="Mail Date From" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
										ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Width="109px" SelectedDate="2006-02-21"
										EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
								<TD style="width: 4%">&nbsp;To: </TD>
								<TD style="width: 31%">&nbsp;
									<ew:calendarpopup id="dtTo" runat="server" Font-Size="8pt" Font-Names="Tahoma" DisplayOffsetY="20"
										ToolTip="Mail Date To" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
										ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Width="117px" SelectedDate="2006-02-21"
										EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
								<TD>&nbsp;
									<asp:button id="btnSubmit" runat="server" Text="Submit" CssClass="clsbutton"></asp:button></TD>
								<TD align="right">
                                    &nbsp;<asp:imagebutton id="imgBtnExcel" runat="server" ImageUrl="../Images/excel_icon.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
						<asp:label id="lblMessage" runat="server" Width="285px" Height="13px" ForeColor="Red"></asp:label></TD>
				</TR>
				<tr>
					<td background="../images/headbar_headerextend.gif" colSpan="2" height="5"></td>
				</tr>
				<TR>
								<TD width="100%" background="../Images/separator_repeat.gif"  height="10"></TD>
							</TR>
				<TR>
					<td>
						<TABLE id="tblTitle" style="WIDTH: 812px; HEIGHT: 10px" cellSpacing="0" cellPadding="0"
							width="812" border="0">
							<tr>
								<td class="clssubhead" height = "34px" background="../../Images/subhead_bg.gif">&nbsp;</td>
								<td class="clssubhead" align="right" height ="34px" background="../../Images/subhead_bg.gif">
									<TABLE id="tblPageNavigation" style="WIDTH: 319px; HEIGHT: 18px" cellSpacing="0" cellPadding="0"
										border="0">
										<TR>
											<TD class="clslabel" align="right">
                                                <strong>Page #</strong></TD>
											<TD align="left"><asp:label id="lblCurrPage" runat="server" Width="25px" CssClass="clslabel" Font-Bold="True"></asp:label></TD>
											<TD class="clslabel" align="right" style="width: 27px">
                                                <strong>Goto</strong></TD>
											<TD align="right" width="25%"><asp:dropdownlist id="ddlPageNo" runat="server" CssClass="clsinputcombo" AutoPostBack="True"></asp:dropdownlist></TD>
											<TD align="right" width="25%"><asp:checkbox id="chkAll" runat="server" Text="Show All" CssClass="clslabel" AutoPostBack="True" Font-Bold="True"></asp:checkbox>&nbsp;&nbsp;</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</TABLE>
					</td>
				</TR>
				
				<TR>
					<TD align="center">
						<TABLE id="tblGrid" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="100%"><asp:datagrid id="dgFTA" runat="server" Width="100%" CssClass="clsleftpaddingtable" AllowPaging="True"
										ShowFooter="True" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="No.">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="lblSerial" runat="server"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Mail Date">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=lblMailDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MailDate", "{0:d}") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="LetterCount" HeaderText="Letters">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Quoted">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:HyperLink id=hlkQuoted runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quote") %>'>
													</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Hired">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:HyperLink id=hlkHired runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Hired") %>'>
													</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Total" HeaderText="Total">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="PostalCost" HeaderText="Postal Cost">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Revenue" HeaderText="Revenue">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Profit" HeaderText="Profit">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TotalReturn" HeaderText="Total Return">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="PercentTotal" HeaderText="% Total">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="PercentHired" HeaderText="% Hires">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="HiresTotalRatio" HeaderText="Hires / Total">
												<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Prev " HorizontalAlign="Center"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif"  height="10"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="4">
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center"></TD>
				</TR>
				<TR>
					<TD align="center"></TD>
				</TR>
				<TR>
					<TD align="center"></TD>
				</TR>
				<TR>
					<TD align="center"></TD>
				</TR>
				<TR>
					<TD align="left"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LORconfirmation.aspx.cs"
    Inherits="HTP.Reports.LORconfirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No LOR Confirmation Alert</title>
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
</head>

<body class=" ">

    <form id="form1" runat="server">
        
        <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
        
            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">No LOR Confirmation Alert</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                
                        </div>
                    </div>
                
                    <div class="clearfix"></div>
                
                    <!-- MAIN CONTENT AREA STARTS -->
                
                    <asp:Label ID="lblMessage" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>

                    <div class="col-xs-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">No LOR Confirmation Report</h2>
                                <div class="actions panel_actions pull-right">
                	                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                    Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>

                                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                            <ContentTemplate>
                                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                                    PageSize="30" CssClass="table" AllowSorting="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.Ticketid_pk")+"&search=0" %>'
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="CauseNumber" HeaderText="Cause Number" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ticketnumber" HeaderText="Ticket Number" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Status" HeaderText="Status" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CourtDate" HeaderText="Court Date" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CourtTime" HeaderText="Court Time" HtmlEncode="false">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CourtRoom" HeaderText="Court Room" HtmlEncode="false">
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </aspnew:UpdatePanel>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>







































<%--<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                        <table style="width: 100%;">
                            <tr>
                                <td align="left" class="clssubhead">
                                    &nbsp;No LOR Confirmation Report</td>
                                <td style="text-align: right;">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                            <ProgressTemplate>
                                <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                    Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                            </ProgressTemplate>
                        </aspnew:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                    AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                    PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.Ticketid_pk")+"&search=0" %>'
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CauseNumber" HeaderText="Cause Number" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ticketnumber" HeaderText="Ticket Number" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FirstName" HeaderText="First Name" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Status" HeaderText="Status" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CourtDate" HeaderText="Court Date" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CourtTime" HeaderText="Court Time" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CourtRoom" HeaderText="Court Room" HtmlEncode="false">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
        </TD></TR></TBODY></TABLE></DIV>
    </form>
</body>--%>
</html>

<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.frmUpdate" Codebehind="ScanRepPopup.aspx.cs" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Violation Update Screen</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script src="../Scripts/jsDate.js" type="text/javascript"></script>
		<script>
		function Validate()
		{
			
			var casenumber =  document.frmUpdate.txt_TicketNo.value;
			if (casenumber == "" ) 
			{
				alert("Invalid Ticket Number.");
				document.frmUpdate.txt_TicketNo.focus();
				return false;
			}
			if (casenumber.indexOf(" ") != -1 )
			 {
				alert("Please make sure that there are no spaces in the ticketnumber input box") 
				document.frmUpdate.txt_TicketNo.focus();
				return false;
			 }

			var sequnumber = document.frmUpdate.txt_SequenceNo.value;
			if (sequnumber.indexOf(" ") != -1 )
			 {
				alert("Please make sure that there are no spaces in the SequenceNumber input box") 
				document.frmUpdate.txt_SequenceNo.focus();
				return false;
			 }

			var courtnumber = document.frmUpdate.txt_courtno.value;
			if (courtnumber.indexOf(" ") != -1 )
			 {
				alert("Please make sure that there are no spaces in the CourtNumber input box") 
				document.frmUpdate.txt_courtno.focus();
				return false;
			 }
			 var refcasenumber = casenumber;
			 var seqnumber = sequnumber;
			 var courtid = document.getElementById("lbl_courtid").innerText;
			 var datetype =document.frmUpdate.ddl_Status.value;
			 var courtnum = document.frmUpdate.txt_courtno.value ;
			 var varhours = document.frmUpdate.ddl_Time.value;

			if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003"))
			{
				if ((refcasenumber.substring(0,1) == "0" ) || (refcasenumber.substring(0,1) == "M") || (refcasenumber.substring(0,1) == "A")) 
				{
					if (seqnumber == "" )
					{
						alert("Invalid Sequence Number.")
						document.frmUpdate.txt_SequenceNo.focus();
						return false;
					}

					if (refcasenumber.length != 9) 
					{
						alert("It requires 9 characters If Case Number starts with '0' or 'M' or 'A'")
						document.frmUpdate.txt_TicketNo.focus();
						return false;
					}	
				}

				if (refcasenumber.substring(0,1) == "F" ) 
				{
					if (refcasenumber.length != 8) 
					{
						alert("It requires 8 characters if Case Number starts with 'F'")
						document.frmUpdate.txt_TicketNo.focus();
						return false;
					}	
				
					if ((seqnumber != "" ) && (seqnumber != 0)) 
					{
						alert("F Tickets don't have any sequence number.")
						document.frmUpdate.txt_SequenceNo.focus();
						return false;
					}	
				}
				if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
				{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM") && (varhours != "10:30 AM") && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							document.frmUpdate.ddl_Time.focus();
							return false;
						}
						if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
						{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								document.frmUpdate.txt_courtno.focus();
								return false;
						}
						
				}
					
				if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
				{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						document.frmUpdate.ddl_Status.focus();
						return false;
				}

				if (courtid != "3003")
				{
					if ((courtnum == "18") )
					{
							alert("This court number can only be set for Dairy Ashford Court")
							document.frmUpdate.txt_courtno.focus();
							return false;
					}
				}
				if (courtid != "3002") 
				{

					if ((courtnum == "13") || (courtnum == "14") ) 
					{
					alert("This court number can only be set for Mykawa")
					document.frmUpdate.txt_courtno.focus();
					return false;
					}
				}
			}
			if ((document.frmUpdate.ddl_Date_Month.value == "--") || (document.frmUpdate.ddl_Date_Day.value == "--") || (document.frmUpdate.ddl_Date_Year.value == "--")) 
			{
				alert("Invalid Court Date.")
				return false;						
			}

			if (document.frmUpdate.ddl_Status.value == "0" ) 
			{
				alert("Invalid Status.")
				document.frmUpdate.ddl_Status.focus();
				return false;
			}

			if (document.frmUpdate.ddl_Time.value == "--" )
			 {
				alert("Invalid Court Time.")
				document.frmUpdate.ddl_Time.focus();
				return false;
			}

			if (document.frmUpdate.txt_courtno.value == "" )
			 {
				alert("Invalid Court Number.")
				document.frmUpdate.txt_courtno.focus();
				return false;
			}	
						
	}
	   
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="frmUpdate" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="450" align="center" border="0">
				<tr>
					<td background="../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
				<TR>
					<td colSpan="2">
						<TABLE class="clsLeftPaddingTable" id="tblsub" borderColor="#ffffff" height="20" cellSpacing="1"
							cellPadding="0" width="100%" align="center" border="1">
							<TR>
							</TR>
							<tr>
								<td class="clsaspcolumnheader" height="22">Case Number:&nbsp;
								</td>
								<TD height="22"><asp:textbox id="txt_TicketNo" runat="server" CssClass="clsinputadministration" Width="80px"></asp:textbox>&nbsp;-
									<asp:textbox id="txt_SequenceNo" runat="server" CssClass="clsinputadministration" Width="40px"
										MaxLength="1"></asp:textbox></TD>
							</tr>
							<TR>
								<TD class="clsaspcolumnheader">Cause Number:</TD>
								<TD>
									<asp:textbox id="txt_CauseNumber" runat="server" Width="134px" CssClass="clsinputadministration"></asp:textbox></TD>
							</TR>
							<tr>
								<td class="clsaspcolumnheader">Court Location:&nbsp;
								</td>
								<TD>
									<asp:Label id="lbl_courtname" runat="server" CssClass="label" ForeColor="#3366CC"></asp:Label>
								</TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" height="22">Verified Court Status:
								</td>
								<TD height="22"><asp:dropdownlist id="ddl_Status" runat="server" CssClass="clsinputcombo"></asp:dropdownlist></TD>
							</tr>
							<tr>
								<td class="clsaspcolumnheader" style="HEIGHT: 4px" height="4">Verified Court 
									Date:&nbsp;
								</td>
								<TD style="HEIGHT: 4px" height="4"><asp:dropdownlist id="ddl_Date_Month" runat="server" CssClass="clsinputcombo" Width="53px">
										<asp:ListItem Value="--">--</asp:ListItem>
										<asp:ListItem Value="1">Jan</asp:ListItem>
										<asp:ListItem Value="2">Feb</asp:ListItem>
										<asp:ListItem Value="3">Mar</asp:ListItem>
										<asp:ListItem Value="4">Apr</asp:ListItem>
										<asp:ListItem Value="5">May</asp:ListItem>
										<asp:ListItem Value="6">Jun</asp:ListItem>
										<asp:ListItem Value="7">Jul</asp:ListItem>
										<asp:ListItem Value="8">Aug</asp:ListItem>
										<asp:ListItem Value="9">Sep</asp:ListItem>
										<asp:ListItem Value="10">Oct</asp:ListItem>
										<asp:ListItem Value="11">Nov</asp:ListItem>
										<asp:ListItem Value="12">Dec</asp:ListItem>
									</asp:dropdownlist><asp:dropdownlist id="ddl_Date_Day" runat="server" CssClass="clsinputcombo" Width="39px">
										<asp:ListItem Value="--">--</asp:ListItem>
										<asp:ListItem Value="1">01</asp:ListItem>
										<asp:ListItem Value="2">02</asp:ListItem>
										<asp:ListItem Value="3">03</asp:ListItem>
										<asp:ListItem Value="4">04</asp:ListItem>
										<asp:ListItem Value="5">05</asp:ListItem>
										<asp:ListItem Value="6">06</asp:ListItem>
										<asp:ListItem Value="7">07</asp:ListItem>
										<asp:ListItem Value="8">08</asp:ListItem>
										<asp:ListItem Value="9">09</asp:ListItem>
										<asp:ListItem Value="10">10</asp:ListItem>
										<asp:ListItem Value="11">11</asp:ListItem>
										<asp:ListItem Value="12">12</asp:ListItem>
										<asp:ListItem Value="13">13</asp:ListItem>
										<asp:ListItem Value="14">14</asp:ListItem>
										<asp:ListItem Value="15">15</asp:ListItem>
										<asp:ListItem Value="16">16</asp:ListItem>
										<asp:ListItem Value="17">17</asp:ListItem>
										<asp:ListItem Value="18">18</asp:ListItem>
										<asp:ListItem Value="19">19</asp:ListItem>
										<asp:ListItem Value="20">20</asp:ListItem>
										<asp:ListItem Value="21">21</asp:ListItem>
										<asp:ListItem Value="22">22</asp:ListItem>
										<asp:ListItem Value="23">23</asp:ListItem>
										<asp:ListItem Value="24">24</asp:ListItem>
										<asp:ListItem Value="25">25</asp:ListItem>
										<asp:ListItem Value="26">26</asp:ListItem>
										<asp:ListItem Value="27">27</asp:ListItem>
										<asp:ListItem Value="28">28</asp:ListItem>
										<asp:ListItem Value="29">29</asp:ListItem>
										<asp:ListItem Value="30">30</asp:ListItem>
										<asp:ListItem Value="31">31</asp:ListItem>
									</asp:dropdownlist><asp:dropdownlist id="ddl_Date_Year" runat="server" CssClass="clsinputcombo" Width="56px">
										<asp:ListItem Value="--">--</asp:ListItem>
										<asp:ListItem Value="2000">2000</asp:ListItem>
										<asp:ListItem Value="2001">2001</asp:ListItem>
										<asp:ListItem Value="2002">2002</asp:ListItem>
										<asp:ListItem Value="2003">2003</asp:ListItem>
										<asp:ListItem Value="2004">2004</asp:ListItem>
										<asp:ListItem Value="2005">2005</asp:ListItem>
										<asp:ListItem Value="2006">2006</asp:ListItem>
										<asp:ListItem Value="2007">2007</asp:ListItem>
										<asp:ListItem Value="2008">2008</asp:ListItem>
										<asp:ListItem Value="2009">2009</asp:ListItem>
										<asp:ListItem Value="2010">2010</asp:ListItem>
									</asp:dropdownlist><asp:dropdownlist id="ddl_Time" runat="server" CssClass="clsinputcombo" Width="80px">
										<asp:ListItem Value="--">--</asp:ListItem>
										<asp:ListItem Value="8:00 AM">8:00 AM</asp:ListItem>
										<asp:ListItem Value="8:15 AM">8:15 AM</asp:ListItem>
										<asp:ListItem Value="8:30 AM">8:30 AM</asp:ListItem>
										<asp:ListItem Value="8:45 AM">8:45 AM</asp:ListItem>
										<asp:ListItem Value="9:00 AM">9:00 AM</asp:ListItem>
										<asp:ListItem Value="9:15 AM">9:15 AM</asp:ListItem>
										<asp:ListItem Value="9:30 AM">9:30 AM</asp:ListItem>
										<asp:ListItem Value="9:45 AM">9:45 AM</asp:ListItem>
										<asp:ListItem Value="10:00 AM">10:00 AM</asp:ListItem>
										<asp:ListItem Value="10:15 AM">10:15 AM</asp:ListItem>
										<asp:ListItem Value="10:30 AM">10:30 AM</asp:ListItem>
										<asp:ListItem Value="10:45 AM">10:45 AM</asp:ListItem>
										<asp:ListItem Value="11:00 AM">11:00 AM</asp:ListItem>
										<asp:ListItem Value="11:15 AM">11:15 AM</asp:ListItem>
										<asp:ListItem Value="11:30 AM">11:30 AM</asp:ListItem>
										<asp:ListItem Value="11:45 AM">11:45 AM</asp:ListItem>
										<asp:ListItem Value="12:00 PM">12:00 PM</asp:ListItem>
										<asp:ListItem Value="12:15 PM">12:15 PM</asp:ListItem>
										<asp:ListItem Value="12:30 PM">12:30 PM</asp:ListItem>
										<asp:ListItem Value="12:45 PM">12:45 PM</asp:ListItem>
										<asp:ListItem Value="1:00 PM">1:00 PM</asp:ListItem>
										<asp:ListItem Value="1:15 PM">1:15 PM</asp:ListItem>
										<asp:ListItem Value="1:30 PM">1:30 PM</asp:ListItem>
										<asp:ListItem Value="1:45 PM">1:45 PM</asp:ListItem>
										<asp:ListItem Value="2:00 PM">2:00 PM</asp:ListItem>
										<asp:ListItem Value="2:15 PM">2:15 PM</asp:ListItem>
										<asp:ListItem Value="2:30 PM">2:30 PM</asp:ListItem>
										<asp:ListItem Value="2:45 PM">2:45 PM</asp:ListItem>
										<asp:ListItem Value="3:00 PM">3:00 PM</asp:ListItem>
										<asp:ListItem Value="3:15 PM">3:15 PM</asp:ListItem>
										<asp:ListItem Value="3:30 PM">3:30 PM</asp:ListItem>
										<asp:ListItem Value="3:45 PM">3:45 PM</asp:ListItem>
										<asp:ListItem Value="4:00 PM">4:00 PM</asp:ListItem>
										<asp:ListItem Value="4:15 PM">4:15 PM</asp:ListItem>
										<asp:ListItem Value="4:30 PM">4:30 PM</asp:ListItem>
										<asp:ListItem Value="4:45 PM">4:45 PM</asp:ListItem>
										<asp:ListItem Value="5:00 PM">5:00 PM</asp:ListItem>
										<asp:ListItem Value="5:15 PM">5:15 PM</asp:ListItem>
										<asp:ListItem Value="5:30 PM">5:30 PM</asp:ListItem>
										<asp:ListItem Value="5:45 PM">5:45 PM</asp:ListItem>
										<asp:ListItem Value="6:00 PM">6:00 PM</asp:ListItem>
										<asp:ListItem Value="6:15 PM">6:15 PM</asp:ListItem>
										<asp:ListItem Value="6:30 PM">6:30 PM</asp:ListItem>
										<asp:ListItem Value="6:45 PM">6:45 PM</asp:ListItem>
										<asp:ListItem Value="7:00 PM">7:00 PM</asp:ListItem>
										<asp:ListItem Value="7:15 PM">7:15 PM</asp:ListItem>
										<asp:ListItem Value="7:30 PM">7:30 PM</asp:ListItem>
										<asp:ListItem Value="7:45 PM">7:45 PM</asp:ListItem>
										<asp:ListItem Value="8:00 PM">8:00 PM</asp:ListItem>
										<asp:ListItem Value="8:15 PM">8:15 PM</asp:ListItem>
										<asp:ListItem Value="8:30 PM">8:30 PM</asp:ListItem>
										<asp:ListItem Value="8:45 PM">8:45 PM</asp:ListItem>
										<asp:ListItem Value="9:00 PM">9:00 PM</asp:ListItem>
									</asp:dropdownlist></TD>
							</tr>
							<TR>
								<TD class="clsaspcolumnheader">Verified Court Number:</TD>
								<TD><asp:textbox id="txt_courtno" runat="server" CssClass="clsinputadministration" Width="40px" MaxLength="1"></asp:textbox></TD>
							</TR>
							<tr>
								<TD class="clsaspcolumnheader" height="22">
									<asp:Label id="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></TD>
								<TD vAlign="top" align="right" height="22"><asp:button id="btn_submit" runat="server" CssClass="clsbutton" Text="Update" Width="60px"></asp:button>&nbsp;
									<asp:button id="btnClose" runat="server" Width="60px" CssClass="clsbutton" Text="Close"></asp:button>&nbsp;</TD>
							</tr>
							<TR>
								<TD class="clsaspcolumnheader" height="22"></TD>
								<TD style="VISIBILITY: hidden" vAlign="top" align="right" height="22">
									<asp:Label id="lbl_courtid" runat="server">Label</asp:Label></TD>
							</TR>
						</TABLE>
					</td>
				<tr>
					<td background="../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentDiscrepancyReport.aspx.cs" Inherits="lntechNew.Reports.PaymentDiscrepancyReport" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payment Discrepancy Report</title>
      <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" width="780" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                <uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </td>
                        </tr>
                       <TR background="../images/separator_repeat.gif">
								<TD width="100%" background="../images/separator_repeat.gif" colSpan="5" height="11"></TD>
							</TR>
							
							<tr>
							    <td>
							        <TABLE id="tblSearchCriteria" cellSpacing="1" cellPadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="clsLeftPaddingTable" width="20%">
                                               <font color="#3366cc"> <STRONG>Transaction Date Search :</STRONG></font></td>
                                            <td class="clsLeftPaddingTable"  width="15%">
                                            </td>
                                            <td class="clsLeftPaddingTable"  width="18%">
                                            </td>
                                            <td class="clsleftpaddingtable"  width="15%">
                                            </td>
                                            <td class="clsLeftPaddingTable"  width="10%">
                                            </td>
                                            <td class="clsleftpaddingtable" >
                                            </td>
                                        </tr>
							<TR>
							    <td colspan="6">
							    <table  border="0" cellpadding="0" cellspacing="0" width="100%">
							    <tr>
								<TD class="clsLeftPaddingTable" width="10%" ><font color="#3366cc"><STRONG> From :</STRONG></font></TD>
								<TD class="clsLeftPaddingTable" width="15%" ><ew:calendarpopup id="cal_todate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
								<TD class="clsLeftPaddingTable" width="5%"><font color="#3366cc"><STRONG> To :</STRONG></font>&nbsp;</TD>
								<TD class="clsleftpaddingtable" width="15%"><ew:calendarpopup id="cal_fromDate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
										ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
									<td class="clsLeftPaddingTable" width="10%">
                                        <asp:CheckBox ID="chkAll" runat="server" Text="ShowAll" CssClass="clslabelnew" Font-Bold="True" />
									</td>
								
								<TD class="clsleftpaddingtable" ><asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click"></asp:button></TD>
								</tr>
								</table>
								</td>
							</TR>
                                       
						</TABLE>
							    </td>
							</tr>
							
							
							
                    
                    </table>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                        <tr>
                            <TD background="../Images/subhead_bg.gif" height ="34px" ><IMG height="34" src="../Images/subhead_bg.gif" style="width: 1px"></TD>
                        </tr>
                        
                        <tr>
                            <td >
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                              <tr>
                                <td align="center">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                    <asp:GridView ID="gv" runat="server" Width="100%" AutoGenerateColumns="False"  BackColor="#EFF4FB" BorderColor="White" CssClass="clsLeftPaddingTable" OnRowDataBound="gv_RowDataBound" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_PageIndexChanging" OnSorting="gv_Sorting" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="S.NO">
                                            <HeaderStyle CssClass="clsaspcolumnheader"   HorizontalAlign="Left" />
                                                    <ItemStyle VerticalAlign="Top"  />       
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HPSNo" runat="server" NavigateUrl="#">[HPSNo]</asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Ticket Number">
                                                <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top"
                                                         HorizontalAlign="Left"        />
                                                 <ItemStyle VerticalAlign="Top"  />                 
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HPTicketNo" runat="server" Text='<%# bind("TicketNo") %>' NavigateUrl="#"></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="First Name">
                                             <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                  <ItemStyle VerticalAlign="Top"  />                
                                                <ItemTemplate>
                                           
                                                    <asp:Label ID="lblfirstname" CssClass="label" runat="server" Text='<%# bind("firstname") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name">
                                             <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                                 <ItemStyle VerticalAlign="Top"  /> 
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllastname" CssClass="label" runat="server" Text='<%# bind("lastname") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Phone Number">
                                            <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                  <ItemStyle VerticalAlign="Top"  />                
                                                <ItemTemplate>
                                                  <table id="tbl_contacts" border="0" cellpadding="0" cellspacing="0" 
                                                                    width="100%">
                                                                    <tr>
                                                                    <td>
                                                    <asp:Label ID="lblContact1" CssClass="label" runat="server" Text='<%# bind("contact1") %>'></asp:Label>
                                                    </td></tr>
                                                    <tr><td>
                                                    <asp:Label ID="lblcontact2"  CssClass="label" runat="server" Text='<%# bind("contact2") %>' ></asp:Label>
                                                    </td></tr>
                                                    <tr><td>
                                                    <asp:Label ID="lblcontact3" CssClass="label" runat="server" Text='<%# bind("contact3") %>'></asp:Label>
                                                    </td></tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Transaction Date">
                                             <HeaderStyle CssClass="clsaspcolumnheader"  VerticalAlign="Top" HorizontalAlign="Left"
                                                                 />
                                                                 <ItemStyle VerticalAlign="Top"  /> 
                                                <ItemTemplate>
                                                    <asp:Label ID="transDate" CssClass="label" runat="server" Text='<%# bind("TransactionDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblticketid" runat="server" Text='<%# bind("ticketid_pk") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        
                                       
                                        
                                        
                                    </asp:GridView>
                                </td>
                              </tr>
                              </table>
                            </td>
                        </tr>
                    </table>
                </td>
                
            </tr>
            <tr>
                                <td background="../images/separator_repeat.gif" height="11">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:Footer ID="Footer1" runat="server" />
                                </td>
                            </tr>
        </table>
    </div>
    </form>
</body>
</html>

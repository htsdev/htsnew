<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ConcentrationReport.aspx.cs"
    Inherits="lntechNew.Reports.ConcentrationReport" %>

<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Concentration Report</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
     function ShowPopup(CourtDate,CourtID,CourtRoom,ViolationID)
        {
       
            var WinSettings = "center:yes;resizable:no;dialogHeight:150px;dialogwidth:275px;scroll:no"
            var Arg = 1;
            var conf = window.showModalDialog("Popup_SelectFirm.aspx?CourtDate="+CourtDate+"&CourtID="+CourtID+"&CourtRoom="+CourtRoom+"&ViolationID="+ViolationID,Arg,WinSettings);
            
            if(conf == 1)
            {
                return(true);
            }
            else
            {
                return(false);                
            }
        
        }
        
        function ShowHideCheckBox()
        {
            var courts = document.getElementById('ddl_Courts');
            if(courts.value == '0' || courts.value == '1')
            {
                document.getElementById('td_checkBox').style.display = 'block';
                document.getElementById('hf_CheckBoxSelected').value = '1';
            }
            else
            {
                document.getElementById('td_checkBox').style.display = 'none';
                document.getElementById('hf_CheckBoxSelected').value = '0';
            }
        }
        
        function EnableCheckBox()
        {
           if(document.getElementById('hf_CheckBoxSelected').value == '0')
                document.getElementById('td_checkBox').style.display = 'none';
           else
                document.getElementById('td_checkBox').style.display = 'block';
        }
        
    </script>

</head>

<body class=" ">

    <form id="form1" runat="server">
    
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Concentration Report</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-xs-12">
                        <section class="box ">
                            <div class="content-body">

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Date</label>
                                            <div class="controls">
                                                <ew:CalendarPopup ID="dtp_From" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                                    EnableHideDropDown="True">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                                </ew:CalendarPopup>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Court</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddl_Courts" runat="server" CssClass="form-control m-bot15" onChange="return ShowHideCheckBox()"></asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Sort by</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control m-bot15">
                                                    <asp:ListItem Selected="True" Text="Time then Court" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Court then Time" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                                
                                                <asp:HiddenField ID="hf_CheckBoxSelected" runat="server" Value="1" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <table>
                                                    <tr>
                                                        <td id="td_checkBox" runat="server">
                                                            <asp:CheckBox ID="chk_HMCDetail" runat="server" CssClass="form-label" Text="Show Detail" Checked="True" OnCheckedChanged="chk_HMCDetail_CheckedChanged" />
                                                            <asp:CheckBox ID="chk_hidestatus" runat="server" CssClass="form-label" Text="Display Without Case Status" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xs-12">

                                        <div class="form-group">
                                            <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label></td>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btn_Submit_Click" />
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            
                            </div>
                        </section>
                    </div>

                    <table id="tblGrids" runat="server" align="center" class="table">
                        <tr>
                            
                        
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_11" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowDataBound="gv_11_RowDataBound" OnRowCommand="gv_11_RowCommand" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                                <asp:Label ID="lbl_Message1" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_12" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message2" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_13" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message3" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_14" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message4" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_15" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message5" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_21" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message6" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_22" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message7" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_23" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle BorderStyle="None" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message8" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_24" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message9" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" valign="top" width="20%">
                                <asp:GridView ID="gv_25" runat="server" CssClass="" AutoGenerateColumns="False"
                                    OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CourtNum" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ClientCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lbl_Message10" runat="server" CssClass="form-label" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                    </table>



                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>

















<%--<body onload="EnableCheckBox()">
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center">
                            <tr>
                                <td align="right" class="clslabel" style="width: 300px">
                                    Date:
                                    <ew:CalendarPopup ID="dtp_From" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                        Width="86px" EnableHideDropDown="True">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                    &nbsp; Court:<asp:DropDownList ID="ddl_Courts" runat="server" CssClass="clsinputcombo"
                                        onChange="return ShowHideCheckBox()">
                                    </asp:DropDownList>
                                </td>
                                <td align="right" class="clslabel">
                                    Sort By:<asp:DropDownList ID="ddlSort" runat="server" CssClass="clsinputcombo" Width="125px">
                                        <asp:ListItem Selected="True" Text="Time then Court" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Court then Time" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hf_CheckBoxSelected" runat="server" Value="1" />
                                </td>
                                <td id="td_checkBox" runat="server" style="display: block; width: 152px;" class="clslabel">
                                    <asp:CheckBox ID="chk_HMCDetail" runat="server" CssClass="clslabel" Text="Show Detail"
                                        Height="9px" Width="165px" Checked="True" OnCheckedChanged="chk_HMCDetail_CheckedChanged" />
                                    <asp:CheckBox ID="chk_hidestatus" runat="server" CssClass="clslabel" Text="Display Without Case Status"
                                        Height="9px" Width="167px" /><br />
                                </td>
                                <td>
                                    <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_Submit_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" background="../Images/separator_repeat.gif" height="11" width="11">
                    </td>
                </tr>
                <tr>
                    <td align="center" style="height: 13px">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <table id="tblGrids" runat="server" align="center">
                            <tr>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_11" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        Width="100%" OnRowDataBound="gv_11_RowDataBound" OnRowCommand="gv_11_RowCommand" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message1" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_12" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message2" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_13" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message3" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_14" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message4" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%">
                                    <asp:GridView ID="gv_15" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message5" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_21" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message6" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_22" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message7" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_23" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound" Width="100%" >
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message8" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_24" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        Width="100%" OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message9" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                                <td align="center" valign="top" width="20%" style="height: 2px">
                                    <asp:GridView ID="gv_25" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
                                        Width="100%" OnRowCommand="gv_11_RowCommand" OnRowDataBound="gv_11_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowSeperator" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle BorderStyle="None" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain","{0:t}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_CourtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CourtID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CourtNum" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.CourtNumbermain") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ClientCount" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ClientCount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortDescription") %>'></asp:Label>
                                                    <asp:HiddenField ID="hf_ViolationID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Attorney" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lbl_Message10" runat="server" CssClass="clslabel" Font-Bold="True"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/separator_repeat.gif" style="height: 6px">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>--%>

</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POLMReport old.aspx.cs" Inherits="HTP.Reports.POLMReport" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

    function ShowPanel(status,comments)    
    {
    
        var lblComment= $get("lblComments");
        lblComment.innerHTML=$get(comments).innerText;
        $find("AEFadeIn").get_OnClickBehavior().play();                        
         $find("MPEClientInfo").show();
         return false;
       
    }
    
    function ShowFollowUpPanel(Name,EmailAddress,Phone,FollowUpDate,comments)    
    {
        var lblName= $get("lblName");
        lblName.innerHTML=Name;
        var lblEmailAddress= $get("lblEmailAddress");
        lblEmailAddress.innerHTML=EmailAddress;
        var lblPhone= $get("lblPhone");
        lblPhone.innerHTML=Phone;
        var lblFollowUpDate= $get("lblFollowUpDate");
        lblFollowUpDate.innerHTML=FollowUpDate;
        var lblComment= $get("lblComments");  
        lblComment.innerHTML=$get(comments).innerText;
        $find("AnExtFollowUp").get_OnClickBehavior().play();                        
         $find("MPEFollowUp").show();
         return false;
       
    }
    
    function hide()
    {
    
    $find("ae").get_OnClickBehavior().play();
    
    $find("MPEClientInfo").hide();         
     
            return false;
    }


    </script>

    <style type="text/css">
        .style1
        {
            height: 21px;
            width: 107px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" width="800px">
            <tr>
                <td colspan="4" style="height: 14px">
                    <uc1:ActiveMenu ID="activemenu" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" class="clsleftpaddingtable" style="height: 34px">
                    <table id="tbldate" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 70px; height: 21px">
                                <strong><span class="clssubhead">Start Date&nbsp; </span></strong>
                            </td>
                            <td align="left" style="width: 120px; height: 21px">
                                <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Report Date" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 55px; height: 21px">
                                <strong><span class="clssubhead">End Date&nbsp; </span></strong>
                            </td>
                            <td align="left" style="width: 120px; height: 21px">
                                <ew:CalendarPopup ID="todate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    UpperBoundDate="12/31/9999 23:59:00" Width="90px" LowerBoundDate="1900-01-01">
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 95px; height: 21px">
                                <strong><span class="clssubhead">Ref Attor&nbsp; </span></strong>
                            </td>
                            <td align="left" style="width: 80px; height: 21px">
                                <asp:DropDownList ID="ddlRefAttorneys" runat="server" CssClass="clsInputCombo">
                                    <asp:ListItem Value="2" Text="All" />
                                    <asp:ListItem Value="0" Selected="True" Text="Pending" />
                                    <asp:ListItem Value="1" Text="Confirmed" />
                                </asp:DropDownList>
                            </td>
                            <td style="width: 70px; height: 21px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkShowall" runat="server" Text="Show All" Checked="true" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                        <td align="left" style="width: 70px; height: 21px">
                                <strong><span class="clssubhead">Case Type&nbsp; </span></strong>
                            </td>
                            <td align="left" style="width: 80px; height: 21px">
                                <asp:DropDownList ID="ddlCaseType" runat="server" CssClass="clsInputCombo">
                                    <asp:ListItem Value="2" Text="All" />
                                    <asp:ListItem Value="0" Selected="True" Text="Pending" />
                                    <asp:ListItem Value="1" Text="Confirmed" />
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 55px; height: 21px">
                                <strong><span class="clssubhead">Status&nbsp; </span></strong>
                            </td>
                            <td align="left" style="width: 80px; height: 21px">
                                <asp:DropDownList ID="ddlStatuses" runat="server" CssClass="clsInputCombo">
                                    <asp:ListItem Value="2" Text="All Cases" />
                                    <asp:ListItem Value="0" Selected="True" Text="Closed-Hired" />
                                    <asp:ListItem Value="1" Text="Closed-Not Hired" />
                                </asp:DropDownList>
                            </td>
                            
                            <td align="left" style="width: 95px; height: 21px">
                                <strong><span class="clssubhead">Assig Attor&nbsp; </span></strong>
                            </td>
                            <td align="left" style="width: 80px; height: 21px">
                                <asp:DropDownList ID="ddlAttorneys" runat="server" CssClass="clsInputCombo">
                                    <asp:ListItem Value="2" Text="All" />
                                    <asp:ListItem Value="0" Selected="True" Text="Pending" />
                                    <asp:ListItem Value="1" Text="Confirmed" />
                                </asp:DropDownList>
                            </td>
                            
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" Width="70px"
                                    OnClientClick="return CheckDateValidation();" onclick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                    <table style="width: 800px;">
                        <tr>
                            <td>
                            </td>
                            <td style="text-align: right;">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="7" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" colspan="2" style="width: 100%">
                    <aspnew:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updpnlPOLM">
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                CssClass="clsLabel"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress>
                    <aspnew:UpdatePanel ID="updpnlPOLM" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gvPOLM" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                Width="100%" CellPadding="4" PageSize="30" CssClass="clsLeftPaddingTable" OnRowCommand="gvPOLM_RowCommand"
                                OnRowDataBound="gvPOLM_RowDataBound" OnPageIndexChanging="gvPOLM_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hl_SerialNumber" runat="server" Text='<%# Eval("Sno") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_recdate" runat="server" Text='<%# Eval("recdate") %>' ForeColor="#123160">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkBtnName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Name") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Referring Attorney">
                                        <ItemTemplate>
                                            <label id="lblStatus" runat="server" text='<%# DataBinder.Eval(Container,"DataItem.calldescription") %>'>
                                            </label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>                                
                                    <asp:TemplateField HeaderText="Case Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_phone" runat="server" Text='<%# Eval("Phone") %>'></asp:Label><br />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned Attorney">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_emailadress" runat="server" Text='<%# Eval("EmailAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Case Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Question" runat="server" Text='<%# Eval("Question") %>' ForeColor="#123160"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="35%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Foll up Notes">
                                        <ItemTemplate>
                                            <label id="lblFollowupDate" runat="server" text='<%# DataBinder.Eval(Container,"DataItem.FollowupDate") %>'>
                                            </label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server"></asp:Label>
                                            <img id="aFollowup" runat="server" src="../Images/add.gif" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td width="100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
        <aspnew:UpdatePanel ID="updpnlClintInfo" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="MPEClientInfo" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="lnkbtnCloseFollowUp" PopupControlID="pnlClientInfo" TargetControlID="btndummynone">
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:AnimationExtender ID="AEFadeIn" runat="server" Enabled="True" TargetControlID="hfFadeIn">
                    <Animations>
                        <OnClick>
                            <%-- We need set the AnimationTarget with the control which needs to make animation --%>
                            <Sequence AnimationTarget="pnlClientInfo">
                                <%--The FadeIn and Display animation.--%>                     
                                <FadeIn Duration=".3" MinimumOpacity="0" MaximumOpacity="1" />
                                
                                
                            </Sequence>
                        </OnClick>
                    </Animations>
                </ajaxToolkit:AnimationExtender>
                <ajaxToolkit:AnimationExtender ID="ae" runat="server" TargetControlID="hfFadeIn">
                    <Animations>
                          <OnClick>
                            <Sequence AnimationTarget="pnlClientInfo">
                              <FadeOut Duration="0.5" Fps="24" />                              
                          </Sequence>
                          </OnClick>
                    </Animations>
                </ajaxToolkit:AnimationExtender>
                <asp:HiddenField runat="server" ID="hfFadeIn" />
                <asp:Button ID="btndummynone" runat="server" />
                <asp:Panel ID="pnlClientInfo" runat="server" Height="310" Width="395px">
                    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                        width: 342px">
                        <tr>
                            <td background="../Images/subhead_bg.gif" valign="bottom" colspan="2">
                                <table border="0" width="100%">
                                    <tr>
                                        <td class="clssubhead" style="height: 26px">
                                            <asp:Label ID="lbl_head" runat="server"></asp:Label>
                                        </td>
                                        <td align="right">
                                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" width="100%">
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Status
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlStatus" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Referring Attorney
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlRefAttor" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Attorney
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAttor" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Category
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCat" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Comments :
                                        </td>
                                        <td style="height: 30px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr id="tr_comment" runat="server">
                                        <td valign="top" colspan="2">
                                            <table enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                                                width: 323px;">
                                                <tr>
                                                    <td style="height: 15px; width: 314px;">
                                                        <div id="divcomment" runat="server" style="overflow: auto; height: 50px; width: 375px;">
                                                            <asp:Label ID="lblComments" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 314px">
                                                        <asp:TextBox ID="tb_GeneralComments" runat="server" Height="70px" TextMode="MultiLine"
                                                            Width="375px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="height: 20px">
                                            <asp:Button ID="btnsave" runat="server" CssClass="clsbutton" Text="Save" Width="53px" />&nbsp;
                                            <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" OnClientClick="return hide();"
                                                Text="Cancel" Width="53px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="MPEFollowUp" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="lnkbtnCloseFollowUp" PopupControlID="pnlFollowUp" TargetControlID="btnFollUp">
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:AnimationExtender ID="AnExtFollowUp" runat="server" Enabled="True" TargetControlID="hfFollUp">
                    <Animations>
                <OnClick>
                    <%-- We need set the AnimationTarget with the control which needs to make animation --%>
                    <Sequence AnimationTarget="pnlFollowUp">
                        <%--The FadeIn and Display animation.--%>                     
                        <FadeIn Duration=".3" MinimumOpacity="0" MaximumOpacity="1" />
                        
                        
                    </Sequence>
                </OnClick>
                    </Animations>
                </ajaxToolkit:AnimationExtender>
                <ajaxToolkit:AnimationExtender ID="AnimationExtender2" runat="server" Enabled="True"
                    TargetControlID="hfFollUp">
                    <Animations>
                   <OnClick>
                        <Parallel Duration=".2" Fps="20">
                         <FadeOut Duration=".5" Fps="20" AnimationTarget="pnlFollowUp" />
                        </Parallel>
                     </OnClick>
                    </Animations>
                </ajaxToolkit:AnimationExtender>
                <asp:HiddenField runat="server" ID="hfFollUp" />
                <asp:Button ID="btnFollUp" runat="server" />
                <asp:Panel ID="pnlFollowUp" runat="server" Height="310" Width="395px">
                    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                        width: 342px">
                        <tr>
                            <td background="../Images/subhead_bg.gif" valign="bottom" colspan="2">
                                <table border="0" width="100%">
                                    <tr>
                                        <td class="clssubhead" style="height: 26px">
                                            <asp:Label ID="Label2" runat="server"></asp:Label>
                                        </td>
                                        <td align="right">
                                            &nbsp;<asp:LinkButton ID="lnkbtnCloseFollowUp" runat="server">X</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" width="100%">
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Name:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblName" runat="server" CssClass="clsLabel"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Email:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblEmailAddress" runat="server" CssClass="clsLabel"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Contact No:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPhone" runat="server" CssClass="clsLabel"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Current Follow up Date
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFollowUpDate" runat="server" CssClass="clsLabel"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Next Follow up Date
                                        </td>
                                        <td>
                                            <ew:CalendarPopup ID="dtpCourtDate" runat="server" Text=" " Width="80px" ImageUrl="../images/calendar.gif"
                                                SelectedDate="2006-03-14" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                                CalendarLocation="Bottom" ShowGoToToday="True" ShowClearDate="false" AllowArbitraryText="False"
                                                Culture="(Default)" Nullable="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                                ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" JavascriptOnChangeFunction="CheckDate">
                                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></WeekdayStyle>
                                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="LightGray"></WeekendStyle>
                                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                    BackColor="White"></HolidayStyle>
                                            </ew:CalendarPopup>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" style="width: 122px">
                                            Comments :
                                        </td>
                                        <td style="height: 30px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr id="tr1" runat="server">
                                        <td valign="top" colspan="2">
                                            <table enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                                                width: 323px;">
                                                <tr>
                                                    <td style="height: 15px; width: 314px;">
                                                        <div id="div1" runat="server" style="overflow: auto; height: 50px; width: 375px;">
                                                            <asp:Label ID="Label3" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 314px">
                                                        <asp:TextBox ID="TextBox1" runat="server" Height="70px" TextMode="MultiLine" Width="375px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="height: 20px">
                                            <asp:Button ID="Button1" runat="server" CssClass="clsbutton" Text="Save" Width="53px" />&nbsp;
                                            <asp:Button ID="Button2" runat="server" CssClass="clsbutton" OnClientClick="hide();"
                                                Text="Cancel" Width="53px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

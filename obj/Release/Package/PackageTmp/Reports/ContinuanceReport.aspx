<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContinuanceReport.aspx.cs" Inherits="lntechNew.Reports.ContinuanceReport" %>
<%--<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>--%>
<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Continuance Report</title>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #cbl_status label{
	        padding-right: 20px;
        padding-left: 5px;
        }
    </style>


    <%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
   <script language=javascript>
    
    
    function PopUp(ticketid)
		{
		    var path="PopupContinuanceReport.aspx?ticketid="+ticketid;
		    window.open(path,'',"fullscreen=no,scrollbars=yes,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes,width=590,height=270");
		    return false;
		}
   </script>
   
</head>

<body class=" ">
    <form id="form1" runat="server">
    
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">
                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Continuance Report</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-xs-12">
                        <section class="box ">
                            <div class="content-body">

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Start Date</label>
                                            <div class="controls">
                                                <%--<picker:datepicker id="datefrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>
                                                <ew:CalendarPopup ID="datefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                                    Nullable="True" PadSingleDigits="True" SelectedDate="2003-01-11" ShowClearDate="True" ShowGoToToday="True"
                                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                                </ew:CalendarPopup>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">End Date</label>
                                            <div class="controls">
                                                <%--<picker:datepicker id="dateto" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>
                                                <ew:CalendarPopup ID="dateto" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                                    Nullable="True" PadSingleDigits="True" SelectedDate="2007-12-11" ShowClearDate="True" ShowGoToToday="True"
                                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                                </ew:CalendarPopup>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddl_courts" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:CheckBox ID="chk_viewall" runat="server" Text="View all future dates" CssClass="form-label" Checked="True" />
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xs-12">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Status</label>
                                            <div class="controls">
                                                <asp:CheckBoxList ID="cbl_status" runat="server" style="padding-right: 15px;" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="form-label"></asp:CheckBoxList>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xs-12">

                                        <div class="form-group">
                                            <asp:Label ID="lbl_message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xs-12">

                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:Button ID="btn_search" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btn_search_Click" />
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </section>
                    </div>

                    <div class="col-lg-12">
                        <section class="box ">
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <asp:GridView ID="gv_results" runat="server" CssClass="table" AutoGenerateColumns="False" Font-Size="Small" AllowSorting="True" OnSorting="gv_results_Sorting" OnRowDataBound="gv_results_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S.No">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "../clientinfo/violationfeeold.aspx?search=0&casenumber="+DataBinder.Eval(Container,"DataItem.Ticketid_pk" ) %>' Text='<%# DataBinder.Eval(Container,"DataItem.SNO" ) %>'></asp:HyperLink>&nbsp;
                                                        <asp:Label ID="lbl_ticketid" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.Ticketid_pk" ) %>'
                                                            Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.FirstName" ) %>'></asp:Label>&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.LastName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                    
                                                <asp:TemplateField HeaderText="Continuance Status" SortExpression="ContinuanceStatus">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_continuancestatus" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.ContinuanceStatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Continuance Date" SortExpression="ContinuanceDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_continuancedate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.ContinuanceDate","{0:d}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Court Name" SortExpression="shortname">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_shortname" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.shortname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Crt." SortExpression="courtnumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_courtnumber" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.courtnumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rep" SortExpression="Rep">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_rep" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container,"DataItem.Rep") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif" Width="19" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->        

        </div>
    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
    
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>
































   
   
<%--<body>
    <form id="form1" runat="server">
               <table cellpadding="0" cellspacing="0" border="0" align="center" >
                <tr>
                    <td  > 
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                      
                    </td>
                </tr>
                <tr>
                    <td style="height: 15px" background="../images/separator_repeat.gif">
                    </td>
                </tr>
                <tr class="clsLeftPaddingTable">
                <td>
                <table style="width: 780px" cellpadding="0" cellspacing="0">
                <tr>
                
                    <td style="height: 27px;" class="clssubhead">
                        &nbsp; Start Date</td>
                    <td style="height: 27px;">
                        &nbsp;<ew:CalendarPopup ID="datefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                            ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                            PadSingleDigits="True" SelectedDate="2003-01-11" ShowClearDate="True" ShowGoToToday="True"
                            ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                        </ew:CalendarPopup>
                    </td>
                    <td style="height: 27px;" class="clssubhead">
                        End Date</td>
                    <td style="height: 27px;">
                        <ew:CalendarPopup ID="dateto" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                            ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                            PadSingleDigits="True" SelectedDate="2007-12-11" ShowClearDate="True" ShowGoToToday="True"
                            ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TextboxLabelStyle CssClass="clstextarea" />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray" />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black" />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black" />
                        </ew:CalendarPopup>
                    </td>
                    <td style="width: 2px; height: 27px">
                        <asp:DropDownList ID="ddl_courts" runat="server" CssClass="clsinputcombo">
                        </asp:DropDownList>
                    </td>                    
                    <td style="height: 26px;" align="right" colspan="2">
                        <asp:CheckBox ID="chk_viewall" runat="server" Text="View all future dates" CssClass="label" Checked="True" />&nbsp;</td>
                    <td align="right" colspan="1" style="height: 26px">
                        <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" Text="Search" OnClick="btn_search_Click" />
                    </td>
                </tr>
                <tr class="clsLeftPaddingTable">
                    <td style="height: 30px;" class="clssubhead">Status
                    </td>
                    <td colspan="7" style="height: 30px;" valign="middle">
                        <asp:CheckBoxList ID="cbl_status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="label">
                        </asp:CheckBoxList>
                    </td>                                     
                </tr>
                       
             </table>
                </td>
                </tr>
                <tr>
                    <td style="height: 9px" background="../images/separator_repeat.gif">
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td valign="top" align="center">
                        <asp:GridView ID="gv_results" runat="server" CssClass="clsleftpaddingtable" AutoGenerateColumns="False" Width="780px" AllowSorting="True" OnSorting="gv_results_Sorting" OnRowDataBound="gv_results_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="S.No">
                                    <HeaderStyle CssClass="clssubhead" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "../clientinfo/violationfeeold.aspx?search=0&casenumber="+DataBinder.Eval(Container,"DataItem.Ticketid_pk" ) %>' Text='<%# DataBinder.Eval(Container,"DataItem.SNO" ) %>'></asp:HyperLink>&nbsp;
                                        <asp:Label ID="lbl_ticketid" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.Ticketid_pk" ) %>'
                                            Visible="False"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.FirstName" ) %>'></asp:Label>&nbsp;
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.LastName") %>'></asp:Label>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                <asp:TemplateField HeaderText="Continuance Status" SortExpression="ContinuanceStatus">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_continuancestatus" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.ContinuanceStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="Continuance Date" SortExpression="ContinuanceDate">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_continuancedate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.ContinuanceDate","{0:d}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Court Name" SortExpression="shortname">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_shortname" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.shortname") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="Crt." SortExpression="courtnumber">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_courtnumber" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.courtnumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    
                                        <asp:TemplateField HeaderText="Rep" SortExpression="Rep">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_rep" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.Rep") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="View">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPreviewDoc" runat="server" CommandName="View" ImageUrl="../images/Preview.gif" Width="19" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
               
                <tr>
                    <td  valign="top" background="../images/separator_repeat.gif"  style="height: 9px" >
                    </td>
                </tr>
                <tr>
                    <td  valign="top">
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
              
            </table>
    </form>
</body>--%>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocketQuickUpdate.aspx.cs" Inherits=" lntechNew.Reports.DocketQuickUpdate" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title> Docket - Quick Update </title>
    <link  href="../Styles.css" type ="text/css" rel="stylesheet" />
    <SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>
    <script type="text/javascript">
    
    function ValidateForm()
    {
    
    var courtid = "<% =Request.QueryString["CourtID"] %>";
    var courtnum = document.getElementById("txt_CourtLoc").value;
    var bflag = "<% = ViewState["BondFlag"] %>";
    var datetype = document.getElementById("ddl_QuickUpdate_Status").value;
    var strdatewithtime = document.getElementById("txt_MM").value + "/" +document.getElementById("txt_DD").value+"/" + document.getElementById("txt_YYYY").value + " " + document.getElementById("ddl_CourtTime").value;
    var varhours = document.getElementById("ddl_CourtTime").value;
    var strdate = document.getElementById("txt_MM").value + "/" +document.getElementById("txt_DD").value+"/" + document.getElementById("txt_YYYY").value;
        
        if(chkdate(strdate)==false)
        {
            alert('Invalid Date');
            return false;
        }
        
        if(document.getElementById("ddl_CourtTime").value=="<>")
        {
            alert('Please select a valid time');
            return false;
        }
        
        
        //Check For Court Date
			
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
			
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					//UpdateViolation.txt_Month.focus();
					return false;			
				}
			}

        if(courtid == "3001")
		{ 
			var courtno = courtnum * 1;
			if ((courtno < 0) || (courtno > 17)||(courtno > 12 && courtno < 15))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
		}			
			
	    if((courtid == "3002")||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}
				
				
				

    if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM" ) && (varhours != "10:30 AM" ) && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							//UpdateViolation.ddl_Time.focus();
							return false;
						}
						
						
						
						var bodflag=bflag;
						
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
							if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
							{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								//UpdateViolation.txt_CourtNo.focus();
								return false;
							}
						}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						//UpdateViolation.ddl_Status.focus();
						return false;
					}
				}

        var bdflag = bflag;
        if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (courtid != "3003") 
					{
						if ((courtnum == "18") ) 
						{
							alert("This court number can only be set for Dairy Ashford Court");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					if (courtid == "3003") 
					{
						if ((courtnum != "18") ) 
						{
							alert("Court number 18 can only be set for Dairy Ashford Court");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa");
							//UpdateViolation.txt_CourtNo.focus();
							return false;
						}
					}
				}

    //Validating Status
    var status = document.getElementById("ddl_QuickUpdate_Status").value;
    
    if(status == "<>")
    {
        alert("Please select a valid status");
        return false;
    }
			
    }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <table id="tbl_Quick_Update" runat="server" cellSpacing="0" cellPadding="0" width="420" align="left" border="1" class="clsLeftPaddingTable" >
                        <tr>
                            <td style="text-align: center">
                                <strong>
                                Quick Update</strong></td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" >
                                <table style="width: 100%; height: 100%" border="0">
                                    <tr>
                                        <td style="width: 110px; height: 23px;">
                                            <asp:TextBox ID="txt_MM" runat="server" Width="20px" CssClass="clsinputadministration" MaxLength="2"></asp:TextBox>/<asp:TextBox
                                                ID="txt_DD" runat="server" Width="20px" CssClass="clsinputadministration" MaxLength="2"></asp:TextBox>/<asp:TextBox ID="txt_YYYY"
                                                    runat="server" Width="40px" CssClass="clsinputadministration" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 69px; height: 23px;">
                                            <asp:DropDownList ID="ddl_CourtTime" runat="server" CssClass="clsinputcombo">
                                                <asp:ListItem>&lt;&gt;</asp:ListItem>
                                                <asp:ListItem>8:00 AM</asp:ListItem>
                                                <asp:ListItem>8:15 AM</asp:ListItem>
                                                <asp:ListItem>8:30 AM</asp:ListItem>
                                                <asp:ListItem>8:45 AM</asp:ListItem>
                                                <asp:ListItem>9:00 AM</asp:ListItem>
                                                <asp:ListItem>9:15 AM</asp:ListItem>
                                                <asp:ListItem>9:30 AM</asp:ListItem>
                                                <asp:ListItem>9:45 AM</asp:ListItem>
                                                <asp:ListItem>10:00 AM</asp:ListItem>
                                                <asp:ListItem>10:15 AM</asp:ListItem>
                                                <asp:ListItem>10:30 AM</asp:ListItem>
                                                <asp:ListItem>10:45 AM</asp:ListItem>
                                                <asp:ListItem>11:00 AM</asp:ListItem>
                                                <asp:ListItem>11:15 AM</asp:ListItem>
                                                <asp:ListItem>11:30 AM</asp:ListItem>
                                                <asp:ListItem>11:45 AM</asp:ListItem>
                                                <asp:ListItem>12:00 PM</asp:ListItem>
                                                <asp:ListItem>12:15 PM</asp:ListItem>
                                                <asp:ListItem>12:30 PM</asp:ListItem>
                                                <asp:ListItem>12:45 PM</asp:ListItem>
                                                <asp:ListItem>1:00 PM</asp:ListItem>
                                                <asp:ListItem>1:15 PM</asp:ListItem>
                                                <asp:ListItem>1:30 PM</asp:ListItem>
                                                <asp:ListItem>1:45 PM</asp:ListItem>
                                                <asp:ListItem>2:00 PM</asp:ListItem>
                                                <asp:ListItem>2:15 PM</asp:ListItem>
                                                <asp:ListItem>2:30 PM</asp:ListItem>
                                                <asp:ListItem>2:45 PM</asp:ListItem>
                                                <asp:ListItem>3:00 PM</asp:ListItem>
                                                <asp:ListItem>3:15 PM</asp:ListItem>
                                                <asp:ListItem>3:30 PM</asp:ListItem>
                                                <asp:ListItem>3:45 PM</asp:ListItem>
                                                <asp:ListItem>4:00 PM</asp:ListItem>
                                                <asp:ListItem>4:15 PM</asp:ListItem>
                                                <asp:ListItem>4:30 PM</asp:ListItem>
                                                <asp:ListItem>4:45 PM</asp:ListItem>
                                                <asp:ListItem>5:00 PM</asp:ListItem>
                                                <asp:ListItem>5:15 PM</asp:ListItem>
                                                <asp:ListItem>5:30 PM</asp:ListItem>
                                                <asp:ListItem>5:45 PM</asp:ListItem>
                                                <asp:ListItem>6:00 PM</asp:ListItem>
                                                <asp:ListItem>6:15 PM</asp:ListItem>
                                                <asp:ListItem>6:30 PM</asp:ListItem>
                                                <asp:ListItem>6:45 PM</asp:ListItem>
                                                <asp:ListItem>7:00 PM</asp:ListItem>
                                                <asp:ListItem>7:15 PM</asp:ListItem>
                                                <asp:ListItem>7:30 PM</asp:ListItem>
                                                <asp:ListItem>7:45 PM</asp:ListItem>
                                                <asp:ListItem>8:00 PM</asp:ListItem>
                                                <asp:ListItem>8:15 PM</asp:ListItem>
                                                <asp:ListItem>8:30 PM</asp:ListItem>
                                                <asp:ListItem>8:45 PM</asp:ListItem>
                                                <asp:ListItem>9:00 PM</asp:ListItem>
                                            </asp:DropDownList></td>
                                        <td style="width: 33px; height: 23px;">
                                            <asp:TextBox ID="txt_CourtLoc" runat="server" Width="40px" CssClass="clsinputadministration" MaxLength="3"></asp:TextBox></td>
                                        <td style="width: 100px; height: 23px;">
                                            <asp:DropDownList ID="ddl_QuickUpdate_Status" runat="server" CssClass="clsinputcombo">
                                                <asp:ListItem>&lt;--Choose--&gt;</asp:ListItem>
                                                <asp:ListItem Value="135">Bond</asp:ListItem>
                                                <asp:ListItem Value="26">Jury</asp:ListItem>
                                                <asp:ListItem Value="103">Judge</asp:ListItem>
                                                <asp:ListItem Value="66">Pretrial</asp:ListItem>
                                                <asp:ListItem Value="3">Arraignment</asp:ListItem>
                                                <asp:ListItem Value="161">Non-Issue</asp:ListItem>
                                                <asp:ListItem Value="27">Scire</asp:ListItem>
                                                <asp:ListItem Value="147">Other</asp:ListItem>
                                                <asp:ListItem Value="104">Waiting</asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable" style="text-align: center">
                                <asp:Button ID="btn_QuickUpdate" runat="server" Text="Update" CssClass="clsbutton" OnClick="btn_QuickUpdate_Click" OnClientClick="return ValidateForm()" /></td>
                        </tr>
          <tr>
              <td class="clsLeftPaddingTable" style="text-align: center">
                  <asp:Label ID="lbl_Error" runat="server" ForeColor="Red"></asp:Label></td>
          </tr>
                    </table>
    
    </div>
    </form>
</body>
</html>

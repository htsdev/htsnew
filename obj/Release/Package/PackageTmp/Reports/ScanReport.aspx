<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.ScanReport" Codebehind="ScanReport.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Scan Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>	       
	       
	       function OpenScanPopup(DocID,DocNum)
	      {	      	    
	          var PDFWin
		      PDFWin = window.open("ScanRepPopup.aspx?docid="+DocID+"&docnnum="+DocNum,"","fullscreen=no,toolbar=no,width=500,height=200,left=180,top=180,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	         function OpenScanPopup1(DocID,DocNum)
	      {	      	    
	          var PDFWin
		      PDFWin = window.open("../paperless/previewMain.aspx?RecType=0&DocExt=000&docid="+DocID+"&docnum="+DocNum,"","fullscreen=no,toolbar=no,width=800,height=800,left=0,top=0,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
	       } 
	       
	       
	       function SelectDeselectAll()
	       {
	       
			var cnt = parseInt(document.getElementById("txtHidden").value);
			var chkAll = document.getElementById("dg_valrep__ctl2_chkDelAll");
			//var chkDel = document.getElementById(ctl);
			var grdName = "dg_valrep";
			var idx=2;
			cnt = cnt + 2;
			for (idx=2; idx < (cnt); idx++)
				{
				var chkDel = "";
				
				if( idx < 10)
				{
				chkDel =  grdName+ "_ctl0" + idx + "_chkDel";
				}
				else
				{
				chkDel =  grdName+ "_ctl" + idx + "_chkDel";
				}
				var ctl = document.getElementById(chkDel)
				if (ctl != null)
					{
					if (chkAll.checked  == false)
						ctl.checked = false;
					else 
						ctl.checked = true;
					}
				}	
			}
	       
	       
	      
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px" colSpan="4"><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Scan Report </font></STRONG>
									</td>
								</tr>-->
								<tr>
									<td background="../images/separator_repeat.gif" colSpan="7" height="11"></td>
								</tr>
								<TR>
									<TD align="center">
										<TABLE id="tblCriteria" cellSpacing="0" cellPadding="0" width="780" bgColor="white" border="0">
											<tr>
												<TD height="22" style="width: 7%">&nbsp;From:
                                                </TD>
                                                <td height="22" style="width: 17%">
													<ew:calendarpopup id="calQueryFrom" runat="server" Nullable="True" Width="71px" ImageUrl="../images/calendar.gif"
														Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
														AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
														ToolTip="Select Report Date Range" SelectedDate="2005-09-01">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>
                                                </td>
                                                <td align="left" height="22" style="width: 4%" valign="middle">
                                                    To:</td>
												<TD align="left" height="22" style="width: 17%">&nbsp;
                                                    <ew:CalendarPopup ID="calQueryTo" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                        ControlDisplay="TextBoxImage" Culture="(Default)"
                                                        Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                                        PadSingleDigits="True" SelectedDate="2005-09-01" ShowGoToToday="True" ToolTip="Select Report Date Range"
                                                        UpperBoundDate="12/31/9999 23:59:00" Width="86px" Height="18px">
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TextboxLabelStyle CssClass="clstextarea" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </TD>
                                                <td align="left" height="22" style="width: 6%">
													</td>
												<TD align="left" height="22" style="width: 10%">&nbsp;<asp:dropdownlist id="ddlType" runat="server" Font-Size="Smaller" CssClass="clinputcombo" Font-Bold="True"
														ForeColor="#3366cc" Width="167px">
														<asp:ListItem Value="All">All</asp:ListItem>
														<asp:ListItem Value="both" Selected="True">Pending &amp; Updated</asp:ListItem>
														<asp:ListItem Value="0">Pending</asp:ListItem>
														<asp:ListItem Value="1">Updated</asp:ListItem>
														<asp:ListItem Value="2">Exists</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD align="right" style="width: 8%">&nbsp;</TD>
												<td align="right" width="32%" height="22"><asp:label id="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" CssClass="cmdlinks"
														ForeColor="#3366cc" Font-Bold="True" Height="8px" Visible="False">Current Page :</asp:label><asp:label id="lblPNo" runat="server" Width="9px" Font-Size="Smaller" CssClass="cmdlinks" ForeColor="#3366cc"
														Font-Bold="True" Height="10px" Visible="False">a</asp:label>&nbsp;<asp:label id="lblGoto" runat="server" Width="16px" Font-Size="Smaller" CssClass="cmdlinks"
														ForeColor="#3366cc" Font-Bold="True" Height="7px" Visible="False">Goto</asp:label>&nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" Font-Size="Smaller" CssClass="clinputcombo" ForeColor="#3366cc"
														Font-Bold="True" AutoPostBack="True" Visible="False"></asp:dropdownlist>
												</td>
											</tr>
                                            <tr>
                                                <td height="22" style="width: 7%">
                                                </td>
                                                <td height="22" style="width: 17%">
                                                </td>
                                                <td align="left" height="22" style="width: 4%">
                                                </td>
                                                <td align="left" height="22" style="width: 17%">
                                                </td>
                                                <td align="left" height="22" style="width: 6%">
                                                </td>
                                                <td align="left" height="22" style="width: 10%">
                                                    <asp:linkbutton id="btnDelRecs" runat="server" ToolTip="Deletes the selected records from the list (Only primary users).."
														Visible="False" Width="171px">Delete selected Record(s)</asp:linkbutton></td>
                                                <td align="right" style="width: 8%">
                                                </td>
                                                <td align="left" height="22" width="32%">
                                                    <asp:button id="btn_submit" runat="server" Text="Submit" CssClass="clsbutton"></asp:button></td>
                                            </tr>
											<tr>
												<td width="780" background="../images/separator_repeat.gif" colSpan="9" height="11"></td>
												<TD width="780" background="../images/separator_repeat.gif" height="11"></TD>
											</tr>
										</TABLE>
										<asp:label id="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:label></TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
											<tr>
												<td style="HEIGHT: 136px" vAlign="top" align="center" colSpan="2" height="136"><asp:datagrid id="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable" BorderStyle="None"
														BorderColor="White" AutoGenerateColumns="False" BackColor="#EFF4FB" PageSize="20" AllowPaging="True" AllowSorting="True">
														<Columns>
															<asp:TemplateColumn>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:HyperLink id="hlk_SNo" runat="server" Visible="False"></asp:HyperLink>
																	<asp:Label id="lbl_Sno" runat="server" CssClass="label"></asp:Label>
																	<asp:Label id=lbl_docid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doc_id") %>' CssClass="label" Visible="False">
																	</asp:Label>
																	<asp:Label id=lbl_docnum runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.docnum") %>' CssClass="label" Visible="False">
																	</asp:Label>
																	<asp:HyperLink id="hlkTicketId" runat="server" Visible="False">HyperLink</asp:HyperLink>
																	<asp:Label id=lblRecId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recid") %>' Visible="False">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="ticketnumber" HeaderText="Ticket No.">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id=lnkb_ticketno runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="courtdate" HeaderText="Court Date">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_verified runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>' CssClass="label">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="courtno" HeaderText="Court">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_upload runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtno") %>' CssClass="Label">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="description" HeaderText="Case Status">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_adescription runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>' CssClass="Label">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="updateflag" HeaderText="Update Status">
																<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_acourtname runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.updateflag") %>' CssClass="Label" Visible="False">
																	</asp:Label>
																	<asp:LinkButton id=lnkb_status runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.updateflag") %>' ForeColor="Red">
																	</asp:LinkButton>
																	<asp:HyperLink id=hlnk_status runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.updateflag") %>' ForeColor="Black" Visible="False">
																	</asp:HyperLink>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Delete">
																<HeaderTemplate>
																	<asp:CheckBox id="chkDelAll" runat="server"></asp:CheckBox>
																</HeaderTemplate>
																<ItemTemplate>
																	<asp:CheckBox id="chkDel" runat="server"></asp:CheckBox>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle VerticalAlign="Middle" NextPageText="   Next &gt;" PrevPageText="  &lt; Previous        "
															HorizontalAlign="Center"></PagerStyle>
													</asp:datagrid></td>
											</tr>
											<tr>
												<td width="780" background="../images/separator_repeat.gif" colSpan="5" height="11"></td>
											</tr>
											<TR>
												<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
											<TR>
												<TD colSpan="5" style="VISIBILITY: hidden"><asp:TextBox id="txtHidden" runat="server"></asp:TextBox></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</tbody>
			</TABLE>
		</form>
	</body>
</HTML>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaitingFollowUpTraffic.aspx.cs"
    Inherits="HTP.Reports.WaitingFollowUpTraffic" %>
<%@ Register Assembly="ReportSettingControl" Namespace="ReportSettingControl" TagPrefix="cc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Abid Ali 5018 12/28/2008 Enabled Disable family fiters controls -->
    <title>Traffic Waiting Follow Up</title>
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        //Zeeshan 10286 08/15/2012 Traffic Waiting Follow up report Show Setting control
        function CloseReportSettingPopup() {
            $find("ModalBehaviour").hide();
            return true;
        }

        // Abid Ali 5018 12/28/2008 Enabled Disable family fiters controls
        function EnabledFamilyFilterControls(cal_FromDateFilterClientId, cal_ToDateFilterClientId, chkFamilyShowAllClientId, chkFamilyShowPastClinetId, actionNumber) {
            var calFromCtrl = document.getElementById(cal_FromDateFilterClientId);
            var calToCtrl = document.getElementById(cal_ToDateFilterClientId);
            var chkShowAll = document.getElementById(chkFamilyShowAllClientId);
            var chkShowPast = document.getElementById(chkFamilyShowPastClinetId);

            // check following actionNumber value and do the appropriate task
            // 1 --> For calendar control or date control
            // 2 --> For show all check box
            // 3 or Else --> For show past records
            if (actionNumber == 1) {
                chkShowAll.checked = false;
                chkShowAll.disabled = true;

                chkShowPast.checked = false;
                chkShowPast.disabled = true;
            }
            else if (actionNumber == 2) {
                chkShowPast.checked = false;
                chkShowPast.disabled = chkShowAll.checked;
                calFromCtrl.disabled = chkShowAll.checked;
                calToCtrl.disabled = chkShowAll.checked;
            }
            else {
                chkShowAll.checked = false;
                chkShowAll.disabled = chkShowPast.checked;
                calFromCtrl.disabled = chkShowPast.checked;
                calToCtrl.disabled = chkShowPast.checked;
            }
        } // end of EnabledFamilyFilterControls

        function CheckDateValidation() {
            // abid ali 5387 1/15/2009 date comparision
            if (IsDatesEqualOrGrater(document.form1.cal_FromDateFilter.value, 'MM/dd/yyyy', '01/01/1900', 'MM/dd/yyyy') == false) {
                alert("Please enter valid date, From Date must be grater then or equal to 1/1/1900");
                // Fahad 4354 1/16/2009 comment out
                //document.form1.cal_FromDateFilter.focus(); 
                return false;
            }

            if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value, 'MM/dd/yyyy', '01/01/1900', 'MM/dd/yyyy') == false) {
                alert("Please enter valid date, To Date must be grater then or equal to 1/1/1900");
                // Fahad 4354 1/16/2009 comment out
                //document.form1.cal_ToDateFilter.focus(); 
                return false;
            }
            if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value, 'MM/dd/yyyy', document.form1.cal_FromDateFilter.value, 'MM/dd/yyyy') == false) {
                alert("Please enter valid date, To Date must be grater then or equal to From Date");
                // Fahad 4354 1/16/2009 comment out
                //document.form1.cal_ToDateFilter.focus(); 
                return false;
            }
            else {
                return true;
            }
        }
    </script>

</head>

<body class=" ">

    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Traffic Waiting Follow Up</h1>
                                <!-- PAGE HEADING TAG - END -->      
                            </div>
                        
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    
                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-xs-12">
                        <section class="box ">
                            <header class="title pull-left">
                                <h2 class="title pull-left">Follow Up Date Range</h2>
                                <!-- START CODE TO IMPLEMENT REPORT SETTING CONTRTOL -->
                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lnkbtn_ShowSetting" runat="server" CssClass="actions panel_actions form-label pull-right">Show Settings</asp:LinkButton>
                                        <asp:Panel ID="pnl_popup" runat="server" Style="z-index: 3000; position: absolute;
                                            padding: 100px; text-align: center; background-color: transparent; display: block;">
                                            <table border="0" width="800px;">
                                                <tr>
                                                    <td valign="top" align="left">
                                                        <cc1:ReportSetting ID="ReportSetting1" runat="server" ReportId="222" ReportTitle="Report Attribute"
                                                            CurrentUserId="3991" ConnectionStringKey="HoustonTraffic"
                                                            LabelCssClass="clsLeftPaddingTable" GridviewCssClass="table"
                                                            CellBackgroundImageUrl="CellBackgroundImage" TextboxCssClass="form-control"
                                                            ModalPopupExtenderId = "mpeReportSetting" DropdownCssClass="form-control m-bot15" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeReportSetting" runat="server" TargetControlID="lnkbtn_ShowSetting"
                                            PopupControlID="pnl_popup" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                            BehaviorID="ModalBehaviour">
                                        </ajaxToolkit:ModalPopupExtender>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                <!-- END CODE TO IMPLEMENT REPORT SETTING CONTRTOL -->
                            </header>
                            <div class="content-body">

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">From</label>
                                            <div class="controls">
                                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                    Nullable="True" PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                                    UpperBoundDate="9999-12-29">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                                </ew:CalendarPopup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">To</label>
                                            <div class="controls">
                                                <ew:CalendarPopup Visible="true" ID="cal_ToDateFilter"
                                                    runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                                    Culture="(Default)" EnableHideDropDown="True" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                    ShowGoToToday="True" Text=" " ToolTip="Date" UpperBoundDate="9999-12-29">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                             </ew:CalendarPopup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:CheckBox ID="chkFamilyShowAll" runat="server" Text="Show All" CssClass="form-label" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:CheckBox ID="chkFamilyShowPast" runat="server" Text="Display Past Records" CssClass="form-label" />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:Button ID="btnFamilySubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                                                    OnClick="btnFamilySubmit_Click" OnClientClick="return CheckDateValidation();" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        </section>
                    </div>

                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Traffic Waiting Follow Up</h2>
                                <div class="actions panel_actions pull-right">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                        <ProgressTemplate>
                                            <img src="../images/plzwait.gif" alt="" />
                                            <asp:Label ID="lbl1" runat="server" Text="Please Wait ......" CssClass="form-label"></asp:Label>
                                        </ProgressTemplate>
                                    </aspnew:UpdateProgress>
                                    <div class="col-xs-12">

                                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                            <ContentTemplate>
                                                <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table" AllowSorting="True" OnRowDataBound="gv_Records_RowDataBound"
                                                    AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="30"
                                                    OnRowCommand="gv_Records_RowCommand" OnSorting="gv_Records_Sorting">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="S#">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                            <ItemTemplate>
                                                                <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                            </ItemTemplate>
                                                            <ControlStyle Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lastname" HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_LastName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="firstname" HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_FirstName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead"
                                                            HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_TiketNo" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Cause No</u>" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead"
                                                            HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Causenumber" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="<u>LOC</u>" SortExpression="shortname" HeaderStyle-CssClass="clssubhead"
                                                            HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_LOC" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Cov</u>" SortExpression="Cov" HeaderStyle-CssClass="clssubhead"
                                                            HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Cov" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Cov") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="verifiedcourtstatus">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_verstatus" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.verifiedcourtstatus") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>CrtDate</u>" SortExpression="sortcourtdate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_crtDate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Past Due</u>" SortExpression="sortedPastDue">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_pastDue" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.pastdue") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="TrafficWaitingFollowUp">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_followup" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TrafficWaitingFollowUp") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                                <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                                <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                                <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                                <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                                <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("CourtID") %>' />
                                                                <asp:HiddenField ID="hf_TrafficFollowUpDate" runat="server" Value='<%#Eval("TrafficFollowUpDate") %>' />
                                                                <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                                <asp:HiddenField ID="hf_shortname" runat="server" Value='<%#Eval("shortname") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </aspnew:UpdatePanel>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                        <ProgressTemplate>
                            <img src="../images/plzwait.gif" alt="" />
                            <asp:Label ID="lbl2" runat="server" Text="Please Wait ......" CssClass="form-label"></asp:Label>
                        </ProgressTemplate>
                    </aspnew:UpdateProgress>
                    <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlFollowup" runat="server">
                                            

                                <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Traffic Waiting Follow Up (Non HMC and HCJP)" />
                            </asp:Panel>
                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                                

                    <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                        <ContentTemplate>
                            <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlFollowup" TargetControlID="btn">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button ID="btn" runat="server" Style="display: none;" CssClass="btn btn-primary" />
                        </ContentTemplate>
                    </aspnew:UpdatePanel>

                    <table>
                        <tr>
                            <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="form-label" ForeColor="Black"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>


















<%--<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <table border="0" cellpadding="1" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td>
                                            <!-- START CODE TO IMPLEMENT REPORT SETTING CONTRTOL -->
                                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="lnkbtn_ShowSetting" runat="server">Show Settings</asp:LinkButton>
                                                    <asp:Panel ID="pnl_popup" runat="server" Style="z-index: 3000; position: absolute;
                                                        padding: 100px; text-align: center; background-color: transparent; display: block;">
                                                        <table border="0" width="800px;">
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <cc1:ReportSetting ID="ReportSetting1" runat="server" ReportId="222" ReportTitle="Report Attribute"
                                                                        CurrentUserId="3991" ConnectionStringKey="HoustonTraffic" HeaderCssClass="clssubhead"
                                                                        LabelCssClass="clsLeftPaddingTable" GridviewCssClass="clsLeftPaddingTable"
                                                                        CellBackgroundImageUrl="CellBackgroundImage" TextboxCssClass="clsInputadministration"
                                                                        ModalPopupExtenderId = "mpeReportSetting" DropdownCssClass="clsInputCombo" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <ajaxToolkit:ModalPopupExtender ID="mpeReportSetting" runat="server" TargetControlID="lnkbtn_ShowSetting"
                                                        PopupControlID="pnl_popup" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        BehaviorID="ModalBehaviour">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                            <!-- END CODE TO IMPLEMENT REPORT SETTING CONTRTOL -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 135px">
                                <span class="clssubhead">Follow Up Date Range :</span>
                            </td>
                            <td align="left" style="width: 130px">
                                <span class="clsLabel">From :</span>
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 120px">
                                <span class="clsLabel">To :&nbsp;</span><ew:CalendarPopup Visible="true" ID="cal_ToDateFilter"
                                    runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" ControlDisplay="TextBoxImage"
                                    Culture="(Default)" EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                    ShowGoToToday="True" Text=" " ToolTip="Date" UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 70px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkFamilyShowAll" runat="server" Text="Show All" />
                                </span>
                            </td>
                            <td align="left" style="width: 120px">
                                <span style="font-size: XX-Small;">
                                    <asp:CheckBox ID="chkFamilyShowPast" runat="server" Text="Display Past Records" />
                                </span>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnFamilySubmit" runat="server" Text="Submit" CssClass="clsbutton"
                                    OnClick="btnFamilySubmit_Click" OnClientClick="return CheckDateValidation();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Traffic Waiting Follow Up
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowSorting="True" OnRowDataBound="gv_Records_RowDataBound"
                                            AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging" PageSize="30"
                                            OnRowCommand="gv_Records_RowCommand" OnSorting="gv_Records_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CaseNo" Visible="false">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.ticketid_pk")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Last Name</u>" SortExpression="lastname" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>First Name</u>" SortExpression="firstname" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Ticket No</u>" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_TiketNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Cause No</u>" SortExpression="refcasenumber" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Causenumber" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.causenumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="<u>LOC</u>" SortExpression="shortname" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_LOC" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Cov</u>" SortExpression="Cov" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Cov" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Cov") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Status</u>" SortExpression="verifiedcourtstatus">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_verstatus" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.verifiedcourtstatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>CrtDate</u>" SortExpression="sortcourtdate">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_crtDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Past Due</u>" SortExpression="sortedPastDue">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_pastDue" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.pastdue") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="TrafficWaitingFollowUp">
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" Width="90px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TrafficWaitingFollowUp") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif" />
                                                        <asp:HiddenField ID="hf_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                        <asp:HiddenField ID="hf_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                        <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                        <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#Eval("causenumber") %>' />
                                                        <asp:HiddenField ID="hf_courtid" runat="server" Value='<%#Eval("CourtID") %>' />
                                                        <asp:HiddenField ID="hf_TrafficFollowUpDate" runat="server" Value='<%#Eval("TrafficFollowUpDate") %>' />
                                                        <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                        
    
    <asp:HiddenField ID="hf_shortname" runat="server" Value='<%#Eval("shortname") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            

                                            <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="Traffic Waiting Follow Up (Non HMC and HCJP)" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                                

                                <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                                    <ContentTemplate>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>
                            </td>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>



    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>--%>

</html>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateViolationSRV.ascx" TagName="UpdateViolationSRV"
    TagPrefix="uc4" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.ValidationReport2"
    CodeBehind="ValidationReport2.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>ValidationReport2</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script>
	      function OpenEditWin(violationid)
	      {
	      
	     //a;  fullscreen=no,toolbar=no,width=640,height=620,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes
	          var PDFWin
		      PDFWin = window.open("../ClientInfo/UpdateViolation.aspx?ticketsViolationID="+violationid,"","");				
		      return false;				      
	       } 
	  
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780"
        align="center" border="0">
        <tr>
            <td style="height: 14px" colspan="4">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td style="height: 9px" background="../images/separator_repeat.gif">
            </td>
        </tr>
        <tr>
            <td>
                <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Validation II (v=a) </font></STRONG>
									</td>
								</tr>-->
                    <%--<tr>
                        <td background="../images/separator_repeat.gif" colspan="7">
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="clsLeftPaddingTable" colspan="2" width="100%">
                            <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tr>
                                    <td align="left" style="width: 70px">
                                        <asp:Label ID="lblStartDate" runat="server" CssClass="clssubhead">Start Date</asp:Label>
                                    </td>
                                    <td style="width: 120px" align="left">
                                        <ew:CalendarPopup ID="calQueryFrom" runat="server" Width="90px" ImageUrl="../images/calendar.gif"
                                            Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                            ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                            PadSingleDigits="True" ToolTip="Select Report Date Range" SelectedDate="2005-09-01"
                                            DisableTextboxEntry="true" ClearDateText="">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                    </td>
                                    <td style="width: 70px" align="right">
                                        <asp:Label ID="lblEndDate" runat="server" CssClass="clssubhead">End Date</asp:Label>
                                    </td>
                                    <td style="width: 120px" align="right">
                                        <ew:CalendarPopup ID="calQueryTo" runat="server" Width="90px" ImageUrl="../images/calendar.gif"
                                            Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Bottom"
                                            ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                            PadSingleDigits="True" ToolTip="Select Report Date Range" SelectedDate="2006-09-01"
                                            DisableTextboxEntry="true">
                                            <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                            <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></WeekdayStyle>
                                            <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                            <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                BackColor="AntiqueWhite"></OffMonthStyle>
                                            <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                            <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                            <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                            <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="LightGray"></WeekendStyle>
                                            <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                            <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                ForeColor="Black" BackColor="White"></ClearDateStyle>
                                            <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                BackColor="White"></HolidayStyle>
                                        </ew:CalendarPopup>
                                    </td>
                                    <td align="center" style="width: 300px">
                                        &nbsp;<asp:CheckBox ID="chk_showall" runat="server" Font-Size="XX-Small" Text="Show All">
                                        </asp:CheckBox>&nbsp;<asp:CheckBox ID="chkb_DISP" runat="server" Font-Size="XX-Small"
                                            Text="No DISP" />
                                        <asp:CheckBox ID="cb_noaworbw" runat="server" Font-Size="XX-Small" Text="No A/W or B/W" />&nbsp;
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btn_submit" runat="server" Text="Submit" CssClass="clsbutton"></asp:Button>
                                    </td>
                                    <%--<td align="right" style="width: 300px">
                                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <br />
                                                    <asp:Label ID="lblCurrPage" runat="server" Width="83px" Font-Size="Smaller" CssClass="cmdlinks"
                                                        ForeColor="#3366cc" Font-Bold="True" Height="8px">Current Page :</asp:Label><asp:Label
                                                            ID="lblPNo" runat="server" Width="9px" Font-Size="Smaller" CssClass="cmdlinks"
                                                            ForeColor="#3366cc" Font-Bold="True" Height="10px">a</asp:Label>&nbsp;<asp:Label
                                                                ID="lblGoto" runat="server" Width="16px" Font-Size="Smaller" CssClass="cmdlinks"
                                                                ForeColor="#3366cc" Font-Bold="True" Height="7px">Goto</asp:Label><asp:DropDownList
                                                                    ID="cmbPageNo" runat="server" Font-Size="Smaller" CssClass="clinputcombo" ForeColor="#3366cc"
                                                                    Font-Bold="True" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                            &nbsp; &nbsp;
                                        </td>--%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" colspan="5" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                        <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                            <ContentTemplate>
                                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                            </ContentTemplate>
                                        </aspnew:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" colspan="4" height="11">
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lbl_message" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" colspan="2" style="height: 144px">
                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="upnl_validationrec">
                    <ProgressTemplate>
                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                            CssClass="clsLabel"></asp:Label>
                    </ProgressTemplate>
                </aspnew:UpdateProgress>
                <table id="tbl1" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td align="center" width="100%">
                            <aspnew:UpdatePanel ID="upnl_validationrec" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                                        AllowPaging="True" AutoGenerateColumns="False" PageSize="30">
                                        <PagerStyle NextPageText="   Next &gt;" HorizontalAlign="Center" PrevPageText="  &lt; Previous        "
                                            VerticalAlign="Middle"></PagerStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="S#" HeaderStyle-CssClass="clssubhead">
                                                <HeaderStyle CssClass="clssubhead" />
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lbl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" +   DataBinder.Eval(Container,"DataItem.ticketid_pk") %>'
                                                        Text='<%# DataBinder.Eval(Container.DataItem, "SNo") %>'></asp:HyperLink>
                                                    <asp:Label ID="lbl_violationid" runat="server" Width="17px" Text='<%# DataBinder.Eval(Container.DataItem, "TicketsViolationID") %>'
                                                        CssClass="clsLeftPaddingTable" Visible="False">
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_ticketid" runat="server" Width="17px" Text='<%# DataBinder.Eval(Container.DataItem, "TicketID_pk") %>'
                                                        CssClass="clsLeftPaddingTable" Visible="False">
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_search" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "activeflag") %>'
                                                        Visible="False" Width="17px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Cause No" HeaderStyle-CssClass="clssubhead">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CauseNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "causenumber") %>'
                                                        CssClass="clsLeftPaddingTable"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Ticket No" HeaderStyle-CssClass="clssubhead">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HLTicketno" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumberseq") %>'
                                                        Target="_self" Visible="False"></asp:HyperLink>
                                                    <asp:Label ID="lbl_TicketNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RefCaseNumberseq") %>'
                                                        CssClass="clsLeftPaddingTable"></asp:Label>
                                                    <asp:HiddenField ID="hf_causeno" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.CauseNumber") %>' />
                                                    <asp:HiddenField ID="hf_bondamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.Bondamount","{0:F0}") %>' />
                                                    <asp:HiddenField ID="hf_fineamount" runat="server" Value='<%#  DataBinder.Eval(Container, "DataItem.FineAmount","{0:F0}") %>' />
                                                    <asp:HiddenField ID="hf_courtloc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.shortname") %>' />
                                                    <asp:HiddenField ID="hf_priceplan" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' />
                                                    <asp:HiddenField ID="hf_bondflag" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' />
                                                    <asp:HiddenField ID="hf_violationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ViolationNumber_PK") %>' />
                                                    <asp:HiddenField ID="hf_ticketviolationid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketsViolationID") %>' />
                                                    <asp:HiddenField ID="hf_courtviolationstatusid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Vrs") %>' />
                                                    <asp:HiddenField ID="hf_categoryid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.vtypeid") %>' />
                                                    <asp:HiddenField ID="hf_courtid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtid") %>' />
                                                    <asp:HiddenField ID="hf_OscareCourt" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.OscarCourtDetail") %>' />
                                                    <asp:HiddenField ID="hf_ticketnumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.refcasenumberseq") %>' />
                                                    <asp:HiddenField ID="hf_courtdate" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtdatemain_1","{0:d}") %>' />
                                                    <asp:HiddenField ID="hf_crm" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>' />
                                                    <asp:HiddenField ID="hf_ctime" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.CourtDateMain_1","{0:t}") %>' />
                                                    <asp:HiddenField ID="hf_VDesc" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>' />
                                                    <asp:HiddenField ID="hf_courtstatus" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.VDesc") %>' />
                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ticketid_pk") %>' />
                                                    <asp:HiddenField ID="hf_iscriminalcourt" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.iscriminalcourt") %>' />
                                                    <asp:HiddenField ID="hf_chargelevel" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.chargelevel") %>' />
                                                    <asp:HiddenField ID="hf_cdi" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.cdi") %>' />
                                                    <asp:HiddenField ID="hf_isFTAViolation" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.isFTAViolation") %>' />
                                                    <asp:HiddenField ID="hf_hasFTAViolations" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasFTAViolations") %>' />
                                                    <asp:HiddenField ID="hf_hasNOS" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.hasNOS") %>' />
                                                    <asp:HiddenField ID="hf_coveringFirmId" runat="server" Value='<%# Bind("coveringFirmId") %>' />
                                                    <asp:HiddenField ID="hf_arrestdate" runat="server" Value='<%# Bind("arrestdate","{0:d}") %>' />
                                                    <asp:HiddenField ID="hf_casetype" runat="server" Value='<%# Bind("casetypeid") %>' />
                                                    <asp:HiddenField ID="hf_missedcourttype" runat="server" Value='<%# Bind("missedcourttype") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Verified Court Info" HeaderStyle-CssClass="clssubhead">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_verified" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                    <asp:Label ID="Label11" runat="server" Text='#' CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_verifiedcrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumbermain") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_vdescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VDesc") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateColumn>
                                            <%--<asp:TemplateColumn>
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_vdescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VDesc") %>'
                                                        CssClass="Label">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>--%>
                                            <asp:TemplateColumn Visible="False">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_vcourtname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Auto Court Info" HeaderStyle-CssClass="clssubhead">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_upload" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Courtdate") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                    <asp:Label ID="Label1" runat="server" Text='#' CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_uploadcrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtnumber") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                    <asp:Label ID="lbl_adescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ADesc") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateColumn>
                                            <%--<asp:TemplateColumn>
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_adescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ADesc") %>'
                                                        CssClass="Label">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>--%>
                                            <asp:TemplateColumn Visible="False">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_acourtname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                        CssClass="clsLeftPaddingTable">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Select" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="clssubhead">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkb_manverify" runat="server" CommandName="Clicked">Manual Verify</asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <table style="display: none" id="tbl_plzwait1" runat="server">
                                        <tbody>
                                            <tr>
                                                <td style="width: 785px" class="clssubhead" valign="middle" align="center">
                                                    <img src="../Images/plzwait.gif" />
                                                    Please wait While we update your Violation information.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <aspnew:AsyncPostBackTrigger ControlID="UpdateViolationSRV1$btn_popup" EventName="click" />
                                </Triggers>
                            </aspnew:UpdatePanel>
                            <%-- <aspnew:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upnl_validationrec">
                                <ProgressTemplate>
                                    <table width="800">
                                        <tbody>
                                            <tr>
                                                <td class="clssubhead" valign="middle" align="center" style="width: 785px">
                                                    <img src="../Images/plzwait.gif" />
                                                    Please wait While we update your Violation information.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>--%>
                        </td>
                    </tr>
                    <tr>
                        <td width="780" background="../images/separator_repeat.gif" colspan="5" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <uc4:UpdateViolationSRV runat="server" ID="UpdateViolationSRV1" GridID="dg_valrep"
        GridField="hf_ticketviolationid" DivID="tbl_plzwait1" IsCaseDisposition="false" />
    </form>
</body>
</html>

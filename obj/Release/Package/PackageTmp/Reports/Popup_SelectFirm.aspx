<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Popup_SelectFirm.aspx.cs" Inherits="lntechNew.Reports.Popup_SelectFirm" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Select Firm</title>
    <base target="_self" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    
    function CloseOnUpdate()
    {
        var flag = document.getElementById('UpdateFlag').value;
        
        if(flag == '1')
        {
            window.returnValue = 1;
            window.close();
        }
    }
    
    function CloseWindow()
    {
        window.returnValue=0;
        window.close();
        return(false);
    }
    
    </script>
</head>
<body onload="CloseOnUpdate()">
    <form id="form1" runat="server">
    <div>
        <table width="300" align="left" height="100">
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" style="height: 34px">
                    Select Firm</td>
            </tr>
            <tr>
                <td align="right" class="clssubhead">
                    <table width="100%">
                        <tr>
                            <td align="center" style="width: 22%">
                                <asp:Label ID="Label1" runat="server" CssClass="clslabel" Text="Firm:"></asp:Label></td>
                            <td align="center" width="50%">
                                <asp:DropDownList ID="ddl_Firm" runat="server" CssClass="clsinputcombo">
                                </asp:DropDownList>
                                <asp:HiddenField ID="UpdateFlag" runat="server" Value="0" />
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="clssubhead" style="height: 18px">
                    <table style="width: 50%; height: 50%">
                        <tr>
                            <td style="width: 100px">
                                <asp:Button ID="btn_Save" runat="server" CssClass="clsbutton" Text="Save" Width="50px" OnClick="btn_Save_Click" /></td>
                            <td style="width: 100px">
                                <asp:Button ID="btn_Cancel" runat="server" CssClass="clsbutton" Text="Cancel" Width="50px" OnClientClick="return CloseWindow()" /></td>
                        </tr>
                    </table>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" background="../Images/separator_repeat.gif" class="clssubhead"
                    height="11">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

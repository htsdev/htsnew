﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AGReport.aspx.cs" Inherits="HTP.Reports.AGReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AG Validation</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                </td>
                            </tr>
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                </td>
                                <td style="text-align: right;">
                                    <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                        <ContentTemplate>
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                            PageSize="30" CssClass="clsLeftPaddingTable" AllowSorting="true">
                            <Columns>
                                <asp:TemplateField HeaderText="S#">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="refcasenumber" HeaderText="Ticket Number">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="lastname" HeaderText="Last Name">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="firstname" HeaderText="First Name">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ShortName" HeaderText="CRT">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="verifiedcourtstatus" HeaderText="Verified Status">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="autostatus" HeaderText="Auto Status">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="verifiedcourtdate" HeaderText="Verified Date">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="autocourtdate" HeaderText="Auto Date">
                                    <ItemStyle CssClass="GridItemStyle" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" />
                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

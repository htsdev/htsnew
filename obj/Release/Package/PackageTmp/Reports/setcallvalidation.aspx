<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="setcallvalidation.aspx.cs"
    Inherits="HTP.Reports.setcallvalidation" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Set Call Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function showhide(ele,caller) 
        {
            var srcElement = document.getElementById(ele);
            if(srcElement != null) {
            if(srcElement.style.display == "block") {
               caller.innerHTML = "Show Settings";
               srcElement.style.display= 'none';
            }
            else {
               caller.innerHTML = "Hide Settings";
               srcElement.style.display='block';
            }
            return false;
          }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <%--Sabir 4272 07/16/2008 Implement Ajax--%>
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
    <div>
        <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="z-index: 101"
            width="780">
            <tbody>
                <tr>
                    <td colspan="4" style="height: 14px">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                            <ContentTemplate>
                                <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td background="../images/separator_repeat.gif" colspan="7" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="1" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td style="width: 600px;" align="left">
                                                        <uc5:ShowSetting ID="ShowSetting" lbl_TextBefore="Number of Business Days after case reset:"
                                                            runat="server" Attribute_Key="Days" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="right">
                                                                    <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                                                </td>
                                                                <%--<td align="right" class="clssubhead">
                                                    <asp:Label ID="Label2" runat="server" CssClass="clsLabel" Text="Current Page : "></asp:Label>
                                                    <asp:Label ID="LB_curr" runat="server" CssClass="clsLabel" Width="41px"></asp:Label>
                                                    <asp:Label ID="totlb" runat="server" CssClass="clsLabel" Text="Go To :     "></asp:Label>
                                                    <asp:DropDownList ID="DL_pages" runat="server" OnSelectedIndexChanged="DL_pages_SelectedIndexChanged"
                                                        Width="61px" CssClass="clsInputCombo" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 13px; width: 781px;">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                            <%--Sabir 4272 07/16/2008 Implement Ajax--%>
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                        Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="11" style="width: 781px">
                                            <%--Sabir 4272 07/16/2008 Imlement Ajax--%>
                                            <%--Sabir 4272 07/16/2008 allowsorting and Onsorting properties of gv_records has been set for sorting and also the header property(sortexpression) of each column has been set.--%>
                                            <%--Yasir Kamal 5555 03/03/2009 set default page size 20 --%>
                                            <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                                AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" CellPadding="3"
                                                PageSize="20" CssClass="clsLeftPaddingTable" AllowSorting="True" PagerSettings-Visible="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#" HeaderStyle-CssClass="clssubhead">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid")+"&search=0" %>'
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="Name" HeaderText="Name" HtmlEncode="false" ItemStyle-Width="120">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle" Width="120px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="Language" HeaderText="Language" HtmlEncode="false">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="RecDate" HeaderText="Reset Date" DataFormatString="{0:d}" HtmlEncode="false">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="Bond" HeaderText="Bond" HtmlEncode="false" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="GridItemStyle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="Comments" HeaderText="Comments" HtmlEncode="false">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="CRT" HeaderText="CRT" HtmlEncode="false" ItemStyle-Width="50">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle" Width="50px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="TrialDesc" HeaderText="Setting Information" HtmlEncode="false" ItemStyle-Width="140">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle" Width="140px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderStyle-CssClass="clssubhead" ItemStyle-CssClass="GridItemStyle"
                                                        DataField="SetCallStatus" HeaderText="CallBack Status" HtmlEncode="false" ItemStyle-Width="50">
                                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                        <ItemStyle CssClass="GridItemStyle" Width="50px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Contact Info">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcontact1" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact1")%>'></asp:Label><br />
                                                            <asp:Label ID="lblcontact2" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact2")%>'></asp:Label><br />
                                                            <asp:Label ID="lblcontact3" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.contact3")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &gt;" PreviousPageText="&lt; Previous"
                                                    FirstPageText="&lt;&lt; First     " LastPageText="      Last &gt;&gt;" />
                                                <%--<PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />--%>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" background="../Images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc1:Footer ID="Footer1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReminderCallValidation.aspx.cs"
    Inherits="HTP.Reports.ReminderCallValidation" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc4" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reminder Call Alert</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    
    
    
<%-- Yasir kamal 6050 06/17/2009 showSetting Control added.

     <script language="javascript">
        function showhide(ele,caller) 
        {
            var srcElement = document.getElementById(ele);
            if(srcElement != null) {
            if(srcElement.style.display == "block") {
               caller.innerHTML = "Show Settings";
               srcElement.style.display= 'none';
            }
            else {
               caller.innerHTML = "Hide Settings";
               srcElement.style.display='block';
            }
            return false;
          }
        }
        
    </script>--%>
    
    
</head>
<body>
    <form id="form1" runat="server">
    <%--Sabir 4272 07/16/2008 Implement Ajax--%>
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
    <div>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="MainTable" cellspacing="0" cellpadding="0" align="center" border="0" style="width: 900px">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr >
                        <td style="width: 600px;" align="left">
                            <uc4:ShowSetting ID="ShowSetting" runat="server" lbl_TextBefore="Number of Business Days before court date :"
                                lbl_TextAfter="" Attribute_Key="Days" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" height="34" class="clssubhead" align="right">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                    </td>
                                    <td align="right" valign="middle">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            <%--Sabir 4272 07/16/2008 Implement Ajax--%>
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                        Text="Please Wait ......" CssClass="clssubhead"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td height="11">
                            <%--Sabir 4272 07/16/2008 Imlement Ajax--%>
                            <%--Sabir 4272 07/16/2008 allowsorting and Onsorting properties of gv_records has been set for sorting and also the header property(sortexpression) of each column has been set.--%>
                            <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                                PagerSettings-Visible="true" Width="100%" AllowPaging="True" AllowSorting="True"
                                PageSize="20" OnPageIndexChanging="gv_Records_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="S#&nbsp;" HeaderStyle-CssClass="clssubhead">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid")+"&search=0" %>'
                                                Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="clssubhead">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.fullname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" Width="130px"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Language" HeaderStyle-CssClass="clssubhead">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLanguageSpeak" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.languagespeak") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bond" HeaderStyle-CssClass="clssubhead">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBond" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comments" HeaderStyle-CssClass="clssubhead">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComments" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.comments") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CRT Loc" HeaderStyle-Width="50px" HeaderStyle-CssClass="clssubhead">
                                        <ItemTemplate>
                                            <asp:Label ID="lblShortName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead" Width="50px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Insurance" HeaderStyle-CssClass="clssubhead" HeaderStyle-Width="80px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInsurance" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.insurance") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="CallBack Status" HeaderStyle-CssClass="clssubhead" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCallBackStatus" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ReminderCallStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trial Desc" HeaderStyle-CssClass="clssubhead" HeaderStyle-Width="135px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTrialDesc" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.trialdesc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First     " LastPageText="      Last &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

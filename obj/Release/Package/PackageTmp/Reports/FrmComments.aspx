<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.WebForm1" Codebehind="FrmComments.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Insert Comments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
	   function closewin()
	    {
	      opener.location.reload();	      	     
	      self.close();	   
	    }
	    function MaxLength()
		{
		    if(document.getElementById("txtComments").value.length >2000-27)//-27 bcoz to show last time partconcatenated with comments
		    {
		    alert("The length of character is not in the specified Range");
		    document.getElementById("txtComments").focus();
		    return false;
		    }
		}
		 function ValidateLenght()
	     { 
			var cmts = document.getElementById("txtComments").value;
			if (cmts.length > 2000-27)
			{
			    event.returnValue=false;
                event.cancel = true;
			}
	    }
	   
	    
		</script>
	</HEAD>
	<body onunload="closewin();" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" width="350" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
				<TR>
					<td colSpan="2">
						<Table id="tblsub" width="350" class="clsLeftPaddingTable" height="20" border="0" align="center"
							cellpadding="0" cellspacing="1">
							<tr>
								<TD vAlign="top">
									<P><STRONG><FONT color="#3366cc">Comments</FONT></STRONG></P>
								</TD>
							</tr>
							<tr>
								<TD><asp:textbox id="txtComments" runat="server" TextMode="MultiLine" Width="350px" Height="120px"
										CssClass="clstextarea" onkeypress="ValidateLenght();"></asp:textbox></TD>
							</tr>
							<TR>
								<TD align="right">
									<asp:button id="btn_submit" runat="server" Text="Update" CssClass="clsbutton" OnClientClick="return MaxLength();"></asp:button></TD>
							</TR>
						</Table>
					</td>
				<tr>
					<td background="../../images/separator_repeat.gif" colSpan="2" height="11"></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

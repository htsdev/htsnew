<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.TrialNotificationDL"
    CodeBehind="TrialNotificationDL.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Trial Notification Letter</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%" align="center"
        border="0">
        <tr>
            <td background="../Images/subhead_bg.gif" height="31">
            </td>
        </tr>
        <tr>
            <td>
                <table class="clsmainhead" id="Table3" cellspacing="0" cellpadding="0" width="100%"
                    align="left" border="0">
                    <tr>
                        <td valign="bottom" width="31">
                            <img height="17" src="../Images/head_icon.gif" width="31">
                        </td>
                        <td class="clssubhead" valign="baseline">
                            &nbsp;<asp:Label ID="lblTitle" runat="server"></asp:Label>
                        </td>
                        <td align="right" colspan="2">
                            <asp:ImageButton ID="IBtn" runat="server" Height="25px" ImageUrl="../Images/MSSmall.gif"
                                ToolTip="Word"></asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../Images/separator_repeat.gif" colspan="3" height="9">
            </td>
        </tr>
        <tr id="trpdf" height="100%">
            <td colspan="3">
                &nbsp;
                <iframe tabindex="0" src="frmTrial_NotificationDL.aspx?casenumber=<%=ViewState["vTicketId"]%>&reportnumber=<%=ViewState["vSelectedReport"]%>"
                    frameborder="1" width="98%" scrolling="auto" height="98%"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

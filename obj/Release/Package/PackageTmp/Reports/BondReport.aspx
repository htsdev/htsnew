﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BondReport.aspx.cs" Inherits="HTP.Reports.BondReport"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="updatefollowupinfo"
    TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bond Alert</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <link href="../Styles.css" type="text/css" rel="Stylesheet" />
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script src="../Scripts/boxover.js" type="text/javascript"></script>
    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>

     <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
     
        <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" >
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>
                

                 </div>

                 <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl_message" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>

                 </div>
                 </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title"> Bond Alert</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>
                  <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left"> Bond Alert</h2>
                     <div class="actions panel_actions pull-right">

                         <aspnew:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                     
                         <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                              <label class="form-label">Start Date :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                    <ProgressTemplate>
                                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                            CssClass="form-label" ></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>
                                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <div  class="table-responsive" data-pattern="priority-columns">
                                                            <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                                                            CssClass="table table-small-font table-bordered table-striped" OnRowCommand="gv_records_RowCommand" OnRowDataBound="gv_records_RowDataBound"
                                                            AllowSorting="True" OnSorting="gv_records_Sorting">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S#">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                        <asp:HiddenField ID="hf_TicketID" runat="server" 
                                                                            Value="<%# Bind('TicketID_PK') %>" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Last Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_lastname" runat="server" CssClass="form-label"  Text='<%# Bind("lastname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_firstname" runat="server" CssClass="form-label"  Text='<%# Bind("firstname") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Cause No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_causenum" runat="server" CssClass="form-label"  Text='<%# Bind("CauseNumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ticket No">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_ticketnumber" runat="server" CssClass="form-label"  Text='<%# Bind("TicketNumber") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Court Loc</u>" SortExpression="CRT_Location">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_crtloc" runat="server" CssClass="form-label"  Text='<%# Bind("CRT_Location") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lb_Status" runat="server" CssClass="form-label"  Text='<%# Bind("Status") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="FollowUpDate">
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_followup" runat="server" CssClass="form-label"  Text='<%# Bind("followupdate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <input type="hidden" id="hfTicketId" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' />
                                                                        <input type="hidden" id="hfComments" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.GeneralComments") %>' />
                                                                        <input type="hidden" id="hf_courtid" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.Courtid") %>' />
                                                                        <asp:ImageButton ID="img_Add" runat="server" CommandName="btnclick" ImageUrl="../Images/add.gif"
                                                                            CommandArgument='<%# Eval("TicketID_PK") %>' />
                                                                        <asp:HiddenField ID="hf_CourtCode" runat="server" Value="<%# Bind('courtid') %>" />
                                                                        <asp:HiddenField ID="hf_GeneralComments" runat="server" Value="<%# Bind('generalcomments') %>" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                        </div>
                                                        </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                    </div>
                                                                </div>
                         </div>

                    

                    
                     

                    </div>
                     </div>
                                                               </section>

             <div class="clearfix"></div>
            <uc3:updatefollowupinfo ID="UpdateFollowUpInfo1" runat="server" Title="Bond Follow Up Date" />

            <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlFollowup" runat="server">
                                                            <uc3:updatefollowupinfo ID="UpdateFollowUpInfo2" runat="server" Title="Bond Follow Up Date" />
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>

        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
                    PopupControlID="pnlFollowup" BackgroundCssClass="modalBackground" HideDropDownList="false">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
            </ContentTemplate>
        </aspnew:UpdatePanel>
       
    </form>





     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <%--<script language="JavaScript" type="text/javascript" src="../Scripts/wz_tooltip.js"></script>--%>






    <script language="javascript" type="text/javascript">
        //document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        //document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRptjurytrial.aspx.cs" Inherits="lntechNew.Reports.frmRptjurytrial" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">

    <title>Tried Cases Report</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
     <SCRIPT src="../Scripts/boxover.js" type="text/javascript"></SCRIPT>
     <SCRIPT SRC="../Scripts/ClipBoard.js"></SCRIPT>--%>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
     
     
     
<script>

        var err = null;
        
        function StateTracePoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).innerText;
		     ShowMsg()
		     //document.getElementById("txtb_StateTraceMsg").value = err;
		     //document.getElementById("txtb_StateTraceMsg").style.visibility = 'visible';
		    // document.getElementById("ddl_court").style.visibility='hidden';
		     return false;
		}
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function ShowMsg()
        {
            document.getElementById("txt_StateTraceMsg").value=err;
        }
        function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		    // document.getElementById("ddl_court").style.visibility='visible';
		}
</script>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
</head>

<body class=" ">
    <form id="form1" runat="server">
        <asp35:ScriptManager ID="ScriptManager1" runat="server"></asp35:ScriptManager>
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">
                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Tried Cases Report</h1>
                                <!-- PAGE HEADING TAG - END -->  
                            </div>   
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-xs-12">
                        <section class="box ">
                    
                                <div class="content-body">

                                    <div class="row">
                                
                                        <div class="col-md-6 col-sm-7 col-xs-8">
                                            <div class="form-group">
                                                <label class="form-label" for="field-1">From</label>
                                                <div class="controls">
                                                    <picker:datepicker id="dtFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                    <%--<ew:calendarpopup id="dtFrom" runat="server" EnableHideDropDown="True"
											            ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False"
											            Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Select start date">
											            <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
										            </ew:calendarpopup>--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-7 col-xs-8">
                                            <div class="form-group">
                                                <label class="form-label" for="field-1">To</label>
                                                <div class="controls">
                                                    <picker:datepicker id="dtTo" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                    <%--<ew:calendarpopup id="dtTo" runat="server" EnableHideDropDown="True"
											            ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
											            UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Select end date">
											            <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
										            </ew:calendarpopup>--%>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                
                                        <div class="col-md-6 col-sm-7 col-xs-8">
                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Attorney</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddl_attorney" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-7 col-xs-8">
                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Court</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddl_court" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <asp:label id="lblMessage" runat="server" CssClass="form-label" Visible="False"></asp:label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                
                                        <div class="col-md-6 col-sm-7 col-xs-8">
                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Attorney</label>
                                                <div class="controls">
                                                    <asp:Button ID="btn_search" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btn_search_Click" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                        </section>
                    </div>

                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Jury Trial</h2>
                                <div class="actions panel_actions pull-right">
                                    <table>
                                        <tr>
                                            <td><asp:label id="lblCurrPage" runat="server" class="form-label">Current Page :</asp:label></td>
										    <td><asp:label id="lblPNo" runat="server" class="form-label">0</asp:label></td>
										    <td><asp:label id="lblGoto" runat="server" class="form-label">Goto</asp:label></td>
										    <td><asp:dropdownlist id="cmbPageNo" runat="server" CssClass="form-control m-bot15" Font-Size="Smaller" AutoPostBack="True" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged"></asp:dropdownlist></td>
                                        </tr>
                                    </table>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <asp:datagrid id="dgJuryTrial" runat="server" AutoGenerateColumns="False" AllowPaging="false" CssClass="table" PageSize="12" OnItemDataBound="dgJuryTrial_ItemDataBound" OnPageIndexChanged="dgJuryTrial_PageIndexChanged">
						                    <Columns>
                            
                                                <asp:TemplateColumn HeaderText="Date">
                            
                                                    <ItemTemplate>
                                                        <asp:Label id="lblrecdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recdate","{0:d}") %>'></asp:Label>                                    
                                                        <asp:Label ID="lblrowid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rowid") %>' Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                            
                                                <asp:TemplateColumn HeaderText="Attorney">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblattorney" Text='<%# DataBinder.Eval(Container, "DataItem.attorney") %>'></asp:Label>                                    
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                            
                                                <asp:TemplateColumn HeaderText="Verdict">                            
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblverdict" Text='<%# DataBinder.Eval(Container, "DataItem.verdict") %>'></asp:Label>
                                                    </ItemTemplate>   
                                                </asp:TemplateColumn>
                            
                                                <asp:TemplateColumn HeaderText="Case No">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblcaseno" Text='<%# DataBinder.Eval(Container, "DataItem.caseno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                            
                                                <asp:TemplateColumn HeaderText="Fine">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfine" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fine","{0:C2}") %>'></asp:Label>
                                                    </ItemTemplate>  
                                                </asp:TemplateColumn>
                            
                                                <asp:TemplateColumn HeaderText="Brief Facts">
                                                    <ItemTemplate>
                                                        <DIV TITLE="div_status=[on] offsetx=[-418] offsety=[-10] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='400px'><tr><td><textarea id='txt_StateTraceMsg' class='form-control' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
                                                            <asp:Label ID="lblbrieffacts" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.brieffacts") %>' style="Display :none"></asp:Label>
                                                            <asp:ImageButton ID="ImageButton1" CssClass="btn btn-primary" runat="server" ImageUrl="~/Images/textfile.gif"  />
                                                        </DIV>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                            
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:HyperLink id ="hlnk_edit" runat="server" NavigateUrl='<%# "../activities/jurytrial.aspx?id="+DataBinder.Eval(Container, "DataItem.rowid") %>'    Text="Edit" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
						
						                    </Columns>
						                    <PagerStyle NextPageText="Next &gt;" PrevPageText="&lt; Previous" HorizontalAlign="Center" PageButtonCount="5"></PagerStyle>
					                    </asp:datagrid>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->
            <div class="page-chatapi hideit">

            <div class="search-bar">
                <input type="text" placeholder="Search" class="form-control">
            </div>

            <div class="chat-wrapper">
                <h4 class="group-head">Groups</h4>
                <ul class="group-list list-unstyled">
                    <li class="group-row">
                        <div class="group-status available">
                            <i class="fa fa-circle"></i>
                        </div>
                        <div class="group-info">
                            <h4><a href="#">Work</a></h4>
                        </div>
                    </li>
                    <li class="group-row">
                        <div class="group-status away">
                            <i class="fa fa-circle"></i>
                        </div>
                        <div class="group-info">
                            <h4><a href="#">Friends</a></h4>
                        </div>
                    </li>

                </ul>


                <h4 class="group-head">Favourites</h4>
                <ul class="contact-list">

                        <li class="user-row " id='chat_user_1' data-user-id='1'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clarine Vassar</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_2' data-user-id='2'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Brooks Latshaw</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_3' data-user-id='3'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clementina Brodeur</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                </ul>


                <h4 class="group-head">More Contacts</h4>
                <ul class="contact-list">

                        <li class="user-row " id='chat_user_4' data-user-id='4'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Carri Busey</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_5' data-user-id='5'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Melissa Dock</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_6' data-user-id='6'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Verdell Rea</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_7' data-user-id='7'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Linette Lheureux</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_8' data-user-id='8'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Araceli Boatright</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_9' data-user-id='9'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clay Peskin</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_10' data-user-id='10'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Loni Tindall</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_11' data-user-id='11'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Tanisha Kimbro</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row " id='chat_user_12' data-user-id='12'>
                            <div class="user-img">
                                <a href="#"><img src="../data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Jovita Tisdale</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                </ul>
            </div>

        </div>


        <div class="chatapi-windows ">




        </div>    </div>
        <!-- END CONTAINER -->
    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>































<%--<body>
    <form id="form1" runat="server">
    	<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<TBODY>
				<TR>
					<TD style="width: 790px">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<tr>
			<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD align="center" style="width: 790px">
						<TABLE id="tblSelectionCriteria" cellSpacing="0" cellPadding="0" width="100%" border="0">
							
								<TR>
									<TD style="width: 97px" Height="15px">
										<asp:label id="lblFromDate" runat="server" Height="16px" CssClass="frmtd" Width="65px"> From:</asp:label>
										</TD>
									<TD style="width: 152px"><ew:calendarpopup id="dtFrom" runat="server" Width="80px" EnableHideDropDown="True" Font-Names="Tahoma"
											ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False"
											Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Select start date"
											ImageUrl="../images/calendar.gif" Font-Size="8pt">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup>
										</TD>
									<TD style="width: 80px">
										<asp:label id="lblToDate" runat="server" Height="16px" CssClass="frmtd" Width="60px">To:</asp:label></TD>
									<TD style="WIDTH: 131px"><ew:calendarpopup id="dtTo" runat="server" Width="80px" EnableHideDropDown="True" Font-Names="Tahoma"
											ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
											UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Select end date" ImageUrl="../images/calendar.gif"
											Font-Size="8pt">
											<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
											<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></WeekdayStyle>
											<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></MonthHeaderStyle>
											<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
												BackColor="AntiqueWhite"></OffMonthStyle>
											<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></GoToTodayStyle>
											<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGoldenrodYellow"></TodayDayStyle>
											<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Orange"></DayHeaderStyle>
											<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="LightGray"></WeekendStyle>
											<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="Yellow"></SelectedDateStyle>
											<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></ClearDateStyle>
											<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
												BackColor="White"></HolidayStyle>
										</ew:calendarpopup></TD>
									<TD >
										</TD>
								</TR>
								<TR>
									<TD style="width: 97px">
										<asp:label id="lblPCCR" runat="server" CssClass="frmtd" Width="85px">Attorney:</asp:label>
										</TD>
									<TD style="width: 152px">
                                        <asp:DropDownList ID="ddl_attorney" runat="server" CssClass="label" Width="106px">
                                        </asp:DropDownList></TD>
									<TD style="width: 80px">
										<asp:label id="Label1" runat="server" CssClass="frmtd" Width="64px">Court:</asp:label>
										</TD>
									<TD>
                                        <asp:DropDownList ID="ddl_court" runat="server" CssClass="label" Width="235px">
                                        </asp:DropDownList></TD>
									<TD align="left">&nbsp; &nbsp;<asp:Button ID="btn_search" runat="server" CssClass="clsbutton" 
                                            Text="Search" OnClick="btn_search_Click" />&nbsp;&nbsp;&nbsp;</TD>
					
					
				</TR>
			</TABLE>
			<tr>
			<td style="width: 790px" align="center">
			<asp:label id="lblMessage" runat="server" Font-Size="X-Small" ForeColor="Red" Width="414px" Visible="False"></asp:label>
			</TD>
			</TR>
			<tr>
			<td background="../../images/separator_repeat.gif" colSpan="7" height="11"></td>
				</tr>
			<TR>
				<td style="width: 790px">
					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
						<tr>
							<td class="clsbutton" width="155" height="20" align="left"><font color="#ffffff">&nbsp;&nbsp;Jury Trial</font></td>
							<td class="clsbutton" align="right" width="20%">
								<table id="tblPageNavigation" cellSpacing="0" cellPadding="0" border="0" style="display:none">
									<tr>
										<td align="right" width="28%" style="height: 18px"><asp:label id="lblCurrPage" runat="server" Width="97px" Font-Names="Verdana" Font-Size="8.5pt"
												ForeColor="White" Font-Bold="True">Current Page :</asp:label></td>
										<td align="left" style="height: 18px; width: 27%;"><asp:label id="lblPNo" runat="server" Width="48px" Font-Names="Verdana" Font-Size="8.5pt" ForeColor="White" Font-Bold="True">0</asp:label></td>
										<td align="right" width="28%" style="height: 18px"><asp:label id="lblGoto" runat="server" Width="16px" Font-Names="Verdana" Font-Size="8.5pt"
												ForeColor="White" Font-Bold="True">Goto</asp:label></td>
										<td align="right" width="17%" style="height: 18px"><asp:dropdownlist id="cmbPageNo" runat="server" CssClass="frmtd" Font-Size="Smaller"
												AutoPostBack="True" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged">
                                        </asp:dropdownlist></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</TR>
			<TR>
				<TD style="width: 790px"><asp:datagrid id="dgJuryTrial" runat="server" Width="781px" AutoGenerateColumns="False" AllowPaging="false"
						PageSize="12" OnItemDataBound="dgJuryTrial_ItemDataBound" OnPageIndexChanged="dgJuryTrial_PageIndexChanged">
						<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
						<Columns>
                            
                            <asp:TemplateColumn HeaderText="Date">
                            
                                <ItemTemplate>
                                    <asp:Label id="lblrecdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recdate","{0:d}") %>'></asp:Label>                                    
                                    <asp:Label ID="lblrowid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rowid") %>'
                                        Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Attorney">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblattorney" Text='<%# DataBinder.Eval(Container, "DataItem.attorney") %>'></asp:Label>                                    
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Verdict">                            
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblverdict" Text='<%# DataBinder.Eval(Container, "DataItem.verdict") %>'></asp:Label>
                                </ItemTemplate>                                
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Case No">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblcaseno" Text='<%# DataBinder.Eval(Container, "DataItem.caseno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Fine">
                                <ItemTemplate>
                                    <asp:Label ID="lblfine" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fine","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>                                
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Brief Facts">
                                <ItemTemplate>
                                
                                                       
                                    
                                     <DIV TITLE="div_status=[on] offsetx=[-418] offsety=[-10] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='400px'><tr><td><textarea id='txt_StateTraceMsg' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
                                <asp:Label ID="lblbrieffacts" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.brieffacts") %>' style="Display :none"></asp:Label>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/textfile.gif"  />
                                
                                &nbsp;</DIV>
                                
                                  
                                    
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:HyperLink id ="hlnk_edit" runat="server" NavigateUrl='<%# "../activities/jurytrial.aspx?id="+DataBinder.Eval(Container, "DataItem.rowid") %>'    Text="Edit" ></asp:HyperLink>
                                    
                                </ItemTemplate>
                                 <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                            </asp:TemplateColumn>
						
						</Columns>
						<PagerStyle NextPageText="Next &gt;" PrevPageText="&lt; Previous" HorizontalAlign="Center" PageButtonCount="5"></PagerStyle>
					</asp:datagrid></TD>
			</TR>
			<tr>
			<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
												<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
											</TR>
			
			</TBODY>
			</TABLE>
    </form>
</body>--%>
</html>

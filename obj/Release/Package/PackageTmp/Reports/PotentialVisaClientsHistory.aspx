<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PotentialVisaClientsHistory.aspx.cs"
    MaintainScrollPositionOnPostback="true" Inherits="HTP.Reports.PotentialVisaClientsHistory" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
    <title>Potential Visa Clients</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function HideModalPopup(popupId) {
            var modalPopupBehavior = $find(popupId);
            modalPopupBehavior.hide();
            return false;
        }
        //check all 
        function checkallDataGrid(con, gridname) {

            gridname = gridname.id;
            var grid = document.getElementById(gridname);
            var rows = grid.rows.length;
            var col = 14;

            var elmt;
            for (i = 0; i < rows; i++) {
                elmt = null;
                if (grid.rows[i].cells.length >= col && grid.rows[i].cells[col].getElementsByTagName("INPUT").length > 0) {
                    elmt = grid.rows[i].cells[col].getElementsByTagName("INPUT")[0];
                    elmt.checked = con.checked;
                }
            }

        }

        function checkDataGrid(con, gridname) {

            gridname = gridname.id;
            var grid = document.getElementById(gridname);
            var rows = grid.rows.length;
            var elmt;
            var count = 0;
            var col = 14;

            for (i = 1; i < rows; i++) {
                elmt = null;
                if (grid.rows[i].cells.length >= col && grid.rows[i].cells[col].getElementsByTagName("INPUT").length > 0) {
                    elmt = grid.rows[i].cells[col].getElementsByTagName("INPUT")[0];
                    if (elmt.checked) {
                        count++;
                    }

                }
            }
            if (count == rows - 1)
                grid.rows[0].cells[col].getElementsByTagName("INPUT")[0].checked = true;
            else
                grid.rows[0].cells[col].getElementsByTagName("INPUT")[0].checked = false;
        }

        function validategrid(gridname) {
            var grid = document.getElementById(gridname);
            if (grid == null) {
                alert("Please open and select atleast one record");
                return false;
            }
            var rows = grid.rows.length;
            var elmt;
            var count = 0;
            var col = 14;

            for (i = 1; i < rows; i++) {
                elmt = null;
                if (grid.rows[i].cells.length >= col && grid.rows[i].cells[col].getElementsByTagName("INPUT").length > 0) {
                    elmt = grid.rows[i].cells[col].getElementsByTagName("INPUT")[0];
                    if (elmt.checked) {
                        return true;
                    }

                }
            }
            alert("Please open and select atleast one record");
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline" 
        UpdateMode="Conditional">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" width="950px" align="center" border="0">
                <tbody>
                    <tr>
                        <td>
                            <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td id="tr_pagging" class="clsLeftPaddingTable" align="left" runat="server" valign="middle">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="clsLeftPaddingTable">
                                    <td class="clssubhead" style="width: 100px;" align="left">
                                        Date of Birth :
                                    </td>
                                    <td style="width: 110px;" align="left">
                                        <ew:CalendarPopup ID="txtDOB" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                            ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                            PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                            ToolTip="Date of Birth" UpperBoundDate="12/31/9999 23:59:00" Width="82px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>
                                    </td>
                                    <td align="left" style="width: 20px;">
                                        <asp:CheckBox ID="cbDOBFilter" runat="server" CssClass="clsLabelNew" />
                                    </td>
                                    <td class="clssubhead" style="width: 100px;" align="right">
                                        Date of Inquiry : 
                                    </td>
                                    <td align="left" style="width: 120px;">
                                        &nbsp;<ew:CalendarPopup ID="txtDateOfInquiry" runat="server" AllowArbitraryText="False"
                                            CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20"
                                            EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Nullable="True" PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True"
                                            ShowGoToToday="True" ToolTip="Date of Inquiry" UpperBoundDate="12/31/9999 23:59:00"
                                            Width="85px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>
                                    </td>
                                    <td align="left" style="width: 20px;">
                                        <asp:CheckBox ID="cbDOIFilter" runat="server" CssClass="clsLabelNew" />
                                    </td>
                                    <td align="right" style="width: 80px">
                                        <span class="clssubhead">Case Status :</span>
                                    </td>
                                    <td align="left">
                                        <%--Sabir Khan 4749 09/11/2008--%>
                                        &nbsp;<asp:DropDownList ID="ddlCaseStatus" runat="server" CssClass="clsInputadministration"
                                            Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                    </td>
                                    <td style="width: 240px;" align="right">
                                        <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_search_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" colspan="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="clssubhead" style="width: 100px;">
                                        &nbsp;</td>
                                    <td align="left" style="width: 110px;">
                                        &nbsp;</td>
                                    <td align="left" style="width: 40px;">
                                        &nbsp;</td>
                                    <td align="left" class="clssubhead" style="width: 120px;">
                                        &nbsp;</td>
                                    <td align="left" style="width: 120px;">
                                        &nbsp;</td>
                                    <td align="left" style="width: 40px;">
                                        &nbsp;</td>
                                    <td align="left" style="width: 100px">
                                        &nbsp;</td>
                                    <td align="right" colspan="3">
                                        <asp:HyperLink ID="hp" runat="server" NavigateUrl="PotentialVisaClients.aspx">Back</asp:HyperLink>
                                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <%--Sabir Khan 4635 09/26/2008--%>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="img_pagging" runat="server">
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                <ProgressTemplate>
                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                        CssClass="clsLabel"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>
                            <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                OnRowDataBound="gv_records_RowDataBound" Width="100%" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                                PageSize="20" AllowSorting="True" OnSorting="gv_records_Sorting" OnRowCommand="gv_records_RowCommand">
                                <PagerSettings PreviousPageText="&amp;lt; Previous " Mode="NextPreviousFirstLast"
                                    LastPageText="Last &amp;gt;&amp;gt;" FirstPageText="&amp;lt;&amp;lt; First" NextPageText="Next &amp;gt; ">
                                </PagerSettings>
                                <Columns>
                                    <asp:TemplateField HeaderText="S#">
                                        <HeaderStyle Width="10%" CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hl_sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                Text='<%# Container.DataItemIndex + 1 %>'></asp:HyperLink>
                                           <asp:HiddenField ID="hf_TicketId" Value='<%# Eval("ticketid_pk") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="firstname" HeaderText="First Name">
                                        <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("firstname") %>' CssClass="clsLeftPaddingTable"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="LastName" HeaderText="Last Name">
                                        <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_lname" runat="server" Text='<%# Eval("LastName") %>' CssClass="clsLeftPaddingTable"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DOB" HeaderText="Date of Birth">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DOB" runat="server" Text='<%# Eval("DOB") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="Comments" HeaderText="Immigration Comments" ItemStyle-Width="200px">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Comments" runat="server" Text='<%# Eval("Comments") %>' CssClass="clsLeftPaddingTable" Width="200px" style="word-wrap:break-word;">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="Address" HeaderText="Address">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Address" runat="server" Text='<%# Eval("Address") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="City" HeaderText="City">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_City" runat="server" Text='<%# Eval("City") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="State" HeaderText="State">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_State" runat="server" Text='<%# Eval("State") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="Zip" HeaderText="Zip">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Zip" runat="server" Text='<%# Eval("Zip") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="Contact1" HeaderText="Contact Numbers">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Contact1" runat="server" Text='<%# Eval("Contact1").ToString().Replace("(-)", "").Replace("()", "")%>' CssClass="clsLeftPaddingTable"></asp:Label><br />
                                            <asp:Label ID="lbl_Contact2" runat="server" Text='<%# Eval("Contact2").ToString().Replace("(-)", "").Replace("()", "")%>' CssClass="clsLeftPaddingTable"></asp:Label><br />
                                            <asp:Label ID="lbl_Contact3" runat="server" Text='<%# Eval("Contact3").ToString().Replace("(-)", "").Replace("()", "")%>' CssClass="clsLeftPaddingTable"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="InquiryDate" HeaderText="Date of Inquiry">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_InquiryDate" runat="server" Text='<%# Eval("InquiryDate") %>'
                                                CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="Status" HeaderText="Status">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Status" runat="server" Text='<%# Eval("Status") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="PrintDate" HeaderText="Print Date & Time">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PrintDate" runat="server" Text='<%# Eval("PrintDate") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="LetterName" HeaderText="Type of Letter">
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_LetterType" runat="server" Text='<%# Eval("LetterName") %>' CssClass="clsLeftPaddingTable">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Literal ID="lbl_ViewImage" runat="server" Text='<%# Eval("ViewImage") %>'>
                                            </asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ajaxToolkit:ModalPopupExtender ID="mpeShowPolm" runat="server" TargetControlID="polmHiddenButton"
                                PopupControlID="pnlPolm" BackgroundCssClass="modalBackground" DropShadow="false">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button runat="server" ID="polmHiddenButton" Text="More" Style="display: none;" />
                            <asp:Panel ID="pnlPolm" runat="server">
                                <table id="Table2" bgcolor="white" border="1" style="border-top: black thin solid;
                                    border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;"
                                    cellpadding="0" cellspacing="0" width="250px">
                                    <tr>
                                        <td align="left" valign="bottom" background="../Images/subhead_bg.gif" colspan="2">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td style="height: 26px" class="clssubhead">
                                                        <asp:Label runat="server" ID="lbl_title" Text="Update Case Status"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:LinkButton ID="lbtn_close2" runat="server" OnClientClick="return HideModalPopup('mpeShowPolm');">X</asp:LinkButton>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clssubhead" align="center">
                                            Case Status
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlUpdateCaseStatus" runat="server" CssClass="clsInputadministration"
                                                Width="100px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                            <asp:HiddenField ID="hfTicketId" Value="0" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td height="11">
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

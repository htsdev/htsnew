﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PastDueCalls.aspx.cs" Inherits="HTP.Reports.PastDueCalls" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Past Due Calls Alert</title>
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet" />--%>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class=" ">
    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
    
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">
                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">PAst Due Calls Alert</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->
                    <aspnew:UpdatePanel ID="pnl_main" runat="server">
                        <ContentTemplate>
    
                            <div class="col-xs-12">
                                <section class="box ">
                                    <div class="content-body">

                                        <div class="row">
                                            
                                            <div class="col-md-6 col-sm-4 col-xs-6">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Case Type</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddlCaseTypes" runat="server" CssClass="form-control m-bot15">
                                                            <asp:ListItem Value="0">All</asp:ListItem>
                                                            <asp:ListItem Value="1">Traffic</asp:ListItem>
                                                            <asp:ListItem Value="2">Criminal</asp:ListItem>
                                                            <asp:ListItem Value="4">Family Law</asp:ListItem>
                                                            <asp:ListItem Value="5">Other</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-sm-4 col-xs-6">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">&nbsp;</label>
                                                    <div class="controls">
                                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" OnClick="btnSubmit_Click" Text="Submit" />
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">

                                                <div class="form-group">
                                                    <asp:Label ID="lblMessage" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </section>
                            </div>

                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                <ProgressTemplate>
                                    <img alt="" src="../images/plzwait.gif" />&nbsp;
                                    <asp:Label ID="lbl1" runat="server" CssClass="form-label" Text="Please Wait ......"></asp:Label>
                                </ProgressTemplate>
                            </aspnew:UpdateProgress>

                            <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <div class="actions panel_actions pull-right">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                                                CellPadding="3" PageSize="20" CssClass="table" OnSorting="gv_records_Sorting">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hl_Sno" CssClass="form-label" runat="server" Text='<%# Eval("SNo") %>'
                                                                NavigateUrl='<%# "../clientinfo/ViolationFeeold.aspx?casenumber=" + Eval("TicketID_PK")+"&search=0" %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="ClientName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCli" runat="server" Text='<%# Bind("ClientName", "{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Crt</u>" SortExpression="crt">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCrt" runat="server" Text='<%# Bind("crt", "{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Crt Type</u>" SortExpression="CrtType">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCrtType" runat="server" Text='<%# Bind("CrtType", "{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Crt Date</u>" SortExpression="CourtDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCourtDate" runat="server" Text='<%# Eval("CourtDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Total Fee</u>" SortExpression="TotalFeeCharged">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTotalFeeCharged" runat="server" Text='<%# Bind("TotalFeeCharged", "{0:C0}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Amt Due</u>" SortExpression="Owes">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOwes" runat="server" Text='<%# Bind("Owes", "{0:C0}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Payment Due</u>" SortExpression="PaymentDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Follow up</u>" SortExpression="PaymentFollowUpdate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPaymentFollowUpdate" runat="server" Text='<%# Eval("PaymentFollowUpdate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<u>Past Days</u>" SortExpression="Pastdays">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPastdays" runat="server" Text='<%# Bind("Pastdays") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        </ContentTemplate>
                    </aspnew:UpdatePanel>
                    
                    <!-- MAIN CONTENT AREA ENDS -->

                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>





















<%--<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="pnl_main" runat="server">
            <ContentTemplate>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="780">
                    <tbody>
                        <tr>
                            <td colspan="4" style="height: 14px">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cssclass="clsLeftPaddingTable" border="0" cellpadding="0" cellspacing="0"
                                    width="780">
                                    <tr>
                                        <td class="clssubhead" align="left">
                                            Case Type :&nbsp;&nbsp;
                                            <asp:DropDownList ID="ddlCaseTypes" runat="server" CssClass="clsInputCombo" Width="80px">
                                                <asp:ListItem Value="0">All</asp:ListItem>
                                                <asp:ListItem Value="1">Traffic</asp:ListItem>
                                                <asp:ListItem Value="2">Criminal</asp:ListItem>
                                                <asp:ListItem Value="4">Family Law</asp:ListItem>
                                                <asp:ListItem Value="5">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" OnClick="btnSubmit_Click"
                                                Text="Submit" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" align="right" valign="middle" style="height: 34px">
                                <table>
                                    <tr>
                                        <td colspan="2" style="text-align: right;" valign="middle">
                                            <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="pnl_main">
                                    <ProgressTemplate>
                                        <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                            CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                    AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                                    CellPadding="3" PageSize="20" CssClass="clsLeftPaddingTable" OnSorting="gv_records_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="S#">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hl_Sno" CssClass="clssubhead" runat="server" Text='<%# Eval("SNo") %>'
                                                    NavigateUrl='<%# "../clientinfo/ViolationFeeold.aspx?casenumber=" + Eval("TicketID_PK")+"&search=0" %>'></asp:HyperLink>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="ClientName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCli" runat="server" Text='<%# Bind("ClientName", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt</u>" SortExpression="crt">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCrt" runat="server" Text='<%# Bind("crt", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt Type</u>" SortExpression="CrtType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCrtType" runat="server" Text='<%# Bind("CrtType", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Crt Date</u>" SortExpression="CourtDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourtDate" runat="server" Text='<%# Eval("CourtDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Total Fee</u>" SortExpression="TotalFeeCharged">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalFeeCharged" runat="server" Text='<%# Bind("TotalFeeCharged", "{0:C0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Amt Due</u>" SortExpression="Owes">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOwes" runat="server" Text='<%# Bind("Owes", "{0:C0}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" Font-Underline="True" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Payment Due</u>" SortExpression="PaymentDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Follow up</u>" SortExpression="PaymentFollowUpdate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentFollowUpdate" runat="server" Text='<%# Eval("PaymentFollowUpdate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<u>Past Days</u>" SortExpression="Pastdays">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPastdays" runat="server" Text='<%# Bind("Pastdays") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                            <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>--%>
</html>

<%@ Page Language="C#" AutoEventWireup="true" Codebehind="FreeConsultationComments.aspx.cs"
    Inherits="lntechNew.Reports.FreeConsultationComments" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2"
    Namespace="eWorld.UI" TagPrefix="ew" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Consultation Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td style="width: 55px" class="clslabel">
                                    From
                                </td>
                                <td style="width: 136px">
                                    <ew:CalendarPopup ID="dtp_From" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                        Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td class="clslabel" style="width: 30px">
                                    To
                                </td>
                                <td style="width: 128px">
                                    <ew:CalendarPopup ID="dtp_To" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                        Width="86px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td>
                                    <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="clsbutton" OnClick="btn_Submit_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                </tr>
                 <tr>
            <td width="100%" background="../Images/separator_repeat.gif"  height="11"  ></td>
            </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                            CssClass="clsleftpaddingtable" AllowSorting="True" OnSorting="gv_Records_Sorting" AllowPaging="True" OnPageIndexChanging="gv_Records_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="SNo">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <ControlStyle Width="10%" />
                                    <HeaderStyle Width="8%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="First Name">
                                    <ControlStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_FirstName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Last Name">
                                    <ControlStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_LastName" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rep" SortExpression="username">
                                    <HeaderStyle ForeColor="Black" />
                                    <ControlStyle Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblusername" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                 <asp:TemplateField HeaderText="Rec Date" SortExpression="sortcommentsdate">
                                    <HeaderStyle ForeColor="Black" />
                                    <ControlStyle Width="10%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblrecdate" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.commentsdate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Comments" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="55%" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td width="100%" background="../Images/separator_repeat.gif" height="11">
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

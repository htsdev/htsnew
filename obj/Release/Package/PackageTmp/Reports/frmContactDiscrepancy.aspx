<%@ Page Language="C#" AutoEventWireup="true" Codebehind="frmContactDiscrepancy.aspx.cs"
    Inherits="lntechNew.Reports.frmContactDiscrepancy" EnableEventValidation="false" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact Discrepancy</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
  		
		function closepopup()
		{		     
		      document.getElementById("pnl_records").style.display = 'none';
		      document.getElementById("DisableDive").style.display = 'none';		      
		      return false;
		}
		function FillUpdata()
		{
		    if(document.getElementById("ddl_address").innerText == "")
		    {
		    alert("Please select Address.");
		    document.getElementById("ddl_address").selectedIndex=0;
		    return false;
		    }
		    
		    document.getElementById("hf_addresses").value = document.getElementById("ddl_address").value;		    
		    document.getElementById("hf_midnumber").value = document.getElementById("lbl_midnumber").innerText;	
		    closepopup();
		    plzwait();	    
		}
		function plzwait()
		{
		
		document.getElementById("tbl_plzwait1").style.display = 'block';
		document.getElementById("tbl_grid").style.display = 'none';
		
		}
		
		
		function submitAddress(canupdatecontact)
		{	   
		   
		    document.getElementById(canupdatecontact)
		    
		    var chk = confirm("Would you also like to update all the contact information of the Client. [Ok = Yes || Cancel = No]");
		    
		    if (chk)
		    document.getElementById(canupdatecontact).value = "1";
		    else
		    document.getElementById(canupdatecontact).value = "0";
		    
		    closepopup();
		    plzwait();
		    return true;
		}
		
		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="DisableDive" style="display: none; position: absolute; left: 1; top: 1;
            height: 1px; background-color: Silver; filter: alpha(opacity=50)">
            <table width="100%" height="100%">
                <tr>
                    <td style="width: 100%; height: 100%">
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            </aspnew:ScriptManager>
            <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
                border="0">
                <tr>
                    <td>
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table id="tbl_grid" runat="server" cellpadding="0" width="100%" cellspacing="0">
                                                <tr>
                                                    <td align="center" background="../images/separator_repeat.gif" style="height: 12px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lbl_Message" runat="server" CssClass="label" ForeColor="OrangeRed"
                                                            Font-Bold="True"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="clsleftpaddingtable" valign="top" align="right" id='td_pageno' runat="server">
                                                        <asp:Label ID="lblPage" runat="server" Text="Goto Page:"></asp:Label><asp:DropDownList ID="ddl_Pages" runat="server" CssClass="clsinputcombo" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddl_Pages_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gv_ContactDisc" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                                                            Width="100%" AllowPaging="True" PageSize="20" AllowSorting="True" OnRowDataBound="gv_ContactDisc_RowDataBound"
                                                            OnPageIndexChanging="gv_ContactDisc_PageIndexChanging" OnRowCommand="gv_ContactDisc_RowCommand">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="S.No">
                                                                    <HeaderStyle Width="4%" HorizontalAlign="Left" CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hl_SNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>' Visible="False"></asp:HyperLink>
                                                                        <asp:Label ID="lbl_sno" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.SNO") %>'></asp:Label>
                                                                        <asp:HiddenField ID="hf_TicketID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>' />
                                                                        &nbsp;
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <HeaderStyle Width="18%" HorizontalAlign="Left" CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Name" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ClientName") %>' Visible="False"></asp:Label>
                                                                        <asp:HyperLink ID="hl_clientname" runat="server" NavigateUrl='<%# "../ClientInfo/violationfeeold.aspx?search=0&casenumber="  + Eval("ticketid_pk") %>'
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.ClientName") %>'></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mid Number">
                                                                    <HeaderStyle Width="12%" HorizontalAlign="Left" CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <%--<asp:Label ID="lbl_MidNumber" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MidNum") %>'></asp:Label>--%>
                                                                        <asp:HyperLink ID="hl_midnum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MidNum") %>'
                                                                            Visible="False"></asp:HyperLink>
                                                                        <asp:LinkButton ID="lnkbtn_midnum" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.MidNum") %>'
                                                                            CommandName="showaddress" Text='<%# DataBinder.Eval(Container, "DataItem.MidNum") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Address">
                                                                    <HeaderStyle Width="20%" HorizontalAlign="Left" CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Address" CssClass="label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="City ">
                                                                    <HeaderStyle CssClass="clssubhead" Width="12%"/>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_city" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.city") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="State">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_state" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.state") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Zip">
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_zip" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.zipcode") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Contact No">
                                                                    <HeaderStyle Width="23%" HorizontalAlign="Left" CssClass="clssubhead" />
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_contact1" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Contact1") %>' />
                                                                        <asp:HiddenField ID="hf_contact2" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Contact2") %>' />
                                                                        <asp:HiddenField ID="hf_contact3" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>' />
                                                                        <asp:HiddenField ID="hf_contacttype1" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ContactType1") %>' />
                                                                        <asp:HiddenField ID="hf_contacttype2" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ContactType2") %>' />
                                                                        <asp:HiddenField ID="hf_contacttype3" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ContactType3") %>' />
                                                                        <asp:Label ID="lbl_Contacts" runat="server" CssClass="label" Text='Contact'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tbl_plzwait1" style="display: none" width="800">
                                                <tr>
                                                    <td class="clssubhead" valign="middle" align="center">
                                                        <img src="../Images/plzwait.gif" />
                                                        Please wait While we update your selected Address.
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="pnl_records" style="position: absolute; top: 400p; left: 400px; display: none;">
                                                <table id="Table1" cellpadding="0" cellspacing="1" style="background-color: White;
                                                    border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                                    border-bottom: black thin solid;" border="0">
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" colspan="2" style="height: 34px; background-image: url(../Images/subhead_bg.gif);"
                                                            background="../images/headbar_headerextend.gif" valign="middle">
                                                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                <tr>
                                                                    <td class="clssubhead">
                                                                        <strong>Address Updation </strong>
                                                                    </td>
                                                                    <td align="right">
                                                                        &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" OnClientClick='return closepopup();'>X</asp:LinkButton>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DataList ID="dl_addresses" runat="server" OnItemCommand="dl_addresses_ItemCommand"
                                                                OnItemDataBound="dl_addresses_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <table border="0" cellpadding="1" cellspacing="1" class="clsleftpaddingtable" style="width: 300px">
                                                                        <tr>
                                                                            <td class="clslabel" style="width: 90px; height: 33px">
                                                                                <strong>&nbsp;Address 1</strong></td>
                                                                            <td colspan="2">
                                                                                &nbsp;<asp:Label ID="lbl_address1" runat="server" CssClass="clslabel" Text='<%# Bind("address1") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="width: 90px; height: 29px">
                                                                                <strong>&nbsp;Address 2</strong></td>
                                                                            <td colspan="2" style="height: 29px">
                                                                                &nbsp;<asp:Label ID="lbl_address2" runat="server" CssClass="clslabel" Text='<%# Bind("address2") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="width: 90px; height: 26px">
                                                                                <strong>&nbsp;City</strong></td>
                                                                            <td style="width: 118px; height: 26px">
                                                                                &nbsp;<asp:Label ID="lbl_city" runat="server" CssClass="clslabel" Text='<%# Bind("city") %>'></asp:Label></td>
                                                                            <td class="clslabel" style="width: 142px; height: 26px">
                                                                                <strong>State </strong>
                                                                                <asp:Label ID="lbl_state" runat="server" CssClass="clslabel" Text='<%# Bind("state") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="width: 90px; height: 26px">
                                                                                &nbsp;<strong>Zip Code</strong></td>
                                                                            <td colspan="2" style="height: 26px">
                                                                                &nbsp;<asp:Label ID="lbl_zip" runat="server" CssClass="clslabel" Text='<%# Bind("zipcode") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="height: 26px">
                                                                                <strong>&nbsp;Contact 1</strong></td>
                                                                            <td colspan="2" style="height: 26px">
                                                                                &nbsp;<asp:Label ID="lbl_contact1" runat="server" CssClass="clslabel" Text='<%# Bind("contact1") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="height: 26px">
                                                                                <strong>&nbsp;Contact 2</strong></td>
                                                                            <td colspan="2" style="height: 26px">
                                                                                &nbsp;<asp:Label ID="lbl_contact2" runat="server" CssClass="clslabel" Text='<%# Bind("contact2") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="height: 28px">
                                                                                <strong>&nbsp;Contact 3</strong></td>
                                                                            <td colspan="2" style="height: 28px">
                                                                                &nbsp;<asp:Label ID="lbl_contact3" runat="server" CssClass="clslabel" Text='<%# Bind("contact3") %>'></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="clslabel" style="height: 21px">
                                                                            </td>
                                                                            <td align="right" colspan="2" style="height: 21px">
                                                                                <asp:HiddenField ID="hf_updatecontact" runat="server" Value="0" />
                                                                                <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Bind("ticketid_pk") %>' />
                                                                                <asp:Button ID="btn_save" runat="server" CssClass="clsbutton" Text="Update" Width="58px"
                                                                                    CommandArgument='<%# Bind("midnum") %>' CommandName="updateaddress" /></td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <asp:HiddenField ID="hf_showpopup" runat="server" Value="0" />
                                        </ContentTemplate>
                                    </aspnew:UpdatePanel>

                                    <script type="text/javascript">
				    
	       
       
                                var postbackElement;
                                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
                                
        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
        }
       function pageLoaded(sender, args) 
       {
            
               // Show Address Popup
               if ( document.form1.hf_showpopup.value == "1")
               {
                //Display Popup
	            document.getElementById("pnl_records").style.display = 'block';
                document.getElementById("DisableDive").style.display = 'block';
                // Set Popup Location
                var top  = 400;
	            var left = 400;
	            var height = document.body.offsetHeight;
	            var width  = document.body.offsetWidth
                // Setting Panel Location
	            if ( width > 1100 || width <= 1280) left = 575;
	            if ( width < 1100) left = 500;
              
                document.getElementById("pnl_records").style.top = top;		
                document.getElementById("pnl_records").style.left = left;		
                // Display Background Div
		        if (  document.body.offsetHeight > document.body.scrollHeight )
		            document.getElementById("DisableDive").style.height = document.body.offsetHeight;
		        else
		            document.getElementById("DisableDive").style.height = document.body.scrollHeight ;
    			
		        document.getElementById("DisableDive").style.width = document.body.offsetWidth;
		        		        
		       }
              
              document.form1.hf_showpopup.value = "0";                    
       }
		    
				    
                                    </script>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <tr>
                        <td background="../images/separator_repeat.gif" style="height: 10px">
                        </td>
                    </tr>
                <tr>
                    <td>
                        <uc1:Footer ID="Footer1" runat="server" />
                        <asp:HiddenField ID="hf_addresses" runat="server" />
                        <asp:HiddenField ID="hf_midnumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td width="300" style="height: 18px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

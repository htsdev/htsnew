﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommentsReport.aspx.cs" 
    Inherits="HTP.Reports.CommentsReport" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Namespace="eWorld.UI" TagPrefix="ew" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comment Report</title>
    <%--<link href="../Styles.css" type="text/css" rel="Stylesheet" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />


    <script src="../Scripts/boxover.js" type="text/javascript"></script>
    <script src="../Scripts/ClipBoard.js" type="text/javascript"></script>
    <script src="../Scripts/Dates.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
    
		
		//to show commment on mouse hower
		function CommentPoupup(ControlName)
		{	
		
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg()
		     
		}
		
		//if check box "All" is check then check all check box
		function CheckBox()
		{	
		     var chk_Gen=document.getElementById("ChkGenCom");
		     var chk_Rem=document.getElementById("ChkRemCallCom");
		     var chk_Comp=document.getElementById("ChkComp");
		     var chk_All=document.getElementById("ChkAll");
		     
		     if(chk_Gen.checked && chk_Rem.checked && chk_Comp.checked)
		        {
		            chk_All.checked=true;
		        }
		        else
		        {
		           chk_All.checked=false;
		        }
		        		      
		   
		     		     
		     
		}
		
		  function CheckDateValidation()
        {
            // abid ali 5387 1/15/2009 date comparision
            if (IsDatesEqualOrGrater(document.form1.cal_FromDateFilter.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, Start Date must be greater than or equal to 1/1/1900");
			    // Fahad 4354 1/16/2009 comment out
				//document.form1.cal_FromDateFilter.focus(); 
				return false;
			}
			
			if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to 1/1/1900");
				// Fahad 4354 1/16/2009 comment out
				//document.form1.cal_ToDateFilter.focus(); 
				return false;
			}
            if (IsDatesEqualOrGrater(document.form1.cal_ToDateFilter.value,'MM/dd/yyyy',document.form1.cal_FromDateFilter.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to Start Date");
			    // Fahad 4354 1/16/2009 comment out
				//document.form1.cal_ToDateFilter.focus(); 
				return false;
			}
			else
			{
			    return true;
			}
        }
		
		//check comment type check box
		function CheckBoxAll()
		{	
		     var chk_Gen=document.getElementById("ChkGenCom");
		     var chk_Rem=document.getElementById("ChkRemCallCom");
		     var chk_Comp=document.getElementById("ChkComp");
		     var chk_All=document.getElementById("ChkAll");
		     
		     if(chk_All.checked)
		     {
		        chk_Gen.checked=true;
		        chk_Rem.checked=true;
		        chk_Comp.checked=true;
		     }
		     else 
		     {
		     chk_Gen.checked=false;
		        chk_Rem.checked=false;
		        chk_Comp.checked=false;
		     }
		     
		     		     
		     
		}
	
		     function AllCheck() 
		     {
		      
		     
		     var chk_Gen=document.getElementById("ChkGenCom");
		     var chk_Rem=document.getElementById("ChkRemCallCom");
		     var chk_Comp=document.getElementById("ChkComp");
		     var chk_All=document.getElementById("ChkAll");
		     
		        if(   !chk_Gen.checked &&
		        !chk_Rem.checked &&
		        !chk_Comp.checked &&
		        !chk_All.checked)
		        {
		            alert("Please select at least one comment type");    
		            return false;
		        }
		       if(!CheckDateValidation())
		       {return false;}
		        
		     }
	
		
		
		//change cursor icon
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
		function ShowMsg()
        {   
            document.getElementById("txt_CommentsMsg").value=err;
        }
		
    </script>
</head>

<body class=" ">
    
    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:activemenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Comment Report</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <!-- MAIN CONTENT AREA STARTS -->

                    <div class="col-xs-12">
                        <section class="box ">
                            <div class="content-body">

                                <div class="row">

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Start Date</label>
                                            <div class="controls">
                                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                    ImageUrl="../images/calendar.gif" Nullable="True"
                                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                                    UpperBoundDate="9999-12-29">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                                </ew:CalendarPopup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Start Date</label>
                                            <div class="controls">
                                                <ew:CalendarPopup Visible="true" ID="cal_ToDateFilter" runat="server" AllowArbitraryText="False"
                                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                    ImageUrl="../images/calendar.gif" Nullable="True"
                                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                                    UpperBoundDate="9999-12-29">
                                                    <TextboxLabelStyle CssClass="form-control" />
                                                </ew:CalendarPopup>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Attorneys</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlAttorneys" CssClass="form-control m-bot15" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Case Type</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlCaseType" CssClass="form-control m-bot15" DataMember="CaseTypeName"
                                                    DataValueField="CaseTypeId" DataTextField="CaseTypeName" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-9 col-sm-10 col-xs-11">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Comment Type</label>
                                            <div class="controls">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="25%">
                                                            <asp:CheckBox ID="ChkGenCom" runat="server" Text="General Comments"  onclick="CheckBox()" CssClass="clsLabel"/>
                                                        </td>
                                                        <td width="25%">
                                                            <asp:CheckBox ID="ChkRemCallCom" runat="server" Text="Reminder Call Comments" onclick="CheckBox()" CssClass="clsLabel"/>
                                                        </td>
                                                        <td width="25%">
                                                            <asp:CheckBox ID="ChkComp" runat="server" Text="Complaint" onclick="CheckBox()" CssClass="clsLabel"/>
                                                        </td>
                                                        <td width="25%">
                                                            <asp:CheckBox ID="ChkAll" runat="server" Text="All" onclick="CheckBoxAll()" CssClass="clsLabel"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">
                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:CheckBox ID="chkShowAll" class="clssubhead" Text="Show All" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <div class="controls">
                                                <asp:Button ID="btnSearch" Text="Search" CssClass="btn btn-primary" OnClientClick="return AllCheck();" OnClick="btnSearch_Click" runat="server" />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </section>
                    </div>

                    <table>
                        <tr id="tr_General" runat="server">
                            <td>
                                <div class="col-lg-12">
                                    <section class="box ">
                                        <header class="panel_header">
                                            <h2 class="title pull-left">General Comments</h2>
                                            <div class="actions panel_actions pull-right">
                                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <uc3:pagingcontrol ID="PagingctrlGenComments" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </div>
                                        </header>
                                        <div class="content-body">
                                            <div class="row">

                                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlGeneral">
                                                    <ProgressTemplate>
                                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl4" runat="server" Text="Please Wait ......"
                                                            CssClass="clsLabel"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>

                                                <asp:Label ID="lblMessage_general" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>

                                                <div class="col-xs-12">

                                                    <aspnew:UpdatePanel ID="upnlGeneral" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gv_GenComments" runat="server" AutoGenerateColumns="False"
                                                                CssClass="table" AllowSorting="True" AllowPaging="True" PageSize="30"
                                                                OnSorting="gv_GenComments_Sorting" OnPageIndexChanging="gv_GenComments_PageIndexChanging" OnRowDataBound="gv_Records_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="S#">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Name" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Area of Practice</u>" SortExpression="Area" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Area" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.Area") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Attorney" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Comment Date and time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                                        HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_CDate" runat="server" CssClass="form-control" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Comments">
                                                                        <ItemTemplate>
                                                                            <div style="height:40px;width:320px; overflow:auto">
                                                                                <asp:Label ID="lbl_General" CssClass="form-control" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                            
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                        <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] 
                                                                header=[
                                                                        <table border='0' width='400px'>
                                                                            <tr>
                                                                                <td width='100%' align='right'>
                                                                                    <img alt='../Images/close_button.png' src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    ] 
                                                        
                                                                body=[
                                                                        <table border='0' width='400px'>
                                                                            <tr>
                                                                                <td>
                                                                                    <textarea id='txt_CommentsMsg' name='txt_CommentsMsg' cols='46' rows='10'></textarea>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    ] ">
                                                                    <asp:Label ID="lbl_Comments" runat="server" CssClass="form-control" onclick="copyToClipboard(document.getElementById('txt_CommentsMsg').value);"><img src='../Images/btndetail.gif' border='0' /></asp:Label>
                                                                </div>
                                                                <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FullComments") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </aspnew:UpdatePanel>

                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </td>
                        </tr>
                        <tr  id="tr_ReminderCall" runat="server" >
                            <td>
                                <div class="col-lg-12">
                                    <section class="box ">
                                        <header class="panel_header">
                                            <h2 class="title pull-left">Reminder Call Comments</h2>
                                            <div class="actions panel_actions pull-right">
                                                <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <uc3:pagingcontrol ID="PagingcontrolRemCall" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </div>
                                        </header>
                                        <div class="content-body">
                                            <div class="row">

                                                <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="upnlRemCall">
                                                    <ProgressTemplate>
                                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                            CssClass="form-label"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>

                                                <asp:Label ID="lblMessage_ReminderCall" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>

                                                <div class="col-xs-12">

                                                    <aspnew:UpdatePanel ID="upnlRemCall" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gv_RemCallComments" runat="server" AutoGenerateColumns="False"
                                                                CssClass="table" AllowSorting="True" AllowPaging="True"
                                                                PageSize="30" OnSorting="gv_RemCallComments_Sorting" OnPageIndexChanging="gv_RemCallComments_PageIndexChanging" OnRowDataBound="gv_Records_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="S#">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Name" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Area of Practice</u>" SortExpression="Area" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Area" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Area") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Attorney" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Comment Date and time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                                        HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_CDate" runat="server" CssClass="form-label" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Comments">
                                                                        <ItemTemplate>
                                                                                <div style="height:40px;width:320px; overflow:auto">
                                                                        <asp:Label ID="lbl_ReminnderCall" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                                        </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                            
                                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] 
                                                                header=[
                                                                        <table border='0' width='400px'>
                                                                            <tr>
                                                                                <td width='100%' align='right'>
                                                                                    <img alt='../Images/close_button.png' src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    ] 
                                                        
                                                                body=[
                                                                        <table border='0' width='400px'>
                                                                            <tr>
                                                                                <td>
                                                                                    <textarea id='txt_CommentsMsg' name='txt_CommentsMsg' cols='46' rows='10'></textarea>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    ] ">
                                                                    <asp:Label ID="lbl_Comments" runat="server" Width="30px" CssClass="form-label" onclick="copyToClipboard(document.getElementById('txt_CommentsMsg').value);"><img src='../Images/btndetail.gif' border='0' /></asp:Label>
                                                                </div>
                                                                <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FullComments") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </aspnew:UpdatePanel>

                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </td>
                        </tr>
                        <tr  id="tr_Complaint" runat="server">
                            <td>
                                <div class="col-lg-12">
                                    <section class="box ">
                                        <header class="panel_header">
                                            <h2 class="title pull-left">Complaint</h2>
                                            <div class="actions panel_actions pull-right">
                                                <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <uc3:pagingcontrol ID="PagingcontrolComplaints" runat="server" />
                                                    </ContentTemplate>
                                                </aspnew:UpdatePanel>
                                            </div>
                                        </header>
                                        <div class="content-body">
                                            <div class="row">

                                                <aspnew:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="upnlComp">
                                                    <ProgressTemplate>
                                                        <img src="../Images/plzwait.gif" /><asp:Label ID="lbl3" runat="server" Text="Please Wait ......"
                                                            CssClass="form-label"></asp:Label>
                                                    </ProgressTemplate>
                                                </aspnew:UpdateProgress>

                                                <asp:Label ID="lblMessage_Complaint" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>

                                                <div class="col-xs-12">

                                                    <aspnew:UpdatePanel ID="upnlComp" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gv_Complaint" runat="server" AutoGenerateColumns="False"
                                                                CssClass="table" AllowSorting="True" AllowPaging="True" PageSize="30"
                                                                OnSorting="gv_Complaint_Sorting" OnPageIndexChanging="gv_Complaint_PageIndexChanging" OnRowDataBound="gv_Records_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="S#">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                                Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Name" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Area of Practice</u>" SortExpression="Area" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Area" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Area") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_Attorney" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<u>Comment Date and time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                                        HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_CDate" runat="server" CssClass="form-label" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Comments">
                                                                        <ItemTemplate>
                                                                        <div style="height:40px;width:320px; overflow:auto">
                                                                        <asp:Label ID="lbl_Complaint" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                                        </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] 
                                                                header=[
                                                                        <table border='0' width='400px'>
                                                                            <tr>
                                                                                <td width='100%' align='right'>
                                                                                    <img alt='../Images/close_button.png' src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    ] 
                                                        
                                                                body=[
                                                                        <table border='0' width='400px'>
                                                                            <tr>
                                                                                <td>
                                                                                    <textarea id='txt_CommentsMsg' name='txt_CommentsMsg' cols='46' rows='10'></textarea>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    ] ">
                                                                    <asp:Label ID="lbl_Comments" runat="server" CssClass="form-label" onclick="copyToClipboard(document.getElementById('txt_CommentsMsg').value);"><img src='../Images/btndetail.gif' border='0' /></asp:Label>
                                                                </div>
                                                                <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FullComments") %>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <HeaderStyle CssClass="clssubhead" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </aspnew:UpdatePanel>

                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END -->

</body>
















<%--<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellpadding="0" cellspacing="0" width="820px" align="center" border="0">
            <tr>
                <td>
                    <uc1:activemenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" width="100%" >
                        <tr style="width: 100%">
                            <td style="width: 10%">
                                <span class="clssubhead">Start Date :</span>
                            </td>
                            <td style="width: 12%">
                                <ew:CalendarPopup Visible="true" ID="cal_FromDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 8%">
                                <span class="clssubhead">End Date :</span>
                            </td>
                            <td style="width: 12%">
                                <ew:CalendarPopup Visible="true" ID="cal_ToDateFilter" runat="server" AllowArbitraryText="False"
                                    CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" Text=" " ToolTip="Date"
                                    UpperBoundDate="9999-12-29" Width="65px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td style="width: 10%">
                                <asp:CheckBox ID="chkShowAll" class="clssubhead" Text="Show All" runat="server" />
                            </td>
                            <td style="width: 10%">
                                <span class="clssubhead">Attorneys :</span>
                            </td>
                            <td style="width: 35%">
                                <asp:DropDownList ID="ddlAttorneys" CssClass="clsInputCombo" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr style="width: 100%">
                            <td style="width: 10%">
                                <span class="clssubhead">Case Type :</span>
                            </td>
                            <td style="width: 10%">
                                <asp:DropDownList ID="ddlCaseType" CssClass="clsInputCombo" DataMember="CaseTypeName"
                                    DataValueField="CaseTypeId" DataTextField="CaseTypeName" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 12%">
                                <span class="clssubhead">Comment Type :</span>
                            </td>
                            <td style="width: 17%">
                                <asp:CheckBox ID="ChkGenCom" runat="server" Text="General Comments"  onclick="CheckBox()" CssClass="clsLabel"/>
                            </td>
                            <td style="width: 23%">
                                <asp:CheckBox ID="ChkRemCallCom" runat="server" Text="Reminder Call Comments" onclick="CheckBox()" CssClass="clsLabel"/>
                            </td>
                            <td style="width: 10%">
                                <asp:CheckBox ID="ChkComp" runat="server" Text="Complaint" onclick="CheckBox()" CssClass="clsLabel"/>
                            </td>
                            <td style="width: 5%">
                                <asp:CheckBox ID="ChkAll" runat="server" Text="All" onclick="CheckBoxAll()" CssClass="clsLabel"/>
                            </td>
                            <td style="width: 10%" align="right">
                                <asp:Button ID="btnSearch" Text="Search" CssClass="clsbutton" OnClientClick="return AllCheck();" OnClick="btnSearch_Click"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr id="tr_General" runat="server">
                <td>
                    <table  width="820px">
                      
                        <tr>
                            <td background="../Images/subhead_bg.gif" height="34" class="clssubhead">
                                <table style="width:100%">
                                    <tr>
                                        <td align="left">
                                            <span class="clssubhead">Genral Comments</span>
                                        </td>
                                        <td align="right">
                                            <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <uc3:pagingcontrol ID="PagingctrlGenComments" runat="server" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                    width="100%">
                                    <tr>
                                        <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                            width="780">
                                        </td>
                                    </tr>
                                    <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage_general" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlGeneral">
                                                <ProgressTemplate>
                                                    <img src="../Images/plzwait.gif" /><asp:Label ID="lbl4" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <aspnew:UpdatePanel ID="upnlGeneral" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gv_GenComments" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="30"
                                                        OnSorting="gv_GenComments_Sorting" OnPageIndexChanging="gv_GenComments_PageIndexChanging" OnRowDataBound="gv_Records_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S#">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Name" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Area of Practice</u>" SortExpression="Area" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Area" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Area") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Attorney" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Comment Date and time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                                HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments"  HeaderStyle-CssClass="clssubhead"
                                                                HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <div style="height:40px;width:320px; overflow:auto">
                                                                <asp:Label ID="lbl_General" CssClass="GridItemStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                                </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>                                                            
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] 
                                                        header=[
                                                                <table border='0' width='400px'>
                                                                    <tr>
                                                                        <td width='100%' align='right'>
                                                                            <img alt='../Images/close_button.png' src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            ] 
                                                        
                                                        body=[
                                                                <table border='0' width='400px'>
                                                                    <tr>
                                                                        <td>
                                                                            <textarea id='txt_CommentsMsg' name='txt_CommentsMsg' cols='46' rows='10'></textarea>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            ] ">
                                                            <asp:Label ID="lbl_Comments" runat="server" CssClass="Label" Width="30px" onclick="copyToClipboard(document.getElementById('txt_CommentsMsg').value);"><img src='../Images/btndetail.gif' border='0' /></asp:Label>
                                                        </div>
                                                        <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FullComments") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr  id="tr_ReminderCall" runat="server" >
                <td>
                    <table  width="820px">
                        <tr>
                            <td background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" height="34" class="clssubhead">
                                <table style="width:100%">
                                    <tr>
                                        <td align="left">
                                            <span class="clssubhead">Reminder Call Comments</span>
                                        </td>
                                        <td align="right">
                                            <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <uc3:pagingcontrol ID="PagingcontrolRemCall" runat="server" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <table id="Table1" bgcolor="white" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                            width="780">
                                        </td>
                                    </tr>
                                    <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage_ReminderCall" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="upnlRemCall">
                                                <ProgressTemplate>
                                                    <img src="../Images/plzwait.gif" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <aspnew:UpdatePanel ID="upnlRemCall" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gv_RemCallComments" runat="server" AutoGenerateColumns="False"
                                                        Width="100%" CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True"
                                                        PageSize="30" OnSorting="gv_RemCallComments_Sorting" OnPageIndexChanging="gv_RemCallComments_PageIndexChanging" OnRowDataBound="gv_Records_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S#">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Name" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Area of Practice</u>" SortExpression="Area" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Area" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Area") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Attorney" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Comment Date and time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                                HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments" HeaderStyle-CssClass="clssubhead"
                                                                HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                     <div style="height:40px;width:320px; overflow:auto">
                                                                <asp:Label ID="lbl_ReminnderCall" CssClass="GridItemStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                                </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>                                                            
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] 
                                                        header=[
                                                                <table border='0' width='400px'>
                                                                    <tr>
                                                                        <td width='100%' align='right'>
                                                                            <img alt='../Images/close_button.png' src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            ] 
                                                        
                                                        body=[
                                                                <table border='0' width='400px'>
                                                                    <tr>
                                                                        <td>
                                                                            <textarea id='txt_CommentsMsg' name='txt_CommentsMsg' cols='46' rows='10'></textarea>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            ] ">
                                                            <asp:Label ID="lbl_Comments" runat="server" Width="30px" CssClass="Label" onclick="copyToClipboard(document.getElementById('txt_CommentsMsg').value);"><img src='../Images/btndetail.gif' border='0' /></asp:Label>
                                                        </div>
                                                        <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FullComments") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr  id="tr_Complaint" runat="server">
                <td>
                    <table width="820px">
                        <tr>
                            <td background="../Images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" height="34" class="clssubhead">
                                <table style="width:100%">
                                    <tr>
                                        <td align="left">
                                            <span class="clssubhead">Complaint</span>
                                        </td>
                                        <td align="right">
                                            <aspnew:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <uc3:pagingcontrol ID="PagingcontrolComplaints" runat="server" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <table id="Table2" bgcolor="white" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                            width="780">
                                        </td>
                                    </tr>
                                    <tr>
                            <td align="center">
                                <asp:Label ID="lblMessage_Complaint" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <aspnew:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="upnlComp">
                                                <ProgressTemplate>
                                                    <img src="../Images/plzwait.gif" /><asp:Label ID="lbl3" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <aspnew:UpdatePanel ID="upnlComp" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gv_Complaint" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" PageSize="30"
                                                        OnSorting="gv_Complaint_Sorting" OnPageIndexChanging="gv_Complaint_PageIndexChanging" OnRowDataBound="gv_Records_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S#">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlnk_SNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Client Name</u>" SortExpression="FullName" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Name" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.FullName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Area of Practice</u>" SortExpression="Area" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Area" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Area") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Attorney</u>" SortExpression="AttorneyName" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_Attorney" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.AttorneyName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Comment Date and time</u>" SortExpression="RecDate" HeaderStyle-CssClass="clssubhead"
                                                                HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" CssClass="GridItemStyle" Text='<%# Eval("RecDate","{0:MM/dd/yyyy}") + " @ " + Eval("RecDate","{0:hh:mm tt}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments"  HeaderStyle-CssClass="clssubhead"
                                                                HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                <div style="height:40px;width:320px; overflow:auto">
                                                                <asp:Label ID="lbl_Complaint" CssClass="GridItemStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                                </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <div title="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] 
                                                        header=[
                                                                <table border='0' width='400px'>
                                                                    <tr>
                                                                        <td width='100%' align='right'>
                                                                            <img alt='../Images/close_button.png' src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            ] 
                                                        
                                                        body=[
                                                                <table border='0' width='400px'>
                                                                    <tr>
                                                                        <td>
                                                                            <textarea id='txt_CommentsMsg' name='txt_CommentsMsg' cols='46' rows='10'></textarea>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            ] ">
                                                            <asp:Label ID="lbl_Comments" runat="server" Width="30px" CssClass="Label" onclick="copyToClipboard(document.getElementById('txt_CommentsMsg').value);"><img src='../Images/btndetail.gif' border='0' /></asp:Label>
                                                        </div>
                                                        <asp:HiddenField ID="hf_Comments" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.FullComments") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderStyle CssClass="clssubhead" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                                    <td background="../../images/separator_repeat.gif" height="11">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:Footer ID="Footer1" runat="server" />
                                       </td>
                                </tr>
        </table>
    </div>
    </form>
</body>--%>

</html>

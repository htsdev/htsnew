<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MissingCauseNumber.aspx.cs"
    Inherits="HTP.Reports.MissingCauseNumber" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ShowSetting.ascx" TagName="ShowSetting" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Missing Cause Number Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
     <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
    <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780"
        align="center" border="0">
        <tbody>
            <tr>
                <td style="height: 14px" colspan="4">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                      <tr class="clsLeftPaddingTable">
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="left" style="height: 25px">
                                            <uc5:ShowSetting ID="ShowSetting_MissingCauseNumber" lbl_TextBefore="Number of business days in future allowed for follow up date:"
                                                runat="server" Attribute_Key="Days" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr align="right">                            
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="780" background="../images/separator_repeat.gif" colspan="7" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                    border="0">
                                    <tr>
                                       <td>
                                            <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center" colspan="2">
                                            <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>                                            
                                            <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" AllowSorting="True" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                                                        OnSorting="gv_records_Sorting" OnRowDataBound="gv_records_RowDataBound" PageSize="20"
                                                        OnRowCommand="gv_records_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                    <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cause No">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_causenumber" CssClass="Label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "casenumassignedbycourt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ticket Number">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_ticketno" CssClass="Label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "refcasenumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_lastName" CssClass="Label" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "lastname") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="First" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_vdescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firstName") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DOB" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_verified" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dob") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DL" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_verifiedTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dlnumber") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Court Date" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_verifiedcrtno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_adescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortdescription") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Loc" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_auto" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shortname") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Flags" >
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_autoTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                            
                                                            <asp:TemplateField HeaderText="<u>Follow Up Date</u>" SortExpression="sortfollowupdate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FollowUp" runat="server" Text='<%# Eval("followupdate", "{0:MM/dd/yyyy}") %>'
                                                                        CssClass="Label">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemStyle CssClass="clsLeftPaddingTable" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                     <asp:LinkButton ID="img_Add" runat="server" CommandName="btnclick" Text="&lt;img src='../Images/add.gif' border='0'/&gt;" />
                                                                    <asp:HiddenField ID="hf_Sno" runat="server" Value='<%#Eval("sno") %>' />
                                                                    <asp:HiddenField ID="hf_CauseNo" runat="server" Value='<%#Eval("casenumassignedbycourt") %>' />
                                                                    <asp:HiddenField ID="hf_ticketno" runat="server" Value='<%#Eval("refcasenumber") %>' />
                                                                    <asp:HiddenField ID="hf_Last" runat="server" Value='<%#Eval("lastname") %>' />
                                                                    <asp:HiddenField ID="hf_First" runat="server" Value='<%#Eval("firstName") %>' />
                                                                    <asp:HiddenField ID="hf_DOB" runat="server" Value='<%#Eval("dob") %>' />
                                                                    <asp:HiddenField ID="hf_DL" runat="server" Value='<%#Eval("dlnumber") %>' />
                                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%#Eval("courtdatemain") %>' />
                                                                    <asp:HiddenField ID="hf_Status" runat="server" Value='<%#Eval("shortdescription") %>' />
                                                                    <asp:HiddenField ID="hf_Loc" runat="server" Value='<%#Eval("shortname") %>' />
                                                                    <asp:HiddenField ID="hf_Flags" runat="server" Value='<%#Eval("bondflag") %>' />
                                                                    <asp:HiddenField ID="hf_FollowUpdate" runat="server" Value='<%#Eval("followupdate") %>' />
                                                                    <asp:HiddenField ID="hf_SortedFollowUpdate" runat="server" Value='<%#Eval("sortfollowupdate") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        <table style="display: none" id="tbl_plzwait1" width="800">
                                                <tbody>
                                                    <tr>
                                                        <td class="clssubhead" valign="middle" align="center" style="width: 785px">
                                                            <img src="../Images/plzwait.gif" />
                                                            Please wait While we update your Violation information.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                                <ProgressTemplate>
                                                    <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                        CssClass="clsLabel"></asp:Label>
                                                </ProgressTemplate>
                                            </aspnew:UpdateProgress>
                                            <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlFollowup" runat="server">
                                                        <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server"  Title="Missing Cause Follow Up Date" />
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                            <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                                                <ContentTemplate>
                                                    <ajaxToolkit:ModalPopupExtender ID="mpemissingcause" runat="server" BackgroundCssClass="modalBackground"
                                                        PopupControlID="pnlFollowup" TargetControlID="btn">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                </ContentTemplate>
                                            </aspnew:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="780" background="../images/separator_repeat.gif" colspan="5" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    </ContentTemplate>
        <Triggers>
        </Triggers>
    </aspnew:UpdatePanel>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
    document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

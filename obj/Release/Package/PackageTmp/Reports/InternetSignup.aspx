<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternetSignup.aspx.cs"
    Inherits="HTP.Reports.InternetSignup" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Online Internet Signup</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 22px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0" width="780" align="center" border="0">
        <tbody>
            <tr>
                <td>
                    <uc3:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td id="tr_pagging" class="clsLeftPaddingTable" align="center" runat="server" valign="middle">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr class="clsLeftPaddingTable">
                            <td class="clssubhead" style="width: 70px;" align="left">
                                Start Date :
                            </td>
                            <td style="width: 110px;" align="left">
                                <ew:CalendarPopup ID="datefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="82px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clssubhead" style="width: 60px;" align="left">
                                End Date :
                            </td>
                            <td align="left" style="width: 110px;">
                                <ew:CalendarPopup ID="dateto" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20" EnableHideDropDown="True"
                                    Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                    PadSingleDigits="True" SelectedDate="2006-02-21" ShowClearDate="True" ShowGoToToday="True"
                                    ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00" Width="85px">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td align="left" style="width: 100px">
                                <span class="clssubhead">Call back status :</span>
                            </td>
                            <td align="left" style="width: 110px">
                                <%--Sabir Khan 4749 09/11/2008--%>
                                &nbsp;<asp:DropDownList ID="ddlContactType" runat="server" CssClass="clsInputadministration"
                                    Width="100px">
                                    <asp:ListItem Value="0">--- All --</asp:ListItem>
                                    <asp:ListItem Value="1">Contacted</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">Not Contacted</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="cb_alldates" runat="server" CssClass="clssubhead" Text="All Dates"
                                    Checked="True" />
                            </td>
                            <td style="width: 68px;" align="center">
                                <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" Text="Search" OnClick="btn_search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <%--Sabir Khan 4635 09/26/2008--%>
                        <tr>
                            <td background="../images/separator_repeat.gif" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                                <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                <%--<table id="tblPageNavigation" runat="server" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="right" class="clssubhead">
                                                Page # &nbsp;
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblCurrPage" runat="server" CssClass="clssubhead" Width="25px"></asp:Label></td>
                                            <td align="right" class="clssubhead">
                                                Goto&nbsp;&nbsp;
                                            </td>
                                            <td align="right">
                                                <asp:DropDownList ID="ddlPageNo" runat="server" AutoPostBack="True" CssClass="clssubhead"
                                                    OnSelectedIndexChanged="ddlPageNo_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>--%>
                            </td>
                        </tr>
                        <%--</tr>--%>
                    </table>
                </td>
            </tr>
            <tr id="img_pagging" runat="server">
                <td background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                        OnRowDataBound="gv_records_RowDataBound" Width="100%" AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging"
                        PageSize="20" AllowSorting="True" OnSorting="gv_records_Sorting">
                        <PagerSettings PreviousPageText="&amp;lt; Previous " Mode="NextPreviousFirstLast"
                            LastPageText="Last &amp;gt;&amp;gt;" FirstPageText="&amp;lt;&amp;lt; First" NextPageText="Next &amp;gt; ">
                        </PagerSettings>
                        <Columns>
                            <asp:TemplateField HeaderText="S#">
                                <HeaderStyle Width="10%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hl_sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="lbtn_lname" runat="server" CommandArgument='<%# "../DOCSTORAGE/ScanDoc/images/" + DataBinder.Eval(Container, "DataItem.FileName") %>'
                                        Text='<%# Eval("LastName") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <HeaderStyle Width="30%" CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("firstname") %>' CssClass="clsLeftPaddingTable"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="recdate" HeaderText="Signup Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_recdate" runat="server" Text='<%# Eval("recdate","{0:d}") %>'
                                        CssClass="clsLeftPaddingTable">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="recdate" HeaderText="Signup Time">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_rectime" runat="server" Text='<%# Eval("recdate","{0:t}") %>'
                                        CssClass="clsLeftPaddingTable">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td height="11">
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>

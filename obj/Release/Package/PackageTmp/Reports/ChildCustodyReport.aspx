<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChildCustodyReport.aspx.cs"
    Inherits="HTP.Reports.ChildCustodyReport" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuTop.ascx" TagName="MenuTop" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Child Custody Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script>

function OpenPopup(childcustodyid)
{
    //window.open("ChildCustodyCaseDetail.aspx?childcustodyid="+childcustodyid,'','resizable=no,scrollbars=yes,menubars=no,toolbars=no,height=550,width=790');
    openCenteredWindow("ChildCustodyCaseDetail.aspx?childcustodyid="+childcustodyid,790,550);
    return false;
    
    
    
    
}

 function openCenteredWindow(url,width,height) 
    {
        var left = parseInt((screen.availWidth/2) - (width/2));
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + 
            ",status,left=" + left + ",top=" + top + 
            "screenX=" + left + ",screenY=" + top;
        window.open(url, "", windowFeatures + ",resizable=no,scrollbars=yes,menubars=no,toolbars=no");
    }


function ClearForm()
{
    
    document.getElementById("calDocketDateFrom").value = "";
    document.getElementById("calDocketDateTo").value = "";
    document.getElementById("calLoadDateFrom").value = "";
    document.getElementById("calLoadDateTo").value = "";
    document.getElementById("ddlSettingType").value = 0;
    document.getElementById("ddlCourts").value = 0;
    document.getElementById("ddlCaseStatus").value = 0;
     return false;
    

}

    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tr>
                <td>
                    <uc3:ActiveMenu ID="ActiveMenu1" runat="server"></uc3:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="clsleftpaddingtable" colspan="1">
                                <table align="center" cellpadding="0" cellspacing="0" style="width: 100%" class="clsLeftPaddingTable">
                                    <tr>
                                        <td align="left" class="clssubhead" style="width: 120px; height: 28px;">
                                            Courts
                                        </td>
                                        <td style="height: 28px">
                                            <asp:DropDownList ID="ddlCourts" runat="server" CssClass="clsInputCombo" Width="185px"
                                                DataTextField="CourtName" DataValueField="ChildCustodyCourtid">
                                                <asp:ListItem Selected="True" Value="0">-- All  --</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="clssubhead" style="width: 86px; height: 28px;" align="left">
                                            Docket Date
                                        </td>
                                        <td style="height: 28px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="clsLabel" style="width: 121px">
                                                        &nbsp;From
                                                        <ew:CalendarPopup ID="calDocketDateFrom" runat="server" AllowArbitraryText="False"
                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" 
                                                            Culture="(Default)" Font-Names="Tahoma"
                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                                            ShowGoToToday="True" Style="position: relative" ToolTip="Select Report Date Range"
                                                            UpperBoundDate="12/31/9999 23:59:00"  Width="70px" 
                                                            LowerBoundDate="1900-01-01">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <MonthHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Yellow" />
                                                            <OffMonthStyle ForeColor="Gray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="AntiqueWhite" />
                                                            <GoToTodayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <TodayDayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGoldenrodYellow" />
                                                            <DayHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Orange" />
                                                            <WeekendStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGray" />
                                                            <SelectedDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" BackColor="Yellow" />
                                                            <ClearDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <HolidayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                    <td align="right" class="clsLabel">
                                                        To
                                                    </td>
                                                    <td>
                                                        <ew:CalendarPopup ID="calDocketDateTo" runat="server" AllowArbitraryText="False"
                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" 
                                                            Culture="(Default)" Font-Names="Tahoma"
                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                                            ShowGoToToday="True" Style="position: relative" ToolTip="Select Report Date Range"
                                                            UpperBoundDate="12/31/9999 23:59:00"  Width="70px" 
                                                            LowerBoundDate="1900-01-01">
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <WeekdayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <MonthHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Yellow" />
                                                            <OffMonthStyle ForeColor="Gray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="AntiqueWhite" />
                                                            <GoToTodayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <TodayDayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGoldenrodYellow" />
                                                            <DayHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Orange" />
                                                            <WeekendStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGray" />
                                                            <SelectedDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" BackColor="Yellow" />
                                                            <ClearDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <HolidayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clssubhead" style="width: 120px; height: 28px;">
                                            Setting Type
                                        </td>
                                        <td style="height: 28px">
                                            <asp:DropDownList ID="ddlSettingType" runat="server" CssClass="clsInputCombo" DataValueField="ChildCustodySettingTypeid"
                                                DataTextField="SettingTypeName" Width="185px">
                                                <asp:ListItem Selected="True" Value="-1">-- Choose --</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="clssubhead" style="width: 86px; height: 28px;" align="left">
                                            Load Date
                                        </td>
                                        <td style="height: 28px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td class="clsLabel" style="width: 121px">
                                                        &nbsp;From&nbsp;<ew:CalendarPopup ID="calLoadDateFrom" runat="server" AllowArbitraryText="False"
                                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" 
                                                            Culture="(Default)" Font-Names="Tahoma"
                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                                            ShowGoToToday="True" Style="position: relative" ToolTip="Select Report Date Range"
                                                            UpperBoundDate="12/31/9999 23:59:00"  Width="70px" 
                                                            LowerBoundDate="1900-01-01">
                                                            <SelectedDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" BackColor="Yellow" />
                                                            <WeekendStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGray" />
                                                            <GoToTodayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <DayHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Orange" />
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <MonthHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Yellow" />
                                                            <WeekdayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <HolidayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <OffMonthStyle ForeColor="Gray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="AntiqueWhite" />
                                                            <ClearDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <TodayDayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGoldenrodYellow" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                    <td align="right" class="clsLabel">
                                                        To
                                                    </td>
                                                    <td>
                                                        <ew:CalendarPopup ID="calLoadDateTo" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowGoToToday="True"
                                                            Style="position: relative" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="70px">
                                                            <SelectedDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" BackColor="Yellow" />
                                                            <WeekendStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGray" />
                                                            <GoToTodayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <DayHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Orange" />
                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                            <MonthHeaderStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="Yellow" />
                                                            <WeekdayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <HolidayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <OffMonthStyle ForeColor="Gray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="AntiqueWhite" />
                                                            <ClearDateStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="White" />
                                                            <TodayDayStyle ForeColor="Black" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                BackColor="LightGoldenrodYellow" />
                                                        </ew:CalendarPopup>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clssubhead" style="width: 120px; height: 28px;">
                                            Case Status
                                        </td>
                                        <td style="height: 28px">
                                            <asp:DropDownList ID="ddlCaseStatus" runat="server" CssClass="clsInputCombo" DataTextField="CaseStatusName"
                                                DataValueField="ChildCustodyCaseStatusId" Width="185px">
                                                <asp:ListItem Selected="True" Value="-1">-- Choose --</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="clssubhead" style="width: 86px; height: 28px">
                                        </td>
                                        <td style="height: 28px" align="center">
                                            <asp:Button ID="btn_save_cdi" runat="server" CssClass="clsbutton" OnClick="btn_save_cdi_Click"
                                                Text="Search" Width="50px" />
                                            &nbsp;<asp:Button ID="btn_Reset_cdi" runat="server" CssClass="clsbutton" OnClientClick="return ClearForm();"
                                                Text="Reset" Width="50px" />&nbsp;<asp:CheckBox ID="cbshowall" runat="server" CssClass="clsLabel"
                                                    Text="<span class='clslabel'>Show All</span>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" Font-Bold="True" ForeColor="Red"></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trHeaderImage" runat="server" style="display: none">
                <td background="../Images/separator_repeat.gif" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tblresult" runat="server" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                        height: 100%" visible="false">
                        <tr>
                            <td background="../Images/subhead_bg.gif" style="height: 32px" valign="middle">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td class="clssubhead" style="width: 50%">
                                            &nbsp;Result
                                        </td>
                                        <td align="right" style="width: 50%">
                                            <uc4:PagingControl ID="PagingControl1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trImageFooter" runat="server" style="display: none">
                            <td background="../Images/separator_repeat.gif" height="11" width="100%">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="True" PageSize="30" OnPageIndexChanging="gvResult_PageIndexChanging"
                                    OnRowDataBound="gvResult_RowDataBound">
                                    <Columns>
                                        <asp:BoundField HeaderText="S#" DataField="SNO">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Style" HeaderStyle-CssClass="clssubhead">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblStyle" runat="server" Text='<%# Eval("Style") %>' OnClientClick='<%# "return OpenPopup(" +  Eval("ChildCustodyID") + ");" %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Case No" DataField="CaseNumber">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Setting Type" DataField="SettingType">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Case Status" DataField="CaseStatus">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Crt" DataField="CourtRoom">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Next Setting Date" DataField="NextSettingDate" DataFormatString="{0:MM/dd/yyyy}">
                                            <HeaderStyle CssClass="clssubhead" Width="100px" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Result" DataField="Result">
                                            <HeaderStyle CssClass="clssubhead" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Load Date" DataField="RECDATE" DataFormatString="{0:MM/dd/yyyy}">
                                            <HeaderStyle CssClass="clssubhead" Width="80px" Height="25px" />
                                            <ItemStyle CssClass="GridItemStyleBig" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerSettings Mode="NextPreviousFirstLast" LastPageText="Last &amp;gt;&amp;gt;"
                                        FirstPageText="&amp;lt;&amp;lt; First" NextPageText="Next &amp;gt;" PreviousPageText="&amp;lt; Previous" />
                                    <PagerStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

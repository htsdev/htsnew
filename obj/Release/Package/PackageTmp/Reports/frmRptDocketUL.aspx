<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.frmRptDocketUL" Codebehind="frmRptDocketUL.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Docket UL</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
					
			var dtfrom=document.getElementById("dtFrom").value;
			var dtTo=document.getElementById("dtTo").value;
			if(dtfrom > dtTo)
			{
			alert("Please Specify Valid Future Date!");
			return false;
			}
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD align="left">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<!-- 				<tr vAlign="middle">
					<td align="left"><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp;<STRONG>
							Docket Report</STRONG></td>
				</tr> -->
				<tr>
					<td background="../images/separator_repeat.gif" height="11"></td>
				</tr>
				<TR>
					<TD align="left" style="height: 33px">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 213px" width="213"><asp:dropdownlist id="ddlCourtLoc" runat="server" Width="182px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD style="WIDTH: 171px"><asp:dropdownlist id="ddlCaseStatus" runat="server" Width="139px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
                                <td style="width: 38px">
                                    From:</td>
								<TD style="WIDTH: 173px" valign="middle"><ew:calendarpopup id="dtFrom" runat="server" Width="106px" Font-Size="8pt" Font-Names="Tahoma" DisplayOffsetY="20"
										ToolTip="Date From" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)"
										AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
										EnableHideDropDown="True" Text=" ">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
                                    &nbsp;</TD>
                                <td style="width: 15px">
                                    To:</td>
								<TD style="WIDTH: 209px"><ew:calendarpopup id="dtTo" runat="server" Width="103px" Font-Size="8pt" Font-Names="Tahoma" DisplayOffsetY="20"
										ToolTip="Date To" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True"
										Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
										ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup></TD>
								<TD><asp:button id="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit" Width="87px"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td background="../images/separator_repeat.gif" height="11"></td>
				</tr>
				<TR>
					<td width="100%">
						<TABLE id="tblTitle" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="clssubhead" height="34px" background="../Images/subhead_bg.gif">&nbsp;</td>
								<td class="clssubhead" align="right" height = "34px" background="../../Images/subhead_bg.gif">
									<TABLE id="tblPageNavigation" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD class="clslabel" align="right" style="width: 48px">
                                                <strong>Page</strong> #</TD>
											<TD align="left">&nbsp;<asp:label id="lblCurrPage" runat="server" Width="25px" CssClass="clslabel" Font-Bold="True"></asp:label></TD>
											<TD class="clslabel" align="right">&nbsp;<strong>Goto</strong> &nbsp;</TD>
											<TD align="right"><asp:dropdownlist id="ddlPageNo" runat="server" CssClass="clsinputcombo" AutoPostBack="True"></asp:dropdownlist></TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</TABLE>
					</td>
				</TR>
				
				<TR>
					<TD align="left"><asp:datagrid id="dgDocket" runat="server" Width="780px" CssClass="clsleftpaddingtable" BorderColor="White"
							CellPadding="0" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" PageSize="50">
							<ItemStyle Height="18px"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="S. No">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblSerial" runat="server"></asp:Label>
										<asp:Label id=lbl_Ticketid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>' Visible="False">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="lastname" HeaderText="Last Name">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id=hlk_LName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="firstname" SortExpression="firstname" HeaderText="First Name">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="entry" SortExpression="entry" HeaderText="Entry" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="court" SortExpression="court" HeaderText="Location">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="contactdate" SortExpression="contactdate" HeaderText="Contact Date" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="datedifference">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="courtdatemain" SortExpression="courtdatemain" HeaderText="Court Date"
									DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="courttime">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="courtnumbermain">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="description" SortExpression="description" HeaderText="Status">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="firmabbreviation" HeaderText="Atty">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="bondflag" HeaderText="Bond">
									<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Prev" HorizontalAlign="Center"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="center"><asp:label id="lblMsg" runat="server" ForeColor="Red"></asp:label></TD>
				</TR>
				<tr>
					<td background="../images/separator_repeat.gif" height="11"></td>
				</tr>
				<tr>
							<TD style="WIDTH: 760px" align="left" colSpan="4">
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</tr>
			</TABLE>
		</form>
	</body>
</HTML>

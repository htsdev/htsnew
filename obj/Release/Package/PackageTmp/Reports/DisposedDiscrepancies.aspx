<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisposedDiscrepancies.aspx.cs" Inherits="lntechNew.Reports.DisposedDiscrepancies" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Disposed Discrepancies</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body class=" ">
    
    <form id="form1" runat="server">

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>
        
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Disposed Discrepancies</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                        
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-lg-12">
                        <section class="box ">
                            <div class="content-body">
                                <div class="row">
                                    <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
                                    <div class="col-xs-12">

                                        <asp:GridView ID="gv_Result" runat="server" AutoGenerateColumns="False" CssClass="table"
                                            AllowPaging="True" PageSize="50" AllowSorting="True" OnRowDataBound="gv_Result_RowDataBound" OnPageIndexChanging="gv_Result_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="NO" SortExpression="S NO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Ticketid" Visible=false runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")  %>'></asp:Label>
                                                        <asp:HyperLink ID="hf_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                                            Text=""></asp:HyperLink>
                                                        <asp:HyperLink ID="hl_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.S No") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CAUSE NO"  >
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hf_CauseNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>' Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TICKET NO" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_TicketNo" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>' Visible="False"></asp:Label>
                                                        <asp:HyperLink ID="hf_TicketNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STATUS" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Status" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.casestatus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="COURT DATE"  >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Update" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast"  />
                                            <PagerStyle HorizontalAlign="Center" />
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>






























<%--<body>
    <form id="form1" runat="server">
   
    <table  id = "MainTable" cellSpacing="0" cellPadding="0" align="center" border="0" style="width: 780px">
    <tr>
    <td style="height: 16px">
        &nbsp;
        <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
    </td>
    </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
    
        <tr>
            <td align="center">
                <asp:Label ID="lbl_Message" runat="server" CssClass="clslabel" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gv_Result" runat="server" AutoGenerateColumns="False" CssClass="clsleftpaddingtable"
                    Width="780px" AllowPaging="True" PageSize="50" AllowSorting="True" OnRowDataBound="gv_Result_RowDataBound" OnPageIndexChanging="gv_Result_PageIndexChanging">
                    <Columns>
                            <asp:TemplateField HeaderText="NO" SortExpression="S NO">
                            <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid" Visible=false runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")  %>'></asp:Label>
                                    <asp:HyperLink ID="hf_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text=""></asp:HyperLink>
                                    <asp:HyperLink ID="hl_SNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.S No") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CAUSE NO"  >
                                <HeaderStyle Width="75px" HorizontalAlign="Left"  />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hf_CauseNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>' Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TICKET NO" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>' Visible="False"></asp:Label>
                                    <asp:HyperLink ID="hf_TicketNo" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search=0" %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS" >
                                <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Status" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.casestatus") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COURT DATE"  >
                                <HeaderStyle HorizontalAlign="Left" ForeColor="Black" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Update" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.courtdate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    <PagerSettings Mode="NumericFirstLast"  />
                    <PagerStyle HorizontalAlign="Center" />
                </asp:GridView>
               
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Footer ID="Footer1" runat="server" />
            </td>
        </tr>
    </table>
    
   
    </form>
</body>--%>

</html>

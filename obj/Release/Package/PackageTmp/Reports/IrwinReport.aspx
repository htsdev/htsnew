<%@ Page Language="c#" AutoEventWireup="false" Inherits="lntechNew.Reports.IrwinReport" Codebehind="IrwinReport.aspx.cs" %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FTA Letter's</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Style.css" type="text/css" rel="stylesheet">
		<script>
		function HideRpt()
		{
			//if (document.Form1.textname.value == 1)  
				//{
				el=document.getElementById("tdrpt").style;
				el.display ='none';
			//	}
		}
		
			
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; WIDTH: 780px" cellSpacing="1" cellPadding="0"
				width="100%" align="center" border="0">
				<TR>
					<TD class="clssubhead" style="HEIGHT: 35px" background="../Images/subhead_bg.gif" colSpan="4">&nbsp;</TD>
				</TR>
				<TR>
					<TD class="clsmainhead" style="HEIGHT: 14px" colSpan="4">&nbsp;<IMG alt="" src="../Images/head_icon.gif">
						&nbsp;&nbsp;&nbsp;Irwin Letter</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 19px" width="62" height="19"><STRONG>FROM</STRONG></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 257px; HEIGHT: 19px" width="257"><asp:textbox id="txtSdate" runat="server" TextMode="SingleLine" CssClass="clsInputnamefield"></asp:textbox>&nbsp;<STRONG>(MM/DD/YYYY)</STRONG></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 59px; HEIGHT: 19px" width="59"><STRONG>TO</STRONG></TD>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 19px" width="415"><asp:textbox id="txtEndDate" runat="server" TextMode="SingleLine" CssClass="clsInputnamefield"></asp:textbox><STRONG>(MM/DD/YYYY)</STRONG></TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable"></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 257px"></TD>
					<TD class="clsLeftPaddingTable" colSpan="2"></TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 53px"><STRONG>Filter</STRONG></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 257px; HEIGHT: 53px">
						<asp:RadioButtonList id="opt_btn" runat="server" CssClass="clsInputnamefield" Width="121px" Height="48px"
							BorderStyle="None">
							<asp:ListItem Value="1">Record Load Date</asp:ListItem>
							<asp:ListItem Value="2">Violation Date</asp:ListItem>
						</asp:RadioButtonList></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 59px; HEIGHT: 53px"><asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Search"></asp:button></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 254px; HEIGHT: 53px">
						<DIV style="FLOAT: left" align="center"><asp:button id="BtnPrint" runat="server" CssClass="clsbutton" Text="Export Letters To PDF" EnableViewState="False"></asp:button></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" style="DISPLAY: none" colSpan="4">&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtSdate" ErrorMessage="Please Enter FROM Date"
							Width="177px"></asp:requiredfieldvalidator><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate" ErrorMessage="Please Enter TO Date"
							Width="218px"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" ControlToValidate="txtEndDate" ErrorMessage="Please Enter the TO Date Properly  (MM/DD/YYYY)"
							ValidationExpression="\d{1,2}\/\d{1,2}/\d{4}"></asp:regularexpressionvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator2" runat="server" ControlToValidate="txtSdate" ErrorMessage="Please Enter the FROM Date Properly  (MM/DD/YYYY)"
							ValidationExpression="\d{1,2}\/\d{1,2}/\d{4}"></asp:regularexpressionvalidator>
						<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" Width="14px" ErrorMessage="Select a fiilter option"
							ControlToValidate="opt_btn">*</asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 37px"></TD>
					<TD class="clsLeftPaddingTable" style="WIDTH: 316px; HEIGHT: 37px" colSpan="2"><asp:validationsummary id="ValidationSummary1" runat="server" Width="216px" Height="68px"></asp:validationsummary></TD>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 37px">&nbsp;</TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:datagrid id="dgrdQuote" runat="server" Width="780px" AllowSorting="True" AllowPaging="True"
							Font-Size="Small" PageSize="25" ForeColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px"
							BackColor="White" CellPadding="0" CellSpacing="1" GridLines="None" AutoGenerateColumns="False">
							<SelectedItemStyle Font-Size="Smaller" Font-Bold="True" ForeColor="White" BackColor="#9471DE"></SelectedItemStyle>
							<EditItemStyle Font-Size="Smaller"></EditItemStyle>
							<AlternatingItemStyle Font-Size="Smaller"></AlternatingItemStyle>
							<ItemStyle Font-Size="Smaller" ForeColor="Black" BackColor="#D1D9F7"></ItemStyle>
							<HeaderStyle Font-Size="Small" Font-Bold="True" ForeColor="Fuchsia" BackColor="#FFCC66"></HeaderStyle>
							<FooterStyle ForeColor="Black" BackColor="#C6C3C6"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderTemplate>
										Date
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox id=Checkbox1 Text='<%# DataBinder.Eval(Container.DataItem, "RecDAte") %>' Runat="server">
										</asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="TotalTickets" HeaderText="Irwin Letter">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="TotalPrints" HeaderText="Total No Of Prints "></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#C6C3C6" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD id="tdrpt" align="center" colSpan="4">&nbsp;</TD>
				<TR>
					<TD align="center" colSpan="4"><asp:label id="lblMessage" runat="server" Width="352px" ForeColor="Red" Height="16px"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

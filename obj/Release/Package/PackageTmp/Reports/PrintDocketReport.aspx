<%@ Page language="c#" AutoEventWireup="true" Inherits="lntechNew.Reports.PrintDocketReport" Codebehind="PrintDocketReport.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>HTML Print</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<script>
		
		</script>
		<form id="Form1" method="post" runat="server">
			&nbsp;
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="715" border="0">
				<TR>
					<TD style="HEIGHT: 9px" align="center" colSpan="3"><asp:label id="lblPrintDate" runat="server" Font-Names="Arial" Width="210px" Font-Size="7.5pt"
							Font-Bold="True">Hello</asp:label></TD>
				</TR>
				<TR>
					<TD style="width: 774px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="width: 774px">
						<p align="left"><font face="Arial" size="2"><asp:label id="LblAttrorney" runat="server" Font-Names="Arial" Width="264px" Font-Size="10pt"
									Font-Bold="True">ENATION ATTORNEYS: </asp:label></font></p>
					</TD>
					
					<TD align="right">___________________<br>
						<font face="Arial" size="1"><STRONG>Attorney</STRONG></font>
					</TD>
					</TR>
				<tr>
					<td align="center" colSpan="3" style="height: 43px">
						<P>
                            &nbsp;</P>
						<P>
				<asp:GridView id="gv_Data" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDataBound="gv_Data_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="NO">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ticketid" Visible=false runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk")  %>'></asp:Label>
                                    <asp:HyperLink ID="hf_SNo" runat="server" ForeColor="Black" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search="+DataBinder.Eval(Container, "DataItem.ACTIVEFLAG") %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.S No") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CAUSE NO">
                                <ItemTemplate>
                                    &nbsp;<asp:HyperLink ID="hf_CauseNo" ForeColor="black" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search="+DataBinder.Eval(Container, "DataItem.ACTIVEFLAG") %>' Text='<%# DataBinder.Eval(Container, "DataItem.CAUSENUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TICKET NO">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TicketNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>' Visible="False"></asp:Label>
                                    <asp:HyperLink ID="hf_TicketNo" ForeColor="black" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk")+"&search="+DataBinder.Eval(Container, "DataItem.ACTIVEFLAG") %>'
                                        Text='<%# DataBinder.Eval(Container, "DataItem.TICKETNUMBER") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="HIRE">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Hire" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.HIRE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UPDATE">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Update" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="LAST NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LASTNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FIRST NAME">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.FIRSTNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DOB">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_DOB" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.DOB","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DL">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_DL" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.DL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COURT DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.COURTDATE","{0:g}") %>'></asp:Label>
                                    <asp:Label ID="lbl_CourtNo" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.COURTNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STAT">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Stat" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.STAT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="LOC">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_LOC" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.LOC") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Flags">
                                <ItemTemplate>
                                    <asp:Label ID="lblflags" runat="server" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.bflag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    <HeaderStyle Font-Size="8pt" />
                    </asp:GridView>
				&nbsp;</P>
					</td>
				</tr>
				<tr>
				<td style="width: 774px">
                    &nbsp;</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenServiceTicketReport.aspx.cs"
    Inherits="HTP.Reports.OpenServiceTicketReport" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Service Ticket Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
     <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script type="text/jscript"> 
       
        
       //Added by Zeeshan Ahmed       
       function unCheck(id)
       {
           var chkbox = document.getElementById(id)
           if ( chkbox != null)
           chkbox.checked = false;
       }
  
        // tahir 6082 06/26/2009 added all past due option.
        function ToggleSelection(id)
        {
         var AllTickets = document.getElementById ("chkShowAllfollowupRecords");
         var AllPending = document.getElementById ("chkAllPastDue");
         
         if (id == "chkShowAllfollowupRecords")
         {
            AllPending.checked = false;
         }
         else if (id == "chkAllPastDue")
         {
            AllTickets.checked = false;
         }
         
         return false;
         
        }
       
       function RecordsType()
       {
//       var all=document.getElementById("chkShowallRecords");
//       var followup=document.getElementById("chk_followuprecords");
//       if(all.checked==true&&followup.checked==true)
//       {
//          a"Sorry,Only one option can be selected at a time ");
//          all.checked=false;
//          followup.checked=false;
//          return false
//       }
       return true;
       }
       
         function PopUp(ticketid, fid)
		{
		    //Fahad 7791 08/13/2010 Fixed Popup issue which is not updated every time
		    var getdate = new Date();
		    //Fahad 7791 08/13/2010 Concatenated the time slot with Query String
		    var path="GeneralCommentsPopup.aspx?ticketid="+ticketid+"&Fid="+fid+"&OSTR=1&Report=1&time="+getdate.getTime();
		    window.open(path,'',"fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=no,width=650,height=480,scrollbars=yes");
		    return false;
		}
		
		function ShowAttorneyList(ticketid,fid)
		{
		
		var div = document.getElementById("pnl_attorney");
		var hf = document.getElementById("hf_ticketid");
		var hffid = document.getElementById("hf_fid");
		
		 var IpopTop = (document.body.clientHeight - div.offsetHeight) / 2;
         var IpopLeft = (document.body.clientWidth - div.offsetWidth) / 2;
         
         div.style.left=IpopLeft + document.body.scrollLeft;
         div.style.top=IpopTop + document.body.scrollTop;
        				
		hf.value = ticketid;
		hffid.value = fid;
		div.style.display = "block";
		document.getElementById("ddl_attorney").value = "4028";
		return false;
				
		}
		
		function closepopup()
		{
		    document.getElementById("pnl_attorney").style.display = "none";
		    return false;		
		}
		 
		function Delete(username,comp)
		{		    
		    
		    //Added By Zeeshan Ahmed
		    if ( comp != "100%")
		    {
		        //a"To close this service ticket please complete it to 100%.");
		        $("#txtErrorMessage").text("To close this service ticket please complete it to 100%.");
		        $("#errorAlert").modal();
		        return false;
		    }		    
		  
		    if(ConfirmYesNo("Close service ticket for " + username ))
		    {
		        return true;   
		    }    
		    else
		    {
		        return false;
		        
		    }
		}
		
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 92px;
        }
        .style2
        {
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server"/>
     <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" >
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>
                

                 </div>

                 <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl_Message" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>

                 </div>
                 </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">  Service Ticket Report</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div> <%--(moiz check it--%>

               <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">  Service Ticket Report</h2>
                     <div class="actions panel_actions pull-right">

                         
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Start Date :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="caldatefrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                 <%--    <ew:CalendarPopup ID="caldatefrom" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                Width="90px">
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                            </ew:CalendarPopup>--%>


                                    </div>
                                                                </div>
                         </div>

                     <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">End Date :</label>
                                <span class="desc"></span>
                                <div class="controls">

                                    <picker:datepicker id="caldateto" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                   <%-- <ew:CalendarPopup ID="caldateto" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                SelectedDate="2005-09-01" ShowGoToToday="True" ToolTip="Select Report Date Range"
                                                UpperBoundDate="12/31/9999 23:59:00" Width="90px">
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                            </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>


                     <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:CheckBox ID="chkShowAllfollowupRecords" runat="server" Text="All follow up dates" CssClass="checkbox-custom" onclick="ToggleSelection(this.id);" />
                                    </div>
                                                                </div>
                         </div>



                     <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label"> </label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:CheckBox ID="chkAllPastDue" runat="server" Text="All Past Due" CssClass="checkbox-custom" Checked="true" onclick="ToggleSelection(this.id);"/>
                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Division :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlDivision" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>



                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:RadioButtonList ID="rbl_tickettype" runat="server" RepeatDirection="Horizontal"
                                              CssClass="checkbox-custom">
                                              <asp:ListItem  Selected="True" Value="1">Open Ticket</asp:ListItem>
                                              <asp:ListItem Value="0">Close Ticket</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                                                </div>
                         </div>
                   

                    <div class="clearfix"></div>

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:Button ID="btn_search" runat="server" CssClass="btn btn-primary" OnClick="btn_search_Click"
                                                Text="Search" OnClientClick="return RecordsType()" />
                                    </div>
                                                                </div>
                         </div>


                    </div>
                     </div>


            </section>
            <div class="clearfix"></div>

            
               <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">  LIST</h2>
                     <div class="actions panel_actions pull-right">

                         <uc2:PagingControl ID="PagingControl1" runat="server" />
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                           <%--   <label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                     
                                     <asp:GridView ID="gv" runat="server" Width="100%" AutoGenerateColumns="False" BackColor="#EFF4FB"
                                                BorderColor="White" CssClass="table table-small-font table-bordered table-striped" OnRowDataBound="gv_RowDataBound"
                                                AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gv_PageIndexChanging"
                                                OnSorting="gv_Sorting" PageSize="20" OnRowCommand="gv_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S.No">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="HPSNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>' CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COM">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="HpLastUpdate" runat="server" ImageUrl="/Images/Add.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Division">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDivision" runat="server" CssClass="form-label" Text='<%#Eval("CaseTypeName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category" SortExpression="Category">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategory" runat="server" CssClass="form-label" Text='<%# Bind("category") %>'></asp:Label>
                                                            <asp:Label ID="lblCategoryOption" runat="server" CssClass="form-label" Text="<%# Bind('ContinuanceOption') %>"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Wrap="False" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HFTicketid" runat="server" Value='<%# Bind("ticketid_pk") %>' />
                                                            <!-- khalid  2727 date : 30/1/08 to remove unused field 
                                                                   <asp:HiddenField ID="HFFlagid" runat="server" Value='<%# Bind("flagid") %>' />
                                                                 -->
                                                            <asp:HiddenField ID="HFFid" runat="server" Value='<%# Bind("fid") %>' />
                                                            <asp:HyperLink ID="HPClientName" NavigateUrl="#" runat="server" Text='<%# Bind("clientname") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt Date">
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtdate" runat="server" CssClass="form-label" Text='<%# Bind("courtdate","{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Crt Time">
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crttime" runat="server" CssClass="form-label" Text='<%# Bind("courttime") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtstatus" runat="server" CssClass="form-label" Text='<%# Bind("shortdescription") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Loc">
                                                        <HeaderStyle CssClass="clssubhead" Width="50px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_crtloc" runat="server" CssClass="form-label" Text='<%# Bind("settingInformation") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Open D" SortExpression="sortopendate">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOpenDate" CssClass="form-label" runat="server" Text='<%# Bind("opendate","{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Open By" SortExpression="rep">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblopenby" CssClass="form-label" runat="server" Text='<%# Bind("rep") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Update D" SortExpression="sortrecsortdate">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbllastupdate" CssClass="form-label" runat="server" Text='<%# Eval("recsortdate","{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Closed D" SortExpression="closeddate">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Center" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcloseddate" CssClass="form-label" runat="server" Text='<%# Bind("closeddate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Priority" SortExpression="Priority">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPriority" CssClass="form-label" runat="server" Text='<%# Bind("priority") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ASSN">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_assignto" runat="server" Text='<%# Eval("AssignedTo")%>' CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Follow Up Date">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_followupdate" runat="server" Text='<%# Eval("followupdate")%>' CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COMP.%">
                                                        <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_percentage" runat="server" Text='<%# Eval("completion") + "%" %>'
                                                                CssClass="form-label"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtn_close" runat="server" CommandName="close" ImageUrl="~/Images/cross.gif"
                                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Fid").ToString() + "-" +  DataBinder.Eval(Container.DataItem, "ticketid_pk").ToString() + "-" + DataBinder.Eval(Container.DataItem, "CaseTypeId").ToString() %>'
                                                                ToolTip="Close Ticket" />
                                                            <asp:HiddenField ID="HFFid2" runat="server" Value='<%# Bind("Fid") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            </asp:GridView>
                                        </div>

                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                   </section>







            <table>
             <tr>
                                            <td style="display:none">
                                            <asp:Button ID="btnDum" runat="server"  Text="Button" onclick="btnDum_Click" />
                                            </td>
                                            </tr>
                                </table>









            </section>
                     </section>
         </div>

         






    <script language="vbscript">
    
        Function ConfirmYesNo(msg)
                                         
                       
            if  MsgBox(msg,36,"Microsoft Internet Explorer                      ") = 6 then
            ConfirmYesNo =  true
            else
            ConfirmYesNo = false
            end if 
        
        End function             


    </script>

    </form>
         <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script language="JavaScript" type="text/javascript" src="../Scripts/wz_tooltip.js"></script>

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->




</body>
</html>

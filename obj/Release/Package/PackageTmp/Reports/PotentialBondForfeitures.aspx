﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PotentialBondForfeitures.aspx.cs"
    Inherits="HTP.Reports.PotentialBondForfeitures" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Potential Bond Forfeiture</title>

    <script type="text/javascript">
    function pnl_SMSEmail(radio)
    {
 
        if (radio.id == "rdb_Email")
        {
            document.getElementById("div_Email").style.display = '';
            document.getElementById("div_SMS").style.display = 'none';
            document.getElementById("rdb_SMS").checked = false;
        }
        else if (radio.id == "rdb_SMS")
        {
            document.getElementById("div_Email").style.display = 'none';
            document.getElementById("div_SMS").style.display = '';  
            document.getElementById("rdb_Email").checked = false;          
        }
    }
    function disable_Button(btn)
    {     
        var validationGroup = btn.id=="btn_Email"?"Email":"SMS";
        if(Page_ClientValidate(validationGroup))
        {
            var button = document.getElementById(btn.id);
            btn.id=="btn_Email"?tr_Email.style.display='none':tr_Sms.style.display='none';
            tr_Img.style.display='';
        }
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
                    <tr>
                        <td>
                            <uc12:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr class="clsLeftPaddingTable">
                        <td align="right">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                    <td width="685px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkShowAll" runat="server" Text="Show All " CssClass="clssubhead"
                                            OnCheckedChanged="chkShowAll_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/separator_repeat.gif" height="11">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="clssubhead">
                                        Potential Bond Forfeiture
                                    </td>
                                    <td align="right" class="clssubhead">
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                        width="780">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 100%">
                                        <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" valign="top">
                                        <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                            <ProgressTemplate>
                                                <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                    CssClass="clsLabel"></asp:Label>
                                            </ProgressTemplate>
                                        </aspnew:UpdateProgress>
                                        <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="clsLeftPaddingTable" AllowPaging="True" PageSize="30" OnRowCommand="gv_Records_RowCommand"
                                            OnPageIndexChanging="gv_Records_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="S#">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="HlnkSNo" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ticket Number">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.RefCaseNumber")%>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Left" />
                                                    <ControlStyle Width="10%" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cause Number">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container, "DataItem.casenumassignedbycourt")%>
                                                    </ItemTemplate>
                                                    <ControlStyle Width="10%" />
                                                    <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblLastName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFirstName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Court" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblTiketNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Crt Date & Time" HeaderStyle-CssClass="clssubhead"
                                                    HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblCausenumber" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtDateMain") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="115px" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Violations" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblLoc" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ViolationDescription") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact Info">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lblcontact1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact1")%>'></asp:Label><br />
                                                        <asp:Label ID="Lblcontact2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact2")%>'></asp:Label><br />
                                                        <asp:Label ID="Lblcontact3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact3")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="GridItemStyle" Width="87px" HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgAdd" runat="server" CommandName="btnclick" ImageUrl="~/Images/Email.jpg"
                                                            CommandArgument='<%#Eval("ticketid_pk")+";"+Eval("TicketsViolationID") %>' />
                                                        <asp:HiddenField ID="HfFname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                        <asp:HiddenField ID="HfLname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                        <asp:HiddenField ID="HfTicketno" runat="server" Value='<%#Eval("ticketid_pk") %>' />
                                                        <asp:HiddenField ID="HfTicketnumber" runat="server" Value='<%#Eval("RefCaseNumber") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlFollowup" runat="server">
                                            <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse;
                                                width: 342px">
                                                <tr>
                                                    <td background="../Images/subhead_bg.gif" valign="bottom">
                                                        <table border="0" width="100%">
                                                            <tr>
                                                                <td class="clssubhead" style="height: 26px">
                                                                    Client Case Information
                                                                </td>
                                                                <td align="right">
                                                                    &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="clsLeftPaddingTable">
                                                        <table>
                                                            <tr>
                                                                <td class="clsLabel">
                                                                    <asp:RadioButton ID="rdb_Email" runat="server" Text="Email" onclick="pnl_SMSEmail(this)" />
                                                                </td>
                                                                <td class="clsLabel">
                                                                    <asp:RadioButton ID="rdb_SMS" runat="server" Text="SMS" onclick="pnl_SMSEmail(this)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div id="div_Email" style="display: none">
                                                            <table>
                                                                <tr>
                                                                    <td class="clsLabel">
                                                                        To:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAttorneyEmail" runat="server" CssClass="clsInputadministration"
                                                                            Width="250px" MaxLength="200"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter Email Address"
                                                                            Display="Dynamic" Text="*" ControlToValidate="txtAttorneyEmail" ValidationGroup="Email"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Email"
                                                                            Text="*" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                            ControlToValidate="txtAttorneyEmail" ErrorMessage="Please enter valid Email Address"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="clsLabel">
                                                                        Subject:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txt_Subject" runat="server" CssClass="clsInputadministration" Width="250px" MaxLength="500"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_Subject"
                                                                            Text="*" ValidationGroup="Email" ErrorMessage="Please enter subject"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="clsLabel" valign="top">
                                                                        Notes:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txt_Notes" runat="server" CssClass="clsInputadministration" TextMode="MultiLine"
                                                                            Width="250px" MaxLength="1000"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_Notes"
                                                                            Text="*" ValidationGroup="Email" ErrorMessage="Please enter email notes"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Email" runat="server"
                                                                            DisplayMode="List" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="tr_Email">
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btn_Email" runat="server" CssClass="clsbutton" Text="Send Email"
                                                                            Width="100px" ValidationGroup="Email" OnClientClick="disable_Button(this);" OnClick="btnEmail_Click" />&nbsp;
                                                                        <asp:Button ID="Button2" runat="server" CssClass="clsbutton" Text="Cancel" Width="100px"
                                                                            OnClientClick="$find('mpeTrafficwaiting').hide();return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="div_SMS" style="display: none">
                                                            <table>
                                                                <tr>
                                                                    <td class="clsLabel" style="width: 50px">
                                                                        Cell #:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txt_CellNumber" runat="server" CssClass="clsInputadministration"
                                                                            Width="250px" MaxLength="20"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                                            ValidationGroup="SMS" Text="*" ControlToValidate="txt_CellNumber" ErrorMessage="Please enter Cell number"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                                                            ErrorMessage="Please enter valid Cell Number" Text="*" ControlToValidate="txt_CellNumber"
                                                                            ValidationExpression="((\(\d{3}\) ?)|(\d{3}))?\d{3}\d{4}" ValidationGroup="SMS"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="clsLabel" valign="top">
                                                                        Notes:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txt_SmsNotes" runat="server" CssClass="clsInputadministration" TextMode="MultiLine"
                                                                            Width="250px" MaxLength="1000"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_SmsNotes"
                                                                            ValidationGroup="SMS" Text="*" ErrorMessage="Please enter SMS notes"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="SMS" runat="server"
                                                                            DisplayMode="List" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="tr_Sms">
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btn_Sms" runat="server" CssClass="clsbutton" Text="Send SMS" Width="100px"
                                                                            OnClientClick="disable_Button(this);" ValidationGroup="SMS" OnClick="btnSmS_Click" />&nbsp;
                                                                        <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" Text="Cancel" Width="100px"
                                                                            OnClientClick="$find('mpeTrafficwaiting').hide();return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="tr_Img" style="display: none" align="center">
                                                    <td class="clsLeftPaddingTable">
                                                        <img src="../images/plzwait.gif" alt="" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlFollowup" TargetControlID="btn" CancelControlID="lbtn_close">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="btn" runat="server" Style="display: none;" />
                                        <asp:HiddenField ID="hf_Email_TicketId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../../images/separator_repeat.gif" colspan="5" style="height: 11px;
                                        width: 780">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display: none;">
                                    </td>
                                    <td>
                                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

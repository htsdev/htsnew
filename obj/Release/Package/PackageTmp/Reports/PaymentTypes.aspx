﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentTypes.aspx.cs" Inherits="HTP.Reports.PaymentTypes" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Types</title>

        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
    
        function ValidateAddCategory() {
     
            var txt = document.getElementById('txtPaymentType');
            var shortdesc = document.getElementById('txtShortDesc');
        
        
            if(txt.value == '')
            {
                alert('Please enter some text for the payment description');
                txt.focus();
                return(false);
            }
            if (shortdesc == '')
            {
                alert('Please enter some text for short payment description');
                shortdesc.focus();
                return(false);
            }
        
        }

    </script>
</head>

<body class=" ">
    
    <form id="form1" runat="server">

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
            <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                <div class='col-xs-12'>
                    <div class="page-title">

                        <div class="pull-left">
                            <!-- PAGE HEADING TAG - START -->
                            <h1 class="title">Payment Types</h1>
                            <!-- PAGE HEADING TAG - END -->  
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- MAIN CONTENT AREA STARTS -->
    
                <div class="col-xs-12">
                    <section class="box ">
                        
                        <div class="content-body">

                            <div class="row">

                                <div class="col-md-6 col-sm-7 col-xs-8">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Payment Type</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtPaymentType" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6 col-sm-7 col-xs-8">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Short Description</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtShortDesc" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12">

                                    <div class="form-group">
                                        <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                    </div>

                                </div>

                            </div>
                                
                            <div class="row">

                                <div class="col-md-6 col-sm-7 col-xs-8">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">Is Active?</label>
                                        <div class="controls">
                                            <asp:CheckBox ID="ChkIsActive" runat="server" CssClass="pull-right" />
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6 col-sm-7 col-xs-8">

                                    <div class="form-group">
                                        <label class="form-label" for="field-1">&nbsp;</label>
                                        <div class="controls">
                                            <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnAdd_Click" OnClientClick="return ValidateAddCategory()" Text="Add" />
                                            &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnCancel_Click" Text="Cancel" Visible="False" />
                                            &nbsp;<asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnUpdate_Click" OnClientClick="return ValidateAddCategory()" Text="Update" Visible="False" />
                                            
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <asp:HiddenField ID="HF_PaymentTypeId" runat="server" />

                        </div>

                    </section>
                </div>

                <div class="col-lg-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Payment Types</h2>
                            <div class="actions panel_actions pull-right">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                        <ProgressTemplate>
                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                CssClass="clsLabel"></asp:Label>
                                        </ProgressTemplate>
                                    </aspnew:UpdateProgress>
                                    <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                    </aspnew:UpdatePanel>
                                    <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="table" CellPadding="3" OnRowCommand="gv_Records_RowCommand" OnRowDeleting="gv_Records_RowDeleting" DataKeyNames="Paymenttype_PK" onselectedindexchanged="gv_Records_SelectedIndexChanged">
                                        <Columns>
                                        
                                            <asp:TemplateField HeaderText="Description">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtn_Desc" runat="server" Text='<%# Bind("Description") %>' CommandName="Select"
                                                
                                                    CommandArgument='<%# Bind("Paymenttype_PK") %>'
                                                    ></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Short Description">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblShortDesc" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Is Active">
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="ChkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' 
                                                        Enabled="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/remove2.gif"
                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.Paymenttype_PK") %>'
                                                        CommandName="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                    <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>













<%--<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />
        <table cellspacing="0" cellpadding="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 819px">
                    <table style="width: 622px">
                        <tr>
                            <td style="width: 234px;">
                                <asp:Label ID="Lbl_PaymentType" runat="server" CssClass="clsSubhead" Text="Payment Type:"
                                    Width="123px"></asp:Label>
                            </td>
                            <td style="width: 239px">
                                &nbsp;<asp:TextBox ID="txtPaymentType" runat="server" CssClass="clsinputadministration"
                                    Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 234px;">
                                <asp:Label ID="Lbl_ShortDesc" runat="server" CssClass="clsSubhead" Text="Short Description:"
                                    Width="123px"></asp:Label>
                            </td>
                            <td style="width: 239px">
                                &nbsp;<asp:TextBox ID="txtShortDesc" runat="server" CssClass="clsinputadministration" Width="200px"></asp:TextBox>
                            </td>
                            <td style="width: 266px" align="left">
                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                    OnClientClick="return ValidateAddCategory()" Text="Add" Width="60px" />
                            &nbsp;<asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" OnClick="btnUpdate_Click"
                                    OnClientClick="return ValidateAddCategory()" Text="Update" Width="60px" 
                                    Visible="False" />
                            &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" 
                                    OnClick="btnCancel_Click" Text="Cancel" Width="60px" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 234px;">
                                <asp:Label ID="LblIsActive" runat="server" CssClass="clsSubhead" Text="Is Active"
                                    Width="123px"></asp:Label>
                            </td>
                            <td style="width: 239px">
                                <asp:CheckBox ID="ChkIsActive" runat="server" />
                            </td>
                            <td style="width: 266px" align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 234px;">
                                &nbsp;
                            </td>
                            <td colspan="2">
                                &nbsp;
                                <asp:HiddenField ID="HF_PaymentTypeId" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Payment types
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td align="center" style="width: 100%">
                                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 100%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                    <ProgressTemplate>
                                        <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                            CssClass="clsLabel"></asp:Label>
                                    </ProgressTemplate>
                                </aspnew:UpdateProgress>
                                <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                                </aspnew:UpdatePanel>
                                <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                    CellPadding="3" Height="141px" Width="404px" OnRowCommand="gv_Records_RowCommand"
                                    OnRowDeleting="gv_Records_RowDeleting" DataKeyNames="Paymenttype_PK" 
                                    onselectedindexchanged="gv_Records_SelectedIndexChanged">
                                    <Columns>
                                        
                                        <asp:TemplateField HeaderText="Description">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtn_Desc" runat="server" Text='<%# Bind("Description") %>' CommandName="Select"
                                                
                                                CommandArgument='<%# Bind("Paymenttype_PK") %>'
                                                ></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Description">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LblShortDesc" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Is Active">
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="ChkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ChkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' 
                                                    Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/remove2.gif"
                                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.Paymenttype_PK") %>'
                                                    CommandName="Delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="2" style="height: 11px;
                                width: 780">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="label" ForeColor="Black"></asp:TextBox>
                            </td>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>--%>
</html>

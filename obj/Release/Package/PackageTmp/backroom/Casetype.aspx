﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Casetype.aspx.cs" Inherits="HTP.backroom.Casetype" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Type</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager runat="server">
        </aspnew:ScriptManager>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="800" align="center"
            border="0">
            <tbody>
                <tr>
                    <td colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td style="height: 14px" width="100%" background="../Images/separator_repeat.gif"
                        height="14">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                        <table style="width: 100%; height: 22px;">
                            <tr>
                                <td>
                                    <span class="clssubhead">Case Type</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <aspnew:UpdatePanel ID="update_grid" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="800px"
                                    AllowPaging="True" CellPadding="3" PageSize="30" CssClass="clsleftpaddingtable"
                                    OnRowCommand="gv_records_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hf_casetyeid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CaseTypeId") %>' />
                                                <asp:LinkButton ID="lnkbtn_name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseTypeName") %>'
                                                    CommandName="click" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.CaseTypeId") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Username" HeaderText="Assigned To">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceTicketEmailAlert" HeaderText="Open Email">
                                            <ItemStyle CssClass="GridItemStyle" HorizontalAlign="Center" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceTicketEmailClose" HeaderText="Close Email">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceTicketAngryEmailOpen" HeaderText="Angry Open Email">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceTicketAngryEmailClose" HeaderText="Angry Close Email">
                                            <ItemStyle CssClass="GridItemStyle" />
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceTicketUpdateEmail" HeaderText="Update Email">
                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="GridItemStyle" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                </asp:GridView>
                            </ContentTemplate>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <AjaxToolkit:ModalPopupExtender ID="modalpopup1" runat="server" PopupControlID="pnl_casetype"
                                    CancelControlID="lbtnclose" TargetControlID="Button2" BackgroundCssClass="modalBackground">
                                </AjaxToolkit:ModalPopupExtender>
                                <asp:Panel ID="pnl_casetype" runat="server" BackColor="white">
                                    <table id="Table1" bgcolor="white" border="1" style="border-top: black thin solid;
                                        border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid"
                                        cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="Left" style="height: 36px"
                                                    colspan="2">
                                                    <table id="tablepopup" cellpadding="0" cellspacing="0" style="width: 351px">
                                                        <tr>
                                                            <td class="clssubhead" style="height: 17px">
                                                                &nbsp;&nbsp;<strong>Update Case Type</strong>
                                                            </td>
                                                            <td align="right" style="height: 17px; width: 33px;">
                                                                &nbsp;
                                                                <asp:LinkButton ID="lbtnclose" runat="server">X</asp:LinkButton>&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label1" runat="server" class="clssubhead" Text="Name"></asp:Label>&nbsp;
                                                </td>
                                                <td align="left" style="width: 215px; height: 24px; height: 25px;">
                                                    &nbsp;
                                                    <asp:TextBox ID="txt_name" runat="server" ReadOnly="true" CssClass="clsInputadministration"
                                                        Width="180px"></asp:TextBox>
                                                    &nbsp;&nbsp;&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label2" runat="server" class="clssubhead" Text="Assigned to"></asp:Label>&nbsp;
                                                </td>
                                                <td style="width: 215px; height: 25px;" align="left">
                                                    &nbsp;&nbsp;<asp:DropDownList ID="dd_Ename" runat="server" Width="186px" CssClass="clsInputCombo"
                                                        DataTextField="UserName" DataValueField="Employeeid">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label3" class="clssubhead" runat="server" Text="Open Email"></asp:Label>&nbsp;
                                                </td>
                                                <td style="width: 215px; height: 25px;" align="left">
                                                    &nbsp;
                                                    <asp:TextBox ID="txt_openmail" runat="server" CssClass="clsInputadministration" Width="180px"
                                                        ValidationGroup="RFV"></asp:TextBox>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label4" class="clssubhead" runat="server" Text="Close Email"></asp:Label>&nbsp;
                                                </td>
                                                <td style="width: 215px; height: 25px;" align="left">
                                                    &nbsp;&nbsp;<asp:TextBox ID="txt_closemail" runat="server" CssClass="clsInputadministration"
                                                        Width="180px"></asp:TextBox>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label5" class="clssubhead" runat="server" Text="Angry Open Email"></asp:Label>&nbsp;
                                                </td>
                                                <td style="width: 215px; height: 25px;" align="left">
                                                    &nbsp;
                                                    <asp:TextBox ID="txt_angryopenemail" runat="server" CssClass="clsInputadministration"
                                                        Width="180px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label6" class="clssubhead" runat="server" Text="Angry Close Email"></asp:Label>&nbsp;
                                                </td>
                                                <td style="width: 215px; height: 25px;" align="left">
                                                    &nbsp;&nbsp;<asp:TextBox ID="txt_angrycloseemail" runat="server" CssClass="clsInputadministration"
                                                        Width="180px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    <asp:Label ID="Label7" class="clssubhead" runat="server" Text="Update Email"></asp:Label>&nbsp;
                                                </td>
                                                <td style="width: 215px; height: 25px;" align="left">
                                                    &nbsp;&nbsp;<asp:TextBox ID="txt_UpdateEmail" runat="server" CssClass="clsInputadministration"
                                                        Width="180px"></asp:TextBox>
                                                </td>
                                            </tr
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 137px; height: 25px;">
                                                    &nbsp;
                                                </td>
                                                <td align="left" style="width: 215px; height: 25px;">
                                                    &nbsp;
                                                    <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" 
                                                        OnClick="btn_update_Click" OnClientClick="return checkvalues();" Text="Update" 
                                                        ValidationGroup="RFV" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="Button2" runat="server" CssClass="clsbutton" 
                                                        Style="display: none" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </aspnew:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="height: 14px" width="100%" background="../Images/separator_repeat.gif"
                        height="14">
                    </td>
                </tr>
                <tr>
                    <td style="width: 760px" align="left" colspan="4">
                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>

    <script type="text/javascript" language="javascript">
    function checkvalues()
    {
        var openmail=document.getElementById("txt_openmail").value; 
        var closemail=document.getElementById("txt_closemail").value;
        
        if (openmail!='')
        {
            if( isEmail(openmail)== false)
		    {
			    alert ("Please Enter Open Email Address in Correct Format.");
			    document.getElementById("txt_openmail").focus(); 
			    return false;			   
		    }
		}
		else
		   { alert ("Please Enter Open Email");
            return false;}
		
		
		if (closemail!='')
        {
		    if( isEmail(closemail)== false)
		    {
			    alert ("Please Enter Close Email Address in Correct Format.");
			    document.getElementById("txt_closemail").focus();
			    return false;			   
		    }
		}
		else
		    { alert ("Please Enter Close Email");
                return false; }
		
		var angrymail=document.getElementById("txt_angryopenemail").value;
		if( angrymail!='' && isEmail(angrymail)== false)
		{
			alert ("Please Enter Angry Open Email Address in Correct Format.");
			 document.getElementById("txt_angryopenemail").focus();
			return false;			   
		}
		
		var angryclosemail=document.getElementById("txt_angrycloseemail").value;
		if( angryclosemail!='' && isEmail(angryclosemail)== false)
		{
			alert ("Please Enter Angry Close Email Address in Correct Format.");
			document.getElementById("txt_angrycloseemail").focus();
			return false;			   
		}
                
    }
    </script>

</body>
</html>

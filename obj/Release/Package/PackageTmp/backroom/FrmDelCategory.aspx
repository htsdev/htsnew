<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.FrmDelCategory" Codebehind="FrmDelCategory.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Add New Category</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
		
	   function closewin()
	    {
			opener.location.reload();
			self.close();	   
	    }	    
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; LEFT: 64px; WIDTH: 357px; POSITION: absolute; TOP: 16px; HEIGHT: 128px"
				cellSpacing="0" cellPadding="0" width="357" align="center" border="0">
				<TR>
					<TD class="clsLeftPaddingTable" style="HEIGHT: 6px" vAlign="top"><asp:label id="lbl_addcategory" BackColor="#EEC75E" CssClass="clssubhead" Runat="server">Delete Category</asp:label></TD>
				</TR>
				<TR>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="tblsub" height="20" cellSpacing="1" cellPadding="0" width="350" align="center"
							border="0">
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 36px" vAlign="top">
									<P><STRONG>Category Name</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" vAlign="top"><asp:label id="txt_CategoryName" runat="server">Label</asp:label></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 36px" vAlign="top">
									<P><STRONG>Category Description</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 18px" vAlign="top"><asp:label id="txt_CategoryDescription" runat="server">Label</asp:label></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 13px" vAlign="top" align="left" colSpan="2"><asp:label id="Label1" runat="server" Font-Size="XX-Small" Font-Names="Verdana" ForeColor="Blue"
										Font-Bold="True" Width="272px">Are you sure you want to delete category.</asp:label></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top" align="right"></TD>
							</TR>
						</TABLE>
					</TD>
				<TR>
					<TD background="../../images/separator_repeat.gif"  height="11"></TD>
				</TR>
				<TR>
					<TD colSpan="2"><asp:label id="lblMessage" runat="server" Font-Size="XX-Small" Font-Names="Verdana" ForeColor="Red"
							Font-Bold="True" Width="272px"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

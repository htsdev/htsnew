<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.FrmAddNewCategory" Codebehind="FrmAddNewCategory.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >



<HTML>
	<HEAD>
		<title>Add New Category</title>
		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta cont  nt="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

		<script>
		
		function MaxLength()
		{
		if(document.getElementById("txt_CategoryDescription").value.length >100)
		{
		alert("The length of character is not in the specified Range");
		document.getElementById("txt_CategoryDescription").focus();
		}
		}
		 function ValidateLenght()
	     { 
			var cmts = document.getElementById("txt_CategoryDescription").value;
			if (cmts.length > 100)
			{
			    event.returnValue=false;
                event.cancel = true;
                
			}
	    }
	    
		function saveClick()
		{
			var alert01 = "Please enter CategoryName to save";
			var txt_CategoryName = document.getElementById("txt_CategoryName");
			if(txt_CategoryName.value=="")
			{
				alert(alert01);
				return false;
			}
			else
			{
				return true;
			}
		}
	   function closewin()
	    {
			opener.location.reload();
			self.close();	   
	    }	    
		</script>
	</HEAD>


    <body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">

        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <div class="col-xs-12">
                <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left"><asp:label id="lbl_addcategory" Runat="server">Add/Edit Category</asp:label></h2>
                        </header>
                        <div class="content-body">
                <div class="row">
                    <div class="col-md-6 col-sm-7 col-xs-8">

                        <div class="form-group">
                            <label class="form-label" for="field-1">Category Name</label>
                            <div class="controls">
                                <asp:textbox id="txt_CategoryName" runat="server" CssClass="form-control" MaxLength="100" ></asp:textbox>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-7 col-xs-8">

                        <div class="form-group">
                            <label class="form-label" for="field-1">Short Description</label>
                            <div class="controls">
                                <asp:textbox id="txt_shortdesc" runat="server" CssClass="form-control" MaxLength="10" ></asp:textbox>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div class="form-group">
                            <label class="form-label" for="field-1">Category Description</label>
                            <div class="controls">
                                <asp:textbox id="txt_CategoryDescription" runat="server" CssClass="form-control" MaxLength="200" TextMode="MultiLine" onkeypress="ValidateLenght();"></asp:textbox>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div class="form-group">
                            <asp:label id="lblMessage" CssClass="form-label" runat="server" Font-Names="Verdana" ForeColor="Red" Font-Bold="True"></asp:label>
                        </div>

                    </div>
                </div>

                            <div class="row">
                
                    <div class="col-xs-12">

                        <div class="form-group">
                            <div class="controls">
                                <asp:button id="btn_submit" runat="server" CssClass="btn btn-primary pull-right" Text="Save" OnClientClick ="MaxLength();" ></asp:button>
                            </div>
                        </div>

                    </div>
                </div>


                </div>
                    </section></div>

            </div>
            <!-- END CONTAINER -->

        </form>
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        </body>













	<%--<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101; LEFT: 64px; WIDTH: 357px; POSITION: absolute; TOP: 16px; HEIGHT: 128px"
				cellSpacing="0" cellPadding="0" width="357" align="center" border="0">
				<TR>
					<TD class="clsLeftPaddingTable" vAlign="top">
						<asp:label id="lbl_addcategory" Runat="server" CssClass="clssubhead" BackColor="#EEC75E">Add/Edit Category</asp:label></TD>
				</TR>
				<TR>
				</TR>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="tblsub" height="20" cellSpacing="1" cellPadding="0" width="350" align="center"
							border="0">
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<P><STRONG>Category Name</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<asp:textbox id="txt_CategoryName" runat="server" CssClass="clsinputadministration" MaxLength="100"
										Width="276px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<P><STRONG>Short Description</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" vAlign="top">
									<asp:textbox id="txt_shortdesc" runat="server" CssClass="clsinputadministration" MaxLength="10"
										Width="276px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 31px" vAlign="top">
									<P><STRONG>Category Description</STRONG></P>
								</TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 31px" vAlign="top">
									<asp:textbox id="txt_CategoryDescription" runat="server" CssClass="clsinputadministration" MaxLength="200"
										Width="276px" TextMode="MultiLine" onkeypress="ValidateLenght();"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" vAlign="top" align="right" colSpan="2">
									<asp:button id="btn_submit" runat="server" CssClass="clsbutton" Width="87px" Text="Save" OnClientClick ="MaxLength();" ></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				<TR>
					<TD background="../../images/separator_repeat.gif" colSpan="2" height="11"></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<asp:label id="lblMessage" runat="server" Width="272px" Font-Size="XX-Small" Font-Names="Verdana"
							ForeColor="Red" Font-Bold="True"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>--%>
</HTML>

﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.WebForm1" Codebehind="PrincingMain.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Price Settings</title>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta cont  nt="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<SCRIPT src="../Scripts/Validationfx.js" type="text/javascript"></SCRIPT>

		<script language="javascript">
		
		function validate()
		{
									
			//a;
			var d1 = document.getElementById("cal_EffectiveFrom").value
			var d2 = document.getElementById("cal_EffectiveTo").value			
			
			if (d1 == d2)
				return true;
			var d3 = document.getElementById("cal_EndFrom").value
			var d4 = document.getElementById("cal_EndTo").value			
			
			if (d3 == d4)
				return true;
			
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false || compareDates(d4,'MM/dd/yyyy',d3,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("cal_EffectiveTo").focus(); 
					return false;
				}
				return true;
		}

		function ActiveDate(calobj)
		{
		var objdt = document.getElementById(calobj);
			alert(objdt.value.length);
			if ((objdt.value.length == 2) || (objdt.value.length ==null))
			{
				calobj.value = calobj.value & "/" ;
			}
		}
		
		function validation()
		{
		var ctrlEffFrom = document.getElementById("cal_EffectiveFrom");
		var ctrlEffTo = document.getElementById("cal_EffectiveTo");
		
		var ctrlEndFrom = document.getElementById("cal_EndFrom");
		var ctrlEndTo = document.getElementById("cal_EndTo");
		
		dtEffFrom = new Date(ctrlEffFrom.value);
		dtEffTo = new Date(ctrlEffTo.value);
		dtEndFrom = new Date(ctrlEndFrom.value);
		dtEndTo = new Date(ctrlEndTo.value);
		
		var ddlEffFrom = document.getElementById("ddl_EffectiveFrom");
		var ddlEffTo = document.getElementById("ddl_EffectiveTo");
		
		var ddlEndFrom = document.getElementById("ddl_EndFrom");
		var ddlEndTo = document.getElementById("ddl_EndTo");
		
		var bol = true; 
		
		bol = chkDateCreatria(dtEffFrom,dtEffTo,ddlEffTo.value,ddlEffFrom.value);
		if (bol == false)
		{
			return false;
		}
	
		bol = chkDateCreatria(dtEndFrom,dtEndTo,ddlEndTo.value,ddlEndFrom.value);
		if (bol == false)
		{
			return false;
		}
		
		bol = chkDateCreatria(dtEffTo,dtEndTo,ddlEndTo.value,ddlEffTo.value);
		if (bol == false)
		{
			return false;
		}
		
	return bol;
		
		}
		
		function chkDateCreatria(dtFrom,dtTo,ddlTo,ddlFrom)
		{
		
		if ((dtTo == "NaN" ) || (dtFrom == "NaN"))
		{
		alert("Select Date Range");
			 
			return false;
		}
		else if ((ddlTo == "00:00" ) || (ddlFrom == "00:00"))
		{
		alert("Select Time Range");
			 
			return false;
		}
		else if ((dtTo <= dtFrom))
		{	
			
			if ((dtTo < dtFrom))
			{
			alert("Invalid Date Range");
			 
			return false;
			}	
			else if(dtFrom <= dtTo)
			{
				if (ddlTo <= ddlFrom)
					{
					
					if (ddlTo < ddlFrom)
					{
						alert("Invalid Time Range");
						return false;
					}
					else if(ddlTo <= ddlFrom)
					{
					var timeFrom = ddlFrom;
					var lnTimeFrom = timeFrom.length - 3;
					var IndexTimeFrom = timeFrom.indexOf(':');
				
					addHFrom = timeFrom.substring(0,lnTimeFrom);
					addMFrom = timeFrom.substring(IndexTimeFrom +1,timeFrom.length);
				
				
					var timeTo = ddlTo;
					var lnTimeTo = timeTo.length - 3;
					var IndexTimeTo = timeTo.indexOf(':');
		
					addHTo = timeTo.substring(0,lnTimeTo);
					addMTo = timeTo.substring(IndexTimeTo+1,timeTo.length);
				
					
					if (addMTo <= addMFrom)
						{
							if (addMTo < addMFrom)
							{
							alert("Invalid Time Range");
							return false;
							}
							else if (addMTo <= addMFrom || addMTo == addMFrom )
							{
							
							return true;
							}
							
					
						}
					}}
				}
			else
			{
			return false;
			}
		}
		}
		
		
		</script>

        <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>        
        
  
	</HEAD>

    <body class=" ">
        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">
                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>


                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">

                                    <!-- PAGE HEADING TAG - START -->
                                    <h1 class="title">Price Plan</h1>
                                    <!-- PAGE HEADING TAG - END -->                            

                                </div>

                            </div>
                        </div>
                        <div class="clearfix">

                        </div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">
                        
                                <div class="content-body">

                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Effective Date From</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="EffectiveFrom" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                    <%--<ew:calendarpopup id="cal_EffectiveFrom" runat="server" Text=" " ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										                Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
										                ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										                <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
									                </ew:calendarpopup>--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Effective Time From</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_EffectiveFrom" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Effective Date To</label>
                                                <div class="controls">
                                                     <asp:TextBox ID="EffectiveTo" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                    <%--<ew:calendarpopup id="cal_EffectiveTo" runat="server" Text=" " ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										            <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
									            </ew:calendarpopup>--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Effective Time To</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_EffectiveTo" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">End Date From</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="EndFrom" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                   <%-- <ew:calendarpopup id="cal_EndFrom" runat="server" Text=" "  ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										                Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
										                ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										                <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
									                </ew:calendarpopup>--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">End Time From</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_EndFrom" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">End Date To</label>
                                                <div class="controls">
                                                     <asp:TextBox ID="EndTo" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                    <%--<ew:calendarpopup id="cal_EndTo" runat="server" Text=" " ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										                <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
									                </ew:calendarpopup>--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">End Date To</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_EndTo" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Court Location</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_crtloc" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">&nbsp;</label>
                                                <div class="controls">
                                                    <asp:checkbox id="Chkb_activeplan" runat="server" Text="Show All Active Plans" cssclass="form-label"></asp:checkbox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">&nbsp;</label>
                                                <div class="controls">
                                                    <asp:button id="btn_SearchPricing" runat="server" Text="Search Pricing Plan" CssClass="btn btn-primary pull-right"></asp:button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                </section></div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Plans</h2>
                                    <div class="actions panel_actions pull-right">
                                        <a href="frmAddPricePlan.aspx" class="btn btn-primary" style="color: white">Add New Plan</a>
                                        <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:datagrid id="dgViolInfo" runat="server" CssClass="table" AutoGenerateColumns="False">
									            <Columns>
										            <asp:TemplateColumn HeaderText="Description">
											            <ItemTemplate>
												            <asp:HyperLink id=lnk_plandesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlanDescription") %>'>
												            </asp:HyperLink>
												            <asp:Label id=lbl_hidPlanID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' Visible="False">
												            </asp:Label>
											            </ItemTemplate>
										            </asp:TemplateColumn>
										            <asp:TemplateColumn HeaderText="Short Name">
											            <ItemTemplate>
												            <asp:Label id=lbl_ShortName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlanShortName") %>' CssClass="label">
												            </asp:Label>
											            </ItemTemplate>
										            </asp:TemplateColumn>
										            <asp:TemplateColumn HeaderText="Effective Date">
											            <ItemTemplate>
												            <asp:Label id=lbl_effectiveDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.effectiveDate", "{0: MM/dd/yyyy hh:mm tt}") %>' CssClass="label">
												            </asp:Label>
											            </ItemTemplate>
										            </asp:TemplateColumn>
										            <asp:TemplateColumn HeaderText="End Date">
											            <ItemTemplate>
												            <asp:Label id=lbl_EndDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.endDate", "{0: MM/dd/yyyy hh:mm tt}") %>' CssClass="label">
												            </asp:Label>
											            </ItemTemplate>
										            </asp:TemplateColumn>
										            <asp:TemplateColumn HeaderText="Location">
											            <ItemTemplate>
												            <asp:Label id=lbl_courtloc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>' CssClass="label">
												            </asp:Label>
											            </ItemTemplate>
										            </asp:TemplateColumn>
										            <asp:TemplateColumn HeaderText="Status">
											            <ItemTemplate>
												            <asp:Label id=lblStatus runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsActivePlan") %>' CssClass="label">
												            </asp:Label>
											            </ItemTemplate>
										            </asp:TemplateColumn>
									            </Columns>
								            </asp:datagrid>

                                            <%--<table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1</th>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">2</th>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3</th>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>--%>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>


                        <!-- MAIN CONTENT AREA ENDS -->
                    
                    </section>

                </section>
                <!-- END CONTENT -->   

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->
        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 

              <script src="../assets/plugins/datepicker/js/datepicker.js"></script>
        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 
        <script type="text/javascript">
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });
</script>
    </body>











	<%--<body >
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" width=780 cellSpacing="0" cellPadding="0"
				align="center" border="0">
				<tbody>
				<tr>
						<tr>
						<td style="HEIGHT: 14px;" >
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
						</td>
					</tr>
	
				<tr>
					<td background="../../images/separator_repeat.gif"  style="height: 11px; "></td>
				</tr>
				<TR>
					<TD  >
						<TABLE class="clsLeftPaddingTable" id="Table2" cellSpacing="0" cellPadding="0"
							border="0" width=100%>
							<TR>
								<TD class="clsLeftPaddingTable" style="width: 292px; height: 7px" >Effective Date & Time From:
									</td>
									<td style="width: 165px; height: 7px"><ew:calendarpopup id="cal_EffectiveFrom" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
										ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 196px; height: 7px;"><asp:dropdownlist id="ddl_EffectiveFrom" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable" style="HEIGHT: 7px">To:</td>
								<td style="width: 179px; height: 7px"><ew:calendarpopup id="cal_EffectiveTo" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True"
										CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 155px; height: 7px;"><asp:dropdownlist id="ddl_EffectiveTo" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 292px">
								End Date & Time From:</td>
								<td style="width: 165px">
									<ew:calendarpopup id="cal_EndFrom" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True"
										Nullable="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
										ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 196px"><asp:dropdownlist id="ddl_EndFrom" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable">To:</td>
								<td style="width: 179px"><ew:calendarpopup id="cal_EndTo" runat="server" Text=" " Width="100px" Font-Size="8pt" Font-Names="Tahoma"
										ToolTip="Call Back Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False"
										ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True">
										<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
										<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></WeekdayStyle>
										<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></MonthHeaderStyle>
										<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
											BackColor="AntiqueWhite"></OffMonthStyle>
										<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></GoToTodayStyle>
										<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGoldenrodYellow"></TodayDayStyle>
										<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Orange"></DayHeaderStyle>
										<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="LightGray"></WeekendStyle>
										<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="Yellow"></SelectedDateStyle>
										<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></ClearDateStyle>
										<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
											BackColor="White"></HolidayStyle>
									</ew:calendarpopup>
									</td>
									<td style="width: 155px"><asp:dropdownlist id="ddl_EndTo" runat="server" Width="105px" CssClass="clsinputcombo"></asp:dropdownlist>
							</TD>
							</TR>
							<TR>
								<TD class="clsLeftPaddingTable" style="WIDTH: 292px">Court 
									Location</td>
									<td style="width: 165px"><asp:dropdownlist id="ddl_crtloc" runat="server" Width="145px" CssClass="clsinputcombo"></asp:dropdownlist></TD>
								<TD class="clsLeftPaddingTable" style="WIDTH: 196px" ><asp:checkbox id="Chkb_activeplan" runat="server" Text="Show All Active Plans" Width="127px" cssclass="clsLeftPaddingTable"></asp:checkbox></TD>
								<TD class="clsLeftPaddingTable" align="right" >&nbsp;</TD>
								<TD class="clsLeftPaddingTable" align="right" style="width: 179px" >&nbsp;</TD>
								<td class="clsLeftPaddingTable" align="right" style="width: 155px"><asp:button id="btn_SearchPricing" runat="server" Text="Search Pricing Plan " CssClass="clsbutton" Width="149px"></asp:button></td>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td background="../../images/headbar_headerextend.gif" ></td>
				</tr>
				<tr>
				    <td>
				        <table cellSpacing="0" cellPadding="0" border = "0" width=100%>
						    <tr>
				            	<td class="clssubhead" background="../../Images/subhead_bg.gif" width=80% height=34>&nbsp;Plans
				            	</td>
					            <td class="clssubhead" align="right" background="../../Images/subhead_bg.gif" ><A href="frmAddPricePlan.aspx">Add New Plan</A>&nbsp;
					            </td>
				            </tr>
				        </table>
				    </td>
				</tr>
				
				<TR>
					<td >
						<table cellSpacing="0" width=100% cellPadding="0" border="0" >
							<tr>
								<TD align="center" ><asp:datagrid id="dgViolInfo" runat="server" Width="100%" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Description">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:HyperLink id=lnk_plandesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlanDescription") %>'>
													</asp:HyperLink>
													<asp:Label id=lbl_hidPlanID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Planid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Short Name">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_ShortName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlanShortName") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Effective Date">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_effectiveDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.effectiveDate", "{0: MM/dd/yyyy hh:mm tt}") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="End Date">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_EndDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.endDate", "{0: MM/dd/yyyy hh:mm tt}") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Location">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_courtloc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Status">
												<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblStatus runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IsActivePlan") %>' CssClass="label">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td background="../../images/separator_repeat.gif"  height="11"></td>
				</tr>
				<TR>
					<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
				</tbody>
			</TABLE>
			</form>
	</body>--%>
	
	
</HTML>

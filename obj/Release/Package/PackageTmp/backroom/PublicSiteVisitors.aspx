<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PublicSiteVisitors.aspx.cs" Inherits="lntechNew.backroom.PublicSiteVisitors" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Public Site Visitors</title>
      <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script language=javascript>
        function OpenPopup(path)
        {
            window.open(path);
            return false;
        }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" style="height: 15px">
                </td>
            </tr>
            <tr class="clsLeftPaddingTable">
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 780px">
                        <tr>
                            <td class="clssubhead" style="height: 27px">
                                &nbsp; Start Date</td>
                            <td style="height: 27px">
                                &nbsp;<ew:calendarpopup id="datefrom" runat="server" allowarbitrarytext="False" calendarlocation="Left"
                                    controldisplay="TextBoxImage" culture="(Default)" displayoffsety="20" enablehidedropdown="True"
                                    font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" nullable="True"
                                    padsingledigits="True" selecteddate="2003-01-11" showcleardate="True" showgototoday="True"
                                    tooltip="Mail Date From" upperbounddate="12/31/9999 23:59:00" width="117px">
                            <TextboxLabelStyle CssClass="clstextarea"  />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black"  />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray"  />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black"  />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black"  />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                        </ew:calendarpopup></td>
                            <td class="clssubhead" style="height: 27px; width: 54px;">
                                End Date</td>
                            <td style="height: 27px">
                                &nbsp;<ew:calendarpopup id="dateto" runat="server" allowarbitrarytext="False" calendarlocation="Left"
                                    controldisplay="TextBoxImage" culture="(Default)" displayoffsety="20" enablehidedropdown="True"
                                    font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" nullable="True"
                                    padsingledigits="True" selecteddate="2007-12-11" showcleardate="True" showgototoday="True"
                                    tooltip="Mail Date From" upperbounddate="12/31/9999 23:59:00" width="117px">
                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black"  />
                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <TextboxLabelStyle CssClass="clstextarea"  />
                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black"  />
                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Gray"  />
                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                ForeColor="Black"  />
                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                Font-Size="XX-Small" ForeColor="Black"  />
                        </ew:calendarpopup></td>
                            <td align="right" style="height: 26px">
                                <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" OnClick="btn_search_Click"
                                    Text="Search" Width="84px" />&nbsp;</td>
                            <td align="right" style="height: 26px">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="PSReport2.aspx?smenu=111">Call Quote Report</asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" style="height: 9px">
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="800px" CssClass="clsleftpaddingtable" OnRowDataBound="GridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="S.No">
                                <HeaderStyle CssClass="clssubhead" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_s" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.casenum") %>' Visible="False"></asp:Label>
                                    <asp:HyperLink ID="hl_s" runat="server">[hl_s]</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Address">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_email" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.UserEmail") %>'></asp:Label>
                                    <asp:HiddenField ID="hf_pf" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.ProcessFlag") %>' />
                                    <asp:HiddenField ID="hf_cid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.Customerid") %>' />
                                    &nbsp;
                                    <asp:LinkButton ID="hl_email" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.UserEmail") %>'
                                        Visible="False"></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ticket Id's">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ticketid" runat="server"  CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.TicketID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Violations">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_violations" runat="server" Visible="false" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.Violations") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Acc.Flag">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_aflag" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.AFlag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CDL">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_cdl" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.CDL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="School Zone">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_zone" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.ZFlag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_amount" runat="server" CssClass="label" Text='<%# "$" + DataBinder.Eval(Container,"DataItem.amount","{0:g0}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Visit Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_logdate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.LogDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Page ">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_lastpage" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.LastPage") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Search Type">
                                <HeaderStyle CssClass="clssubhead" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_searchtype" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.SearchType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td background="../images/separator_repeat.gif" style="height: 9px" valign="top">
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <uc2:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

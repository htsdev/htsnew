<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.AutoDialerLog"
    CodeBehind="AutoDialerLog.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Auto Dialer Log</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
		function validate()
		{		
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}		
		
		
    </script>

    <style type="text/css">
        .style1
        {
            width: 97px;
        }
        .style2
        {
            width: 107px;
        }
        .style3
        {
            width: 120px;
        }
        .style4
        {
            width: 406px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
    <table id="tblMain" cellspacing="0" cellpadding="0" width="870" align="center" border="0">
        <tr>
            <td align="left">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 33px">
                <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clssubhead">
                            Event:
                        </td>
                        <td class="style1">
                            <asp:DropDownList ID="ddl_Events" runat="server" CssClass="clsinputcombo">
                            </asp:DropDownList>
                        </td>
                        <td class="clssubhead" id="td_from">
                            From:
                        </td>
                        <td valign="middle" id="td_fromdate" class="style2">
                            <ew:CalendarPopup ID="dtFrom" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="20" ToolTip="Date From" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False"
                                ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td class="clssubhead" id="td_to">
                            To:
                        </td>
                        <td id="td_todate" class="style3">
                            <ew:CalendarPopup ID="dtTo" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                DisplayOffsetY="20" ToolTip="Date To" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                ShowClearDate="True" Nullable="True" Culture="(Default)" AllowArbitraryText="False"
                                ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td class="style4">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsbutton" Text="Submit"></asp:Button>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table id="tblTitle" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="clssubhead" height="34px" background="../Images/subhead_bg.gif">
                            &nbsp;
                        </td>
                        <td class="clssubhead" align="right" height="34px" background="../../Images/subhead_bg.gif">
                            <table id="tblPageNavigation" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="clslabel" align="right" style="width: 48px">
                                        <strong>Page</strong> #
                                    </td>
                                    <td align="left">
                                        &nbsp;<asp:Label ID="lblCurrPage" runat="server" Width="25px" CssClass="clslabel"
                                            Font-Bold="True"></asp:Label>
                                    </td>
                                    <td class="clslabel" align="right">
                                        &nbsp;<strong>Goto</strong> &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:DropDownList ID="ddlPageNo" runat="server" CssClass="clsinputcombo" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:DataGrid ID="dg_OutCome" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                    BorderColor="White" CellPadding="0" AllowPaging="True" AutoGenerateColumns="False"
                    AllowSorting="True" PageSize="30" OnItemCommand="dg_OutCome_ItemCommand">
                    <ItemStyle Height="18px"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="S#">
                            <HeaderStyle HorizontalAlign="Left" Width="5%"  CssClass="clsaspcolumnheader">
                            </HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblSerial" runat="server"></asp:Label>
                                <asp:Label ID="lbl_Ticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:Label ID="lbl_CallID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="eventtype" HeaderText="Event Type">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="clsaspcolumnheader">
                            </HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="calldatetime" SortExpression="TimeStamp" HeaderText="Call Date & Time">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="clsaspcolumnheader">
                            </HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Note" HeaderText="Note">
                            <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" CssClass="GridItemStyle" VerticalAlign="Middle">
                            </ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle NextPageText="&nbsp; Next >" PrevPageText="< Prev &nbsp;" HorizontalAlign="Center">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" height="11">
            </td>
        </tr>
        <tr>
            <td style="width: 760px" align="left" colspan="4">
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

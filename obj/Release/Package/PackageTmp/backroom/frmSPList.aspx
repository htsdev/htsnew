﻿<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.SqlSrvMrg.frmSPList" Codebehind="frmSPList.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Stored Procedure List</title>

		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

        <script src="../Scripts/Dates.js" type="text/javascript"></script>

		<script language="javascript">

		// Enabling and Disabling Dg_Output(datagrid) Controls(Combobox and textbox) 
		function ShowHide(d1,t1,d2)
		{
			
			var ddl1 = document.getElementById(d1 );
			var ddl2 = document.getElementById(d2 );
			var txt = document.getElementById(t1 );

			if (ddl1.selectedIndex == 0)
				{
				ddl2.disabled = true;
				txt.disabled = true;
				}
			else
				{
				ddl2.disabled = false;
				txt.disabled = false;
				}

		}

			// Validating the Controls
    	 function DoValidate()
		{
			var cnt = (document.Form1.TextBox2.value) * 1 ;
			cnt= cnt + 2;
			
			var grdName = "Dg_Output";
			var idx=2;
		
			var reportname = document.getElementById("txtreportname");
			if (reportname.value == "")
			{
					alert("Please type Report name where you want to save parameters field.");
					return false;
			} 
			for (idx=2; idx < cnt; idx++)
			{
				var ctlName ="";
				var ctext = "";
				var calias = "";
				
				if(idx < 10)
				{
				 ctlName = grdName+ "_ctl0" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl0" + idx + "_txtCmd";
				 calias = grdName+ "_ctl0" + idx + "_txtParamAlias";
				}
				else
				{
				 ctlName = grdName+ "_ctl" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl" + idx + "_txtCmd";
				 calias = grdName+ "_ctl" + idx + "_txtParamAlias";
				}
				
				var elemnt = document.getElementById(ctlName);	
				var etext = document.getElementById(ctext);
				var ealias = document.getElementById(calias);
				if ((elemnt.selectedIndex == 1  && etext.value == "") )
					{
					alert("Please type SQL query or specify procedure name.");
					etext.focus();
					return false;
					}
				if (ealias.value =="")
				{
					alert("Please type Parameter Alias.");
					return false;
				}
				
				
			}
		}
		 
		</script>

	</HEAD>


    <body class=" ">
        
        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">

                                    <!-- PAGE HEADING TAG - START -->
                                    <h1 class="title">Report Editor</h1>
                                    <!-- PAGE HEADING TAG - END -->                            

                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">
                                <div class="content-body">
                                    <div class="row">

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Stored Procedure</label>
                                                <div class="controls">
                                                    <asp:textbox id="txtSearchSp" Runat="server" CssClass="form-control"></asp:textbox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="form-group">
                                                <div class="controls">
                                                    <asp:button id="ImageButton1" runat="server" CssClass="btn btn-primary" Text="Search"></asp:button>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">
                                            <asp:label id="lblCurrPage" runat="server" Font-Size="Smaller">Current Page :</asp:label>&nbsp;<asp:label id="lblPNo" runat="server" Font-Size="Smaller" Width="16px" Height="10px">a</asp:label>
                                            <div class="form-group">
                                                <div class="controls">
                                                    <asp:label id="lblGoto" runat="server" Font-Size="Smaller" Width="16px">Goto</asp:label>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="form-group">
                                                <div class="controls">
                                                    
                                                </div>
                                            </div>

                                        </div>

                                        <asp:label id="lblNoRec" runat="server" CssClass="form-control" ForeColor="Red" Visible="False"></asp:label>

                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Lists of Stored Procedures</h2>  
                                    <div class="actions panel_actions pull-right">
                	                    <asp:dropdownlist id="cmbPageNo" runat="server" CssClass="form-control input-sm m-bot15" Font-Size="Smaller" AutoPostBack="True"></asp:dropdownlist>
                                    </div>                                  
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:datagrid id="DgSqlProcedure" runat="server" CssClass="table" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" PageSize="20">
											    
											    <Columns>
												    <asp:TemplateColumn HeaderText="Procedure Name">
													    <ItemTemplate>
														    <asp:HyperLink id=HLSpnm runat="server" CssClass="form-label" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ProcedureName") %>'>
														    </asp:HyperLink>
														    <asp:LinkButton id=lnkSPName runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ProcedureName") %>' CommandName="ShowParam">
														    </asp:LinkButton>
													    </ItemTemplate>
												    </asp:TemplateColumn>
												    <asp:TemplateColumn HeaderText="Create Date">
													    <ItemTemplate>
														    <asp:Label id=Label1 runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'>
														    </asp:Label>
													    </ItemTemplate>
													    <EditItemTemplate>
														    <asp:TextBox id=TextBox1 runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.crdate") %>' MaxLength="11">
														    </asp:TextBox>
													    </EditItemTemplate>
												    </asp:TemplateColumn>
											    </Columns>
											    <PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
										    </asp:datagrid>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        
                        <asp:label id="lblmsg" runat="server" CssClass="form-label" ForeColor="Red" Visible="False"></asp:label>

                        <table>
                            <tr>
                                <td style="visibility: hidden;">
                                    <asp:textbox id="txtSpName" runat="server" Visible="False"></asp:textbox>
                                    <asp:textbox id="txtchk" runat="server">0</asp:textbox>
                                    <asp:textbox id="txtNoParam" runat="server">0</asp:textbox>
                                    <asp:textbox id="TextBox2" runat="server"></asp:textbox>
                                </td>
                            </tr>
                        </table>
                        

                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                </section>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 


        </body>






















	<%--<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" >
		<form id="Form1" method="post" runat="server">
			<table id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 9px" align="left" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<tr> <!-- tahir   -->
									<td class="clsleftpaddingtable" style="HEIGHT: 25px" vAlign="bottom" align="center"
										width="100%"><STRONG><FONT face="Verdana" color="#0066cc">
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TBODY>
														<TR>
															<TD class="clsleftpaddingtable" style="WIDTH: 95px" width="95"><SPAN class="style2">Stored&nbsp;Procedure</SPAN></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 126px" width="126"><asp:textbox id="txtSearchSp" Runat="server" CssClass="FrmTDLetter"></asp:textbox></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 160px" width="160">&nbsp;<asp:button id="ImageButton1" runat="server" CssClass="clsbutton" Text="Search"></asp:button></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 159px" align="right">&nbsp;</TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 99px" align="right"><asp:label id="lblCurrPage" runat="server" Font-Size="Smaller">Current Page :</asp:label>&nbsp;<asp:label id="lblPNo" runat="server" Font-Size="Smaller" Width="16px" Height="10px">a</asp:label></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 28px" align="right">&nbsp;<asp:label id="lblGoto" runat="server" Font-Size="Smaller" Width="16px">Goto</asp:label></TD>
															<TD align="right">&nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" CssClass="clsinputcombo" Font-Size="Smaller" AutoPostBack="True"></asp:dropdownlist></TD>
														</TR>
													</TBODY>
												</TABLE>
											</FONT></STRONG>
										<asp:label id="lblNoRec" runat="server" CssClass="cmdlinks" Width="184px" ForeColor="Red" Visible="False"></asp:label></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 14px" vAlign="bottom" align="center" width="100%" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 34px" vAlign="middle" align="left" width="100%" background="../../Images/subhead_bg.gif"><STRONG><STRONG class="clssubhead">&nbsp;Lists 
												of Stored Procedures</STRONG></STRONG></TD>
								</TR>
								<TR>
									<td vAlign="top" width="100%"><asp:datagrid id="DgSqlProcedure" runat="server" CssClass="clsleftpaddingtable" Width="100%" AllowPaging="True"
											AutoGenerateColumns="False" CellPadding="0" PageSize="20">
											<HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
											<FooterStyle CssClass="GrdFooter"></FooterStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="Procedure Name">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
													<ItemTemplate>
														<asp:HyperLink id=HLSpnm runat="server" CssClass="cmdLinks2" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ProcedureName") %>'>
														</asp:HyperLink>
														<asp:LinkButton id=lnkSPName runat="server" CssClass="cmdLinks2" Text='<%# DataBinder.Eval(Container, "DataItem.ProcedureName") %>' CommandName="ShowParam">
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Create Date">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=Label1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:TextBox id=TextBox1 runat="server" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.crdate") %>' MaxLength="11">
														</asp:TextBox>
													</EditItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
										</asp:datagrid></td>
								</TR>
								<TR>
									<TD align="center" width="800"><asp:label id="lblmsg" runat="server" CssClass="cmdlinks" ForeColor="Red" Visible="False"></asp:label></TD>
								</TR>
								<TR>
									<TD style="VISIBILITY: hidden; HEIGHT: 20px" align="right"> <!-- DESIGNTIMEDRAGDROP="1460" --><asp:textbox id="txtSpName" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtchk" runat="server">0</asp:textbox><asp:textbox id="txtNoParam" runat="server">0</asp:textbox><asp:textbox id="TextBox2" runat="server"></asp:textbox></TD>
								</TR>
							</TBODY>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
		</form>
	</body>--%>
</HTML>

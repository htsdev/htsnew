<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompetitorsReport_New.aspx.cs"
    Inherits="HTP.backroom.CompetitorsReport_New" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Competitors Report</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script language="javascript">
		
		
		
		

//Kamran 3624 04/04/08 add Date Fix with 1900
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateForm()
{

// TODO : Need to refactor logic
    //a;
//	var dt=document.getElementById("calQueryFrom").value;
//	if (isDate(dt.value)==false)
//	{
//		return false
//	}
//	
//	var dt1=document.getElementById("calQueryTo").value;
//	if (isDate(dt1.value)==false)
//	{
//		return false
//	}    

 }


		
		
		
	      
	        function validate()
		    {
			    //a;
			    var d1 = document.getElementById("calQueryFrom").value
			    var d2 = document.getElementById("calQueryTo").value			
    			
			    if (d1 == d2)
				    return true;
			    if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("calQueryTo").focus(); 
					return false;
				}
				return true;
		    }
		    
		    //Agha Usman 4138 07/07/08
		    function showHideCombo(myval1, myval2) 
		    {
		        //Waqas Javed 5255 12/16/08
		        if(document.getElementById("pnl_Att_Bond_Att").style.display != "none")
		        {
		            document.getElementById("pnl_Att_Bond_Att").style.display = "none";
		        }
		        if(document.getElementById("pnl_Out_Analysis").style.display != "none")
		        {
		            document.getElementById("pnl_Out_Analysis").style.display = "none";
		        }
		        
		        if (myval1 == 0) 
		        {
		           document.getElementById("ddlRecords").style.display = "none";
		           document.getElementById("ddlType").style.display = "none";
		        }
		        else if (myval1 ==1) 
		        {
		            document.getElementById("ddlRecords").style.display = "block";
		            
		            if (myval2 == 1) 
		            {
		                document.getElementById("ddlType").style.display = "block";
		            }
		            else
		            {
		                document.getElementById("ddlType").style.display = "none";
		            }		            
		        }
		        
		    }
	  
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" style="z-index: 200" cellspacing="0" cellpadding="0" width="780"
            align="center" border="0">
            <tbody>
                <tr>
                    <td style="height: 14px" colspan="4">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="TableSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td background="../../images/separator_repeat.gif" style="height: 11px" 
                                    colspan="6">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="Table6" cellspacing="0" cellpadding="0" bgcolor="white" border="0" style="width: 100%;">
                                        <tr>
                                            <td align="left" style="width: 140;">
                                                                                                Start Date:<ew:CalendarPopup ID="calQueryFrom" runat="server" Nullable="True"
                                                    Width="80px" ImageUrl="../images/calendar.gif" Font-Names="Tahoma" Font-Size="8pt"
                                                    ControlDisplay="TextBoxImage" CalendarLocation="Left" ShowGoToToday="True" AllowArbitraryText="False"
                                                    Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                                    ToolTip="Call Back Date" ShowClearDate="True">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td align="left" style="width: 140;">
                                                End Date:
                                                <ew:CalendarPopup ID="calQueryTo" runat="server" Nullable="True" Width="80px" ImageUrl="../images/calendar.gif"
                                                    Font-Names="Tahoma" Font-Size="8pt" ControlDisplay="TextBoxImage" CalendarLocation="Left"
                                                    ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                    PadSingleDigits="True" ToolTip="Call Back Date" ShowClearDate="True">
                                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></WeekdayStyle>
                                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="LightGray"></WeekendStyle>
                                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                                        BackColor="White"></HolidayStyle>
                                                </ew:CalendarPopup>
                                            </td>
                                            <td align="left" style="width: 250;">
                                                <asp:RadioButton ID="rdbtn_Att" runat="server" CssClass="clslabelnew" GroupName="att"
                                                    Text="Attorney Tracker" Visible ="false"  onclick="showHideCombo(0,0)" />
                                                <asp:RadioButton ID="rdbtn_bond" runat="server" CssClass="clslabelnew" GroupName="att"
                                                    Text="Bond Tracker" onclick="showHideCombo(0,0)" />&nbsp;<br />
                                                <asp:RadioButton ID="rdbtn_Att_Archive" runat="server" Checked="True" CssClass="clslabelnew"
                                                    GroupName="att" Text="Attorney Archive" onclick="showHideCombo(1,0)" />
                                                <asp:RadioButton ID="rdbtn_out_analysis" Visible="false" runat="server" CssClass="clslabelnew" GroupName="att"
                                                    Text="Outcome Analysis" onclick="showHideCombo(1,1)" />
                                            </td>
                                            
                                            <td align="left" style="width: 75;" valign="bottom">
                                                <asp:DropDownList ID="ddlRecords" runat="server" CssClass="clsinputcombo">
                                                    <asp:ListItem Value="10">Top 10</asp:ListItem>
                                                    <asp:ListItem Value="20">Top 20</asp:ListItem>
                                                    <asp:ListItem Value="30">Top 30</asp:ListItem>
                                                    <asp:ListItem Value="40">Top 40</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" style="width: 100;" valign="bottom">
                                                <asp:DropDownList ID="ddlType" runat="server" CssClass="clsinputcombo">
                                                    <asp:ListItem Value="Clients">By Clients</asp:ListItem>
                                                    <asp:ListItem Value="Cases">By Cases</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right" style="width: 100;" >
                                                <asp:Button ID="btn_submit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_submit_Click"
                                                    OnClientClick="return ValidateForm();"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" colspan="6" 
                                                style="height: 7">                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="TableGrid" cellspacing="0" cellpadding="0" width="100%" bgcolor="white"
                                        border="0">
                                        <tr>
                                            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;
                                                width: 743px;">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            &nbsp;<asp:Label ID="lbl_status" runat="server" CssClass="clssubhead"></asp:Label>
                                                        </td>
                                                        <td style="text-align: right">
                                                            &nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center" colspan="2">
                                                <asp:Panel ID="pnl_Att_Bond_Att" runat="server">
                                                    <asp:DataGrid ID="dg_valrep" runat="server" Width="100%" CssClass="clsLeftPaddingTable"
                                                        BackColor="#EFF4FB" AutoGenerateColumns="False" BorderColor="White" BorderStyle="None"
                                                        OnItemDataBound="dg_valrep_ItemDataBound" AllowSorting="True" CellPadding="0"
                                                        OnSortCommand="dg_valrep_SortCommand" OnItemCommand="dg_valrep_ItemCommand" PageSize="30"
                                                        OnPageIndexChanged="dg_valrep_PageIndexChanged" ShowFooter="True">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="S.No">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="name" SortExpression="name">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_abbrev" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Underline="true" CommandName="name">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="<u>S.#</u>" SortExpression="noofcases">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_clickscount" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.noofcases") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Font-Underline="true" CommandName="noofcases">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="clients" SortExpression="sumBond_clinents">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totalquote" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sumBond_clients") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" Font-Underline="true" CommandName="sumBond_clients">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="amount" SortExpression="AmountSum">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totalsum" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.AmountSum","{0:C}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton5" runat="server" Font-Underline="true" CommandName="AmountSum">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="percent" SortExpression="percentage">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totalclient" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.percentage") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" Font-Underline="true" CommandName="percentage">LinkButton</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="No. of Arr Sets" SortExpression="ArrCount">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_ArrCount" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ArrCount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton6" runat="server" Font-Underline="true" CommandName="ArrCount">No. of Arr Sets</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="No. of Jury Sets" SortExpression="JuryCount" Visible="False">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_JuryCount" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.JuryCount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton7" runat="server" Font-Underline="true" CommandName="JuryCount">No. of Jury Sets</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="No. of Trial(Non Jury) Sets" SortExpression="NonJuryCount">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_NonJuryCount" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.NonJuryCount") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton8" runat="server" Font-Underline="true" CommandName="NonJuryCount">No. of Trial(Non Jury) Sets</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="<u>% of Clients</u>" SortExpression="notsort">
                                                                <HeaderStyle CssClass="clssubhead"></HeaderStyle>
                                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_perc_clients" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.perc_clients") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="LinkButton9" runat="server" Font-Underline="true" CommandName="perc_clients">% of Clients</asp:LinkButton>
                                                                </HeaderTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" NextPageText="Next&amp;gt;" PrevPageText="&amp;ltPrevious&amp;nbsp;&amp;nbsp;" />
                                                        <FooterStyle CssClass="clssubhead" BackColor="LightGray" />
                                                    </asp:DataGrid>
                                                    <br />
                                                    <asp:Repeater ID="drepDateRange" runat="server" OnItemDataBound="drepDateRange_ItemDataBound">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblStartDate" runat="server" CssClass="Label"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DataGrid ID="dgAttorney" runat="server" AutoGenerateColumns="False" BackColor="#EFF4FB"
                                                                            BorderColor="White" BorderStyle="None" CellPadding="0" CssClass="clsLeftPaddingTable"
                                                                            Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderText="S.No">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Attorney Name">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAttorneyName" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Cases">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCases" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.noofcases") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Clients">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblClients" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sumBond_clients") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="% of Cases">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPercentageOfCases" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.percentage") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="% of Clients">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPercentageOfClients" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.perc_clients") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </asp:Panel>
                                                <asp:Panel ID="pnl_Out_Analysis" runat="server">
                                                    <br />
                                                    <asp:Repeater ID="drepDateRangeOutAnalysis" runat="server" OnItemDataBound="drepDateRangeOutAnalysis_ItemDataBound">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblStartDate" runat="server" CssClass="Label"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DataGrid ID="dg_out_analysis" runat="server" AutoGenerateColumns="False" BackColor="#EFF4FB"
                                                                            BorderColor="White" BorderStyle="None" CellPadding="0" CssClass="clsLeftPaddingTable"
                                                                            Width="100%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderText="S.No">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lbl_sno" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Firm">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFirm" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.attorney") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Total Clients">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTotalClients" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.clients", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Total Cases">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTotalCases" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.cases", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Clients Disposed">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblClientsDisposed" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.c_clients", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Cases Disposed">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCasesDisposed" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.c_cases", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Clients DSC/DADJ">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblClientsDSCDADJ" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.dsc_clients", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Cases DSC/DADJ">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCasesDSCDADJ" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.dsc_cases", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Clients With MOVERS PLEAD">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblClientsPlead" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.plea_clients", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Cases With MOVERS PLEAD">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCasesPlead" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.plea_cases", "{0:N0}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="% of Clients Disposed">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPercentageOfClientDisposed" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.perc_f_clients", "{0:#0.00%}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="% of Cases Disposed">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPercentageOfCasesDisposed" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.perc_f_cases", "{0:#0.00%}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="% of Clients with DSC/DADJ/Movers Plea">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPercentageOfClient" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.perc_g_clients", "{0:#0.00%}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="% of Cases with DSC/DADJ/Movers Plea">
                                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPercentageOfCases" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.perc_g_cases", "{0:#0.00%}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    &nbsp;
                                                </asp:Panel>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center" colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="../../images/separator_repeat.gif" style="width: 743px; height: 12px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 743px">
                                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

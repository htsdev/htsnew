﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoDialerSettings_bkp.aspx.cs"
    Inherits="HTP.backroom.AutoDialerSettings" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Auto Dialer Settings</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td style="width: 100%">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" style="height: 34px; width: 100%" class="clssubhead">
                    &nbsp; Auto Dialer Settings
                </td>
            </tr>
            <tr>
                <td style="width: 100%" align="center">
                    <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <ajaxToolkit:TabContainer ID="tabs_EventType" runat="server"  Width="100%" ScrollBars="Auto">                        
                    </ajaxToolkit:TabContainer>
                </td>
            </tr>
            <tr>
                <td style="height: 14px; width: 100%" background="../Images/separator_repeat.gif">
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" style="height: 11px; width: 100%">
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmEmailedTrialLetter.aspx.cs"
    Inherits="HTP.backroom.frmEmailedTrialLetter" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Response Report</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
     function EnabledFilterControls()
        {
            var hp_TrialletterBatch = document.getElementById( hp_TrialletterBatch );
            var ddlEmailType=document.getElementById( ddlEmailType );

            if( ddlEmailType.selectedvalue == 'Trial' )
                {
                    hp_TrialletterBatch.visible=false;
                }

            else
                { 
                     hp_TrialletterBatch.visible=true;
                }
        }
        //Fahad 7167 12/23/2009 To check Or Validate the From and End Date
        function CheckDateValidation()
        {
            if (IsDatesEqualOrGrater(document.form1.cal_fromDate.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, Start Date must be greater than or equal to 1/1/1900");
				return false;
			}
			
			if (IsDatesEqualOrGrater(document.form1.cal_todate.value,'MM/dd/yyyy','01/01/1900','MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to 1/1/1900");
				return false;
			}
            if (IsDatesEqualOrGrater(document.form1.cal_fromDate.value,'MM/dd/yyyy',document.form1.cal_todate.value,'MM/dd/yyyy')==false)
            {
			    alert("Please enter valid date, End Date must be greater than or equal to Start Date");
				return false;
			}
			
        }
		
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0" />
        <table id="tblMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td style="height: 27px" colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr background="../images/separator_repeat.gif">
                <td background="../images/separator_repeat.gif" colspan="5" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="tblSearchCriteria" cellspacing="1" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="clsLeftPaddingTable" style="width: 80px">
                                <span class="clssubhead">Email Type :</span>
                            </td>
                            <td class="clsLeftPaddingTable" style="width: 120px">
                                <asp:DropDownList ID="ddlEmailType" runat="server" CssClass="clsInputadministration"
                                    Width="110px" AutoPostBack="true" OnSelectedIndexChanged="ddlEmailType_SelectedIndexChanged">
                                    <asp:ListItem Text="Trial Letter Email" Value="Trial" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Thank You Email" Value="Thank">
                                    </asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--<td class="clsLeftPaddingTable" style="width: 30px">
                                <span class="clssubhead">City :</span>
                            </td>
                            <td class="clsLeftPaddingTable">
                                <asp:DropDownList ID="ddlcity" runat="server" CssClass="clsInputadministration">
                                    <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                                    <asp:ListItem Value="1">Houston</asp:ListItem>
                                    <asp:ListItem Value="2">Dallas</asp:ListItem>
                                </asp:DropDownList>
                            </td>--%>
                            <td class="clsLeftPaddingTable">
                                <font color="#3366cc"><strong>From:</strong></font>
                            </td>
                            <td class="clsLeftPaddingTable" style="width: 137px">
                                <ew:CalendarPopup ID="cal_todate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                    ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True"
                                    UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
                                    ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
                                    <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                    <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></WeekdayStyle>
                                    <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                    <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                        BackColor="AntiqueWhite"></OffMonthStyle>
                                    <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                    <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                    <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                    <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="LightGray"></WeekendStyle>
                                    <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                    <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        ForeColor="Black" BackColor="White"></ClearDateStyle>
                                    <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                        BackColor="White"></HolidayStyle>
                                </ew:CalendarPopup>
                                <td class="clsLeftPaddingTable" style="width: 26px">
                                    <font color="#3366cc"><strong>To:</strong></font>
                                </td>
                                <td class="clsLeftPaddingTable" style="width: 133px">
                                    <ew:CalendarPopup ID="cal_fromDate" runat="server" Width="90px" Font-Size="8pt" Font-Names="Tahoma"
                                        ImageUrl="../images/calendar.gif" ToolTip="Select Court Date Range" PadSingleDigits="True"
                                        UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
                                        ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage">
                                        <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                        <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></WeekdayStyle>
                                        <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                        <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                            BackColor="AntiqueWhite"></OffMonthStyle>
                                        <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                        <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                        <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                        <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="LightGray"></WeekendStyle>
                                        <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                        <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            ForeColor="Black" BackColor="White"></ClearDateStyle>
                                        <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                            BackColor="White"></HolidayStyle>
                                    </ew:CalendarPopup>
                                </td>
                                <td class="clsLeftPaddingTable">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClientClick="CheckDateValidation()"
                                        OnClick="btnSearch_Click"></asp:Button>
                                </td>
                                <td class="clsLeftPaddingTable" align="right">
                                    <asp:HyperLink ID="hp_TrialletterBatch" runat="server" NavigateUrl="~/backroom/frmBatchTrialLetter.aspx">Trial Letter Batch Report</asp:HyperLink>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                    <table width="100%">
                        <tr>
                            <td align="left" class="clssubhead">
                                Email Response Report
                            </td>
                            <td align="right" class="clssubhead">
                                <aspnew:UpdatePanel ID="updatepnlpaging" runat="server" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc4:PagingControl ID="Pagingctrl" runat="server" />
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr background="../images/separator_repeat.gif">
                <td background="../images/separator_repeat.gif" colspan="5" height="11" width="100%">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lbl_Msg" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td id="grid" valign="top" width="100%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td valign="top" width="100%">
                                    <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="DG_TrialLetterEmailed" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                CssClass="clsLeftPaddingTable" OnPageIndexChanging="DG_TrialLetterEmailed_PageIndexChanging"
                                                OnRowDataBound="DG_TrialLetterEmailed_RowDataBound" AllowSorting="true" PageSize="20"
                                                Width="100%" OnSorting="DG_TrialLetterEmailed_Sorting">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("ticketid_pk") %>' />
                                                            <asp:HyperLink ID="hp_sno" CssClass="GridItemStyle" runat="server" Text='<%# Eval("SNO") %>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="ClientName" HeaderText="<u>Name</u>">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hl_clientname" CssClass="GridItemStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ClientName")%>'></asp:HyperLink>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email">
                                                        <ItemTemplate>
                                                            <asp:Label ID="HLCauseno" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'>
                                                            </asp:Label>
                                                            <%--<asp:HyperLink ID="HLCauseno" CssClass="GridItemStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email")%>'></asp:HyperLink>--%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="CourtLoc" HeaderText="<u>Court</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_courtloc" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CourtLoc") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="courtdatemain" HeaderText="<u>Court Setting</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_courtdate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.VerifiedCourtInfo") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="TrialLetterEmailedDate" HeaderText="<u>Date</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_emaildate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.TrialLetterEmailedDate") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="ConfirmationDate" HeaderText="<u>Read Date</u>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_emailreaddate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ConfirmationDate") %>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Image ID="img_status" runat="server" ImageUrl="../Images/right.gif" />
                                                            <asp:HiddenField ID="hf_emailconfirmation" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.confirmationid") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;"
                                                    Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous" />
                                            </asp:GridView>
                                            <asp:GridView ID="gv_Data" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable"
                                                Width="100%" OnPageIndexChanging="gv_Data_PageIndexChanging" AllowPaging="True"
                                                AllowSorting="False" PageSize="20">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="S#">
                                                        <ItemTemplate>
                                                            <asp:Label ID="hlnk_SNo" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Client Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ClientName" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ClientName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email Address">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_EmailAddress" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.emailid") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="City">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_City" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_EmailDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ThankYouEmailSentDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Read Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ReadDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.ThankYouEmailReadDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Requested Consultation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_RequestConsultation" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.RequestedConsultation") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                    FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <aspnew:PostBackTrigger ControlID="btnSearch" />
                                            <aspnew:PostBackTrigger ControlID="ddlEmailType" />
                                        </Triggers>
                                    </aspnew:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="Table6" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td width="100%" background="../images/separator_repeat.gif" colspan="5" height="11">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </td>
                </tr>
        </table>
    </div>
    </form>
</body>
</html>

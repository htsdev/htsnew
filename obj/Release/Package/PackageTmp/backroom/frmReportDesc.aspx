<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.frmReportDesc" Codebehind="frmReportDesc.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Stored Procedure List</title>
		<%--<meta content="False" name="vs_snapToGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

		<SCRIPT src="SqlSrvMgr.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		
		// Validate dategrid (DG_Output) controls
		
		
				function ValidateInput()
				{
					var error1 = "Invalid Date";
					var error2 = "Enter valid Numeric number correspond to the Parameter Type.";
					var error3 = "Invalid Bit value";
					var error4 = "Invalid Money value";
					var error5 = "Invalid decimal value";
					
					
					var lblFooterMsg = document.getElementById("lblFooterMsg");
					var cnt = (document.Form1.TextBox2.value) * 1 ;
					var grdName = "Dg_Output";
					var idx=2;
									
										
				    var ret = DoValidate(); 
				     if(ret==false)
				     {
						return false;
				     }
				
				        if(document.getElementById("ddl_reportcategory").selectedIndex == 0)
						{
						    alert("Please select CategoryID. ");
						    document.getElementById("ddl_reportcategory").focus();
						    return false;
						}
						
						 if(document.getElementById("ddl_usertype").selectedIndex == 0)
						{
						    alert("Please select User Type. ");
						    document.getElementById("ddl_usertype").focus();
						    return false;
						}
						
					for (idx=2; idx < (cnt+2); idx++)
					{
						
						var ctltype = "";
						var defval = "";
						var ddlObjectyType =""; 
						
						if(idx < 10)
						{
						 ctltype = grdName+ "_ctl0" + idx + "_lblParamType";
						 defval = grdName + "_ctl0" + idx +  "_txtParamDef";
						 ddlObjectyType = grdName+ "_ctl0" + idx + "_DDLObjType";
						}
						else
						{
						 ctltype = grdName+ "_ctl" + idx + "_lblParamType";
						 defval = grdName + "_ctl" + idx +  "_txtParamDef";
						 ddlObjectyType = grdName+ "_ctl" + idx + "_DDLObjType";
						}
						
						//fol 2 lines will make TextBox and Label reference of txtParamValue and lblParamType resp.
						
						
						var lblParamType = document.getElementById(ctltype);	
							var txtdefval = document.getElementById(defval);
						var objecttype = document.getElementById(ddlObjectyType); 
												
						if ( (lblParamType.innerText!="datetime") && (objecttype.selectedIndex == 2) )
						{
							//////////////////////////////////////////////////
							alert ("Sorry, You can't select this option for non-date time parameters.");
							objecttype.selectedIndex = 0;
							return false;
						}						
						
						switch(lblParamType.innerText)
						{
							case "int":
							case "numeric":
							case "tinyint":
							case "smallint":
								
								var strInt = txtdefval.value;
								if ( !isNonnegativeInteger(strInt))
								{
									alert("Please Enter valid Numeric number correspond to the Parameter Type.");
									txtdefval.focus();
									return false;
								}
								break;
								
							case "varchar":
							case "nvarchar":
							case "text":
								//return true;
								break;
								
							case "bit":
								var strBool = txtdefval.value;
								if (strBool=='1' || strBool=='0'|| strBool.toLowerCase()=='false' || strBool.toLowerCase()=='true')
								{
									break;
								}
								else
								{
									alert("Please Enter a valid boolean value");
									txtdefval.focus();
									return false;
								}
								break;
			
							case "money":
								var strValue = txtdefval.value;
								if( strValue.charAt(0)=='$' )
								{
									strValue = strValue.substring(1,strValue.length);
									txtdefval.value = strValue;
								}	

								if( !isFloat(strValue) )
								{
									alert("Please Enter a valid value");
									txtdefval.focus();
									return false;
								}	

								break;

							case "decimal":
							case "float":
								var strValue = txtdefval.value;
								if( !isFloat(strValue) )
								{
									alert("Please Enter a valid value");
									txtdefval.focus();
									return false;
								}	
								
								break;

							default:
								//return false;
						}

						
						
						
					}
				}

		
		function Hide()
		{
				
			var cnt = (document.Form1.TextBox2.value) * 1 ;

			cnt= cnt + 2;
			
			var grdName = "Dg_Output";
			var idx=2;
			
			for (idx=2; idx < cnt; idx++)
			{
				var CmdType = "";
				var ObjType = "";
				var Cmdtext = "";
				
				if(idx < 10)
				{
				 CmdType = grdName+ "_ctl0" + idx + "_DDLCmdType";
				 ObjType = grdName+ "_ctl0" + idx + "_DDLObjType";
				 Cmdtext = grdName+ "_ctl0" + idx + "_txtCmd";
				}
				else
				{
				 CmdType = grdName+ "_ctl" + idx + "_DDLCmdType";
				 ObjType = grdName+ "_ctl" + idx + "_DDLObjType";
				 Cmdtext = grdName+ "_ctl" + idx + "_txtCmd";
				}
				
				var ddlCmdType = document.getElementById(CmdType);	
				var ddlObjType = document.getElementById(ObjType);	
				var txtCmd = document.getElementById(Cmdtext);
				
				if (ddlObjType.selectedIndex == 0 || ddlObjType.selectedIndex == 2)
					{
					ddlCmdType.disabled = true;
					txtCmd.disabled = true;
					
					}
				else
					{
					ddlCmdType.disabled = false;
					txtCmd.disabled = false;
					}
					
					
				
			}
		}
		
		


		// Enabling and Disabling Dg_Output(datagrid) Controls(Combobox and textbox) 
		function ShowHide(d1,t1,d2)
		{
			
			var ddl1 = document.getElementById(d1 );
			var ddl2 = document.getElementById(d2 );
			var txt = document.getElementById(t1 );


			if (ddl1.selectedIndex == 0 || ddl1.selectedIndex == 2)
				{
				ddl2.disabled = true;
				txt.disabled = true;
				txt.value = ' ';
				}
			else
				{
				ddl2.disabled = false;
				txt.disabled = false;
				}

		}

		// Hide the table	
		function ShowHidePageNavigation()
		{
		
			var tbl =document.getElementById("tblhide").style;
			var grd = document.getElementById("Dg_Output");
			var grd2 = document.getElementById("DgSqlProcedure");
			var msg = document.getElementById("txtNoParam") ;
			var msg2 = document.getElementById("txtchk") ;
		   if ( msg.value == '1' || msg2.value == '1')
			{
		   		tbl.display='none';		// means hidden  table  
		   	}
		   else
			{
				tbl.display ='block';	// show table
			}
		 }

			// Validating the Controls
    	 function DoValidate()
		{
			var error1 = "Please type Report name where you want to save parameters field.";
			var error2 = "Please type Default Values.";
			var error3 = "Please type SQL query or specify procedure name.";
			var error4 = "Please type Parameter Alias.";
			
					
			var lblFooterMsg = document.getElementById("lblFooterMsg");
			var cnt = (document.Form1.TextBox2.value) * 1 ;

			cnt= cnt + 2;
			
			var grdName = "Dg_Output";
			var idx=2;
		
			var reportname = document.getElementById("txtreportname");
			if (reportname.value == "")
			{
					alert(error1);
					reportname.focus();
					return false;
			} 
			for (idx=2; idx < cnt; idx++)
			{
				var ctltype = "";
				var ctlName = "";
				var ctext = "";
				var calias = "";
				var chkvis = "";
				var defval = "";
				
				if( idx < 10)
				{
				 ctltype = grdName+ "_ctl0" + idx + "_lblParamType";
				 ctlName = grdName+ "_ctl0" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl0" + idx + "_txtCmd";
				 calias = grdName+ "_ctl0" + idx + "_txtParamAlias";
				 chkvis = grdName + "_ctl0" + idx + "_chkVisibility";
				 defval = grdName + "_ctl0" + idx +  "_txtParamDef";
				}
				else
				{
				 ctltype = grdName+ "_ctl" + idx + "_lblParamType";
				 ctlName = grdName+ "_ctl" + idx + "_DDLObjType";
				 ctext = grdName+ "_ctl" + idx + "_txtCmd";
				 calias = grdName+ "_ctl" + idx + "_txtParamAlias";
				 chkvis = grdName + "_ctl" + idx + "_chkVisibility";
				 defval = grdName + "_ctl" + idx +  "_txtParamDef";
				
				}
				
	
				
				var lblParamType = document.getElementById(ctltype);	
				var elemnt = document.getElementById(ctlName);	
				var etext = document.getElementById(ctext);
				var ealias = document.getElementById(calias);
				var echkvis = document.getElementById(chkvis);
				
				if(lblParamType.innerText!='datetime')
				{
					var edefval = document.getElementById(defval);
					if (edefval.value == "")  
					{
				   		alert(error2);
				   		edefval.focus();
				   		return false;
					}
				}  
				
				
				if ((elemnt.selectedIndex == 1  && etext.value == "") )
					{
					alert(error3);
					etext.focus();
					return false;
					}
				if (ealias.value =="")
				{
					alert(error4);
					ealias.focus();
					return false;
				}
			}
		}
		 
		</script>

	</HEAD>

    <body class=" ">
        
        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">
            
                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class="clearfix"></div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">
                                    <header class="panel_header">
                                        <h2 class="title pull-left">Report Description</h2>
                                        <div class="actions panel_actions pull-right">
                	                        <asp:HyperLink id="hlback" runat="server" CssClass="btn btn-primary btn-xs" ForeColor="White" NavigateUrl="frmReports.aspx">Back</asp:HyperLink>
                                        </div>
                                    </header>
                                    <div class="content-body">
                                        <asp:label id="lblmsg" runat="server" Visible="False" CssClass="form-label" ForeColor="Red"></asp:label>

                                        <div class="row">
                                            
                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <asp:label id="Label1" runat="server" CssClass="form-label">Stored Procedure</asp:label>
                                                    <div class="controls">
                                                        <asp:textbox id="txtsp" runat="server" CssClass="form-control" ReadOnly="True"></asp:textbox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <asp:label id="lblreportname" runat="server" Visible="False" CssClass="form-label">Report Name</asp:label>
                                                    <div class="controls">
                                                        <asp:textbox id="txtreportname" runat="server" Visible="False" CssClass="form-control"></asp:textbox>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            
                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <asp:label id="Lblloc" runat="server" Visible="False" CssClass="form-label">Location</asp:label>
                                                    <div class="controls">
                                                        <asp:dropdownlist id="dlistloc" runat="server" Visible="False" CssClass="form-control m-bot15">
													        <asp:ListItem Value="0" Selected="True">ALL</asp:ListItem>
													        <asp:ListItem Value="1">Houston</asp:ListItem>
													        <asp:ListItem Value="2">Dallas</asp:ListItem>
												        </asp:dropdownlist>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <asp:label id="lblreporttype" runat="server" CssClass="form-label">Report Category</asp:label>
                                                    <div class="controls">
                                                        <asp:dropdownlist id="ddl_reportcategory" runat="server" CssClass="form-control m-bot15"></asp:dropdownlist>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            
                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" CssClass="form-label">User Type</asp:Label>
                                                    <div class="controls">
                                                        <asp:dropdownlist id="ddl_usertype" runat="server" CssClass="form-control m-bot15">
                                                            <asp:ListItem Value="0">---- Select User Type ----</asp:ListItem>
                                                            <asp:ListItem Value="1">Primary user</asp:ListItem>
                                                            <asp:ListItem Value="2">Secondary user</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">
                                                <div class="form-group">
                                                    <asp:label id="lblreportdesc" runat="server" Visible="False" CssClass="label" Width="122px">Report Description</asp:label>
                                                    <div class="controls">
                                                        <asp:textbox id="txtreportdesc" runat="server" Visible="False" CssClass="form-control" TextMode="MultiLine"></asp:textbox>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <asp:datagrid id="Dg_Output" runat="server" CssClass="table" CellPadding="0" AutoGenerateColumns="False">
										            <Columns>
											            <asp:TemplateColumn HeaderText="Parameter Name">
												            <ItemTemplate>
													            <asp:Label id="lblParamName" runat="server" CssClass="" Text='<%# DataBinder.Eval(Container, "DataItem.Parameter_name") %>'>
													            </asp:Label>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Parameter Type">
												            <ItemTemplate>
													            <asp:Label id="lblParamType" runat="server" CssClass="" Text='<%# DataBinder.Eval(Container, "DataItem.Type") %>'>
													            </asp:Label>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Parameter Alias">
												            <ItemTemplate>
													            <asp:TextBox id="txtParamAlias" Runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.paramalias") %>'>
													            </asp:TextBox>
												            </ItemTemplate>
												            <EditItemTemplate>
													            Alias
												            </EditItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn Visible="False" HeaderText="Parameter Length">
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Default Value">
												            <ItemTemplate>
													            <asp:TextBox id="txtParamDef" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.defaultval") %>'>
													            </asp:TextBox>
													            <ew:calendarpopup id="dtPicker" runat="server" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
														            Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
														            ControlDisplay="TextBoxImage" EnableHideDropDown="True" Font-Names="Tahoma" ToolTip="Select starting list date"
														            ImageUrl="../Images/calendar.gif">
														            <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
													            </ew:calendarpopup>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Visibile">
												            <ItemTemplate>
													            <asp:CheckBox id="chkVisibility" runat="server" CssClass="form-label" TextAlign="Left" Checked='<%# DataBinder.Eval(Container, "DataItem.colvisibility") %>'>
													            </asp:CheckBox>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Obj Type">
												            <ItemTemplate>
													            <asp:DropDownList id="DDLObjType" runat="server" CssClass="form-control m-bot15">
														            <asp:ListItem Value="1" Selected="True">Text Box</asp:ListItem>
														            <asp:ListItem Value="2">Combo</asp:ListItem>
														            <asp:ListItem Value="3">Calendar</asp:ListItem>
													            </asp:DropDownList>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Cmd Type">
												            <ItemTemplate>
													            <asp:DropDownList id="DDLCmdType" runat="server" CssClass="form-control m-bot15" Enabled="False">
														            <asp:ListItem Value="1" Selected="True">Text</asp:ListItem>
														            <asp:ListItem Value="2">SP</asp:ListItem>
													            </asp:DropDownList>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn HeaderText="Cmd Text">
												            <ItemTemplate>
													            <asp:TextBox id="txtCmd" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.CommandText") %>' Enabled="False" Rows="3">
													            </asp:TextBox>
												            </ItemTemplate>
											            </asp:TemplateColumn>
											            <asp:TemplateColumn Visible="False" HeaderText="DetailID">
												            <ItemTemplate>
													            <asp:TextBox id="txt_DetailID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SpDetailID") %>'></asp:TextBox>
												            </ItemTemplate>
											            </asp:TemplateColumn>
										            </Columns>
										            <PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
									            </asp:datagrid>
                                            </div>
                                        </div>

                                        <div class="row">
                                            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:button id="btnReset" runat="server" CssClass="btn btn-danger pull-right" Text="Reset"></asp:button>&nbsp;
                                                        <asp:button id="btnupdate" runat="server" CssClass="btn btn-primary pull-right" Text="Update"></asp:button>&nbsp;
									                    <asp:button id="btnsubmit" runat="server" CssClass="btn btn-primary pull-right" Text="Save"  ></asp:button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <table>
                                            <TR>
								                <TD style="VISIBILITY: hidden; HEIGHT: 20px" align="right"><asp:textbox id="txtchksp" runat="server" Width="50px"></asp:textbox><asp:textbox id="txtRptID" runat="server" Width="50px"></asp:textbox><asp:textbox id="txtSpName" runat="server" Visible="False" Width="50px"></asp:textbox><asp:textbox id="txtchk" runat="server" Width="50px">0</asp:textbox><asp:textbox id="txtNoParam" runat="server">0</asp:textbox><asp:textbox id="TextBox2" runat="server"></asp:textbox></TD>
							                </TR>
                                        </table>

                                    </div>
                                </section></div>

                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                </section>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

    </body>    


















	<%--<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" >
		<form id="Form1" method="post" runat="server">
			<table id="tblMain" borderColor="#e4e2e2" cellSpacing="0" cellPadding="0" width="764" align="center"
				border="0">
				<tr>
					<td vAlign="top">
						<!-- For Header -->
						<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<TR>
					<TD vAlign="middle" align="left"><STRONG class="clsmainhead"></STRONG></TD>
				</TR>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TR>
								<TD style="HEIGHT: 11px" align="right" background="../../images/separator_repeat.gif"></TD>
							</TR>
							<TR>
								<TD align="center" width="780"><asp:label id="lblmsg" runat="server" Visible="False" CssClass="cmdlinks" ForeColor="Red"></asp:label>
									<TABLE id="tblhide" cellSpacing="0" cellPadding="0" width="780" border="0">
										<TR>
											<TD class="clssubhead" style="HEIGHT: 33px" vAlign="middle" align="left" width="780"
												background="../../Images/subhead_bg.gif">&nbsp;Report Description</TD>
											<TD class="clssubhead" style="HEIGHT: 33px" vAlign="middle" align="right" width="780"
												background="../../Images/subhead_bg.gif"><asp:HyperLink id="hlback" runat="server" CssClass="clssubhead" Width="30px" BackColor="Transparent"
													NavigateUrl="frmReports.aspx">Back</asp:HyperLink>&nbsp;</TD>
										</TR>
									</TABLE>
									<asp:label id="lblFooterMsg" runat="server" CssClass="cmdlinks" ForeColor="Red"></asp:label></TD>
							</TR>
							<tr>
								<td class="clsleftpaddingtable" style="HEIGHT: 13px">
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD style="WIDTH: 138px" align="left" width="138">&nbsp;
												<asp:label id="Label1" runat="server" CssClass="label" Width="126px">Stored Procedure</asp:label></TD>
											<TD class="clsleftpaddingtable" align="left">&nbsp; &nbsp;<asp:textbox id="txtsp" runat="server" CssClass="clsinputadministration" Width="248px" ReadOnly="True"
													Height="21"></asp:textbox></TD>
										</TR>
									</table>
								</td>
							</tr>
							<tr>
								<TD class="clsleftpaddingtable" vAlign="top" align="left">
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD style="WIDTH: 139px" vAlign="top" align="left" width="139" colSpan="1" rowSpan="1">&nbsp;
												<asp:label id="lblreportname" runat="server" Visible="False" CssClass="label" Width="103px">Report Name</asp:label></TD>
											<TD style="WIDTH: 262px" align="left" width="262">&nbsp; &nbsp;<asp:textbox id="txtreportname" runat="server" Visible="False" CssClass="clsinputadministration"
													Width="248px" Height="21px"></asp:textbox></TD>
											<TD style="WIDTH: 71px" align="left" width="71">&nbsp;
												<asp:label id="Lblloc" runat="server" Visible="False" CssClass="label" Width="39px" Height="8px">Location</asp:label></TD>
											<TD>&nbsp;
												<asp:dropdownlist id="dlistloc" runat="server" Visible="False" CssClass="clsinputcombo" ForeColor="Black"
													Width="96px">
													<asp:ListItem Value="0" Selected="True">ALL</asp:ListItem>
													<asp:ListItem Value="1">Houston</asp:ListItem>
													<asp:ListItem Value="2">Dallas</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
										
										<TR>
											<TD style="WIDTH: 139px; height: 22px;" vAlign="top" align="left" width="139" colSpan="1" rowSpan="1">&nbsp;
												<asp:label id="lblreporttype" runat="server" CssClass="label" Width="103px">Report Category</asp:label></TD>
											
											<TD style="height: 25px">&nbsp;&nbsp;
												<asp:dropdownlist id="ddl_reportcategory" runat="server" CssClass="clsinputcombo" ForeColor="Black"
													Width="248px">
													
												</asp:dropdownlist></TD>
										</TR>
                                        <tr>
                                            <td align="left" colspan="1" rowspan="1" style="width: 139px; height: 22px" valign="top"
                                                width="139">
                                                &nbsp;
                                                <asp:Label ID="Label2" runat="server" CssClass="label" Width="103px">User Type</asp:Label></td>
                                            <td style="height: 25px">
                                                &nbsp;&nbsp;
                                                <asp:dropdownlist id="ddl_usertype" runat="server" CssClass="clsinputcombo" ForeColor="Black"
													Width="248px">
                                                    <asp:ListItem Value="0">---- Select User Type ----</asp:ListItem>
                                                    <asp:ListItem Value="1">Primary user</asp:ListItem>
                                                    <asp:ListItem Value="2">Secondary user</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
										
									</TABLE>
								</TD>
							</tr>
							<tr>
								<TD style="HEIGHT: 41px" vAlign="top" align="left">
									<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="clsleftpaddingtable" style="WIDTH: 139px" vAlign="top" align="left" width="139">&nbsp;
												<asp:label id="lblreportdesc" runat="server" Visible="False" CssClass="label" Width="122px">Report Description</asp:label></TD>
											<TD class="clsleftpaddingtable" vAlign="top">&nbsp;
												<asp:textbox id="txtreportdesc" runat="server" Visible="False" CssClass="clsinputadministration"
													Width="328px" Height="56px" TextMode="MultiLine"></asp:textbox></TD>
										</TR>
									</TABLE>
									<asp:datagrid id="Dg_Output" runat="server" CssClass="clsleftpaddingtable" Width="780px" CellPadding="0"
										AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn HeaderText="Parameter Name">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblParamName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Parameter_name") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Parameter Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lblParamType" runat="server" CssClass="label" Width="79px" Text='<%# DataBinder.Eval(Container, "DataItem.Type") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Parameter Alias">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:TextBox id="txtParamAlias" Runat="server" CssClass="clsinputadministration" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.paramalias") %>'>
													</asp:TextBox>
												</ItemTemplate>
												<EditItemTemplate>
													Alias
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Parameter Length">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Default Value">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:TextBox id="txtParamDef" runat="server" CssClass="clsinputadministration" Text='<%# DataBinder.Eval(Container, "DataItem.defaultval") %>'>
													</asp:TextBox>
													<ew:calendarpopup id="dtPicker" runat="server" Width="60px" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
														Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
														ControlDisplay="TextBoxImage" EnableHideDropDown="True" Font-Names="Tahoma" ToolTip="Select starting list date"
														ImageUrl="../Images/calendar.gif" Font-Size="8pt">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Visibile">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id="chkVisibility" runat="server" CssClass="label" Width="10px" TextAlign="Left" Checked='<%# DataBinder.Eval(Container, "DataItem.colvisibility") %>'>
													</asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Obj Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:DropDownList id="DDLObjType" runat="server" CssClass="clsinputcombo" Width="74px">
														<asp:ListItem Value="1" Selected="True">Text Box</asp:ListItem>
														<asp:ListItem Value="2">Combo</asp:ListItem>
														<asp:ListItem Value="3">Calendar</asp:ListItem>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Cmd Type">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:DropDownList id="DDLCmdType" runat="server" CssClass="clsinputcombo" Width="49px" Enabled="False">
														<asp:ListItem Value="1" Selected="True">Text</asp:ListItem>
														<asp:ListItem Value="2">SP</asp:ListItem>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Cmd Text">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:TextBox id="txtCmd" runat="server" CssClass="clsinputadministration" Width="187px" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.CommandText") %>' Enabled="False" Rows="3">
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="DetailID">
												<ItemTemplate>
													<asp:TextBox id="txt_DetailID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SpDetailID") %>'></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
							    </TD>
							</tr>
							<TR>
								<TD style="HEIGHT: 31px" align="right"><asp:button id="btnReset" runat="server" CssClass="clsbutton" Text="Reset"></asp:button>&nbsp;
									<asp:button id="btnupdate" runat="server" CssClass="clsbutton" Text="Update"></asp:button>&nbsp;
									<asp:button id="btnsubmit" runat="server" CssClass="clsbutton" Text="Save"  ></asp:button></TD>
							</TR>
							<TR>
								<TD style="VISIBILITY: hidden; HEIGHT: 20px" align="right"><asp:textbox id="txtchksp" runat="server" Width="50px"></asp:textbox><asp:textbox id="txtRptID" runat="server" Width="50px"></asp:textbox><asp:textbox id="txtSpName" runat="server" Visible="False" Width="50px"></asp:textbox><asp:textbox id="txtchk" runat="server" Width="50px">0</asp:textbox><asp:textbox id="txtNoParam" runat="server">0</asp:textbox><asp:textbox id="TextBox2" runat="server"></asp:textbox></TD>
							</TR>
							<tr>
							<td width="780" background="../../images/separator_repeat.gif" colSpan="5" height="11"></td>
							</tr>
						</TABLE>
						<uc1:Footer id="Footer1" runat="server"></uc1:Footer>
					</td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<TR>
					<TD></TD>
				</TR>
			</table>
			<script language="javascript"> ShowHidePageNavigation(); Hide();</script>
		</form>
	</body>--%>
</HTML>

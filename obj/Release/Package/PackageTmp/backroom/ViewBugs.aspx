<%@ Page language="c#" Codebehind="ViewBugs.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.backroom.ViewBugs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>ViewBugs</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="Styles.css" type="text/css" rel="stylesheet" />
	</head>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tr><td >
			    <uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
			</td></tr>
			<TR>
								<TD background="../../images/separator_repeat.gif" Height="11" ></TD>
							</TR>
				<TR>
					<TD class="clssubhead" background="../Images/subhead_bg.gif" height="34">
					    <table border="0" cellpadding="0" cellspacing="0" width="100%">
					    <tr>
					    <td class="clssubhead">
					&nbsp;View Bugs
					</td>
					<td class="clssubhead" align="right">
					    <asp:LinkButton ID="lnkback" runat="server" OnClick="lnkback_Click">Trouble Ticket Management</asp:LinkButton>&nbsp;
					</td>
					</tr>
					</table>
					
					</TD>
					
				</TR>
				<tr>
					<td align="center">
						<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
				</Tr>
				<TR>
					<TD vAlign="top" colSpan="2">
						<TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td >
									<asp:DataGrid id="dg_Viewbug" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True"
										AllowSorting="True" OnItemDataBound="dg_Viewbug_ItemDataBound">
										<AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
										<HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader" VerticalAlign="Middle"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn SortExpression="bug_id" HeaderText="id">
												<ItemTemplate>
													<asp:Label id="lblbugid" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.bug_id") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="shortdescription" HeaderText="Short Description">
												<ItemTemplate>
													<asp:Label id="lblshortdesc" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.shortdescription") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="TicketNumber" HeaderText="Ticket Number">
												<ItemTemplate>
													<asp:Label id="lblTicketNo" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.TicketNumber") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="username" HeaderText="UserName">
												<ItemTemplate>
													<asp:Label id="lblusername" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="posteddate" HeaderText="Date">
												<ItemTemplate>
													<asp:Label id=lblDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.posteddate") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="priority_name" HeaderText="Priority">
												<ItemTemplate>
													<asp:Label id=lblpriority runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.priority_name") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Assigned To" SortExpression="DeveloperName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeveloperName"  CssClass="label" runat="server" Text='<%# Bind("DeveloperName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="status_name" HeaderText="Status">
												<ItemTemplate>
													<asp:Label id=lblstatus runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.status_name") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton id="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
									</asp:DataGrid>
								</td>
							</tr>
							<tr>
									<td background="../../images/separator_repeat.gif" colSpan="5" height="11"></td>
								</tr>
								<tr>
									<TD colSpan="5"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
								</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>

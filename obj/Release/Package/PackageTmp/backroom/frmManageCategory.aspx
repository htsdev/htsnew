<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmManageCategory.aspx.cs" Inherits="lntechNew.backroom.frmManageCategory" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Category </title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
    function addItem()
	{	
		
		 if ( document.form1.lstassigned.length == 1 )
		 {
		  document.form1.addtext.value = "";
		 }
		 	
		 if(document.getElementById("ddl_reportcategory").selectedIndex == 0)
		  {
		    alert("Please select Report Category. ");
		    document.getElementById("ddl_reportcategory").focus();
		    return false;
		  }  
		  
		if (document.form1.lstunassigned.selectedIndex == -1)
		{
		 alert("Please select any Report for Unassigned.");
		 document.getElementById("lstunassigned").focus();
		 return false;
		//document.form1.lstunassigned.selectedIndex != 0 && 
//		var txt = document.form1.lstunassigned[document.form1.lstunassigned.selectedIndex].text;
//		var val = document.form1.lstunassigned[document.form1.lstunassigned.selectedIndex].value;		
//		document.form1.lstassigned.options[document.form1.lstassigned.options.length] = new Option(txt, val);
//		document.form1.lstunassigned.options[document.form1.lstunassigned.selectedIndex]=null;				
//		var adtxt=txt+",";
//		var advalue=val+",";
//		document.form1.addtext.value+=adtxt;
//		document.form1.addvalue.value+=advalue;					
		}		
		
	}
	
	function removeItem()
	{		
	    
	     if(document.getElementById("ddl_reportcategory").selectedIndex == 0)
		   {
		    alert("Please select Report Category. ");
		    document.getElementById("ddl_reportcategory").focus();
		    return false;
		   }  
		    	
	    if (document.form1.lstassigned.selectedIndex == -1)
	     {
	       alert("Please select any report for Assigned to Category");
	       document.getElementById("lstassigned").focus();
	       return false; 
	      //&& document.form1.lstassigned.selectedIndex != 0	     
//		    var txt = document.form1.lstassigned[document.form1.lstassigned.selectedIndex].text
//		    var val = document.form1.lstassigned[document.form1.lstassigned.selectedIndex].value		
//		    document.form1.lstunassigned.options[document.form1.lstunassigned.options.length] = new Option(txt, val);
//		    document.form1.lstassigned.options[document.form1.lstassigned.selectedIndex]=null
//		    var Unassigned=val+",";	
//		    document.form1.addUnassignedvalue.value +=Unassigned;    		 
	    }	
	   
	
	}
    </script>


</head>

    <body class=" ">

        <form id="form1" runat="server">
        
            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>
                        
                        <div class="clearfix"></div>

                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">
                                    <header class="panel_header">
                                        <h2 class="title pull-left">Category Manager</h2>
                                        <div class="actions panel_actions pull-right">
                	                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/backroom/frmReportCategory.aspx" CssClass="btn btn-primary btn-xs" ForeColor="White">Go Back to Report Category</asp:HyperLink>
                                        </div>
                                    </header>
                                    <div class="content-body">
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddl_reportcategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_reportcategory_SelectedIndexChanged" CssClass="form-control m-bot15"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;">
                                                <div class="form-group">
                                                    <asp:label id="lblmsg" runat="server" CssClass="form-label" ForeColor="Red" Visible="False" Font-Bold="True"></asp:label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 col-sm-6 col-xs-7">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Un Assigned</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-3 col-xs-4">

                                            </div>
                                            <div class="col-md-5 col-sm-6 col-xs-7">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Assigned</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-6 col-xs-7">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:ListBox ID="lstunassigned" runat="server" CssClass="form-control m-bot15" Height="400px"></asp:ListBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-3 col-xs-4">
                                                <table class="clsleftpaddingtable" cellpadding="0" cellspacing="0" border="0" align="left" style="height: 100px; width: 100%; ">
                                                    <tr>
                                                        <td align="center">                                                           
                                                            <asp:ImageButton ID="imgbtn_unassigned" runat="server" AlternateText="Move Up" ImageUrl="~/Images/Leftarrow.gif"
                                                                ToolTip="Move Selected  Up one level"
                                                                Width="80px" OnClick="imgbtn_unassigned_Click" />
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgbtn_assigned" runat="server" AlternateText="Move Up" ImageUrl="~/Images/rightArrow-crop.gif"
                                                                ToolTip="Move Selected  Up one level"
                                                                Width="80px" BackColor="White" OnClick="imgbtn_assigned_Click" />
                                                        </td>
                                                    </tr>
                                                  </table>
                                            </div>
                                            <div class="col-md-5 col-sm-6 col-xs-7">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:ListBox ID="lstassigned" runat="server" CssClass="form-control m-bot15" Height="400px"></asp:ListBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnsubmit_Click" Visible="False" />
                                                        <input name="addtext" type="hidden" />
                                                        <input name="addvalue" type="hidden" />
                                                        <input name="addUnassignedvalue" type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </section></div>
                        
                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                </section>

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        </body>











































<%--<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 9px" align="left" background="../../images/separator_repeat.gif"></TD>
								</TR>                                
                                <tr>
                                    <td class="clsleftpaddingtable" align="center">
                                                 <asp:DropDownList ID="ddl_reportcategory" runat="server" Width="372px" AutoPostBack="True" OnSelectedIndexChanged="ddl_reportcategory_SelectedIndexChanged">
                                                 </asp:DropDownList></td>
                                </tr>
								
								<tr>
								  <td class="clsleftpaddingtable" style="height:22px;" align="right" valign="top">
                                      <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/backroom/frmReportCategory.aspx">Go Back to Report Category</asp:HyperLink>
                                      &nbsp;
								  </td>
								</tr>
								<TR>
									<TD class="clsleftpaddingtable" style="HEIGHT: 14px">
                                     
                                     <table class="clsleftpaddingtable" cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                                        <tr>
                                            <td align="center" class="clsleftpaddingtable" valign="top" style="width:50%;height:20px; font-weight:bold;">
                                                Un Assigned</td>
                                                <td style="width:10%" align="left"> 
                                             </td>
                                            <td align="center" class="clsleftpaddingtable" valign="top" style="width:50%; height:20px; font-weight:bold;">
                                                Assigned
                                            </td>
                                        </tr>
                                         <tr>
                                             <td class="clsleftpaddingtable" style="width: 40%">
                                                 <asp:ListBox ID="lstunassigned" runat="server" CssClass="clsinputcombo"
                                                     Height="400px" Width="400px">
                                                 </asp:ListBox></td>
                                             <td class="clsleftpaddingtable" style="width:10%"  align="left"> 
                                             
                                                <table class="clsleftpaddingtable" cellpadding="0" cellspacing="0" border="0" align="left" style="height: 100px;  ">
                                                    <tr>
                                                        <td>                                                           
                                                            <asp:ImageButton ID="imgbtn_unassigned" runat="server" AlternateText="Move Up" ImageUrl="~/Images/Leftarrow.gif"
                                                                ToolTip="Move Selected  Up one level"
                                                                Width="30px" OnClick="imgbtn_unassigned_Click" />
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="imgbtn_assigned" runat="server" AlternateText="Move Up" ImageUrl="~/Images/rightArrow-crop.gif"
                                                                ToolTip="Move Selected  Up one level"
                                                                Width="30px" BackColor="White" OnClick="imgbtn_assigned_Click" />
                                                        </td>
                                                    </tr>
                                                  </table>
                                             
                                             </td>
                                             
                                             
                                             <td style="width: 100%" align="left">
                                                 <asp:ListBox ID="lstassigned" runat="server" CssClass="clsinputcombo"
                                                     Height="400px" Width="400px">
                                                 </asp:ListBox></td>
                                         </tr>
                                     </table>
                                                                             
                                    </TD>
								</TR>
								<TR>
									<td class="clsleftpaddingtable" vAlign="top" width="100%" style="height: 15px" align="right">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="clsbutton"
                                            Text="Submit" OnClick="btnsubmit_Click" Visible="False" />
                                        &nbsp; &nbsp;
                                     </td>
								</TR>
								<tr>
                                    <td class="clsleftpaddingtable" align="left" background="../../images/separator_repeat.gif" style="height: 9px">
                                        <input name="addtext" type="hidden" />
                                        <input name="addvalue" type="hidden" />
                                        <input name="addUnassignedvalue" type="hidden" />
                                        </td>
                                </tr>
								<TR>
									<TD align="center" width="800" style="height: 13px"><asp:label id="lblmsg" runat="server" CssClass="cmdlinks" ForeColor="Red" Visible="False" Font-Bold="True"></asp:label></TD>
								</TR>
							</TBODY>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
    </div>
    </form>
</body>--%>
</html>

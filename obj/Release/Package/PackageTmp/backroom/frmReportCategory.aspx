<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmReportCategory.aspx.cs" Inherits="lntechNew.backroom.frmReportCategory" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Report Category</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        function Validate()
        {
            var cat =document.getElementById("txtcategory");
            if( cat.value == "" || cat.length == 0)
            {
                alert("Please Enter Category Name");
                cat.focus();
                return false;
            }
                        
        }
        
        function DeletCategoryFunction()
        {
            var doyou;
            var cat =document.getElementById("txtcategory");
            
            if(cat.value != "" && cat.value.length >0)
            {
                doyou =confirm("Are you sure you want to delete this category ?")
                
                if(doyou ==true)
                    return true;
                 else
                    return false;    
            }
            else             
              return false;             
        }
        
        function SetCategoryName(category,catid)
        {
            var cat =document.getElementById("txtcategory");           
           cat.value = category;
           document.getElementById("hf_categoryid").value =catid;           
           return false;           
        }
    </script>

</head>

<body class=" ">

    <form id="form1" runat="server">

        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
        
            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">

                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Report Category</h1>
                                <!-- PAGE HEADING TAG - END -->                            

                            </div>
                                
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-xs-12">
                        <section class="box ">
                                
                            <div class="content-body">

                                <div class="row">

                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Category Name</label>
                                            <div class="controls">
                                                <asp:textbox id="txtcategory" Runat="server" CssClass="form-control"></asp:textbox>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:button id="btnsubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnsubmit_Click"></asp:button>
                                                <asp:button id="btndelete" runat="server" CssClass="btn btn-primary" Text="Delete" OnClick="btndelete_Click" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-4 col-xs-5">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">&nbsp;</label>
                                            <div class="controls">
                                                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="form-label" NavigateUrl="~/backroom/frmManageCategory.aspx">Category Manager</asp:HyperLink>
                                            </div>
                                        </div>

                                    </div>

                                    <asp:label id="lblNoRec" runat="server" CssClass="cmdlinks" Width="184px" ForeColor="Red" Visible="False"></asp:label>
                                </div>
                                
                                

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:label id="lblmsg" runat="server" CssClass="form-label" ForeColor="Red" Visible="False" Font-Bold="True"></asp:label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>

                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Lists of Report Categories</h2>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <asp:GridView ID="GV_Category" CssClass="table" runat="server" AutoGenerateColumns="False" CellPadding="0" PageSize="20" AllowPaging="true" OnRowDataBound="GV_Category_RowDataBound">
                                            
                                            <Columns>                                                                                               
                                                <asp:TemplateField HeaderText="S.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_sno" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category Name">                                                  
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hfcategoryname" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />                                                        
                                                        <asp:HyperLink ID="hl_categoryname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>' NavigateUrl="#" >HyperLink</asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <asp:HiddenField ID="hf_categoryid" runat="server" />
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>

























<%--<body>
    <form id="form1" runat="server">
    <div>
        <table id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 9px" align="left" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<tr> <!-- tahir   -->
									<td class="clsleftpaddingtable" style="HEIGHT: 25px" vAlign="bottom" align="center"
										width="100%"><STRONG><FONT face="Verdana" color="#0066cc">
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TBODY>
														<TR>
															<TD class="clsleftpaddingtable" style="WIDTH: 95px" width="95"><SPAN class="style2">Category Name:</SPAN></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 126px" width="126"><asp:textbox id="txtcategory" Runat="server" CssClass="FrmTDLetter" Width="300px"></asp:textbox></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 100px" width="160"><asp:button id="btnsubmit" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnsubmit_Click"></asp:button></TD>
															<TD class="clsleftpaddingtable" style="WIDTH: 109px" align="left">&nbsp;<asp:button id="btndelete" runat="server" CssClass="clsbutton" Text="Delete" OnClick="btndelete_Click" />
                                                            </TD>
															<TD class="clsleftpaddingtable" align="right">
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/backroom/frmManageCategory.aspx">Category Manager</asp:HyperLink>&nbsp;</TD>
															<TD class="clsleftpaddingtable" align="right">&nbsp;</TD>
															<TD align="right">&nbsp;</TD>
														</TR>
													</TBODY>
												</TABLE>
											</FONT></STRONG>
										<asp:label id="lblNoRec" runat="server" CssClass="cmdlinks" Width="184px" ForeColor="Red" Visible="False"></asp:label></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 14px" vAlign="bottom" align="center" width="100%" background="../../images/separator_repeat.gif"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 34px" vAlign="middle" align="left" width="100%" background="../../Images/subhead_bg.gif"><STRONG><STRONG class="clssubhead">&nbsp;Lists 
												of Report Categories</STRONG></STRONG></TD>
								</TR>
								<TR>
									<td vAlign="top" width="100%">
                                        <asp:GridView ID="GV_Category" CssClass="clsleftpaddingtable" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="0" PageSize="20" AllowPaging="true" OnRowDataBound="GV_Category_RowDataBound">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle" />
                                            <FooterStyle CssClass="GrdFooter" />
                                            <Columns>                                                                                               
                                                <asp:TemplateField HeaderText="S.No">
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="clsaspcolumnheader" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_sno" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category Name">   
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="clsaspcolumnheader" />                                                 
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hfcategoryname" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.CategoryID") %>' />                                                        
                                                        <asp:HyperLink ID="hl_categoryname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>' NavigateUrl="#" >HyperLink</asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:GridView>
                                    </td>
								</TR>
								<TR>
									<TD align="center" width="800"><asp:label id="lblmsg" runat="server" CssClass="cmdlinks" ForeColor="Red" Visible="False" Font-Bold="True"></asp:label></TD>
								</TR>
							</TBODY>
						</TABLE>
                        <asp:HiddenField ID="hf_categoryid" runat="server" />
					</td>
				</tr>
				<tr>
					<td><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
				</tr>
			</table>
    </div>
    </form>
</body>--%>
</html>

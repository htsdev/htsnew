﻿<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.backroom.TemplateMain" Codebehind="TemplateMain.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Templates</title>
		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="../Styles.css" type="text/css" rel="stylesheet"/>--%>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta cont  nt="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


        <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
	</head>

    <body class=" ">
        
        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">

                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Pricing Template</h1>
                                <!-- PAGE HEADING TAG - END -->                            

                            </div>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <!-- MAIN CONTENT AREA STARTS -->

                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Templates</h2>
                                <div class="actions panel_actions pull-right">
                                    <a href="frmTemplateDetail.aspx" class="btn btn-primary" style="color: white">Add New Template&nbsp;</a>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <asp:label id="lblMessage" runat="server" CssClass="form-label" ForeColor="Red"></asp:label>
                                        <asp:datagrid id="dg_template" runat="server" CssClass="table" AutoGenerateColumns="False" PageSize="20" AllowPaging="True">
											<Columns>
												<asp:TemplateColumn Visible="False" HeaderText="TemplateID">
													<ItemTemplate>
														<asp:Label id="lblTemplateID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateID") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Name">
													<ItemTemplate>
														<asp:HyperLink id="hlnkPriceTemplates" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateName") %>'>
														</asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Description">
													<ItemTemplate>
														<asp:Label id="lblTemplateDesc" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateDescription") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
											<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
										</asp:datagrid>

                                        <%--<table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Username</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>--%>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
    
                    <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                    </section>
                <!-- END CONTENT -->            

            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 


        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Body goes here...

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
        </body>



















	<%--<body >
		<form id="Form1" method="post" runat="server">
			<table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
				<tr>
					<td>
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></td>
				</tr>
				<tr>
					<td background="../../images/separator_repeat.gif" height="11"></td>
				</tr>
				<tr>
					<td colspan="2">
						<table id="tableSub" align="center" cellspacing="0" cellpadding="0"
							width="780" border="0">

							<tr>
								<td class="clssubhead" background="../../Images/subhead_bg.gif" height="34">&nbsp;Templates</td>
								<td class="clssubhead" align="right" background="../../Images/subhead_bg.gif"
									height="25"><A href="frmTemplateDetail.aspx">Add New Template&nbsp;</A>&nbsp;</td>
							</tr>
							
							<tr>
								<td colspan="4">
									<table id="tblTemplate" cellspacing="0" cellpadding="0" width="100%" border="0">
										<tr>
											<td><asp:datagrid id="dg_template" runat="server" Width="100%" CssClass="clsLeftPaddingTable" AutoGenerateColumns="False"
													PageSize="20" AllowPaging="True">
													<Columns>
														<asp:TemplateColumn Visible="False" HeaderText="TemplateID">
															<ItemTemplate>
																<asp:Label id="lblTemplateID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateID") %>'>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Name">
															<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
															<ItemTemplate>
																<asp:HyperLink id="hlnkPriceTemplates" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateName") %>'>
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Description">
															<HeaderStyle CssClass="clsaspcolumnheader"></HeaderStyle>
															<ItemTemplate>
																<asp:Label id="lblTemplateDesc" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.TemplateDescription") %>'>
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" HorizontalAlign="Center"></PagerStyle>
												</asp:datagrid></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
							    <td colspan="6">
							        <asp:label id="lblMessage" runat="server"  ForeColor="Red"></asp:label>
							    </td>
							</tr>
							<tr>
								<td background="../../images/separator_repeat.gif" colspan="7" height="11"></td>
							</tr>
							<tr>
								<td colspan="6"><uc1:footer id="Footer1" runat="server"></uc1:footer></td>
							</tr>
							
						</table>						
					</td>
				</tr>
			</table>
		</form>
	</body>--%>
</html>

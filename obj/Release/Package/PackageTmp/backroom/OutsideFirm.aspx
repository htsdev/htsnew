﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.backroom.OutsideFirm"
    CodeBehind="OutsideFirm.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Outside Firms</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />


    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

<%--    <link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <script language="javascript">
    
    // tahir 5084 11/06/2008
    function ShowHideRow()
    {
    var cb = document.getElementById("cb_textmessage");
    var row = document.getElementById("tr_textmessage");
    
    if (cb.checked)
        {
        row.style.display = "block";
        //document.getElementById("txtCellNumber").value = "";
        //document.getElementById("txtNoticeDays").value = "";        
        }
    else if (!cb.checked)
        {
        row.style.display = "none";
        //document.getElementById("txtCellNumber").value = "";
        //document.getElementById("txtNoticeDays").value = "0";
        }
   
    }
    // end 5084
    
    //Waqas 5864 06/30/2009
    function trimAll(sString) 
	{
		while (sString.substring(0,1) == ' ')
		{
			sString = sString.substring(1, sString.length);
		}
		while (sString.substring(sString.length-1, sString.length) == ' ')
		{
			sString = sString.substring(0,sString.length-1);
		}
		return sString;
	}
    var whitespace = " \t\n\r";
       function isWhitespace (s)
       {   
        var i;
        if (isEmpty(s)) return true;
        for (i = 0; i < s.length; i++)
        {          
            var c = s.charAt(i);
            if (whitespace.indexOf(c) == -1) return false;
        }

        return true;
       }
    function isEmpty(s)
    {   
        return ((s == null) || (s.length == 0))
    }
		
    function isEmail (s)
        {   
        
        if (isEmpty(s)) 
               if (isEmail.arguments.length == 1) return false;
               else return (isEmail.arguments[1] == true);
           
            
            if (isWhitespace(s)) return false;
               
            
            
            var i = 1;
            var sLength = s.length;

            
            while ((i < sLength) && (s.charAt(i) != "@"))
            { i++
            }

            if ((i >= sLength) || (s.charAt(i) != "@")) return false;
            else i += 2;

            
            while ((i < sLength) && (s.charAt(i) != "."))
            { i++
            }

            
            if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
            else return true;
        }

		function ValidateControls()
		 {  
			
			if (document.frmfirm.txt_fname.value == "")
			{
				alert("Please specify Firm Name");
				document.frmfirm.txt_fname.focus();
				return false;
			}	
		

			if (frmfirm.txt_sname.value == "")
			{
				alert("Please specify Firm Short Name");
				frmfirm.txt_sname.focus();
				return false;
			}

			if (frmfirm.txt_add1.value == "")
			{
				alert("Please specify Firm Address");
				frmfirm.txt_add1.focus();			
				return false;
			}


			if (frmfirm.txt_city.value == "")
			{
				alert("Please specify Court City");
				frmfirm.txt_city.focus();
				return false;
			}


			if (frmfirm.ddl_state.value == 0)
			{
				alert("Please specify Court State");
				frmfirm.ddl_state.focus();		
				return false;
			}


			if (frmfirm.txt_zip.value == "")
			{
				alert("Please specify Court Zip");
				frmfirm.ddl_state.focus();		
				return false;
			}

			if (ValidationInteger(frmfirm.txt_zip) == false )
			{
				alert("Sorry, Invalid Court Zip Code");
				frmfirm.txt_zip.focus();		
				return false;
			}
			
			//Waqas 5864 06/30/2009 Attorney information for ALR
			if (frmfirm.txt_AttFirstName.value == "")
			{
				alert("Please specify Attorney First Name");
				frmfirm.txt_AttFirstName.focus();		
				return false;
			}

			if (frmfirm.txt_AttLastName.value == "")
			{
				alert("Please specify Attorney Last Name");
				frmfirm.txt_AttLastName.focus();		
				return false;
			}
			
			if (frmfirm.txt_AttBarNumber.value == "")
			{
				alert("Please specify Attorney Bar Number");
				frmfirm.txt_AttBarNumber.focus();		
				return false;
            }

            if (isNaN(frmfirm.txt_AttBarNumber.value) == true) {
                alert("Please specify attorney bar number in numeric format");
                frmfirm.txt_AttBarNumber.focus();
                return false;
            }

            var BarNum = frmfirm.txt_AttBarNumber.value;
            if (BarNum.charAt(0) == "+" || BarNum.charAt(0) == "-") {
                alert("Please specify only numbers in attorney bar number");
                frmfirm.txt_AttBarNumber.focus();
                return false;
            }
            
			if (frmfirm.txt_AttEmailAddress.value == "")
			{
				alert("Please specify Attorney Email Address");
				frmfirm.txt_AttEmailAddress.focus();		
				return false;
			}
            
            if(trimAll(frmfirm.txt_AttEmailAddress.value) == "")
            { 
	            alert("Please specify EmailAddress");
	            frmfirm.txt_AttEmailAddress.focus();
	            return false;
            }
            else
            { 
	           if(isEmail(frmfirm.txt_AttEmailAddress.value)== false)
			   {
			       alert ("Please enter Email Address in Correct format.");
			       frmfirm.txt_AttEmailAddress.focus(); 
			       return false;			   
			   }
            }
			
			if (ValidationInteger(frmfirm.txt_basefee) ==false )
			{
				alert("Sorry, Invalid Base Fee Amount");
				frmfirm.txt_basefee.focus();		
				return false;
			}

			if (ValidationInteger(frmfirm.txt_daybeforefee) == false ) 
			{
				alert("Sorry, Invalid Day Before Fee Amount");
				frmfirm.txt_daybeforefee.focus();		
				return false;
			}


			if (ValidationInteger(frmfirm.txt_juryfee) == false ) 
			{
				alert("Sorry, Invalid Judge Fee Amount");
				frmfirm.txt_juryfee.focus()		
				return false;
			}
			
					
			if (  isNaN(document.frmfirm.txt_cc11.value)==true || isNaN(document.frmfirm.txt_cc12.value)==true ||isNaN(document.frmfirm.txt_cc13.value)==true)	
			{
						alert ("Sorry Invalid Phone Number. Phone Number should be like 713-389-9026");			  
						document.frmfirm.txt_cc11.focus();
						return false;	
			}
		
		    // tahir 5084 11/06/2008 
			if (document.frmfirm.cb_textmessage.checked)
			{
			    if (trim(document.frmfirm.txtCellNumber.value) == "")
			    {
						alert ("Please enter text messaging/pager number for the firm.");			  
						document.frmfirm.txtCellNumber.focus();
						return false;	
			    }
//			   if (trim(document.frmfirm.txtNoticeDays.value) == "")
//			    {
//						alert ("Please enter notice period days.");			  
//						document.frmfirm.txtNoticeDays.focus();
//						return false;	
//			    }
//			    
//			   if ( isNaN(document.frmfirm.txtNoticeDays.value) )
//			    {
//						alert ("Please enter a numeric value for notice period days.");			  
//						document.frmfirm.txtNoticeDays.focus();
//						return false;	
//			    }
			    
			}
			// end 5084
			
	}	
		function Clear()
		{
			frmfirm.txt_fname.value ="";
			frmfirm.txt_sname.value="";
			frmfirm.txt_add1.value="";
			frmfirm.txt_add2.value="";
			frmfirm.txt_city.value="";
			frmfirm.ddl_state.selectedIndex=0;
			frmfirm.txt_zip.value="";
			frmfirm.txt_basefee.value="";
			frmfirm.txt_daybeforefee.value="";
			frmfirm.txt_juryfee.value="";
			frmfirm.txt_firmid.value=0;
			frmfirm.txt_cc11.value = "";
            frmfirm.txt_cc12.value = "";
            frmfirm.txt_cc13.value = "";
            frmfirm.txt_cc14.value = "";  
			document.getElementById("lbl_message").innerText="";
		    frmfirm.cb_coverfrom.checked = false;
		    frmfirm.cb_coverby.checked = false;
		    frmfirm.cb_textmessage.checked = false;
		    frmfirm.txtCellNumber.value = "";
		    //Sabir Khan 5298 12/03/2008 set default sender address...
		    frmfirm.txtFromnumber.value = "info@sullolaw.com";
		    //frmfirm.txtNoticeDays.value = "";
		    
			//document.getElementById("chk_payroll").checked=false;
			//document.getElementById("txtEmail").value="";
			
			//Kamran 3580 04/2/08 remove year bug
			
			//Waqas 5864 06/30/3009 ALR Changes
			frmfirm.txt_AttFirstName.value = "";
			frmfirm.txt_AttLastName.value = "";
			frmfirm.txt_AttBarNumber.value = "";
			frmfirm.txt_AttEmailAddress.value = "";
			//Yasir Kamal 7150 01/01/2010 display signature image
			frmfirm.SignatureImage.src = "";
			frmfirm.SignatureImage.style.display = 'none';
		}
		//Sabir Khan 4990 11/13/2008 set check box option...
		function IsCheck(check)
        {
            var chkbond=document.getElementById("chkInactive");
            var chkReguler=document.getElementById("chkActiveFirm");             
            if(chkbond.checked==false && check=="1")        
                chkReguler.checked=true;            
            if(chkReguler.checked==false && check=="2")
                chkbond.checked=true;
       
    }
		
		//FOR INTEGER VALIDATION
		function ValidationInteger(TextBox)
		{
				var error = "Please correct value. Only Positive Integer value is allowed";
				//var error = "Please correct value. Only Whole number is allowed";
				

				if(TextBox.value=="")
				{
					TextBox.value = 0;
					return true;
				}
				else
				{
/*					if (isInteger(TextBox.value)==false) 
					{
						//if -ve and not float
						return false;
					}
					else
					{
						
						return true;
					}
*/					
					var anum=/(^\d+$)|(^\d+\d+$)/
					return (anum.test(TextBox.value)) ;
				}
				
		}

    </script>

    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
</head>

<body class=" ">
    <form id="frmfirm" method="post" runat="server">

        <!-- START CONTAINER -->
        <table id="TableMAin" width="100%">
            <tr>
                <td>
                    <div class="page-container row-fluid container-fluid">

                        <asp:Panel ID="pnl" runat="server">
                            <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                        </asp:Panel>

                        <!-- START CONTENT -->
                        <section id="main-content" class=" ">
                            <section class="wrapper main-wrapper row" style=''>

                                <div class='col-xs-12'>
                                    <div class="page-title">

                                        <div class="pull-left">
                                            <!-- PAGE HEADING TAG - START -->
                                            <h1 class="title">Outside Firm</h1>
                                            <!-- PAGE HEADING TAG - END -->   
                                        </div>     
                                
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <!-- MAIN CONTENT AREA STARTS -->
    
                                <div class="col-xs-12">
                                    <section class="box ">
                                            
                                        <div class="content-body">

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Firm Name</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_fname" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Short Name</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_sname" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Address #1</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_add1" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Address #2</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_add2" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-2 col-sm-3 col-xs-4">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">City</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_city" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-2 col-sm-3 col-xs-4">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">State</label>
                                                        <div class="controls">
                                                            <asp:DropDownList ID="ddl_state" runat="server" CssClass="form-control m-bot15">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-2 col-sm-3 col-xs-4">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Zip</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_zip" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Phone
                                                        </label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_cc11" runat="server" CssClass="form-control pull-left" MaxLength="3" onkeyup="return autoTab(this, 3, event)" style="width: 24%;"></asp:TextBox>
                                                            <asp:TextBox ID="txt_cc12" runat="server" CssClass="form-control pull-left" MaxLength="3" onkeyup="return autoTab(this, 3, event)" style="width: 24%;"></asp:TextBox>
                                                            <asp:TextBox ID="txt_cc13" runat="server" CssClass="form-control pull-left" MaxLength="4" onkeyup="return autoTab(this, 4, event)" style="width: 24%;"></asp:TextBox>
                                                            <p class="pull-left">x</p>
                                                            <asp:TextBox ID="txt_cc14" runat="server" CssClass="form-control pull-left" onkeyup="return autoTab(this, 4, event)" style="width: 25%;"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Attorney First Name</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_AttFirstName" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Attorney Last Name</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_AttLastName" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Attorney Bar Number</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_AttBarNumber" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Attorney Email Address</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_AttEmailAddress" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">
                                                    <div class="col-md-6 col-sm-7 col-xs-8">
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1">Base Fee</label>
                                                            <div class="controls">
                                                                <asp:TextBox ID="txt_basefee" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-8">
                                                        <div class="form-group">
                                                            <label class="form-label" for="field-1">Day Before Fee: $</label>
                                                            <div class="controls">
                                                            
                                                            
                                                                <asp:TextBox ID="txt_daybeforefee" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Judge/Jury Fee</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_juryfee" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-4 col-sm-5 col-xs-6">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">OutSide Firm Client</label>
                                                        <span class="desc">(Somebody else’s client)</span>
                                                        <div class="controls">
                                                            <asp:CheckBox ID="cb_coverfrom" runat="server" CssClass="form-label" />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-4 col-sm-5 col-xs-6">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Firms covering cases for us</label>
                                                        <div class="controls">
                                                            <asp:CheckBox ID="cb_coverby" runat="server" CssClass="form-label" />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-4 col-sm-5 col-xs-6">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Send text message?</label>
                                                        <div class="controls">
                                                            <asp:CheckBox ID="cb_textmessage" runat="server" CssClass="form-label" onclick="ShowHideRow();" />
                                                        </div>
                                                    </div>

                                                </div>

                                                

                                            </div>

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <asp:Label  runat="server" ID="lblUpload" Text="Upload Signature Image:" CssClass="form-label"></asp:Label>
                                                        <div class="controls">
                                                            <asp:FileUpload CssClass="clsInputadministration" Width="254px" ID="ImageUpload" runat="server" />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <asp:Image Width="150px" Height="150px" style="display:none" ID="SignatureImage" runat="server" />
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <table style="width: 100%">
                                                <tr runat="server" id="tr_textmessage" style="display:none">
                                                    <td>
                                                        <div class="row">

                                                                        <div class="col-md-4 col-sm-5 col-xs-6">

                                                                            <div class="form-group">
                                                                                <label class="form-label" for="field-1">To</label>
                                                                                <div class="controls">
                                                                                    <asp:TextBox ID="txtCellNumber" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        <div class="col-md-4 col-sm-5 col-xs-6">

                                                                            <div class="form-group">
                                                                                <label class="form-label" for="field-1">From</label>
                                                                                <div class="controls">
                                                                                    <asp:TextBox ID="txtFromnumber" runat="server" CssClass="form-control" MaxLength="50">info@sullolaw.com</asp:TextBox>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        <div class="col-md-4 col-sm-5 col-xs-6">

                                                                            <div class="form-group">
                                                                                <label class="form-label" for="field-1">Notice Period Days</label>
                                                                                <div class="controls">
                                                                                    <asp:DropDownList ID="ddlNoticeDays" runat="server" CssClass="form-control m-bot15">
                                                                                        <asp:ListItem>0</asp:ListItem>
                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                        <asp:ListItem Selected="True">2</asp:ListItem>
                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                        <asp:ListItem>6</asp:ListItem>
                                                                                        <asp:ListItem>7</asp:ListItem>
                                                                                        <asp:ListItem>8</asp:ListItem>
                                                                                        <asp:ListItem>9</asp:ListItem>
                                                                                        <asp:ListItem>10</asp:ListItem>
                                                                                        <asp:ListItem Value="-1">Always</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                        <%--<table runat="server" id="Table1" style="display: block; width: 100%">
                                                            <tr>
                                                                <td>
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </td>
                                                </tr>
                                            </table>

                                            

                                            <div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <asp:Label ID="lbl_message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                                    </div>

                                                </div>

                                            </div>

                                            <%--<div class="row">

                                                <div class="col-md-6 col-sm-7 col-xs-8">

                                                    <div class="form-group">
                                                        <label class="form-label" for="field-1">Firm Name</label>
                                                        <div class="controls">
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>--%>

                                            <div class="row">
                                                <div class="col-xs-12">

                                                    <div class="form-group">
                                                        <div class="controls pull-right">
                                                            <input class="btn btn-primary" id="btn_clear" onclick="Clear();" type="button" value="Clear">
                                                            <asp:Button ID="btn_Submit" runat="server" CssClass="btn btn-primary" Text="Submit"></asp:Button>&nbsp;
                                                            <asp:Button ID="btn_delete" runat="server" CssClass="btn btn-primary" Text="Delete"></asp:Button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </section>

                                </div>

                                <table id="tblgrid" width="100%">
                                    <tr>
                                        <td>
                                            <div class="col-lg-12">
                                                <section class="box ">
                                                    <header class="panel_header">
                                                        
                                                        <div class="actions panel_actions pull-right">
                                                            <asp:CheckBox ID="chkActiveFirm" runat="server" Checked="true" CssClass="form-label" Text="Active" AutoPostBack="true" OnCheckedChanged="chkActiveFirm_CheckedChanged" />
                                                            <asp:CheckBox ID="chkInactive" runat="server" Checked="false" CssClass="form-label" Text="Inactive" AutoPostBack="true" OnCheckedChanged="chkInactive_CheckedChanged" />
                                                        </div>
                                                    </header>
                                                    <div class="content-body">
                                                        <div class="row">
                                                            <div class="col-xs-12">

                                                                <asp:DataGrid ID="dgResult" runat="server" CssClass="table" PageSize="20" AutoGenerateColumns="False" AllowSorting="True">
                                                                    <Columns>
                                                                        <asp:TemplateColumn SortExpression="firmname">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkbtnFirmName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firmname") %>'
                                                                                    CommandName="FNameClicked">
                                                                                </asp:LinkButton>
                                                                                <asp:Label ID="lbl_FirmID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.firmid") %>'
                                                                                    Visible="False">
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkFirmNameH" runat="server" CommandName="firmname">Firm Name</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="ShortName">
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkShortNameH" runat="server" CommandName="FirmAbbreviation">Short Name</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblShortName" runat="server" CssClass="form-label" Text='<%# Eval("FirmAbbreviation") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Base Fee">
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkBaseFeeH" runat="server" CommandName="basefee">Base Fee</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBaseFee" runat="server" Text='<%# Eval("basefee","{0:C0}") %>' CssClass="form-label"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Day Before Fee">
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkDayBeforeFeeH" runat="server" CommandName="daybeforefee">Day Before Fee</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDayBeforeFee" runat="server" CssClass="form-label" Text='<%# Eval("daybeforefee","{0:C0}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Judge/Jury Fee">
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkJudgeFeeH" runat="server" CommandName="judgefee">Judge/Jury Fee</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblJudgeFee" runat="server" CssClass="form-label" Text='<%# Eval("judgefee","{0:C0}") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Cover From">
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkCoverFromH" runat="server" CommandName="coverfrom">Cover From</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCoverFrom" runat="server" CssClass="form-label" Text='<%# Eval("coverfrom") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Cover By">
                                                                            <HeaderTemplate>
                                                                                <asp:LinkButton ID="lnkCoverByH" runat="server" CommandName="coverby">Cover By</asp:LinkButton>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCoverBy" runat="server" CssClass="form-label" Text='<%# Eval("coverby") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="display: none" align="right">
                                            <asp:TextBox ID="txt_firmid" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>

                                <!-- MAIN CONTENT AREA ENDS -->
                            </section>
                        </section>
                        <!-- END CONTENT -->

                    </div>
                </td>
            </tr>
        </table>
        
        <!-- END CONTAINER -->

    </form>

    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>



















<%--<body ms_positioning="GridLayout" onload="ShowHideRow();">
    <form id="frmfirm" method="post" runat="server">
    <table id="TableMain" style="z-index: 101" cellspacing="0" cellpadding="0" width="780"
        align="center" border="0">
        <tr>
            <td>
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </td>
        </tr>
        <tr>
            <td width="780" background="../../images/separator_repeat.gif" colspan="5" height="11">
            </td>
        </tr>
        <tr>
            <td style="height: 228px">
                <table class="clsleftpaddingtable" width="100%" border="1" bordercolor="white">
                    <tr class="clsleftpaddingtable">
                        <td style="width: 120px" class="clsleftpaddingtable">
                            Firm Name:
                        </td>
                        <td width="210">
                            <asp:TextBox ID="txt_fname" runat="server" Width="205px" CssClass="clsinputadministration"
                                MaxLength="30"></asp:TextBox>
                        </td>
                        <td width="210" class="clsleftpaddingtable" style="width: 140px">
                            Short Name:
                        </td>
                        <td width="210">
                            &nbsp;<asp:TextBox ID="txt_sname" runat="server" Width="204px" CssClass="clsinputadministration"
                                MaxLength="6"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 100px; height: 25px;" class="clsleftpaddingtable">
                            Address #1:
                        </td>
                        <td style="width: 215px; height: 25px;">
                            <asp:TextBox ID="txt_add1" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="30"></asp:TextBox>
                        </td>
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Address #2:
                        </td>
                        <td style="width: 215px; height: 25px">
                            &nbsp;<asp:TextBox ID="txt_add2" runat="server" Width="204px" CssClass="clsinputadministration"
                                MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 100px; height: 23px" class="clsleftpaddingtable">
                            City, State, Zip:
                        </td>
                        <td style="width: 215px; height: 23px">
                            <asp:TextBox ID="txt_city" runat="server" Width="80px" CssClass="clsinputadministration"
                                MaxLength="20" Height="19px"></asp:TextBox>&nbsp;<asp:DropDownList ID="ddl_state"
                                    runat="server" Width="48px" CssClass="clsinputcombo">
                                </asp:DropDownList>
                            <asp:TextBox ID="txt_zip" runat="server" Width="70px" CssClass="clsinputadministration"
                                MaxLength="10" Height="19px"></asp:TextBox>
                        </td>
                        <td style="width: 140px;" class="clsleftpaddingtable">
                            Phone:
                        </td>
                        <td style="width: 215px; height: 23px">
                            &nbsp;<asp:TextBox ID="txt_cc11" runat="server" CssClass="clsinputadministration"
                                MaxLength="3" onkeyup="return autoTab(this, 3, event)" Width="38px"></asp:TextBox>
                            <asp:TextBox ID="txt_cc12" runat="server" CssClass="clsinputadministration" MaxLength="3"
                                onkeyup="return autoTab(this, 3, event)" Width="43px"></asp:TextBox>
                            <asp:TextBox ID="txt_cc13" runat="server" CssClass="clsinputadministration" MaxLength="4"
                                onkeyup="return autoTab(this, 4, event)" Width="58px"></asp:TextBox>
                            x
                            <asp:TextBox ID="txt_cc14" runat="server" CssClass="clsinputadministration" onkeyup="return autoTab(this, 4, event)"
                                Width="43px"></asp:TextBox>
                        </td>
                    </tr>
                    
                    <tr class="clsleftpaddingtable">
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Attorney First Name:
                        </td>
                        <td style="width: 215px; height: 25px;">
                            <asp:TextBox ID="txt_AttFirstName" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="20"></asp:TextBox>
                        </td>
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Attorney Last Name:
                        </td>
                        <td style="width: 215px; height: 25px">
                            &nbsp;<asp:TextBox ID="txt_AttLastName" runat="server" Width="204px" CssClass="clsinputadministration"
                                MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Attorney Bar Number:
                        </td>
                        <td style="width: 215px; height: 25px;">
                            <asp:TextBox ID="txt_AttBarNumber" runat="server" Width="206px" CssClass="clsinputadministration"
                                MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 140px; height: 25px;" class="clsleftpaddingtable">
                            Attorney Email Address:
                        </td>
                        <td style="width: 215px; height: 25px">
                            &nbsp;<asp:TextBox ID="txt_AttEmailAddress" runat="server" Width="204px" CssClass="clsinputadministration"
                                MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="width: 100px; height: 24px" class="clsleftpaddingtable">
                            Base Fee:&nbsp;
                        </td>
                        <td style="width: 215px; height: 24px" class="clsleftpaddingtable">
                            $&nbsp;
                            <asp:TextBox ID="txt_basefee" runat="server" Width="40px" CssClass="clsinputadministration"
                                MaxLength="5"></asp:TextBox>
                            &nbsp;&nbsp;Day Before Fee:&nbsp;&nbsp;&nbsp;$
                            <asp:TextBox ID="txt_daybeforefee" runat="server" Width="43px" CssClass="clsinputadministration"
                                MaxLength="5"></asp:TextBox>
                        </td>
                        <td style="width: 140px; height: 24px;" class="clsleftpaddingtable">
                            Judge/Jury Fee:
                        </td>
                        <td style="width: 215px; height: 24px" class="clsleftpaddingtable">
                            $ &nbsp;<asp:TextBox ID="txt_juryfee" runat="server" Width="40px" CssClass="clsinputadministration"
                                MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td class="clsleftpaddingtable" style="height: 24px">
                            OutSide Firm Client:
                            <br />
                            (Somebody else’s client)
                        </td>
                        <td class="clsleftpaddingtable" style="width: 215px; height: 24px">
                            &nbsp;<asp:CheckBox ID="cb_coverfrom" runat="server" CssClass="label" />
                        </td>
                        <td style="width: 140px; height: 24px;" class="clsleftpaddingtable">
                            Firms covering cases for us:
                        </td>
                        <td style="width: 215px; height: 24px">
                            &nbsp;<asp:CheckBox ID="cb_coverby" runat="server" CssClass="label" />
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td class="clsleftpaddingtable" style="height: 24px; width:140px">
                            Send text message?
                        </td>
                        <td class="clsleftpaddingtable" colspan="3">
                            &nbsp;<asp:CheckBox ID="cb_textmessage" runat="server" CssClass="label" onclick="ShowHideRow();" />
                        </td>
                        
                    </tr>
                     <tr class="clsleftpaddingtable">
                        <td class="clsleftpaddingtable" style="height: 24px; width:140px">
                          <asp:Label  runat="server" ID="lblUpload" Text="Upload Signature Image:" ></asp:Label>  
                        </td>
                        <td colspan="1">
                            <asp:FileUpload CssClass="clsInputadministration" Width="254px" ID="ImageUpload" runat="server" />
                        </td>
                        <td class="clsleftpaddingtable" colspan="1">
                            <asp:Image Width="150px" Height="150px" style="display:none" ID="SignatureImage" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="tr_textmessage" class="clsleftpaddingtable" style="display:none">
                        
                        <td class="clsleftpaddingtable" colspan="4">
                            <table runat="server" id="Table1" border="0" cellpadding="0" cellspacing="0" style="display: block;
                                width: 100%">
                                <tr>
                                    <td class="clsleftpaddingtable" style="width: 140px" height="25">
                                        To:
                                    </td>
                                    <td class="clsleftpaddingtable" >
                                        <asp:TextBox ID="txtCellNumber" runat="server" Width="205px" CssClass="clsinputadministration"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="clsleftpaddingtable" style="width: 140px" height="25">
                                        From:
                                    </td>
                                    <td class="clsleftpaddingtable" >
                                        <asp:TextBox ID="txtFromnumber" runat="server" Width="205px" CssClass="clsinputadministration"
                                            MaxLength="50">info@sullolaw.com</asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="clsleftpaddingtable" height="25">
                                        Notice Period Days:
                                    </td>
                                    <td class="clsleftpaddingtable">
                                        <asp:DropDownList ID="ddlNoticeDays"
                                    runat="server" Width="58px" CssClass="clsinputcombo">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem Selected="True">2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem Value="-1">Always</asp:ListItem>
                                </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="height: 1px" class="clsleftpaddingtable" colspan="4">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:Label ID="lbl_message" runat="server" CssClass="label" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="clsleftpaddingtable">
                        <td style="height: 25px" class="clsleftpaddingtable" colspan="4" align="center">
                            <input class="clsbutton" id="btn_clear" onclick="Clear();" type="button" value="Clear">
                            <asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" Text="Submit"></asp:Button>&nbsp;<asp:Button
                                ID="btn_delete" runat="server" CssClass="clsbutton" Text="Delete"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" style="height: 10px">
            </td>
        </tr>
        <tr>
            <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px;">
                <table style="width: 100%;">
                    <tr>
                        <td>
                        </td>

                        <td style="text-align: right;">
                            <asp:CheckBox ID="chkActiveFirm" runat="server" Checked="true" Text="Active" AutoPostBack="true"
                                OnCheckedChanged="chkActiveFirm_CheckedChanged" />
                            <asp:CheckBox ID="chkInactive" runat="server" Checked="false" Text="Inactive" AutoPostBack="true"
                                OnCheckedChanged="chkInactive_CheckedChanged" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblgrid" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
                    <tr>
                        <td align="center">
                            <asp:DataGrid ID="dgResult" runat="server" Width="780px" CssClass="clsleftpaddingtable"
                                BorderStyle="None" PageSize="20" AutoGenerateColumns="False" BorderColor="White"
                                AllowSorting="True">
                                <Columns>
                                    <asp:TemplateColumn SortExpression="firmname">
                                        <HeaderStyle Width="25%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnFirmName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.firmname") %>'
                                                CommandName="FNameClicked">
                                            </asp:LinkButton>
                                            <asp:Label ID="lbl_FirmID" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.firmid") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkFirmNameH" runat="server" CommandName="firmname">Firm Name</asp:LinkButton>
                                        </HeaderTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ShortName">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkShortNameH" runat="server" CommandName="FirmAbbreviation">Short Name</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblShortName" runat="server" CssClass="Label" Text='<%# Eval("FirmAbbreviation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Base Fee">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkBaseFeeH" runat="server" CommandName="basefee">Base Fee</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBaseFee" runat="server" Text='<%# Eval("basefee","{0:C0}") %>'
                                                CssClass="Label"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Day Before Fee">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkDayBeforeFeeH" runat="server" CommandName="daybeforefee">Day Before Fee</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDayBeforeFee" runat="server" CssClass="Label" Text='<%# Eval("daybeforefee","{0:C0}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Judge/Jury Fee">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkJudgeFeeH" runat="server" CommandName="judgefee">Judge/Jury Fee</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJudgeFee" runat="server" CssClass="Label" Text='<%# Eval("judgefee","{0:C0}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Cover From">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkCoverFromH" runat="server" CommandName="coverfrom">Cover From</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCoverFrom" runat="server" CssClass="Label" Text='<%# Eval("coverfrom") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Cover By">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkCoverByH" runat="server" CommandName="coverby">Cover By</asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCoverBy" runat="server" CssClass="Label" Text='<%# Eval("coverby") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle NextPageText="Next&amp;gt" PrevPageText="&amp;lt;Previous" HorizontalAlign="Center">
                                </PagerStyle>
                                <ItemStyle Font-Names="Verdana,Arial,Helvetica,sans" Font-Size="8.5pt" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="display: none" align="right">
                <asp:TextBox ID="txt_firmid" runat="server" Width="40px" CssClass="clsinputadministration"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td background="../../images/separator_repeat.gif" colspan="6" height="11">
            </td>
        </tr>
        <tr>
            <td colspan="5" style="height: 48px">
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>--%>
</html>

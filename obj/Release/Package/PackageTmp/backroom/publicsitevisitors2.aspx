<%@ Page Language="C#" AutoEventWireup="true" Codebehind="publicsitevisitors2.aspx.cs"
    Inherits="HTP.backroom.publicsitevisitors2" %>

<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Public Site Visitors</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function OpenPopup(path)
        {
            window.open(path);
            return false;
        }
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" style="height: 15px">
                    </td>
                </tr>
                <tr class="clsLeftPaddingTable">
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 980px">
                            <tr>
                                <td class="clssubhead" style="height: 27px; width: 73px;">
                                    &nbsp; Start Date</td>
                                <td style="height: 27px; width: 117px;">
                                    &nbsp;<ew:CalendarPopup ID="calDateFrom" runat="server" AllowArbitraryText="False"
                                        CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20"
                                        EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                        Nullable="True" PadSingleDigits="True" SelectedDate="2003-01-11" ShowClearDate="True"
                                        ShowGoToToday="True" ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00"
                                        Width="90px">
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td class="clssubhead" style="height: 27px; width: 54px;">
                                    End Date</td>
                                <td style="height: 27px; width: 123px;">
                                    &nbsp;<ew:CalendarPopup ID="calDateTo" runat="server" AllowArbitraryText="False"
                                        CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisplayOffsetY="20"
                                        EnableHideDropDown="True" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                        Nullable="True" PadSingleDigits="True" SelectedDate="2007-12-11" ShowClearDate="True"
                                        ShowGoToToday="True" ToolTip="Mail Date From" UpperBoundDate="12/31/9999 23:59:00"
                                        Width="90px">
                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TextboxLabelStyle CssClass="clstextarea" />
                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Gray" />
                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                            ForeColor="Black" />
                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                            Font-Size="XX-Small" ForeColor="Black" />
                                    </ew:CalendarPopup>
                                </td>
                                <td style="width: 102px; height: 27px">
                                    &nbsp;<asp:DropDownList ID="ddlClientType" runat="server" CssClass="clsinputadministration"
                                        Width="90px">
                                        <asp:ListItem Value="0">All</asp:ListItem>
                                        <asp:ListItem Value="1">Houston Cases</asp:ListItem>
                                        <asp:ListItem Value="2">Dallas Cases</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td align="right" style="height: 26px; width: 93px;">
                                    <asp:Button ID="btn_search" runat="server" CssClass="clsbutton" OnClick="btnSubmit_Click"
                                        Text="Search" Width="84px" />&nbsp;</td>
                                <td align="right" style="height: 26px; width: 420px;">
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" style="height: 9px">
                    </td>
                </tr>
                <tr>
                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px"
                        valign="middle">
                        <uc2:PagingControl ID="PagingControl1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" style="height: 9px">
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" ForeColor="Red" Font-Bold="True"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" valign="top" Width="980px">
                        <asp:GridView ID="gv_Results" runat="server" AutoGenerateColumns="False" Width="980px"
                            CssClass="clsleftpaddingtable" OnRowDataBound="GridView1_RowDataBound" PageSize="30" AllowPaging="True" OnPageIndexChanging="gv_Results_PageIndexChanging1">
                            <Columns>
                                <asp:TemplateField HeaderText="S#" >
                                    <HeaderStyle CssClass="clssubhead" Width="2%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" CssClass="Label" Visible="False" Text='<%#bind("ticketid") %>'></asp:Label>
                                        <asp:HyperLink ID="hlkSNo" runat="server" Text ='<%# bind("sno")%>' > </asp:HyperLink>
                                        <asp:HiddenField ID="hfTicketId" Value ='<%# DataBinder.Eval(Container,"DataItem.TicketId") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmail" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.Email") %>'></asp:Label>
                                        <asp:HiddenField ID="hf_pf" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.IsProcessCompleted") %>' />
                                        <asp:HiddenField ID="hf_cid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.Customerid") %>' />
                                        <asp:HiddenField ID="hf_siteid" runat="server" Value='<%# DataBinder.Eval(Container,"DataItem.siteid") %>' />
                                        <asp:LinkButton ID="hlkEmail" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Email") %>'
                                            Visible="False"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ticket No">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblTicketId" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.TicketNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Violations">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblViolations" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.violationDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AFlag" HeaderText="Acc" >
                                    <ItemStyle CssClass="GridItemStyleBig"/>
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CDL" HeaderText="CDL" >
                                    <ItemStyle CssClass="GridItemStyleBig" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Amount">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" CssClass="label" Text='<%# "$" + DataBinder.Eval(Container,"DataItem.amount","{0:g0}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RecDate" HeaderText="Visit Date">
                                    <ItemStyle CssClass="GridItemStyleBig" />
                                    <HeaderStyle CssClass="clssubhead" Width="140px" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Last Page ">
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastPage" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.LastPage") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SearchType" HeaderText="Search Type" >
                                    <ItemStyle CssClass="GridItemStyleBig" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="CaseType" HeaderText="Case Type" >
                                    <ItemStyle CssClass="GridItemStyleBig" />
                                    <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &amp;gt;" PreviousPageText="&amp;lt; Previous" FirstPageText="&amp;lt;&amp;lt; First" LastPageText="Last &amp;gt;&amp;gt;" />
                            <PagerStyle HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td background="../images/separator_repeat.gif" style="height: 9px" valign="top">
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" %>

      <%
          try
          {
              
              if (Request.QueryString.Keys.Count > 0)
              {
                  int ticketId = 0;
                  int empId = 0;
                  int subdoctypeid = 0;
                  int BookId = 0;
                  int picID = 0;
                  int oprationType = 0;
                  string fileToSave = string.Empty;
                  foreach (string key in Request.QueryString.AllKeys)
                  {
                      if (key.Equals("DocId"))
                          ViewState["ScanDocType"] = Request.QueryString[key];
                      else if (key.Equals("txtbID"))
                      {
                          if (Request.QueryString[key] != null && Request.QueryString[key].Length > 0)
                          {
                              ViewState["sSDDocCount"] = ViewState["sSDDocCount"];
                              BookId = Convert.ToInt32(Request.QueryString[key]);
                          }
                          else
                              ViewState["sSDDocCount"] = 1;
                      }
                      else if (key.Equals("Desc"))
                      {
                          if (key != null)
                              ViewState["sSDDesc"] = Request.QueryString[key];
                      }

                      else if (key.Equals("sid"))
                      {
                          if (key != null)
                              ticketId = Convert.ToInt32(Request.QueryString[key]);
                      }

                      else if (key.Equals("empId"))
                          empId = Convert.ToInt32(Request.QueryString[key]);

                      else if (key.Equals("subDoc"))
                      {
                          if (!int.TryParse(Request.QueryString[key], out subdoctypeid))
                              subdoctypeid = 0;
                      }
                      else if (key.Equals("Operation"))
                      {
                          if (!int.TryParse(Request.QueryString[key], out oprationType))
                              oprationType = 0;
                      }
                      else if (key.Equals("name"))
                          fileToSave = Request.QueryString[key];
                          

                  }
                  // Uploading the File on server. . .
                  String strImageName;
                  HttpFileCollection files = HttpContext.Current.Request.Files;
                  HttpPostedFile uploadfile = files["RemoteFile"];
                  strImageName = uploadfile.FileName;
                  uploadfile.SaveAs(System.Configuration.ConfigurationManager.AppSettings["NTPATHScanTemp"].ToString() + strImageName);
                  // End Uploading the File on server. . .
                  
                  ViewState["BookID"] = 0;
                  ViewState["DocCount"] = 0;
                  ViewState["Events"] = "Scan";

                  //Perform database Uploading Task

                  //Ozair 5546 02/17/2009 icluded ticketid whcih is concatenated with session id in scanning component
                  string searchpat = "*" + fileToSave + empId.ToString() + "*.jpg";
                  string[] fileName = System.IO.Directory.GetFiles(ConfigurationManager.AppSettings["NTPATHScanTemp"].ToString(), searchpat);

                  if (fileName.Length > 1)
                  {
                      //fileName = sortfilesbydate(fileName);
                      for (int i = 0; i < fileName.Length; i++)
                      {
                          DateTime cDateTime = System.IO.File.GetCreationTime(fileName[i]);
                          for (int j = i + 1; j < fileName.Length; j++)
                          {
                              DateTime cDateTime1 = System.IO.File.GetCreationTime(fileName[j]);
                              if (DateTime.Compare(cDateTime1, cDateTime) < 0)
                              {
                                  string fname = fileName[j];
                                  fileName[j] = fileName[i];
                                  fileName[i] = fname;
                                  i = -1;
                                  break;
                              }
                          }
                      }
                  }
                  
                  //string Ticket = ViewState["vTicketId"].ToString();
                  string bType = ViewState["ScanDocType"].ToString().Trim();

                  //int BookId = 0, picID = 0;
                  //if (txtbID.Text.Length > 0)
                  //{
                  //    BookId = Convert.ToInt32(txtbID.Text);
                  //}
                  string picName, picDestination;

                  int DocCount = Convert.ToInt32(ViewState["sSDDocCount"].ToString());

                  string description = ViewState["sSDDesc"].ToString().ToUpper();

                  for (int i = 0; i < fileName.Length; i++)
                  {
                      FrameWorkEnation.Components.clsENationWebComponents ClsDb = new FrameWorkEnation.Components.clsENationWebComponents();
                      string[] key = { "@updated", "@extension", "@Description", "@DocType", "@SubDocTypeID", "@Employee", "@TicketID", "@Count", "@Book", "@Events", "@BookID" };
                      object[] value1 = { DateTime.Now, "JPG", description, bType, subdoctypeid, empId, ticketId, DocCount, BookId, ViewState["Events"].ToString(), "" };
                      //call sP and get the book ID back from sP
                      picName = ClsDb.InsertBySPArrRet("usp_hts_NewAddScan", key, value1).ToString();
                      string BookI = picName.Split('-')[0];
                      string picI = picName.Split('-')[1];
                      BookId = (int)Convert.ChangeType(BookI, typeof(int)); ; //
                      picID = (int)Convert.ChangeType(picI, typeof(int)); ; //
                      if (ViewState["ScanDocType"].ToString() != "0")//if (Adf.Checked == false)
                      {
                          //txtbID.Text = BookId.ToString();
                      }
                      DocCount = DocCount + 1;
                      ViewState["sSDDocCount"] = Convert.ToString(DocCount);

                      //Move file
                      picDestination = ConfigurationManager.AppSettings["NTPATHScanImage"].ToString() + picName + ".jpg"; //DestinationImage

                      System.IO.File.Copy(fileName[i].ToString(), picDestination);

                      System.IO.File.Delete(fileName[i].ToString());
                  }
                  ////if (ddlDocType.SelectedValue.ToString() == "0")//if (Adf.Checked)
                  //if (oprationType == 0)//if (Adf.Checked)
                  //{

                  //    //txtbID.Text = "";
                  //    BookId = 0;
                  //    ViewState["sSDDesc"] = "";
                  //    //Adf.Checked = false;
                  //}
              }
          }
          catch (Exception ex)
          {
              string s = ex.Message;
          }
    
    
%>
    
    


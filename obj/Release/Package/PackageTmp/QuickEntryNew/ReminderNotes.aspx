<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.QuickEntryNew.ReminderNotes"
    CodeBehind="ReminderNotes.aspx.cs" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReminderNotes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script language="javascript" type="text/javascript">
        
    
	function popup()
	{
	    this.ModalPopupExtender1.hide();
	    return false;
		
  	}
	function ValidateControls()
	{
	     //Fahad 5296 12/12/2008  the most outer if included for FTACall
	    if(document.getElementById("hfCallType").value=="FTACall")
	    {
	        if(document.getElementById("td_comments").style.display=='block')
	        {
	            if(document.Form1.txt_Notes.value.length==0)
	            {
	                alert('Please enter genaral comments');
	                document.Form1.txt_Notes.focus();
	                return false;	
	            }
	        }
    	    
	        if(document.getElementById("ddl_rStatus").value=="4")
            {
                document.getElementById("hdnFlag").value="1";          
            }                             
            else
            { 
                document.getElementById("hdnFlag").value="0";
              
            }      
    	    
	    }
	    else
	    {
	            var cmts =document.getElementById("txt_RComments").value;
                var dateLenght = 0;
	            var newLenght = document.getElementById("txt_RComments").value.length
        	
	            // Added Zahoor 3977
	            if(document.getElementById("td_comments").style.display=='block')
	            {
	                if(document.Form1.txt_Notes.value.length==0)
	                {
	                    alert('Please enter genaral comments');
	                    document.Form1.txt_Notes.focus();
	                    return false;	
	                }
	            }
	            // end 3977
            	
            	
	            if(document.getElementById("txt_RComments").style.display=='block')
	            {
	                if(document.getElementById("txt_RComments").value.length == 0)
	                {
	                    alert('Please enter contact comments');
	                    // added zahoor: for setting focus on control
	                    document.getElementById("txt_RComments").focus(); 
	                    return false;	
	                }
	            }
        	
        	    
	            if(document.getElementById("ddl_rStatus").value=="4")
                {
                    document.getElementById("hdnFlag").value="1";          
                }                             
                else
                { 
                    document.getElementById("hdnFlag").value="0";
                  
                }      
               
                if (cmts.length > 5000)
		        {
		            event.returnValue=false;
                    event.cancel = true;
		        }
                  
	            //Comments Textboxes
        		  
                if(newLenght > 0)
                    dateLenght =  27
                else
                    dateLenght = 0
                  
                  
		        if (document.getElementById("txt_RComments").value.length + document.getElementById("lblComment").innerText.length > 5000 - dateLenght)//-27 bcoz to show last time partconcatenated with comments
		        {
		            alert("Sorry You cannot type in more than 5000 characters in contact comments")
			        return false;		  
		        }
    		  
	    }
}	

    function MaxLength()
    {
        if(document.getElementById("txt_RComments").value.length >500)
	    {
	        alert("The length of character is not in the specified Range");
	        document.getElementById("txt_RComments").focus();
			
		}
				
	}
		
	function ValidateLenght()
	{ 
	    var cmts = document.getElementById("txt_RComments").value;
		if (cmts.length > 500)
		{
		    event.returnValue=false;
               event.cancel = true;
		}
	}

    </script>

    <style type="text/css">
        .style1
        {
            height: 20px;
            width: 140px;
        }
        .style2
        {
            width: 140px;
        }
        .style3
        {
            width: 140px;
        }
        .style4
        {
            height: 16px;
            width: 140px;
        }
        .style5
        {
            width: 105px;
        }
    </style>
</head>
<body style="background-color: #eff4fb">
    <form id="Form1" method="post" runat="server">
    <div style="left: 0px; width: 0px; position: absolute; top: 0px; height: 0px">
        <table class="clsLeftPaddingTable" height="372" cellspacing="1" cellpadding="0" align="left"
            border="0">
            <tr>
                <td style="width: 498px; height: 16px;">
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="label"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="hfCallType" runat="server" />
                </td>
            </tr>
            <tr>
                <td valign="top" align="right" style="width: 498px; height: 357px;">
                    <table class="clsLeftPaddingTable" cellspacing="0" cellpadding="0" align="left" border="0"
                        style="width: 100%">
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="3" height="11">
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td valign="middle" class="style1">
                                <font color="#3366cc">Name:</font>
                            </td>
                            <td width="520" colspan="2" style="height: 20px; text-align: left;">
                                <asp:Label ID="lbl_Name" runat="server" CssClass="clslabel"></asp:Label><asp:Label
                                    ID="lbl_rid" runat="server" Visible="False" CssClass="clslabel"></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td valign="middle" class="style1">
                                <font color="#3366cc">Case Status:</font>
                            </td>
                            <td width="520" colspan="2" style="height: 20px; text-align: left">
                                <asp:Label ID="lbl_Status" runat="server" CssClass="clslabel"></asp:Label>
                            </td>
                        </tr>                        
                        <tr bgcolor="#eff4fb">
                            <td valign="middle" class="style2">
                                <span style="color: #3366cc">Call Back:</span>
                            </td>
                            <td colspan="2" height="20" style="text-align: left">
                                <asp:DropDownList ID="ddl_rStatus" runat="server" CssClass="clsInputCombo" OnSelectedIndexChanged="ddl_rStatus_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td valign="middle" class="style2">
                                <asp:Label ID="lbl_contacttext" runat="server" Width="104px" ForeColor="#3366CC">Contact Number:</asp:Label>
                            </td>
                            <td colspan="2" height="20" style="text-align: left">
                                <asp:Label ID="lbl_contact1" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'>
                                </asp:Label><br />
                                <asp:Label ID="lbl_Contact2" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'>
                                </asp:Label><br />
                                <asp:Label ID="lbl_Contact3" runat="server" CssClass="clslabel" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td valign="middle" class="style2">
                                <asp:Label ID="lbllanguage" runat="server" Text="Language:" CssClass="label" ForeColor="#3366CC"></asp:Label>
                            </td>
                            <td colspan="2" height="20" style="text-align: left">
                                <asp:Label ID="lbl_Language" runat="server" CssClass="clslabel"></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td class="style2">
                            </td>
                            <td style="text-align: left; width: 345px;">
                            </td>
                            <td style="width: 336px">
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="3" height="11">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" id="td_comments" runat="server" width="100%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr bgcolor="#eff4fb">
                                        <td align="left" class="style5">
                                            <asp:Label ID="lblgen_comments" runat="server" CssClass="label" ForeColor="#3366CC"
                                                Text="Gen. Comments:" Width="113px"></asp:Label>
                                        </td>
                                        <td style="text-align: left; width: 325px;" valign="top">
                                            <div style="width: 330px; height: 83px; overflow: auto" runat="server" id="divNotes">
                                                <asp:Label ID="lblNotes" runat="server" Width="306px" CssClass="label"></asp:Label>
                                            </div>
                                             <br />                                             
                                            <asp:TextBox ID="txt_Notes" runat="server" Width="325px" TextMode="MultiLine" Height="50px"
                                                MaxLength="5000" CssClass="clslabel"></asp:TextBox>
                                        
                                        </td>
                                    </tr>
                                    <!--tr bgcolor="#eff4fb">
                                        
                                    </tr-->
                                    <tr>
                                        <td background="../images/separator_repeat.gif" colspan="3" height="11">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="tr_Ccomments" runat="server" bgcolor="#eff4fb">
                            <td align="left" class="style3">
                                <asp:Label ID="lblcomments" runat="server" CssClass="label" ForeColor="#3366CC" Text="Contact Comments:"
                                    Width="113px"></asp:Label>&nbsp;
                            </td>
                            <td style="height: 91px; text-align: left; width: 325px;" valign="top">
                                <div style="width: 330px; height: 83px; overflow: auto" runat="server" id="div1">
                                    <asp:Label ID="lblComment" runat="server" Width="306px" CssClass="label"></asp:Label>
                                </div>
                                <br />
                                <asp:TextBox ID="txt_RComments" runat="server" Width="325px" TextMode="MultiLine"
                                    Height="50px" MaxLength="5000" CssClass="clslabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#eff4fb">
                            <td valign="top" class="style4">
                                <asp:Label ID="lbl_outsidefirm" runat="server" Width="104px" ForeColor="#3366CC"
                                    Visible="False">Outside attorney=</asp:Label>
                            </td>
                            <td valign="top" align="left" colspan="1" rowspan="1" style="width: 345px; height: 16px;">
                                <asp:Label ID="lbl_Firm" runat="server" CssClass="label" Visible="False" Font-Bold="true"></asp:Label>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="3" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="style1">
                                <asp:Label ID="lbl_Bond" runat="server" Visible="False" Font-Bold="true" BackColor="#FFCC66">Bond</asp:Label>
                            </td>
                            <td valign="top" align="right" style="height: 20px; width: 345px;">
                                <asp:Literal ID="litScript" runat="server"></asp:Literal>
                                <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Update" OnClientClick="return ValidateControls();"
                                    OnClick="btn_update_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td background="../images/separator_repeat.gif" colspan="3" style="height: 11px">
                                <input id="hdnFlag" type="hidden" value="0" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 498px">
                    <input id="hdnStatus" type="hidden" runat="server" />
                    <input id="hdnStatusName" type="hidden" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 498px">
                </td>
            </tr>
        </table>
        <aspnew:ScriptManager ID="scriptmanager1" runat="server">
        </aspnew:ScriptManager>
        <table>
            <%--<tr>
                    <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34px"
                        style="width: 353px">
                    </td>
                </tr>--%>
            <tr>
                <td>
                    <asp:Panel ID="panel1" runat="server" BackColor="#eff4fb">
                        <table id="tablepopup" style="border-top: black thin solid; border-left: black thin solid;
                            border-bottom: black thin solid; border-right: black thin solid" cellpadding="0"
                            cellspacing="0">
                            <tr>
                                <td class="clssubhead" background="../Images/subhead_bg.gif" colspan="2" height="34"
                                    style="width: 353px">
                                    <table style="width: 407px" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td class="clssubhead" align="left" style="height: 16px">
                                                    &nbsp;Confirmation Box
                                                </td>
                                                <td align="right" style="height: 16px">
                                                    &nbsp;<asp:LinkButton ID="lnkbtncancelpopup" runat="server">X</asp:LinkButton>&nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr bgcolor="#eff4fb">
                                <td align="center">
                                    <table width="353px" cellspacing="1" cellpadding="0">
                                        <tr>
                                            <td>
                                                &nbsp;<img src="/images/QuestionIcon.png" border="0" width="50" height="50" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblmessageshow" runat="server" BackColor="#eff4fb" ForeColor="#3366CC"
                                                    Text="Are you sure you want to activate this Flag and send a letter to the client requesting for updated contact information?"
                                                    Width="353px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnok" runat="server" Text="Yes" Style="background: url(../Images/btnOk.ico) no-repeat 2px 0px;"
                                                    CssClass="clsbutton" Height="27px" Width="91px" OnClick="btnok_Click" />&nbsp;
                                                <asp:Button ID="btncancel" runat="server" Text="No" Style="background: url(../Images/btnClose.gif) no-repeat 2px 0px;"
                                                    CssClass="clsbutton" Height="27px" Width="91px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td bgcolor="#eff4fb">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <asp:Button ID="Button1" runat="server" Text="Cancel" Style="display: none;" />
        <AjaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1"
            PopupControlID="tablepopup" CancelControlID="btncancel" BackgroundCssClass="modalBackground">
        </AjaxToolkit:ModalPopupExtender>
    </div>
    </form>
</body>
</html>

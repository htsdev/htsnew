﻿<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d653online37282035f2" %>--%>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>--%>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%--<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>--%>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Reports.frmRptDocketULNew"
    CodeBehind="frmRptDocketULNew.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Docket Report</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script src="../Scripts/dates.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
		function validate()
		{			
			var d1 = document.getElementById("dtFrom").value
			var d2 = document.getElementById("dtTo").value			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
			    //alert("start date cannot greater than end date");
			    $("#txtErrorMessage").text("start date cannot greater than end date");
			    $("#errorAlert").modal();
					document.getElementById("dtTo").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
					
			var dtfrom=document.getElementById("dtFrom").value;
			var dtTo=document.getElementById("dtTo").value;
			if(dtfrom > dtTo)
			{
			    //alert("Please Specify Valid Future Date!");
			    $("#txtErrorMessage").text("Please Specify Valid Future Date!");
			    $("#errorAlert").modal();
			return false;
			}
		}
		
		function showall()
		{		
		 //Yasir Kamal 5025 01/21/2009  ‘Non HMC/HCJP Cases’ under Courts drop down box
		    if (document.getElementById("chk_showall").checked)
		    {
		       document.getElementById("LblTo").disabled =true;
		        document.getElementById("dtFrom").disabled =true;
		        document.getElementById("LblFrom").disabled =true;
		        document.getElementById("dtTo").disabled =true;
		    }
		    else
		    {
		       document.getElementById("LblTo").disabled =false;
		        document.getElementById("dtFrom").disabled =false;
		       document.getElementById("LblFrom").disabled =false;
		        document.getElementById("dtTo").disabled =false;
		    }
		}	
		
		
    </script>

    <style type="text/css">
        .style1
        {
            width: 109px;
        }
        .style2
        {
            width: 111px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" ms_positioning="GridLayout"
    onload="showall()">
    <form id="Form1" method="post" runat="server">
        <asp35:ScriptManager ID="ScriptManager1" runat="server"></asp35:ScriptManager>
     <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="tblMain">
        <section class="wrapper main-wrapper row" id="" style="">

             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                 </div>
                 </div>

            

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Docket Report</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>

            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Docket Report</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                             
                                                                <asp:Label ID="LblCourtLocation" runat="server" Text="Court Location" CssClass="form-label"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCourtLoc" runat="server" Width="" CssClass="form-control">
                            </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              
                                                                <asp:Label ID="LblCaseStatus" runat="server" Text="Case Status" CssClass="form-label"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:DropDownList ID="ddlCaseStatus" runat="server" Width="" CssClass="form-control">
                            </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <asp:Label ID="LblFrom" runat="server" Text="Court Date From" CssClass="form-label"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="dtFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true">

                                    </picker:datepicker>

                                    <%--<picker:datepicker id="cal_EffectiveFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>

                                    <%--<ew:CalendarPopup ID="dtFrom" runat="server" Height="19px" Width="100px" Font-Size="8pt"
                                Font-Names="Tahoma" DisplayOffsetY="20" ToolTip="Date From" PadSingleDigits="True"
                                UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="False" Nullable="True" Culture="(Default)"
                                AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>--%>




                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <asp:Label ID="LblTo" runat="server" Text="Court Date To" CssClass="form-label"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <picker:datepicker id="dtTo" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                   <%-- <ew:CalendarPopup ID="dtTo" runat="server" Height="19px" Width="90px" Font-Size="8pt"
                                Font-Names="Tahoma" DisplayOffsetY="20" ToolTip="Date To" PadSingleDigits="True"
                                UpperBoundDate="12/31/9999 23:59:00" ShowClearDate="False" Nullable="True" Culture="(Default)"
                                AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif"
                                ControlDisplay="TextBoxImage" EnableHideDropDown="True" Text=" ">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>--%>



                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <asp:Label ID="lbl_FirmAbbrevation" runat="server" Text="Out side Firm" CssClass="form-label"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <%-- <asp:DropDownList ID="ddl_FirmAbbreviation" runat="server" CssClass="form-control"
                                Font-Size="XX-Small">
                            </asp:DropDownList>--%>
                                     <asp:DropDownList ID="ddl_FirmAbbreviation" runat="server" CssClass="form-control"
                                Font-Size="">
                            </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <%--<label class="form-label">Date</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                             
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:CheckBox ID="chk_showall" runat="server" Text="Show All" CssClass="checkbox-custom"
                                onclick="showall()" />
                                     <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" Width=""
                                OnClientClick="showall()"></asp:Button>
                                    </div>
                                                                </div>
                         </div>


                    </div>
                     </div>
                </section>

            <div class="clearfix"></div>
            <section class="box" id="tblTitle" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">LIST</h2>
                     <div class="actions panel_actions pull-right">
                     <uc3:PagingControl ID="Pagingctrl" runat="server" />
                         

                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Date</label>--%>
                                <span class="desc"></span>
                                <div class="controls">

                                    <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="table table-small-font table-bordered table-striped" AllowSorting="True" AllowPaging="True" PageSize="30"
                    OnRowDataBound="gv_Records_RowDataBound" OnPageIndexChanging="gv_Records_PageIndexChanging"
                    OnSorting="gv_Records_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="S#">
                            <ItemTemplate>
                                <%--Sabir Khan 5521 02/16/2009 set sno for client profile...--%>
                                <%--  <asp:Label ID="lblSerial" runat="server" CssClass="clsLabel" Text='<%# Bind("sno") %>'></asp:Label>--%>
                                &nbsp;<asp:HyperLink ID="lblSerial" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                    Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                <%--<asp:Label ID="lblSerial" runat="server"></asp:Label>--%>
                                <asp:Label ID="lbl_Ticketid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Last Name&lt;/u&gt;" 
                            SortExpression="lastname">
                            <ItemTemplate>
                                <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("lastname") %>'></asp:Label>
                            </ItemTemplate>                           
                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" />
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;First Name&lt;/u&gt;" 
                            SortExpression="firstname">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("firstname") %>'></asp:Label>
                            </ItemTemplate>                        
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<u>Entry</u>" SortExpression="sortentry">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Paydate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.entry") %>'
                                    Width="100%"> </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Location&lt;/u&gt;" 
                            SortExpression="court" >
                            <ItemTemplate>
                                <asp:Label ID="lblCourt" runat="server" Text='<%# Bind("court") %>'></asp:Label>
                            </ItemTemplate>                           
                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Center" />
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<u>Last Pay Date</u>" SortExpression="sortpaydate">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Paydate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.paydate") %>'
                                    Width="100%"> </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Court Date&lt;/u&gt;" 
                            SortExpression="sortcourtdate">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Courtdate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>'
                                    Width="100%"> </asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="clssubhead" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Status&lt;/u&gt;" 
                            SortExpression="description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                            </ItemTemplate>                          
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Firm">
                            <ItemTemplate>
                                <asp:Label ID="lblFirm" runat="server" Text='<%# Bind("firmabbreviation") %>'></asp:Label>
                            </ItemTemplate>                     
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;u&gt;Bond&lt;/u&gt;" 
                            SortExpression="bondflag">
                            <ItemTemplate>
                                <asp:Label ID="lblBond" runat="server" Text='<%# Bind("bondflag") %>'></asp:Label>
                            </ItemTemplate>                       
                            <ItemStyle CssClass="GridItemStyle" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" />
                    <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                        FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                </asp:GridView>


                                        </div>

                                    </div>
            </div>

                 </div>
                         </div>
                    </div>
                </section>












           </section>
                     </section>
         </div>
    </form>
    <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
     <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRptCourtArrignmentAppearanceAlert.aspx.cs"
    Inherits="HTP.QuickEntryNew.frmRptCourtArrignmentAppearanceAlert" EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/UpdateFollowUpInfo.ascx" TagName="UpdateFollowUpInfo"
    TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Outside Court Arraignment/Appearance Alert</title>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        .modalPopup
        {
            background-color: #FFD9D5;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            position: static;
            left: 200px;
            top: -65px;
        }
        A:link
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
        A:Visited
        {
            font-size: 8pt;
            font-family: Verdana,Arial, Helvetica, sans-serif;
            color: #4169E1;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" style="z-index: 101"
                    width="800">
                    <tbody>
                        <tr>
                            <td colspan="4" style="height: 14px">
                                <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--  <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">--%>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr class="clsLeftPaddingTable">
                                    <td align="right">
                                        <table>
                                            <tr>
                                                <td align="right">
                                                    <asp:CheckBox ID="chkShowAllUserRecords" runat="server" Text="Show All " CssClass="clssubhead"
                                                        OnCheckedChanged="chkShowAllUserRecords_CheckedChanged" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="right" style="height: 34px">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                </td>
                                                <td style="text-align: right">
                                                    <%--<aspnew:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>--%>
                                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    <%-- </ContentTemplate>
                                            </aspnew:UpdatePanel>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td background="../images/separator_repeat.gif" height="11" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="center" style="width: 100%">
                                                    <asp:Label ID="lbl_message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" valign="top">
                                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="upnlResult">
                                                        <ProgressTemplate>
                                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                                CssClass="clsLabel"></asp:Label>
                                                        </ProgressTemplate>
                                                    </aspnew:UpdateProgress>
                                                    <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        AllowPaging="True" OnPageIndexChanging="gv_records_PageIndexChanging" PageSize="20"
                                                        CssClass="clsLeftPaddingTable" OnRowCommand="gv_records_RowCommand" OnRowDataBound="gv_records_RowDataBound"
                                                        AllowSorting="True" OnSorting="gv_records_Sorting">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S#">
                                                                <ItemTemplate>
                                                                    &nbsp;<asp:HyperLink ID="lbl_sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid_pk") %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:HyperLink>
                                                                    <asp:HiddenField ID="hf_TicketID" runat="server" Value="<%# Bind('TicketID_PK') %>" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="First Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_firstname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("firstname") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Last Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_lastname" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("lastname") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cause No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_causenum" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("CauseNumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ticket No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_ticketnumber" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("TICKETNUMBER") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Court Loc</u>" SortExpression="CRT_Location">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_crtloc" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("CRT_Location") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lb_Status" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("COURTSTATUS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="&lt;u&gt;CourtDate&lt;/u&gt;" SortExpression="CourtDate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CourtDate" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Bind("CourtDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Follow-Up Date</u>" SortExpression="NonHMCFollowUpDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FollowUpD" runat="server" CssClass="clsLeftPaddingTable" Text='<%# DataBinder.Eval(Container, "DataItem.NonHMCFollowUpDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton Text="<img src='../Images/add.gif' border='0'/>" ID="img_Add" CommandName="btnclick"
                                                                        runat="server" />
                                                                    <asp:HiddenField ID="hf_criminal_fname" runat="server" Value='<%#Eval("FIRSTNAME") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_lname" runat="server" Value='<%#Eval("LASTNAME") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_ticketno" runat="server" Value='<%#Eval("TICKETNUMBER") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_causeno" runat="server" Value='<%#Eval("CAUSENUMBER") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_loc" runat="server" Value='<%#Eval("CRT_LOCATION") %>' />
                                                                    <asp:HiddenField ID="hf_criminal_courtid" runat="server" Value='<%#Eval("COURTVIOLATIONSTATUSIDMAIN") %>' />
                                                                    <asp:HiddenField ID="hf_NextFollowUpdate" runat="server" Value='<%#Eval("NextFollowUpDate") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../images/separator_repeat.gif" height="11" width="780">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%-- <aspnew:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanelcrtl">
                                                        <ProgressTemplate>
                                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl2" runat="server" Text="Please Wait ......"
                                                                CssClass="clsLabel"></asp:Label>
                                                        </ProgressTemplate>
                                                    </aspnew:UpdateProgress>--%>
                                                    <%-- <aspnew:UpdatePanel ID="UpdatePanelcrtl" runat="server">
                                                        <ContentTemplate>--%>
                                                    <asp:Panel ID="pnlFollowup" runat="server">
                                                        <uc3:UpdateFollowUpInfo ID="UpdateFollowUpInfo2" runat="server" Title="FollowUp Date" />
                                                    </asp:Panel>
                                                    <%--</ContentTemplate>
                                                    </aspnew:UpdatePanel>
                                                    <aspnew:UpdatePanel ID="UpdatePanelmodal" runat="server">
                                                        <ContentTemplate>--%>
                                                    <ajaxToolkit:ModalPopupExtender ID="mpeTrafficwaiting" runat="server" BackgroundCssClass="modalBackground"
                                                        PopupControlID="pnlFollowup" TargetControlID="btn">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <asp:Button ID="btn" runat="server" Style="display: none;" />
                                                    <%--</ContentTemplate>
                                                    </aspnew:UpdatePanel>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc1:Footer ID="Footer1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
            <Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="chkShowAllUserRecords" />
            </Triggers>
        </aspnew:UpdatePanel>
    </div>
    </form>

    <script language="javascript" type="text/javascript">
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_div").style.zIndex = 111111;
        document.getElementById("UpdateFollowUpInfo2_cal_followupdate_monthYear").style.zIndex = 10004;
    </script>

</body>
</html>

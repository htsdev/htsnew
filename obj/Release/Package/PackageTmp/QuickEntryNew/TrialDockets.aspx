﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Page Language="c#" CodeBehind="TrialDockets.aspx.cs" AutoEventWireup="false"
    Inherits="HTP.QuickEntryNew.TrialDockets" %>

<%@ Register Src="~/WebControls/AttorneyScheduler.ascx" TagName="AttorneyScheduler"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>HTP Trial Docket</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <script>
		
		
		
		
		function selectemail()
		{
		
		var WinSettings = "center:yes;resizable:no;dialogHeight:300px;dialogwidth:310px;scroll:yes"
        var Arg = 1;
        var val = window.showModalDialog("SelectEmails.aspx",Arg,WinSettings);
                             
            if ( val=="")
            {
              return false;
            }
            else
            {
            document.getElementById("hf_emails").value = val;
            return true;
            
           }
             
		
		}
		function OpenSingleCR()
		{
		
		var Courtloc = document.getElementById("DDLCourts").value;
		var Courtdate = document.getElementById("dtpCourtDate").value;
		var Courtnumber = document.getElementById("TxtCourtNo").value;
		var Datetype = document.getElementById("LstCaseStatus").value;
		window.open('../Reports/TrialDocketCR.aspx?Courtloc='+Courtloc+'&Courtdate=' + Courtdate + '&Page=1&Courtnumber='+ Courtnumber + '&Datetype=' + Datetype + '&singles=1','','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
		return(false);
		}
		
		function OpenCRAll()
		{
		
		var Courtloc = document.getElementById("DDLCourts").value;
		var Courtdate = document.getElementById("dtpCourtDate").value;
		var Courtnumber = document.getElementById("TxtCourtNo").value;
		var Datetype = document.getElementById("LstCaseStatus").value;
		var showsdetail = document.getElementById("cbowesdetail").checked;
		window.open('../Reports/TrialDocketCR.aspx?Courtloc='+Courtloc+'&Courtdate=' + Courtdate + '&Page=1&Courtnumber='+ Courtnumber + '&Datetype=' + Datetype + '&singles=0&showowesdetail='+ showsdetail ,'','fullscreen=no,toolbar=no,left=0,top=0,status=no,menubar=no,resizable=yes');
		return(false);
		}
		
		
		
		
		function OpenPopUp(path, Val)  //(var path,var name)
		{
		 window.open(path,'',"height=300,width=400,resizable=yes, status=no,toolbar=no,scrollbars=yes,menubar=no,location=no");
		  //return true;
		}
		 //  Noufil 5772 04/14/2009 Validate Court Number
                function CheckName(name)
                { 
                    for ( i = 0 ; i < name.length ; i++)
                    {
                        var asciicode =  name.charCodeAt(i)
                        //If not valid alphabet 
                        if (!((asciicode >= 48 && asciicode <=57)))
                        return false;
                   }
                    return true;
                }
              function CheckCourtNumber()
              {      
                if(CheckName(document.getElementById("TxtCourtNo").value)==false)
                {
                    alert("Invalid Court Number : Please use number");
                    return false;
                }
              }
              
              
             //Nasir 6968 12/14/2009 add new method ShowProgressOnUpdate display loading on show setting click 
          	function ShowProgressOnUpdate()
	        { 
	        
	            //document.getElementById(tbl.id).style.display = 'block';
                //document.getElementById("btn_Update").style.display = 'none';                
                $find("MPELoding").show();
                //$get('td_iframe').style.display = 'none';
                return false;
	        }
      
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div class="page-container row-fluid container-fluid">

            <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
            <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
        <section id="main-content" class="" id="TableSub">
            <section class="wrapper main-wrapper row" style="">
                         <div class="col-xs-12">
                             <div class="alert alert-danger alert-dismissable fade in" id="dvMessage" runat="server" visible="false">
                                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <asp:Label ID="lblRecStatus" runat="server" Visible="False"></asp:Label>
                                 </div>
                         </div> 
                <div class="col-xs-12">
                        <div class="page-title">
                             <div class="pull-left">
                                 <!-- PAGE HEADING TAG - START --><h1 class="title">Trial Docket</h1><!-- PAGE HEADING TAG - END -->                           
                                 </div>

                             
                            </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Filters</h2>

                            <div class="actions panel_actions pull-right">
                    
                	            <a class="box_toggle fa fa-chevron-down"></a>
                    
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-3">
                                   <div class="form-group">
                                      <label class="form-label">Court Location</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <asp:DropDownList ID="DDLCourts" runat="server" CssClass="form-control"
                                                ToolTip="Court Location">
                                                <asp:ListItem Value="None">--Choose--</asp:ListItem>
                                            </asp:DropDownList>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-3">
                                   <div class="form-group">
                                      <label class="form-label">Court Date</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <ew:CalendarPopup ID="dtpCourtDate" runat="server" Text=" " ImageUrl="../images/calendar.gif"
                                    SelectedDate="2006-03-14" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                    CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                    Nullable="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Trial Date">
                                    <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
                                </ew:CalendarPopup>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-3">
                                   <div class="form-group">
                                      <label class="form-label">Court No</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <asp:TextBox ID="TxtCourtNo" runat="server" CssClass="form-control"
                                    ToolTip="Court Room No"></asp:TextBox>
                                           </div>
                                    </div>
                               </div>
                                <div class="col-md-3">
                                   <div class="form-group">
                                      <label class="form-label">Court Status</label>
                                       <span class="desc"></span>
                                           <div class="controls">
                                               <asp:ListBox ID="LstCaseStatus" runat="server" CssClass="form-control m-bot15" Height="80px" ToolTip="Court Status">
                                    <asp:ListItem Value="0" Selected="True">---Choose----</asp:ListItem>
                                    <asp:ListItem Value="2">Arrangement</asp:ListItem>
                                    <asp:ListItem Value="3">Pre Trial Setting</asp:ListItem>
                                    <asp:ListItem Value="5">JudgeTrial</asp:ListItem>
                                    <asp:ListItem Value="4">Jury Trail Setting</asp:ListItem>
                                    <asp:ListItem Value="52">Non Issue</asp:ListItem>
                                    <asp:ListItem Value="57">Scire</asp:ListItem>
                                    <asp:ListItem Value="51">Other</asp:ListItem>
                                </asp:ListBox>
                                           </div>
                                    </div>
                               </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                   <div class="form-group">
                                   <%--   <label class="form-label">Court Status</label>--%>
                                       <span class="desc"></span>
                                           <div class="controls">
                                              <asp:CheckBox ID="cbowesdetail" runat="server" CssClass="checkbox-custom" Text="$ Detail"
                                    Checked="true" />
                                           </div>
                                    </div>
                               </div>
                                <div class="clearfix"></div>
                                <hr />
                                <asp:Button ID="Button_Submit" runat="server" CssClass="btn btn-primary" Text="Submit"
                                    OnClientClick="return CheckCourtNumber();"></asp:Button>
                            </div>
                        </div>
                    </section> 
                    <div class="clearfix"></div>
                    <section class="box ">
                        <header class="panel_header">
                             <asp:LinkButton ID="lnk_ShowSetting" Text="Show Setting" runat="server" CssClass="title pull-left"
                        OnClick="lnk_ShowSetting_Click" OnClientClick="ShowProgressOnUpdate();"></asp:LinkButton>
                            <%--<h2 class="title pull-left">Show Setting</h2>--%>

                            <div class="actions panel_actions pull-right">
                    
                	            <a class="box_toggle fa fa-chevron-down"></a>
                    
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12">
                                   <table width="100%" cellspacing="1" cellpadding="1" border="1" style="border-collapse: collapse">
                                    <tr>
                                        <td align="center" style="font-size: 10pt; width: 34%;">
                                            <asp:ImageButton ID="ImageButton2" runat="server" OnClick="SendMail" ImageUrl="../Images/Email.jpg"
                                                ToolTip="Email" OnClientClick="return selectemail();" /><br />
                                            <font face="Arial" size="1">EMAIL</font>
                                        </td>
                                        <td align="center" style="font-size: 10pt; display: none">
                                            <asp:ImageButton ID="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg"
                                                ToolTip="Print"></asp:ImageButton><br />
                                            <font face="Arial" size="1">HTML PRINT</font>
                                        </td>
                                        <%--<td align="center" style="font-size: 10pt">
                                            <asp:ImageButton ID="imgExp_Ds_to_Excel" runat="server" ImageUrl="../Images/excel_icon.gif"
                                                Width="81px" Height="36px" ToolTip="Excel"></asp:ImageButton><br />
                                            <font face="Arial" size="1">EXCEL</font>
                                        </td>--%>
                                        <td align="center" style="font-size: 10pt; width: 33%">
                                            <asp:ImageButton ID="btnPDFCourts" runat="server" ImageUrl="../Images/pdfall.jpg"
                                                OnClick="img_PDFCourt" ToolTip="Print All Court" Width="39px" />
                                            <asp:DropDownList ID="ddl_Copies" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem Selected="True">5</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <font face="Arial" size="1">MULTIPLES</font>
                                        </td>
                                        <td align="center" style="font-size: 10pt; width: 33%">
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/pdfRM.JPG" ToolTip="Court Room"
                                                OnClick="Single_Click" />
                                            <br />
                                            <font face="Arial" size="1">SINGLES</font>
                                        </td>
                                        <%--<td align="center" style="font-size: 10pt; width: 90px;">
                                            <asp:ImageButton ID="Button_CR" runat="server" ImageUrl="~/Images/Cr.jpg" ToolTip="CrystalReport Version"
                                                Height="41px" OnClientClick="return OpenCRAll()" />&nbsp;&nbsp;<br />
                                            <font face="Arial" size="1">PDF</font>
                                        </td>--%>
                                    </tr>
                                </table>
                               </div>
                                <hr />
                                <asp:Panel id="td_iframe" runat="server"  Visible="false">
                                <div class="col-md-12" >
                                <iframe id="reportFrame" runat="server" tabindex="0" frameborder="1" scrolling="auto"
                        width="98%" height="99%"></iframe>
                                    </div>
                                    </asp:Panel>
                            </div>
                        </div>
                    </section>
                 
                </div>
            </section>
       </section>
            
        </div>
           <asp:HiddenField ID="hf_emails" runat="server" />

                    <aspnew:UpdatePanel ID="upnlcourt" runat="server">
                                    <ContentTemplate>
                                        <ajaxToolkit:ModalPopupExtender ID="MPEShowSettingPopup" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlShowSettingPopup" TargetControlID="Button2">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <%--CancelControlID="lnkbtnCancelPopUp"--%>
                                        <asp:Button ID="Button2" runat="server" Style="display: none" Text="Button" />
                                        <asp:Panel ID="pnlShowSettingPopup" runat="server" Width="550px">
                                            <uc1:AttorneyScheduler ID="AttorneySchedulerID" runat="server" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp35:AsyncPostBackTrigger ControlID="lnk_ShowSetting" EventName="Click" />
                                    </Triggers>
                                </aspnew:UpdatePanel>

                      <ajaxToolkit:ModalPopupExtender ID="MPELoding" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="Loading" TargetControlID="Button1">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Button ID="Button1" runat="server" Style="display: none" />
                    <div id="Loading">
                        <img alt="Please wait" src="../Images/page_loading_ani.gif" />
                    </div>
    
    </form>

     <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

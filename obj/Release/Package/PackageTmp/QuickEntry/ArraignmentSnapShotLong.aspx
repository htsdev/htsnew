<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.QuickEntry.ArraignmentSnapShotLong" Codebehind="ArraignmentSnapShotLong.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Scheduled Arraignments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function Hide()
		{
			if( document.getElementById("ddl_WeekDay").value=="-1")
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='none';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';				
			}
			else if(document.getElementById("ddl_WeekDay").value!=99)
			{
				document.getElementById("jt").style.display='none';
				document.getElementById("jtgrd").style.display='none';
				document.getElementById("ost").style.display='none';
				document.getElementById("ostgrd").style.display='none';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='none';
				document.getElementById("a2").style.display='none';
				document.getElementById("a3").style.display='none';
			}
			
			else
			{
				document.getElementById("jt").style.display='block';
				document.getElementById("jtgrd").style.display='block';
				document.getElementById("ost").style.display='block';
				document.getElementById("ostgrd").style.display='block';
				document.getElementById("uaagrd").style.display='block';
				document.getElementById("a1").style.display='block';
				document.getElementById("a2").style.display='block';
				document.getElementById("a3").style.display='block';				
			}
		}
		</script>
	</HEAD>
	<body onload="javascript: Hide();" MS_POSITIONING="GridLayout">
		<DIV style="LEFT: 104px; WIDTH: 10px; POSITION: absolute; TOP: 24px; HEIGHT: 10px" ms_positioning="FlowLayout">
			<FORM id="Form2" method="post" runat="server">
				<TABLE id="TableMain" cellSpacing="0" cellPadding="0" width="780" align="left" border="0">
					<tr>
						<td>
							<uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu>
						</td>
					</tr>
					<tr>
						<td height="9">
						</td>
					</tr>
					<TR>
						<TD><asp:dropdownlist id="ddl_WeekDay" runat="server" AutoPostBack="True" CssClass="clsinputcombo">
								<asp:ListItem Value="-1">Select a Day</asp:ListItem>
								<asp:ListItem Value="2">Monday</asp:ListItem>
								<asp:ListItem Value="3">Tuesday</asp:ListItem>
								<asp:ListItem Value="4">Wednesday</asp:ListItem>
								<asp:ListItem Value="5">Thursday</asp:ListItem>
								<asp:ListItem Value="6">Friday</asp:ListItem>
								<asp:ListItem Value="99">Not Assigned</asp:ListItem>
							</asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lbl_CurrDateTime" runat="server" Font-Names="Times New Roman" Font-Bold="True"></asp:label></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 9px"></TD>
					</TR>
					<TR>
						<TD><asp:label id="lbl_Message" runat="server" CssClass="Label" Font-Bold="True" ForeColor="Green"
								Visible="False">There is currently no clients arraigned for arraignments</asp:label></TD>
					</TR>
					<TR background="../../images/separator_repeat.gif">
						<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" colSpan="5"
							height="9">
						</TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:imagebutton id="imgPrint_Ds_to_Printer" runat="server" ImageUrl="../Images/print_02.jpg" Visible="False"
								Width="49px" Height="39px"></asp:imagebutton>
						</TD>
					</TR>
					<TR background="../../images/separator_repeat.gif">
						<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" colSpan="5"
							height="9">
						</TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="TableGrid" cellSpacing="0" cellPadding="0" width="100%" bgColor="white" border="0">
								<tr>
									<td style="HEIGHT: 16px"></td>
								</tr>
								<TR>
									<TD id="uaagrd" style="HEIGHT: 9px" width="780" colSpan="5" height="9"><asp:datagrid id="dg_Report" runat="server" EnableViewState="False" AutoGenerateColumns="False"
											BorderColor="Black" BorderStyle="Solid" Width="100%">
											<ItemStyle Font-Names="Times New Roman"></ItemStyle>
											<HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No:">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lbl_sno" runat="server">0</asp:Label>
														<asp:Label id="lbl_Ticketid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Flags">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Case #">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
													<ItemTemplate>
														<asp:HyperLink id=hlnkCaseNo runat="server" Font-Bold="True" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>' Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>' Font-Underline="True">
														</asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Seq">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Label15 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Last Name">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="First Name">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="DL">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="DOB">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lbl_dob runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="MID">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MID") %>' >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Set Date">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lbl_arrdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TrialDateTime") %>' >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td style="HEIGHT: 16px"></td>
								</tr>
								<tr>
									<td id="jt" style="HEIGHT: 14px"><STRONG>Judge Trials</STRONG></td>
								</tr>
								<tr>
									<td id="a1" style="HEIGHT: 14px"></td>
								</tr>
								<TR>
									<TD id="jtgrd" style="HEIGHT: 9px" width="780" colSpan="5" height="9"><asp:datagrid id="dg_ReportJT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
											BorderColor="Black" BorderStyle="Solid" Width="100%">
											<ItemStyle Font-Names="Times New Roman"></ItemStyle>
											<HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No:">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lbl_sno1" runat="server">0</asp:Label>
														<asp:Label id=lbl_Ticketid1 runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Flags">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label141" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>' >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Case #">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
													<ItemTemplate>
														<asp:HyperLink id=hlnkCaseNo1 runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>' Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>' Font-Underline="True">
														</asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Seq">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Label151 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Last Name">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label161" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="First Name">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label171" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="DL">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label181" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="DOB">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>' >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Court Information">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lbl_CCDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
														</asp:Label>
														<asp:Label id=lbl_CCNum runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Officer Day">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lbl_OfficerDay runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OfficerDay") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td id="a2" style="HEIGHT: 16px"></td>
								</tr>
								<tr>
									<td id="ost" style="HEIGHT: 16px"><STRONG>Outside Trials</STRONG></td>
								</tr>
								<tr>
									<td id="a3" style="HEIGHT: 16px"></td>
								</tr>
								<TR>
									<TD id="ostgrd" style="HEIGHT: 9px" width="780" colSpan="5" height="9"><asp:datagrid id="dg_ReportOT" runat="server" EnableViewState="False" AutoGenerateColumns="False"
											BorderColor="Black" BorderStyle="Solid" Width="100%">
											<ItemStyle Font-Names="Times New Roman"></ItemStyle>
											<HeaderStyle ForeColor="Black" BackColor="#FFCC00"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No:">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lbl_sno2" runat="server">0</asp:Label>
														<asp:Label id=lbl_Ticketid2 runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Flags">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Label142 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Case #">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman" Font-Bold="True"></ItemStyle>
													<ItemTemplate>
														<asp:HyperLink id=hlnkCaseNo2 runat="server" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.TicketID_PK"),"&amp;search=0") %>' Target="_self" Text='<%# DataBinder.Eval(Container.DataItem, "TicketNumber_PK") %>' Font-Underline="True">
														</asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Seq">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=Label152 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Violations") %>'>
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Last Name">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label162" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="First Name">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label172" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Firstname") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="DL">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label182" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DLNumber") %>' >
														</asp:Label>&nbsp;
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="DOB">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lbl_dob" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DOB") %>' >
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Court Information">
													<HeaderStyle Font-Names="Times New Roman" Font-Bold="True"></HeaderStyle>
													<ItemStyle Font-Names="Times New Roman"></ItemStyle>
													<ItemTemplate>
														<asp:Label id=lbl_CCDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentDateSet") %>'>
														</asp:Label>
														<asp:Label id=lbl_CCNum runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtNum") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR background="../../images/separator_repeat.gif">
						<TD style="HEIGHT: 9px" width="780" background="../../images/separator_repeat.gif" colSpan="5"
							height="9">
						</TD>
					</TR>
					<tr>
						<td>
							<uc1:footer id="Footer1" runat="server"></uc1:footer>
						</td>
					</tr>
				</TABLE>
			</FORM>
		</DIV>
	</body>
</HTML>

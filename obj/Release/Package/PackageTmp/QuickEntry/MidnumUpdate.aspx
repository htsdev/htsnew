<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MidnumUpdate.aspx.cs" Inherits="lntechNew.QuickEntry.MidnumUpdate" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Mid-number Update</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    
    function show_popup()
    {
    
        if(document.getElementById('txt_CauseNum').value == '' || document.getElementById('txt_MidNum').value== '')
        {
        alert('You must enter both cause number and mid number');
        return(false);
        }
    
        var WinSettings = "center:yes;resizable:no;dialogHeight:200px;dialogwidth:555px;scroll:no";
        var Arg = 1;
        var cause = document.getElementById('txt_CauseNum').value;
        var mid = document.getElementById('txt_MidNum').value;
        var conf = window.showModalDialog("popup_UpdateMidnum.aspx?CauseNo="+cause+"&MidNo="+mid,Arg,WinSettings);
        
        if(conf == -1 || conf == null)
        {
            document.getElementById('lbl_Message').innerHTML = 'Record not updated';
            return false;
        }
        else
            return true;
    }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
    <tr>
    <td>
        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
    </td>
    </tr>
    <tr>
    <td background="../Images/subhead_bg.gif" height="34" class ="clssubhead">
        Midnumber Update</td>
    </tr>
        <tr>
            <td>
            <table width="100%">
            <tr>
            <td align="right" style="width: 48%">
                <asp:Label ID="Label1" runat="server" CssClass="label" Text="Cause Number"></asp:Label></td>
            <td align="left" width="50%">
                <asp:TextBox ID="txt_CauseNum" runat="server" CssClass="clsinputadministration"></asp:TextBox></td>
            </tr>
                <tr>
                    <td align="right" style="width: 48%">
                        <asp:Label ID="Label2" runat="server" CssClass="label" Text="Mid Number"></asp:Label></td>
                    <td align="left" width="50%">
                        <asp:TextBox ID="txt_MidNum" runat="server" CssClass="clsinputadministration"></asp:TextBox></td>
                </tr>
            </table>
                </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td align="center" width="100%" background="../Images/separator_repeat.gif"  height="11">
            </td>
        </tr>
        <tr>
        <td  align="center"><asp:Button ID="btn_Update" runat="server" Text="Update" CssClass="clsbutton" OnClientClick="return show_popup()" OnClick="btn_Update_Click" /></td>
        </tr>
        <tr>
            <td >
                <uc2:Footer ID="Footer1" runat="server" />
            </td>
        </tr>
    </table>
    
    </div>
    </form>
</body>
</html>

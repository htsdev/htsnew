<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocketsReports.aspx.cs" Inherits="lntechNew.ReturnedDockets.DocketsReports" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Scanned Dockets</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

    <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />



        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

        <script src="../Scripts/Dates.js" type="text/javascript"></script>


<%--
    <SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>--%>
    <script type="text/javascript">
		

function isDate()
{
     
    var startdate = document.getElementById("txt_StartDate").value;
	var enddate = document.getElementById("txt_EndDate").value;
	
	if(validate_date(enddate)==true && validate_date(startdate)==true)
	 {
	    return true;
	 }
	 
	 else
	 {
	    return false;
	 }
	
}
function validate_date(inputwidget)
{
    
	var datestring = inputwidget;
	var first = datestring.indexOf('/');
	var last = datestring.lastIndexOf('/');

    if ( first == -1 ) 
    {
        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
		return false;
    }
    if ( last == -1 ) 
    {
        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	    return false;
    }
    

	    var daypart= datestring.substring(0, first);
	    var monthpart  = datestring.substring(first + 1, last);
	    var yearpart = datestring.substring(last + 1, datestring.length);
	   
    	
	    if ((datestring.length > 0) && 
	         ((yearpart.length < 4) || (yearpart.length>4) || (yearpart<1900) || !isvaliddate(yearpart, monthpart, daypart)))  
	        
	    {
		   
		    inputwidget.value = "";
		    alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
		    return false;
	    }
	    else if(isNaN(yearpart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else if(isNaN(monthpart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else if(isNaN(daypart))
	    {
	        alert("The date entered is not valid and date must be in the mm/dd/yyyy format.");
	        return false;
	    }
	    else
	    {
	
		    return true;
	    }
}

function isvaliddate(year, month, day)
{
	//return (month >= 1) && (month <= 12) && (day >= 1) && (day <= mostdaysinmonth(month, year));
	return (day >= 1) && (day <= 12) && (month >= 1) && (month <= mostdaysinmonth(day, year));
}
//End of Date validation

		function PopUpCallBack(DocID)
			{
				
				window.open ("PreviewPDF.aspx?docid="+ DocID);
				return false;
			
			}
			function CheckDel()
			{
				
				var x=confirm("Are you sure you want to delete this docket! \r\n Click [OK] for yes or [Cancel] for No.");
				if(x==false)
				{
				    return false;
				}				
			}
		function validatepage()
		{
			//a;
			var d1 = document.getElementById("txt_StartDate").value;
			var d2 = document.getElementById("txt_EndDate").value;			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("Please Enter start date less than end date");
					document.getElementById("txt_EndDate").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
			var dtStartdate=document.getElementById("txt_StartDate").value;
			var dtEndDate=document.getElementById("txt_EndDate").value;
			if(dtStartdate > dtEndDate)
			{ 
				alert("PLease Specify Valid Future Date!");
				return false;
			}
		}
    </script>
</head>

    <body class=" ">
        
        <form id="form1" runat="server">
            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">
            
                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">
                                    <!-- PAGE HEADING TAG - START --><h1 class="title">Scanned Dockets</h1><!-- PAGE HEADING TAG - END -->                
                                </div>
                        
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">

                                <div class="content-body">

                                    <div class="row">
                                        <div class="col-md-4 col-sm-5 col-xs-6">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Start Date</label>
                                                <div class="controls">
                                                    <%--<ew:calendarpopup id="cal_StartDate" runat="server" PadSingleDigits="True"
													    ShowClearDate="True" Culture="(Default)" AllowArbitraryText="True" ShowGoToToday="True" CalendarLocation="Left"
													    ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" EnableHideDropDown="True" DisableTextboxEntry="False">
													    <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
												    </ew:calendarpopup>--%>
                                                    <asp:TextBox ID="txt_StartDate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4 col-sm-5 col-xs-6">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">End Date</label>
                                                <div class="controls">
                                                    <%--<ew:calendarpopup id="cal_EndDate" runat="server" PadSingleDigits="True" ShowClearDate="True"
													    Culture="(Default)" AllowArbitraryText="True" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
													    EnableHideDropDown="True" DisableTextboxEntry="False">
													    <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
												    </ew:calendarpopup>--%>
                                                    <asp:TextBox ID="txt_EndDate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4 col-sm-5 col-xs-6">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Search By</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_SearchBy" runat="server" CssClass="form-control m-bot15" >
                                                        <asp:ListItem Selected="True" Value="1">Scanned Date</asp:ListItem>
                                                        <asp:ListItem Value="2">Docket Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Attorney Name</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_Attorney" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Employee Name</label>
                                                <div class="controls">
                                                    <asp:dropdownlist id="ddl_Emp" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 pull-right">
                                            <asp:button OnClientClick="return isDate();" id="btnSearch" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSearch_Click"></asp:button>
                                        </div>
                                    </div>

                                </div>
                            </section>

                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:datagrid id="dgresult" runat="server" CssClass="table" AutoGenerateColumns="False" OnItemCommand="dgresult_ItemCommand">
										        <Columns>
											        <asp:TemplateColumn HeaderText="S.No" >
												        <ItemTemplate>
													        <asp:Label id="lbl_sno" runat="server" CssClass="form-label"></asp:Label>
												        </ItemTemplate>
											        </asp:TemplateColumn>
											        <asp:TemplateColumn HeaderText="Scan Date">
												        <ItemTemplate>
													        <asp:Label id=lblScanDate runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ScanDate") %>'>
													        </asp:Label>
												        </ItemTemplate>
											        </asp:TemplateColumn>
											        <asp:TemplateColumn HeaderText="Docket Date">
												        <ItemTemplate>
													        <asp:LinkButton id=lnkbtn_docdate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocketDate", "{0:d}") %>'>
													        </asp:LinkButton>
												        </ItemTemplate>
											        </asp:TemplateColumn>
											        <asp:TemplateColumn HeaderText="Attorney Name">
												        <ItemTemplate>
													        <asp:Label id=lbl_attname runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Attorney") %>'>
													        </asp:Label>
												        </ItemTemplate>
											        </asp:TemplateColumn>
											        <asp:TemplateColumn HeaderText="Pages">
												        <ItemTemplate>
													        <asp:Label id=lbl_pageno runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.DocCount") %>'>
													        </asp:Label>
													        <asp:Label id=lbl_docid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bookid") %>' Visible="False">
													        </asp:Label>
												        </ItemTemplate>
											        </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="img_Delete" ImageUrl="~/Images/DELETE.jpg" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.bookid") %>' />
												        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Emp Abb">
                                                        <ItemTemplate>
													        <asp:Label id=lbl_EmpAbb runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'>
													        </asp:Label>
												        </ItemTemplate>
                                                    </asp:TemplateColumn>
										        </Columns>
									        </asp:datagrid>
                                            
                                            <asp:label id="lbl_error" runat="server" ForeColor="Red" CssClass="form-label"></asp:label></

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        
                        <!-- MAIN CONTENT AREA ENDS -->
                    
                    </section>
                </section>
                <!-- END CONTENT -->


            </div>
            <!-- END CONTAINER -->
        </form>
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 
        
        <script type="text/javascript">
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });
        </script>
</body>




























<%--<body>
    <form id="form1" runat="server">
    <div>
        <TABLE id="TableMain" 
				cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 816px" colSpan="4">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="10"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 46px" colSpan="4">
									<TABLE id="Table2" style="WIDTH: 784px; HEIGHT: 14px" cellSpacing="1" cellPadding="1" width="784"
										border="0">
										<TR>
											<TD class="clsLeftPaddingTable" ><STRONG>Start Date:</STRONG></TD>
											<TD class="clsLeftPaddingTable" >
												<ew:calendarpopup id="cal_StartDate" runat="server" PadSingleDigits="True"
													ShowClearDate="True" Culture="(Default)" AllowArbitraryText="True" ShowGoToToday="True" CalendarLocation="Left"
													ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Width="99px" Font-Size="8pt" Font-Names="Tahoma" EnableHideDropDown="True" DisableTextboxEntry="False">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px; HEIGHT: 23px">
                                                <strong>End Date:</strong></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 128px; HEIGHT: 23px"><ew:calendarpopup id="cal_EndDate" runat="server" PadSingleDigits="True" ShowClearDate="True"
													Culture="(Default)" AllowArbitraryText="True" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
													Width="102px" Font-Size="8pt" Font-Names="Tahoma" EnableHideDropDown="True" DisableTextboxEntry="False">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 106px; HEIGHT: 23px">
                                                <strong>Search By:</strong></TD>
											<TD class="clsLeftPaddingTable" style="HEIGHT: 23px"><asp:dropdownlist id="ddl_SearchBy" runat="server" Width="127px" CssClass="ClsInputCombo" >
                                                <asp:ListItem Selected="True" Value="1">Scanned Date</asp:ListItem>
                                                <asp:ListItem Value="2">Docket Date</asp:ListItem>
                                            </asp:DropDownList></TD>
										</TR>
										<TR>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px"><STRONG>Attorney Name:</STRONG></TD>
											<TD class="clsLeftPaddingTable" ><asp:dropdownlist id="ddl_Attorney" runat="server" Width="127px" CssClass="ClsInputCombo">
                                            </asp:DropDownList></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px">
                                                <strong>Employee Name:</strong></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 128px"><STRONG><asp:dropdownlist id="ddl_Emp" runat="server" Width="127px" CssClass="ClsInputCombo">
                                            </asp:DropDownList></STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 106px"><asp:button OnClientClick="return isDate();" id="btnSearch" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btnSearch_Click"></asp:button></TD>
											<TD class="clsLeftPaddingTable"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 11px" align="left" background="../Images/separator_repeat.gif" colSpan="4">&nbsp;
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="4"><asp:datagrid id="dgresult" runat="server" Width="784px" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
										BorderColor="White" BorderStyle="None" OnItemCommand="dgresult_ItemCommand">
										<Columns>
											<asp:TemplateColumn HeaderText="S.No" >
												<HeaderStyle HorizontalAlign="Left" Width=25px CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno" runat="server" CssClass="label"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Scan Date">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblScanDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ScanDate") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Docket Date">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id=lnkbtn_docdate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocketDate", "{0:d}") %>'>
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Attorney Name">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_attname runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Attorney") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Pages">
												<HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_pageno runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DocCount") %>'>
													</asp:Label>
													<asp:Label id=lbl_docid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bookid") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Delete">
                                            <HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="img_Delete" ImageUrl="~/Images/DELETE.jpg" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.bookid") %>' />
												</ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Emp Abb">
                                            <HeaderStyle HorizontalAlign="Left"  CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
													<asp:Label id=lbl_EmpAbb runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'>
													</asp:Label>
												</ItemTemplate>
                                            </asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%" colSpan="4" height="11">
									<asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="4">
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
    </div>
    </form>
</body>--%>
</html>

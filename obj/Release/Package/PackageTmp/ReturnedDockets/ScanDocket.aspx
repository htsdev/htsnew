<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanDocket.aspx.cs" Inherits="lntechNew.ReturnedDockets.ScanDocket" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Scan Dockets</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <%=Session["objTwain"].ToString()%>
</head>
<body>
    <form id="form1" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server"/>
        <div class="page-container row-fluid container-fluid">

 <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">


                  <div class="col-xs-12">
           
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <asp:Label ID="lblMessage" CssClass="form-label" runat="server" ForeColor="Red" Height="16px"></asp:Label>
                </div>
                 

                 
        </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Scan Dockets</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>
           
             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Scan Dockets</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Date of Docket</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="cal_DocketDate" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                   <%-- <ew:calendarpopup id="cal_DocketDate" runat="server" allowarbitrarytext="False" CssClass="form-control"
                                        calendarlocation="Bottom" controldisplay="TextBoxImage" culture="(Default)" enablehidedropdown="True"
                                        font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" padsingledigits="True"
                                        showcleardate="True" showgototoday="True" text=" " tooltip="Call Back Date" upperbounddate="12/31/9999 23:59:00"
                                        width="80px"> <TEXTBOXLABELSTYLE CssClass="clstextarea" />
                                        <WEEKDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" />
                                        <MONTHHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Yellow" />
                                        <OFFMONTHSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray" BackColor="AntiqueWhite" />
                                        <GOTOTODAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><TODAYDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="LightGoldenrodYellow" /><DAYHEADERSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Orange" /><WEEKENDSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="LightGray" /><SELECTEDDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="Yellow" /><CLEARDATESTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /><HOLIDAYSTYLE Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black" BackColor="White" /></ew:calendarpopup>--%>
                                    </div>
                                                                </div>
                         </div>

                      <div class="col-md-6">
                                                            <div class="form-group">
                               <label class="form-label">Attorney:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddl_Attorney" runat="server" CssClass="form-control">
                                    </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>
                     <div class="clearfix"></div>
                       <div class="col-md-4">
                                                            <div class="form-group">
                              
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:Button ID="btn_Scan" OnClientClick="return StartScan();" runat="server"
                                            Text="Scan Now" CssClass="btn btn-primary" OnClick="btn_Scan_Click" />
                                     <input id="Adf" runat="server" checked="checked" name="Adf" type="checkbox" />Use
                                    Feeder
                                    

                                    </div>
                                                                </div>
                         </div>

                    
 <div class="clearfix"></div>





                    </div>
                     </div>
                 </section>





      <%--       <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <uc2:footer id="Footer1" runat="server"></uc2:footer>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>--%>




            <table>
                <tr>
                    <td colspan="2" style="visibility: hidden">
                        <asp:TextBox ID="txtImageCount" runat="server"></asp:TextBox><asp:TextBox ID="txtQuery"
                            runat="server"></asp:TextBox><asp:TextBox ID="txtempid" runat="server"></asp:TextBox><asp:TextBox
                                ID="txtsessionid" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtbID" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtnoofscandoc" runat="server">1</asp:TextBox></td>
                </tr>
            </table>
















            </section>
                      </section>
            </div>
                     
    
    </form>
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
     <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <DIV id="ErrorString"></DIV>
		<IMG height="1" src="" width="1" name="Img1"> 
		<!-- added by ozair-->
		<SCRIPT language="JavaScript">
function StartScan()
{
    document.getElementById("lblMessage").innerText="";
	if(document.getElementById("ddl_Attorney").value=="-1")
	{
	    //alert("Plese Select Attorney.");
	    $("#txtErrorMessage").text("Please Select Attorney");
	    $("#errorAlert").modal();
		document.getElementById("ddl_Attorney").focus();
		return false;
	}

    var sid= document.getElementById("txtsessionid").value;
	var eid= document.getElementById("txtempid").value;
	var path="<%=ViewState["vNTPATHScanDocketTemp"]%>";
	var Type='network';		
	var AutoFeed=1;
	var sel=OZTwain1.SelectScanner();
	if(sel=="Success")
	{
	    if (document.getElementById("Adf").checked == true)
	    {
	        AutoFeed=-1;
	    }	    
	    
	    OZTwain1.Acquire(sid,eid,path,Type,AutoFeed);
	    //
	    if (document.getElementById("Adf").checked == false)
	      {
		    var x= confirm("Do you want to scan more pages [OK] Yes [Cancel] NO.");
		    if(x)
		    {
			    //OZTwain1.Acquire(sid,eid,path,Type);
			    StartScan();
		    }	
		    else
		    {
		        document.getElementById("Adf").checked = true;
		    }		
	      }
    }
    else if(sel=="Cancel")
    {
        //alert("Operation canceled by user!");
        $("#txtErrorMessage").text("Operation canceled by user!");
        $("#errorAlert").modal();
        return false;
    }
    else 
    {
        // alert("Scanner not installed.");
        $("#txtErrorMessage").text("Scanner not installed.");
        $("#errorAlert").modal();
	    return false;
    }
    return true;
}
		</SCRIPT>
		<!-- added by ozair-->

      <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

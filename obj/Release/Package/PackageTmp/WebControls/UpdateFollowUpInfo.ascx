<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateFollowUpInfo.ascx.cs"
    Inherits="HTP.WebControls.UpdateFollowUpInfo" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<script src="../Scripts/Dates.js" type="text/javascript"></script>

<script src="../Scripts/jsDate.js" type="text/javascript"></script>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
<!-- Favicon -->
<link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
<!-- For iPhone -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
<!-- For iPhone 4 Retina display -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
<!-- For iPad -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
<!-- For iPad Retina display -->




<!-- CORE CSS FRAMEWORK - START -->
<link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
<!-- CORE CSS FRAMEWORK - END -->

<!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->

    <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />
<!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


<!-- CORE CSS TEMPLATE - START -->
<link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
<!-- CORE CSS TEMPLATE - END -->

<script language="javascript" type="text/javascript">

    //Ozair 8101 09/29/2010 code refactored where required

    function ClosePopUp(backdiv, frontdiv) {

        document.getElementById(backdiv).style.display = "none";
        document.getElementById(frontdiv).style.display = "none";
        return false;
    }

    function closeModalPopup(popupid) {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }


    function ValidateInput() {

        var oldval = document.getElementById('<%=this.hf_FollowUpDate.ClientID %>').value;
    var newval = document.getElementById("<%= this.ClientID %>_cal_followupdate").value;

    var date = new Date();
    if ((newval < date)) {
        // modify zahoor 4481 08/08/2008
        //alert("Please specify valid follow up date");
        $("#txtErrorMessage").text("Please specify valid follow up date");
        $("#errorAlert").modal();
        // Add Zahoor 4481 08/9/208
        // to avoid closing popup.
        return false;
    }

    if (oldval != newval) {   //Sabir Khan 5977 07/24/2009 To prevent if there is only general comments...
        var genStr = document.getElementById("<%= this.ClientID %>_tb_GeneralComments").value;
        if (trim(genStr) == "") {
            // alert("Please enter General Comments");
            $("#txtErrorMessage").text("Please enter General Comments");
            $("#errorAlert").modal();
            return false;
        }
    }
    return CheckDate(document.getElementById("<%= this.ClientID %>_cal_followupdate").value, document.getElementById("<%= this.ClientID %>_cal_followupdate"))
    ClosePopUp('<%= this.pnl_control.ClientID %>', '<%=this.Disable.ClientID %>');
}
//Sabir Khan 5977 07/24/2009 Used to trim the general comments...	
function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}

function ShowPopUp_1(generalcomments, ticid, ticketid, followupdate, lastname, firstname, ticketnumber, causeno) {
    debugger;
    var backdiv = '<%=this.Disable.ClientID %>'
    var frontdiv = '<%= this.pnl_control.ClientID %>'


    document.getElementById('<%= this.tb_GeneralComments.ClientID %>').value = "";

    setpopuplocation(backdiv, frontdiv);
    document.getElementById('<%= this.ClientID %>_chk_UpdateAll').checked = false;
   document.getElementById('<%= this.ClientID %>_lblComments').innerHTML = generalcomments;
    document.getElementById('<%= this.ClientID %>_cal_followupdate').value = followupdate;
    document.getElementById('<%=this.hf_ticketid.ClientID %>').value = ticketid;
    document.getElementById('<%=this.hf_ticid.ClientID %>').value = ticid;
    //Ozair 5723 04/03/2009 Full Name in First Last format.
    document.getElementById("<%= this.ClientID %>_lbl_FullName").innerHTML = firstname + " " + lastname;
    document.getElementById("<%= this.ClientID %>_lblTicketNumber").innerHTML = ticketnumber;
    document.getElementById("<%= this.ClientID %>_lblCauseNo").innerHTML = causeno;
    document.getElementById('<%=this.hf_FollowUpDate.ClientID %>').value = followupdate;
    document.getElementById('<%=this.lblCurrentFollup.ClientID %>').innerHTML = followupdate;
    return false;
}

// Set Popup Control Location
function setpopuplocation(backdiv, frontdiv) {

    var top = 400;
    var left = 400;
    var height = document.body.offsetHeight;
    var width = document.body.offsetWidth

    // Setting Panel Location
    if (width > 1100 || width <= 1280) left = 575;
    if (width < 1100) left = 500;

    // Setting popup display
    document.getElementById(frontdiv).style.top = top + "px";
    document.getElementById(frontdiv).style.left = left + "px";

    if (document.body.offsetHeight > document.body.scrollHeight)
        document.getElementById(backdiv).style.height = document.body.offsetHeight;
    else
        document.getElementById(backdiv).style.height = document.body.scrollHeight;

    document.getElementById(backdiv).style.width = document.body.offsetWidth;
    document.getElementById(backdiv).style.display = ''
    document.getElementById(frontdiv).style.display = ''

    document.body.scrollTop = 0;
}
// Noufil 3589 05/29/2008 FUnction for date range control
function CheckDate(seldate, tbID) {
    today = new Date();
    debugger;
    var diff = Math.ceil(DateDiff(seldate, today));
    court = document.getElementById('<%=this.hf_court.ClientID %>').value;
	    courtname = document.getElementById('<%=this.hf_courtname.ClientID %>').value;

	    //Waqas 5653 03/24/2009 Not For No LOR
	    //Waqas 5697 03/26/2009 For HMC-FTA Follow Up
	    //Nasir 5938 05/26/2009 For NOS Follow Up
	    //Sabir 5977 07/02/2009 For HMCSettingDiscrepancy Follow Up
	    //Nasir 7234 01/08/2010 for HMCSameDayArr 

	    //Saeed 8101 09/24/2010 HCJP Auto Update Alert condition added.
	    //Muhammad Nadir Siddiqui 9134 05/02/2011 Added 'NO LOR Confirmation'
	    if (court != "NO LOR" && court != "NO LOR Confirmation" && court != "HMC-FTA" && court != "Non Hmc" && court != "Split" && court != "ALRHEARING" && court != "NOS" && court != "HMCSettingDiscrepancy" && court != "Bond Alert" && court != "HMCSameDayArr" && court != "HCJPAutoUpdateAlert" && court != "BadAddresses") {  //alert('test');
	        //ozair 4442 07/22/2008 ALR Courts(houston, forte bend, conroe)
	        //Fahad 5753 04/10/2009 exclude Cases of Split info
	        if (((court == "3047" || court == "3048" || court == "3070") && diff > 7) || ((court == "3047" || court == "3048" || court == "3070") && diff < 0)) {
	            // alert ("Please enter date within one week from today for court "+courtname )
	            $("#txtErrorMessage").text("Please enter date within one week from today for court " + courtname);
	            $("#errorAlert").modal();
	            var tst1 = formatDate(dateAdd("d", 7, today), "MM/dd/yyyy");
	            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = tst1;
	            return false;
	        }
	        //Muhammad Ali 7747 06/30/2010 for Missing Cause Follow update.
	        if (((court == "MissingCauseNumber") && diff > 7) || ((court == "MissingCauseNumber") && diff < 0)) {
	            //alert ("Please enter date within one week from today for Missing Cause Number Follow Up Date" );
	            $("#txtErrorMessage").text("Please enter date within one week from today for Missing Cause Number Follow Up Date");
	            $("#errorAlert").modal();
	            var tst1 = formatDate(dateAdd("d", 7, today), "MM/dd/yyyy");
	            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = tst1;
	            return false;
	        }
	            // Abid Ali 4912 11/18/2008 -- add one more criteria (court != "Traffic Waiting")
	            // Abid Ali 5018 12/26/2008 -- add one more criteria (court != "Family Waiting")
	            // Ozair 5098 01/13/2009 -- add one more criteria (court != "ContractFollowUp") for no digned contract report
	            // Noufil 5691 04/06/2009 Clause added for Past court date report.
	            // Saeed 7844 06/26/2010 add two more criterias (court != "BadEmailAddress" && (court != "AddressValidation"))
	            //Saeed 7791 08/13/2010 add one criteria of court!= "Family Waiting" 
	        else if (((court != "3047" && court != "3048" && court != "3070" && court != "" && court != "Waiting" && court != "Traffic Waiting" && court != "Family Waiting" && court != "BadEmailAddress" && court != "AddressValidation" && court != "ContractFollowUp" && court != "PastCourtDateHMC") && diff > 14) || ((court != "3047" && court != "3048" && court != "3070" && court != "3041" && court != "" & court != "Waiting" && court != "Traffic Waiting" && court != "Family Waiting" && court != "ContractFollowUp" && court != "PastCourtDateHMC" && court != "BadEmailAddress" && court != "AddressValidation") && diff < 0)) {
	            //alert ("Please enter date within two week from today for court "+courtname)
	            $("#txtErrorMessage").text("Please enter date within two week from today for court " + courtname);
	            $("#errorAlert").modal();
	            //alert(seldate);
	            var tst = formatDate(dateAdd("d", 14, today), "MM/dd/yyyy");
	            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = tst;
	            return false;
	        }

	        //Yasir Kamal 5734 03/31/2009 followUpdate logic set for 2 business days
	        // added zahoor 4481 8/6/2008
	        // court = ""  No court is defined in bond report case.
	        //Sabir Khan 5727 03/31/2009 Follow update logic has been changed into six from 5 business days...
	        //	        else if (((court == "" ) && diff >2 )|| ((court == "" ) && diff <0))
	        //	        {
	        //	            alert ("Please select follow up date with in 6 business days.")
	        //	            //alert(seldate);
	        //	            var tst1 = formatDate(dateAdd("d", 2, today),"MM/dd/yyyy"); 
	        //	            document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value=tst1;	        
	        //	            return false;
	        //	      }
	    }

	    var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
	    var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth() + 1) + "/" + Date.parseInvariant(seldate).getDate() + "/" + Date.parseInvariant(seldate).getFullYear(), "MM/dd/yyyy");
	    newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
	    // Adil 7752 05/29/2010 Non-business days check moved here from bottom.
	    //Fahad 5098 05/01/2009 Add saturday check 
	    //Ozair 5466 01/28/2009 "&&" changed to "||"
	    if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {
	        //alert("Please select any business day");
	        $("#txtErrorMessage").text("Please select any business day");
	        $("#errorAlert").modal();
	        return false;
	    }
	    // end 4481
	    // Noufil 4980 10/30/2008 follow Up date condition set for Bond waiting Report	  

	    if ((court == "Waiting")) {
	        var datediff = Math.ceil(DateDiff(newseldate, today));

	        if (datediff >= 0) {
	            var i = 0, countbusinessdays = 0, countHolidays = 0;
	            for (i = 0; i <= datediff; i++) {

	                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                    countbusinessdays++;
	                }

	                today = dateAdd("d", 1, today);

	            }


	            //Sabir Khan 5695 03/25/2009 Business days changed from 5 to 6 for bond waiting report...
	            if ((countbusinessdays > 3) || (countbusinessdays == 0)) {
	                // alert("Please select follow up date within next 2 business days");
	                $("#txtErrorMessage").text("Please select follow up date within next 2 business days");
	                $("#errorAlert").modal();
	                return false;
	            }
	        }
	        else {
	            //alert("Please select future date");
	            $("#txtErrorMessage").text("Please select future date");
	            $("#errorAlert").modal();
	            return false;
	        }
	    }
	    if ((court == "")) {
	        var datediff = Math.ceil(DateDiff(newseldate, today));

	        if (datediff >= 0) {
	            var i = 0, countbusinessdays = 0, countHolidays = 0;
	            for (i = 0; i <= datediff; i++) {

	                if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                    countbusinessdays++;
	                }

	                today = dateAdd("d", 1, today);

	            }
	            var courtname = document.getElementById('<%=this.hf_courtname.ClientID %>').value;
	          // Afaq 7752 05/04/2010 add check for dispose alert.               
	          if (courtname == "DisposeAlert") {
	              //Fahad 7752 06/23/2010 Resolved issues of Allowed days
	              if ((countbusinessdays > 6) || (countbusinessdays == 0)) {
	                  // alert("Please select follow up date within next 5 business days");
	                  $("#txtErrorMessage").text("Please select follow up date within next 5 business days");
	                  $("#errorAlert").modal();
	                  return false;
	              }
	          }

          }
          else {
	          // alert("Please select future date");
              $("#txtErrorMessage").text("Please select future date");
              $("#errorAlert").modal();
              return false;
          }
      }

	    //SAEED 7844 06/21/2010 Allow to Add next 5 business days in Follow Up date	   
      if (court == "BadEmailAddress" || court == "AddressValidation") {

          var datediff = Math.ceil(DateDiff(newseldate, today));
          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 0; i <= datediff; i++) {
                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }
                  today = dateAdd("d", 1, today);
              }
              if ((countbusinessdays > 6) || (countbusinessdays == 0)) {
                  //alert("Please select Follow Up Date within next 5 business days from today's Date");
                  $("#txtErrorMessage").text("Please select Follow Up Date within next 5 business days from today's Date");
                  $("#errorAlert").modal();
                  return false;
              }
          }
          else {
              // alert("Please select future date");
              $("#txtErrorMessage").text("Please select future date");
              $("#errorAlert").modal();
              return false;
          }
      }
	    // 7844 end



	    //Fahad 9134 05/2/2011 Allow to Add in Follow Up date in next Show Setting Control business days for NO LOR Confirmation.
      if ((court == "NO LOR Confirmation")) {
          var datediff = Math.ceil(DateDiff(newseldate, today));
          var _BusinessDay = document.getElementById('<%=BusinessDay.ClientID%>').value;

	      if (datediff > 0) {
	          var i = 0, countbusinessdays = 0, countHolidays = 0;
	          for (i = 0; i <= datediff; i++) {

	              if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                  countbusinessdays++;
	              }

	              today = dateAdd("d", 1, today);

	          }
	          if ((countbusinessdays > _BusinessDay) || (countbusinessdays == 0)) {

	              //alert("Please select Follow Up Date in next "+ _BusinessDay +" business day(s) from Today's date.");
	              $("#txtErrorMessage").text("Please select Follow Up Date in next " + _BusinessDay + " business day(s) from Today's date.");
	              $("#errorAlert").modal();
	              return false;
	          }

	      }
	      else {
	          //alert("Please select future date");
	          $("#txtErrorMessage").text("Please select future date");
	          $("#errorAlert").modal();

	          return false;
	      }
      }

	    //Fahad 5722 04/07/2009 Allow to Add 2 business day in Follow Up
      if ((court == "Non Hmc")) {

          var datediff = Math.ceil(DateDiff(newseldate, today));

          if (datediff > 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 0; i <= datediff; i++) {

                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }

                  today = dateAdd("d", 1, today);

              }

              if ((countbusinessdays > 3) || (countbusinessdays == 0)) {
                  //alert("Please select follow up date within next 2 business days");
                  $("#txtErrorMessage").text("Please select follow up date within next 2 business days");
                  $("#errorAlert").modal();
                  return false;
              }
          }
          else {
              //alert("Please select future date");
              $("#txtErrorMessage").text("Please select future date");
              $("#errorAlert").modal();
              return false;
          }
      }

	    //Sabir Khan 6706 10/07/2009 Set follow up date for Bond Alert Report...
      if ((court == "Bond Alert")) {

          var datediff = Math.ceil(DateDiff(newseldate, today));

          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 0; i <= datediff; i++) {

                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }

                  today = dateAdd("d", 1, today);

              }

              if ((countbusinessdays > 31) || (countbusinessdays == 0)) {
                  //alert("Please select follow up date within next 30 business days");
                  $("#txtErrorMessage").text("Please select follow up date within next 30 business days");
                  $("#errorAlert").modal();
                  return false;
              }
          }
          else {
              // alert("Please select future date");
              $("#txtErrorMessage").text("Please select future date");
              $("#errorAlert").modal();
              return false;
          }
      }



	    //Fahad 5753 04/13/2009 Allow to Add next 5 business days in Follow Up date
      if (court == "Split") {

          var datediff = Math.ceil(DateDiff(newseldate, today));
          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 1; i <= datediff; i++) {
                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }
                  today = dateAdd("d", 1, today);
              }
              if ((countbusinessdays > 5) || (countbusinessdays == 0)) {
                  //alert("Please select Follow Up Date within next 5 business days from today's Date");
                  $("#txtErrorMessage").text("Please select Follow Up Date within next 5 business days from today's Date");
                  $("#errorAlert").modal();
                  return false;
              }

          }
          else {
              // alert("Please select future date");
              $("#txtErrorMessage").text("Please select future date");
              $("#errorAlert").modal();
              return false;
          }
      }
	    //Saeed 8101 09/24/2010 Allow to add next (n) business days in Follow Up Date
      if (court == "HCJPAutoUpdateAlert") {

          var datediff = Math.ceil(DateDiff(newseldate, today));
          var businessDaysToStop = document.getElementById('<%=this.BusinessDay.ClientID %>').value;
	      if (datediff >= 0) {
	          var i = 0, countbusinessdays = 0, countHolidays = 0;
	          for (i = 1; i <= datediff; i++) {
	              if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                  countbusinessdays++;
	              }
	              today = dateAdd("d", 1, today);
	          }

	          //Saeed 8101 11/02/2010 if businessDaysToStop=0 then allow to set any future date for follow up date.
	          if ((businessDaysToStop != '0') && ((countbusinessdays > businessDaysToStop) || (countbusinessdays == 0))) {
	              //alert("Please select Follow Up Date within next " + businessDaysToStop + " business days from today's Date");
	              $("#txtErrorMessage").text("Please select Follow Up Date within next " + businessDaysToStop + " business days from today's Date");
	              $("#errorAlert").modal();
	              return false;
	          }

	      }
	      else {
	          //alert("Please select future date");
	          $("#txtErrorMessage").text("Please select future date");
	          $("#errorAlert").modal();
	          return false;
	      }
      }

	    // 5734 end

	    // Fahad 5098 10/30/2008 follow Up date condition set for Bond waiting Report	  
      if (court == "ContractFollowUp") {
          var datediff = Math.ceil(DateDiff(newseldate, today));
          if (datediff >= 0) {
              var i = 0, countbusinessdays = 0, countHolidays = 0;
              for (i = 1; i <= datediff; i++) {
                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
                      countbusinessdays++;
                  }
                  today = dateAdd("d", 1, today);
              }
              if ((countbusinessdays > 5) || (countbusinessdays == 0)) {
                  // alert("Please select follow up date with in 5 business days");
                  $("#txtErrorMessage").text("Please select follow up date with in 5 business days");
                  $("#errorAlert").modal();
                  return false;
              }
          }
          else {
              // alert("Please select future date");
              $("#txtErrorMessage").text("Please select future date");
              $("#errorAlert").modal();
              return false;
          }
      }
          // Abid Ali 4912 11/13/2008 - Traffic Waiting follow up date
          //Saeed 7791 08/13/2010 remove [court == "Family Waiting"] condition cause family waiting has max two weeks follow up date, while traffic wiating has 4 weeks.
      else if (court == "Traffic Waiting") {
          var datediff = Math.ceil(DateDiff(newseldate, today));

          // Zeeshan 10286 08/16/2012 Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses.	      
          var FollowUpDate = document.getElementById('<%=this.hf_FollowUpDate.ClientID %>').value;
	      var FollowUpDays = document.getElementById('<%=this.hf_FollowUpDays.ClientID %>').value;
	      var FirstFollowUpDays = document.getElementById('<%=this.hf_FirstFollowUpDays.ClientID %>').value;
	      FollowUpDate = FollowUpDate.toUpperCase() == 'N/A' ? '' : FollowUpDate;

	      if (datediff >= 0) {
	          var i = 0, countbusinessdays = 0, countHolidays = 0;
	          for (i = 0; i <= datediff; i++) {
	              if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                  countbusinessdays++;
	              }
	              today = dateAdd("d", 1, today);
	          }

	          // tahir 5350 12/20/2008 allowed 4 weeks to select...
	          //	          if ((countbusinessdays > 20) || (countbusinessdays == 0)) 
	          //	          {
	          //	              alert("Please enter Follow Up Date with in four weeks from today's date.");
	          //	              return false;
	          //	          }

	          // Zeeshan 10286 08/16/2012 Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses.
	          if (FollowUpDate == '' && FirstFollowUpDays > 0) {
	              if (countbusinessdays > FirstFollowUpDays) {
	                  // alert("Please select date in next " + FirstFollowUpDays + " business days from today's date.");
	                  $("#txtErrorMessage").text("Please select date in next " + FirstFollowUpDays + " business days from today's date.");
	                  $("#errorAlert").modal();
	                  document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = document.getElementById("UpdateFollowUpInfo2_cal_followupdate").defaultValue;
	                  return false;
	              }
	          } else if (FollowUpDate != '' && FollowUpDays > 0) {
	              if (countbusinessdays > FollowUpDays) {
	                  // alert("Please select date in next " + FollowUpDays + " business days from today's date.");
	                  $("#txtErrorMessage").text("Please select date in next " + FollowUpDays + " business days from today's date.");
	                  $("#errorAlert").modal();
	                  document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = document.getElementById("UpdateFollowUpInfo2_cal_followupdate").defaultValue;
	                  return false;
	              }
	          }
	      }
	      else {
	          // alert("Please select future date");
	          $("#txtErrorMessage").text("Please select future date");
	          $("#errorAlert").modal();
	          document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value = document.getElementById("UpdateFollowUpInfo2_cal_followupdate").defaultValue;
	          return false;
	      }
	  }

	      // Abid Ali 5018 11/24/2008 - Family Waiting follow up date
	  else if (court == "Family Waiting") {
	      var datediff = Math.ceil(DateDiff(newseldate, today));
	      if (datediff >= 0) {
	          var i = 0, countbusinessdays = 0, countHolidays = 0;
	          for (i = 0; i <= datediff; i++) {
	              if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                  countbusinessdays++;
	              }
	              today = dateAdd("d", 1, today);
	          }
	          // tahir 5350 12/20/2008 allowed 4 weeks to select...
	          if ((countbusinessdays > 10) || (countbusinessdays == 0)) {
	              //Saeed 7791 08/13/2010 display two weeks alert message.   
	              //   alert("Please enter Follow Up Date with in two weeks from today's date.");
	              $("#txtErrorMessage").text("Please enter Follow Up Date with in two weeks from today's date.");
	              $("#errorAlert").modal();
	              return false;
	          }
	      }
	      else {
	          //alert("Please select future date");
	          $("#txtErrorMessage").text("Please select future date");
	          $("#errorAlert").modal();
	          return false;
	      }
	  }

	      //Waqas 5653 03/24/2009 For No LOR
	      //Waqas 5697 03/26/2009 For HMC-FTA Follow Up
	      // Noufil 5691 04/06/2009 PastCourtDateHMC added
	      //Nasir 5938 05/26/2009 For NOS Follow Up
	      //Sabir Khan 5977 07/02/2009 For HMCSettingDiscrepancy Follow Up
	      //Nasir 7234 01/08/2010 for HMCSameDayArr 

	  else if (court == "NO LOR" || court == "HMC-FTA" || court == "PastCourtDateHMC" || court == "NOS" || court == "HMCSettingDiscrepancy" || court == "HMCSameDayArr" || court == "BadAddresses") {

	      var BusDay = document.getElementById('<%=this.BusinessDay.ClientID %>').value;
	      var datediff = Math.ceil(DateDiff(newseldate, today));
	      //Nasir 5974 06/05/2009 fixed issue
	      if ((weekday[today.getDay()] == "Sunday") || (weekday[today.getDay()] == "Saturday")) {
	          datediff = datediff + 1;
	      }
	      if (datediff >= 0) {
	          var i = 0, countbusinessdays = 0, countHolidays = 0;
	          for (i = 1; i <= datediff; i++) {
	              if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                  countbusinessdays++;
	              }
	              today = dateAdd("d", 1, today);
	          }

	          if ((countbusinessdays > BusDay) || (countbusinessdays == 0)) {

	              //alert("Please select Follow Up Date in next "+ BusDay +" business day(s) from Today's date.");
	              $("#txtErrorMessage").text("Please select Follow Up Date in next " + BusDay + " business day(s) from Today's date.");
	              $("#errorAlert").modal();
	              return false;
	          }

	      }
	      else {
	          //alert("Please select future date");
	          $("#txtErrorMessage").text("Please select future date");
	          $("#errorAlert").modal();
	          return false;
	      }
	  }

	      // Noufil 5819 05/13/2009 ALRHEARING ADDED
	  else if (court == "ALRHEARING") {
	      var BusDay = document.getElementById('<%=this.BusinessDay.ClientID %>').value;
	          var datediff = Math.ceil(DateDiff(newseldate, today));
	          if (datediff >= 0) {
	              var i = 0, countbusinessdays = 0, countHolidays = 0;
	              for (i = 1; i <= datediff; i++) {
	                  if ((weekday[today.getDay()] != "Sunday") && (weekday[today.getDay()] != "Saturday")) {
	                      countbusinessdays++;
	                  }
	                  today = dateAdd("d", 1, today);
	              }

	              if (BusDay == 0 && datediff > 0) {
	                  // alert("Please enter follow-up Date of today's date only.");
	                  $("#txtErrorMessage").text("Please enter follow-up Date of today's date only.");
	                  $("#errorAlert").modal();
	                  return false;
	              }
	              else if (parseInt(BusDay, 0) > 0 && ((countbusinessdays > BusDay) || (countbusinessdays == 0))) {
	                  //alert("Please enter Follow-up Date in next "+ BusDay +" business days from Today's date.");
	                  $("#txtErrorMessage").text("Please enter Follow-up Date in next " + BusDay + " business days from Today's date.");
	                  $("#errorAlert").modal();
	                  return false;
	              }
	          }
	          else {
	              //alert("Please select future Follow-up date.");
	              $("#txtErrorMessage").text("Please select future Follow-up date.");
	              $("#errorAlert").modal();
	              return false;
	          }
	      }








}

function DateDiff(date1, date2) {
    var one_day = 1000 * 60 * 60 * 24;
    var objDate1 = new Date(date1);
    var objDate2 = new Date(date2);
    return (objDate1.getTime() - objDate2.getTime()) / one_day;
}
// Haris Ahmed 9021 01/20/2012 Resolve issue to set followup date on popup calendar


</script>

<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50); left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>


            <div id="pnl_control" runat="server">

                <section class="box" id="" style="">
                    <header class="panel_header">
                        <h2 class="title pull-left">
                            <asp:Label ID="lbl_head" runat="server" CssClass="form-label"></asp:Label>
                        </h2>
                        <div class="actions panel_actions pull-right">

                            <asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>

                            <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                        </div>
                    </header>

                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Client Name :  </label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:Label ID="lbl_FullName" runat="server" CssClass="clsLabel"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <table>
                                    <tr id="tr_TicketNumber" runat="server">
                                        <td>
                                            <div class="form-group">
                                                <label class="form-label">Ticket Number : </label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:Label ID="lblTicketNumber" runat="server" CssClass="clsLabel"></asp:Label>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <table>
                                    <tr id="tr_CauseNumber" runat="server">
                                        <td>


                                            <div class="form-group">
                                                <label class="form-label">Cause Number : </label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:Label ID="lblCauseNo" runat="server" CssClass="clsLabel"></asp:Label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Current Follow up Date :  </label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:Label ID="lblCurrentFollup" runat="server" CssClass="clsLabel"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Next Follow up Date11 : </label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <%--<picker:datepicker id="cal_followupdate" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>
                                             <asp:TextBox ID="txtfollowupdate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server" Enabled="true"></asp:TextBox><%----%>
                                        <%--<ew:CalendarPopup Visible="false" ID="cal_followupdate" runat="server" AllowArbitraryText="False"
                                            CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                            Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                            PadSingleDigits="True" ShowClearDate="False" ShowGoToToday="True" Text="" ToolTip="Date"
                                            UpperBoundDate="9999-12-29" Width="101px" OnClientChange="callme()" JavascriptOnChangeFunction="CheckDate" onClick="CheckFollowUpDate()">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Comments : </label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <table>
                                            <tr id="tr_comment" runat="server">
                                                <td valign="top" colspan="2">
                                                    <table enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 323px;">
                                                        <tr>
                                                            <td style="height: 15px; width: 314px;">
                                                                <div id="divcomment" runat="server" style="overflow: auto; height: 50px; width: 375px;">
                                                                    <asp:Label ID="lblComments" runat="server" CssClass="form-label"></asp:Label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 314px">
                                                                <asp:TextBox ID="tb_GeneralComments" runat="server" Height="70px" TextMode="MultiLine"
                                                                    Width="375px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label"></label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Save" Width=""
                                            OnClientClick="return ValidateInput();" OnClick="btnsave_Click" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" Width="" />
                                        <asp:CheckBox ID="chk_UpdateAll" runat="server" CssClass="checkbox-custom" Text="Update Selected"
                                            Width="108px" Visible="true" />
                                        <asp:CheckBox ID="chk_IsHMCFTARemoved" runat="server" CssClass="checkbox-custom" Text="Remove Client"
                                            Width="108px" Visible="false" />
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>


                <asp:HiddenField ID="hf_ticketid" runat="server" Value="0" />
                <asp:HiddenField ID="hf_FollowUpDate" runat="server" />
                <asp:HiddenField ID="hf_ticid" runat="server" />
                <asp:HiddenField ID="hf_court" runat="server" />
                <asp:HiddenField ID="hf_courtname" runat="server" />
                <asp:HiddenField ID="hf_todaydate" runat="server" />
                <asp:HiddenField ID="Hf_field" runat="server" Value="0" />
                <asp:HiddenField ID="hf_FollowUpType" runat="server" />
                <asp:HiddenField ID="BusinessDay" runat="server" Value="0" />
                <!--SAEED 7844 06/02/2010, hidden field to store contactId-->
                <asp:HiddenField ID="hf_ContactID" runat="server" Value="0" />
                <!-- Zeeshan 10286 08/15/2012  Traffic Waiting Follow up report needs to be flexible for the users to update the follow up dates as per court houses.-->
                <asp:HiddenField ID="hf_FollowUpDays" runat="server" Value="0" />
                <asp:HiddenField ID="hf_FirstFollowUpDays" runat="server" Value="0" />





            </div>
<div id="errorAlert" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error message</h4>
            </div>
            <div class="modal-body" style="min-height: 93px !important; max-height: 162px;">
                <p id="txtErrorMessage">Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>

<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
<script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
<script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>

    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

<%--<script language="JavaScript" type="text/javascript" src="../Scripts/wz_tooltip.js"></script>--%>
<!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
<script type="text/javascript">
    function CheckFollowUpDate() {
        debugger;
        CalendarPopup_Up_SelectDate('UpdateFollowUpInfo2_cal_followupdate', '', 'UpdateFollowUpInfo2_cal_followupdate_div', 'UpdateFollowUpInfo2_cal_followupdate_monthYear', document.getElementById("UpdateFollowUpInfo2_cal_followupdate").value, 1, true, true, 'UpdateFollowUpInfo2_cal_followupdate_Up_PostBack', '', 'UpdateFollowUpInfo2_cal_followupdate_outer_VisibleDate')
    }

   
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });

            $(document).ready(function () {
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                Sys.WebForms.PageRequestManager.getInstance().beginAsyncPostBack();
                function EndRequestHandler(sender, args) {
                    var today = new Date();
                    $('.datepicker').datepicker({
                        setDate: new Date,
                        autoclose: true,

                    });
                }
            });
       
</script>

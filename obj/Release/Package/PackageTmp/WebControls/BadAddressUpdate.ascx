﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BadAddressUpdate.ascx.cs" Inherits="lntechNew.WebControls.BadAddressUpdate" %>
<link href="../Styles.css" type="text/css" rel="stylesheet" />
<script src="../Scripts/Dates.js" type="text/javascript"></script>
<script src="../Scripts/jsDate.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

    function ClosePopUp(backdiv,frontdiv)
    { 
        document.getElementById(backdiv).style.display="none";        
        document.getElementById(frontdiv).style.display="none"; 
        return false;
    }
    
    function CloseModalPopupExtender(popupid)
    {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }

    function EnableDisableControls1() {
        var chkAddress1 = document.getElementById("<%= chk_Address1Exists.ClientID %>");
        if (!chkAddress1.checked) {
            document.getElementById("<%= txt_Address1.ClientID %>").disabled = true;
            document.getElementById("<%= txt_Address1.ClientID %>").value = "";
            document.getElementById("<%= txt_city1.ClientID %>").disabled = true;
            document.getElementById("<%= txt_city1.ClientID %>").value = "";
            document.getElementById("<%= ddl_stat1.ClientID %>").disabled = true;
            document.getElementById("<%= ddl_stat1.ClientID %>").selectedIndex = 0;
            document.getElementById("<%= txt_zip1.ClientID %>").disabled = true;
            document.getElementById("<%= txt_zip1.ClientID %>").value = "";
        }
        else {
            document.getElementById("<%= txt_Address1.ClientID %>").disabled = false;
            document.getElementById("<%= txt_city1.ClientID %>").disabled = false;
            document.getElementById("<%= ddl_stat1.ClientID %>").disabled = false;
            document.getElementById("<%= txt_zip1.ClientID %>").disabled = false;
            document.getElementById("<%= ddl_stat1.ClientID %>").selectedIndex = 0;
        }
        return false;
    }
    
    function EnableDisableControls2 () 
    {
        var chkAddress2 = document.getElementById("<%= chk_Address2Exists.ClientID %>");
        if (!chkAddress2.checked) 
        {
            document.getElementById("<%= txt_Address2.ClientID %>").disabled = true;
            document.getElementById("<%= txt_Address2.ClientID %>").value = "";
            document.getElementById("<%= txt_city2.ClientID %>").disabled = true;
            document.getElementById("<%= txt_city2.ClientID %>").value = "";
            document.getElementById("<%= ddl_stat2.ClientID %>").disabled = true;
            document.getElementById("<%= ddl_stat2.ClientID %>").selectedIndex = 0;
            document.getElementById("<%= txt_zip2.ClientID %>").disabled = true;
            document.getElementById("<%= txt_zip2.ClientID %>").value = "";
        }
        else 
        {
            document.getElementById("<%= txt_Address2.ClientID %>").disabled = false;
            document.getElementById("<%= txt_city2.ClientID %>").disabled = false;
            document.getElementById("<%= ddl_stat2.ClientID %>").disabled = false;
            document.getElementById("<%= txt_zip2.ClientID %>").disabled = false;
            document.getElementById("<%= ddl_stat2.ClientID %>").selectedIndex = 0;
        }
    return false;
    }

    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, "");
    };


    function ValidateUserInput() {

        var txtAddress1 = document.getElementById("<%= txt_Address1.ClientID %>");
        var txtAddress2 = document.getElementById("<%= txt_Address2.ClientID %>");
        var txtCity1 = document.getElementById("<%= txt_city1.ClientID %>");
        var txtCity2 = document.getElementById("<%= txt_city2.ClientID %>");
        var ddlState1 = document.getElementById("<%= ddl_stat1.ClientID %>");
        var ddlState2 = document.getElementById("<%= ddl_stat2.ClientID %>");
        var txtZip1 = document.getElementById("<%= txt_zip1.ClientID %>");
        var txtZip2 = document.getElementById("<%= txt_zip2.ClientID %>");
        var chkAddress2 = document.getElementById("<%= chk_Address2Exists.ClientID %>");
        var chkAddress1 = document.getElementById("<%= chk_Address1Exists.ClientID %>");

        if ((!chkAddress1.checked) && (!chkAddress2.checked)) {
            alert("Please enter new address 1/new address 2 or select Red X for No New Address");
            chkAddress1.focus();
            return false;
        }

        if ((!chkAddress1.checked) && (chkAddress2.checked)) {
            alert("Please Update Address 1 before updating Address 2");
            chkAddress1.focus();
            return false;
        }

        if (chkAddress1.checked) {
            if ( txtAddress1.value.trim().length == 0) {
                alert("Please Enter Address 1");
                txtAddress1.focus();
                return false;
            }

            if (txtCity1.value.trim().length == 0) {
                alert("Please Enter City for Address 1");
                txtCity1.focus();
                return false;
            }

            if (ddlState1.selectedIndex == 0) {
                alert("Please Select State for Address 1");
                ddlState1.focus();
                return false;
            }

            if (txtZip1.value == "") {
                alert("Please Enter Zipcode for Address 1");
                txtZip1.focus();
                return false;
            }
        }
        if (chkAddress2.checked) {
            if (txtAddress2.value.trim().length == 0) {
                alert("Please Enter Address 2");
                txtAddress2.focus();
                return false;
            }

            if (txtCity2.value.trim().length == 0) {
                alert("Please Enter City for Address 2");
                txtCity2.focus();
                return false;
            }

            if (ddlState2.selectedIndex == 0) {
                alert("Please Select State for Address 2");
                ddlState2.focus();
                return false;
            }

            if (txtZip2.value == "") {
                alert("Please Enter Zipcode for Address 2");
                txtZip2.focus();
                return false;
            }
        }

        if ((chkAddress1.checked) && (chkAddress2.checked)) {
            if ((txtAddress1.value.trim().toUpperCase() == txtAddress2.value.trim().toUpperCase()) && (txtCity1.value.trim().toUpperCase() == txtCity2.value.trim().toUpperCase())
            && (txtZip1.value.trim() == txtZip2.value.trim()) && (ddlState1.selectedIndex == ddlState2.selectedIndex)) {
                alert("Address 1 and Address 2 are same, Please provide different Addresses");
                return false;
            }
        }
        
        var isConfirmed = false;
        if (chkAddress1.checked) {
            var isOk1 = confirm("Please confirm the New Address 1 entered is correct");
            if (isOk1 == true) {
                var reConfirm1 = confirm("Please re-confirm the New Address 1 entered is correct");
                if (reConfirm1 == true)
                    isConfirmed = true;
                else {
                    txtAddress1.focus();
                    return false; 
                }
            } else {
                txtAddress1.focus();
                return false;
            }
        }

        if (chkAddress2.checked) {
            var isOk2 = confirm("Please confirm the New Address 2 entered is correct");
            if (isOk2 == true) {
                var reConfirm2 = confirm("Please re-confirm the New Address 2 entered is correct");
                if (reConfirm2 == true)
                    isConfirmed = true;
                else {
                    txtAddress2.focus();
                    return false;
                }
            } else {
                txtAddress2.focus();
                return false;
            }
        }
        return isConfirmed;
    }

    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9-]");
        if (key == 8) {
            keychar = String.fromCharCode(key);
        }
        if (key == 13) {
            key = 8;
            keychar = String.fromCharCode(key);
        }
        return reg.test(keychar);
    } 

</script>

<style type="text/css">
    .style1
    {
    	text-align: left;
        font-weight: bold;
        font-size: 8pt;
        color: #3366cc;
        font-family: Tahoma;
        text-decoration: none;
        width: 130px;
        padding-left: 6px;
        height: 30px;
    }
    
    
    .style2
    {
    	font-family: Tahoma;
	    font-size: 8pt;
	    color: #123160;
	    border-bottom-width: 0;
	    border-left-width: 0;
	    border-right-width: 0;
	    border-top-width: 0;
	    text-align: left;
	    width: 310px;
    }
    .clsinputadministrationAddress
        {
        	border-right: #3366cc 1px solid;
	        border-top: #3366cc 1px solid;
	        font-size: 8pt;
	        border-left: #3366cc 1px solid;
	        width: 262px;
	        color: #123160;
	        border-bottom: #3366cc 1px solid;
	        font-family: Tahoma;
	        background-color: white;
	        text-align: left;
        }
        .clsinputadministration
        {
        	border-right: #3366cc 1px solid;
	        border-top: #3366cc 1px solid;
	        font-size: 8pt;
	        border-left: #3366cc 1px solid;
	        width: 90px;
	        color: #123160;
	        border-bottom: #3366cc 1px solid;
	        font-family: Tahoma;
	        background-color: white;
	        text-align: left;
        }
    </style>

<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50);
    left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<div id = "dv_badAddressControl" runat = "server">
    <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse; width: 450px">
                <tr>
                    <td background="../Images/subhead_bg.gif" valign="bottom">
                        <table border="0" width="100%">
                            <tr>
                                <td class="clssubhead" style="height: 26px">
                                    <asp:Label ID="lbl_head" Text = "New Update Address Control" runat="server"></asp:Label>
                                </td>
                                <td align="right">
                                    &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" border="0">
                            <tr>
                                <td class="style1">
                                    Client Name :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:Label ID="lbl_FullName" runat="server" CssClass="clsLabel"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    Existing Address :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:Label ID="lbl_existingAddress" runat="server" CssClass="clsLabel"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    Cause Numbers :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:Label ID="lbl_CauseNumbers" runat="server" CssClass="clsLabel"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1" style="height: 30px;">
                                    New Address 1 :
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" onclick="EnableDisableControls1(this);" Checked="True" ID="chk_Address1Exists"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    Address :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:TextBox ID = "txt_Address1" runat = "server" CssClass = "clsinputadministrationAddress" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    City/State/Zip :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:TextBox ID = "txt_city1" runat = "server" CssClass = "clsinputadministration" style ="width: 90px;" />
                                    &nbsp;
                                    <asp:DropDownList ID = "ddl_stat1" runat = "server" CssClass = "clsInputCombo" style ="width: 60px;"  />
                                    &nbsp;
                                    <asp:TextBox ID = "txt_zip1" runat = "server" CssClass = "clsinputadministration" style ="width: 90px;" MaxLength="10" />
                                </td>
                            </tr>
                            <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                            </tr>
                            <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    New Address 2 :
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" onclick="EnableDisableControls2(this);" Checked="True" ID="chk_Address2Exists"/>
                                </td>
                            </tr>
                             <tr>
                                <td class="style1">
                                    Address :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:TextBox ID = "txt_Address2" Enabled="False" runat = "server" CssClass = "clsinputadministrationAddress" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    City/State/Zip :
                                </td>
                                <td class ="style2" style="height: 30px;">
                                    <asp:TextBox ID = "txt_city2" Enabled="False" runat = "server" CssClass = "clsinputadministration" style ="width: 90px;" />
                                    &nbsp;
                                    <asp:DropDownList ID = "ddl_stat2" Enabled="False" runat = "server" CssClass = "clsInputCombo" style ="width: 60px;"  />
                                    &nbsp;
                                    <asp:TextBox ID = "txt_zip2" Enabled="False" runat = "server" CssClass = "clsinputadministration" style ="width: 90px;" MaxLength="10" />
                                </td>
                            </tr>
                            <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                            </tr>
                            <tr>
                                <td colspan = "2" align = "center" > 
                                    <asp:Button ID = "btn_Update" OnClick="btn_Update_Click" runat = "server" 
                                    Text = "Update" OnClientClick="return ValidateUserInput();" CssClass ="clsbutton" />
                                </td>
                            </tr>
                            <tr>
                                <td> &nbsp; </td>
                                <td> &nbsp; </td>
                            </tr>
                            <tr>
                                <td colspan = "2" > 
                                    <asp:HiddenField ID="hf_recordid" runat="server" Value="0" />
                                    <asp:HiddenField ID="hf_existingAddress" runat="server" />
                                    <asp:HiddenField ID="hf_newAddress1" runat="server" />
                                    <asp:HiddenField ID="hf_newCity1" runat="server" />
                                    <asp:HiddenField ID="hf_newState1" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_newZip1" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_newAddress2" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_newCity2" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_newState2" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_newZip2" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_modelPopupId" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_employeeId" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_firstName" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_lastName" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_State1ZP4" runat="server" Value="" />
                                    <asp:HiddenField ID="hf_State2ZP4" runat="server" Value="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
             </table>
        </ContentTemplate>
    </aspnew:UpdatePanel>
</div>

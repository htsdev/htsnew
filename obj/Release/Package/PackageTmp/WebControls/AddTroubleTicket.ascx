<%@ Control Language="C#" AutoEventWireup="true" Codebehind="AddTroubleTicket.ascx.cs"
    Inherits="lntechDallasNew.WebControls.AddTroubleTicket" %>
<link href="../Styles.css" type="text/css" rel="stylesheet">

<script language="jscript">
		function Validation()
		{ 
			var txtShortDesc=document.getElementById("tt2$txt_shortdesc").value;
			var txtdescription = document.getElementById("tt2$txt_Desc").value;
			
			
			if(txtShortDesc == "")
			{ 
				alert("PLease Enter Title");
				document.getElementById("tt2$txt_shortdesc").focus();
				return false;
			}
			
			if(txtdescription == "")
			{
			alert("Please Enter Bug Description");
			document.getElementById("tt2$txt_Desc").focus();
			return false;
			}
			
			
			
			if(txtdescription.length > 2000)
			{
			alert("You Cannot Enter More Than 2000 Characters");
			document.getElementById("tt2$txt_Desc").focus();
			return false;
			}
			
		}
		
</script>

<body>
    <table id="TableMain" cellspacing="0" cellpadding="0" border="0" style="width: 380px;
        height: 1px">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="clssubhead">
                                        &nbsp;Online Issues Submission
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2" style="height: 302px">
                <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td align="right" class="clsLeftPaddingTable" style="width: 113px">
                            Title&nbsp;</td>
                        <td class="clsLeftPaddingTable">
                            <asp:TextBox ID="txt_shortdesc" runat="server" Width="240px" CssClass="clstextarea"
                                MaxLength="100"></asp:TextBox>&nbsp; </td>
                    </tr>
                    <tr>
                        <td align="right" class="clsLeftPaddingTable" style="width: 113px">
                            Priority&nbsp;</td>
                        <td class="clsLeftPaddingTable">
                            <asp:DropDownList ID="ddl_priority" runat="server" Width="72px" CssClass="clsinputadministration">
                            </asp:DropDownList>&nbsp;&nbsp;Status
                            <asp:DropDownList ID="ddl_status" runat="server" Width="140px" CssClass="clsinputadministration">
                            </asp:DropDownList>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="clsLeftPaddingTable" style="width: 113px">
                            Page Url&nbsp;</td>
                        <td class="clsLeftPaddingTable">
                            <asp:TextBox ID="txt_pageurl" runat="server" Width="252px" CssClass="clstextarea"
                                MaxLength="100"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right" class="clsLeftPaddingTable" style="width: 113px">
                            Ticket Number&nbsp;</td>
                        <td class="clsLeftPaddingTable">
                            <asp:TextBox ID="txt_ticketno" runat="server" Width="96px" MaxLength="15" CssClass="clstextarea"></asp:TextBox>
                            Cause No
                            <asp:TextBox ID="txtCauseNo" runat="server" CssClass="clstextarea" MaxLength="15"
                                Width="103px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="right" style="width: 113px">
                            Please Enter the Bug Description&nbsp;
                        </td>
                        <td class="clsLeftPaddingTable">
                            <asp:TextBox ID="txt_Desc" runat="server" Width="252px" CssClass="clstextarea" Height="65px"
                                TextMode="MultiLine" MaxLength="1000"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="right" style="height: 21px; width: 113px;">
                            Upload File&nbsp;</td>
                        <td class="clsLeftPaddingTable" style="height: 21px">
                            <input id="fp_file" type="file" name="File1" runat="server" style="width: 252px"
                                class="clstextarea"></td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="right" style="width: 113px">
                            File Description&nbsp;
                        </td>
                        <td class="clsLeftPaddingTable">
                            <asp:TextBox ID="txt_filedesc" runat="server" Width="252px" MaxLength="100" CssClass="clstextarea"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" style="height: 5px; width: 113px;">
                        </td>
                        <td class="clsLeftPaddingTable" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="right" style="width: 113px">
                        </td>
                        <td class="clsLeftPaddingTable" align="center">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="clsButton" OnClick="btnsubmit_Click1"
                                Text="Submit" OnClientClick="return Validation();" CausesValidation="true" /></td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="center" colspan="2">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td background="../../images/separator_repeat.gif" colspan="6" style="height: 11px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidationReportScheduling.ascx.cs" Inherits="HTP.WebControls.ValidationReportScheduling" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<script type="text/javascript">
function ValidateTimings()
{
    //SAEED 7791 06/30/2010 method created.
    var weekdayTiming1= document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlWeekTimings1").value;
    var weekdayTiming2= document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlWeekTimings2").value;
    var weekdayTiming3= document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlWeekTimings3").value;
    
    var satTimings1= document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlSatTimings1").value;
    var satTimings2= document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlSatTimings2").value;
    var satTimings3= document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlSatTimings3").value;    
    
    if((weekdayTiming1==weekdayTiming2 && weekdayTiming1!='-1' && weekdayTiming2!='-1') || (weekdayTiming2==weekdayTiming3 && weekdayTiming2!='-1' && weekdayTiming3!='-1' ) || (weekdayTiming3==weekdayTiming1 && weekdayTiming3!='-1' && weekdayTiming1!='-1'))
    {
        alert('Please select different weekday timings');
        document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlWeekTimings1").focus();
        return false;
    }
    if((satTimings1==satTimings2 && satTimings1!='-1' && satTimings2!='-1' ) || (satTimings2==satTimings3 && satTimings2!='-1' && satTimings3!='-1') || (satTimings3==satTimings1 && satTimings3!='-1' && satTimings1!='-1'))
    {
        alert('Please select different saturday timings');
        document.getElementById("ValidationReportSettings_ValidationReportScheduling1_ddlSatTimings1").focus();
        return false;
    }
    return true;
}
</script>
<link href="../Styles.css" type="text/css" rel="stylesheet" />
<asp:HiddenField ID="hdnReportID" runat="server" />
<asp:Panel ID="Disable" runat="server" Style="display: none; z-index: 1; filter: alpha(opacity=50);
    left: 1px; position: absolute; top: 1px; height: 1px; background-color: silver">
</asp:Panel>
<div id="pnl_control" runat="server">
<asp:HiddenField ID="hdnWeekdayTiming1" Value="-1" runat="server" />
<asp:HiddenField ID="hdnWeekdayTiming2" Value="-1" runat="server" />
<asp:HiddenField ID="hdnWeekdayTiming3" Value="-1" runat="server" />
<asp:HiddenField ID="hdnSaturdayTiming1" Value="-1" runat="server" />
<asp:HiddenField ID="hdnSaturdayTiming2" Value="-1" runat="server" />
<asp:HiddenField ID="hdnSaturdayTiming3" Value="-1" runat="server" />


    <table border="2" enableviewstate="true" style="border-color: navy; border-collapse: collapse; " width="780" >
        <tr>
            <td background="../Images/subhead_bg.gif" valign="bottom">
                <table border="0" width="100%">
                    <tr>
                        <td class="clssubhead" style="height: 26px">
                            <asp:Label ID="lbl_head" runat="server" Text="Validation Report Schedule"></asp:Label>
                        </td>
                        <td align="right">
                            &nbsp;<asp:LinkButton ID="lbtn_close" runat="server">X</asp:LinkButton>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="100%" >
                <table cellpadding="0" cellspacing="0" class="clsLeftPaddingTable" border="1" width="100%">
                    <tr>
                        <td class="clssubhead" style="width: 240px">&nbsp;</td>
                        <td class="clssubhead" style="width: 240px" align="center">Weekday Timings</td>
                        <td class="clssubhead" style="width: 240px" align="center">Saturday Timings</td>
                    </tr>
                    <tr>                        
                        <td style="height: 30px;">
                        <asp:Label ID="lblReport" runat="server" CssClass="clsLabel"></asp:Label>                            
                        </td>
                        <td style="height: 30px;">                         
                            <asp:DropDownList ID="ddlWeekTimings1" runat="server"  CssClass="clsInputadministration" Width="80px"></asp:DropDownList>
                            <asp:DropDownList ID="ddlWeekTimings2" runat="server"  CssClass="clsInputadministration"  Width="80px"></asp:DropDownList>
                            <asp:DropDownList ID="ddlWeekTimings3" runat="server"  CssClass="clsInputadministration"  Width="80px"></asp:DropDownList>
                        </td>
                        <td style="height: 30px;">
                            <asp:DropDownList ID="ddlSatTimings1" runat="server"  CssClass="clsInputadministration" Width="80px"></asp:DropDownList>
                            <asp:DropDownList ID="ddlSatTimings2" runat="server"  CssClass="clsInputadministration"  Width="80px"></asp:DropDownList>
                            <asp:DropDownList ID="ddlSatTimings3" runat="server"  CssClass="clsInputadministration"  Width="80px"></asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                    <td colspan="3" align="right">
                    <asp:Button ID="BtnUpdate" runat="server" CssClass="clsbutton" Text="Update"  OnClientClick="return ValidateTimings();" />
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

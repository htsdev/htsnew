﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInfo.ascx.cs"
    Inherits="HTP.WebControls.ContactInfo" %>

<script language="javascript" type="text/javascript">
 function closeModalPopup(popupid)
    {
        var modalPopupBehavior = $find(popupid);
        modalPopupBehavior.hide();
        return false;
    }
 function ConfirmClient()
 {
        alert("CID for an active client can not be disassociated.");
        return false;
 }   
 function ConfirmDisAssociation()
 {
        check = confirm("Removing CID. Would you like to continue? (OK = Yes , Cancel = No)"); 
	    if (check == false) 
	    {
		    return false;
	    }
 }
</script>
<div aria-hidden="false" role="dialog" tabindex="-1" id="pnl_CID" runat="server" class="modal fade in" style="display:none;height: 200px;">

     <div class="modal-dialog">
        <div class="modal-content">
                      <div class="modal-header">
                          <asp:LinkButton ID="lbtn_close" aria-hidden="true" data-dismiss="modal" class="close" runat="server" style="float:right">X</asp:LinkButton>    
                            <h4 class="modal-title"><asp:Label ID="lbl_head" Text="Contact Information" runat="server"></asp:Label></h4>
                            
                      </div>    
                         <div class="modal-body">
                             <form role="form">
                                 <div class="row">
                                 <div class="form-group col-xs-6" >
                                    <h4><label for="modalemail1" class="form-label ">CID</label></h4>
                                     
                                    <asp:Label ID="lblCID" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                              <div class="form-group col-xs-6" >
                                    <h4><label for="modalemail1" class="form-label">First Name</label></h4>
                                  
                                    <asp:Label ID="lblFirstName" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                                     </div>

                                 <div class="row">
                                <div class="form-group col-xs-6" >
                                   <h4> <label for="modalemail1" class="form-label">Middle Name</label></h4>
                                   <asp:Label ID="lblMiddleName" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                              <div class="form-group col-xs-6" >
                                   <h4> <label for="modalemail1" class="form-label">Last Name</label></h4>
                                    <asp:Label ID="lblLastName" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                             </div>

                                 <div class="row">
                             <div class="form-group col-xs-6" >
                                   <h4> <label for="modalemail1" class="form-label">DOB</label></h4>
                                    <asp:Label ID="lblDOB" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                               <div class="form-group col-xs-6" >
                                   <h4> <label for="modalemail1" class="form-label">DL Number</label></h4>
                                   <asp:Label ID="lblDLNumber" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                                     </div>
                                 <div class="row">
                               <div class="form-group col-xs-6" >
                                    <h4><label for="modalemail1" class="form-label">DL State</label></h4>
                                    <asp:Label ID="lblDLState" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                             
                              <div class="form-group col-xs-6" >
                                    <h4><label for="modalemail1" class="form-label">Phone 1</label></h4>
                                   <asp:Label ID="lblPhone1" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                                         </div>
                                 <div class="row">
                               <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">Phone 2</label></h4>
                                  <asp:Label ID="lblPhone2" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                             
                              <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">Phone 3</label></h4>
                                   <asp:Label ID="lblPhone3" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                                         </div>
                                 <div class="row">
                                <div class="form-group col-xs-6" >
                                 <h4>   <label for="modalemail1" class="form-label">Address</label></h4>
                                   <asp:Label ID="lblAddress" runat="server" CssClass="clsLabel"></asp:Label>
                                  </div>
                              <div class="form-group col-xs-6" >
                                   <h4> <label for="modalemail1" class="form-label">Email</label></h4>
                                   <asp:Label ID="lblEmail" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
    </div>
                                 <div class="row">
                              <div class="form-group col-xs-6" >
                                 <h4>   <label for="modalemail1" class="form-label">Language</label></h4>
                                  <asp:Label ID="lblLanguage" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                              <div class="form-group col-xs-6" >
                                 <h4>   <label for="modalemail1" class="form-label">Mid Number</label></h4>
                                   <asp:Label ID="lblMidNumber" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
                                 </div>
                                 <div class="row">
                              <div class="form-group col-xs-6" >
                                 <h4>   <label for="modalemail1" class="form-label">Gender</label></h4>
                                    <asp:Label ID="lblGenderRace" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                           
                              <div class="form-group col-xs-6" >
                                 <h4>   <label for="modalemail1" class="form-label">Race</label></h4>
                                    <asp:Label ID="lblRace" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                                 </div>
                                 <div class="row">

                              <div class="form-group col-xs-6" >
                                 <h4>   <label for="modalemail1" class="form-label">Height</label></h4>
                                    <asp:Label ID="lblHeight" runat="server" CssClass="form-label"></asp:Label>
                                  </div>


                             <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">Weight</label></h4>
                                    <asp:Label ID="lblWeight" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                               </div>
                                 <div class="row">

                             <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">Hair Color</label></h4>
                                    <asp:Label ID="lblHair" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                            

                              <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">Hair Color</label></h4>
                                    <asp:Label ID="Label2" runat="server" CssClass="form-label"></asp:Label>
                                  </div>
    </div>
                                 <div class="row">
                              <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">Eye Color</label></h4>
                                    <asp:Label ID="lblEye" runat="server" CssClass="clsLabel"></asp:Label>
                                  </div>

                            


                             <div class="form-group col-xs-6" >
                                  <h4>  <label for="modalemail1" class="form-label">CDL Flag</label></h4>
                                   <asp:Label ID="lblCDLFlag" runat="server" CssClass="form-label"></asp:Label>
                                  </div>

                              </div>

                             
                                 </form>
                         </div>  
            <div class="modal-footer">
                <asp:HiddenField ID="hdnTicketID" runat="server" />
                            <asp:Button ID="btnDisAssociate" runat="server" CssClass="btn btn-primary" Text="Disassociate"
                                OnClick="btnDisAssociate_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Never mind" />
                            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                </div>
        </div>    
         
     </div>

</div>

<link href="../Styles.css" type="text/css" rel="Stylesheet" />


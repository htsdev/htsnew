<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="popup.aspx.cs" Inherits="lntechNew.WebScan.popup" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Queue Process</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var refreshinterval=5
		var displaycountdown="yes"
		var starttime
		var nowtime
		var reloadseconds=0
		var secondssinceloaded=0

		function starttime() {
			starttime=new Date()
			starttime=starttime.getTime()
			countdown()
		}

		function countdown() {
			nowtime= new Date()
			nowtime=nowtime.getTime()
			secondssinceloaded=(nowtime-starttime)/1000
			reloadseconds=Math.round(refreshinterval-secondssinceloaded)
			if (refreshinterval>=secondssinceloaded) {
				var timer=setTimeout("countdown()",1000)
				if (displaycountdown=="yes") {
					window.status="Page will be refreshed in "+reloadseconds+ " seconds"
				}
			}
			else {
				clearTimeout(timer)
				window.location.reload(true)
			} 
		}
		window.onload=starttime
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" align="center" width="600">
            <tr>
                <td align="center" class="clssubhead">Note:Closing this window will not terminate the current process.
                </td>
            </tr>
            <tr>
                <td class="clssubhead" background="../Images/subhead_bg.gif"  height="34">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clssubhead">
                                Batch:
                            </td>
                            <td >
                                <asp:Label ID="lblBatch" runat="server" Text=""></asp:Label>                                
                            </td>
                            <td class="clssubhead">
                                Started At:                            
                            </td>
                            <td>
                                <asp:Label ID="lblstime" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="clssubhead">
                                Total:
                            </td>
                            <td>
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="clssubhead">
                                Processed:
                            </td>
                            <td>
                                <asp:Label ID="lblProcessed" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="clssubhead">
                                No Cause:
                            </td>
                            <td>
                                <asp:Label ID="lblNoCause" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="clssubhead">
                                Quote Client:
                            </td>
                            <td>
                                <asp:Label ID="lblQuoteClient" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>                
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gv_Scan" runat="server" AutoGenerateColumns="False" Width="100%">
                        <AlternatingRowStyle BackColor="#EEEEEE" />
                        <HeaderStyle CssClass="GrdHeader" HorizontalAlign="Left" VerticalAlign="Middle" />
                        <Columns>
                            <asp:TemplateField HeaderText="Reset" >
                                <ItemTemplate>
                                    <asp:Label ID="lblreset" runat="server" Text='<%# bind("reset") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle ForeColor="#006699" Width="60px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cause No" >
                                <ItemTemplate>
                                    <asp:Label ID="lblCause" runat="server" Text='<%# bind("causeno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle ForeColor="#006699" Width="60px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Message" >
                                <ItemTemplate>
                                    <asp:Label ID="lblimages" runat="server" CssClass="label" Text='<%# bind("message") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle ForeColor="#006699" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" >
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" runat="server" CssClass="label" Text='<%# bind("status") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle ForeColor="#006699" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Msg Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblScanType" runat="server" CssClass="label" Text='<%# bind("msgdate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle ForeColor="#006699" />
                            </asp:TemplateField>                            
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" style="height: 11px">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                    
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

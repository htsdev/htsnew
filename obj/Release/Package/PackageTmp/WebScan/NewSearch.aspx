<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewSearch.aspx.cs" Inherits="lntechNew.WebScan.NewSearch" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Page</title>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function DeleteConfirm()
        {
            var isDelete=confirm("Are You Sure You Want To Delete This Batch");
            if(isDelete== true)
            {
           
            }
        
            else
            {
                return false;
            }
        }
    
    </script>

    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
</head>

<body class=" ">

    <form id="form1" runat="server">
        
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">
        
            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">BSDA (HMC)</h1><!-- PAGE HEADING TAG - END -->
                            </div>
                        </div>
                    </div>
                
                    <div class="clearfix"></div>
            
                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <div class="col-lg-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Search Documents</h2>
                                <div class="actions pane</section>l_actions pull-right">
                                    <asp:HyperLink ID="scan" runat="server" NavigateUrl="~/WebScan/Default.aspx" CssClass="form-label">Scan</asp:HyperLink>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <section class="box ">
                                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="form-label"></asp:Label>
                                            <div class="col-md-3 col-sm-4 col-xs-5">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Search Scanned Batch Dates</label>
                                                    <div class="controls">
                                                        <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" ImageUrl="~/Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                            ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" LowerBoundDate="1900-01-01">
                                                            <TextboxLabelStyle CssClass="form-control" />
                                                        </ew:CalendarPopup>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-xs-5">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">To</label>
                                                    <div class="controls">
                                                        <ew:CalendarPopup ID="enddate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" ImageUrl="~/Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                            ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" LowerBoundDate="1900-01-01">
                                                            <TextboxLabelStyle CssClass="form-control" />
                                                        </ew:CalendarPopup>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-xs-5">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">&nbsp;</label>
                                                    <div class="controls">
                                                        <asp:CheckBox ID="chk" runat="server" CssClass="form-label" Text="Show Batches With Non Verified" Checked="True" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-xs-5">
                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">&nbsp;</label>
                                                    <div class="controls">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Label ID="lblProess" runat="server" Text="Please Wait Scanning and OCR is in Process..." ForeColor="Red" Font-Bold="true" CssClass="form-control"></asp:Label>
                                        </section>
                                        <asp:GridView ID="gv_Scan" runat="server" CssClass="table" AutoGenerateColumns="False" OnRowDataBound="gv_Scan_RowDataBound" OnSorting="gv_Scan_Sorting"
                                            AllowPaging="True" AllowSorting="True" PageSize="15" OnPageIndexChanging="gv_Scan_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Batch" SortExpression="batchid">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblbatchid" CssClass="form-label" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Images" SortExpression="ImagesCount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblimages" CssClass="form-label" runat="server" Text='<%# bind("ImagesCount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScanType" CssClass="form-label" runat="server" Text='<%# bind("scantype") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDate" CssClass="form-label" runat="server" Text='<%# bind("scandate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Verified" SortExpression="verifiedCount">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="verified" ForeColor="RoyalBlue" runat="server" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No Cause" SortExpression="NoCauseCount">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="nocause" ForeColor="RoyalBlue" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Queued" SortExpression="QueuedCount">
                                                    <HeaderStyle ForeColor="#006699" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="queued" ForeColor="RoyalBlue" runat="server" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Discrepancy" SortExpression="DiscrepancyCount">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="discrepancy" ForeColor="RoyalBlue" runat="server" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pending" SortExpression="PendingCount">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="pending" ForeColor="RoyalBlue" runat="server" Text='<%# bind("PendingCount") %>'></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandArgument='<%# bind("batchid") %>' OnCommand="delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtn_verify" runat="server" CommandArgument='<%# bind("batchid") %>' CommandName="AutoVerify" OnCommand="lnkbtn_verify_Command">Auto Verify</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <table width="100%">
                                            <tr>
                                                <td style="visibility: hidden">
                                                    <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="display: none">
                                                    <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server" Text="Label"></asp:Label>
                                                    <asp:Label ID="lbl_IsSplit" runat="server" Text="Label"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                
                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 

</body>




















































<%--<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780" align="center"
            border="0">
            <tr>
                <td colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34">
                                &nbsp;Search Documents
                            </td>
                            <td class="clssubhead" background="../Images/subhead_bg.gif" height="34" align="right">
                                &nbsp;<asp:HyperLink ID="scan" runat="server" NavigateUrl="~/WebScan/Default.aspx">Scan</asp:HyperLink>&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="clsLeftPaddingTable">
                </td>
            </tr>
            <tr>
                <td class="clsLeftPaddingTable" colspan="2" width="100%">
                    <table id="tbl" cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td class="clsLeftPaddingTable" width="25%">
                                <strong><span style="font-size: 9pt">Search Scanned Batch Dates:</span></strong>
                            </td>
                            <td width="16%">
                                <ew:CalendarPopup ID="fromdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="~/Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td class="clsLeftPaddingTable" width="4%">
                                <strong>To</strong>
                            </td>
                            <td width="16%">
                                <ew:CalendarPopup ID="enddate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                    ImageUrl="~/Images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                    ToolTip="Select Scan Date From" UpperBoundDate="12/31/9999 23:59:00" Width="90px"
                                    LowerBoundDate="1900-01-01">
                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TextboxLabelStyle CssClass="clstextarea" />
                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Gray" />
                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                        ForeColor="Black" />
                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                        Font-Size="XX-Small" ForeColor="Black" />
                                </ew:CalendarPopup>
                            </td>
                            <td width="25%">
                                <asp:CheckBox ID="chk" runat="server" CssClass="clsLeftPaddingTable" Text="Show Batches With Non Verified"
                                    Checked="True" />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="clsLeftPaddingTable" id="tdProcess" style="display: none">
                    <asp:Label ID="lblProess" runat="server" Text="Please Wait Scanning and OCR is in Process..."
                        ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="height: 144px">
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="100%">
                                <asp:GridView ID="gv_Scan" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AllowPaging="True" AllowSorting="True" PageSize="15" OnPageIndexChanging="gv_Scan_PageIndexChanging"
                                    OnRowDataBound="gv_Scan_RowDataBound" OnSorting="gv_Scan_Sorting">
                                    <AlternatingRowStyle BackColor="#EEEEEE" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="GrdHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Batch" SortExpression="batchid">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbatchid" runat="server" Text='<%# bind("batchid") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Images" SortExpression="ImagesCount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblimages" CssClass="label" runat="server" Text='<%# bind("ImagesCount") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Scan Type" SortExpression="scantype">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScanType" CssClass="label" runat="server" Text='<%# bind("scantype") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Scan Date" SortExpression="scandate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDate" CssClass="label" runat="server" Text='<%# bind("scandate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle ForeColor="#006699" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Verified" SortExpression="verifiedCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="verified" ForeColor="RoyalBlue" runat="server" Text='<%# bind("verifiedCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Cause" SortExpression="NoCauseCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="nocause" ForeColor="RoyalBlue" runat="server" Text='<%# bind("NoCauseCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Queued" SortExpression="QueuedCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="queued" ForeColor="RoyalBlue" runat="server" Text='<%# bind("QueuedCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Discrepancy" SortExpression="DiscrepancyCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="discrepancy" ForeColor="RoyalBlue" runat="server" Text='<%# bind("DiscrepancyCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pending" SortExpression="PendingCount">
                                            <HeaderStyle ForeColor="#006699" />
                                            <ItemTemplate>
                                                <asp:HyperLink ID="pending" ForeColor="RoyalBlue" runat="server" Text='<%# bind("PendingCount") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="img_delete" runat="server" ImageUrl="~/Images/cross.gif" CommandArgument='<%# bind("batchid") %>'
                                                    OnCommand="delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtn_verify" runat="server" CommandArgument='<%# bind("batchid") %>'
                                                    CommandName="AutoVerify" OnCommand="lnkbtn_verify_Command">Auto Verify</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                            </td>
                        </tr>
                        <tr>
                            <td style="visibility: hidden">
                                <asp:TextBox ID="txtsessionid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtempid" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtSrv" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
    </div>
    </form>
</body>--%>
</html>

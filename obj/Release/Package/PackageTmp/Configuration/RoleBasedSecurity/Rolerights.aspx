﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Rolerights.aspx.cs" Inherits="HTP.Configuration.RoleBasedSecurity.Rolerights" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Roles Rights</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
//    function select_deselectAll(chkVal, idVal, gv) 
//	{
//		var frm = document.forms[0];
//		var max=0;
//		// Loop through all elements
//		if(gv=='gvMenuRights')
//        max=14;
//        else
//        max=17;
//		for (i=0; i<frm.length; i++) 
//		{
//		    if(frm.elements[i].name.substring(0,max) == gv+'$c' )
//		    {
//			    // Look for our Header Template's Checkbox
//			    if (frm.elements[i].name.indexOf(idVal) > -1 ) 
//			    {
//				    // Check if main checkbox is checked, then select or deselect datagrid checkboxes 
//				    if(chkVal == true) 
//				    {    					
//						    frm.elements[i].checked = true;
//				    } 
//				    else 
//				    {
//						    frm.elements[i].checked = false;
//				    }
//			    }
//			}
//		}
//	}
	function select_deselectAll(chkVal, idValChild, gv) 
	{
	    var a = 0;     
        var c_value = "";
        var grid ="";
        if(gv=='gvMenuRights')
        grid = document.getElementById("<%= gvMenuRights.ClientID%>");
        else
         grid = document.getElementById("<%= gvProcessRights.ClientID%>");
        
        var cell;
        var chk;
                
        if(grid != null && grid.rows.length > 0)
        {       
            for(i = 2; i<= grid.rows.length;i++)
            {
                if( i < 10)
                {
                    chk = gv+'_ctl0'+i.toString()+'_'+idValChild;                    
                }
                else
                {
                    chk = gv+'_ctl'+i.toString()+'_'+idValChild;                         
                }
                
                var chkbox = document.getElementById(chk);
                                
                if(chkbox != null)
                {
                    chkbox.checked = chkVal;
                }
            }            
        }
	}
	function CheckCheckboxSelection(idValHead, idVal, gv) 
	{
	    var a = 0;     
        var c_value = "";
        var grid ="";
        if(gv=='gvMenuRights')
        grid = document.getElementById("<%= gvMenuRights.ClientID%>");
        else
         grid = document.getElementById("<%= gvProcessRights.ClientID%>");
        
        var cell;
        var chk;
         
        if(grid != null && grid.rows.length > 0)
        {       
            for(i = 2; i<= grid.rows.length;i++)
            {
                if( i < 10)
                {
                    chk = gv+'_ctl0'+i.toString()+'_'+idVal;                    
                }
                else
                {
                    chk = gv+'_ctl'+i.toString()+'_'+idVal;                         
                }
                
                var chkbox = document.getElementById(chk);
                                
                if(chkbox != null && chkbox.checked)
                {
                    a += 1;
                    chkbox.checked = true;
                }
            }
            if (grid.rows.length-1 == a)
            {
                document.getElementById(gv + "_ctl01_" + idValHead).checked = true;
            }
            else
            {
                document.getElementById(gv +"_ctl01_" + idValHead).checked = false;
            }
        }
	}
	
	//This function is used to disable Update Button on Client Side.
	function CheckUpdate()
	{
	    document.getElementById("btnUpdate").disabled=true	    
	}

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <span class="clssubhead">Application Name:</span> &nbsp;
                                        <asp:DropDownList ID="ddlApplication" OnChange="CheckUpdate()" runat="server" CssClass="clsInputadministration"
                                            Width="100px">
                                        </asp:DropDownList>
                                        &nbsp; <span class="clssubhead">Role:</span>&nbsp;
                                        <asp:DropDownList ID="ddlRole" OnChange="CheckUpdate()" CssClass="clsInputCombo"
                                            runat="server" DataTextField="RoleName" DataValueField="RoleId">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnSearch" runat="server" CssClass="clsbutton" Text="Search" Width="60px"
                                            OnClick="btnSearch_Click" ValidationGroup="contact" />
                                        &nbsp;&nbsp;
                                        <asp:Button ID="btnUpdate" Enabled="false" runat="server" CssClass="clsbutton" Text="Update"
                                            Width="60px" OnClick="btnUpdate_Click" ValidationGroup="contact" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:ValidationSummary ID="ValidationSummary1" CssClass="clssubhead" DisplayMode="List"
                                            runat="server" ValidationGroup="contact" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="clssubhead" ControlToValidate="ddlApplication"
                                            runat="server" Display="None" InitialValue="0" ErrorMessage="Please select Application."
                                            ValidationGroup="contact"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="clssubhead" ControlToValidate="ddlRole"
                                            runat="server" Display="None" InitialValue="0" ErrorMessage="Please select Role."
                                            ValidationGroup="contact"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px;
                            width: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 50%; vertical-align: top" align="center">
                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="400px">
                                            <asp:Label ID="lblMenuMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                            <asp:GridView ID="gvMenuRights" AutoGenerateColumns="false" CssClass="clsLeftPaddingTable"
                                                runat="server" Width="95%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Menu Title" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMenuTitle" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("TITLE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="clssubhead" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkViewAll" onclick="select_deselectAll(this.checked, 'ChkView','gvMenuRights');"
                                                                runat="server" Text="View" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkView" onclick="CheckCheckboxSelection('ChkViewAll','ChkView','gvMenuRights');"
                                                                runat="server" Checked='<%# Eval("CanView") %>'></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkEditAll" onclick="select_deselectAll(this.checked, 'Chkedit','gvMenuRights');"
                                                                runat="server" Text="Edit" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="Chkedit" onclick="CheckCheckboxSelection('ChkEditAll','Chkedit','gvMenuRights');"
                                                                runat="server" Checked='<%# Eval("CanEdit") %>'></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAddAll" onclick="select_deselectAll(this.checked, 'Chkadd','gvMenuRights');"
                                                                runat="server" Text="Add" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="Chkadd" onclick="CheckCheckboxSelection('ChkAddAll','Chkadd','gvMenuRights');"
                                                                runat="server" Checked='<%# Eval("CanAdd") %>'></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkDeleteAll" onclick="select_deselectAll(this.checked, 'Chkdelete','gvMenuRights');"
                                                                runat="server" Text="Delete" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="Chkdelete" onclick="CheckCheckboxSelection('ChkDeleteAll','Chkdelete','gvMenuRights');"
                                                                runat="server" Checked='<%# Eval("CanDelete") %>'></asp:CheckBox>
                                                            <asp:HiddenField ID="HfMenuId" runat="server" Value='<%# Eval("MenuId") %>' />
                                                            <asp:HiddenField ID="HfRightsId" runat="server" Value='<%# Eval("RightsId") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                    <td valign="top" style="width: 50%; vertical-align: top" align="center">
                                        <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Height="400px">
                                            <asp:Label ID="lblProcessMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                            <asp:GridView ID="gvProcessRights" CssClass="clsLeftPaddingTable" AutoGenerateColumns="false"
                                                runat="server" Width="95%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Process Name" HeaderStyle-CssClass="clssubhead">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProcess" runat="server" CssClass="clsLeftPaddingTable" Text='<%# Eval("ProcessName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="clssubhead">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkExecuteAll" onclick="select_deselectAll(this.checked, 'ChkExecute','gvProcessRights');"
                                                                runat="server" Text="Execute" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="clssubhead" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkExecute" onclick="CheckCheckboxSelection('ChkExecuteAll','ChkExecute','gvProcessRights');"
                                                                runat="server" Checked='<%# Eval("CanExecute") %>'></asp:CheckBox>
                                                            <asp:HiddenField ID="HfProcessId" runat="server" Value='<%# Eval("ProcessId") %>' />
                                                            <asp:HiddenField ID="HfRightId" runat="server" Value='<%# Eval("RightsId") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:Footer ID="Footer1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

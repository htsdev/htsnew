﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="HTP.Configuration.RoleBasedSecurity.Users" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Footer" Src="~/WebControls/Footer.ascx" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User</title>
    <link href="../../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../../Scripts/jsDate.js" type="text/javascript"></script>

</head>
<body>

    <script type="text/javascript" language="javascript">
        
        
        
		var whitespace = " \t\n\r";

		
		   function isWhitespace (s)
           {   var i;

    
            if (isEmpty(s)) return true;
            for (i = 0; i < s.length; i++)
            {          
                var c = s.charAt(i);
                if (whitespace.indexOf(c) == -1) return false;
            }
     
            return true;
           }

		
		
        function stripWhitespace (s)
        {   return stripCharsInBag (s, whitespace)
        }
		
		function isEmpty(s)
        {   return ((s == null) || (s.length == 0))
        }
		
		function isEmail (s)
        {   if (isEmpty(s)) 
               if (isEmail.arguments.length == 1) return false;
               else return (isEmail.arguments[1] == true);
           
            
            if (isWhitespace(s)) return false;
               
            
            
            var i = 1;
            var sLength = s.length;

            
            while ((i < sLength) && (s.charAt(i) != "@"))
            { i++
            }

            if ((i >= sLength) || (s.charAt(i) != "@")) return false;
            else i += 2;

            
            while ((i < sLength) && (s.charAt(i) != "."))
            { i++
            }

            
            if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
            else return true;
        }
        // Function to check the letter in the input
	    function alphanumeric(alphane)
        {
	        var numaric = alphane;
	        for(var j=0; j<numaric.length; j++)
		        {
		              var alphaa = numaric.charAt(j);
		              var hh = alphaa.charCodeAt(0);
		              if (!((hh > 64 && hh<91) || (hh > 96 && hh<123)))
		              {
		              return false;
		              }    
		        }
	    }
	
		    function trimAll(sString) 
		    {
			    while (sString.substring(0,1) == ' ')
			    {
				    sString = sString.substring(1, sString.length);
			    }
			    while (sString.substring(sString.length-1, sString.length) == ' ')
			    {
				    sString = sString.substring(0,sString.length-1);
			    }
			    return sString;
		    }
		    //Function to accpet only alphabets and numbers removing special character
		    function CheckName(name)
            {       
                for ( i = 0 ; i < name.length ; i++)
                {
                    var asciicode =  name.charCodeAt(i)
                    if (  ! ((asciicode >= 64 && asciicode <=90) || ( asciicode >= 97 && asciicode <=122)||(asciicode >= 48 && asciicode <=57)))
                    return false;
               }
                return true;
            }
		
		        function formSubmit() 
		        {
        		    	    
                    var lname=document.getElementById("tb_lname").value;
                    
                    if (trimAll(lname) == "") {
	                    alert("Please specify Last Name");
	                    document.getElementById("tb_lname").focus();
	                    return false;
                    }
                    
                    if (!isNaN(lname))
                    {
                        alert("Name should only contains alphabets.");
                        return false;
                    }           
                    
                    if(alphanumeric(lname)==false)
                    {
                        alert("Name should only contains alphabets.");
                        return false;
                    }	
                    var fname=document.getElementById("tb_fname").value;
                    
                    if (trimAll(fname) == "") 
                    {
	                    alert("Please specify First Name");
	                    document.getElementById("tb_fname").focus();
	                    return false;
                    }
                    
                    if (!isNaN(fname))
                    {
                        alert("Name should only contains alphabets.");
                        return false;
                    }           
                    
                    if(alphanumeric(fname)==false)
                    {
                        alert("Name should only contains alphabets.");
                        return false;
                    }	

                    if (trimAll(document.getElementById("tb_abbrev").value) == "") {
	                    alert("Please specify Short Name");
	                    document.getElementById("tb_abbrev").focus();
	                    return  false;
                    }

                    if (trimAll(document.getElementById("tb_uname").value) == "") {
	                    alert("Please specify User Name");
	                    document.getElementById("tb_uname").focus();
	                    return  false;
                    }
                    if (CheckName (document.getElementById("tb_uname").value)== false)
                    {
                        alert("User name must only contain alphabets and numbers");
	                    document.getElementById("tb_uname").focus();
                        return false;
                    }
                    
                    if(document.getElementById("trPassword").style.display!="none")
                    {
                        if (trimAll(document.getElementById("tb_password").value) == "") {
	                        alert("Please specify Password");
	                        document.getElementById("tb_password").focus();
	                        return  false;
                        }
                        //Allow only alphabets and number in password n user name
                        if (CheckName (document.getElementById("tb_password").value)== false)
                        {
                        
                            alert("Password must only contain alphabets and numbers");
	                        document.getElementById("tb_password").focus();
                            return false;
                        }
                        
                         if (trimAll(document.getElementById("tb_passwordReType").value) == "") {
	                        alert("Please Re-Type Password");
	                        document.getElementById("tb_passwordReType").focus();
	                        return  false;
                        }                    
                        //Allow only alphabets and number in password n user name
                        if (CheckName (document.getElementById("tb_passwordReType").value)== false)
                        {
                        
                            alert("Password must only contain alphabets and numbers");
	                        document.getElementById("tb_passwordReType").focus();
                            return false;
                        }
                        //Password and Re-Type Password must be same
                        if (trimAll(document.getElementById("tb_password").value)!= trimAll(document.getElementById("tb_passwordReType").value))
                        {
                        
                            alert("Password and Re-Type Password must be same.");
	                        document.getElementById("tb_passwordReType").focus();
                            return false;
                        }
                    }    
                    if(trimAll(document.getElementById("txt_email").value) == "")
                    { 
	                    alert("Please specify Email Address");
	                    document.getElementById("txt_email").focus();
	                    return false;
                    }
                    else
                    { 
	                   if(isEmail(document.getElementById("txt_email").value)== false)
			           {
			               alert ("Please enter Email Address in Correct format.");
			               document.getElementById("txt_email").focus(); 
			               return false;			   
			           }
                    } 
                    if(document.getElementById("trRC").style.display!="none")
                    {
                        if(document.getElementById("ddlRole").value!="0")
                        {
                            if(document.getElementById("ddlCompany").value=="0")
                            {
                                alert ("Please select Company.");
			                    document.getElementById("ddlCompany").focus(); 
			                    return false;	
			                }
                        }
                        if(document.getElementById("ddlCompany").value!="0")
                        {
                            if(document.getElementById("ddlRole").value=="0")
                            {
                                alert ("Please select Role.");
			                    document.getElementById("ddlRole").focus(); 
			                    return false;	
			                }
                        }
                    }
                    if(document.getElementById("ddlStatus").value=="-1")
                    {
                        alert ("Please select Status.");
			            document.getElementById("ddlStatus").focus(); 
			            return false;	
                    }
                    //Ozair 6766 04/28/2010 Issue Fix for inactive user in User in Role
                    if(document.getElementById("ddlStatus").value!="1" && document.getElementById("ddlCompany").value!="0" && document.getElementById("ddlRole").value!="0")
                    {
                        alert("Role and Company can only be added for Active status.");
                        document.getElementById("ddlStatus").focus(); 
			            return false;	
                    }
                    return true;
                }
                
                  function HideAddPopup() 
                        {
                            var modalPopupBehavior = $find('mpeConfigAdd');
                            modalPopupBehavior.hide();
                            return false;
                        }

            function HideResetPasswordPopup()
	        {
                var modalPopupBehavior = $find('mpeResetPassword');
                modalPopupBehavior.hide();
                return false;
            }
            
            function ResetPasswordSubmit()
            {
                        if (trimAll(document.getElementById("txtNewPassword").value) == "") {
	                        alert("Please specify New Password");
	                        document.getElementById("txtNewPassword").focus();
	                        return  false;
                        }
                        //Allow only alphabets and number in password n user name
                        if (CheckName (document.getElementById("txtNewPassword").value)== false)
                        {                        
                            alert("Password must only contain alphabets and numbers");
	                        document.getElementById("txtNewPassword").focus();
                            return false;
                        }
                        
                         if (trimAll(document.getElementById("txtReTypeNewPassword").value) == "") {
	                        alert("Please Re-Type New Password");
	                        document.getElementById("txtReTypeNewPassword").focus();
	                        return  false;
                        }                    
                        //Allow only alphabets and number in password n user name
                        if (CheckName (document.getElementById("txtReTypeNewPassword").value)== false)
                        {
                        
                            alert("Password must only contain alphabets and numbers");
	                        document.getElementById("txtReTypeNewPassword").focus();
                            return false;
                        }
                        //Password and Re-Type Password must be same
                        if (trimAll(document.getElementById("txtNewPassword").value)!= trimAll(document.getElementById("txtReTypeNewPassword").value))
                        {
                        
                            alert("New Password and Re-Type New Password must be same.");
	                        document.getElementById("txtReTypeNewPassword").focus();
                            return false;
                        }
            }
    </script>

    <form id="form1" runat="server">
    <div ms_positioning="GridLayout">
        <aspnew:ScriptManager ID="ScriptManager" runat="server" />
        <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="850" align="center"
                    border="0">
                    <tbody>
                        <tr>
                            <td>
                                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 14px" width="100%" background="../../Images/separator_repeat.gif"
                                height="14">
                            </td>
                        </tr>
                        <tr>
                            <td class="clsLeftPaddingTable">
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="right" class="clsLeftPaddingTable">
                                            <asp:LinkButton ID="lnk_AddNewRecord" runat="server" OnClick="lnk_AddNewRecord_Click">Add New User</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 14px" width="100%" background="../../Images/separator_repeat.gif"
                                height="14">
                            </td>
                        </tr>
                        <tr>
                            <td id="pri">
                                <table id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                    <tr>
                                        <td class="clssubhead" valign="middle" colspan="4" align="left" background="../../Images/subhead_bg.gif">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:RadioButtonList ID="rbl_status" runat="server" CssClass="clssubhead" OnSelectedIndexChanged="rbl_status_SelectedIndexChanged"
                                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Value="1" Selected="True">&lt;span class=&quot;clssubhead&quot; &gt;Active Users &lt;/span&gt;</asp:ListItem>
                                                            <asp:ListItem Value="0">&lt;span class=&quot;clssubhead&quot; &gt;Inactive Users&lt;/span&gt;</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td align="right" class="clssubhead">
                                                        <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clsLeftPaddingTable" style="height: 16px" background="../../Images/separator_repeat.gif">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%">
                                                        <asp:GridView ID="dg_results" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            CssClass="clsLeftPaddingTable" OnRowDataBound="dg_results_RowDataBound" AllowPaging="True"
                                                            PageSize="20" OnRowCommand="dg_results_RowCommand" OnPageIndexChanging="dg_results_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="HlnkLname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'
                                                                            Visible="False">
                                                                        </asp:HyperLink>
                                                                        <asp:LinkButton ID="LnkbtnLname" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'
                                                                            CommandName="click">
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblFname" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
                                                                        </asp:Label>
                                                                        <asp:HiddenField ID="HfUserId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.UserId") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Short Name" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblAbbrev" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Abbreviation") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblUname" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblEmail" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="NTUserID">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblNtUserId" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.NTUserID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblStatus" runat="server" Text='<%# (Convert.ToBoolean(Eval("IsActive")) == false) ? "InActive" : "Active" %>'
                                                                            CssClass="GridItemStyle"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LnkbtnResetPassword" runat="server" Text="Reset Password"
                                                                            CommandName="reset">
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="clssubhead" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                                FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" background="../../Images/separator_repeat.gif" height="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 760px" align="left" colspan="4">
                                            <uc2:Footer ID="Footer1" runat="server"></uc2:Footer>
                                            <asp:Label ID="lblRecCount" runat="server" ForeColor="White"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ajaxToolkit:ModalPopupExtender ID="mpeConfigAdd" runat="server" BackgroundCssClass="modalBackground"
                                                PopupControlID="pnlAdd" TargetControlID="btn123">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <asp:Panel ID="pnlAdd" runat="server" Width="700px" Style="display: none;">
                                                <table id="table7" style="border-top: black thin solid; border-left: black thin solid;
                                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                                    cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                                    <tr>
                                                        <td background="../../Images/subhead_bg.gif" valign="bottom" width="100%" style="height: 32px">
                                                            <table border="0" width="100%" style="height: 100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <span class="clssubhead">User Information:</span>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:LinkButton ID="lbtn_close" OnClientClick="return HideAddPopup();" runat="server">X</asp:LinkButton>&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" valign="top">
                                                            <table id="Table2" bordercolor="#ffffff" cellspacing="1" cellpadding="1" width="100%"
                                                                border="0">
                                                                <tr>
                                                                    <td align="left" class="clssubhead" style="width: 100px; height: 22px">
                                                                        Last Name :
                                                                    </td>
                                                                    <td class="clssubhead" style="width: 155px; height: 22px" align="right">
                                                                        <asp:TextBox ID="tb_lname" runat="server" CssClass="clsInputadministration" Width="150px"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="clssubhead" style="width: 110px; height: 22px">
                                                                        First Name :
                                                                    </td>
                                                                    <td class="clssubhead" style="width: 155px; height: 22px" align="right">
                                                                        <asp:TextBox ID="tb_fname" runat="server" CssClass="clsInputadministration" Width="150px"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="clssubhead" style="width: 100px; height: 22px">
                                                                        Short Name:
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="height: 22px; width: 155px;">
                                                                        <asp:TextBox ID="tb_abbrev" runat="server" CssClass="clsInputadministration" Width="150px"
                                                                            MaxLength="3"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="clssubhead" style="width: 110px; height: 22px">
                                                                        User Name :
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="tb_uname" runat="server" CssClass="clsInputadministration" Width="150px"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="trPassword">
                                                                    <td align="left" class="clssubhead" style="width: 100px; height: 22px">
                                                                        Password :
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="tb_password" runat="server" TextMode="Password" CssClass="clsInputadministration"
                                                                            Width="150px" MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="clssubhead" style="width: 110px; height: 22px">
                                                                        Re-Type Password:
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="tb_passwordReType" TextMode="Password" runat="server" CssClass="clsInputadministration"
                                                                            Width="150px" MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="clssubhead" style="width: 100px; height: 22px">
                                                                        Email Address :
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="txt_email" runat="server" CssClass="clsInputadministration" Width="150px"
                                                                            MaxLength="100"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" style="width: 110px; height: 22px" class="clssubhead">
                                                                        NT User ID :
                                                                    </td>
                                                                    <td align="right" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="txt_NTUserID" runat="server" CssClass="clsInputadministration" MaxLength="20"
                                                                            Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="trRC">
                                                                    <td align="left" class="clssubhead" style="width: 100px; height: 22px">
                                                                        Role :
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:DropDownList ID="ddlRole" runat="server" CssClass="clsInputadministration" Width="154px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="left" style="width: 110px; height: 22px" class="clssubhead">
                                                                        Company :
                                                                    </td>
                                                                    <td align="right" style="width: 155px; height: 22px">
                                                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="clsInputadministration"
                                                                            Width="154px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width: 100px; height: 22px" class="clssubhead">
                                                                        Status :
                                                                    </td>
                                                                    <td align="right" style="width: 155px; height: 22px">
                                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="clsInputadministration"
                                                                            Width="154px">
                                                                            <asp:ListItem Value="-1">--Choose--</asp:ListItem>
                                                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                                                            <asp:ListItem Value="0">InActive</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="left" colspan="2" style="height: 22px">
                                                                        <asp:Button ID="btn_update" runat="server" Width="50px" CssClass="clsbutton" OnClick="btn_update_Click"
                                                                            Text="Update" OnClientClick="return formSubmit();" ></asp:Button>
                                                                        <asp:Button ID="btn_clear" runat="server" CssClass="clsbutton" OnClick="btn_clear_Click"
                                                                            Width="50px" Text="Clear"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Button ID="btn123" runat="server" Style="display: none;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ajaxToolkit:ModalPopupExtender ID="mpeResetPassword" runat="server" BackgroundCssClass="modalBackground"
                                                PopupControlID="pnlResetPassword" TargetControlID="btnDummyResetPassword">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <asp:Panel ID="pnlResetPassword" runat="server" Width="320px" Style="display: none;">
                                                <table id="table3" style="border-top: black thin solid; border-left: black thin solid;
                                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                                    cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                                    <tr>
                                                        <td background="../../Images/subhead_bg.gif" valign="bottom" width="100%" style="height: 32px">
                                                            <table border="0" width="100%" style="height: 100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <span class="clssubhead">Reset Password:</span>
                                                                    </td>
                                                                    <td align="right">
                                                                        <asp:LinkButton ID="lnkbtnCloseResetPasswordPopup" OnClientClick="return HideResetPasswordPopup();"
                                                                            runat="server">X</asp:LinkButton>&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable" valign="top">
                                                            <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="1" border="0">
                                                                <tr runat="server">
                                                                    <td align="left" class="clssubhead" style="width: 140px; height: 22px">
                                                                        New Password :
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="clsInputadministration"
                                                                            Width="150px" MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="clssubhead" style="width: 135px; height: 22px">
                                                                        Re-Type New Password:
                                                                    </td>
                                                                    <td align="right" class="clssubhead" style="width: 155px; height: 22px">
                                                                        <asp:TextBox ID="txtReTypeNewPassword" TextMode="Password" runat="server" CssClass="clsInputadministration"
                                                                            Width="150px" MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="2" style="height: 22px">
                                                                        <asp:Button ID="btnResetPassword" runat="server" Width="50px" CssClass="clsbutton"
                                                                            Text="Reset" OnClientClick="return ResetPasswordSubmit();" onclick="btnResetPassword_Click1"></asp:Button>&nbsp;
                                                                        <asp:Button ID="btnClearResetPassword" runat="server" CssClass="clsbutton" Width="50px"
                                                                            Text="Clear" onclick="btnClearResetPassword_Click"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Button ID="btnDummyResetPassword" runat="server" Style="display: none;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </aspnew:UpdatePanel>
    </div>
    </form>
</body>
</html>

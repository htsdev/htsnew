﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourtAssociation.aspx.cs"
    Inherits="lntechNew.Configuration.CourtAssociation" EnableViewState="true" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagPrefix="uc1" TagName="activemenu" %>
<%@ Register Src="~/WebControls/PagingControl.ascx" TagName="pagingcontrol" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Styles.css" type="text/css" rel="Stylesheet" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
    function SetRoomValue(StatusID,CourtRoomNumber,chk)
    {

        var statusroom = $get("wizCourtAssociation_hfStatusRoom");
        if(chk.checked)
        {
         statusroom.value = statusroom.value + StatusID + '-' + CourtRoomNumber + '-' + '1' + ','
        }
        else
        {
         statusroom.value = statusroom.value + StatusID + '-' + CourtRoomNumber + '-' + '0' + ','
        }
    }
    
        function SetTimeValue(StatusID,CourtRoomNumber,TimeID,chk)
            {
            debugger;
                var statusroomTime = $get("wizCourtAssociation_hfStatusRoomTime");
                if(chk.checked)
                {
                 statusroomTime.value = statusroomTime.value + StatusID + '-' + CourtRoomNumber + '-' + TimeID + '-' +  '1' + ','
                }
                else
                {
                 statusroomTime.value = statusroomTime.value + StatusID + '-' + CourtRoomNumber + '-' + TimeID + '-' + '0' + ','
                }
            }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp35:ScriptManager ID="ScriptManager1" runat="server">
        </asp35:ScriptManager>
        <table cellpadding="0" cellspacing="0" width="900px" align="center" border="0">
            <tr>
                <td>
                    <uc1:activemenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%">
                    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClientClick="" Text="Button" />
                    <asp:Button ID="Button2" runat="server" OnClientClick="" Text="Button" />
                </td>
            </tr>
            <tr id="tr_seperator" runat="server" style="display: none;">
                <td background="../../images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hf_ticketviolationids" runat="server" />
                </td>
            </tr>
        </table>
        <ajaxToolkit:ModalPopupExtender PopupControlID="pnlAddRule" TargetControlID="Button1"
            runat="server" CancelControlID="Button2" ID="mpCourtAssociation">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnlAddRule" runat="server">
            <table id="Table1" style="border-top: black thin solid; border-left: black thin solid;
                border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                cellspacing="0" width="400px" class="clsLeftPaddingTable" runat="server">
                <tr>
                    <td background="../Images/subhead_bg.gif" valign="bottom" width="100%" style="height: 32px">
                        <table border="0" width="100%" style="height: 100%">
                            <tr>
                                <td align="left">
                                    <span class="clssubhead">Add Configuration Settings</span>
                                </td>
                                <td align="right">
                                    <asp:LinkButton ID="lbtn_close" OnClientClick="return HideAddPopup();" runat="server">X</asp:LinkButton>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp35:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Wizard runat="server" ID="wizCourtAssociation" ActiveStepIndex="0" OnNextButtonClick="wizCourtAssociation_NextButtonClick"
                                    DisplaySideBar="false" OnPreviousButtonClick="wizCourtAssociation_PreviousButtonClick"
                                    OnFinishButtonClick="wizCourtAssociation_FinishButtonClick">
                                    <WizardSteps>
                                        <asp:WizardStep Title="Division and Court" runat="server" ID="Step1">
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label1" runat="server" Text="Division"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDivision" runat="server">
                                                                <asp:ListItem Text="Houston" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Dallas" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label2" runat="server" Text="Court"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCourt" runat="server">
                                                                <asp:ListItem Text="Houston" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Dallas" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:WizardStep>
                                        <asp:WizardStep Title="Status" runat="server" ID="Step2">
                                            <asp:Panel ID="Panel3" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label3" runat="server" Text="Division"></asp:Label>
                                                            <asp:Label ID="lblDivision" runat="server" Text="Division"></asp:Label>
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label4" runat="server" Text="Court"></asp:Label>
                                                            <asp:Label ID="lblCourt" runat="server" Text="Court"></asp:Label>
                                                        </td>
                                                     
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBoxList ID="ChkStatus" runat="server">
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:WizardStep>
                                        <asp:WizardStep ID="Step3" Title="Room Number" runat="server">
                                            <asp:Panel ID="PnlRoom" runat="server">
                                                <table id="tbl_room" runat="server">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label5" runat="server" Text="Division"></asp:Label>
                                                            <asp:Label ID="Label6" runat="server" Text="Division"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label7" runat="server" Text="Court"></asp:Label>
                                                            <asp:Label ID="Label8" runat="server" Text="Court"></asp:Label>
                                                            <asp:HiddenField ID="hfStatusRoom" runat="server" />
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:WizardStep>
                                        <asp:WizardStep ID="Step4" Title="Room Number Time" runat="server">
                                            <asp:Panel ID="Panel4" runat="server">
                                                <table id="tbl_CourtTime" class="clsLeftPaddingTable" runat="server">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label9" runat="server" Text="Division"></asp:Label>
                                                        </td>
                                                        <td colspan="20">
                                                            <asp:Label ID="Label10" runat="server" Text="Division"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label11" runat="server" Text="Court"></asp:Label>
                                                        </td>
                                                        <td colspan="20">
                                                            <asp:Label ID="Label12" runat="server" Text="Court"></asp:Label>
                                                            <asp:HiddenField ID="hfStatusRoomTime" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:WizardStep>
                                    </WizardSteps>
                                </asp:Wizard>
                            </ContentTemplate>
                        </asp35:UpdatePanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>

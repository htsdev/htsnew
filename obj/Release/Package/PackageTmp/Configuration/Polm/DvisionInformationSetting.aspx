﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DvisionInformationSetting.aspx.cs"
    Inherits="HTP.Configuration.Polm.DvisionInformationSetting" %>

<%@ Register Src="~/WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Source Information Setting</title>
    <link href="../../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="~/Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
    <aspnew:UpdatePanel ID="pnl_main" runat="server">
        <ContentTemplate>
            <div>
                <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
                    border="0">
                    <tr>
                        <td>
                            <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" align="right">
                            <span class="clssubhead">Source :</span>
                            <asp:DropDownList ID="dd_division" AutoPostBack="true" CssClass="clsInputCombo" runat="server"
                                OnSelectedIndexChanged="dd_division_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px;
                            width: 819px;">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        &nbsp;<span class="clssubhead">Quick Legal Matter Description Setting</span>
                                    </td>
                                    <td align="right">
                                        <a href="QuickLegalMatterSetting.aspx"><u>Details</u></a>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="clsLeftPaddingTable">
                            <table>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:Label ID="lblQLMDMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="clssubhead">UnAssigned Quick Legal Matter Description</span>
                                        <asp:ListBox ID="lbUnAssignedQLMD" runat="server" class="clsInputCombo" Width="275px"
                                            Height="200px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                    <td style="width: 10%" align="center">
                                        <asp:ImageButton ID="imgAssignQLMD" runat="server" ImageUrl="~/Images/rightArrow-crop.gif"
                                            OnClick="imgAssignQLMD_Click" /><br />
                                        <br />
                                        <asp:ImageButton ID="imgUnassignQLMD" runat="server" ImageUrl="~/Images/arrow-crop.gif"
                                            OnClick="imgUnassignQLMD_Click" />
                                    </td>
                                    <td>
                                        <span class="clssubhead">Assigned Quick Legal Matter Description</span>
                                        <asp:ListBox ID="lbAssignedQLMD" runat="server" class="clsInputCombo" Width="275px"
                                            Height="200px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px;
                            width: 819px;">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        &nbsp;<span class="clssubhead">Bodily Damage Setting</span>
                                    </td>
                                    <td align="right">
                                        <a href="BodilyDamageSetting.aspx"><u>Details</u></a>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="clsLeftPaddingTable">
                            <table>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:Label ID="lblBodilyDamageMessage" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="clssubhead">UnAssigned Bodily Damage Value</span>
                                        <asp:ListBox ID="lbUnassignedBD" runat="server" class="clsInputCombo" Width="275px"
                                            Height="200px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                    <td style="width: 10%" align="center">
                                        <asp:ImageButton ID="imgAssignBodilyDamage" runat="server" ImageUrl="~/Images/rightArrow-crop.gif"
                                            OnClick="imgAssignBodilyDamage_Click" Style="height: 19px" /><br />
                                        <br />
                                        <asp:ImageButton ID="imgUnassignBodilyDamage" runat="server" ImageUrl="~/Images/arrow-crop.gif"
                                            OnClick="imgUnassignBodilyDamage_Click" />
                                    </td>
                                    <td>
                                        <span class="clssubhead">Assigned Bodily Damage Value</span>
                                        <asp:ListBox ID="lbAssignedBD" runat="server" class="clsInputCombo" Width="275px"
                                            Height="200px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../../Images/separator_repeat.gif" height="9" width="750px">
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </aspnew:UpdatePanel>
    </form>
</body>
</html>

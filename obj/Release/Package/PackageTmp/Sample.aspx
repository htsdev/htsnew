﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sample.aspx.cs" Inherits="lntechNew.Sample" %>


<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="gw" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Courts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body class=" ">
    <form id="frmcourts" method="post" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>


            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Form Elements - Basic Fields</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>

                            <div class="pull-right hidden-xs">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="index.html"><i class="fa fa-home"></i>Home</a>
                                    </li>
                                    <li>
                                        <a href="form-elements.html">Form Elements</a>
                                    </li>
                                    <li class="active">
                                        <strong>Field Elements</strong>
                                    </li>
                                </ol>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- MAIN CONTENT AREA STARTS -->

                    <div class="col-xs-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Basic Elements</h2>
                                <div class="actions panel_actions pull-right">
                                    <a class="box_toggle fa fa-chevron-down"></a>
                                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                    <a class="box_close fa fa-times"></a>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-md-8 col-sm-9 col-xs-10">

                                        <div class="form-group">
                                            <label class="form-label" for="field-1">Name</label>
                                            <span class="desc">e.g. "Beautiful Mind"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-1">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="field-2">Password</label>
                                            <span class="desc">e.g. "Cre@t!v!ty"</span>
                                            <div class="controls">
                                                <input type="password" value="Cre@t!v!ty" class="form-control" id="field-2">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="field-3">Email</label>
                                            <span class="desc">e.g. "me@somesite.com"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-3">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="field-4">Placeholder</label>
                                            <span class="desc">e.g. "Username"</span>
                                            <div class="controls">
                                                <input type="text" id="field-4" placeholder="Enter your desired text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="field-5">Disabled</label>
                                            <span class="desc">e.g. "non-editable text"</span>
                                            <div class="controls">
                                                <input type="text" id="field-5" placeholder="Disabled input field" class="form-control" disabled="disabled">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="field-6">Text area</label>
                                            <span class="desc">e.g. "Enter any size of text description here"</span>
                                            <div class="controls">
                                                <textarea class="form-control" cols="5" id="field-6"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="field-7">Auto grow</label>
                                            <span class="desc">e.g. "This text area field auto grows"</span>
                                            <div class="controls">
                                                <textarea class="form-control autogrow" cols="5" id="field-7" placeholder="This textarea will grow in size with new lines." style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 50px;"></textarea>
                                            </div>
                                        </div>


                                        <div class="form-group has-error">
                                            <label class="form-label" for="field-8">Error Field</label>
                                            <span class="desc">e.g. "Beautiful Mind"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-8" placeholder="I am a error field">
                                            </div>
                                        </div>

                                        <div class="form-group has-warning">
                                            <label class="form-label" for="field-9">Warning Field</label>
                                            <span class="desc">e.g. "Beautiful Mind"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-9" placeholder="I am a warning field">
                                            </div>
                                        </div>

                                        <div class="form-group has-success">
                                            <label class="form-label" for="field-10">Success Field</label>
                                            <span class="desc">e.g. "Beautiful Mind"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-10" placeholder="I am a success field">
                                            </div>
                                        </div>

                                        <div class="form-group has-info">
                                            <label class="form-label" for="field-11">Info Field</label>
                                            <span class="desc">e.g. "Beautiful Mind"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-11" placeholder="I am a info field">
                                            </div>
                                        </div>

                                        <div class="form-group has-focus">
                                            <label class="form-label" for="field-12">Focus</label>
                                            <span class="desc">e.g. "Beautiful Mind"</span>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-12" placeholder="I am a focused field ">
                                            </div>
                                        </div>

                                        <div class="form-group has-help">
                                            <label class="form-label" for="field-13">Help Field</label>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="field-13" placeholder="I am a help field">
                                                <span class="help-block">A help or brief description message of the input field.</span>
                                            </div>
                                        </div>

                                        <div class="form-group has-static">
                                            <label class="form-label">Static Field</label>
                                            <div class="controls">
                                                <p class="form-control-static">A static text field</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </section>
                    </div>



                    <div class="col-xs-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Input Groups</h2>
                                <div class="actions panel_actions pull-right">
                                    <a class="box_toggle fa fa-chevron-down"></a>
                                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                    <a class="box_close fa fa-times"></a>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-md-8 col-sm-9 col-xs-10">

                                        <div class="input-group transparent">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Transparent">
                                        </div>

                                        <br>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="arrow"></span>
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Default">
                                        </div>

                                        <br>

                                        <div class="input-group primary">
                                            <span class="input-group-addon">
                                                <span class="arrow"></span>
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Primary">
                                        </div>


                                        <br>


                                        <div class="input-group primary">
                                            <input type="text" class="form-control text-right" placeholder="Right Align" aria-describedby="basic-addon1">
                                            <span class="input-group-addon" id="basic-addon1"><span class="arrow"></span><i class="fa fa-user"></i></span>
                                        </div>
                                        <br>

                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2">@example.com</span>
                                        </div>
                                        <br>


                                        <div class="input-group primary">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                                            <span class="input-group-addon">.00</span>
                                        </div>







                                        <br>

                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control">
                                            <span class="input-group-addon">.00</span>
                                        </div>

                                        <br>

                                        <div class="input-group primary">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-red dropdown-toggle" data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>

                                                <ul class="dropdown-menu dropdown-red no-spacing">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </span>

                                            <input type="text" class="form-control no-left-border form-focus-red" placeholder="Dropdown">
                                        </div>

                                        <br>


                                        <div class="input-group m-bot15">
                                            <span class="input-group-addon">
                                                <input type="checkbox" class="iCheck">
                                            </span>
                                            <input type="text" class="form-control" placeholder="Input with Checkbox">
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>

                    <div class="col-xs-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Element Sizes</h2>
                                <div class="actions panel_actions pull-right">
                                    <a class="box_toggle fa fa-chevron-down"></a>
                                    <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                    <a class="box_close fa fa-times"></a>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-md-8 col-sm-9 col-xs-10">

                                        <input class="form-control input-lg m-bot15" type="text" placeholder=".input-lg">
                                        <br>
                                        <input class="form-control m-bot15" type="text" placeholder="Default input">
                                        <br>
                                        <input class="form-control input-sm m-bot15" type="text" placeholder=".input-sm">
                                        <br>

                                        <select class="form-control input-lg m-bot15">
                                            <option>Option 1</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                        <br>
                                        <select class="form-control m-bot15">
                                            <option>Option 1</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>
                                        <br>
                                        <select class="form-control input-sm m-bot15">
                                            <option>Option 1</option>
                                            <option>Option 2</option>
                                            <option>Option 3</option>
                                        </select>

                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>




                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->
            
        </div>
        <!-- END CONTAINER -->

    </form>
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START -->
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->


    <!-- General section box modal start -->
    <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog animated bounceInDown">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Section Settings</h4>
                </div>
                <div class="modal-body">
                    Body goes here...

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success" type="button">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal end -->
</body>
</html>

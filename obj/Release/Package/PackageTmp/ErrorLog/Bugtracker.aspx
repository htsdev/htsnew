﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" Codebehind="Bugtracker.aspx.cs" AutoEventWireup="True" Inherits="HTP.Errorlog.Bugtracker" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Bugtracker</title>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />

        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

		<SCRIPT SRC="BoxOver.js"></SCRIPT>
		<SCRIPT SRC="../Scripts/ClipBoard.js"></SCRIPT>
		<script language="JavaScript">
		
		/* Commented by Farhan Sabir
		
		var refreshinterval=60
		var displaycountdown="yes"
		var starttime
		var nowtime
		var reloadseconds=0
		var secondssinceloaded=0

		function starttime() {
			starttime=new Date()
			starttime=starttime.getTime()
			countdown()
		}

		function countdown() {
			nowtime= new Date()
			nowtime=nowtime.getTime()
			secondssinceloaded=(nowtime-starttime)/1000
			reloadseconds=Math.round(refreshinterval-secondssinceloaded)
			if (refreshinterval>=secondssinceloaded) {
				var timer=setTimeout("countdown()",1000)
				if (displaycountdown=="yes") {
					window.status="Page refreshing in "+reloadseconds+ " seconds"
				}
			}
			else {
				clearTimeout(timer)
				window.location.reload(true)
			} 
		}
		window.onload=starttime
		*/
		
		var err = null;
		
		function StateTracePoupup(ControlName)
		{	
		     CursorIcon();
		     err = null;
		     err = document.getElementById(ControlName).value;
		     ShowMsg()
		     //document.getElementById("txtb_StateTraceMsg").value = err;
		     //document.getElementById("txtb_StateTraceMsg").style.visibility = 'visible';
		}
		
		
		function validate()
		{
			
			//var d1 = document.getElementById("dtp_dtFrom").value;
		    //var d2 = document.getElementById("dtp_DtTo").value;		

		    var d1 = document.getElementById("txt_dtFrom").value;
		    var d2 = document.getElementById("txt_DtTo").value;

			if (d1 == d2)
				return true;
			
				var diff = DateDiff(d2,d1);
				if(diff<0)
				{
					alert("Invalid date range! Please make sure that the 'From Date' is greater than 'To Date'");
					document.getElementById("txt_DtTo").focus(); 
					return false;
				}
				
				else
					return true;
		}
		
		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
		
		//change cursor icon
		function CursorIcon()
		{
		    document.body.style.cursor = 'pointer';
		}
		function CursorIcon2()
		{
		    document.body.style.cursor = 'default';
		}
		
		
		</script>

        
	</HEAD>

    <body class=" ">

        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">
                                    <!-- PAGE HEADING TAG - START -->
                                    <h1 class="title">Error Log</h1>
                                    <!-- PAGE HEADING TAG - END -->    
                                </div>


                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                        <section class="box ">

                                <div class="content-body">

                                    <div class="row">

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">From</label>
                                                <div class="controls">
                                                    <%--<ew:CalendarPopup ID="dtp_dtFrom" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif">
                                                        <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
                                                    </ew:CalendarPopup>--%>
                                                    <asp:TextBox ID="txt_dtFrom" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">To</label>
                                                <div class="controls">
                                                    <%--<ew:CalendarPopup ID="dtp_DtTo" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif">
                                                        <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
                                                    </ew:CalendarPopup>--%>
                                                    <asp:TextBox ID="txt_DtTo" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">To</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddl_Options" runat="server" AutoPostBack="True" CssClass="form-control m-bot15" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                        <asp:ListItem>Houston Traffic System</asp:ListItem>
                                                        <asp:ListItem>Dallas Traffic System</asp:ListItem>
                                                        <asp:ListItem>Public Site</asp:ListItem>
                                                        <asp:ListItem>QuickBooks</asp:ListItem>
                                                        <asp:ListItem>LoaderSservice</asp:ListItem>
                                                        <asp:ListItem>E-Signature</asp:ListItem>
                                                        <asp:ListItem>Outlook Addin</asp:ListItem>
                                                        <asp:ListItem>SulloLaw</asp:ListItem>
                                                        <asp:ListItem>POLM</asp:ListItem>
                                                        <asp:ListItem>Product Defects</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-3 col-sm-4 col-xs-5">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">&nbsp;</label>
                                                <div class="controls">
                                                    <asp:Button ID="btn_Submit" runat="server" CssClass="btn btn-primary pull-right" OnClick="btn_Submit_Click" Text="Submit" />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-xs-12">

                                            <div class="form-group">
                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>                                        
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </section></div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <div class="actions panel_actions pull-right">
                                        <asp:Label ID="lblCurrPage" runat="server" CssClass="" Font-Bold="True" Font-Size="Smaller"
                                            ForeColor="#3366cc" Height="8px" Width="83px">Current Page :</asp:Label>
                                        <asp:Label ID="lblPNo" runat="server" CssClass="" Font-Bold="True" Font-Size="Smaller"
                                            ForeColor="#3366cc" Height="10px" Width="9px">a</asp:Label>
                                        <asp:Label ID="lblGoto" runat="server" CssClass="" Font-Bold="True" Font-Size="Smaller"
                                            ForeColor="#3366cc" Height="7px" Width="16px">Goto</asp:Label>
                                        <asp:DropDownList ID="cmbPageNo" runat="server" AutoPostBack="True" CssClass="form-control m-bot15" Font-Bold="True" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:DataGrid ID="dg_bug" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnItemDataBound="dg_bug_ItemDataBound" PageSize="50" CssClass="table">
                                            
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Bug ID.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bugid" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container.DataItem,"eventid") %>'>
										        </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Date/Time">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_datetime" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container.DataItem,"Timestamp") %>'>
										        </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Message">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_message" runat="server" CssClass="form-label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem,"message"))  %>'>
										        </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Source">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_source" runat="server" CssClass="form-label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem, "source")) %>'>
										        </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Target Site">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_targetsite" runat="server" CssClass="form-label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem,"targetsite")) %>'>
										        </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Stack Trace">
                                                        <ItemTemplate>
										                    <DIV TITLE="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] header=[<table class='table' border='0'><tr><td><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table class='table' border='0'><tr><td><textarea id='txt_StateTraceMsg' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
										                    <asp:Label id=lbl_statetrace runat="server" CssClass="label" onclick="copyToClipboard(document.getElementById('txt_StateTraceMsg').value);" >Trace</asp:Label>
                                                            </DIV>
                                                            <asp:HiddenField ID="hf_statetrace" runat="server" Value='<%# Server.HtmlEncode((string)DataBinder.Eval(Container, "DataItem.stacktrace")) %>' />
									                    </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " />
                                            </asp:DataGrid>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                    </section>
                <!-- END CONTENT -->
            

            </div>
            <!-- END CONTAINER -->

        </form>


        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

        

        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>

        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
        <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        <script type="text/javascript">
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });
        </script>

</body>





























	<%--<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
            <table id="MainTable" cellSpacing="1" cellPadding="1" width="780" align="center" border="0">
                <tr>
                    <td>
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <table id="Table1" align="center" border="1" cellpadding="1" cellspacing="1" width="780">
                            <tr>
                                <td style="width: 786px">
                                    <strong>&nbsp;&nbsp;
                                        <table id="Table2" border="0" cellpadding="1" cellspacing="1" width="780">
                                            <tr>
                                                <td style="width: 56px">
                                                    <strong>From:</strong></td>
                                                <td style="width: 139px">
                                                    <ew:CalendarPopup ID="dtp_dtFrom" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                                        Width="96px">
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </td>
                                                <td style="width: 31px">
                                                    To:</td>
                                                <td style="width: 127px">
                                                    <ew:CalendarPopup ID="dtp_DtTo" runat="server" ControlDisplay="TextBoxImage" ImageUrl="~/Images/calendar.gif"
                                                        Width="96px">
                                                        <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Gray" />
                                                        <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                            Font-Size="XX-Small" ForeColor="Black" />
                                                        <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                        <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                            ForeColor="Black" />
                                                    </ew:CalendarPopup>
                                                </td>
                                                <td>
                                                    Database: &nbsp;&nbsp;<asp:DropDownList ID="ddl_Options" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                        <asp:ListItem>Houston Traffic System</asp:ListItem>
                                                        <asp:ListItem>Dallas Traffic System</asp:ListItem>
                                                        <asp:ListItem>Public Site</asp:ListItem>
                                                        <asp:ListItem>QuickBooks</asp:ListItem>
                                                        <asp:ListItem>LoaderSservice</asp:ListItem>
                                                        <asp:ListItem>E-Signature</asp:ListItem>
                                                        <asp:ListItem>Outlook Addin</asp:ListItem>
                                                         <asp:ListItem>SulloLaw</asp:ListItem>
                                                         <asp:ListItem>POLM</asp:ListItem>
                                                         <asp:ListItem>Product Defects</asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp; &nbsp;<asp:Button ID="btn_Submit" runat="server" CssClass="clsbutton" OnClick="btn_Submit_Click"
                                                        Text="Submit" /></td>
                                            </tr>
                                        </table>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 786px">
                                    <table width="100%">
                                        <tr>
                                            <td width="70%">
                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                                            <td align="right">
                                                <asp:Label ID="lblCurrPage" runat="server" CssClass="cmdlinks" Font-Bold="True" Font-Size="Smaller"
                                                    ForeColor="#3366cc" Height="8px" Width="83px">Current Page :</asp:Label>
                                                <asp:Label ID="lblPNo" runat="server" CssClass="cmdlinks" Font-Bold="True" Font-Size="Smaller"
                                                    ForeColor="#3366cc" Height="10px" Width="9px">a</asp:Label>
                                                <asp:Label ID="lblGoto" runat="server" CssClass="cmdlinks" Font-Bold="True" Font-Size="Smaller"
                                                    ForeColor="#3366cc" Height="7px" Width="16px">Goto</asp:Label><asp:DropDownList ID="cmbPageNo"
                                                        runat="server" AutoPostBack="True" CssClass="clinputcombo" Font-Bold="True" Font-Size="Smaller"
                                                        ForeColor="#3366cc" OnSelectedIndexChanged="cmbPageNo_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 786px">
                                    <asp:DataGrid ID="dg_bug" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        OnItemDataBound="dg_bug_ItemDataBound" PageSize="50" Width="780px">
                                        <AlternatingItemStyle BackColor="#EEEEEE" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Bug ID.">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_bugid" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"eventid") %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Date/Time">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_datetime" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Timestamp") %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Message">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_message" runat="server" CssClass="label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem,"message"))  %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Source">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_source" runat="server" CssClass="label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem, "source")) %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Target Site">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_targetsite" runat="server" CssClass="label" Text='<%# Server.HtmlEncode((string)DataBinder.Eval(Container.DataItem,"targetsite")) %>'>
										</asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Stack Trace">
                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                <ItemTemplate>
										<DIV TITLE="hideselects=[on] offsetx=[-410] offsety=[-200] singleclickstop=[on] requireclick=[off] header=[<table border='0' width='400px'><tr><td width='100%' align='right'><img src='../Images/close_button.png' border='0' onmouseover='CursorIcon()' onmouseout='CursorIcon2()' onclick='hideBox()'></td></tr></table>] body=[<table border='0' width='400px'><tr><td><textarea id='txt_StateTraceMsg' name='txt_StateTraceMsg' cols='46' rows='10'></textarea></td></tr></table>] ">
										<asp:Label id=lbl_statetrace runat="server" CssClass="label" onclick="copyToClipboard(document.getElementById('txt_StateTraceMsg').value);" >Trace</asp:Label>
                                        </DIV>
                                        <asp:HiddenField ID="hf_statetrace" runat="server" Value='<%# Server.HtmlEncode((string)DataBinder.Eval(Container, "DataItem.stacktrace")) %>' />
									</ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " />
                                    </asp:DataGrid></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td >
                        <uc2:Footer ID="Footer1" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
		</form>
	</body>--%>


	<script language="javascript" >
		function ShowMsg()
        {
            document.getElementById("txt_StateTraceMsg").value=err;
        }
		</script>
</HTML>

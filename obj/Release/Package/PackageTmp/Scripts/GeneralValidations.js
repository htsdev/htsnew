
   function checkValidation ( addressField ) {
   
    if ( stringEmpty ( addressField.value ) )
        alert ( "Please enter email address." );
    else if ( noAtSign ( addressField.value ) )
        alert ( "The E-Mail address does not contain an '@' character" );
    else if ( nothingBeforeAt ( addressField.value ) )
        alert ( "An E-Mail address must contain at least one character before the '@' character" );
    else if ( noLeftBracket ( addressField.value ) )
        alert ( "The E-Mail address contains a right square bracket ']',\nbut no corresponding left square bracket '['" );
    else if ( noRightBracket ( addressField.value ) )
        alert ( "The E-Mail address contains a left square bracket '[',\nbut no corresponding right square bracket ']'" );
    else if ( noValidPeriod ( addressField.value ) )
        alert ( "An E-Mail address must contain a period ('.') character" );
    else if ( noValidSuffix ( addressField.value ) )
        alert ( "An E-Mail address must contain a two or three character suffix" );
    else if(CheckGap( addressField.value))
        alert("There must be atleast one letter gap between '@' and '.'");
     else if(CheckSpace( addressField.value))
        alert("Space is not allowed between '@' and '.'");
    else
        return (true);

    return ( false );
}

function linkCheckValidation ( formField ) {
    if ( checkValidation ( formField ) == true ) {
        alert ( 'E-Mail Address Validates OK' );
    }

    return ( false );
}

function stringEmpty ( address ) {
    // CHECK THAT THE STRING IS NOT EMPTY
    
    if ( address.length < 1 ) {
   
        return ( true );
    } else {
       
        return ( false );
    }
}

function noAtSign ( address ) {
    // CHECK THAT THERE IS AN '@' CHARACTER IN THE STRING
    if ( address.indexOf ( '@', 0 ) == -1 ) {
        return ( true )
    } else {
        return ( false );
    }
}

function nothingBeforeAt ( address ) {
    // CHECK THERE IS AT LEAST ONE CHARACTER BEFORE THE '@' CHARACTER
    if ( address.indexOf ( '@', 0 ) < 1 ) {
        return ( true )
    } else {
        return ( false );
    }
}

function noLeftBracket ( address ) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN CHECK FOR LEFT BRACKET
    if ( address.indexOf ( '[', 0 ) == -1 && address.charAt ( address.length - 1 ) == ']' ) {
        return ( true )
    } else {
        return ( false );
    }
}

function noRightBracket ( address ) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN CHECK FOR RIGHT BRACKET
    if ( address.indexOf ( '[', 0 ) > -1 && address.charAt ( address.length - 1 ) != ']' ) {
        return ( true );
    } else {
        return ( false );
    }
}

function noValidPeriod ( address ) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN WE ARE NOT INTERESTED
    if ( address.indexOf ( '@', 0 ) > 1 && address.charAt ( address.length - 1 ) == ']' )
        return ( false );

    // CHECK THAT THERE IS AT LEAST ONE PERIOD IN THE STRING
    if ( address.indexOf ( '.', 0 ) == -1 )
        return ( true );

    return ( false );
}

function noValidSuffix ( address ) {
    // IF EMAIL ADDRESS IN FORM 'user@[255,255,255,0]', THEN WE ARE NOT INTERESTED
    if ( address.indexOf ( '@', 0 ) > 1 && address.charAt ( address.length - 1 ) == ']' )
        return ( false );

    // CHECK THAT THERE IS A TWO OR THREE CHARACTER SUFFIX AFTER THE LAST PERIOD
    var len = address.length;
    var pos = address.lastIndexOf ( '.', len - 1 ) + 1;
    if ( ( len - pos ) < 2 || ( len - pos ) > 3 ) {
        return ( true );
    } else {
        return ( false );
    }
}


 function openCenteredWindow(url,width,height) 
    {
        var left = parseInt((screen.availWidth/2) - (width/2));
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + 
            ",status,left=" + left + ",top=" + top + 
            "screenX=" + left + ",screenY=" + top;
        window.open(url, "", windowFeatures);
    }
    
    
    function CheckGap(address)
    {
       
        var first=address.indexOf("@");
        var last=address.lastIndexOf(".");
        
        if(last-first<2)
        {
            return true;
        }
        return false;
               
    }
     
     function CheckSpace(address)
     {
       
        var first=address.indexOf("@");
        var last=address.lastIndexOf(".");
        var space = address.indexOf(" ");
       
       if ( space > first && space < last)
       return true; 

       return false;
     }  
    
function LTrim(value) 
{
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
}

// Removes leading and ending whitespaces
function trim( value ) {

	return LTrim(RTrim(value));
	
}

    
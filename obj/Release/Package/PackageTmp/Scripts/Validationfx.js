
var digits = "0123456789";

var lowercaseLetters = "abcdefghijklmnopqrstuvwxyz"

var uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"



var whitespace = " \t\n\r";



var decimalPointDelimiter = "."



var phoneNumberDelimiters = "()- ";



var validUSPhoneChars = digits + phoneNumberDelimiters;




var validWorldPhoneChars = digits + phoneNumberDelimiters + "+";




var SSNDelimiters = "- ";




var validSSNChars = digits + SSNDelimiters;





var digitsInSocialSecurityNumber = 9;





var digitsInUSPhoneNumber = 10;




var ZIPCodeDelimiters = "-";




var ZIPCodeDelimeter = "-"



var validZIPCodeChars = digits + ZIPCodeDelimiters





var digitsInZIPCode1 = 5
var digitsInZIPCode2 = 9



var creditCardDelimiters = " "







var mPrefix = "You did not enter a value into the "
var mSuffix = " field. This is a required field. Please enter it now."



var sUSLastName = "Last Name"
var sUSFirstName = "First Name"
var sWorldLastName = "Family Name"
var sWorldFirstName = "Given Name"
var sTitle = "Title"
var sCompanyName = "Company Name"
var sUSAddress = "Street Address"
var sWorldAddress = "Address"
var sCity = "City"
var sStateCode = "State Code"
var sWorldState = "State, Province, or Prefecture"
var sCountry = "Country"
var sZIPCode = "ZIP Code"
var sWorldPostalCode = "Postal Code"
var sPhone = "Phone Number"
var sFax = "Fax Number"
var sDateOfBirth = "Date of Birth"
var sExpirationDate = "Expiration Date"
var sEmail = "Email"
var sSSN = "Social Security Number"
var sCreditCardNumber = "Credit Card Number"
var sOtherInfo = "Other Information"






var iStateCode = "This field must be a valid two character U.S. state abbreviation (like CA for California). Please reenter it now."
var iZIPCode = "This field must be a 5 or 9 digit U.S. ZIP Code (like 94043). Please reenter it now."
var iUSPhone = "This field must be a 10 digit U.S. phone number (like 415 555 1212). Please reenter it now."
var iWorldPhone = "This field must be a valid international phone number. Please reenter it now."
var iSSN = "This field must be a 9 digit U.S. social security number (like 123 45 6789). Please reenter it now."
var iEmail = "This field must be a valid email address (like foo@bar.com). Please reenter it now."
var iCreditCardPrefix = "This is not a valid "
var iCreditCardSuffix = " credit card number. (Click the link on this form to see a list of sample numbers.) Please reenter it now."
var iDay = "This field must be a day number between 1 and 31.  Please reenter it now."
var iMonth = "This field must be a month number between 1 and 12.  Please reenter it now."
var iYear = "This field must be a 2 or 4 digit year number.  Please reenter it now."
var iDatePrefix = "The Day, Month, and Year for "
var iDateSuffix = " do not form a valid date.  Please reenter them now."





var pEntryPrompt = "Please enter a "
var pStateCode = "2 character code (like CA)."
var pZIPCode = "5 or 9 digit U.S. ZIP Code (like 94043)."
var pUSPhone = "10 digit U.S. phone number (like 415 555 1212)."
var pWorldPhone = "international phone number."
var pSSN = "9 digit U.S. social security number (like 123 45 6789)."
var pEmail = "valid email address (like foo@bar.com)."
var pCreditCard = "valid credit card number."
var pDay = "day number between 1 and 31."
var pMonth = "month number between 1 and 12."
var pYear = "2 or 4 digit year number."



























var defaultEmptyOK = false










function makeArray(n) {
   for (var i = 1; i <= n; i++) {
      this[i] = 0
   } 
   return this
}

var daysInMonth = makeArray(12);
daysInMonth[1] = 31;
daysInMonth[2] = 29;   
daysInMonth[3] = 31;
daysInMonth[4] = 30;
daysInMonth[5] = 31;
daysInMonth[6] = 30;
daysInMonth[7] = 31;
daysInMonth[8] = 31;
daysInMonth[9] = 30;
daysInMonth[10] = 31;
daysInMonth[11] = 30;
daysInMonth[12] = 31;







var USStateCodeDelimiter = "|";
var USStateCodes = "AL|AK|AS|AZ|AR|CA|CO|CT|DE|DC|FM|FL|GA|GU|HI|ID|IL|IN|IA|KS|KY|LA|ME|MH|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|MP|OH|OK|OR|PW|PA|PR|RI|SC|SD|TN|TX|UT|VT|VI|VA|WA|WV|WI|WY|AE|AA|AE|AE|AP"






function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}




function isFirstDigitNumeric(s)

{   var i;
    
    if (isEmpty(s)) return true;    

	var c = s.charAt(0);
    if (isInteger(c)==false)
    {
		return false;    
    }            
    return true;
}



function isWhitespace (s)

{   var i;

    
    if (isEmpty(s)) return true;

    
    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    
    return true;
}





function stripCharsInBag (s, bag)

{   var i;
    var returnString = "";

    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }

    return returnString;
}






function stripCharsNotInBag (s, bag)

{   var i;
    var returnString = "";

    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);
        if (bag.indexOf(c) != -1) returnString += c;
    }

    return returnString;
}







function stripWhitespace (s)

{   return stripCharsInBag (s, whitespace)
}
























function charInString (c, s)
{   for (i = 0; i < s.length; i++)
    {   if (s.charAt(i) == c) return true;
    }
    return false
}







function stripInitialWhitespace (s)

{   var i = 0;

    while ((i < s.length) && charInString (s.charAt(i), whitespace))
       i++;
    
    return s.substring (i, s.length);
}














function isLetter (c)
{   return ( ((c >= "a") && (c <= "z")) || ((c >= "A") && (c <= "Z")) )
}






function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}





function isLetterOrDigit (c)
{   return (isLetter(c) || isDigit(c))
}






























function isInteger (s)

{   var i;

    if (isEmpty(s)) 
       if (isInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isInteger.arguments[1] == true);

    
    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    
    return true;
}




























function isSignedInteger (s)

{   if (isEmpty(s)) 
       if (isSignedInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isSignedInteger.arguments[1] == true);

    else {
        var startPos = 0;
        var secondArg = defaultEmptyOK;

        if (isSignedInteger.arguments.length > 1)
            secondArg = isSignedInteger.arguments[1];

        
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;    
        return (isInteger(s.substring(startPos, s.length), secondArg))
    }
}











function isPositiveInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];

    
    
    
    
    
    
   
    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s,10) > 0) ) ); //ozair 3901 on 04/30/2008 included radix "10"
}













function isNonnegativeInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isNonnegativeInteger.arguments.length > 1)
        secondArg = isNonnegativeInteger.arguments[1];
    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s,10) >= 0) ) ); //ozair 3901 on 04/30/2008 included radix "10"
}













function isNegativeInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isNegativeInteger.arguments.length > 1)
        secondArg = isNegativeInteger.arguments[1];

    
    
    
    
    
    

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s,10) < 0) ) ); //ozair 3901 on 04/30/2008 included radix "10"
}













function isNonpositiveInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isNonpositiveInteger.arguments.length > 1)
        secondArg = isNonpositiveInteger.arguments[1];

    
    
    
    
    
    

    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s,10) <= 0) ) ); //ozair 3901 on 04/30/2008 included radix "10"
}


















function isFloat (s)

{   var i;
    var seenDecimalPoint = false;

    if (isEmpty(s)) 
       if (isFloat.arguments.length == 1) return defaultEmptyOK;
       else return (isFloat.arguments[1] == true);

    if (s == decimalPointDelimiter) return false;

    
    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);

        if ((c == decimalPointDelimiter) && !seenDecimalPoint) seenDecimalPoint = true;
        else if (!isDigit(c)) return false;
    }

    
    return true;
}





















function isSignedFloat (s)

{   if (isEmpty(s)) 
       if (isSignedFloat.arguments.length == 1) return defaultEmptyOK;
       else return (isSignedFloat.arguments[1] == true);

    else {
        var startPos = 0;
        var secondArg = defaultEmptyOK;

        if (isSignedFloat.arguments.length > 1)
            secondArg = isSignedFloat.arguments[1];

        
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;    
        return (isFloat(s.substring(startPos, s.length), secondArg))
    }
}
















function isAlphabetic (s)

{   var i;

    if (isEmpty(s)) 
       if (isAlphabetic.arguments.length == 1) return defaultEmptyOK;
       else return (isAlphabetic.arguments[1] == true);

    
    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);

        if (!isLetter(c))
        return false;
    }

    
    return true;
}
















function isAlphanumeric (s)

{   var i;

    if (isEmpty(s)) 
       if (isAlphanumeric.arguments.length == 1) return defaultEmptyOK;
       else return (isAlphanumeric.arguments[1] == true);

    
    
    

    for (i = 0; i < s.length; i++)
    {   
        
        var c = s.charAt(i);

        if (! (isLetter(c) || isDigit(c) ) )
        return false;
    }

    
    return true;
}





















































function reformat (s)

{   var arg;
    var sPos = 0;
    var resultString = "";

    for (var i = 1; i < reformat.arguments.length; i++) {
       arg = reformat.arguments[i];
       if (i % 2 == 1) resultString += arg;
       else {
           resultString += s.substring(sPos, sPos + arg);
           sPos += arg;
       }
    }
    return resultString;
}















function isSSN (s)
{   if (isEmpty(s)) 
       if (isSSN.arguments.length == 1) return defaultEmptyOK;
       else return (isSSN.arguments[1] == true);
    return (isInteger(s) && s.length == digitsInSocialSecurityNumber)
}















function isUSPhoneNumber (s)
{   if (isEmpty(s)) 
       if (isUSPhoneNumber.arguments.length == 1) return defaultEmptyOK;
       else return (isUSPhoneNumber.arguments[1] == true);
    return (isInteger(s) && s.length == digitsInUSPhoneNumber)
}




















function isInternationalPhoneNumber (s)
{   if (isEmpty(s)) 
       if (isInternationalPhoneNumber.arguments.length == 1) return defaultEmptyOK;
       else return (isInternationalPhoneNumber.arguments[1] == true);
    return (isPositiveInteger(s))
}















function isZIPCode (s)
{  if (isEmpty(s)) 
       if (isZIPCode.arguments.length == 1) return defaultEmptyOK;
       else return (isZIPCode.arguments[1] == true);
   return (isInteger(s) && 
            ((s.length == digitsInZIPCode1) ||
             (s.length == digitsInZIPCode2)))
}













function isStateCode(s)
{   if (isEmpty(s)) 
       if (isStateCode.arguments.length == 1) return defaultEmptyOK;
       else return (isStateCode.arguments[1] == true);
    return ( (USStateCodes.indexOf(s) != -1) &&
             (s.indexOf(USStateCodeDelimiter) == -1) )
}














function isEmail (s)
{   if (isEmpty(s)) 
       if (isEmail.arguments.length == 1) return defaultEmptyOK;
       else return (isEmail.arguments[1] == true);
   
    
    if (isWhitespace(s)) return false;
    
    
    
    
    var i = 1;
    var sLength = s.length;

    
    while ((i < sLength) && (s.charAt(i) != "@"))
    { i++
    }

    if ((i >= sLength) || (s.charAt(i) != "@")) return false;
    else i += 2;

    
    while ((i < sLength) && (s.charAt(i) != "."))
    { i++
    }

    
    if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
    else return true;
}






















function isYear (s)
{   if (isEmpty(s)) 
       if (isYear.arguments.length == 1) return defaultEmptyOK;
       else return (isYear.arguments[1] == true);
    if (!isNonnegativeInteger(s)) return false;
    return ((s.length == 2) || (s.length == 4));
}












function isIntegerInRange (s, a, b)
{   if (isEmpty(s)) 
       if (isIntegerInRange.arguments.length == 1) return defaultEmptyOK;
       else return (isIntegerInRange.arguments[1] == true);

    
    
    if (!isInteger(s, false)) return false;

    
    
    
    //ozair 3901 on 04/30/2008 included radix "10"
    var num = parseInt (s,10);
    return ((num >= a) && (num <= b));
}











function isMonth (s)
{   if (isEmpty(s)) 
       if (isMonth.arguments.length == 1) return defaultEmptyOK;
       else return (isMonth.arguments[1] == true);
    return isIntegerInRange (s, 1, 12);
}











function isDay (s)
{   if (isEmpty(s)) 
       if (isDay.arguments.length == 1) return defaultEmptyOK;
       else return (isDay.arguments[1] == true);   
    return isIntegerInRange (s, 1, 31);
}








function daysInFebruary (year)
{   
    
    return (  ((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0) ) ) ? 29 : 28 );
}









function isDate (year, month, day)
{   
    if (! (isYear(year) && isMonth(month) && isDay(day))) return false;

    
    //ozair 3901 on 04/30/2008 included radix "10"
    var intYear = parseInt(year,10);
    var intMonth = parseInt(month,10);
    var intDay = parseInt(day,10);

    
    if (intDay > daysInMonth[intMonth]) return false; 

    if ((intMonth == 2) && (intDay > daysInFebruary(intYear))) return false;

    return true;
}

//Modified by Kazim Tasd Id:2536
//Remove the function because this validation is now take place in Dates.js 

/*function isValidDate (year, month, day)
{   
    month = month.replace(/0/,"");
    day = day.replace(/0/,"");
    
    if (! (isYear(year) && isMonth(month) && isDay(day))) return false;

    
    
    var intYear = parseInt(year);
    var intMonth = parseInt(month);
    var intDay = parseInt(day);
    var d = new Date();
    //Modified by kazim Task id:2536
   //Add '=' sign because 
    if (intYear < 1900 || intYear >= d.getFullYear() )
        return false;
    
    if (intDay > daysInMonth[intMonth]) return false; 

    if ((intMonth == 2) && (intDay > daysInFebruary(intYear))) return false;

    return true;
}*/







function prompt (s)
{   window.status = s
}





function promptEntry (s)
{   window.status = pEntryPrompt + s
}








function warnEmpty (theField, s)
{   theField.focus()
    alert(mPrefix + s + mSuffix)
    return false
}







function warnInvalid (theField, s)
{   theField.focus()
    theField.select()
    alert(s)
    return false
}













function checkString (theField, s, emptyOK)
{   
    
    if (checkString.arguments.length == 2) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    if (isWhitespace(theField.value)) 
       return warnEmpty (theField, s);
    else return true;
}










function checkStateCode (theField, emptyOK)
{   if (checkStateCode.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
    {  theField.value = theField.value.toUpperCase();
       if (!isStateCode(theField.value, false)) 
          return warnInvalid (theField, iStateCode);
       else return true;
    }
}






function reformatZIPCode (ZIPString)
{   if (ZIPString.length == 5) return ZIPString;
    else return (reformat (ZIPString, "", 5, "-", 4));
}











function checkZIPCode (theField, emptyOK)
{   if (checkZIPCode.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
    { var normalizedZIP = stripCharsInBag(theField.value, ZIPCodeDelimiters)
      if (!isZIPCode(normalizedZIP, false)) 
         return warnInvalid (theField, iZIPCode);
      else 
      {  
         theField.value = reformatZIPCode(normalizedZIP)
         return true;
      }
    }
}






function reformatUSPhone (USPhone)
{   return (reformat (USPhone, "(", 3, ") ", 3, "-", 4))
}










function checkUSPhone (theField, emptyOK)
{   if (checkUSPhone.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
    {  var normalizedPhone = stripCharsInBag(theField.value, phoneNumberDelimiters)
       if (!isUSPhoneNumber(normalizedPhone, false)) 
          return warnInvalid (theField, iUSPhone);
       else 
       {  
          theField.value = reformatUSPhone(normalizedPhone)
          return true;
       }
    }
}










function checkInternationalPhone (theField, emptyOK)
{   if (checkInternationalPhone.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
    {  if (!isInternationalPhoneNumber(theField.value, false)) 
          return warnInvalid (theField, iWorldPhone);
       else return true;
    }
}










function checkEmail (theField, emptyOK)
{   if (checkEmail.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else if (!isEmail(theField.value, false)) 
       return warnInvalid (theField, iEmail);
    else return true;
}






function reformatSSN (SSN)
{   return (reformat (SSN, "", 3, "-", 2, "-", 4))
}







function checkSSN (theField, emptyOK)
{   if (checkSSN.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
    {  var normalizedSSN = stripCharsInBag(theField.value, SSNDelimiters)
       if (!isSSN(normalizedSSN, false)) 
          return warnInvalid (theField, iSSN);
       else 
       {  
          theField.value = reformatSSN(normalizedSSN)
          return true;
       }
    }
}









function checkYear (theField, emptyOK)
{   if (checkYear.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    if (!isYear(theField.value, false)) 
       return warnInvalid (theField, iYear);
    else return true;
}







function checkMonth (theField, emptyOK)
{   if (checkMonth.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    if (!isMonth(theField.value, false)) 
       return warnInvalid (theField, iMonth);
    else return true;
}







function checkDay (theField, emptyOK)
{   if (checkDay.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    if (!isDay(theField.value, false)) 
       return warnInvalid (theField, iDay);
    else return true;
}














function checkDate (yearField, monthField, dayField, labelString, OKtoOmitDay)
{   
    
    if (checkDate.arguments.length == 4) OKtoOmitDay = false;
    if (!isYear(yearField.value)) return warnInvalid (yearField, iYear);
    if (!isMonth(monthField.value)) return warnInvalid (monthField, iMonth);
    if ( (OKtoOmitDay == true) && isEmpty(dayField.value) ) return true;
    else if (!isDay(dayField.value)) 
       return warnInvalid (dayField, iDay);
    if (isDate (yearField.value, monthField.value, dayField.value))
       return true;
    alert (iDatePrefix + labelString + iDateSuffix)
    return false
}





function getRadioButtonValue (radio)
{   for (var i = 0; i < radio.length; i++)
    {   if (radio[i].checked) { break }
    }
    return radio[i].value
}






function checkCreditCard (radio, theField)
{   var cardType = getRadioButtonValue (radio)
    var normalizedCCN = stripCharsInBag(theField.value, creditCardDelimiters)
    if (!isCardMatch(cardType, normalizedCCN)) 
       return warnInvalid (theField, iCreditCardPrefix + cardType + iCreditCardSuffix);
    else 
    {  theField.value = normalizedCCN
       return true
    }
}








function isCreditCard(st) {
  
  if (st.length > 19)
    return (false);

  sum = 0; mul = 1; l = st.length;
  for (i = 0; i < l; i++) {
    digit = st.substring(l-i-1,l-i);
    tproduct = parseInt(digit ,10)*mul;
    if (tproduct >= 10)
      sum += (tproduct % 10) + 1;
    else
      sum += tproduct;
    if (mul == 1)
      mul++;
    else
      mul--;
  }









  if ((sum % 10) == 0)
    return (true);
  else
    return (false);

} 





function isVisa(cc)
{
  if (((cc.length == 16) || (cc.length == 13)) &&
      (cc.substring(0,1) == 4))
    return isCreditCard(cc);
  return false;
}  






function isMasterCard(cc)
{
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 16) && (firstdig == 5) &&
      ((seconddig >= 1) && (seconddig <= 5)))
    return isCreditCard(cc);
  return false;

} 







function isAmericanExpress(cc)
{
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 15) && (firstdig == 3) &&
      ((seconddig == 4) || (seconddig == 7)))
    return isCreditCard(cc);
  return false;

} 






function isDinersClub(cc)
{
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 14) && (firstdig == 3) &&
      ((seconddig == 0) || (seconddig == 6) || (seconddig == 8)))
    return isCreditCard(cc);
  return false;
}





function isCarteBlanche(cc)
{
  return isDinersClub(cc);
}






function isDiscover(cc)
{
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) && (first4digs == "6011"))
    return isCreditCard(cc);
  return false;

} 







function isEnRoute(cc)
{
  first4digs = cc.substring(0,4);
  if ((cc.length == 15) &&
      ((first4digs == "2014") ||
       (first4digs == "2149")))
    return isCreditCard(cc);
  return false;
}





function isJCB(cc)
{
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) &&
      ((first4digs == "3088") ||
       (first4digs == "3096") ||
       (first4digs == "3112") ||
       (first4digs == "3158") ||
       (first4digs == "3337") ||
       (first4digs == "3528")))
    return isCreditCard(cc);
  return false;

} 





function isAnyCard(cc)
{
  if (!isCreditCard(cc))
    return false;
  if (!isMasterCard(cc) && !isVisa(cc) && !isAmericanExpress(cc) && !isDinersClub(cc) &&
      !isDiscover(cc) && !isEnRoute(cc) && !isJCB(cc)) {
    return false;
  }
  return true;

} 


function isCardMatch (cardType, cardNumber)
{

	cardType = cardType.toUpperCase();
	var doesMatch = true;

	if ((cardType == "VISA") && (!isVisa(cardNumber)))
		doesMatch = false;
	if ((cardType == "MASTERCARD") && (!isMasterCard(cardNumber)))
		doesMatch = false;
	if ( ( (cardType == "AMERICANEXPRESS") || (cardType == "AMEX") )
                && (!isAmericanExpress(cardNumber))) doesMatch = false;
	if ((cardType == "DISCOVER") && (!isDiscover(cardNumber)))
		doesMatch = false;
	if ((cardType == "JCB") && (!isJCB(cardNumber)))
		doesMatch = false;
	if ((cardType == "DINERS") && (!isDinersClub(cardNumber)))
		doesMatch = false;
	if ((cardType == "CARTEBLANCHE") && (!isCarteBlanche(cardNumber)))
		doesMatch = false;
	if ((cardType == "ENROUTE") && (!isEnRoute(cardNumber)))
		doesMatch = false;
	return doesMatch;

}  






function IsCC (st) {
    return isCreditCard(st);
}

function IsVisa (cc)  {
  return isVisa(cc);
}

function IsVISA (cc)  {
  return isVisa(cc);
}

function IsMasterCard (cc)  {
  return isMasterCard(cc);
}

function IsMastercard (cc)  {
  return isMasterCard(cc);
}

function IsMC (cc)  {
  return isMasterCard(cc);
}

function IsAmericanExpress (cc)  {
  return isAmericanExpress(cc);
}

function IsAmEx (cc)  {
  return isAmericanExpress(cc);
}

function IsDinersClub (cc)  {
  return isDinersClub(cc);
}

function IsDC (cc)  {
  return isDinersClub(cc);
}

function IsDiners (cc)  {
  return isDinersClub(cc);
}

function IsCarteBlanche (cc)  {
  return isCarteBlanche(cc);
}

function IsCB (cc)  {
  return isCarteBlanche(cc);
}

function IsDiscover (cc)  {
  return isDiscover(cc);
}

function IsEnRoute (cc)  {
  return isEnRoute(cc);
}

function IsenRoute (cc)  {
  return isEnRoute(cc);
}

function IsJCB (cc)  {
  return isJCB(cc);
}

function IsAnyCard(cc)  {
  return isAnyCard(cc);
}

function IsCardMatch (cardType, cardNumber)  {
  return isCardMatch (cardType, cardNumber);
}


function formatDecimal(argvalue, addzero, decimaln) 
{
  
  vflag= isFloat(argvalue);
  if ( ((argvalue != "") && (vflag)) || (argvalue == "0")) 
                                          
  {
		//alert("True");
		var numOfDecimal = (decimaln == null) ? 2 : decimaln;
		var number = 1;

		//alert("Float");
		number = Math.pow(10, numOfDecimal);

		argvalue = Math.round(parseFloat(argvalue) * number) / number;
		// If you're using IE3.x, you will get error with the following line.
		// argvalue = argvalue.toString();
		// It works fine in IE4.
		argvalue = "" + argvalue;

		if (argvalue.indexOf(".") == 0)
			argvalue = "0" + argvalue;

		if (addzero == true)
		{
			if (argvalue.indexOf(".") == -1)
			argvalue = argvalue + ".";

			while ((argvalue.indexOf(".") + 1) > (argvalue.length - numOfDecimal))
			argvalue = argvalue + "0";
		}

		return argvalue;
		  
  }
  
  else
  {//alert("Not");
		return argvalue;
  }
}


function lengthcheck(temp,size)
  {
    if (temp.value.length > size)
       {
	  //temp.value = temp.value.substring(0,size );
	  alert(" Length must not be more than " + size + " characters");
	  temp.focus();
	  return(false);
       }
    return(true);   
   }	
   
   function minlengthcheck(temp,size)
  {
    if (temp.value.length < size)
       {
	  //temp.value = temp.value.substring(0,size );
	  alert(" Length must not be less than " + size + " characters");
	  temp.focus();
	  return(false);
       }
    return(true);   
   }	
// Auto Tab added by zee

	var isNN = (navigator.appName.indexOf("Netscape")!=-1);
		function autoTab(input,len, e) 
		{
			var keyCode = (isNN) ? e.which : e.keyCode; 
			var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
			if(input.value.length >= len && !containsElement(filter,keyCode)) 
			{
				input.value = input.value.slice(0, len);
				input.form[(getIndex(input)+1) % input.form.length].focus();
			}
			function containsElement(arr, ele) 
			{
				var found = false, index = 0;
				while(!found && index < arr.length)
				if(arr[index] == ele)
				found = true;
				else
				index++;
				return found;
			}
			function getIndex(input) 
			{
				var index = -1, i = 0, found = false;
				while (i < input.form.length && index == -1)
				if (input.form[i] == input)index = i;
				else i++;
				return index;
			}
			return true;
		}

///////////////////////////////Added By Ozair/////////////////////////
		
		function chkdate(objName) 
		{
			var strDatestyle = "US"; //United States date style
			//var strDatestyle = "EU";  //European date style
			var strDate;
			var strDateArray;
			var strDay;
			var strMonth;
			var strYear;
			var intday;
			var intMonth;
			var intYear;
			var booFound = false;
			var datefield = objName;
			var strSeparatorArray = new Array("-"," ","/",".");
						
			var str = new Array(5);
			var iyr;

			var intElementNr;
			var err = 0;
			var strMonthArray = new Array(12);
			strMonthArray[0] = "01";
			strMonthArray[1] = "02";
			strMonthArray[2] = "03";
			strMonthArray[3] = "04";
			strMonthArray[4] = "05";
			strMonthArray[5] = "06";
			strMonthArray[6] = "07";
			strMonthArray[7] = "08";
			strMonthArray[8] = "09";
			strMonthArray[9] = "10";
			strMonthArray[10] = "11";
			strMonthArray[11] = "12";

			strDate = datefield;

			if (strDate.length < 1) 
			{
				return true;
			}

			for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) 
			{
				if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) 
				{
					strDateArray = strDate.split(strSeparatorArray[intElementNr]);
					if (strDateArray.length != 3) 
					{
						err = 1;
						return false;
					}
					else 
					{
						strDay = strDateArray[0];
						strMonth = strDateArray[1];
						strYear = strDateArray[2];
					}
					booFound = true;
				}
			}
			if (booFound == false) 
			{
				if (strDate.length>5) 
				{
					strDay = strDate.substr(0, 2);
					strMonth = strDate.substr(2, 2);
					var stryear;
					strYear = 0;
					strYear = strDate.substr(4);
				}
				
			}
            
            if(strYear != null)// if string was given it won't give error
            {
			    if (strYear.length == 2) 
			    {
				    if (strYear <= 50 ) 
				    {
					    strYear = '20' + strYear;
				    }
			    }
            }
			// US style
			if (strDatestyle == "US") 
			{
				strTemp = strDay;
				strDay = strMonth;
				strMonth = strTemp;
			}
			
			 //Kamran 3585 04/09/08 remove Month and day bug		
			 
			var flagM = false;			
			var flagD = false;
			var D;
			var M;
			
			if(isMonth(strMonth))
			{
			    for(iM=0; iM<strMonth.length; iM++)
			    {
			    
			        M = parseInt(strMonth.charAt(iM), 10);
			        if(isNaN(M))
			        {
			          flagM=true;
			          break;			    
			        }    			    
			    }
			}
			else
			  flagM=true;
			  
			if(flagM==true)
			{
			    err = 6;
				return false;
			}
				
				
			
			if(isDay(strDay))
			{
			    for(iD=0; iD<strDay.length; iD++)
			    {
			    
			        D = parseInt(strDay.charAt(iD), 10);
			        if(isNaN(D))
			        {
			          flagD=true;
			          break;			    
			        }    			    
			    }
			}
			else
			  flagD=true;
			  
			if(flagD==true)
			{
			    err = 3;
				return false;
			}
			
			intday = parseInt(strDay, 10);
			if (isNaN(intday)) 
			{
				err = 2;
				return false;
			}
			intMonth = parseInt(strMonth, 10);

			if (isNaN(intMonth)) 
			{
				for (k = 0;k<12;k++) 
				{
					if (strMonth.toUpperCase() == strMonthArray[k].toUpperCase()) 
					{
						intMonth = k+1;
						strMonth = strMonthArray[k];
						k = 12;
					}
				}
				if (isNaN(intMonth)) 
				{
					err = 3;
					return false;
				}
			}
			
			
			//Kamran 3585 03/31/08 remove year bug
			var flag = false;
			var yr;
			
			if(isYear (strYear))
			{
			    for(iyr=0; iyr<strYear.length; iyr++)
			    {
			    
			        yr = parseInt(strYear.charAt(iyr), 10);
			        if(isNaN(yr))
			        {
			          flag=true;
			          break;			    
			        }    			    
			    }
			}
			else
			  flag=true;
		
			if(flag==true)
			{
			    err = 4;
				return false;
			}
			
			// Sabir Khan 9717 10/04/2011 Uncommented code to get year in variable "intYear".
			intYear = parseInt(strYear, 10);
			/*
			if (isNaN(intYear)) 
			{
				err = 4;
				return false;
			}
			*/
			if (intMonth>12 || intMonth<1) 
			{
				err = 5;
				return false;
			}

			if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) 
			{
				err = 6;
				return false;
			}
			if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) 
			{
				err = 7;
				return false;
			}
			if (intMonth == 2) 
			{
				if (intday < 1) 
				{
					err = 8;
					return false;
				}
				if (LeapYear(intYear) == true) 
				{
					if (intday > 29) 
					{
						err = 9;
						return false;
					}
				}
				else 
				{
					if (intday > 28) 
					{
						err = 10;
						return false;
					}
				}
			}

			if (strDatestyle == "US") 
			{
				datefield = strMonthArray[intMonth-1] + "/" + intday+ "/" + strYear;
			}
			else 
			{
				datefield = intday + " " + strMonthArray[intMonth-1] + " " + strYear;
			}

			return true;
		}

		function LeapYear(intYear) 
		{
			if (intYear % 100 == 0) 
			{
				if (intYear % 400 == 0) 
				{
					return true; 
				}
			}
			else 
			{
				if ((intYear % 4) == 0) 
				{ 
					return true; 
				}
			}
			return false;

			//alert(datefield);
		}
		/////////////////////////////////////////////////////////////
		// Email Validation Javascript


function validateEmail(addr,man,db) 
{
	if (addr == '' && man) 
	{
	if (db) alert('email address is mandatory');
	return false;
	}
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';

	for (i=0; i<invalidChars.length; i++) {
	if (addr.indexOf(invalidChars.charAt(i),0) > -1) 
	{
		if (db) alert('email address contains invalid characters');
		return false;
	}
	}
	for (i=0; i<addr.length; i++) 
	{
	if (addr.charCodeAt(i)>127) 
	{
		if (db) alert("email address contains non ascii characters.");
		return false;
	}
	}

	var atPos = addr.indexOf('@',0);
	if (atPos == -1) 
	{
	if (db) alert('email address must contain an @');
	return false;
	}
	if (atPos == 0) 
	{
	if (db) alert('email address must not start with @');
	return false;
	}
	if (addr.indexOf('@', atPos + 1) > - 1) 
	{
	if (db) alert('email address must contain only one @');
	return false;
	}
	if (addr.indexOf('.', atPos) == -1) 
	{
	if (db) alert('email address must contain a period in the domain name');
	return false;
	}
	if (addr.indexOf('@.',0) != -1) 
	{
	if (db) alert('period must not immediately follow @ in email address');
	return false;
	}
	if (addr.indexOf('.@',0) != -1)
	{
	if (db) alert('period must not immediately precede @ in email address');
	return false;
	}
	if (addr.indexOf('..',0) != -1) 
	{
	if (db) alert('two periods must not be adjacent in email address');
	return false;
	}
	var suffix = addr.substring(addr.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') 
	{
	if (db) alert('invalid primary domain in email address');
	return false;
	}
	return true;
}

//Zeeshan Ahmed 3825 04/23/2008

function LTrim(value) 
{
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
}

// Removes leading and ending whitespaces
function trim( value ) {

	return LTrim(RTrim(value));
	
}

//Zeeshan 3709 06/12/2008 Function used to validate DOB
function chkdob(objName) 
		{
			var strDatestyle = "US"; //United States date style
			//var strDatestyle = "EU";  //European date style
			var strDate;
			var strDateArray;
			var strDay;
			var strMonth;
			var strYear;
			var intday;
			var intMonth;
			var intYear;
			var booFound = false;
			var datefield = objName;
			var strSeparatorArray = new Array("-"," ","/",".");
						
			var str = new Array(5);
			var iyr;

			var intElementNr;
			var err = 0;
			var strMonthArray = new Array(12);
			strMonthArray[0] = "01";
			strMonthArray[1] = "02";
			strMonthArray[2] = "03";
			strMonthArray[3] = "04";
			strMonthArray[4] = "05";
			strMonthArray[5] = "06";
			strMonthArray[6] = "07";
			strMonthArray[7] = "08";
			strMonthArray[8] = "09";
			strMonthArray[9] = "10";
			strMonthArray[10] = "11";
			strMonthArray[11] = "12";

			strDate = datefield;
            //Sabir 4326 07/08/2008 Check Separator in input date if exist than validate
            //Start 
            var isContainSeparator = false;
                      
            for (i =0 ; i < strSeparatorArray.length ; i++ )
            {
                
                if( strDate.indexOf(strSeparatorArray[i]) != -1)
                {   
                    isContainSeparator = true;
                    break;
                }
            }
                                                      
           if ( !isContainSeparator)
           return false;
            //End
			if (strDate.length < 1) 
			{
				return true;
			}

			for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) 
			{
				if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) 
				{
					strDateArray = strDate.split(strSeparatorArray[intElementNr]);
					if (strDateArray.length != 3) 
					{
						err = 1;
						return false;
					}
					else 
					{
						strDay = strDateArray[0];
						strMonth = strDateArray[1];
						strYear = strDateArray[2];
					}
					booFound = true;
				}
			}
			if (booFound == false) 
			{
				if (strDate.length>5) 
				{
					strDay = strDate.substr(0, 2);
					strMonth = strDate.substr(2, 2);
					var stryear;
					strYear = 0;
					strYear = strDate.substr(4);
				}
				
			}
            
            if(strYear != null)// if string was given it won't give error
            {
			    var currDate = new Date();
			    
			    if (strYear.length == 2) 
			    {
				    
				    var currYear = currDate.getFullYear() + "";
		
				    if (currYear.substring(2) < strYear ) 
				        strYear = '19' + strYear;
				    else 
				        strYear = '20' + strYear;
			    }
            }
			// US style
			if (strDatestyle == "US") 
			{
				strTemp = strDay;
				strDay = strMonth;
				strMonth = strTemp;
			}
			
			 //Kamran 3585 04/09/08 remove Month and day bug		
			 
			var flagM = false;			
			var flagD = false;
			var D;
			var M;
			
			if(isMonth(strMonth))
			{
			    for(iM=0; iM<strMonth.length; iM++)
			    {
			    
			        M = parseInt(strMonth.charAt(iM), 10);
			        if(isNaN(M))
			        {
			          flagM=true;
			          break;			    
			        }    			    
			    }
			}
			else
			  flagM=true;
			  
			if(flagM==true)
			{
			    err = 6;
				return false;
			}
				
				
			
			if(isDay(strDay))
			{
			    for(iD=0; iD<strDay.length; iD++)
			    {
			    
			        D = parseInt(strDay.charAt(iD), 10);
			        if(isNaN(D))
			        {
			          flagD=true;
			          break;			    
			        }    			    
			    }
			}
			else
			  flagD=true;
			  
			if(flagD==true)
			{
			    err = 3;
				return false;
			}
			
			intday = parseInt(strDay, 10);
			if (isNaN(intday)) 
			{
				err = 2;
				return false;
			}
			intMonth = parseInt(strMonth, 10);

			if (isNaN(intMonth)) 
			{
				for (k = 0;k<12;k++) 
				{
					if (strMonth.toUpperCase() == strMonthArray[k].toUpperCase()) 
					{
						intMonth = k+1;
						strMonth = strMonthArray[k];
						k = 12;
					}
				}
				if (isNaN(intMonth)) 
				{
					err = 3;
					return false;
				}
			}
			
			
			//Kamran 3585 03/31/08 remove year bug
			var flag = false;
			var yr;
			
			if(isYear (strYear))
			{
			    for(iyr=0; iyr<strYear.length; iyr++)
			    {
			    
			        yr = parseInt(strYear.charAt(iyr), 10);
			        if(isNaN(yr))
			        {
			          flag=true;
			          break;			    
			        }    			    
			    }
			}
			else
			  flag=true;
		
			if(flag==true)
			{
			    err = 4;
				return false;
			}
			//noufil 4275 06/20/2008 intYear uncommented to use on leap year
			intYear = parseInt(strYear, 10);
			if (isNaN(intYear)) 
			{
				err = 4;
				return false;
			}
			
			if (intMonth>12 || intMonth<1) 
			{
				err = 5;
				return false;
			}

			if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) 
			{
				err = 6;
				return false;
			}
			if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) 
			{
				err = 7;
				return false;
			}
			if (intMonth == 2) 
			{
				if (intday < 1) 
				{
					err = 8;
					return false;
				}
				if (LeapYear(intYear) == true) 
				{
					if (intday > 29) 
					{
						err = 9;
						return false;
					}
				}
				else 
				{
					if (intday > 28) 
					{
						err = 10;
						return false;
					}
				}
			}

			if (strDatestyle == "US") 
			{
				datefield = strMonthArray[intMonth-1] + "/" + intday+ "/" + strYear;
			}
			else 
			{
				datefield = intday + " " + strMonthArray[intMonth-1] + " " + strYear;
			}

			return true;
		}
		
		//Zeeshan 3709 06/12/2008
		//This Function Convert Date in MM/DD/YYYY from MM/DD/YY format
		function formatDate(datestring)
		{
		    if ( datestring.length <= 8 )
		    {		    
   		    
		           var strDateArray = datestring.split("/");
		           		
		           var strDay = strDateArray[1];
				   var strMonth = strDateArray[0];
				   var strYear = strDateArray[2];		
		           		           		            
		            if(strYear != null)// if string was given it won't give error
                    {
			            var currDate = new Date();
        			    
			            if (strYear.length == 2) 
			            {
        				    
				            var currYear = currDate.getFullYear() + "";
        		
				            if (currYear.substring(2) < strYear ) 
				                strYear = '19' + strYear;
				            else 
				                strYear = '20' + strYear;
			            }
                   }	
                   return (strMonth + "/" + strDay  + "/" + strYear);
             }
             return datestring
		}
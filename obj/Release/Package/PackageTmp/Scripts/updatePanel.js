

// **********Added By : Zeeshan Ahmed ************//
//   Added For Hiding and Showing Drop Down List  //
//************************************************//


    var  _backgroundElement = null;
    var _foregroundElement = null;
    var _saveTabIndexes = new Array();
    var _saveDesableSelect = new Array();
    var _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');

function disableTab()  {
                
                
        _tagWithTabIndex = new Array('A','BUTTON','TEXTAREA','INPUT','IFRAME');
              
        var i = 0;
        var tagElements;
        var tagElementsInPopUp = new Array();
        Array.clear(_saveTabIndexes);
        
         _foregroundElement = document.getElementById("zee");

       //Save all popup's tag in tagElementsInPopUp
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = _foregroundElement.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                tagElementsInPopUp[i] = tagElements[k];
                i++;
            }
        }

        i = 0;
        for (var j = 0; j < _tagWithTabIndex.length; j++) {
            tagElements = document.getElementsByTagName(_tagWithTabIndex[j]);
            for (var k = 0 ; k < tagElements.length; k++) {
                if (Array.indexOf(tagElementsInPopUp, tagElements[k]) == -1)  {
                    _saveTabIndexes[i] = {tag: tagElements[k], index: tagElements[k].tabIndex};
                    tagElements[k].tabIndex="-1";
                    i++;
                }
            }
        }

        //IE6 Bug with SELECT element always showing up on top
       
        i = 0;
            //Save SELECT in PopUp
             var tagSelectInPopUp = new Array();
            for (var j = 0; j < _tagWithTabIndex.length; j++) 
            {
                tagElements = _foregroundElement.getElementsByTagName('SELECT');
                for (var k = 0 ; k < tagElements.length; k++) {
                    tagSelectInPopUp[i] = tagElements[k];
                    i++;
                }
            }

            i = 0;
            Array.clear(_saveDesableSelect);
            tagElements = document.getElementsByTagName('SELECT');
            for (var k = 0 ; k < tagElements.length; k++) 
              {
                if (Array.indexOf(tagSelectInPopUp, tagElements[k]) == -1)  
                {
                    _saveDesableSelect[i] = {tag: tagElements[k], visib: getCurrentStyle(tagElements[k], 'visibility')} ;
                    tagElements[k].style.visibility = 'hidden';
                    i++;
                }
             }
        
        
    }
           
    

    function restoreTab()
     {
        
        for (var i = 0; i < _saveTabIndexes.length; i++) {
            _saveTabIndexes[i].tag.tabIndex = _saveTabIndexes[i].index;
        }

        for (var k = 0 ; k < this._saveDesableSelect.length; k++) {
                _saveDesableSelect[k].tag.style.visibility = _saveDesableSelect[k].visib;
            
        }
    }


        function getCurrentStyle (element, attribute, defaultValue) {
    

        var currentValue = null;
        if (element) {
            if (element.currentStyle) {
                currentValue = element.currentStyle[attribute];
            } else if (document.defaultView && document.defaultView.getComputedStyle) {
                var style = document.defaultView.getComputedStyle(element, null);
                if (style) {
                    currentValue = style[attribute];
                }
            }
            
            if (!currentValue && element.style.getPropertyValue) {
                currentValue = element.style.getPropertyValue(attribute);
            }
            else if (!currentValue && element.style.getAttribute) {
                currentValue = element.style.getAttribute(attribute);
            }       
        }
        
        if ((!currentValue || currentValue == "" || typeof(currentValue) === 'undefined')) {
            if (typeof(defaultValue) != 'undefined') {
                currentValue = defaultValue;
            }
            else {
                currentValue = null;
            }
        }   
        return currentValue;  
    }


//************************************************//





// Created By Zeeshan For Updateds



		function submitPopup1(formname)
		{
		
		    frmviolationfee =  document.getElementById(formname);
		    
		
		    //Validating Ticket Number
			var casenumber = frmviolationfee.tb_pticketno.value;
			var refcasenumber = casenumber;
			
			if (refcasenumber == "" ) 
			{
				alert("Invalid Ticket Number.");
				frmviolationfee.tb_pticketno.focus();
				return false;
			}
			if (refcasenumber.length<5)
			{
				alert("Ticket Number must be greater then four characters.");
				frmviolationfee.tb_pticketno.focus();
				return false;
			}
			if (casenumber.indexOf(" ") != -1 ) 
			{
				alert("Please make sure that there are no spaces in the ticketnumber input box"); 
				frmviolationfee.tb_pticketno.focus();
				return false;
			}
		    
		    // Validating Cause Number
		    var courtnum = frmviolationfee.tb_prm.value ;
			var datetype = frmviolationfee.ddl_pstatus.value;
			selectedindex = frmviolationfee.ddl_pstatus.selectedIndex;
			var strdatetype =frmviolationfee.ddl_pstatus.value;
			var varhours = frmviolationfee.ddl_pcrttime.value;
			var strdate = frmviolationfee.tb_pmonth.value + "/" + frmviolationfee.tb_pday.value + "/20" + frmviolationfee.tb_pyear.value;
			var strdatewithtime = strdate + " " + varhours;
			var courtstatusauto = document.getElementById("hf_AutoStatusID").value;
			var courtnumauto =document.getElementById("hf_AutoNo").value;
			var courtdateauto = document.getElementById("hf_AutoCourtDate").value + " " + document.getElementById("hf_AutoTime").value;
			//Violation Description Check
				   
		    if ( courtnum.length > 1)
		     {
		       if ( courtnum.substring(0,1) == "0")
		       {
		          courtnum = courtnum.substring(1);
		       }
		    }
			
			
			if(document.getElementById("ddl_pvdesc").style.display != 'none')
			{
			   
			    var ddltext=frmviolationfee.ddl_pvdesc.options[frmviolationfee.ddl_pvdesc.selectedIndex].text;
				if (frmviolationfee.ddl_pvdesc.value==0)
				{
					alert("Please Select ViolationDescription.");
					frmviolationfee.ddl_pvdesc.focus();
					return false;
				}
				if(ddltext.substring(0,3)== "---")
				{ 
				    alert("Please Select ViolationDescription.");
					frmviolationfee.ddl_pvdesc.focus();
					return false;
				}
					   
			    document.getElementById("hf_violationid").value = frmviolationfee.ddl_pvdesc.value;
			    
			}
		    
		    if (  document.getElementById("hf_violationid").value  == "0" ) 
		    {
		    
		    alert("Please Select ViolationDescription.");
			
			if ( frmviolationfee.ddl_pvdesc.style.display == "none")
			{
                
                DisplayToggleP(1);
			    frmviolationfee.ddl_pvdesc.focus();
			    
			}
			else
			{
			frmviolationfee.ddl_pvdesc.focus();
			}
			
			
			//frmviolationfee.ddl_pvdesc.focus();
			return false;
		    
		    }
		    
		    //Fine AMount Check
			var fineamount = frmviolationfee.tb_pfineamount.value;
			if (fineamount=="") 
			{
				alert("Please enter Fine Amount."); 
				frmviolationfee.tb_pfineamount.focus();
				return false;
			}
			if (fineamount.indexOf(" ") != -1) 
			{
				alert("Please make sure that there are no spaces in the Fine Amount input box"); 
				frmviolationfee.tb_pfineamount.focus();
				return false;
			}
			if(isNaN(fineamount))
			{
				alert("Invalid Fine Amount.");
				frmviolationfee.tb_pfineamount.focus();
				return false;
			}
			//Bond AMount Check
			
		    if (frmviolationfee.tb_pbondamount.value=="") 
			{
				alert("Please enter Bond Amount."); 
				frmviolationfee.tb_pbondamount.focus();
				return false;
			}
			
			if(isNaN(frmviolationfee.tb_pbondamount.value))
			{
				alert("Invalid Bond Amount.");
				frmviolationfee.tb_pbondamount.focus();
				return false;
			}
			var ValidChars = "-";
			var Char;
			var sText=frmviolationfee.tb_pfineamount.value;
			
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid fine amount");
					frmviolationfee.tb_pfineamount.focus();
					return false;
				}
			}
			var sText=frmviolationfee.tb_pbondamount.value;
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid bond amount");
					frmviolationfee.tb_pbondamount.focus();
					return false;
				}
			}
		    
		    // Remaining Validation
		    
		     if ( 	 frmviolationfee.tb_pmonth.value =="" || frmviolationfee.tb_pday.value =="" || frmviolationfee.tb_pyear.value == "")
		     {
		            alert("Please enter court date.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }

            if (isNaN(frmviolationfee.tb_pmonth.value) )
		     {
		            alert("Please enter valid month.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }
		        
		     if (  frmviolationfee.tb_pmonth.value > 12 )
		     {
		            alert("Please enter valid month.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }
		     
		     if (isNaN(frmviolationfee.tb_pday.value))
             {
	                alert("Please enter valid day.");
	                frmviolationfee.tb_pday.focus();
	                return false;
             }		     
			    
	        if (isNaN(frmviolationfee.tb_pyear.value))
	        {
                alert("Please enter valid year.");
                frmviolationfee.tb_pyear.focus();
                return false;
	        }
		   		   
            
		        
		    if( isvaliddate ("20"+ frmviolationfee.tb_pyear.value,  frmviolationfee.tb_pday.value ,frmviolationfee.tb_pmonth.value  ) == false )
		    {
		    alert('Invalid court date entered.\nPlease enter date in MM/DD/YYYY format.');
		    frmviolationfee.tb_pmonth.focus();
		    return false;
		    }  
		    
		    
		    
		    //Status Check
			
			if(document.getElementById("ddl_pstatus").style.display != 'none')
			{
			   
			    var ddltext=frmviolationfee.ddl_pstatus.options[frmviolationfee.ddl_pstatus.selectedIndex].text;
				if (frmviolationfee.ddl_pstatus.value==0)
				{
					alert("Please Select Case Status.");
					frmviolationfee.ddl_pstatus.focus();
					return false;
				}
				
					   
			    document.getElementById("hf_status").value = frmviolationfee.ddl_pstatus.value;
			    
			}
					
			if (document.getElementById("hf_status").value=="0")
			{
				alert("Please Select Case Status.");
							
				if ( frmviolationfee.ddl_pstatus.style.display == "none")
				{
				   DisplayToggleP(2);
				   frmviolationfee.ddl_pstatus.focus();
						
				}
				else
				{					
					frmviolationfee.ddl_pstatus.focus();
				}
						
				return false;
			}
				
			if (document.getElementById("ddl_pcrtloc").style.display != "none")
			{
			        if ( document.getElementById("ddl_pcrtloc").selectedIndex == 0 )
			        {
			            alert("Please select court location.");
			            document.getElementById("ddl_pcrtloc").focus();
			            return false;
        			
			        }
			        
			        if(document.getElementById("ddl_pcrtloc").value =="3061")
			        {
			            if(document.getElementById("tb_oscare_court").value =="")
			            {
			                alert("Please enter Oscare Court Detail.");
			                document.getElementById("tb_oscare_court").focus();
			                return false;
			            }
			        }
			        
			       document.getElementById("hf_courtid").value =document.getElementById("ddl_pcrtloc").value;
			
			}
		
			if ( document.getElementById("ddl_ppriceplan").selectedIndex == 0 )
			{
			    alert("Please select price plan.");
			    document.getElementById("ddl_ppriceplan").focus();
			    return false;
			
			}
			
			
			var courtid = 3001 ;
			courtid =  document.getElementById("hf_courtid").value;
			//Check For Court Date
			var bflag=0;
			if(document.getElementById("rbl_bondflag_0").checked)
			{
				bflag = 1;
			}
			else
			{
				bflag = 0;
			}
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
			
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					frmviolationfee.tb_pmonth.focus();
					return false;			
				}
			}
			
			var samedateflag = false;
			
			if (DateDiff(strdatewithtime, courtdateauto) == 0) 
			{
				samedateflag  = true;
			}
			
			var doyou;
			if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
			{
				if (courtstatusauto == strdatetype) 
				{
					if ((samedateflag == false) || (parseInt(courtnumauto) != parseInt(courtnum))) 
					{
						doyou = confirm("The verified court status and auto load courts status are the same but have conflicting setting information.  Would you like to proceed? (OK = Yes   Cancel = No)");
						if (doyou == true) 
						{
						}
						else
						{
							return false;
						}
					}
				}

				if ((refcasenumber.substring(0,1) == "0" ) || (refcasenumber.substring(0,1) == "M") || (refcasenumber.substring(0,1) == "A")) 
				{
    
				}

		if(courtid == "3001")
		{ 
			var courtno = courtnum * 1;
			if ((courtno < 0) || (courtno > 17)||(courtno > 12 && courtno < 15))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
		}
		
				if((courtid == "3002")||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}

				if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM") && (varhours != "10:30 AM") && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							frmviolationfee.ddl_pcrttime.focus();
							return false;
						}
						
						
						
						var bodflag=0;
						if(document.getElementById("rbl_bondflag_0").checked)
						{
							bodflag = 1;
						}
						else
						{
							bodflag = 0;
						}
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
							if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
							{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								frmviolationfee.tb_prm.focus();
								return false;
							}
						}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						
						
						if ( frmviolationfee.ddl_pstatus.style.display == "none")
						{
						    DisplayToggleP(2);
						    frmviolationfee.ddl_pstatus.focus();
						
						}
						else
						{					
						frmviolationfee.ddl_pstatus.focus();
						}
						return false;
					}
				}
				
				var bdflag=0;
				if(document.getElementById("rbl_bondflag_0").checked)
				{
					bdflag = 1;
				}
				else
				{
					bdflag = 0;
				}
				if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (courtid != "3003") 
					{
						if ((courtnum == "18") ) 
						{
							alert("This court number can only be set for Dairy Ashford Court");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}
					if (courtid == "3003") 
					{
						if ((courtnum != "18") ) 
						{
							alert("Court number 18 can only be set for Dairy Ashford Court");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}
				}
			}	
			
			
			var strdate = frmviolationfee.tb_pmonth.value + "/" + frmviolationfee.tb_pday.value + "/20" + frmviolationfee.tb_pyear.value; 
			var strtime = frmviolationfee.ddl_pcrttime.value;
			var bbflags=0;
			if(document.getElementById("rbl_bondflag_0").checked)
			{
				bbflags = 1;
			}
			else
			{
				bbflags = 0;
			}
			if (bbflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
				if (strtime == "--") 
				{
					alert("Invalid Court Time. ");
					frmviolationfee.ddl_pcrttime.focus();
					return false;
				}
				if (strtime == "11") 
				{
					if (datetype != 80 )
					{
						selectedindex1 = frmviolationfee.ddl_pvdesc.selectedIndex;
						var strviolationnew = trimAll(frmviolationfee.ddl_pvdesc.options[selectedindex1].text);
					
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Time. ");
							frmviolationfee.ddl_pcrttime.focus();
							return false;
						}
					}
				}
			}
		
			//Check For Court No
			var bndflag=0;
			var datetype1 = frmviolationfee.ddl_pstatus.value;
			if(document.getElementById("rbl_bondflag_0").checked)
			{
				bndflag = 1;
			}
			else
			{
				bndflag = 0;
			}
			if (bndflag ==0 && datetype1 != 104 && datetype1 != 105 && datetype1 != 135)
			{
				var courtnumber = frmviolationfee.tb_prm.value;
				if (courtnumber.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the CourtNumber input box"); 
					frmviolationfee.tb_prm.focus();
					return false;
				}
				if (courtnumber=="") 
				{
					 frmviolationfee.tb_prm.value=0;
					//alert("Please Enter Court Number.");
					//frmviolationfee.tb_prm.focus();
					//return false;
				}
/*				if (courtnumber=="0") 
				{
					alert("Invalid Court Number.");
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
*/				
				var ValidChars = "0123456789";
				var Char;
				for (j = 0; j < courtnumber.length; j++) 
				{ 
					Char = courtnumber.charAt(j); 
					if (ValidChars.indexOf(Char) == -1) 
					{
						alert("Invalid Court Number");
						frmviolationfee.tb_prm.focus();
						return false;
					}
				}
			}
			
			
			if (strdate == "0/--/2005" )   
			{
				var bflags=0;
				if(document.getElementById("rbl_bondflag_0").checked)
				{
					bflags = 1;
				}
				else
				{
					bflags = 0;
				}
				if (bflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (datetype != 80) 
					{
						selectedindex1 = frmviolationfee.ddl_pvdesc.selectedIndex;
						var strviolationnew = trimAll(frmviolationfee.ddl_pvdesc.options[selectedindex1].text);		
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Date. ");
							frmviolationfee.tb_pmonth.focus();
							return false;
						}
					}
				}
			}
			else 
			{
				//alert(launchCheck(strdate))
				var fticketsflag1 ="";// frmviolationfee.txt_FTicketCount.value;
				//var bondflag = UpdateViolation.txt_BondFlag.value;
				var bondflag = 0;
				if(document.getElementById("rbl_bondflag_0").checked)
				{
					bondflag = 1;
				}
				else
				{
					bondflag = 0;
				}
								
				      
				       
				if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
				{
					if (bondflag == 0) 
					{
						if (datetype != 80 && datetype != 104 && datetype != 105 && datetype != 135 && datetype != 196)  // Not Disposed
						{
							if (launchCheck(strdate) == false && document.getElementById("hf_newviolation").value != "1" && document.getElementById("hf_allowolddate").value =="0" )  // if date is in the past
							{
								alert("Court Date has already passed. Please specify a future Date. ");
								frmviolationfee.tb_pmonth.focus();
								return false;
							}
						}
					//}
						//a;
						var bondflagT = 1;//frmviolationfee.txt_BondFlag.value;
/*						if(bondflagT == 0)
						{
							if ( fticketsflag1 == 0 & UpdateViolation.txt_IsNew.value=="0" & datetype != 135 & launchCheck(strdate) == false ) 
							{
								alert("Please Enter at least one Fticket to process this client. ");
								return false;
							}
						}
*/						
					}	
				}
				if (i != 3 ) 
				{
					var violationnumber;
					var fticketflag = false;
					if (launchCheck(strdate) == false ) 
					{
						//alert(strdate)
						for (var i=0; i<=frmviolationfee.ddl_pvdesc.length-1;i++) 
						{
							violationnumber = frmviolationfee.ddl_pvdesc[i].value;
							if ((violationnumber == "50") || (violationnumber == "448") || (violationnumber == "2821") || (violationnumber == "2822") || (violationnumber == "2824") || (violationnumber == "4337") || (violationnumber == "4999") || (violationnumber == "5103")) 
							{
								fticketflag = true;
								break;
							}
						}

						if (fticketflag == false)
						{
							if (datetype != 80) 
							{
								//alert("Court Date has already passed. Please specify a different Date or Click Insert Violation link. ");
								//return false;
							}
						}
					}
				}		
			}
		    
		   	  
			
	    plzwait('tbl_violations','tbl_plzwait1');	  
		closepopup();
		return true;
		}
		
		function DateDiff(date1, date2)
		{
			var objDate1=new Date(date1);
			var objDate2=new Date(date2);
			return (objDate1.getTime()-objDate2.getTime())/1000;
		}
		
		
		function launchCheck(strdate) 
		{
			var today = new Date(); // today
			var date = new Date(strdate); // your launch date
			//alert(date.getTime())
			//alert(today.getTime())
			var oneday = 1000*60*60*24
			//alert(Math.ceil(today.getTime()-date.getTime())/oneday)
			if (Math.ceil(today.getTime()-date.getTime())/oneday >= 1 ) 
			//if ( date < today )
			{
				return false;
			}
 
			//if (today.getTime()  > date.getTime()) { // on or after launch date
				//return false;
			//}
		
		document.getElementById("tbl_plzwait1").style.display = "block";
		document.getElementById("tbl_violations").style.display = "none";
		document.getElementById("tbl_plzwait1").focus();
		
		
			return true;
		}
		
		function closepopup(){	    
		   
		    document.getElementById("zee").style.display = 'none';
	        document.getElementById("Disable").style.display = 'none';	
	        restoreTab();
	      
	    }
	    
	    function plzwait(hidepanelname  , showpanelname)
	    {
	    
	        document.getElementById(showpanelname).style.display = "block";
		    document.getElementById(hidepanelname).style.display = "none";
		    document.getElementById(showpanelname).focus();
	    
	    }
	    
	    function setpopuplocation()
		{
	     	    
	    var top  = 400;
	    var left = 400;
	    var height = document.body.offsetHeight;
	    var width  = document.body.offsetWidth
	    
	    var resolution =  width  + "x" + height;
	    	   	    
	    if ( width > 1100 || width <= 1280)
	    {
	        left = 575
	    }
	    
	    if ( width < 1100)
	    {
	        left = 500;
	    
	    }
	        
	    
		// Setting popup display
	    document.getElementById("zee").style.top =top;
		document.getElementById("zee").style.left =left;
		
				
		if (  document.body.offsetHeight > document.body.scrollHeight )
			document.getElementById("Disable").style.height = document.body.offsetHeight;
			
		else
		document.getElementById("Disable").style.height = document.body.scrollHeight ;
		
		
		document.getElementById("Disable").style.width = document.body.offsetWidth;
		document.getElementById("Disable").style.display = 'block'
		document.getElementById("zee").style.display = 'block'
		
			
		}
		
		
		function selectValues(ddl ,srch)
		{	     
	                
	        for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( srch == value)
		            {
		               ddl.options[i].selected = true;
		               break;
		            }
		    }
		}
		
		
	function DisplayToggleP(v)
		{		    
		    if ( v == "1")
		    {
		        
		    var temp =   document.getElementById("ddl_pvdesc").style.display;
			document.getElementById("ddl_pvdesc").style.display = document.getElementById("lbl_pvdesc").style.display;
		    document.getElementById("lbl_pvdesc").style.display = temp;
		    		    
		    }
		    		    
		    if ( v == "2")
		    {
		    var temp =   document.getElementById("ddl_pstatus").style.display;
			document.getElementById("ddl_pstatus").style.display = document.getElementById("lbl_pstatus").style.display;
		    document.getElementById("lbl_pstatus").style.display = temp;
		  	}
				
		}
				
		// Close Update Popup
		function closepopup(){	    
		   
		   restoreTab();
		   document.getElementById("zee").style.display = 'none';
	       document.getElementById("Disable").style.display = 'none';	
	        //ddlstate(false);    
	    }
	    
	    
	    
	    //--------------------------Added By Zeeshan Ahmed--------------------------------\\
	    //---------------------- Scripts of Violation Fee Page ---------------------------\\
	    	    	    
	    /*function plzwait()
		{
		  // Hide Calulation Panel for Updation
		  plzwait('FeeCalc','tbl_plzwait');
		}*/
	    
	    
	    
	    // Add New Violation Method -- Clears All Controls
	    
	    function addnewviolation()
		{
		     var v  = document.getElementById("hf_lastrow").value ; 
	         v = parseInt(v) + 1;
    		 
		     if ( parseInt(v) < 10 )
		     v = "0" + v;
    			
		    showpopup("dgViolationInfo_ctl"+ v ,1)
		    //For New Violation Set Value = 1 
		    document.getElementById("hf_violationid").value = "-1";
	        // Clearing All PopUp Controls
	        document.getElementById("lbl_pvdesc").style.display = 'none';
	        document.getElementById("lbl_pstatus").style.display = 'none';
		    document.getElementById("lbl_pvdesc").innerText = '';
	        document.getElementById("lbl_pstatus").innerText = '';
		    document.getElementById("ddl_pvdesc").style.display = 'block';
		    document.getElementById("ddl_pstatus").style.display = 'block';
		    document.getElementById("tb_causeno").value = "";
		    document.getElementById("tb_pfineamount").value = "0.0";
		    document.getElementById("tb_pbondamount").value = "0.0";
	        document.getElementById("hf_violationid").value = "-1";
	        document.getElementById("hf_ticketviolationid").value = "-1";
	        document.getElementById("hf_ticketsviolationid").value = "0";
	        document.getElementById("ddl_pvdesc").selectedIndex =  0;
            document.getElementById("ddl_pcrtloc").style.display = "block";		    
		    document.getElementById("lbl_pcname").style.display = "none";	
            //Setting Popup location
            setpopuplocation(); 
       	
		}
	    
	    
	    // Fill Update Popup
	    
	    // Show Update Popup
	    function showpopup(v,w)
		{				
		  
		    
		  if ( document.getElementById("ddlSpeed").style.display != 'none')
		  {
		    DisplayToggle();  		 
		  }
		  		  
		  document.frmviolationfee.cb_updateall.checked = false;
		  document.getElementById("tb_causeno").value =   document.getElementById(v + "_lbl_causeno").innerText;
		  document.getElementById("tb_pticketno").value =   document.getElementById(v + "_lnkbtn_caseno").innerText;
		 
		  // Displaying Court Date 
		  var date = document.getElementById(v + "_lbl_courtdate").innerText;
		 		 
		  if ( date == "1/1/1900" || date == "01/01/1900")
		  {
		  document.getElementById("tb_pmonth").value ="";
		  document.getElementById("tb_pday").value   ="";
		  document.getElementById("tb_pyear").value  ="";
		  }
		  else
		  {	   		    
		 
		  document.getElementById("tb_pmonth").value =   date.substring(0, date.indexOf('/'));
		  document.getElementById("tb_pday").value =   date.substring(date.indexOf('/')+1 , date.lastIndexOf('/'));
		  document.getElementById("tb_pyear").value =   date.substring(date.lastIndexOf('/') + 1+2 ,date.length);
		  }
		  
		 document.getElementById("tb_prm").value = document.getElementById(v + "_lbl_room").innerText;
		 document.getElementById("tb_pfineamount").value = document.getElementById(v+"_hf_fineamount").value.substring(1);
		 document.getElementById("tb_pbondamount").value = document.getElementById(v+"_hf_bondamount").value.substring(1);
	     document.getElementById("hf_violationid").value = document.getElementById(v+"_hf_violationid").value;
	     document.getElementById("hf_ticketviolationid").value = document.getElementById(v+"_hf_ticketviolationid").value;
		 document.getElementById("hf_ticketsviolationid").value = document.getElementById(v+"_hf_ticketsviolationid").value;
		 // Display Court Time 
	     document.getElementById("ddl_pcrttime").value =   document.getElementById(v + "_lbl_time").innerText;
		 document.getElementById("lbl_pvdesc").innerText =   document.getElementById(v + "_Label4").innerText;
		 document.getElementById("lbl_pstatus").innerText =   document.getElementById(v + "_lbl_status").innerText;
		 document.getElementById("hf_temp2").innerText =   document.getElementById(v + "_Label4").innerText;
		 document.getElementById("hf_temp1").innerText =   document.getElementById(v + "_lbl_status").innerText;
		 // Filling Hidden Fields
		 document.getElementById("hf_AutoStatusId").value =   document.getElementById(v + "_hf_courtviolationstatusid").value;
		 document.getElementById("hf_AutoNo").value =   document.getElementById(v + "_lbl_room").innerText;
		 document.getElementById("hf_AutoCourtDate").value =   document.getElementById(v + "_lbl_courtdate").innerText;
		 document.getElementById("hf_AutoTime").value =   document.getElementById(v + "_lbl_time").innerText;
		 // Hiding Violation DropDownList   
		 document.getElementById("ddl_pvdesc").style.display='none';
		 document.getElementById("ddl_pstatus").style.display='none';
		 document.getElementById("lbl_pvdesc").style.display='block';
		 document.getElementById("lbl_pstatus").style.display='block';
		
		
		 		  
	     var  bondflag = document.getElementById(v+"_hf_bondflag").value
		   
		    if ( bondflag == "YES" )
		    {
		        document.getElementById("rbl_bondflag_0").checked= true;
		    }
		    else
		    {
		     document.getElementById("rbl_bondflag_1").checked = true;
		    }
		 		  
		    var srch = document.getElementById(v + "_hf_priceplan").value;
		    var ddl = document.getElementById("ddl_ppriceplan")
	
		    for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( value == srch)
		            {
		               ddl.options[i].selected = true;
		               break;
		            }
		     }
		    
		   
		    document.getElementById("hf_courtid").value =document.getElementById(v + "_hf_courtid").value;
		    
		    var srch = document.getElementById(v + "_hf_courtloc").value;
		   	var ddl = document.getElementById("ddl_pcrtloc")
	        var checkcourt = false;
	        for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( text == srch)
		            {
		               ddl.options[i].selected = true;
		               checkcourt = true;
		               break;
		            }
		     }
		    	
		    if ( checkcourt)
		    {
		        document.getElementById("ddl_pcrtloc").style.display = "block";		    
		        document.getElementById("lbl_pcname").style.display = "none";		    
		    }
		    else
		    {
		         document.getElementById("ddl_pcrtloc").style.display = "none";		    
		         document.getElementById("lbl_pcname").style.display = "block";		
		         document.getElementById("lbl_pcname").innerText = document.getElementById(v + "_lbl_court").innerText
		    }
		     	
		    	   
		    
		    if ( document.getElementById( v +"_lbl_status").innerText == "N/A" ||document.getElementById( v +"_lbl_status").innerText == ""  )
		    {
		        document.getElementById("hf_status").value= 0;
		        document.getElementById("ddl_pstatus").selectedIndex = 0;
		    	    
		    }    
		    else
		    {
		    
		    var srch = document.getElementById(v + "_hf_courtviolationstatusid").value;
		    document.getElementById("hf_status").value = srch;
		   	var ddl = document.getElementById("ddl_pstatus")
	     
	        for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( value == srch  )
		            {
		               ddl.options[i].selected = true;
		               document.getElementById("lbl_pstatus").innerText =  ddl.options[i].innerText;
		               break;
		            }
		     }
		    	   	   
		    }
		   
		    
		    var srch = document.getElementById(v + "_Label4").innerText;
		   	
		   	if ( srch == "0" )
		   	{
		   	
		   	document.getElementById("hf_ticketsviolationid").value = "0";
		   	document.getElementById("ddl_pvdesc").selectedIndex = 0;
		   	     	    
		   	}
		   	else
		   	{
		   	
		   	
		   	var ddl = document.getElementById("ddl_pvdesc")
	        document.getElementById("hf_ticketsviolationid").value = document.getElementById(v+"_hf_violationid").value;
	        for ( i =0 ; i < ddl.options.length ; i++)
		    {
		            var text = ddl.options[i].innerText;
		            var value = ddl.options[i].value;
		            
		            if ( value == document.getElementById(v+"_hf_violationid").value)
		            {
		               ddl.options[i].selected = true;
		               break;
		            }
		     }
		   }
		   
		    var ddlo = document.getElementById("ddl_pcrtloc")
		
		 //if(document.getElementById("ddl_pcrtloc").value =="3061")
		
		   if(ddlo.value =="3061")
		    {
		        document.getElementById("tb_oscare_court").style.display = "block";
		        document.getElementById("tb_oscare_court").value = document.getElementById(v + "_hf_OscareCourt").value;
		    }
		    else
		    {
		         document.getElementById("tb_oscare_court").value ="";
		         document.getElementById("tb_oscare_court").style.display = "none";		         
		    }
		   
		   setpopuplocation();  
	            
	        if ( w == "0")
	        {	       
	                if ( document.getElementById(v + "_hf_courtviolationstatusid").value == "0" && w == "0")
	                {
	                  //alert( document.getElementById(v + "_hf_courtviolationstatusid").value );
	                  document.getElementById("hf_allowolddate").value = 1 ;
	                }
	                else
	                {
	                document.getElementById("hf_newviolation").value = 0;
	                document.getElementById("hf_allowolddate").value = 0;
	                }
            }
            else
            {
            
            document.getElementById("hf_newviolation").value = 1;            
            document.getElementById("hf_allowolddate").value = 1 ;
            
            }
		 
		 disableTab();
		    
		}
	    
	    
	    // Update Panel Validation
	    
	    function submitPopup()
		{
		
		    //Validating Ticket Number
			var casenumber = document.frmviolationfee.tb_pticketno.value;
			var refcasenumber = casenumber;
			
			if (refcasenumber == "" ) 
			{
				alert("Invalid Ticket Number.");
				document.frmviolationfee.tb_pticketno.focus();
				return false;
			}
			if (refcasenumber.length<5)
			{
				alert("Ticket Number must be greater then four characters.");
				document.frmviolationfee.tb_pticketno.focus();
				return false;
			}
			if (casenumber.indexOf(" ") != -1 ) 
			{
				alert("Please make sure that there are no spaces in the ticketnumber input box"); 
				document.frmviolationfee.tb_pticketno.focus();
				return false;
			}
		    
		    // Validating Cause Number
		    var courtnum = frmviolationfee.tb_prm.value ;
		   
		    if ( courtnum.length > 1)
		     {
		       if ( courtnum.substring(0,1) == "0")
		       {
		          courtnum = courtnum.substring(1);
		       }
		    }
		     
		    
			var datetype = frmviolationfee.ddl_pstatus.value;
			selectedindex = frmviolationfee.ddl_pstatus.selectedIndex;
			var strdatetype =frmviolationfee.ddl_pstatus.value;
			var varhours = frmviolationfee.ddl_pcrttime.value;
			var strdate = frmviolationfee.tb_pmonth.value + "/" + frmviolationfee.tb_pday.value + "/20" + frmviolationfee.tb_pyear.value;
			var strdatewithtime = strdate + " " + varhours;
			var courtstatusauto = document.getElementById("hf_AutoStatusID").value;
			var courtnumauto =document.getElementById("hf_AutoNo").value;
			var courtdateauto = document.getElementById("hf_AutoCourtDate").value + " " + document.getElementById("hf_AutoTime").value;
			//Violation Description Check
			
					
			if(document.getElementById("ddl_pvdesc").style.display != 'none')
			{
			   
			    var ddltext=document.frmviolationfee.ddl_pvdesc.options[document.frmviolationfee.ddl_pvdesc.selectedIndex].text;
				if (frmviolationfee.ddl_pvdesc.value==0)
				{
					alert("Please Select ViolationDescription.");
					frmviolationfee.ddl_pvdesc.focus();
					return false;
				}
				if(ddltext.substring(0,3)== "---")
				{ 
				    alert("Please Select ViolationDescription.");
					frmviolationfee.ddl_pvdesc.focus();
					return false;
				}
					   
			    document.getElementById("hf_violationid").value = document.frmviolationfee.ddl_pvdesc.value;
			    
			}
		    
		    if (  document.getElementById("hf_violationid").value  == "0" ) 
		    {
		    
		    alert("Please Select ViolationDescription.");
			
			if ( frmviolationfee.ddl_pvdesc.style.display == "none")
			{
                
                DisplayToggleP(1);
			    frmviolationfee.ddl_pvdesc.focus();
			    
			}
			else
			{
			frmviolationfee.ddl_pvdesc.focus();
			}
			
			
			//frmviolationfee.ddl_pvdesc.focus();
			return false;
		    
		    }
		    
		    //Fine AMount Check
			var fineamount = document.frmviolationfee.tb_pfineamount.value;
			if (fineamount=="") 
			{
				alert("Please enter Fine Amount."); 
				frmviolationfee.tb_pfineamount.focus();
				return false;
			}
			if (fineamount.indexOf(" ") != -1) 
			{
				alert("Please make sure that there are no spaces in the Fine Amount input box"); 
				frmviolationfee.tb_pfineamount.focus();
				return false;
			}
			if(isNaN(fineamount))
			{
				alert("Invalid Fine Amount.");
				frmviolationfee.tb_pfineamount.focus();
				return false;
			}
			//Bond AMount Check
			
		    if (frmviolationfee.tb_pbondamount.value=="") 
			{
				alert("Please enter Bond Amount."); 
				frmviolationfee.tb_pbondamount.focus();
				return false;
			}
			
			if(isNaN(frmviolationfee.tb_pbondamount.value))
			{
				alert("Invalid Bond Amount.");
				frmviolationfee.tb_pbondamount.focus();
				return false;
			}
			var ValidChars = "-";
			var Char;
			var sText=frmviolationfee.tb_pfineamount.value;
			
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid fine amount");
					frmviolationfee.tb_pfineamount.focus();
					return false;
				}
			}
			var sText=frmviolationfee.tb_pbondamount.value;
			for (i = 0; i < sText.length; i++) 
			{ 
				
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) != -1) 
				{
					alert("Invalid bond amount");
					frmviolationfee.tb_pbondamount.focus();
					return false;
				}
			}
		    
		   	   
		    // Remaining Validation
		    
		     if ( 	 frmviolationfee.tb_pmonth.value =="" || frmviolationfee.tb_pday.value =="" || frmviolationfee.tb_pyear.value == "")
		     {
		            alert("Please enter court date.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }
		    
		    if (frmviolationfee.tb_pday.value =="0" )
		     {
		            alert("Please enter valid day.");
		            frmviolationfee.tb_pday.focus();
		            return false;
		     }
		      if (frmviolationfee.tb_pmonth.value =="0" )
		     {
		            alert("Please enter valid month.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }
		   
		    if (frmviolationfee.tb_pyear.value =="0" )
		     {
		            alert("Please enter valid year.");
		            frmviolationfee.tb_pyear.focus();
		            return false;
		     }
		     
		    if (isNaN(frmviolationfee.tb_pmonth.value) )
		     {
		            alert("Please enter valid month.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }
		        
		     if (  frmviolationfee.tb_pmonth.value > 12 )
		     {
		            alert("Please enter valid month.");
		            frmviolationfee.tb_pmonth.focus();
		            return false;
		     }
		     
		     if (isNaN(frmviolationfee.tb_pday.value))
             {
	                alert("Please enter valid day.");
	                frmviolationfee.tb_pday.focus();
	                return false;
             }		     
			    
	        if (isNaN(frmviolationfee.tb_pyear.value))
	        {
                alert("Please enter valid year.");
                frmviolationfee.tb_pyear.focus();
                return false;
	        }
		   		   
		   
			if( isvaliddate ("20"+ frmviolationfee.tb_pyear.value, frmviolationfee.tb_pday.value, frmviolationfee.tb_pmonth.value ) == false )
		    {
		    alert('Invalid court date entered.\nPlease enter date in MM/DD/YYYY format.');
		    frmviolationfee.tb_pmonth.focus();
		    return false;
		    }
		    
		    //Status Check
					
			if(document.getElementById("ddl_pstatus").style.display != 'none')
			{
			   
			    var ddltext=document.frmviolationfee.ddl_pstatus.options[document.frmviolationfee.ddl_pstatus.selectedIndex].text;
				if (frmviolationfee.ddl_pstatus.value==0)
				{
					alert("Please Select Case Status.");
					frmviolationfee.ddl_pstatus.focus();
					return false;
				}
				
					   
			    document.getElementById("hf_status").value = document.frmviolationfee.ddl_pstatus.value;
			    
			}
					
			if (document.getElementById("hf_status").value=="0")
			{
				alert("Please Select Case Status.");
							
				if ( frmviolationfee.ddl_pstatus.style.display == "none")
				{
				   DisplayToggleP(2);
				   frmviolationfee.ddl_pstatus.focus();
						
				}
				else
				{					
					frmviolationfee.ddl_pstatus.focus();
				}
						
				return false;
			}
				    
		    if (document.getElementById("ddl_pcrtloc").style.display != "none")
			{
			        if ( document.getElementById("ddl_pcrtloc").selectedIndex == 0 )
			        {
			            alert("Please select court location.");
			            document.getElementById("ddl_pcrtloc").focus();
			            return false;
        			
			        }
			        if(document.getElementById("ddl_pcrtloc").value =="3061")
			        {
			            if(document.getElementById("tb_oscare_court").value =="")
			            {
			                alert("Please enter Oscare Court Detail.");
			                document.getElementById("tb_oscare_court").focus();
			                return false;
			            }
			        }
			       document.getElementById("hf_courtid").value =document.getElementById("ddl_pcrtloc").value;
			
			}
			if ( document.getElementById("ddl_ppriceplan").selectedIndex == 0 )
			{
			    alert("Please select price plan.");
			    document.getElementById("ddl_ppriceplan").focus();
			    return false;
			
			}
			
			
			var courtid = 3001 ;
			courtid =  document.getElementById("hf_courtid").value;
			//Check For Court Date
			var bflag=0;
			if(document.getElementById("rbl_bondflag_0").checked)
			{
				bflag = 1;
			}
			else
			{
				bflag = 0;
			}
			if (bflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
			
				if (chkdate(strdate)==false)
				{
					alert("Invalid Court Date.");
					frmviolationfee.tb_pmonth.focus();
					return false;			
				}
			}
			
			var samedateflag = false;
			
			if (DateDiff(strdatewithtime, courtdateauto) == 0) 
			{
				samedateflag  = true;
			}
			
			var doyou;
			if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
			{
				if (courtstatusauto == strdatetype) 
				{
					if ((samedateflag == false) || (parseInt(courtnumauto) != parseInt(courtnum))) 
					{
						doyou = confirm("The verified court status and auto load courts status are the same but have conflicting setting information.  Would you like to proceed? (OK = Yes   Cancel = No)");
						if (doyou == true) 
						{
						}
						else
						{
							return false;
						}
					}
				}

				if ((refcasenumber.substring(0,1) == "0" ) || (refcasenumber.substring(0,1) == "M") || (refcasenumber.substring(0,1) == "A")) 
				{
    
				}

		if(courtid == "3001")
		{ 
			var courtno = courtnum * 1;
			if ((courtno < 0) || (courtno > 17)||(courtno > 12 && courtno < 15))
			{
				alert("Please Enter 0 to 12 or 15 to 17 for Lubbock Court");
				return false;
			}
			
		}
		
				if((courtid == "3002")||(courtid)== "3003")
				{ 
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{ 
						alert("Jury Trial is not a Valid Status");
						return false;
					}
				}
				//borntowin
				/*if(courtid == "3002")
				{ 
					if ( datetype == "103" ) 
					{ 
						alert("Judge Trial can only be set for HMC-L and HMC-D.");
						return false;
					}
				}*/

				if ((courtid == "3001") || (courtid == "3002") || (courtid == "3003")) 
				{
					if ((datetype == "4") || (datetype == "24") || (datetype == "26") || (datetype == "28")) 
					{
						if ((varhours != "8:00 AM") && (varhours != "9:00 AM") && (varhours != "10:30 AM") && (varhours != "10:00 AM")) 
						{
							alert("Jury Trial must be at one of the following times: 8am, 9am, 10am or 10:30am");
							frmviolationfee.ddl_pcrttime.focus();
							return false;
						}
						
						
						
						var bodflag=0;
						if(document.getElementById("rbl_bondflag_0").checked)
						{
							bodflag = 1;
						}
						else
						{
							bodflag = 0;
						}
						if (bodflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
						{
							if ((courtnum != "6") && (courtnum != "1") && (courtnum != "2") && (courtnum != "3") && (courtnum != "8") && (courtnum != "11")  && (courtnum != "12")) 
							{
								alert("Jury Trial sets at this location must be in one of the following court rooms: 1,2,3,6,8,11,12");
								frmviolationfee.tb_prm.focus();
								return false;
							}
						}
					}
					//alert(datetype)
					if ((datetype == "66") || (datetype == "85") || (datetype == "101") || (datetype == "104")) 
					{
						alert("This court location cannot have a status of Waiting or Pre-Trial");
						
						
						if ( frmviolationfee.ddl_pstatus.style.display == "none")
						{
						    DisplayToggleP(2);
						    frmviolationfee.ddl_pstatus.focus();
						
						}
						else
						{					
						frmviolationfee.ddl_pstatus.focus();
						}
						return false;
					}
				}
				
				var bdflag=0;
				if(document.getElementById("rbl_bondflag_0").checked)
				{
					bdflag = 1;
				}
				else
				{
					bdflag = 0;
				}
				if (bdflag ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (courtid != "3003") 
					{
						if ((courtnum == "18") ) 
						{
							alert("This court number can only be set for Dairy Ashford Court");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}
					if (courtid == "3003") 
					{
						if ((courtnum != "18") ) 
						{
							alert("Court number 18 can only be set for Dairy Ashford Court");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}

					if (courtid != "3002") 
					{
						if ((courtnum == "13") || (courtnum == "14") ) 
						{
							alert("This court number can only be set for Mykawa");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}
					//borntowin
					if (courtid == "3002") 
					{
						if ((courtnum != "13") && (courtnum != "14") ) 
						{
							alert("Court number 13 or 14 can only be set for Mykawa.");
							frmviolationfee.tb_prm.focus();
							return false;
						}
					}
				}
			}	
			
			
			var strdate = frmviolationfee.tb_pmonth.value + "/" + frmviolationfee.tb_pday.value + "/20" + frmviolationfee.tb_pyear.value; 
			var strtime = frmviolationfee.ddl_pcrttime.value;
			var bbflags=0;
			if(document.getElementById("rbl_bondflag_0").checked)
			{
				bbflags = 1;
			}
			else
			{
				bbflags = 0;
			}
			if (bbflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
			{
				if (strtime == "--") 
				{
					alert("Invalid Court Time. ");
					document.frmviolationfee.ddl_pcrttime.focus();
					return false;
				}
				if (strtime == "11") 
				{
					if (datetype != 80 )
					{
						selectedindex1 = document.frmviolationfee.ddl_ViolationDescription.selectedIndex;
						var strviolationnew = trimAll(document.frmviolationfee.ddl_ViolationDescription.options[selectedindex1].text);
					
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Time. ");
							frmviolationfee.ddl_pcrttime.focus();
							return false;
						}
					}
				}
			}
		
			//Check For Court No
			var bndflag=0;
			var datetype1 = frmviolationfee.ddl_pstatus.value;
			if(document.getElementById("rbl_bondflag_0").checked)
			{
				bndflag = 1;
			}
			else
			{
				bndflag = 0;
			}
			if (bndflag ==0 && datetype1 != 104 && datetype1 != 105 && datetype1 != 135)
			{
				var courtnumber = document.frmviolationfee.tb_prm.value;
				if (courtnumber.indexOf(" ") != -1 ) 
				{
					alert("Please make sure that there are no spaces in the CourtNumber input box"); 
					document.frmviolationfee.tb_prm.focus();
					return false;
				}
				if (courtnumber=="") 
				{
					 document.frmviolationfee.tb_prm.value=0;
					//alert("Please Enter Court Number.");
					//document.frmviolationfee.tb_prm.focus();
					//return false;
				}
/*				if (courtnumber=="0") 
				{
					alert("Invalid Court Number.");
					document.UpdateViolation.txt_CourtNo.focus();
					return false;
				}
*/				
				var ValidChars = "0123456789";
				var Char;
				for (j = 0; j < courtnumber.length; j++) 
				{ 
					Char = courtnumber.charAt(j); 
					if (ValidChars.indexOf(Char) == -1) 
					{
						alert("Invalid Court Number");
						document.frmviolationfee.tb_prm.focus();
						return false;
					}
				}
			}
			
			
			if (strdate == "0/--/2005" )   
			{
				var bflags=0;
				if(document.getElementById("rbl_bondflag_0").checked)
				{
					bflags = 1;
				}
				else
				{
					bflags = 0;
				}
				if (bflags ==0 && datetype != 104 && datetype != 105 && datetype != 135)
				{
					if (datetype != 80) 
					{
						selectedindex1 = document.frmviolationfee.ddl_pvdesc.selectedIndex;
						var strviolationnew = trimAll(document.frmviolationfee.ddl_pvdesc.options[selectedindex1].text);		
						if (trimAll(strviolationnew) != '------Default Out----')
						{
							alert("Invalid Court Date. ");
							frmviolationfee.tb_pmonth.focus();
							return false;
						}
					}
				}
			}
			else 
			{
				//alert(launchCheck(strdate))
				var fticketsflag1 ="";// frmviolationfee.txt_FTicketCount.value;
				//var bondflag = UpdateViolation.txt_BondFlag.value;
				var bondflag = 0;
				if(document.getElementById("rbl_bondflag_0").checked)
				{
					bondflag = 1;
				}
				else
				{
					bondflag = 0;
				}
								
				      
				       
				if ((courtid == "3001") ||(courtid == "3002") || (courtid == "3003")) 
				{
					if (bondflag == 0) 
					{
						if (datetype != 80 && datetype != 104 && datetype != 105 && datetype != 135)  // Not Disposed
						{
							if (launchCheck(strdate) == false && document.getElementById("hf_newviolation").value != "1" && document.getElementById("hf_allowolddate").value =="0" )  // if date is in the past
							{
								alert("Court Date has already passed. Please specify a future Date. ");
								frmviolationfee.tb_pmonth.focus();
								return false;
							}
						}
					//}
						//a;
						var bondflagT = 1;//frmviolationfee.txt_BondFlag.value;
/*						if(bondflagT == 0)
						{
							if ( fticketsflag1 == 0 & UpdateViolation.txt_IsNew.value=="0" & datetype != 135 & launchCheck(strdate) == false ) 
							{
								alert("Please Enter at least one Fticket to process this client. ");
								return false;
							}
						}
*/						
					}	
				}
				if (i != 3 ) 
				{
					var violationnumber;
					var fticketflag = false;
					if (launchCheck(strdate) == false ) 
					{
						//alert(strdate)
						for (var i=0; i<=frmviolationfee.ddl_pvdesc.length-1;i++) 
						{
							violationnumber = frmviolationfee.ddl_pvdesc[i].value;
							if ((violationnumber == "50") || (violationnumber == "448") || (violationnumber == "2821") || (violationnumber == "2822") || (violationnumber == "2824") || (violationnumber == "4337") || (violationnumber == "4999") || (violationnumber == "5103")) 
							{
								fticketflag = true;
								break;
							}
						}

						if (fticketflag == false)
						{
							if (datetype != 80) 
							{
								//alert("Court Date has already passed. Please specify a different Date or Click Insert Violation link. ");
								//return false;
							}
						}
					}
				}		
			}
		    
		   
		  
		document.getElementById("tbl_plzwait1").style.display = "block";
		document.getElementById("dgViolationInfo").style.display = "none";
		document.getElementById("tbl_plzwait1").focus();
		  
		closepopup();
		return true;
		}
	    
	    //---------------------------------------------------------------------------------\\
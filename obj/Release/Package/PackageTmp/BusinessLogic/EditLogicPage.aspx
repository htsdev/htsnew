<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditLogicPage.aspx.cs"
    Inherits="HTP.BusinessLogic.EditLogicPage" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Business Logic</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 562px;
        }
        .style3
        {
            font-weight: bold;
            font-size: 8pt;
            color: #3366cc;
            font-family: Tahoma;
            text-decoration: none;
            width: 55px;
        }
        .style5
        {
            width: 562px;
        }
    </style>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    
    <!-- CORE CSS TEMPLATE - END -->
    <script language="javascript" type="text/javascript">
    
//    function CheckEmpty()
//    {
//    debugger;
//         if (document.getElementById("ftbBusinessLogic").value.length==0)
//         {
//            alert("Please insert the Text before Click the Submit button!");
//            return false;
//        }
//        else
//            return false;
//    }
    </script>
</head>
<body style="margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px">
    <form id="form1" runat="server">
        <div class="page-container row-fluid container-fluid">
              <section id="main-content" class="">
    <section class="wrapper main-wrapper row" style="">


             <div class="col-xs-12">

             <section class="box" id="">

                 <header class="panel_header">
                     <h2 class="title pull-left">Business Logic Editor</h2>
                     <div class="actions panel_actions pull-right">
                     <asp:HyperLink ID="hlk_BackLogicPage" runat="server" 
                                Font-Underline="True">BACK</asp:HyperLink>



                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                 
                 <div class="content-body">
            <div class="row">

                 <div class="col-md-12">
                                                        <div class="form-group">
                            
                            <span class="desc"></span>
                            <div class="controls">
                                 <asp:Label ID="lblMessage" runat="server" CssClass="form-label" Text="" ForeColor="red"> </asp:Label>
                                <FTB:FreeTextBox ID="ftbBusinessLogic" runat="server" 
                    ToolbarLayout="ParagraphMenu,FontFacesMenu,FontSizesMenu,FontForeColorsMenu|Bold,Italic,Underline,Strikethrough;Superscript,Subscript,RemoveFormat|JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;InsertRule|Cut,Copy,Paste;Undo,Redo,Print">
                </FTB:FreeTextBox>
                                 <asp:Button ID="btnSubmit" runat="server" align="right" CssClass="btn btn-primary" OnClick="btnupSubmit_Click"
                    Text="Submit" />
                                </div>
                                                            </div>
                     </div>
                </div>
                     </div>
                 </section>

             <div class="clearfix"></div>




         </div>




        </section>
                  </section>
            </div>



  
    </form>
     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewBatchLetters.aspx.cs"
    Inherits="HTP.DocumentTracking.ViewBatchLetters" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="~/WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Batch Letter</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </asp:ScriptManager>
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tr>
                <td colspan="2">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clssubhead" height="34" background="../Images/subhead_bg.gif">
                                &nbsp;Veiw Batch Letters
                            </td>
                            <td align="right" class="clssubhead" height="34" style="font-weight: bold; font-size: 9pt;"
                                background="../Images/subhead_bg.gif">
                                &nbsp;
                                <asp:HyperLink ID="hlk_Back" runat="server">Back</asp:HyperLink>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="font-family: Tahoma">
                <td class="clsLeftPaddingTable" colspan="2">
                </td>
            </tr>
            <tr style="font-family: Tahoma">
                <td align="center" class="clsLeftPaddingTable" colspan="2">
                    <asp:UpdatePanel ID="panel5" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr style="font-family: Tahoma">
                <td colspan="2" valign="top">
                    <table id="tbl1" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="clsLeftPaddingTable">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="clsLeftPaddingTable">
                                    <tr>
                                        <td class="clsLeftPaddingTable">
                                            Batch ID
                                        </td>
                                        <td class="clsLeftPaddingTable">
                                            Document ID
                                        </td>
                                        <td class="clsLeftPaddingTable">
                                            Document Type
                                        </td>
                                        <td class="clsLeftPaddingTable">
                                            Scan Date
                                        </td>
                                        <td class="clsLeftPaddingTable">
                                            Total Pages
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 17px">
                                            <asp:Label ID="lbl_BatchID" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td style="height: 17px">
                                            <asp:Label ID="lbl_LetterID" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td style="height: 17px">
                                            <asp:Label ID="lbl_DocumentType" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td style="height: 17px">
                                            <asp:Label ID="lbl_ScanDate" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td style="height: 17px">
                                            <asp:Label ID="lbl_PageCount" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="33%">
                                                    <asp:ImageButton ID="ImgMoveFirst" runat="server" ImageUrl="~/Images/MoveFirst.gif"
                                                        ToolTip="Move First" OnClick="ImgMoveFirst_Click" />&nbsp;
                                                    <asp:ImageButton ID="ImgMovePrev" runat="server" ImageUrl="~/Images/MovePrevious.gif"
                                                        ToolTip="Previous" OnClick="ImgMovePrev_Click" Style="height: 18px" />
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblpageno" runat="server" CssClass="clsLabel"></asp:Label>&nbsp;
                                                    of &nbsp;
                                                    <asp:Label ID="lblCount" runat="server" CssClass="clsLabel"></asp:Label>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlImgsize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlImgsize_SelectedIndexChanged">
                                                        <asp:ListItem Value="500">500%</asp:ListItem>
                                                        <asp:ListItem Value="400">400%</asp:ListItem>
                                                        <asp:ListItem Value="200">200%</asp:ListItem>
                                                        <asp:ListItem Value="175">175%</asp:ListItem>
                                                        <asp:ListItem Value="150" Selected="True">150%</asp:ListItem>
                                                        <asp:ListItem Value="100">100%</asp:ListItem>
                                                        <asp:ListItem Value="75">75%</asp:ListItem>
                                                        <asp:ListItem Value="50">50%</asp:ListItem>
                                                        <asp:ListItem Value="25">25%</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="img_flip" runat="server" ImageUrl="~/Images/Rotate.bmp" OnClick="img_flip_Click"
                                                        ToolTip="Flip Image" />
                                                </td>
                                                <td align="right" style="width: 33%">
                                                    <asp:ImageButton ID="ImgMoveNext" runat="server" ImageUrl="~/Images/MoveNext.gif"
                                                        ToolTip="Next" OnClick="ImgMoveNext_Click" />&nbsp;
                                                    <asp:ImageButton ID="ImgMoveLast" runat="server" ImageUrl="~/Images/MoveLast.gif"
                                                        ToolTip="Move Last" OnClick="ImgMoveLast_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center">
                                            <asp:UpdatePanel ID="panel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="vertical-align: middle; overflow: auto; width: 780px; height: 450px;
                                                        text-align: center">
                                                        <asp:Image ID="img_docs" runat="server" Height="840px" Width="798px" /></div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMoveFirst" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMovePrev" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMoveNext" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ImgMoveLast" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="ddlImgsize" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="img_flip" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td background="../Images/separator_repeat.gif" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:Footer ID="Footer1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="display: none">
                                <asp:Label ID="lbl_IsAlreadyInBatchPrint" runat="server" Text="Label"></asp:Label><asp:Label
                                    ID="lbl_IsSplit" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%@ Page language="c#" AutoEventWireup="false" Inherits="HTP.DocumentTracking.PreviewMain" Codebehind="PreviewMain.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PreviewMain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
			 function ClearFiles()			 
			 {
				window.navigate("ClearFiles.aspx"); 				
			 }
			 function CloseWindow() 
			 {
			    window.opener = self;
                window.close();
			 }
		</script>
	</HEAD>
	<body onunload="ClearFiles();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<iframe src="frmPreview.aspx?DBID=<%=ViewState["DBID"]%>&SBID=<%=ViewState["SBID"]%>" width="100%" height="100%"></iframe>
		</form>
	</body>
</HTML>

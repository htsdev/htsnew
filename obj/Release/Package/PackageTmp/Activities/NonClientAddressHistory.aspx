﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonClientAddressHistory.aspx.cs" Inherits="lntechNew.Activities.NonClientAddressHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Non Client History </title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
       
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
    </aspnew:ScriptManager>
    <table width="780px">
       <tr>
       <td background="../images/separator_repeat.gif" colspan="2" height="11">
              </td>
              </tr>
              <tr>
              <td>
              <asp:Label ID="lbl_LastName" runat="server" CssClass="Label"></asp:Label>,
              <asp:Label ID="lbl_FirstName" runat="server" CssClass="Label"></asp:Label>
              </td>
              </tr>
               <tr id = "tr_msg" runat="server" style="display:none;">
            <td style="height: 21px">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="False" CssClass="label"
                    ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
        <tr>
             <td class="clsLeftPaddingTable">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Address Modification History :" Width="170px"></asp:Label>
             </td>
           </tr>
        <tr>
            <td width = "100%" >
                <asp:GridView ID="gv_AddressUpdateHistory" Width="100%"  BorderColor="DarkGray" BorderStyle="Solid" HeaderStyle-HorizontalAlign="Center" 
                AllowPaging="False" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable" runat="server">
                    <Columns>
                        <asp:TemplateField HeaderText="Date & Time" >
                            <HeaderStyle BorderColor="DarkGray"  CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle BorderColor="DarkGray"  CssClass="GridItemStyleBig" Width="25%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" ID = "lbl_datetime" CssClass="Label" Text= '<%# Eval ("Insertdate", "{0:MM/dd/yyyy} @ {0:hh:mm:ss tt}") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User">
                            <HeaderStyle BorderColor="DarkGray"  CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle BorderColor="DarkGray"  CssClass="GridItemStyleBig" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" ID = "lbl_user"  CssClass="Label" Text= '<%# Eval("Lastname") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activity">
                            <HeaderStyle BorderColor="DarkGray"  CssClass="clsaspcolumnheader"></HeaderStyle>
                            <ItemStyle BorderColor="DarkGray"  CssClass="GridItemStyleBig" Width="65%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" ID = "lbl_activity"  CssClass="Label" Text= '<%# Eval("Activity") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                    
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" style="height: 40px">
                <asp:Button ID="btn_Close" runat="server" CssClass="clsbutton" Text="Close" OnClientClick="window.close();">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td background="../images/separator_repeat.gif" colspan="2" height="11">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

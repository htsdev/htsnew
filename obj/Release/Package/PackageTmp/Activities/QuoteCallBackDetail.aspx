<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.QuoteCallBackDetail"
    CodeBehind="QuoteCallBackDetail.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>QuoteCallBackDetail</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/cBoxes.js" type="text/javascript"></script>

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script>
    
    
    
			function ValidateControl()
			{			
	           
			    if(document.getElementById("CallBackDate").style.display !="none" )
			    {
			        dayofweek=weekdayName( document.getElementById("CallBackDate").value,true); 
			        //Sabir Khan 6981 11/12/2009 No need to display alert when no is selected as follow up...
			        if(document.getElementById("optFollowUp_0").checked == true)
			        {
			            if (dayofweek=="Sat" || dayofweek=="Sun")
			            {
			                alert("Please select any working day.");
			                document.getElementById("CallBackDate").focus();
			                return false;
			            }
			        }
			    }
			    
			    //Yasir Kamal 5744 04/06/2009 Quote Call back changes.
			     var cmbFollowUp = document.getElementById("cmbFollowUp").value;
			    
			     
			    if (document.getElementById("optFollowUp_1").checked == true)
			    {
			  
			        if(cmbFollowUp == 1 || cmbFollowUp ==5 || cmbFollowUp == 14)
			    {
			       alert("Please Select Follow Up Call");
			       document.getElementById("cmbFollowUp").focus();
			       return false;
			       }
			    }
			    
			    
	    var gComments=document.getElementById("WCC_GeneralComments_txt_Comments");
	    var comments = document.getElementById("WCC_GeneralComments");
	   
		  gComments.value=trim(gComments.value);
		 
		  if(gComments.value.length==0)
		  {
		     alert("Please enter General Comments.");
		     
		  if (gComments.offsetHeight ==0)
		  {
		 
		 var com =  comments.getElementsByTagName("a");

	
		    if(com[0].innerHTML=='Edit')
		    {
		    
		    com[0].innerHTML='Cancel';
		    }
            
	        gComments.style.display = 'block';
	   	
		  }
		     
		  if (gComments.offsetHeight > 0)
		   {
		    gComments.focus();
		   }
		    return false;
		    
		  }
			    			    
			
			// tahir 4982 10/16/2008 do not prompt for future date if follow-up = no.
			if (document.getElementById("optFollowUp_1").checked == false)
			{
			if (compareDates(document.getElementById("CallBackDate").value,'MM/dd/yyyy',document.Form1.txtCurrentDate.value,'MM/dd/yyyy')==false)
				{
					alert("Please enter valid future date for callback");
					document.getElementById("CallBackDate").focus();
					return false;
				}
			}

               // var callbackdate = document.Form1.txt_cb_Month.value + '/' + document.Form1.txt_cb_Day.value +'/' + document.Form1.txt_cb_Year.value;
//                var cb_mm = document.Form1.txt_cb_Month.value;
//                var cb_dd = document.Form1.txt_cb_Day.value;
//                var cb_yy = '20' +document.Form1.txt_cb_Year.value;
				//a;
//				if (callbackdate!="" && callbackdate!="//")
//				{
//					//if (isDate(callbackdate,'MM/dd/yy')==false)
//					if (chkdate(cb_mm+'/'+cb_dd+'/'+cb_yy)==false)
//					{
//						alert("Please enter valid Date format (mm/dd/yy)!");
//						document.Form1.txt_cb_Month.focus(); 
//						return false;
//					}
//					
//					if (compareDates(callbackdate,'MM/dd/yy',document.Form1.txtCurrentDate.value,'MM/dd/yyyy')==false)
//					{
//						alert("Please enter valid future date for callback");
//						document.Form1.txt_cb_Month.focus();
//						return false;
//					}
//					if(document.Form1.ddl_cb_time.value=='--')
//					{
//					    alert('Please select a valid callback time');
//					    document.Form1.ddl_cb_time.focus();
//					    return false;
//					}
//				}
//				else
//				{	//document.Form1.calQCallback.value= '01/01/1900'; 
//					
//				}
				
				//a;				
				//var AppDate = document.Form1.txt_App_Month.value + '/' + document.Form1.txt_App_Day.value +'/' + document.Form1.txt_App_Year.value;
				//var ap_mm = document.Form1.txt_App_Month.value;
                //var ap_dd = document.Form1.txt_App_Day.value;
                //var ap_yy = '20' +document.Form1.txt_App_Year.value;
                
//				if (AppDate!="" && AppDate!="//" )
//				{		
//					//alert("Please enter valid Date!");
//					//document.Form1.txtAppointmentDate.value= '01/01/1900'; 
//					//document.Form1.txtAppointmentDate.focus(); 
//					//return false;
//					
//					//if (isDate(AppDate,'MM/dd/yy')==false)
//					if (chkdate(ap_mm+'/'+ap_dd+'/'+ ap_yy)==false)
//					{	
//						alert("Please enter valid Date format (mm/dd/yy)!");
//						document.Form1.txt_App_Month.focus(); 
//						return false;
//					}	
//					
//					if (compareDates(AppDate,'MM/dd/yy',document.Form1.txtCurrentDate.value,'MM/dd/yyyy')==false)
//					{
//						alert("Please enter valid future date for Appointment");
//						document.Form1.txt_App_Month.focus(); 
//						return false;
//					}	
//					
////					if(document.Form1.ddl_ap_time.value=='--')
////					{
////					    alert('Please select a valid appointment time');
////					    document.Form1.ddl_ap_time.focus();
////					    return false;
////					}
//				}
//				else
//				{
					//document.Form1.calApptDate.value= '01/01/1900'; 
					return true;
			//	}				
			}	
			
			function BindDate()
			{
				
				
				if (document.Form1.dMonth.value!=1 && document.Form1.dDay.value!=0 && document.Form1.dYear.value!=0)				
				{
					if (Len(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value) == 1 )				
					{				
						strTemp= "0" + document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value;				
					}
					else
					{	strTemp = document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex-1].value;}
				
					if (Len(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) == 1 )				
					{				
						strTemp= strTemp + "/0" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;				
					}
					else
					{	strTemp = strTemp + "/" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;}								
				
					strTemp = strTemp + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value;
					document.Form1.txtCallBackDate.value = strTemp;
				}
				else
				{
					document.Form1.txtCallBackDate.value = '';
				}
												
				//*******************
				if (document.Form1.ApptMonth.value!=1 && document.Form1.ApptDay.value!=0 && document.Form1.ApptYear.value!=0)
				{
					if (Len(document.Form1.ApptMonth.options[document.Form1.ApptMonth.selectedIndex-1].value) == 1 )				
					{				
						strTemp= "0" + document.Form1.ApptMonth.options[document.Form1.ApptMonth.selectedIndex-1].value;				
					}
					else
					{	strTemp = document.Form1.ApptMonth.options[document.Form1.ApptMonth.selectedIndex-1].value;}
				
					if (Len(document.Form1.ApptDay.options[document.Form1.ApptDay.selectedIndex].value) == 1 )				
					{				
						strTemp= strTemp + "/0" + document.Form1.ApptDay.options[document.Form1.ApptDay.selectedIndex].value;				
					}
					else
					{	strTemp = strTemp + "/" + document.Form1.ApptDay.options[document.Form1.ApptDay.selectedIndex].value;}								
				
					strTemp = strTemp + "/" + document.Form1.ApptYear.options[document.Form1.ApptYear.selectedIndex].value;
				
					document.Form1.txtAppointmentDate.value = strTemp;				
				}				
				else
				{
					document.Form1.txtAppointmentDate.value = '';
				}
								
			}
			
			
		/*	function CloseWindow()
			{				
				if (document.getElementById("txtFlagUpdated").value==1)
				{
					//a;
					
					//window.opener.location.href= window.opener.location.href;
					//window.opener.location.href= 'http://localhost/lntechNet/QuoteCallBack/QuoteCallBackMain.aspx';
					window.opener.focus();							
					window.opener.location.href= 'QuoteCallBackMain.aspx';
					
					//window.opener.document.location.reload 
					//window.opener.document.Form1.txtCallBackFlag.value = 1;		
					//window.opener.document.Form1.cmbFlag.value = 1;																				
					window.close(); 
				}
			}*/
		
		
			function SplitDate()
			{
		       var time=new Date();
               var date=time.getDate();
               var year=time.getYear();
               if (year < 2000)    // Y2K Fix, Isaac Powell
               year = year + 1900; // http://onyx.idbsu.edu/~ipowell
		       
		       var d1 = new dateObj(document.Form1.dMonth, document.Form1.dDay, document.Form1.dYear);			   
			   
			   if ((document.Form1.txtCallBackDate.value != "") || (document.Form1.txtCallBackDate.value != (eval(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex].value) + "/" +  eval(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value)))
			   {			   		   
					var strDOB = new String(document.Form1.txtCallBackDate.value)
					var stringArray = strDOB.split("/"); 
					initDates(2002 ,  year, stringArray[2], stringArray[1], stringArray[0], d1);
			   }			   
			   var d2 = new dateObj(document.Form1.ApptMonth,document.Form1.ApptDay, document.Form1.ApptYear);
			   
			   if ((document.Form1.txtAppointmentDate.value != "") || (document.Form1.txtAppointmentDate.value != (document.Form1.ApptMonth.options[document.Form1.ApptMonth.selectedIndex].value + "/" +  document.Form1.ApptDay.options[document.Form1.ApptDay.selectedIndex].value + "/" + document.Form1.ApptYear.options[document.Form1.ApptYear.selectedIndex].value)))
			   {
					var strAppt = new String(document.Form1.txtAppointmentDate.value)
					var stringArray = strAppt .split("/"); 
					//initDates(1930, year, stringArray[2], stringArray[0], stringArray[1], d2);
					initDates(2002, year, stringArray[2], stringArray[1], stringArray[0], d2);	           
			   }			   
			   
			} 
			

			 function HideBox()
		     {																	        
				 element = document.getElementById('txtCurrentDate').style;
		         element.display='none';
		         element = document.getElementById('txtFlagUpdated').style;
		         element.display='none';
		         element = document.getElementById('txtCallBackDate').style;
		         element.display='none';
		         element = document.getElementById('txtAppointmentDate').style;
		         element.display='none';				
		         
		         element = document.getElementById('tdHide1').style;
		         element.display = 'none';						         		         
			 }
			 
			 function AutoCall()
			 {
				/*
				if (document.getElementById('chkAuotCallBack').checked==true)
				{					
					var dtCB = document.getElementById('txtCallBackDate').value;
					
					//var tmp = dtCB.substring(3,5);
					//tmp = tmp + "/" + dtCB.substring(0,2);
					//tmp = tmp + "/" + dtCB.substring(6,10);
					
					var dtCB= dateAdd('d',7, dtCB);
					var dtNew = new date(dtCB);
					
					var tmp = month(dtNew)
					tmp = tmp + "/" + dtCB;
					tmp = tmp + "/" + year(dtCB);
					
					document.getElementById('txtCallBackDate').value = dtCB;
					//SplitDate();										
					
				}
				*/
			 }

    </script>

</head>
<body onload="HideBox();" ms_positioning="GridLayout" bgcolor="#EFF4FB">
    <form id="Form1" method="post" runat="server">
    <table id="TableMain" class="clsLeftPaddingTable" style="z-index: 101; width: 646px;"
        cellspacing="0" cellpadding="0" align="left" border="0">
        <tr >
            <td class="clsLeftPaddingTable" style="width: 15px; height: 9px">
            </td>
            <td class="clsLeftPaddingTable" style="width: 477px; height: 9px">
            </td>
            <td class="clsLeftPaddingTable" style="height: 9px">
            </td>
        </tr>
        <tr>
            <td class="clsLeftPaddingTable" style="width: 15px; height: 100px;">
            </td>
            <td class="clsLeftPaddingTable" style="width: 477px; height: 100px;" valign="top"
                colspan="2">
                &nbsp;
                <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" class="clsLeftPaddingTable">
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 130px">
                            Name
                        </td>
                        <td class="clsLeftPaddingTable">
                            <asp:Label ID="lblName" runat="server" CssClass=".clsLabel"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 130px; height: 16px;">
                            Quote Amount
                        </td>
                        <td class="clsLeftPaddingTable" style="height: 16px">
                            <asp:Label ID="lblAmount" runat="server" CssClass=".clsLabel"></asp:Label>
                        </td>
                        <td style="height: 16px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 130px" valign="top">
                            Contact No.
                        </td>
                        <td class="clsLeftPaddingTable">
                            <asp:Label ID="lblContactNo" runat="server" CssClass=".clsLabel"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>                   
                </table>
               
            </td>
        </tr>
        <tr>
        <td class="clsLeftPaddingTable" style="width: 15px;">
            </td>
            <td  width="100%" colspan="4">
                <table id="vioDetail" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tbody>
                        <tr>
                            <td valign="top">
                            <asp:GridView ID="dgDetailInfo" runat="server" CssClass="clsLeftPaddingTable"
                          Width="100%" BorderColor="Gray"  BorderStyle="Solid" AutoGenerateColumns="False"
                                    AllowSorting="True">
                                    <Columns>
                                         <asp:TemplateField HeaderText="Cause Number">
                                            <HeaderStyle CssClass="clsaspcolumnheader" Width="9%"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CauseNo" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.CauseNumber") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField  HeaderText="Ticket Number">
                                            <HeaderStyle Width="9%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Labe66" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.RefCaseNumber") %>'>
                                                </asp:Label>
                                             </ItemTemplate>
                                        </asp:TemplateField>
                                      <asp:TemplateField  HeaderText="Matter Description">
                                            <HeaderStyle Width="13%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.violationDescription") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                       </asp:TemplateField>
                                         <asp:TemplateField  HeaderText="Crt Date" >
                                            <HeaderStyle Width="7%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_courtdate" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtDate","{0:d}") %>'></asp:Label>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField  HeaderText="Crt" >
                                            <HeaderStyle Width="3%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Crt" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.CourtNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField  HeaderText="Status" >
                                            <HeaderStyle Width="5%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container,"DataItem.AutoCourtDesc") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField  HeaderText="Crt Loc" >
                                            <HeaderStyle Width="8%" CssClass="clsaspcolumnheader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Loc" runat="server" CssClass="Label" Text='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    
                                    </asp:GridView>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="clsLeftPaddingTable" style="width: 15px; height: 370px;">
            </td>
            <td class="clsLeftPaddingTable" style="width: 477px; height: 370px;" colspan="2">
                <table id="Table2" cellspacing="0" cellpadding="0" border="0" style="width: 589px">
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 130px; display: none">
                            Original Quote Result
                        </td>
                        <td class="clsLeftPaddingTable" style="width: 339px; display: none;">
                            <asp:DropDownList ID="cmbOQR" runat="server" CssClass="clsInputCombo" Width="180px"
                                Enabled="False">
                            </asp:DropDownList>
                            <asp:Label ID="lblOQRDate" runat="server" CssClass="clsLeftPaddingTable" Width="92px"></asp:Label>
                        </td>
                        <td style="width: 142px">
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 160px; height: 28px">
                            &nbsp;Follow-Up Call
                        </td>
                        <td class="clsLeftPaddingTable" style="width: 339px; height: 28px">
                            <asp:DropDownList ID="cmbFollowUp" runat="server" CssClass="clsInputCombo" Width="180px">
                            </asp:DropDownList>
                            <asp:Label ID="lblFollowUpDate" runat="server" CssClass="clsLeftPaddingTable" Width="92px"></asp:Label>&nbsp;
                        </td>
                        <td style="height: 28px; width: 142px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 160px; height: 4px">
                            &nbsp;Follow-Up
                        </td>
                        <td class="clsLeftPaddingTable" style="width: 339px; height: 4px">
                            <asp:RadioButtonList ID="optFollowUp" runat="server" Width="104px" RepeatDirection="Horizontal"
                                Font-Names="Tahoma" Font-Size="8pt">
                            </asp:RadioButtonList>
                        </td>
                        <td style="height: 4px; width: 142px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 160px; height: 76px">
                            &nbsp;General Comments
                        </td>
                        <td class="clsLeftPaddingTable" style="width: 160px; height: 76px" colspan="2">
                            <cc2:WCtl_Comments ID="WCC_GeneralComments" runat="server" Height="100px" Width="373px">
                            </cc2:WCtl_Comments>
                        </td>
                    </tr>
                    <tr>
                        <td class="clssubhead" style="font-weight: bold; width: 160px; height: 23px">
                            &nbsp;Call Back Date
                        </td>
                        <td class="clsLeftPaddingTable" style="width: 339px; height: 23px">
                            &nbsp;<ew:CalendarPopup ID="CallBackDate" runat="server" AllowArbitraryText="False"
                                CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" DisableTextboxEntry="False"
                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True"
                                PadSingleDigits="True" ShowClearDate="True" ShowGoToToday="True" ToolTip="Call Back Date"
                                UpperBoundDate="12/31/9999 23:59:00" Width="80px">
                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TextboxLabelStyle CssClass="clstextarea" />
                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Gray" />
                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                    ForeColor="Black" />
                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    Font-Size="XX-Small" ForeColor="Black" />
                            </ew:CalendarPopup>
                        </td>
                        <td style="height: 23px; width: 142px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable">
                        </td>
                        <td class="clsLeftPaddingTable">
                            &nbsp;
                        </td>
                        <td style="height: 10px; width: 142px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" style="font-weight: bold; width: 160px; height: 11px">
                        </td>
                        <td class="clsLeftPaddingTable" style="width: 339px; height: 11px">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" Width="60px" Text="Update"
                                OnClick="btnSubmit_Click1"></asp:Button>
                        </td>
                        <td style="height: 11px; width: 142px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="clsLeftPaddingTable" style="font-weight: bold; width: 160px; height: 11px">
                            &nbsp;
                        </td>
                        <td class="clsLeftPaddingTable" id="tdHide1" style="width: 345px; height: 11px">
                            &nbsp;&nbsp;
                            <select class="clsInputCombo" id="dMonth" onblur="BindDate()" name="dMonth">
                            </select>
                            <select class="clsInputCombo" id="dDay" onblur="BindDate()" name="dDay">
                            </select>
                            <select class="clsInputCombo" id="dYear" onblur="BindDate()" name="dYear">
                            </select>
                            <asp:DropDownList ID="cmbCallBackTime" runat="server" CssClass="clsInputCombo" Width="70px">
                            </asp:DropDownList>
                            <select class="clsInputCombo" id="ApptMonth" onblur="BindDate()" name="ApptMonth">
                            </select>
                            <select class="clsInputCombo" id="ApptDay" onblur="BindDate()" name="ApptDay">
                            </select>
                            <select class="clsInputCombo" id="ApptYear" onblur="BindDate()" name="ApptYear">
                            </select>
                            <asp:DropDownList ID="cmbApptTime" runat="server" CssClass="clsInputCombo" Width="70px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtAppointmentDate" runat="server" CssClass="clstextarea" Width="5px"></asp:TextBox><asp:TextBox
                                ID="txtCallBackDate" runat="server" CssClass="clstextarea" Width="5px"></asp:TextBox><asp:TextBox
                                    ID="txtFlagUpdated" runat="server" Width="1px">0</asp:TextBox><asp:TextBox ID="txtCurrentDate"
                                        runat="server" Width="1px"></asp:TextBox>
                        </td>
                        <td style="height: 11px; width: 142px;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="100%" background="../Images/separator_repeat.gif" colspan="4" height="11">
            </td>
        </tr>
        <tr>
            <td class="clsLeftPaddingTable" style="width: 15px; height: 18px" align="center"
                colspan="3">
                &nbsp;
                <asp:Label ID="lblMessage" runat="server" Width="512px" Font-Names="Arial" Font-Size="X-Small"
                    ForeColor="Red" CssClass=".clsLabel"></asp:Label>
            </td>
        </tr>
    </table>
    </form>

    <script>
		
		/*		var time=new Date();
				var date=time.getDate();
				var year=time.getYear();
				if (year < 2000)    // Y2K Fix, Isaac Powell
					year = year + 1900; // http://onyx.idbsu.edu/~ipowell
				
				var d1 = new dateObj(document.Form1.dMonth,document.Form1.dDay, document.Form1.dYear);
				initDates(2002,year, '--','--','--', d1);
				
				var d2 = new dateObj(document.Form1.ApptMonth,document.Form1.ApptDay, document.Form1.ApptYear);				
				initDates(2002, year, '--','--','--', d2);
		*/
    </script>

</body>
</html>

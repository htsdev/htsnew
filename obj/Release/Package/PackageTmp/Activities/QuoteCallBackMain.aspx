﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.QuoteCallBackMain"
    MaintainScrollPositionOnPostback="true" CodeBehind="QuoteCallBackMain.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register Src="../WebControls/Navigation.ascx" TagName="Navigation" TagPrefix="uc6" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Quote CallBack</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #TableDates {
            width: 776px;
        }
    </style>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="HideBox();ShowDateBoxes();">

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script src="../Scripts/cBoxes.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

        function ShowHidePageNavigation() {
            //			var rec = document.getElementById("lblRecCount").innerText * 1;
            //			var tbl =  document.getElementById("tblPageNavigation").style;
            //					
            //			if (rec == '0')
            //				tbl.display = 'none';
            //			else
            //				tbl.display= 'block';

        }

        function SplitDate() {
            var time = new Date();
            var date = time.getDate();
            var year = time.getYear();
            if (year < 2000)    // Y2K Fix, Isaac Powell
                year = year + 1900; // http://onyx.idbsu.edu/~ipowell

            var d1 = new dateObj(document.Form1.dMonth, document.Form1.dDay, document.Form1.dYear);

            if ((document.Form1.txtFrom.value != "") || (document.Form1.txtFrom.value != (eval(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex].value) + "/" + eval(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value))) {
                var strDOB = new String(document.Form1.txtFrom.value)
                var stringArray = strDOB.split("/");
                initDates(2002, year, stringArray[2], stringArray[1], stringArray[0], d1);
            }
            var d2 = new dateObj(document.Form1.ToMonth, document.Form1.ToDay, document.Form1.ToYear);

            if ((document.Form1.txtTo.value != "") || (document.Form1.txtTo.value != (document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex].value + "/" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value + "/" + document.Form1.ToYear.options[document.Form1.ToYear.selectedIndex].value))) {
                var strAppt = new String(document.Form1.txtTo.value)
                var stringArray = strAppt.split("/");
                initDates(2002, year, stringArray[2], stringArray[1], stringArray[0], d2);
            }

        }

        function BindDate() {
            if (document.Form1.dMonth.value != 1 && document.Form1.dDay.value != 0 && document.Form1.dYear.value != 0) {
                if (Len(document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex - 1].value) == 1) {
                    strTemp = "0" + document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex - 1].value;
                }
                else { strTemp = document.Form1.dMonth.options[document.Form1.dMonth.selectedIndex - 1].value; }

                if (Len(document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value) == 1) {
                    strTemp = strTemp + "/0" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value;
                }
                else { strTemp = strTemp + "/" + document.Form1.dDay.options[document.Form1.dDay.selectedIndex].value; }

                strTemp = strTemp + "/" + document.Form1.dYear.options[document.Form1.dYear.selectedIndex].value;
                document.Form1.txtFrom.value = strTemp;
            }
            else {
                document.Form1.txtFrom.value = '';
            }

            //*******************
            if (document.Form1.ToMonth.value != 1 && document.Form1.ToDay.value != 0 && document.Form1.ToYear.value != 0) {
                if (Len(document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex - 1].value) == 1) {
                    strTemp = "0" + document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex - 1].value;
                }
                else { strTemp = document.Form1.ToMonth.options[document.Form1.ToMonth.selectedIndex - 1].value; }

                if (Len(document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value) == 1) {
                    strTemp = strTemp + "/0" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value;
                }
                else { strTemp = strTemp + "/" + document.Form1.ToDay.options[document.Form1.ToDay.selectedIndex].value; }

                strTemp = strTemp + "/" + document.Form1.ToYear.options[document.Form1.ToYear.selectedIndex].value;

                document.Form1.txtTo.value = strTemp;
            }
            else {
                document.Form1.txtTo.value = '';
            }
        }

        //Yasir Kamal 5744 04/06/2009 Quote Call back chages. 
        function PopUpCallBack(TicketID, QuoteID) {
            document.getElementById("txtCallBackFlag").value = 1;
            window.open("QuoteCallBackDetail.aspx?TicketID=" + TicketID + "&QuoteID=" + QuoteID, "QuoteCallBack", 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=yes,resizable=no,left=150,Top=100,height=520,width=685');
            return false;

        }

        function ShowViolationFee(TicketID) {
            var strURL = "//ClientInfo/violationsfees.asp?caseNumber=" & TicketID;
            window.navigate(strURL);
        }

        function GoBack() {
            window.navigate('../../Reports/BatchPrintSingle.asp');
        }

        function ShowDateBoxes() {

            element = document.getElementById('TableDates').style;
            if (document.Form1.rlstQuoteType[0].checked == true) {

                var date = '<%=ViewState["date"]%>';
                document.Form1.CallDateCalender_txtDate.value = date;

                element.display = 'block';
                element.margin = '1.5px 0px';

                element1 = document.getElementById('tdRpt1').style
                element1.display = 'block';
                element1.margin = '1.5px 0px';
                element1_1 = document.getElementById('td_days').style
                element1_1.display = 'none';
                element1_1.margin = '1.5px';
                element3 = document.getElementById('tdRpt3').style
                element3.display = 'none';
                element3.margin = '0px';

                element1_1 = document.getElementById('td_days').style
                element1_1.display = 'none';
                element1_1.margin = '1.5px';
            }

            else if (document.Form1.rlstQuoteType[1].checked == true) {
                var from = '<%=ViewState["from"]%>';
                var to = '<%=ViewState["to"]%>';

                document.Form1.calFrom1_txtDate.value = from;
                document.Form1.calTo1_txtDate.value = to;

                element.display = 'block';
                element.margin = '1.5px 0px';

                element1 = document.getElementById('tdRpt1').style
                element1.display = 'none';
                element1.margin = '1.5px';
                element1_1 = document.getElementById('td_days').style
                element1_1.display = 'block';
                element1_1.margin = '0px';
                element3 = document.getElementById('tdRpt3').style
                element3.display = 'none';
                element3.margin = '0px';

            }

            else {
                element.display = 'none';
                element.margin = '0px';
            }


    }


    function SearchValidation() {

        if (document.Form1.rlstQuoteType[0].checked == true) {
            if (document.Form1.calFrom1_txtDate.value == "") {
                //alert("Please enter valid Date!");
                $("#txtErrorMessage").text("Please enter valid Date!");
                $("#errorAlert").modal();
                document.Form1.calFrom1_txtDate.focus();
                return false;
            }

            if (isDate(document.Form1.calFrom1_txtDate.value, 'MM/dd/yyyy') == false) {
                // alert("Please enter valid Date format (mm/dd/yyyy)!");
                $("#txtErrorMessage").text("Please enter valid Date format (mm/dd/yyyy)!");
                $("#errorAlert").modal();
                document.Form1.calFrom1_txtDate.focus();
                return false;
            }
            if (document.Form1.calTo1_txtDate.value == "") {
                //alert("Please enter valid Date!");
                $("#txtErrorMessage").text("Please enter valid Date!");
                $("#errorAlert").modal();
                document.Form1.calTo1_txtDate.focus();
                return false;
            }

            if (isDate(document.Form1.calTo1_txtDate.value, 'MM/dd/yyyy') == false) {
                //alert("Please enter valid Date format (mm/dd/yyyy)!");
                $("#txtErrorMessage").text("Please enter valid Date format (mm/dd/yyyy)!");
                $("#errorAlert").modal();
                document.Form1.calTo1_txtDate.focus();
                return false;
            }

            if (IsDatesEqualOrGrater(document.Form1.calTo1_txtDate.value, 'MM/dd/yyyy', document.Form1.calFrom1_txtDate.value, 'MM/dd/yyyy') == false) {
                // alert("Please enter valid date, To-Date must be grater then or equal to From-Date");
                $("#txtErrorMessage").text("Please enter valid date, To-Date must be grater then or equal to From-Date");
                $("#errorAlert").modal();
                document.Form1.calTo1_txtDate.focus();
                return false;
            }
        }


    }

    function show(divid) {
        divid.style.display = 'block';
        divid.style.margin = '1.5px 0px';
    }

    function hide(divid) {
        divid.style.display = 'none';
        divid.style.margin = '0px';
    }

    function DefaultLoad() {

    }

    function HideBox() {
        element = document.getElementById('txtCallBackFlag').style;
        element.display = 'none';
        element = document.getElementById('txtFrom').style;
        element.display = 'none';
        element = document.getElementById('txtTo').style;
        element.display = 'none';
        element = document.getElementById('tdHide').style;
        element.display = 'none';
    }

    function RefreshGrid() {
        if (document.getElementById("txtCallBackFlag").value == 1) {
            document.getElementById("txtCallBackFlag").value = 0;
        }
    }
    function ShowBoxes() {

        var tdRpt1 = document.getElementById('tdRpt1').style;
        var td_days = document.getElementById('td_days').style;
        var tdRpt3 = document.getElementById('tdRpt3').style;
        if (document.Form1.rlstQuoteType[1].checked == true) {
            td_days.display = "block";
            tdRpt1.display = "none";
            tdRpt3.display = "none";
        }


    }

    // Fahad 6934 12/17/2009 Hide Quote Call Back control modal popup
    function HidePopup() {
        var modalPopupBehavior = $find('mpeQuoteCall');
        modalPopupBehavior.hide();
        //return false;
    }


    </script>

    <form id="Form1" method="post" runat="server">
        <aspnew:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
            </Scripts>
        </aspnew:ScriptManager>
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
            <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="Table1" style="">

            <div class="col-xs-12">
            <%--<asp:Label ID="lblMessage" CssClass="form-label" runat="server" ForeColor="Red"></asp:Label>--%>
            <%--<div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        <asp:Label ID="lblMessage" CssClass="form-label" runat="server"
                                                         ForeColor="Red"></asp:Label>
                </div>--%>
        </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Quote Call Back</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>


            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Quote Call Back</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <%--<label class="form-label">Date</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:RadioButtonList ID="rlstQuoteType" runat="server" Font-Names="Tahoma" Font-Size="6pt"
                                                CssClass="checkbox-custom" onclick="ShowDateBoxes();" Width="264px" DataTextField="QueryTypeDesc"
                                                DataValueField="QueryTypeID">
                                            </asp:RadioButtonList>

                                    </div>
                                                                </div>
                         </div>

                     <div class="col-md-8">
                                                            <div class="form-group">
                              
                                                                <%-- <asp:Label ID="Label6" runat="server" CssClass="form-label">From</asp:Label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    <%--<picker:datepicker id="calFrom1" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>

                                      <table id="TableDates" border="0" cellpadding="0" cellspacing="0" style="display: block;
                                                width: 432px" width="432">
                                                <tr id="trRpt1">
                                                    <td id="tdRpt1">
                                                        <asp:Label ID="Label1" runat="server" CssClass="form-label">From</asp:Label>
                                                        <picker:datepicker id="calFrom1" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                        <%--<ew:CalendarPopup ID="calFrom1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Court Date" Width="">
                                                            <TextboxLabelStyle CssClass="form-control inline-textbox" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>--%>
                                                        <asp:Label ID="Label7" runat="server" CssClass="form-label">To</asp:Label>

                                                         <picker:datepicker id="calTo1" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                      <%--  <ew:CalendarPopup ID="calTo1" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Court Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <TextboxLabelStyle CssClass="form-control inline-textbox" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        
                                                    </td>
                                                </tr>
                                                <tr id="tr_days">
                                                    <td id="td_days">
                                                        <asp:Label ID="Label12" runat="server" CssClass="form-label">Date</asp:Label>
                                                      <picker:datepicker id="CallDateCalender" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                          <%--<ew:CalendarPopup ID="CallDateCalender" runat="server" AllowArbitraryText="False"
                                                            CalendarLocation="Left" ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma"
                                                            Font-Size="8pt" ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True"
                                                            ShowClearDate="True" ShowGoToToday="True" ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TextboxLabelStyle CssClass="form-control inline-textbox" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                        </ew:CalendarPopup>--%>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td id="tdRpt3">
                                                        <asp:Label ID="Label8" runat="server" CssClass="form-label">From</asp:Label>
                                                        <picker:datepicker id="calFrom3" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                        <%--<ew:CalendarPopup ID="calFrom3" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <TextboxLabelStyle CssClass="form-control inline-textbox" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>--%>
                                                        
                                                        <asp:Label ID="Label9" runat="server" CssClass="form-label">To</asp:Label>
                                                        <picker:datepicker id="calTo3" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                        <%--<ew:CalendarPopup ID="calTo3" runat="server" AllowArbitraryText="False" CalendarLocation="Left"
                                                            ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                            ImageUrl="../images/calendar.gif" Nullable="True" PadSingleDigits="True" ShowClearDate="True"
                                                            ShowGoToToday="True" ToolTip="Call Back Date" UpperBoundDate="12/31/9999 23:59:00"
                                                            Width="80px">
                                                            <TextboxLabelStyle CssClass="form-control inline-textbox" />
                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                ForeColor="Black" />
                                                        </ew:CalendarPopup>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="tdHide">
                                                        <asp:Label ID="Label2" runat="server" CssClass="form-label">From</asp:Label>
                                                        <select id="dMonth" class="form-control" name="dMonth" onblur="BindDate()">
                                                        </select>
                                                        <select id="dDay" class="form-control" name="dDay" onblur="BindDate()">
                                                        </select>
                                                        <select id="dYear" class="form-control" name="dYear" onblur="BindDate()">
                                                        </select>
                                                        <asp:Label ID="Label5" runat="server" CssClass="form-label">To</asp:Label>
                                                        <select
                                                            id="ToMonth" class="form-control" name="ToMonth" onblur="BindDate()"></select><select
                                                                id="ToDay" class="form-control" name="ToDay" onblur="BindDate()"></select><select
                                                                    id="ToYear" class="form-control" name="ToYear" onblur="BindDate()"></select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control inline-textbox" ></asp:TextBox>
                                                        <asp:TextBox ID="txtTo" runat="server" CssClass="form-control inline-textbox" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>

                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                    

                      <div class="col-md-4">
                                                            <div class="form-group">
                                <asp:Label ID="lbl_case" runat="server" CssClass="form-label" Text="Case Type"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                 <asp:DropDownList ID="dd_casetype" runat="server" Width="" CssClass="form-control"
                                                        DataTextField="CaseTypeName" DataValueField="CaseTypeId">
                                                    </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>
                      <div class="col-md-4">
                                                            <div class="form-group">
                                <%--<asp:Label ID="Label3" runat="server" CssClass="form-label" Text="Case Type"></asp:Label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                 <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search"
                                                        OnClick="btnSearch_Click1" />

                                    </div>
                                                                </div>
                         </div>

                    
                    

                     


                    </div>
                     </div>
                </section>

            <div class="clearfix"></div>
 <aspnew:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
            
            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Quote Client Details</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                         <uc3:PagingControl ID="Pagingctrl" runat="server" />
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Date</label>--%>
                                <span class="desc"></span>
                                <div class="controls">

                                    <aspnew:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
                                                        DisplayAfter="2">
                                                        <ProgressTemplate>
                                                            <img src="../images/plzwait.gif" alt="" /><asp:Label ID="lbl1" runat="server" Text="Please Wait ......"
                                                                CssClass="form-label"></asp:Label>
                                                        </ProgressTemplate>
                                                    </aspnew:UpdateProgress>

                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Label ID="lblMessage" CssClass="form-label" runat="server" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                         <asp:GridView ID="gv_Quote" runat="server" AutoGenerateColumns="False" 
                                                        CssClass="table" AllowPaging="True" PageSize="20" OnRowCommand="gv_Quote_RowCommand"
                                                        OnPageIndexChanging="gv_Quote_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.No">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="sno" runat="server" NavigateUrl='<%# "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + Eval("ticketid_pk") %>'
                                                                        Text='<%# Eval("sno") %>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Customer" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <%--  <asp:HyperLink ID="lnkCustomer" runat="server" CssClass="GridItemStyle" NavigateUrl="#"
                                                    Text='<%# Eval("Customer") %>'>															
                                                </asp:HyperLink>--%>
                                                                    <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Customer") %>'
                                                                        CommandName="Click" CommandArgument='<%# Eval("TicketID_PK") %>'>									
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Contact" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatus" runat="server" CssClass="GridItemStyle" Text='<%# Eval("LastContactDate")+ "("+ Eval("Days") +")" %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Court Date" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" CssClass="GridItemStyle" Text='<%# Eval("CourtDate") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Crt.Loc" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_crt" runat="server" CssClass="GridItemStyle" Text='<%# Eval("crt") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Quote" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblQuote" runat="server" CssClass="GridItemStyle" Text='<%# Eval("Calculatedtotalfee") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Follow Up Status" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFollowUpStatus" runat="server" CssClass="GridItemStyle" Text='<%# Eval("FollowUpStatus") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Call Back Date" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle  HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCallbackDate" runat="server" CssClass="GridItemStyle" Text='<%# DataBinder.Eval(Container, "DataItem.CallBackDate", "{0:MM/dd/yyyy}") %>'>															
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                                    </asp:GridView>

                                    </div>
                                   

                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                </section>

            </ContentTemplate>
                                </aspnew:UpdatePanel>
           


           <div class="clearfix"></div>
          

                    <table>
                    <tr>
                            <td>
                                <aspnew:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <ajaxToolkit:ModalPopupExtender ID="mpeQuoteCall" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlControl" TargetControlID="Button1">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Button ID="Button1" runat="server" Style="display: none" />
                                        <asp:Panel ID="pnlControl" runat="server" Style="display: none;">
                                            <uc6:Navigation ID="Navigation1" runat="server" ControlTitle="Quote Call Client Details" />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </aspnew:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 760px" align="center" colspan="4">
                                <asp:TextBox ID="txtCallBackFlag" runat="server" Width="23px" CssClass="form-control inline-textbox"
                                    AutoPostBack="True">0</asp:TextBox>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 760px" align="center" colspan="4">
                                <asp:Label ID="lblRecCount" runat="server" CssClass="form-label"></asp:Label>
                            </td>
                        </tr>
                    </table>



























        </section>










            </section>
            </section>
        </div>






    </form>

    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <script language="javascript" type="text/javascript">

        var time = new Date();
        var date = time.getDate();
        var year = time.getYear();
        if (year < 2000)    // Y2K Fix, Isaac Powell
            year = year + 1900; // http://onyx.idbsu.edu/~ipowell
        //var d1 = new dateObj(document.Form1.dDay,document.Form1.dMonth, document.Form1.dYear);
        var d1 = new dateObj(document.Form1.dMonth, document.Form1.dDay, document.Form1.dYear);
        initDates(2002, year, '--', '--', '--', d1);

        var d2 = new dateObj(document.Form1.ToMonth, document.Form1.ToDay, document.Form1.ToYear);
        initDates(2002, year, '--', '--', '--', d2);

        ShowHidePageNavigation();

        document.getElementById("Navigation1_CallBackDate_div").style.zIndex = 100003;
        document.getElementById("Navigation1_CallBackDate_monthYear").style.zIndex = 100004;

    </script>

     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

</body>
</html>

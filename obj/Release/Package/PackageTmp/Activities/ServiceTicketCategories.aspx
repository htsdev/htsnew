<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceTicketCategories.aspx.cs"
    Inherits="HTP.Activities.ServiceTicketCategories" EnableEventValidation="false" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Service Ticket Categories</title>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->



    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">
    
    function ValidateAddCategory()
    {
        var txt = document.getElementById('txtNewCategory');
        var cblist = document.getElementById('Cblist_casetype');
        var ddlAdd = document.getElementById('ddlUserAdd');
        if(txt.value == '')
        {
            alert('Please enter some text for the new category');
            txt.focus();
            return(false);
        }
         if(ddlAdd.value == '0')
        {
            alert('Please select user to assign the new category');
            ddlAdd.focus();
            return(false);
        }
        
        
        if ((! document.getElementById("Cblist_casetype_0").checked) &&  (! document.getElementById("Cblist_casetype_1").checked) && (! document.getElementById("Cblist_casetype_2").checked)&& (! document.getElementById("Cblist_casetype_3").checked))
        {
            alert("Please select Case type for Category");
            return false;
        }
        // sadaf Aziz 10235 12/05/2012 validating Category name 
        
        var alpha = '^[a-zA-Z]+[a-zA-Z (\\)/`’”.]*$';
        if(!txt.value.match(alpha))
        {
        alert('invalid Characters');
        txt.focus();
        return(false);
       
      
               }
        
    }
    
    function ValidateUpdateCategory()
    {
        var txt = document.getElementById('txtCategoryUpdate');
         var ddlUpdate = document.getElementById('ddlUserUpdate');
        if(txt.value == '')
        {
            alert('Please enter some text for the new category');
            txt.focus();
            return(false);
        }
         if(ddlUpdate.value == '0')
        {
            alert('Please select any User to assign the new category');
            ddlUpdate.focus();
            return(false);
        }                
        if ((! document.getElementById("cblist_updatecasetype_0").checked) &&  (! document.getElementById("cblist_updatecasetype_1").checked) && (! document.getElementById("cblist_updatecasetype_2").checked)&& (! document.getElementById("cblist_updatecasetype_3").checked) )
        {
            alert("Please select Case type for Category");
            return false;
        }
    }
    
    </script>

</head>

<body class=" ">

    <form id="form1" runat="server">
    
        <!-- START CONTAINER -->
        <div class="page-container row-fluid container-fluid">

            <asp:Panel ID="pnl" runat="server">
                <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
            </asp:Panel>

            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>

                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">Service Ticket Categories</h1><!-- PAGE HEADING TAG - END -->                            </div>

                                                                            
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- MAIN CONTENT AREA STARTS -->
    
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="v_Add" runat="server" OnActivate="v_Add_Activate">

                            <div class="col-xs-12">
                                <section class="box ">
                            
                                    <div class="content-body">

                                        <div class="row">

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">New Category Name</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtNewCategory" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Open Email To</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddlUserAdd" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xs-12">

                                                <div class="form-group">
                                                    <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Case Type</label>
                                                <div class="controls">
                                                    <asp:CheckBoxList ID="Cblist_casetype" runat="server" RepeatColumns="7" DataTextField="CaseTypeName" DataValueField="CaseTypeId">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">&nbsp;</label>
                                                <div class="controls">
                                                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnAdd_Click" OnClientClick="return ValidateAddCategory()" Text="Add" />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    </div>
                                </section>
                            </div>

                            <div class="col-lg-12">
                                <section class="box ">
                                    <header class="panel_header">
                                        <h2 class="title pull-left">Service Ticket Categories</h2>
                                    </header>
                                    <div class="content-body">
                                        <div class="row">
                                            <div class="col-xs-12">

                                                <asp:GridView ID="gvServiceTicketCategories" runat="server" AutoGenerateColumns="False" CssClass="table" OnRowCommand="gvServiceTicketCategories_RowCommand" OnRowDeleting="gvServiceTicketCategories_RowDeleting" CellPadding="3" CellSpacing="0">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="SNo">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSno" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Category">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbtn_Desc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'
                                                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SNo") %>' CommandName="Select"></asp:LinkButton>
                                                                <asp:HiddenField ID="hf_CategoryID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ID") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Case Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_casetype" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.casetypename") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Open Email To">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label6" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.EmpName") %>'></asp:Label>
                                                                <asp:Label ID="lbl_AssignedUser" runat="server" Visible="false" CssClass="form-label"
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.AssignTo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/remove2.gif"
                                                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>' CommandName="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </asp:View>

                        <asp:View ID="v_Update" runat="server" OnActivate="v_Update_Activate">

                            <div class="col-xs-12">
                                <section class="box ">
                            
                                    <div class="content-body">

                                        <div class="row">
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="Label3" runat="server" CssClass="clssubhead" Text="Update Category"></asp:Label>:
                                                        <asp:Label ID="lblCategory" runat="server" CssClass="clsLabel"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">New Category Name</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtCategoryUpdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Open Email To</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddlUserUpdate" runat="server" CssClass="form-control m-bot15"></asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        

                                        <div class="row">

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Case Type</label>
                                                    <div class="controls">
                                                        <asp:CheckBoxList ID="cblist_updatecasetype" CssClass="clsLabel" runat="server" RepeatColumns="7"
                                                            DataTextField="CaseTypeName" DataValueField="CaseTypeId">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-sm-7 col-xs-8">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">&nbsp;</label>
                                                    <div class="controls">
                                                        <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnUpdate_Click" Text="Update" OnClientClick="return ValidateUpdateCategory()" />
                                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary m-bot15 pull-right" OnClick="btnCancel_Click" Text="Cancel" />
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </section>
                            </div>

                        </asp:View>

                    </asp:MultiView>
                    

                    <!-- MAIN CONTENT AREA ENDS -->
                </section>
            </section>
            <!-- END CONTENT -->
        
        </div>
        <!-- END CONTAINER -->

    </form>

    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


    <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 


</body>




<%--<body>
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" cellspacing="0" cellpadding="0" width="780px" align="center"
            border="0">
            <tr>
                <td>
                    <uc2:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="v_Add" runat="server" OnActivate="v_Add_Activate">
                            <table width="780px%">
                                <tr>
                                    <td background="../Images/separator_repeat.gif" height="9" width="750px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 103%; height: 100%">
                                            <tr style="background-color: #EFF4FB">
                                                <td align="center" style="width: 819px">
                                                    <table style="width: 622px">
                                                        <tr>
                                                            <td style="width: 234px;" colspan="3">
                                                                <asp:Label ID="Lbl_newcategory" runat="server" CssClass="clsSubhead" Text="New Category Name :"
                                                                    Width="123px"></asp:Label>
                                                            </td>
                                                            <td style="width: 239px">
                                                                &nbsp;
                                                                <asp:TextBox ID="txtNewCategory" runat="server" CssClass="clsinputadministration" MaxLength="50"
                                                                    Width="200px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 234px;" colspan="3">
                                                                <asp:Label ID="Label4" runat="server" CssClass="clsSubhead" Text="Open Email To:"
                                                                    Width="123px"></asp:Label>
                                                            </td>
                                                            <td style="width: 239px">
                                                                &nbsp;
                                                                <asp:DropDownList ID="ddlUserAdd" runat="server" Width="200px" CssClass="clsInputCombo">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 266px" align="left">
                                                                <asp:Button ID="btnAdd" runat="server" CssClass="clsbutton" OnClick="btnAdd_Click"
                                                                    OnClientClick="return ValidateAddCategory()" Text="Add" Width="60px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 234px;" colspan="3">
                                                                <asp:Label ID="lbl_casetype" runat="server" CssClass="clsSubhead" Text="Case Type :"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:CheckBoxList ID="Cblist_casetype" runat="server" RepeatColumns="7" DataTextField="CaseTypeName"
                                                                    DataValueField="CaseTypeId">
                                                                </asp:CheckBoxList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../Images/separator_repeat.gif" height="9" style="width: 819px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px;
                                                    width: 819px;">
                                                    &nbsp;Service Ticket Categories
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 819px">
                                                    <asp:Label ID="lbl_Message" runat="server" CssClass="clsLabel" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 819px">
                                                    <asp:GridView ID="gvServiceTicketCategories" runat="server" AutoGenerateColumns="False"
                                                        CssClass="clsLeftPaddingTable" OnRowCommand="gvServiceTicketCategories_RowCommand"
                                                        OnRowDeleting="gvServiceTicketCategories_RowDeleting" CellPadding="3" CellSpacing="0">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SNo">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSno" runat="server" CssClass="clssubhead" Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Category">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbtn_Desc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.SNo") %>' CommandName="Select"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hf_CategoryID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ID") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Case Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_casetype" runat="server" Width="100px" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.casetypename") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open Email To">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label6" runat="server" Width="100px" CssClass="clsLabel" Text='<%# DataBinder.Eval(Container, "DataItem.EmpName") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_AssignedUser" runat="server" Width="100px" Visible="false" CssClass="clsLabel"
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.AssignTo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/remove2.gif"
                                                                        CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") %>' CommandName="Delete" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <td align="center">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="v_Update" runat="server" OnActivate="v_Update_Activate">
                            <table width="100%" style="background-color: #EFF4FB; border-color: #EFF4FB;">
                                <tr>
                                    <td background="../Images/separator_repeat.gif" height="9">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="Label3" runat="server" CssClass="clssubhead" Text="Update Category"></asp:Label>:
                                        <asp:Label ID="lblCategory" runat="server" CssClass="clsLabel"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table>
                                            <tr>
                                                <td class="style1">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 117px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="clsSubhead" Text="New Category Name:"></asp:Label>
                                                </td>
                                                <td style="width: 213px">
                                                    &nbsp;
                                                    <asp:TextBox ID="txtCategoryUpdate" runat="server" CssClass="clsinputadministration"
                                                        Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 117px">
                                                    <asp:Label ID="Label5" runat="server" CssClass="clsSubhead" Text="Assigned To :"
                                                        Width="110px"></asp:Label>
                                                </td>
                                                <td style="width: 213px">
                                                    &nbsp;
                                                    <asp:DropDownList ID="ddlUserUpdate" runat="server" Width="200px" CssClass="clsInputCombo">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="clsbutton" OnClick="btnUpdate_Click"
                                                        Text="Update" OnClientClick="return ValidateUpdateCategory()" />
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="clsbutton" OnClick="btnCancel_Click"
                                                        Text="Cancel" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="clsSubhead" Text="Case Type :"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:CheckBoxList ID="cblist_updatecasetype" CssClass="clsLabel" runat="server" RepeatColumns="7"
                                                        DataTextField="CaseTypeName" DataValueField="CaseTypeId">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            <tr>
                <td background="../Images/separator_repeat.gif" height="11">
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:Footer ID="Footer1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>--%>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmManualCC.aspx.cs" Inherits="lntechNew.Activities.frmManualCC" %>

<%@ Register TagName="ActiveMenu" TagPrefix="uc4" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manual CC Processing</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />    
    <SCRIPT src="../Scripts/validatecreditcard.js" type="text/javascript"></SCRIPT>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    
    <script type="text/jscript"> 
     		
     var txtCardSatus=1;
     function GetFocus()
     {
        if(txtCardSatus !="0")
        {
         document.getElementById("txt_creditcard").focus();
        }
        return;
     }
     
     function VerifyCard()
        {	
            var carddata =document.getElementById("txt_creditcard").value;
            
            if(carddata !=null || carddata.length > 2)
            {
            
                var CardHolderName;
                var CardNo;
                var ExpDate;                         
                 var FirstName;
                var LastName;
                var temp; 
                var tIndex;                     
                
                CardHolderName =carddata.substring(carddata.indexOf("^")+1,carddata.lastIndexOf("^"));
                // swap first name with last name and replace "/" with empty space.
                tIndex = CardHolderName.indexOf("/");
                if (tIndex != 0 && tIndex != -1 )
                {
                    LastName = CardHolderName.substring(0, tIndex);
                    FirstName =  CardHolderName.substring(tIndex+1,CardHolderName.length);
                    CardHolderName =  trimAll(FirstName) +" "+ LastName;
                }
                document.getElementById("lbl_cardholdername").innerText =CardHolderName.toUpperCase(); 
                
                ExpDate =carddata.substring(carddata.lastIndexOf("^")+1,carddata.lastIndexOf("^")+5);            
                document.getElementById("lbl_expmonth").innerText =ExpDate.substring(2,4);
                document.getElementById("lbl_expyear").innerText =ExpDate.substring(0,2);            
                
                document.getElementById("lbl_cardno").innerText =carddata.substring(2,carddata.indexOf("^"));     
               
            }
        }
     
     function FillCreditInfo()
     {
        if(document.getElementById("lbl_cardholdername").innerText == "" || document.getElementById("lbl_expyear").length ==0)
        {
            //alert("Please swap the card.");
            $("#txtErrorMessage").text("Please swap the card.");
            $("#errorAlert").modal();
             document.getElementById("txt_creditcard").focus();  
            return false;
        }
        document.getElementById("txt_nameoncard").value =document.getElementById("lbl_cardholdername").innerText;
        document.getElementById("txt_ccnumber").value =document.getElementById("lbl_cardno").innerText;
        document.getElementById("ddl_month").value =document.getElementById("lbl_expmonth").innerText;
        document.getElementById("ddl_year").value =document.getElementById("lbl_expyear").innerText;
        closereaderpopup("0");
        
     }

    function ShowReaderPopup() 
	{
	    txtCardSatus="1";  
		   
		document.getElementById("PopupPanel").style.display = 'block';
        document.getElementById("PopupPanel").style.top = 455;			
	      
	    document.getElementById("DisableDive").style.height = document.body.offsetHeight * 2 ;
		document.getElementById("DisableDive").style.width = document.body.offsetWidth;
		document.getElementById("DisableDive").style.display = 'block';
		document.getElementById("lbl_cardholdername").innerText = "";
        document.getElementById("lbl_expmonth").innerText="";
        document.getElementById("lbl_cardno").innerText="";
        document.getElementById("lbl_expyear").innerText="";
        document.getElementById("txt_creditcard").innerText="";
		document.getElementById("txt_creditcard").focus();  
		return false;
	}
		
		function closereaderpopup(w)
		{
		  txtCardSatus=0;
		  document.getElementById("PopupPanel").style.display = 'none';
		  document.getElementById("DisableDive").style.display = 'none';
		  if(w=="1")
		  {
		    document.getElementById("txt_nameoncard").value="";
		    document.getElementById("txt_ccnumber").value ="";
		    document.getElementById("ddl_month").selectedIndex="0";
		    document.getElementById("ddl_year").selectedIndex="0";		   		 
		    txtCardSatus=0;
		   
		  }
		  if(w == "0")
		  {
		     document.getElementById("txt_ccnumber").focus();
		  }
		  document.getElementById("txt_creditcard").value="";
		  return false;
		}
    </script>
    
    
  <script language="javascript">
    function FillNameOnCard()
    {
      if((document.getElementById("txt_fname") !="") || (document.getElementById("txt_lname") !="" ))
      {
        var nameoncard =value=document.getElementById("txt_fname").value +" "+ document.getElementById("txt_lname").value;       
        document.getElementById("txt_nameoncard").value =nameoncard;              
      }
    }
    
    function FormValidation()
    {   
        var fname = document.getElementById("txt_fname").value;
        if(fname.length <= 0)
         {
            //alert("Please Enter First Name");
            $("#txtErrorMessage").text("Please Enter First Name");
            $("#errorAlert").modal();
            document.getElementById("txt_fname").focus();
            return false;
         }
       var lname =document.getElementById("txt_lname").value;
       if(lname.length <= 0)
       {
       //document.getElementById("txt_lname").style.background="yellow";
           //alert("Please Enter Last Name");              
           $("#txtErrorMessage").text("Please Enter Last Name");
           $("#errorAlert").modal();
       document.getElementById("txt_lname").focus();
        return false
       }
       var nameoncard =document.getElementById("txt_nameoncard").value;
       if(nameoncard.length <= 0)
       {
           // alert("Please Enter NameOnCard");
           $("#txtErrorMessage").text("Please Enter NameOnCard");
           $("#errorAlert").modal();
        document.getElementById("txt_nameoncard").focus();
        return false
       }
       ///////////// Credit card cheking ///////
     //  CheckManualCardNumber(form)
        var ccnumber =document.getElementById("txt_ccnumber").value;
       if(ccnumber.length <= 0)
       {
           //alert("Please Enter CC Number");
           $("#txtErrorMessage").text("Please Enter CC Number");
           $("#errorAlert").modal();
        document.getElementById("txt_ccnumber").focus();
        return false
       }  
       //////
//       var cin =document.getElementById("txt_cin").value;
//       if(cin.length <= 0)
//       {
//        alert("Please Enter CIN");
//        document.getElementById("txt_cin").focus();
//        return false
//       }
       var amount =document.getElementById("txt_amount").value;
       if(amount.length <= 0)
       {
           //alert("Please Enter Amount");
           $("#txtErrorMessage").text("Please Enter Amount");
           $("#errorAlert").modal();
        document.getElementById("txt_amount").focus();
        return false
       }
       var clienttype =document.getElementById("ddl_clienttype").value;
       if(clienttype <= 0)
       {
           // alert("Please Select ClienType")
           $("#txtErrorMessage").text("Please Select Client Type");
           $("#errorAlert").modal();
        document.getElementById("ddl_clienttype").focus();
        return false;
       }
       ///Checking Valid amount    
       if(isNaN(document.getElementById("txt_amount").value))
       {
           //  alert("This is not Valid amount");
           $("#txtErrorMessage").text("This is not Valid amount");
           $("#errorAlert").modal();
         document.getElementById("txt_amount").focus();
         return false;
       }                               
       
       //CALLING FUNCTION TO VALIDATE CARD NUMBER			   
	     if (CheckManualCardNumber(document.forms[0])==false)		   
		   return false; 	
     
//       var intamount=0;
//		intamount=document.getElementById("txt_amount").value;
//		if (isNaN(document.getElementById("txt_amount").value) == false && intamount!="" && intamount !=0  )
//		{
//		            alert ("Incorrect payment amount.");
//					document.getElementById("txt_amount").focus(); 
//					return false;							
//		}

        ///////// Checking Transaction mode////
        var doyou;
	    if(document.getElementById("hf_transactionmode").value =="1")
	     {
	        doyou =confirm("This transaction is running in test mode. Are you sure you want to process it further?");
	        if(doyou == false)
	            return false;
	                
	     }		
			     
        plzwait();        
        
    }
     function plzwait()
		{
		
		document.getElementById("tbl_plzwait").style.display = 'block';
		document.getElementById("allcreditcard").style.display = 'none';
		
		}
		
    function ClearControls()
    {
        document.getElementById("txt_fname").value="";
        document.getElementById("txt_lname").value="";            
        document.getElementById("txt_nameoncard").value="";
        document.getElementById("txt_ccnumber").value="";
        document.getElementById("txt_cin").value="";
        document.getElementById("txt_amount").value="";
        document.getElementById("txt_description").value="";
        document.getElementById("ddl_clienttype").value=0;        
        document.getElementById("ddl_month").value=01;                     
        //document.getElementById("ddl_year").value=07;
        return false;
    }
    
    //In order to select appropriate card by entering num		
		function SelectCard()
		{	
			var cardnum=document.getElementById("txt_ccnumber").value;
			var ddl_cardtype=document.getElementById("ddl_cardtype");
			
			if (cardnum.charAt(0)==4)
			{
				ddl_cardtype.value=	1			
			}	
			if (cardnum.charAt(0)==5)
			{
				ddl_cardtype.value=	2			
			}		
			if (cardnum.charAt(0)==3)
			{
				ddl_cardtype.value=	3			
			}	
			if (cardnum.charAt(0)==6)
			{
				ddl_cardtype.value=	4			
			}	
		}
//    function ResetColor(x)
//    {      
//      document.getElementById("txt_lname").style.background="white";
//      return false;
//    }
  </script>
    

    
    
</head>

<body>
    <form id="form1" runat="server">
        <div id="DisableDive" class="customPopup1" style="display :none;>
            <table width="100%" height="100%">                                                
            <tr>
                <td  style="width : 100%;height :100%" > 
                </td>
             </tr>
            </table>
        </div>
    
        <%--<asp:ScriptManager ID="" runat="server"/>--%>
        <asp35:ScriptManager ID="ScriptManager2" runat="server"></asp35:ScriptManager>
       
        <div class="page-container row-fluid container-fluid">
            <asp:Panel ID="pnl" runat="server">
                <uc4:ActiveMenu ID="ActiveMenu1" runat="server"></uc4:ActiveMenu>
            </asp:Panel>
            <section id="main-content" class="" id="Table2">
                <section class="wrapper main-wrapper row" id="" style="">

                    <div class="col-xs-12">
           
                        <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <asp:label id="lblMessage" CssClass="form-label" runat="server" Width="704px" Height="13px" ForeColor="Red"></asp:label>                              
                        </div>
                        <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <asp:Label ID="lbl_transmode" CssClass="form-label" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START --><h1 class="title">Manual CC</h1><!-- PAGE HEADING TAG - END -->                          

                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <asp35:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <section class="box" id="allcreditcard" style="">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Manual CC</h2>
                                    <div class="actions panel_actions pull-right">
                     
                                        <a class="box_toggle fa fa-chevron-down"></a>
                    
                                    </div>
                                </header>
                                
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-md-8">

                                        
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label">First Name</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txt_fname" runat="server" CssClass="form-control" MaxLength="100" TabIndex="2"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                                                    <div class="form-group">
                                                        <label class="form-label">Last Name</label>
                                                        <span class="desc"></span>
                                                        <div class="controls">
                                                                <asp:TextBox ID="txt_lname" runat="server" CssClass="form-control" MaxLength="50"
                                                                        TabIndex="3"></asp:TextBox>

                                                            </div>
                                                                                        </div>
                                                    </div>

                                                <div class="clearfix"></div>

                                            <div class="col-md-6">
                                                                                                <div class="form-group">
                                                                    <label class="form-label">Name On Card</label>
                                                                    <span class="desc"></span>
                                                                    <div class="controls">
                                                                        <asp:TextBox ID="txt_nameoncard" runat="server" CssClass="form-control"
                                                                                    onfocus="FillNameOnCard()" MaxLength="100" Width="" TabIndex="5"></asp:TextBox>

                                                                        </div>
                                                                                                    </div>
                                                                </div>
                                            <div class="col-md-5">
                                                                                    <div class="form-group">
                                                        <label class="form-label">CC Number</label>
                                                        <span class="desc"></span>
                                                        <div class="controls">
                                                            <asp:TextBox ID="txt_ccnumber" runat="server" onblur="SelectCard();" CssClass="form-control" MaxLength="16"
                                                                        Width="" TabIndex="6"></asp:TextBox>

                                                            </div>
                                                                                        </div>
                                                    </div>
                                            <div class="col-md-1">
                                                                                    <div class="form-group">
                                                        <%-- <label class="form-label">CC Number</label>--%>
                                                        <span class="desc"></span>
                                                        <div class="controls">
                                                            <asp:ImageButton ID="imgbtn_creditcard" runat="server" Height="21px" ImageUrl="~/Images/creditcard1.jpg"
                                                                                    Width="21px" TabIndex="4" />

                                                            </div>
                                                                                        </div>
                                                    </div>


                                            <div class="clearfix"></div>

                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                    <label class="form-label" >Expiry date</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                            <asp:DropDownList ID="ddl_month" runat="server" CssClass="form-control " Width="" TabIndex="7">
                                                                    <asp:ListItem Value="1">01</asp:ListItem>
                                                                    <asp:ListItem Value="2">02</asp:ListItem>
                                                                    <asp:ListItem Value="3">03</asp:ListItem>
                                                                    <asp:ListItem Value="4">04</asp:ListItem>
                                                                    <asp:ListItem Value="5">05</asp:ListItem>
                                                                    <asp:ListItem Value="6">06</asp:ListItem>
                                                                    <asp:ListItem Value="7">07</asp:ListItem>
                                                                    <asp:ListItem Value="8">08</asp:ListItem>
                                                                    <asp:ListItem Value="9">09</asp:ListItem>
                                                                    <asp:ListItem>10</asp:ListItem>
                                                                    <asp:ListItem>11</asp:ListItem>
                                                                    <asp:ListItem>12</asp:ListItem>
                                                                </asp:DropDownList>
                                    
                                            
                                                                <%--<asp:DropDownList ID="ddl_year" runat="server" CssClass="form-control " Width="" TabIndex="8">
                                                                </asp:DropDownList>--%>

                                                        </div>
                                                                                    </div>
                                                </div>
                    
                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                    <label class="form-label">Expiry date</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <label class="form-control inline-textbox">/</label>
                                                                <asp:DropDownList ID="ddl_year" runat="server" CssClass="form-control inline-textbox " Width="" TabIndex="8">
                                                                </asp:DropDownList>

                                                        </div>
                                                                                    </div>
                                                </div>

                                            <div class="col-md-3" style="display: none">
                                                                                <div class="form-group">
                                                    <label class="form-label">Card Type</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                            <asp:DropDownList ID="ddl_cardtype" runat="server" CssClass="form-control" Width="">
                                                                </asp:DropDownList>

                                                        </div>
                                                                                    </div>
                                                </div>

                                            <div class="clearfix"></div>

                    
                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                    <label class="form-label"> CIN</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txt_cin" runat="server" CssClass="form-control" MaxLength="4"
                                                                        Width="" TabIndex="9"></asp:TextBox>

                                                        </div>
                                                                                    </div>
                                                </div>

                                            <div class="col-md-6" >
                                                                                <div class="form-group">
                                                    <label class="form-label">Amount</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                            <asp:TextBox ID="txt_amount" runat="server" CssClass="form-control" MaxLength="5"
                                                                    Width="" TabIndex="10"></asp:TextBox>

                                                        </div>
                                                                                    </div>
                                                </div>

                                            <div class="clearfix"></div>

                    
                                            <div class="col-md-6" style="display: none">
                                                                                <div class="form-group">
                                                    <label class="form-label"> Card Type</label>
                                                    <span class="desc"></span>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control" Width="">
                                                            </asp:DropDownList>

                                                        </div>
                                                                                    </div>
                                                </div>

                                                
                                        
                                        </div>

                                        <div class="col-md-4">

                                        
                                        
                                        
                                        
                                        
                                            <div class="col-md-12">
                                                                                    <div class="form-group">
                                                        <label class="form-label">Client type</label>
                                                        <span class="desc"></span>
                                                        <div class="controls">
                                                                <asp:DropDownList ID="ddl_clienttype" runat="server" CssClass="form-control" Width="" TabIndex="11">
                                                                    <asp:ListItem Selected="True" Value="0">------------------ Select ----------------</asp:ListItem>
                                                                    <asp:ListItem Value="1">Traffic</asp:ListItem>
                                                                    <asp:ListItem Value="2">Oscar Client</asp:ListItem>
                                                                    <asp:ListItem Value="3">Lisa Client</asp:ListItem>
                                                                    <asp:ListItem Value="4">Oher</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>
                                                                                        </div>
                                                    </div>

                                            <div class="clearfix"></div>

                                                <div class="col-md-12" >
                                                                                    <div class="form-group">
                                                        <label class="form-label"> Description</label>
                                                        <span class="desc"></span>
                                                        <div class="controls">
                                     

                                                                <asp:TextBox ID="txt_description" runat="server" CssClass="form-control inline-textbox"
                                                                        Height="107px" MaxLength="500" TextMode="MultiLine"
                                                                        Width="100%" TabIndex="12"></asp:TextBox>

                                                            </div>
                                                                                        </div>
                                                    </div>

                                        </div>

                                            <div class="clearfix"></div>

                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                        <label class="form-label"> </label>
                                                        <span class="desc"></span>
                                                        <div class="controls">
                                     

                                                                <asp:Button ID="btn_submit" runat="server" CssClass="btn btn-primary" Text="Submit"
                                                                    Width="" OnClick="btn_submit_Click" TabIndex="13" />
                                                            <asp:Button ID="btn_reset" runat="server" CssClass="btn btn-primary"
                                                                        EnableViewState="False" Text="Reset" Width="" TabIndex="14" />

                                                            </div>
                                                                                        </div>
                                                    </div>


                                            </div>
                                                </div>
                                        </section>

                            <table id="tbl_plzwait" class="clssubhead" style="display: none; height: 60px" width="100%">
                                <tr>
                                    <td align="center" class="clssubhead" valign="middle">
                                        <img src="../Images/plzwait.gif" />
                                        &nbsp; Please wait while we process your request.
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp35:AsyncPostBackTrigger ControlID="btn_submit" EventName="Click" />
                        </Triggers>
                    </asp35:UpdatePanel>
                    <%--<asp35:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                            
                                
                        <%--</ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btn_submit" EventName="Click" />
                        </Triggers>
                    </asp35:UpdatePanel>--%>
                    
                    <div class="clearfix"></div>



                    <section class="box" id="" style="">
                     

                            <div class="content-body">
                        <div class="row">

                                <div class="col-md-12">
                                                                    <div class="form-group">
                                        <label class="form-label"></label>
                                        <span class="desc"></span>
                                        <div class="controls">
                                            <%--<uc2:Footer ID="Footer1" runat="server" />--%>

                                                
                                            </div>
                                                                        </div>
                                    </div>
                            </div>
                                </div>

                            </section>






<asp:HiddenField ID="hf_transactionmode" runat="server" />

<asp:Panel ID="PopupPanel" runat="server" Height="50px" Width="500px" style="position:absolute;top:0; left:0;display :none" CssClass="creditCardReaderPopup">

                                                    <section class="box" id="" style="">
                                <header class="panel_header">
                                <h2 class="title pull-left">Credit Card Reader</h2>
                                <div class="actions panel_actions pull-right">
                                <asp:LinkButton ID="lnkbtnpclose" runat="server" OnClientClick='return closereaderpopup("1");'>X</asp:LinkButton>
                                    <a class="box_toggle fa fa-chevron-down"></a>
                    
                        </div>
                    </header>

                            <div class="content-body">
                        <div class="row">

                                <div class="col-md-6">
                                                                    <div class="form-group">
                                        <label class="form-label"><strong> Card Holder Name: </strong></label>
                                        <span class="desc"></span>
                                        <div class="controls">
                                                <asp:Label ID="lbl_cardholdername" runat="server" Width="151px" CssClass="form-control inline-textbox" ></asp:Label>

                                            </div>
                                                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                            <div class="col-md-6">
                                                                    <div class="form-group">
                                        <label class="form-label"> <strong>Card Number: </strong></label>
                                        <span class="desc"></span>
                                        <div class="controls">
                                                <asp:Label ID="lbl_cardno" runat="server" Width="151px" CssClass="form-control inline-textbox"></asp:Label>

                                            </div>
                                                                        </div>
                                    </div>
                            <div class="clearfix"></div>
                                <div class="col-md-6">
                                                                    <div class="form-group">
                                        <label class="form-label"> <strong> Expiration Date:</strong> </label>
                                        <span class="desc"></span>
                                        <div class="controls">
                                                <strong> Month</strong>
                                                    <asp:Label ID="lbl_expmonth" runat="server" Width="" CssClass="form-control"></asp:Label>
                                                    <br>
                                                            <strong>Year</strong>
                                                            <asp:Label ID="lbl_expyear" runat="server" Width="" CssClass="form-control"></asp:Label>

                                            </div>
                                                                        </div>
                                    </div>

                            <div class="clearfix"></div>
                                <div class="col-md-6">
                                                                    <div class="form-group">
                                        <%--<label class="form-label"> <strong> Expiration Date:</strong> </label>--%>
                                        <span class="desc"></span>
                                        <div class="controls" id="">
                                            <table>
                                                <tr>
                                                    <td id="td_txtcreditcard" runat="server">
                                                        <asp:Button ID="btn_popupCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" Width="" />
                                                            <asp:Button ID="btn_popupok" runat="server" Text="Ok" CssClass="btn btn-primary" Width="" />
                                                            <asp:TextBox ID="txt_creditcard" runat="server" Height="1px" Width="1px" CssClass="form-control inline-textbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                       

                                            </div>
                                                                        </div>
                                    </div>





                            </div>
                                </div>
                                                        </section>
                                                <%--  <table id="tbl_telnos" cellpadding="0"  cellspacing="1"  style="width: 348px;background-color :White; border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;" border="0">
                                                    <tr >
                                                        <td class="clsLeftPaddingTable" colspan="2" style="height: 34px; background-image: url(../Images/subhead_bg.gif);" background="../images/headbar_headerextend.gif" valign="middle">
                                                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                <tr>
                                                                    <td class="clssubhead">
                                                                        <strong> Credit Card Reader </strong></td>
                                                                    <td align="right">
                                                                        &nbsp;<asp:LinkButton ID="lnkbtnpclose" runat="server" OnClientClick='return closereaderpopup("1");'>X</asp:LinkButton>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable">
                                                            <strong> Card Holder Name:</strong></td>
                                                        <td style="height: 20px" class="clsLeftPaddingTable">
                                                    <asp:Label ID="lbl_cardholdername" runat="server" Width="151px" CssClass="form-label"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable">
                                                            <strong> Card Number: </strong></td>
                                                        <td style="height: 24px" class="clsLeftPaddingTable">
                                                            <asp:Label ID="lbl_cardno" runat="server" Width="151px" CssClass="form-label"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 28px;" class="clsLeftPaddingTable">
                                                            <strong> Expiration Date:</strong></td>
                                                        <td class="clsLeftPaddingTable" style="width: 220px; height: 28px">
                                                            <strong> Month</strong>
                                                    <asp:Label ID="lbl_expmonth" runat="server" Width="47px" CssClass="form-label"></asp:Label>&nbsp;
                                                            <strong>Year</strong>
                                                            <asp:Label ID="lbl_expyear" runat="server" Width="47px" CssClass="form-label"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable">
                                                        </td>
                                                        <td id="td_txtcreditcard" runat="server" style="width: 200px; height: 36px; display:block" class="clsLeftPaddingTable" valign="bottom">
                                                            <asp:Button ID="btn_popupCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" Width="54px" />
                                                            <asp:Button ID="btn_popupok" runat="server" Text="Ok" CssClass="btn btn-primary" Width="54px" /><asp:TextBox ID="txt_creditcard" runat="server" Height="1px" Width="1px" CssClass="form-control inline-textbox"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="clsLeftPaddingTable">
                                                        </td>
                                                        <td class="clsLeftPaddingTable" style="width: 200px; height: 10px">
                                                </td>
                                                    </tr>
                                                </table>--%>
                                        </asp:Panel>





                    </section>
            </section>
        </div>
       

   
    </form>
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
      <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

﻿<%@ Page Language="c#" Inherits="HTP.Activities.frmClientLookup" CodeBehind="frmClientLookup.aspx.cs"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Taffic Alerts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script language="javascript">

	
	function DateValidation()
	{
	
	 var datDate1= Date.parse(document.getElementById('dtFrom').value);
     var datDate2= Date.parse(document.getElementById('dtTo').value);
     var dif=((datDate2-datDate1)/(24*60*60*1000))
     if(dif>31)
     {
         //alert('Please Enter date range of within 30 days');
         $("#txtErrorMessage").text("Please Enter date range of within 30 days");
         $("#errorAlert").modal();
     return(false);
     }
     if(dif<0)
     {
         // alert('Please Enter From date which is greater than to date');
         $("#txtErrorMessage").text("Please Enter From date which is greater than to date");
         $("#errorAlert").modal();
     return(false);
     }
     document.getElementById('lblMessage').innerText = '';
     document.getElementById('tbl_wait').style.display='block';
     document.getElementById('td_main').style.display='none';
     document.getElementById("tblClientGrid").style.display='none' ;
	 document.getElementById("tblPageNavigation").style.display='none';
	 if(document.getElementById("chkAll")!=null)
	 {
	    document.getElementById("chkAll").style.display='none';
	    document.getElementById("lblAll").style.display='none';
	 
	 }
	
	 
	 document.getElementById("tblNewInfoDetail").style.display='none';
	 document.getElementById("tblNewInfoDetailGrid").style.display='none';
	 document.getElementById("tblPrevInfoGrid").style.display='none';
	 document.getElementById('tblClientNavigation').style.display='none';
     
	return (true);
	}	
	
	function RefreshPage()
	{
	    if(document.getElementById('hf_Refresh').value=="1")
	    {
	        document.getElementById('hf_Refresh').value="0";
	        document.Form1.submit();
	    }
	    document.getElementById('td_main').style.display='block';
	    
	    if(document.getElementById('hf_rec_count').value=="0" /*|| document.getElementById('hf_Hide_flag').value=="1"*/)
	    {
	     ShowHidePageNavigation();
	     document.getElementById("tblNewInfoDetail").style.display = 'none' ;
		 document.getElementById("tblNewInfoDetailGrid").style.display = 'none'  ;
		 document.getElementById("tblPrevInfoGrid").style.display = 'none'  ;
	    }
	    else
	    {
	    document.getElementById('tblNewInfoDetail').style.display='block';
	     document.getElementById('tblPrevInfoGrid').style.display='block';
	     document.getElementById("tblPrevInfoGrid").style.display = 'block';
	    }
	}
	
	
		function SignUp()
		{  
			var caseNo = document.getElementById("txtNewCaseNo");
			var status = document.getElementById("ddlUpdateStatus");
			var status2 = document.getElementById("txtStatus");
			var profile = document.getElementById("hf_ProfileExists").value;
			
						
			/*if (status.selectedIndex == 0)
				{
				alert("Please first update the quote status.");
				status.focus();
				return false;
				}*/
				
				if(document.getElementById('chkNoContactList').checked==true)
				{
				   // alert('Sorry, No contact box selected. The client can not sign up, please use update button to move client to no contact group.');
				    $("#txtErrorMessage").text("Sorry, No contact box selected. The client can not sign up, please use update button to move client to no contact group.");
				    $("#errorAlert").modal();
				    return(false);
				}
				
				if(status.options[status.selectedIndex].text != status2.value)
				{
				   // alert("Sorry, The alert status was changed and the new status selected was not updated. Please update the status first.");
				    $("#txtErrorMessage").text("Sorry, The alert status was changed and the new status selected was not updated. Please update the status first.");
				    $("#errorAlert").modal();
				status.focus();
				return false;
				}
				
				var validate = DoValidation();
				if(validate == false)
				{
				return(false);
				}
				
/*			if (caseNo.value == '')
			{ 
				alert("To sign up the client for an FTA Ticket, please use case number hyperlink.");
				return false;
			}
*/			
		}
	
		function TogglePageNavigation()
		{
			var tbl =  document.getElementById("tblPageNavigation").style;
			
			if (tbl.display == 'block')
				tbl.display = 'none';
			else
				tbl.display = 'block';
		}

		function ShowHidePageNavigation()
		{ 
			var tbl =document.getElementById("tblPageNavigation").style;
			var	el  =document.getElementById("tblClientGrid").style;
			var	e2  =document.getElementById("tblNewInfoDetailGrid").style;
			var	e3  =document.getElementById("tblPrevInfoGrid").style;
			var	e4  =document.getElementById("tblNewInfoDetail").style;
			var	e5  =document.getElementById("tblClientNavigation").style ;
			var e6  =document.getElementById("tblClientNavigation").style ;
			if(document.getElementById ("chkAll")!=null)
			{
			    var chk = document.getElementById("chkAll").style ;
		        var lbl = document.getElementById("lblAll").style;
			}
			
	
			var lbl2 =  document.getElementById("lblPageSize").style;
			var ddl = document.getElementById("ddlPageSize").style;
					
			if (document.getElementById("txtRecCount").value == 0 ) 
				{
				tbl.display='none';		
				el.display ='none';
				document.Form1.imgClient.src = "../images/grdexpand.bmp";
				e2.display ='none';				
				document.Form1.imgNew.src  = "../images/grdexpand.bmp";
				e3.display ='none';
				e4.display ='none';
				e5.display = 'none';
				e6.display = 'none';
				if(document.getElementById ("chkAll")!=null)
				{
				    chk.display = 'none';	
				    lbl.display = 'none';
				}
				
				
				//lbl2.display = 'none';
				//ddl.display = 'none';			
				}
			else
				{
				if(document.getElementById ("chkAll")!=null)
				{
				if (document.getElementById("chkAll").checked == true)
					{
					tbl.display = 'none';
					//lbl2.display ='none';
					//ddl.display ='none';
					}
				else
					{
					tbl.display='block';	
					//lbl2.display ='block';
					//ddl.display ='block';
					}
				}	
				el.display = 'block';
				document.Form1.imgClient.src = "../images/grdcollapse.bmp";
				e2.display =  'block';
				document.Form1.imgNew.src  = "../images/grdcollapse.bmp";
				e3.display = 'block';
				e4.display ='block';
				e5.display = 'block';
				e6.display = 'block';	
				if(document.getElementById ("chkAll")!=null)
				{
				    chk.display = 'block';	
				    lbl.display = 'block';		
				}
															
				}
		}
		
		function ShowHideClientGrid()
		{
			var client = document.getElementById("tblClientGrid").style ;
			var tbl2   = document.getElementById("tblPageNavigation").style;
			if(document.getElementById ("chkAll")!=null)
			{
			    var chk = document.getElementById("chkAll");
			    var lbl = document.getElementById("lblAll").style;
			
			}
			

			if ( client.display == 'none' )
				{
				client.display = 'block';
				if(document.getElementById ("chkAll")!=null)
				{
				    if (chk.checked == false )
					    tbl2.display = 'block';
				}
				if (document.getElementById("txtRecCount").value != '0'  ) 
				{
					if(document.getElementById ("chkAll")!=null)
					{
					    chk.style.display = 'block';
					    lbl.display = 'block';
					}
					
				}
				document.Form1.imgClient.src = "../images/grdcollapse.bmp";
				}
			else
				{
				client.display = 'none';
				tbl2.display = 'none';
				if(document.getElementById ("chkAll")!=null)
			    {
				    chk.style.display = 'none';
				    lbl.display = 'none';
				}
					
				document.Form1.imgClient.src = "../images/grdexpand.bmp";
				}
				
		}


		function ShowHideNewInfo()
		{
			var elemnt1 = document.getElementById("tblNewInfoDetail").style ;
			var elemnt2 = document.getElementById("tblNewInfoDetailGrid").style ;
			var elemnt3 = document.getElementById("tblPrevInfoGrid").style ;

			if ( elemnt1.display == 'none' )
				{
				elemnt1.display = 'block';
				elemnt3.display = 'block';
				document.Form1.imgNew.src  = "../images/grdcollapse.bmp";
				}
			else
				{
				elemnt1.display = 'none';	
				elemnt3.display = 'none';	
				document.Form1.imgNew.src  = "../images/grdexpand.bmp";
				}
				
			if (elemnt2.display == 'none' )
				elemnt2.display = 'block';
			else
				elemnt2.display = 'none';	
				
				
		}

		function ShowHidePrevInfo()
		{
			var elemnt3 = document.getElementById("tblPrevInfoGrid").style ;


			if ( elemnt3.display == 'none' )
				{
				elemnt3.display = 'block';
				}
			else
				{
				elemnt3.display = 'none';		
				}
		}


		function DoValidation()
		{
			/*if ( document.Form1.ddlUpdateStatus.selectedIndex == 0 )
				{
				alert("Please select status.");
				document.Form1.ddlUpdateStatus.focus();
				return false;
				}*/
				
				var profile = document.getElementById("hf_ProfileExists").value;
			
			if(profile=="1")
			{
			    //alert('Sorry, Profile already exists for this record');
			    $("#txtErrorMessage").text("Sorry, Profile already exists for this record");
			    $("#errorAlert").modal();
			return(false);
			}
				
				var pending = '<% = ViewState["PendingStatusID"] %>';
				
				if(document.getElementById('ddlUpdateStatus').value == pending && document.getElementById('chkNoContactList').checked == true)
				{
				    // alert('Sorry, A record with pending status can not be sent to no contact list. Please select a valid status');
				    $("#txtErrorMessage").text("Sorry, A record with pending status can not be sent to no contact list. Please select a valid status");
				    $("#errorAlert").modal();
				    return(false);
				}
				
			var str = document.Form1.txtComments.value;
			if (str.length > 1980)
				{
			    //alert("Please insert only 2000 characters as comments!");
			    $("#txtErrorMessage").text("Please insert only 2000 characters as comments!");
			    $("#errorAlert").modal();
				document.Form1.txtComments.focus();
				return false;
				}
				
			var cdlY = document.getElementById("optCDLYes");
			var cdlN = document.getElementById("optCDLNo");
			
			if (cdlY.checked == false && cdlN.checked == false)
				{
			    //alert("Please select an option for CDL.");
			    $("#txtErrorMessage").text("Please select an option for CDL.");
			    $("#errorAlert").modal();
				cdlY.focus();
				return false;
				}
				
			var accY =  document.getElementById("optAccYes");
			var accN = document.getElementById("optAccNo");
			
			if (accY.checked == false && accN.checked== false)
			{
			    //alert("Please select an option for Accident.");
			    $("#txtErrorMessage").text("Please select an option for Accident.");
			    $("#errorAlert").modal();
				accY.focus();
				return false;
			}
			
			document.getElementById('tbl_plzwait1').style.display='block';
			document.getElementById('tblNewInfoDetail').style.display='none';

		}	
		


    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
    </aspnew:ScriptManager>
   <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" >
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>
                

                 </div>

                 <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lbl_Message" runat="server" CssClass="form-label"  ForeColor="Red"></asp:Label>

                 </div>
                 </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">  Traffic Alerts</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div> <%--(moiz check it--%>

               <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Uploaded Previous Client Report: Filters </h2>
                     <div class="actions panel_actions pull-right">

                         
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Upload Range:</label>
                                <span class="desc"></span>
                                <div class="controls">

                                    <%--<picker:datepicker id="dtFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>
                                    <asp:TextBox ID="txtfrom" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                   <%-- <ew:CalendarPopup ID="dtFrom" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                    AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                    PadSingleDigits="True" ToolTip="Select starting list date" Font-Size="8pt" ImageUrl="../images/calendar.gif">
                                                    <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
                                         </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>
                       <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">To:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <%--<picker:datepicker id="dtTo" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>
                                    <asp:TextBox ID="txtto" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                    <%--<ew:CalendarPopup ID="dtTo" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                    AllowArbitraryText="False" Culture="(Default)" UpperBoundDate="12/31/9999 23:59:00"
                                                    PadSingleDigits="True" ToolTip="Select ending list date" Font-Size="8pt" ImageUrl="../images/calendar.gif">
                                                    <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
                                                </ew:CalendarPopup>--%>


                                    </div>
                                                                </div>
                         </div>

                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Uploaded Case Status:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="cmbCaseType" runat="server" Width="" CssClass="form-control">
                                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                                    <asp:ListItem Value="2" Selected="True">ARRAIGNMENT OR BLANK</asp:ListItem>
                                                </asp:DropDownList>


                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Type:</label>
                                <span class="desc"></span>
                                <div class="controls">

                                   <asp:DropDownList ID="cmbClientType" runat="server" Width="" Height="" CssClass="form-control">
                                                    <%--tahir  7028 11/19/2009--%> 
                                                    <%--<asp:ListItem Value="2">ALL</asp:ListItem>--%>
                                                    <asp:ListItem Value="1" Selected="True">PREVIOUS CLIENT</asp:ListItem>
                                                    <%--tahir  7028 11/19/2009--%> 
                                                    <%--<asp:ListItem Value="0">PREVIOUS QUOTE</asp:ListItem>--%>
                                                </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>

                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Alert Status:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="cmbPCCR" runat="server" Width="" CssClass="form-control">
                                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                                    <asp:ListItem Value="1" Selected="True">NO ACTION</asp:ListItem>
                                                </asp:DropDownList>


                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                    
                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Court Location:</label>
                                <span class="desc"></span>
                                <div class="controls">

                                    <asp:DropDownList ID="cmbCourtType" runat="server" Width="" CssClass="form-control">
                                                    <asp:ListItem Value="0" Selected="True">ALL</asp:ListItem>
                                                    <asp:ListItem Value="1">INSIDE COURTS</asp:ListItem>
                                                    <asp:ListItem Value="2">OUTSIDE COURTS</asp:ListItem>
                                                </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>

                       <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Only Future Court Dates:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                     <asp:RadioButton ID="optFutureYes" runat="server" CssClass="checkbox-custom" GroupName="FutureCourtDate"
                                                                Text="Yes" Width=""></asp:RadioButton>
                                    <asp:RadioButton ID="optFutureNo" runat="server" CssClass="checkbox-custom" GroupName="FutureCourtDate"
                                                                Text="No" Checked="True" Width=""></asp:RadioButton>

                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    

                                    </div>
                                                                </div>
                         </div>
                    
                    <div class="clearfix"></div>
                     <div class="col-md-12">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                   <asp:ImageButton ID="btnSubmit" runat="server" CssClass="btn btn-primary"
                                                                AlternateText="Search" OnClientClick="return DateValidation()"></asp:ImageButton>
                                      <asp:ImageButton ID="btn_Reset" runat="server" AlternateText="Reset" CssClass="btn btn-primary">  
                                                            </asp:ImageButton>

                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                   </section>
             <div class="clearfix"></div>

 <aspnew:UpdatePanel ID="pnl_MainGrid" runat="server" UpdateMode="Conditional" Visible="true">
                        <ContentTemplate>

                            <table id="tbl_wait" width="100%" style="display: none">
                                                <tr>
                                                    <td class="clssubhead" valign="middle" align="center">
                                                        <img src="../Images/plzwait.gif" />
                                                        Please wait while your request is being processed..
                                                    </td>
                                                </tr>
                                            </table>

                            <section class="box" id="tblClientList" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Client List: </h2>
                     <div class="actions panel_actions pull-right">

                          <img id="imgClient" onclick="ShowHideClientGrid();" src="../images/grdcollapse.bmp"
                                                                        name="imgClient">
                     
                          <asp:Label ID="lblRecCount" runat="server" Width="160px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                        ForeColor="White" Visible="False"></asp:Label>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12" id="tblClientGrid">
                                                            <div class="form-group">
                              <label class="form-label"> </label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <div class="table-responsive" data-pattern="priority-columns">
                                      <asp:DataGrid ID="dgClient" runat="server" Width="" Font-Names="Verdana" Font-Size="2px" CssClass="table table-small-font table-bordered table-striped"
                                                            AllowSorting="True" AutoGenerateColumns="False" PageSize="30" AllowPaging="True">
                                                            <SelectedItemStyle BackColor="#FFFFCC"></SelectedItemStyle>
                                                            <ItemStyle BackColor="White"></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="grdheader" VerticalAlign="Middle">
                                                            </HeaderStyle>
                                                            <FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                            </FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="S. No">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        &nbsp;
                                                                        <asp:LinkButton ID="btnSerial" runat="server" CommandName="GetDetails"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hf_TicketNumber" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TicketNumber") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="ClientName" HeaderText="Client Name">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="language" HeaderText="Language">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:TemplateColumn SortExpression="listdate" HeaderText="Upload Date">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label6" runat="server" CssClass="grdlabel" Text='<%# DataBinder.Eval(Container, "DataItem.listdate", "{0:MM/dd/yyyy}") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="midnumber" HeaderText="MID Number">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="courtdate" HeaderText="Court Date" DataFormatString="{0:MM/dd/yyyy}"
                                                                    SortExpression="courtdate">
                                                                    <HeaderStyle CssClass="TaskGrid_Head" HorizontalAlign="Left"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TicketNumber" HeaderText="TicketNumber" Visible="False">
                                                                </asp:BoundColumn>
                                                                 <asp:BoundColumn DataField="StatusDescription" HeaderText="Alert Status">
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                            <PagerStyle VerticalAlign="Middle" Visible="False" NextPageText="Next" Font-Size="XX-Small"
                                                                PrevPageText="Previous" HorizontalAlign="Right" ForeColor="Black" BackColor="#999999">
                                                            </PagerStyle>
                                                        </asp:DataGrid>

                                         </div>
                                     <asp:HiddenField ID="hf_Rec_count" runat="server" Value="0" />
                                                        <asp:HiddenField ID="hf_ProfileExists" runat="server" Value="0" />
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                                  <table id="tblPageNavigation" cellspacing="0" cellpadding="0" width="100%" align="right"
                                                            border="0">
                                                            <tr>
                                                                <td align="right">
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPageSize" runat="server" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White">Rec/Page</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlPageSize" runat="server" CssClass="frmtd" Font-Size="Smaller"
                                                                            AutoPostBack="True">
                                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                                            <asp:ListItem Value="15">15</asp:ListItem>
                                                                            <asp:ListItem Value="20">20</asp:ListItem>
                                                                            <asp:ListItem Value="25">25</asp:ListItem>
                                                                            <asp:ListItem Value="30" Selected="True">30</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCurrPage" runat="server" Width="88px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White" Visible="False">Current Page :</asp:Label>&nbsp;
                                                                        <asp:Label ID="lblPNo" runat="server" Width="30px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White" Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblGoto" runat="server" Width="16px" Font-Names="Verdana" Font-Size="8.5pt"
                                                                            ForeColor="White" Visible="False">Goto</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cmbPageNo" runat="server" CssClass="frmtd" Font-Size="Smaller"
                                                                            Visible="False" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </td>
                                                            </tr>
                                                        </table>
                             <asp:CheckBox ID="chkAll" runat="server" Font-Names="Verdana" Font-Size="8.5pt" ForeColor="White"
                                                            AutoPostBack="True" Visible="False"></asp:CheckBox>
                             <asp:Label ID="lblAll" runat="server" Font-Names="Verdana" Font-Size="8.5pt" ForeColor="White"
                                                            Visible="False">ALL</asp:Label>
                                </section>



                          

                            <%--</ContentTemplate>
     </aspnew:UpdatePanel>--%>




             <div class="clearfix"></div>

             <aspnew:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
               <section class="box" id="tblNewTckInfo" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">  Alert Status:</h2>
                     <div class="actions panel_actions pull-right">

                         <img id="imgNew" onclick="ShowHideNewInfo();" src="../images/grdcollapse.bmp" name="imgNew">
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Alert Status:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                     <script type="text/javascript" language="javascript">
                                         var prm = Sys.WebForms.PageRequestManager.getInstance();
                                         prm.add_pageLoaded(pageLoaded);
                                         prm.add_beginRequest(beginRequest);
                                         var postbackElement;

                                         function beginRequest(sender, args) {
                                             postbackElement = args.get_postBackElement();
                                         }

                                         function pageLoaded(sender, args) {
                                             var updatedPanels = args.get_panelsUpdated();
                                             if (typeof (postbackElement) == 'undefined') {
                                                 return;
                                             }
                                             RefreshPage();
                                             navigateToRecord();

                                         }
                                                                </script>

                                                                <table id="tbl_plzwait1" style="display: none" width="800px">
                                                                    <tr>
                                                                        <td class="clssubhead" valign="middle" align="center">
                                                                            <img src="../Images/plzwait.gif" />
                                                                            Please wait while your request is being processed..
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:DropDownList ID="ddlUpdateStatus" runat="server" Width="" CssClass="form-control">
                                                                                            <asp:ListItem Value="1">NO ACTION</asp:ListItem>
                                                                                        </asp:DropDownList>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>
                       <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Name:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    
                                                               <asp:Label ID="lblName" runat="server" Width="" Height="" CssClass="form-control"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>
                      <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Language:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    
                                                               <asp:Label ID="lblLanguage" runat="server" Width="120px" Height="16" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>



                     <div class="clearfix"></div>

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">CDL:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    
                                                               <asp:RadioButton ID="optCDLYes" runat="server" Width="50px" Height="16px" CssClass="checkbox-custom"
                                                                                            GroupName="cdl" Text="Yes"></asp:RadioButton><asp:RadioButton ID="optCDLNo" runat="server"
                                                                                                Width="50px" Height="16px" CssClass="checkbox-custom" GroupName="cdl" Text="No">
                                                                                        </asp:RadioButton>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>

                          <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Accident:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    
                                                             <asp:RadioButton ID="optAccYes" runat="server" Width="50px" Height="16px" CssClass="checkbox-custom"
                                                                                            GroupName="ACC" Text="Yes"></asp:RadioButton><asp:RadioButton ID="optAccNo" runat="server"
                                                                                                Width="50px" Height="16px" CssClass="checkbox-custom" GroupName="ACC" Text="No">
                                                                                        </asp:RadioButton>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>

                          <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Estimated Fee:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    
                                                               <asp:Label ID="lblFee" runat="server" Width="88px" Height="16px" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>


                    <div class="clearfix"></div>

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Contact 1:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblContact1" runat="server" Width="" Height="" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>

                                         <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Contact 2:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblContact2" runat="server" Width="" Height="" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>


                                         <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Contact 3:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblContact3" runat="server" Width="" Height="" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>


                     <div class="clearfix"></div>

                      <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Contact 4:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblContact4" runat="server" Width="" Height="" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>

                                         <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Contact 5:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblContact5" runat="server" Width="" Height="" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>


                                         <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Contact 6:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:Label ID="lblContact6" runat="server" Width="" Height="" CssClass="form-label"></asp:Label>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>


                     <div class="col-md-12">
                                                            <div class="form-group">
                              <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                     <table id="tblComments" style="border-collapse: collapse" cellspacing="0" cellpadding="0"
                                                                                rules="all" width="100%" border="1">
                                                                                <tr>
                                                                                    <td valign="top" width="100%" height="120">
                                                                                        <asp:TextBox ID="txtComments" runat="server" Width="100%" Height="120px" TextMode="MultiLine"
                                                                                            Rows="10" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                     <table style="border-collapse: collapse; display: none" cellspacing="0" cellpadding="0"
                                                                                rules="all" width="100%" border="1">
                                                                                <tr>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkPriceShopping" runat="server" CssClass="frmtd" Text="Price Shopping">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkHiredAnother" runat="server" CssClass="frmtd" Text="Hired Another">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkHandleMySelf" runat="server" CssClass="frmtd" Text="Handle Myself">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkPaidTickets" runat="server" CssClass="frmtd" Text="Paid Ticket">
                                                                                        </asp:CheckBox>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkDSCProb" runat="server" CssClass="frmtd" Text="DSC / Prob">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkNoMoney" runat="server" CssClass="frmtd" Text="No Money"></asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkTooExpensive" runat="server" CssClass="frmtd" Text="Too Expensive">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                    <td height="20">
                                                                                        &nbsp;
                                                                                        <asp:CheckBox ID="chkPoorService" runat="server" CssClass="frmtd" Text="Poor Service">
                                                                                        </asp:CheckBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <asp:HiddenField ID="hf_UpdateFlag" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hf_Refresh" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hf_Hide_flag" runat="server" Value="0" />

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

  <div class="col-md-4">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Contact 6:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:CheckBox ID="chkNoContactList" runat="server" Width="" Height="" CssClass="checkbox-custom"
                                                                                Text="No Contact List"></asp:CheckBox>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Contact 6:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                   
                                   
                                    <asp:ImageButton ID="btnSignUp" runat="server" CssClass="btn btn-primary" Text="Change Status"></asp:ImageButton>
                                    <asp:ImageButton
                                                                                ID="btnUpdateComments" runat="server" ImageUrl="" CssClass="btn btn-primary" AlternateText="Update">
                                                                            </asp:ImageButton>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>
                         <div class="col-md-4" style="display: none; visibility: hidden">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Contact 6:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:TextBox ID="txtRecCount" runat="server" Width="15px" Height="15px"></asp:TextBox><asp:TextBox
                                                                                ID="txtNewCaseNo" runat="server"></asp:TextBox><asp:TextBox ID="txtStatus" runat="server"></asp:TextBox><asp:TextBox
                                                                                    ID="txtFTATicket" runat="server"></asp:TextBox>

                                                               



                                                               


                                    </div>
                                                                </div>
                         </div>





                    </div>
                     </div>


                     <table id="tblClientNavigation" cellspacing="0" cellpadding="0" width="130" align="right"
                                                                        border="0">
                                                                        <tr>
                                                                            <td class="TDHeading" valign="middle" align="right">
                                                                                <asp:LinkButton ID="btnPrevClient" runat="server" Font-Names="Verdana" Font-Size="8.5pt"
                                                                                    ForeColor="White"> Previous </asp:LinkButton>|
                                                                            </td>
                                                                            <td class="TDHeading" valign="middle" align="left">
                                                                                <asp:LinkButton ID="btnNextClient" runat="server" Width="32px" Font-Names="Verdana"
                                                                                    Font-Size="8.5pt" ForeColor="White">Next</asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>


                   </section>


 </ContentTemplate>
                                                            <Triggers>
                                                                <aspnew:AsyncPostBackTrigger ControlID="btnUpdateComments" EventName="Click" />
                                                            </Triggers>
                                                        </aspnew:UpdatePanel>

             <div class="clearfix"></div>

            
               <section class="box" id="" style="display:none">
                     <header class="panel_header" >
                     <h2 class="title pull-left" >  
                            <table >
                                                                        <tr >
                                                                            <td id="td_title_hired" runat="server" >
                          NON-HIRED CASES
                                                                                </td>
                                                                            </tr>
                                </table>
                         </h2>
                     <div class="actions panel_actions pull-right">

                         
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                           <%--   <label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls">

                                    <table id="tblNewInfoDetailGrid" style="display: block" cellspacing="0" cellpadding="0"
                                                            width="100%" border="0">
                                                            <tr>
                                                                <td id="td_title" class="producthead" style="height: 20px" runat="server" visible="false">
                                                                    NON-HIRED CASES
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:DataGrid ID="dgViolNew" runat="server" Width="795px" Font-Names="Verdana" Font-Size="2px"
                                                                        AutoGenerateColumns="False">
                                                                        <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                        </HeaderStyle>
                                                                        <FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                        </FooterStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderText="Case Number">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:HyperLink ID="lnkNewCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNo") %>'>
                                                                                    </asp:HyperLink>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="ViolDesc" HeaderText="Violation Description">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="250px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CaseStatus" HeaderText="Case Status">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="135px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CourtName" HeaderText="Court Name">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CourtDate" HeaderText="Court Date" DataFormatString="{0:d}">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="RecordId">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recordid") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.recordid") %>'>
                                                                                    </asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="Midnum">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblMidNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="address">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="zip">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zip") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="CourtDate" DataFormatString="{0:t}" HeaderText="Court Time">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CourtNumber" HeaderText="Court Room">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="FineAmount" HeaderText="Fine Amount" DataFormatString="{0:C}">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle VerticalAlign="Middle" NextPageText="Next Page" Font-Size="XX-Small"
                                                                            PrevPageText="Previous Page" HorizontalAlign="Right" ForeColor="Black" BackColor="#999999">
                                                                        </PagerStyle>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                   </section>

            <div class="clearfix"></div>



            
               <section class="box" id="tblPrevInfoHeading" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">  HIRED CASES</h2>
                     <div class="actions panel_actions pull-right">

                        
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                           <%--   <label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls" id="tblPrevInfoGrid">
                                      <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:DataGrid ID="dgViolPrevious" runat="server" Width="" Font-Names="Verdana" CssClass="table table-small-font table-bordered table-striped"
                                                                                    Font-Size="2px" AutoGenerateColumns="False" PageSize="5" ShowFooter="True" OnItemDataBound="dgViolPrevious_ItemDataBound">
                                                                                    <FooterStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                                    </FooterStyle>
                                                                                    <ItemStyle BackColor="White"></ItemStyle>
                                                                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                                                    </HeaderStyle>
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderText="Case Number">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnPrevCaseNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CaseNo") %>'
                                                                                                    CommandName="cmdPrevCaseNo">
                                                                                                </asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="ViolDesc" HeaderText="Violation Description">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="250px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CaseStatus" HeaderText="Case Status">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="135px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:TemplateColumn HeaderText="Viol Outcome">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="120px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="btnViolOutcome" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolOutCome") %>'
                                                                                                    CommandName="cmdViolOutcome">
                                                                                                </asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="TextBox8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ViolOutCome") %>'>
                                                                                                </asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="CourtName" HeaderText="Court Name">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CourtDate" HeaderText="Court Date" DataFormatString="{0:d}">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CourtDate" DataFormatString="{0:t}" HeaderText="Court Time">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="75px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="CourtNumber" HeaderText="Court Room">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="50px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="AmountPaid" HeaderText="Amount Paid">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="Firm" HeaderText="Firm">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="80px" CssClass="GrdHeader"></HeaderStyle>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="grdlabel" VerticalAlign="Middle"></ItemStyle>
                                                                                        </asp:BoundColumn>
                                                                                        <asp:TemplateColumn Visible="False" HeaderText="ticketid">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblTicketId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketid") %>'
                                                                                                    Visible="False" />
                                                                                                <asp:Label ID="lbl_CDLFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CDLFlag") %>'
                                                                                                    Visible="False" />
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="TextBox9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketit") %>'>
                                                                                                </asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn>
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="ibtn_GC" runat="server" ImageUrl="~/Images/note04.gif" OnClientClick="return(false)"
                                                                                                    ToolTip='<%# DataBinder.Eval(Container, "DataItem.Generalcomments") %>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                    </Columns>
                                                                                    <PagerStyle VerticalAlign="Middle" NextPageText="Next Page" Font-Size="XX-Small"
                                                                                        PrevPageText="Previous Page" HorizontalAlign="Right" ForeColor="Black" BackColor="#999999">
                                                                                    </PagerStyle>
                                                                                </asp:DataGrid>
                                          </div>


                                    <script language="javascript"> ShowHidePageNavigation();  </script>
                                    <asp:HiddenField ID="hf_FocusFlag" runat="server" Value="0" />

                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                   </section>


            </ContentTemplate>
                        <Triggers>
                            <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                        </Triggers>
                    </aspnew:UpdatePanel>


            </section>
                     </section>
       </div>

    <script type="text/javascript">
        function navigateToRecord()
        {
			var flag_focus = document.getElementById('hf_FocusFlag').value;
			
			if(flag_focus=="1")
			{
			window.location="#details";
			}
	    }
			
    </script>

    </form>
      <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

   
   
    
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });
</script>
</body>
</html>

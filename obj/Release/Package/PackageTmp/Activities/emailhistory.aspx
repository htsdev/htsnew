﻿<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.emailhistory" CodeBehind="emailhistory.aspx.cs" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Text Message</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />


    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->



    <style type="text/css">
        .style1
        {
            width: 139px;
        }
    </style>

    <script type="text/javascript">
    
    //Yasir Kamal 6064 07/01/2009 do not allow to search for past dates.
    
        function validate()
		{
		    var strCalFrom =  document.getElementById("calFrom").value;
		    var strCalEnd =  document.getElementById("calTo").value;
		    var dd_sendsmsto =  document.getElementById("dd_sendsmsto").value;
		    var sdate = Date.parse(strCalFrom);
		    var edate = Date.parse(strCalEnd);
		    today = new Date(); 
            var curr = Date.parse(today.toDateString());
             
	        if (sdate > curr || edate > curr)
            {
	            //alert("Please select today's or past date");
	            $("#txtErrorMessage").text("Please select today's or past date");
	            $("#errorAlert").modal();
            return false;
            }
            
            // Noufil 5884 07/06/2009 Javascript message added.
            if (dd_sendsmsto == "-1")
            {
                // alert("Please select message category.");
                $("#txtErrorMessage").text("Please select message category.");
                $("#errorAlert").modal();
                return false;
            }
		}
		    
		    // Noufil 5884 07/07/2009 Check Selected Records
		    function CheckSelectedRecords()
		    {
		        var checkdate = "0";
		        var gv = document.getElementById("gvresult");
		        document.getElementById("hf_isRecordCheck").value = "";
                var gridRowCount = gv.rows.length;
                for (var i=1; i < gridRowCount; i++)
                {
                    if (gv.rows[i].cells[0].innerText.trim() != "Next >   Last Page >>")
                    {
                        if (parseInt(gv.rows[i].cells[6].childNodes.length) >1 && parseInt(gv.rows[i].cells[6].childNodes.length)>0)
                        {   
                            if ((gv.rows[i].cells[6].all[1].type== "checkbox") && (gv.rows[i].cells[6].all[1].checked))
                            {
                                if (document.getElementById("hf_isRecordCheck").value == "")
                                    document.getElementById("hf_isRecordCheck").value = i +",";
                                else
                                    document.getElementById("hf_isRecordCheck").value += i +",";
                            }
                        }
                    }
                }
                if (document.getElementById("hf_isRecordCheck").value == "")
                {
                    // alert("Please select records to resend.");
                    $("#txtErrorMessage").text("Please select records to resend.");
                    $("#errorAlert").modal();
                    return false;
                }   
                else 
                    return true;
		        
		    }
    </script>

</head>

<body>

    <form id="Form1" method="post" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="upnlResult" runat="server">
        <ContentTemplate>

             <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="Table2">
        <section class="wrapper main-wrapper row" id="" style="">

               <div class="col-xs-12">
           
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <asp:Label ID="lbl_error" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                </div>
                 

                 
        </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Text Message</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>

             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Message Sent Date</h2>
                     <div class="actions panel_actions pull-right">
                        
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>


                                     <div class="content-body">
                <div class="row">


                      <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">From</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="calFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>


                                    </div>
                                                                </div>
                         </div>
                      <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">To</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <picker:datepicker id="calTo" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>


                                    </div>
                                                                </div>
                         </div>

                      <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Message Category :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                             <asp:DropDownList ID="dd_sendsmsto" runat="server" CssClass="form-control" Width="">
                                                    <asp:ListItem Selected="True" Text="--- Choose ---" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Sent to Attorney" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Sent to Client" Value="1"></asp:ListItem>
                                                </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>
                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">First Name:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <asp:Button ID="btn_submit" runat="server" OnClientClick="return validate();" CssClass="btn btn-primary"
                                                    Text="Search" OnClick="btn_submit_Click" />

                                    </div>
                                                                </div>
                         </div>

                    </div>
                                         </div>
                 </section>
             <div class="clearfix"></div>



<section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">History</h2>
                     <div class="actions panel_actions pull-right">
                        
                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="nexttelemail.aspx?sMenu=26" CssClass="form-label pull-right">Global Settings</asp:HyperLink>
                         <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>


                                     <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">First Name:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp35:UpdateProgress ID="UpdateProgress1" runat="server">
                                       <ProgressTemplate>
                                           <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                       </ProgressTemplate>
                                   </asp35:UpdateProgress>
                                    <%--<asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="upnlResult">
                                        <ProgressTemplate>
                                            <img alt="" src="../images/plzwait.gif" />&nbsp;<asp:Label ID="lbl1" runat="server"
                                                CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>--%>
                                    <div class="table-responsive" data-pattern="priority-columns">

                                     <asp:GridView ID="gvresult" runat="server" BorderColor="White" CssClass="table table-small-font table-bordered table-striped"
                                        Width="100%" AutoGenerateColumns="False" BorderStyle="None" ShowFooter="True"
                                        CellPadding="0" OnRowCommand="gvresult_RowCommand" AllowPaging="True" OnPageIndexChanging="gvresult_PageIndexChanging"
                                        PageSize="50">
                                        <PagerStyle HorizontalAlign="Center" />
                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="S.No">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SNo" runat="server" Text='<%# Eval("SNo") %>' CssClass="form-label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader" Width="100px">
                                                </HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_emailto" runat="server" CssClass="form-label" Text='<%# Eval("emailto") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Subject">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_client" runat="server" CssClass="form-label" Text='<%# Eval("client") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Information">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_subject" runat="server" CssClass="form-label" Text='<%# Eval("subject") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Send">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_dates" runat="server" CssClass="form-label" Text='<%# Eval("dates") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Resend By">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="clsaspcolumnheader"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Resend" runat="server" CssClass="form-label" Text='<%# Eval("resendby") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" Font-Bold="False"
                                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False">
                                                </HeaderStyle>
                                                <HeaderTemplate>
                                                    <asp:Button ID="btn_update" runat="server" CssClass="clsbutton" Text="Resend" CommandName="resend"
                                                        OnClientClick="return CheckSelectedRecords();"></asp:Button>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbox_resend" runat="server" CssClass="form-label"></asp:CheckBox>
                                                    <asp:Label ID="lbl_id" runat="server" Text='<%# Eval("id") %>' Visible="False">
                                                    </asp:Label>
                                                    <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("ticketid") %>' />
                                                    <asp:HiddenField ID="hf_phonenumber" runat="server" Value='<%# Eval("PhoneNumber") %>' />
                                                    <asp:HiddenField ID="hf_TicketNumber" runat="server" Value='<%# Eval("TicketNumber") %>' />
                                                    <asp:HiddenField ID="hf_CourtDate" runat="server" Value='<%# Eval("CourtDate") %>' />
                                                    <asp:HiddenField ID="hf_CourtNumber" runat="server" Value='<%# Eval("CourtNumber") %>' />
                                                    <asp:HiddenField ID="hf_Client" runat="server" Value='<%# Eval("Clientname") %>' />
                                                    <asp:HiddenField ID="hf_courtname" runat="server" Value='<%# Eval("shortname") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>


                                        </div>





                                    </div>
                                                                </div>
                         </div>
                    </div>
                                         </div>
    </section>



             <div class="clearfix"></div>


            <section class="box" id="" style="display:none">
                     <header class="panel_header">
                     <h2 class="title pull-left"></h2>
                     <div class="actions panel_actions pull-right">
                        
                   
                    
                </div>
            </header>


                                     <div class="content-body">
                <div class="row">



                     <table id="Table2" style="height: 24px; display: none" cellspacing="1" cellpadding="1"
                                        width="100%" border="0">
                                        <tr>
                                            <td width="5%" style="height: 22px">
                                                <asp:Label ID="Label1" runat="server" CssClass="clsaspcolumnheader">S.No</asp:Label>
                                            </td>
                                            <td width="12%" style="height: 22px">
                                                <asp:Label ID="Label2" runat="server" CssClass="clsaspcolumnheader">To</asp:Label>
                                            </td>
                                            <td width="17%" style="height: 22px">
                                                <asp:Label ID="Label3" runat="server" CssClass="clsaspcolumnheader">Client</asp:Label>
                                            </td>
                                            <td width="33%" style="height: 22px">
                                                <asp:Label ID="Label5" runat="server" CssClass="clsaspcolumnheader">Information</asp:Label>
                                            </td>
                                            <td width="20%" style="height: 22px">
                                                <asp:Label ID="Label6" runat="server" CssClass="clsaspcolumnheader">Send</asp:Label>
                                            </td>
                                            <td align="right" width="15%" style="height: 22px">
                                                <asp:Button ID="btn_send" runat="server" CssClass="clsbutton" Text="Resend" CommandName="resend">
                                                </asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                     <asp:HiddenField ID="hf_isRecordCheck" runat="server" Value="" />
                    </div>
                                         </div>
                </section>








            </section>
                     </section>
                 </div>




         
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>


     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>

     <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

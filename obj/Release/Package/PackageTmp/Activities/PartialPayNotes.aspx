﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="HTP.Activities.PartialPayNotes" Codebehind="PartialPayNotes.aspx.cs" %>
<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Partial Pay Notes</title>
		<%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

        <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />

        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />


		<script src="../Scripts/jsDate.js" type="text/javascript"></script>
		<script>
		function Validate()
		{
		
			var date =document.getElementById("txt_PayDueDate").value;		
			var today;
			var expiry;
			if (!MMDDYYYYDate(date))
			{				
				return false;
			}
			else
			{				
						today = new Date();
						expiry = new Date(date);
						if (today.getTime() >= expiry.getTime())
						{
						alert("Please select future Date");
						return false;					
						}
			}
			
						
		}
	   
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			
						<TABLE class="clsLeftPaddingTable" id="tblsub" class="table" height="20" cellSpacing="1" cellPadding="0">
							<TR>
							</TR>
							<tr>
								<td style="width: 116px">
                                    Name:
								</td>
								<TD><asp:label id="lbl_name" runat="server" CssClass="form-label"></asp:label></TD>
							</tr>
							<tr>
								<td style="width: 116px">
                                    Telephone:</td>
								<TD height="49" style="HEIGHT: 49px">
									<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD><asp:label id="lbl_telephone1" runat="server" CssClass="form-label"></asp:label></TD>
										</TR>
										<TR>
											<TD>
												<asp:label id="lbl_telephone2" runat="server" CssClass="form-label"></asp:label></TD>
										</TR>
										<TR>
											<TD>
												<asp:label id="lbl_telephone3" runat="server" CssClass="form-label"></asp:label></TD>
										</TR>
									</TABLE>
									&nbsp;
								</TD>
							</tr>
							<tr>
								<td style="width: 116px">
                                    Appearance:</td>
								<TD><asp:label id="lbl_appdate" runat="server" CssClass="form-label"></asp:label></TD>
							</tr>
							<tr>
								<td style="width:116px">Owed Amount($):</td>
								<TD>
									<asp:label id="lbl_oweamount" runat="server" CssClass="form-label"></asp:label></TD>
							</tr>
							<TR>
								<TD style="width: 116px">Due&nbsp;Date:</TD>
								<TD>
									<%--<ew:calendarpopup id="PayDueDate" runat="server" DisableTextboxEntry="False" ToolTip="Select Report Date Range"
										PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00" Culture="(Default)" AllowArbitraryText="False"
										ShowGoToToday="True" CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Nullable="True">
										<TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>										
									</ew:calendarpopup>--%>
                                    <asp:TextBox ID="txt_PayDueDate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
								</TD>
							</TR>
							<TR>
								<TD style="width: 116px">
									<P>
                                        General Comments:&nbsp;</P>
								</TD>
								<TD>
                                    <cc2:wctl_comments id="WCC_GeneralComments" runat="server" width="300px"></cc2:wctl_comments>
                                </TD>
							</TR>
							<tr>
								<TD style="width: 116px"></TD>
								<TD ><asp:button id="btn_submit" runat="server" CssClass="btn btn-primary" Text="Submit"></asp:button></TD>
							</tr>
						</TABLE>
					
		</form>
        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
        <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        <script type="text/javascript">
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });
        </script>
	</body>
</HTML>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoNotMail.aspx.cs" Inherits="HTP.Reports.DoNotMail" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Do Not Mail Flag Update</title>
    <link href="../Styles.css" type="text/css" rel="stylesheet" />

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script type="text/javascript">
    
function ClearForm()
{
    document.getElementById('Tabs_pnl_ByDescription_txt_FirstName').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_LastName').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_Address').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_ZipCode').value='';
    document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').value='';
    document.getElementById('Tabs_pnl_ByTicketID_txt_TicketID').value='';
    document.getElementById('Tabs_pnl_ByDescription_txt_City').value='';
    document.getElementById('Tabs_pnl_ByDescription_ddl_States').value=0;
    document.getElementById('lbl_Message').innerText='';
    document.getElementById('Tabs_pnl_ByAddress_txtAddress').value='';
    document.getElementById('Tabs_pnl_ByAddress_txtCity').value='';
    document.getElementById('Tabs_pnl_ByAddress_txtZipCode').value='';
    document.getElementById('Tabs_pnl_ByAddress_ddlStates').value=0;

    var grid = document.getElementById('gv')
    if (grid != null)
        grid.style.display ='none';
    return(false);
}

// Abbas Qamar 9726 11-03-2011 Validation only Address and zipcode for Address Tab.
function ValidateAddressForm(){
	
    var var2 = document.getElementById('Tabs_pnl_ByAddress_txtAddress');
    var var4 = document.getElementById('Tabs_pnl_ByAddress_txtZipCode');

    if(var2.value == '')
    {
        //alert('Please enter an Address')
        $("#txtErrorMessage").text("Please enter an Address");
        $("#errorAlert").modal();
        var2.focus();
        return(false);
    }

    if(var4.value == '')
    {
        //alert('Please enter zip code');
        $("#txtErrorMessage").text("Please enter zip code");
        $("#errorAlert").modal();
        var4.focus();
        return(false);
    }

    return(true);
}

function ValidateForm(){

    var var2 = document.getElementById('Tabs_pnl_ByDescription_txt_LastName');
    var var3 = document.getElementById('Tabs_pnl_ByDescription_txt_Address');
    var var4 = document.getElementById('Tabs_pnl_ByDescription_txt_ZipCode');

	if(var2.value == '')
	{
	    // alert('Please enter Last Name');
	    $("#txtErrorMessage").text("Please enter Last Name");
	    $("#errorAlert").modal();
        var2.focus();
        return(false);
    }

    if(var3.value == '')
    {
        // alert('Please enter an address');
        $("#txtErrorMessage").text("Please enter an address");
        $("#errorAlert").modal();
        var3.focus();
        return(false);
    }

    if(var4.value == '')
    {
        // alert('Please enter zip code');
        $("#txtErrorMessage").text("Please enter zip code");
        $("#errorAlert").modal();
        var4.focus();
        return(false);
    }

    return(true);
}

function ValidateLetterID() {

	if(document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').value=='')
    {
	    //alert('Please enter a letter id');
	    $("#txtErrorMessage").text("Please enter a letter id");
	    $("#errorAlert").modal();
        document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').focus();
        return(false);
    }

    var letterid=document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').value;
    if(isNaN(letterid)==true)
    {
        //   alert('Please enter correct letter id');
        $("#txtErrorMessage").text("Please enter correct letter id");
        $("#errorAlert").modal();
        document.getElementById('Tabs_pnl_ByLetterID_txt_LetterID').focus();
        return(false);
    }
}

function ValidateTicketID()
{
/*
var ddl = document.getElementById('ddl_DB');

if(ddl.value == '-1')
{
    alert('Please select a database');
    ddl.focus();
    return(false);
}

if(ddl.value == '1')
{
    alert('You can not update a record by \'Ticket Number\' when \'Criminal Client\' is selected');
    ddl.focus();
    return(false);
}
*/
    if(document.getElementById('Tabs_pnl_ByTicketID_txt_TicketID').value=='')
    {
        // alert('Please enter a ticket id');
        $("#txtErrorMessage").text("Please enter a ticket id");
        $("#errorAlert").modal();
        document.getElementById('Tabs_pnl_ByTicketID_txt_TicketID').focus();
        return(false);
    }
    var tr=document.getElementById('Tabs_pnl_ByTicketID_plzwait');
    tr.style.display='block';
    var trsearch=document.getElementById('Tabs_pnl_ByTicketID_searcharea');
    trsearch.style.display='none';
}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="scriptManager" runat="server">
        <Scripts>
            <aspnew:ScriptReference Name="MicrosoftAjax.js" Path="../Scripts/System.Web.Extensions/1.0.61025.0/MicrosoftAjax.js" />
        </Scripts>
    </aspnew:ScriptManager>
        <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc2:ActiveMenu ID="ActiveMenu1" runat="server"></uc2:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">

            
             <div class="col-xs-12">
           
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                       <asp:Label ID="lbl_Message" runat="server" ForeColor="Red" CssClass="form-label"></asp:Label> 
                </div>
        </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Do Not Mail Update</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>

            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left"> Do Not Mail Update</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <asp:HyperLink ID="hl_records" runat="server" NavigateUrl="~/Activities/NoMailFlagRecords.aspx">No Mail Flag Records</asp:HyperLink>
                                |
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="NoMailFlagRecordsByAddress.aspx?sMenu=121">No Mail Address</asp:HyperLink>
                         
                    
                </div>
            </header>
                </section>
                
            <div class="clearfix"></div>


            <ajaxToolkit:TabContainer ID="Tabs" runat="server" Width="100%" ScrollBars="Auto"
                                    OnClientActiveTabChanged="ClearForm">
                                    <ajaxToolkit:TabPanel ID="pnl_ByDescription" runat="server" HeaderText="By Description"
                                        TabIndex="0">
                                        <ContentTemplate>


            
            <section class="box" id="" style="">
                    

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">First Name:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_FirstName" runat="server" CssClass="form-control inline-textbox"
                                                            
                                        ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>

                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"><font color="red">*</font>Last Name:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_LastName" runat="server" CssClass="form-control inline-textbox" 
                                        ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"><font color="red">*</font>Address:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_Address" runat="server" CssClass="form-control inline-textbox"
                                        ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                     <div class="clearfix"></div>

                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">City:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_City" runat="server" CssClass="form-control inline-textbox" 
                                        ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                      <div class="clearfix"></div>

                       <div class="col-md-2">
                                                            <div class="form-group">
                              <label class="form-label"> State:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:DropDownList ID="ddl_States" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>

                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"> <font color="red">*</font> Zip Code:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:TextBox ID="txt_ZipCode" runat="server" CssClass="form-control inline-textbox" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                             
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:Button ID="btnSearch_Details" runat="server" CssClass="btn btn-primary" Text="Search"
                                                            OnClick="btnSearch_Details_Click" OnClientClick="return ValidateForm();" >
                                                        </asp:Button>
                                    <asp:Button ID="btn_Reset" runat="server" CssClass="btn btn-primary" Text="Reset"
                                                            OnClientClick="return ClearForm();"></asp:Button>
                                                        <asp:Button ID="BtnAdd" runat="server" Text="Add" OnClick="BtnAdd_Click" CssClass="btn btn-primary"
                                                            OnClientClick="return ValidateForm();" />
                                    </div>
                                                                </div>
                         </div>



                    </div>
                     </div>


                </section>
</ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                 <ajaxToolkit:TabPanel ID="pnl_ByLetterID" runat="server" HeaderText="By Letter ID">
                                        <ContentTemplate>

<section class="box" id="" style="">
                    

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Letter ID:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_LetterID" runat="server" CssClass="form-control inline-textbox" MaxLength="8"></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>
                       <div class="col-md-6">
                                                            <div class="form-group">
                              <%--<label class="form-label">Letter ID:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:Button ID="btnSearch_LID" runat="server" CssClass="btn btn-primary" Text="Search"
                                                            OnClick="btnSearchByLetterID_Click" OnClientClick="return ValidateLetterID();"
                                                           ></asp:Button>
                                    <asp:Button ID="btn_Reset_LetterID" runat="server" CssClass="btn btn-primary" Text="Reset"
                                                            OnClientClick="return ClearForm();" ></asp:Button>
                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
    </section>
                                             </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="pnl_ByTicketID" runat="server" HeaderText="By Ticket Number">
                                        <ContentTemplate>
                                           

<section class="box" id="" style="">
                    

                 <div class="content-body">
                <div class="row">
                   <table style="width: 50%">
                       <tr id="searcharea" runat="server">
                           <td>

                           
                     <div class="col-md-6" >
                                                            <div class="form-group">
                                                                
                        
                    
                              <label class="form-label">Ticket Number:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_TicketID" runat="server" CssClass="form-control inline-textbox"></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>
                       <div class="col-md-6">
                                                            <div class="form-group">
                              <%--<label class="form-label">Letter ID:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                       <asp:Button ID="btn_Save_TicketID" runat="server" CssClass="btn btn-primary" Text="Search"
                                                            OnClick="btnSaveByTicketNum_Click" OnClientClick="return ValidateTicketID();"
                                                            ></asp:Button>
                                     <asp:Button ID="btn_Reset_TicketID" runat="server" CssClass="btn btn-primary" Text="Reset"
                                                            OnClientClick="return ClearForm();" ></asp:Button>


                                    <table>
                                    <tr id="plzwait" style="display: none" runat="server">
                                                    <td valign="middle" align="center" class="clssubhead" colspan="4">
                                                        <img src="../Images/plzwait.gif" alt="" />
                                                        &nbsp; Please wait while we process your request.
                                                    </td>
                                                </tr>
                                        </table>


                                    </div>
                                                                </div>
                         </div>
                       </td>
                       </tr>
                   </table>

                    </div>
                     </div>
    </section>
                                            </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="pnl_ByAddress" runat="server" HeaderText="By Addresses" TabIndex="0">
                                        <ContentTemplate>
                                            <section class="box" id="" style="">
                    

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"> Address:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control inline-textbox" ></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>
                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"> City:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:TextBox ID="txtCity" runat="server" CssClass="form-control inline-textbox" ></asp:TextBox>


                                    


                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>
                       <div class="col-md-2">
                                                            <div class="form-group">
                              <label class="form-label"> State:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:DropDownList ID="ddlStates" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>


                                    


                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>
                       <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">  Zip Code:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control inline-textbox" ></asp:TextBox>


                                    


                                    </div>
                                                                </div>
                         </div>
                     <div class="clearfix"></div>
                       <div class="col-md-6">
                                                            <div class="form-group">
                              <%--<label class="form-label">  Zip Code:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                      <asp:Button ID="UpdateByAddress" runat="server" CssClass="btn btn-primary" Text="Update"
                                                            OnClick="UpdateByAddress_Click" OnClientClick="return ValidateAddressForm();"
                                                            ></asp:Button>
                                    <asp:Button ID="btn_Reset_address" runat="server" CssClass="btn btn-primary" Text="Reset"
                                                             OnClientClick="return ClearForm();"></asp:Button>


                                    


                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
    </section>
                                            </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                </ajaxToolkit:TabContainer>

            <section class="box" id="" style="">
                    <header class="panel_header">
                     <h2 class="title pull-left">LIST</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">Ticket Number:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped"
                                     AllowPaging="false" OnRowCommand="gv_command">
                                    <Columns>
                                        <asp:TemplateField HeaderText="DBId" Visible="false">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_DBId" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.dbid") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RecordId" Visible="false">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_RecordId" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.recordid") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Case Type">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CaseType" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.casetype") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Ticket Number">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TicketNumber" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ticketnumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="First Name">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbllastName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbladdress" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.state") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Zip Code">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblzipcode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.zipcode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--             <asp:TemplateField HeaderText="Do Not Mail">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_DoNotMail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.nomailflag") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Do Not Mail Flag Date">
                                            <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" />
                                            <ItemStyle VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblinsertdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DoNotMailUpdateDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="HP" runat="server" CommandArgument='<%# bind("sno") %>'>Update</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                    </div>
                                    </div>
                                                                </div>
                         </div>
                    <div class="clearfix"></div>
                    

                    </div>
                     </div>
    </section>
            
            <div class="clearfix"></div>

           <%-- <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <uc1:Footer ID="Footer1" runat="server" />
                                        
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>--%>








            </section>
                     </section>
            </div>
    <div>
      
    </div>
    </form>
     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
       <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineInquiries.aspx.cs"
    Inherits="HTP.Activities.OnlineInquiries" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagName="PolmControl" TagPrefix="uc1" Src="~/WebControls/PolmControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Leads</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script language="javascript" type="text/javascript">

        function checkradio() {

            if (document.getElementById("rb_report_0").checked == true) {
                document.getElementById("tr_image").style.display = 'none';
                document.getElementById("tr_image2").style.display = 'block';
            }
            if (document.getElementById("rb_report_1").checked == true) {
                document.getElementById("tr_image2").style.display = 'none';
                document.getElementById("tr_image").style.display = 'block';
            }

        }

        function checkcomments(type) {
            var datelenght;
            if (type == 1) {
                var comm = document.getElementById("txt_onlinecomment").value;
                var comm_lenght = document.getElementById("txt_onlinecomment").value.length;
                var oldcomm_lenght = document.getElementById("hf_onlineoldcomments").value.length;
            }
            else if (type == 0) {
                var comm = document.getElementById("txt_comment").value;
                var comm_lenght = document.getElementById("txt_comment").value.length;
                var oldcomm_lenght = document.getElementById("hf_oldcomments").value.length;
            }
            else if (type == 2) {
                var comm = document.getElementById("tbContactRepComments").value;
                var comm_lenght = document.getElementById("tbContactRepComments").value.length;
                var oldcomm_lenght = document.getElementById("hfContactOldComments").value.length;
            }

            if (comm_lenght == 0) {
                //alert("Please Enter Comments")
                $("#txtErrorMessage").text("Please Enter Comments");
                $("#errorAlert").modal();
                return false;
            }

            if (CheckName(comm) == false) {
                // alert("Please enter valid comments.");
                $("#txtErrorMessage").text("Please Enter Comments");
                $("#errorAlert").modal();
                return false;
            }
            if (comm_lenght > 0)
                dateLenght = 27
            else
                dateLenght = 0

            if ((oldcomm_lenght + comm_lenght) > 5000 - dateLenght)//-27 bcoz to show last time partconcatenated with comments
            {
                //alert("Sorry You cannot type in more than 5000 characters in contact comments")
                $("#txtErrorMessage").text("Sorry You cannot type in more than 5000 characters in contact comments");
                $("#errorAlert").modal();
                return false;
            }

            //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
            return CheckDate();
        }

        function CheckName(name) {
            for (i = 0 ; i < name.length ; i++) {
                var asciicode = name.charCodeAt(i)
                //If not valid alphabet 
                if ((asciicode == 60) || (asciicode == 62))
                    return false;
            }
        }

        function showHideDateRange(chkPending) {
            var dateRange = document.getElementById("tblContactUs");

            if (document.getElementById("rb_report_1").checked) {
                dateRange.style.display = "block";
                document.getElementById("cal_EffectiveFrom").disabled = true;

                if (chkPending == 1)
                    document.getElementById("dd_callback").value = "0";
            }
            else {
                dateRange.style.display = "none";
                document.getElementById("cal_EffectiveFrom").disabled = false;
            }
        }

        //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
        function DateDiff(date1, date2) {
            var one_day = 1000 * 60 * 60 * 24;
            var objDate1 = new Date(date1);
            var objDate2 = new Date(date2);
            return (objDate1.getTime() - objDate2.getTime()) / one_day;
        }

        function CheckDate() {
            today = new Date();
            seldate = document.getElementById("calfollowupdate").value;
            var diff = Math.ceil(DateDiff(seldate, today));
            var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
            var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth() + 1) + "/" + Date.parseInvariant(seldate).getDate() + "/" + Date.parseInvariant(seldate).getFullYear(), "MM/dd/yyyy");
            newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
            var callBackddl = document.getElementById("ddl_onlinecallback").selectedIndex;

            if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {
                //alert("Please select any business day");
                $("#txtErrorMessage").text("Please select any business day");
                $("#errorAlert").modal();

                return false;
            }

            if (diff <= 0 && (callBackddl == 0 || callBackddl == 2 || callBackddl == 3)) {
                //alert("Please select future date");
                $("#txtErrorMessage").text("Please select future date");
                $("#errorAlert").modal();
                return false;
            }

        }

        // Noufil 6766 02/10/2010 Hide modal popup
        function HideModalPopup(popupId) {
            //            document.getElementById('polmControl1_dd_QuickLegalDescription').value = "-1";
            //            document.getElementById('polmControl1_dd_damagevalue').value = "-1";
            //            document.getElementById('polmControl1_dd_Bodilydamage').value = "-1";
            //            document.getElementById('polmControl1_dd_attorney').value = "-1";
            //            try { document.getElementById('polmControl1_dd_Division').value = "1"; } catch(e){}
            //            document.getElementById('polmControl1_TxtLegalMatterDescription').value = ""; 
            //            document.getElementById('polmControl1_lblMessage').innerHTML = "";            
            //            document.getElementById('polmControl1_td_buttons').style.display = "";
            var modalPopupBehavior = $find(popupId);
            modalPopupBehavior.hide();
            return false;
        }

    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            width: 40%;
            padding-left: 5px;
            background-color: #EFF4FB;
        }
    </style>
</head>
<body>
    <form id="Form2" method="post" runat="server">
    <aspnew:ScriptManager ID="ScriptManager2" runat="server" />

     

         <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>

                 <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">
            <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red" EnableViewState="False"></asp:Label>
                 </div>
            
                </div>
        

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Leads</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>

             <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
            <ContentTemplate>
             <section class="box" id="" style="">
                     <%--<header class="panel_header">
                     <h2 class="title pull-left">Leads</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>--%>

                 <div class="content-body">
                <div class="row">

                   <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Start Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="calStartDate" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                   <%--  <ew:CalendarPopup ID="calStartDate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                            CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                            ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                            ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Text=" " Width="80px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>

                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">End Date:</label>

                                <span class="desc"></span>
                                <div class="controls">
                                     <picker:datepicker id="calenddate" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                 <%--    <ew:CalendarPopup ID="calenddate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                            CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                            ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                            ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Text=" " Width="80px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                       <div class="col-md-3">
                                                            <div class="form-group">
                              <%--<label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:CheckBox ID="cb_showall" runat="server" Text="All Dates" CssClass="checkbox-custom"
                                            Checked="True" />
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
                 <span class="desc"></span>
                                <div class="controls">
                                    <asp:CheckBox ID="cb_AllFollowUpDate" runat="server" Text="All Follow-Up Dates" CssClass="checkbox-custom"
                                            Checked="false" />
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>


                      <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Practice Area:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddl_practiceArea" runat="server" CssClass="form-control" Width="">
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Call Back Status:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="dd_callback" runat="server" CssClass="form-control"
                                            Width="">
                                            <asp:ListItem Text="Active" Value="1" Selected="True" Enabled="true"></asp:ListItem>
                                            <asp:ListItem Text="Close" Value="0" Selected="False" Enabled="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>
                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">Call Back Status:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary pull-right" OnClick="btn_update1_Click1">
                                        </asp:Button>
                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                 </section>

             <div class="clearfix"></div>

             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Leads</h2>
                     <div class="actions panel_actions pull-right">
                     
                          <uc3:PagingControl ID="Pagingctrl" runat="server" />
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                   <div class="col-md-12">
                                                            <div class="form-group">
                              <label class="form-label">Start Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                      <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="820px"
                                                        CellPadding="3" CssClass="table table-small-font table-bordered table-striped" OnRowCommand="gv_records_RowCommand"
                                                        OnRowDataBound="gv_records_RowDataBound" ShowHeader="False" AllowPaging="true"
                                                        PageSize="20" OnPageIndexChanging="gv_records_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table class="clsLeftPaddingTable" id="Table1" cellspacing="1" cellpadding="0" width="820"
                                                                        align="center" border="0">
                                                                        <tr>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" valign="middle">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="lbl_CallerID" runat="server" CssClass="clssubhead">Caller ID :</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 100%">
                                                                                <asp:Label ID="lbl_CallerIDValue" runat="server" Text='<%# Eval("[CallerId]") %>' CssClass="clssubhead"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>    
                                                                             <td valign="middle" align="left" bgcolor="#eff4fb" rowspan="7" style="width: 8%">
                                                                                <asp:HyperLink ID="hl_SerialNumber" runat="server" Text='<%# Eval("Sno") %>' CssClass="clssubhead"></asp:HyperLink>
                                                                                <asp:HiddenField ID = "hf_ticketId" runat ="server" Value = '<%# Eval("TicketID") %>' />
                                                                            </td>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label11" runat="server" CssClass="clssubhead">Name:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 100%">
                                                                                <asp:Label ID="lnk_name" runat="server" Text='<%# Eval("[Name]") %>' CssClass="clssubhead"></asp:Label>
                                                                                <asp:Label ID="lbl_legalid" runat="server" Text='<%# Eval("ID") %>' CssClass="Label"
                                                                                    ForeColor="#123160" Visible="False">
                                                                                </asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" align="left" valign="top" bgcolor="#eff4fb" height="20"
                                                                                rowspan="4" style="width: 175px">
                                                                                <asp:Label ID="lbl_phone" runat="server" Text='<%# Eval("Phone") %>' CssClass="clsLeftPaddingTable"></asp:Label><br />
                                                                                <asp:Label ID="lbl_fax" runat="server" Text='<%# Eval("Fax") %>' CssClass="clsLeftPaddingTable"></asp:Label><br>
                                                                                <asp:LinkButton ID="lnk_Comment" runat="server" Width="130px" CommandName="Add_gv_Comments"
                                                                                    Text="Add Comments"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkShowPolm" runat="server" Width="130px" CommandName="AddPolm"
                                                                                    Text="Add Polm"></asp:LinkButton>
                                                                                <br>
                                                                            </td>
                                                                            <td valign="top" width="205" bgcolor="#eff4fb" height="20" rowspan="4" align="left">
                                                                                <div style="width: 280px; height: 83px; overflow: auto;">
                                                                               <%-- Muhammad Nadir siddiqui 9158 04/15/2011--%>
                                                                                    <asp:Label ID="lbl_comment" runat="server" Text='<%# Eval("Repcomments") %>' CssClass="clsLeftPaddingTable"></asp:Label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label1" runat="server" CssClass="clssubhead">Call Back Status :</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lbl_callback" runat="server" Text='<%# Eval("calldescription") %>'
                                                                                    CssClass="Label" ForeColor="#123160">
                                                                                </asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label2" runat="server" CssClass="clssubhead">Email Received Date 
                                                                                :</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lbl_recdate" runat="server" Text='<%# Eval("recdate") %>' CssClass="Label"
                                                                                    ForeColor="#123160">
                                                                                </asp:Label>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                        <tr>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label4" runat="server" CssClass="clssubhead">Language :</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("Language") %>'
                                                                                    CssClass="Label" ForeColor="#123160">
                                                                                </asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label12" runat="server" CssClass="clssubhead">Email Address:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" height="20" align="left" style="width: 100px">
                                                                                <asp:Label ID="lbl_emailadress" runat="server" Text='<%# Eval("EmailAddress") %>'
                                                                                    CssClass="Label" ForeColor="#123160" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label13" runat="server" CssClass="clssubhead">Question/Comments:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lbl_Question" runat="server" Text='<%# Eval("Comments") %>' CssClass="Label"
                                                                                    ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- Fahad 6429 Add New Row--%>
                                                                        <tr id="trPracArea" runat="server">
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="lblPracArea" runat="server" CssClass="clssubhead">Practice Area:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("PracticeArea") %>' CssClass="Label"
                                                                                    ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trFollowUpdate" runat="server">
                                                                            <td>
                                                                            </td>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label5" runat="server" CssClass="clssubhead">Follow-Up date:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lblfollowupdate" runat="server" Text='<%# Eval("followupdate") %>'
                                                                                    CssClass="Label" ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- Haris Ahmed 10181 04/10/2012 Add IPAddress and SaleRep Columns--%>
                                                                        <tr id="trIPAddress" runat="server">
                                                                            <td>
                                                                            </td>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label8" runat="server" CssClass="clssubhead">IP Address:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lblIPAddress" runat="server" Text='<%# Eval("IPAddress") %>'
                                                                                    CssClass="Label" ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                         <tr id="trSaleRep" runat="server">
                                                                            <td>
                                                                            </td>
                                                                            <td align="left" bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label9" runat="server" CssClass="clssubhead">Sales Rep:</asp:Label>
                                                                            </td>
                                                                            <td class="clsLeftPaddingTable" bgcolor="#eff4fb" align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lblSaleRep" runat="server" Text='<%# Eval("SaleRep") %>'
                                                                                    CssClass="Label" ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- Fahad 6429 end Add New Row--%>
                                                                        <tr>
                                                                            <td align="left" colspan="3">
                                                                                <asp:Label ID="lbl_sitetype" runat="server" BackColor="#FFCC66" Font-Bold="True" Text='<%# Eval("sitetype") %>'></asp:Label> <%--Kashif jawed 9043 03/28/2011 set text for lbl_sitetype--%> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr ID = "tr_clientInfo" runat = "server">
                                                                            <td align="left" colspan="3" >
                                                                                <asp:Label ID="lblClientInfo" runat="server" BackColor="#FFCC66" Font-Bold="True" Text='<%# Eval("CelintInfo") %>'></asp:Label> <%--Kashif jawed 9043 03/28/2011 set text for lbl_sitetype--%> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td bgcolor="#eff4fb">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td bgcolor="#eff4fb" colspan="4">
                                                                           <%-- Muhammad Nadir Siddiqui 9158 04/15/2011--%>
                                                                                <asp:HiddenField ID="hf_GeneralComments" runat="server" Value="<%# Bind('Comments') %>" />
                                                                                <asp:HiddenField ID="hf_question" runat="server" Value="<%# Bind('Comments') %>" />
                                                                                <asp:HiddenField ID="hf_email" runat="server" Value="<%# Bind('EmailAddress') %>" />
                                                                                <asp:HiddenField ID="hf_onlinecallback" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.callback") %>' />
                                                                                <asp:HiddenField ID="hf_comm" runat="server" Value="<%# Bind('legal') %>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 11px" width="780" background="../../images/separator_repeat.gif"
                                                                                height="11">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td background="../../images/separator_repeat.gif" colspan="4" height="11" style="height: 11px"
                                                                                width="780">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&#160;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&#160;&#160;&#160;" LastPageText="&#160;&#160;&#160;Last Page &gt;&gt;" />
                                                    </asp:GridView>

                                        </div>

                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                 </section>

             <div class="clearfix"></div>

            <ajaxToolkit:ModalPopupExtender ID="Modal_gvrecords" runat="server" TargetControlID="Button1"
                                                        PopupControlID="pnl_online" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        CancelControlID="btn_onlinecancel">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <asp:Button ID="btn" runat="server" Style="display: none" />
                                                    <asp:Button ID="Button1" runat="server" Style="display: none" />
                                                    <asp:Panel ID="pnl_online" runat="server" Height="100px" Width="420px" Style="display: none;">
                                                        <table id="Table2" bgcolor="white" border="1" style="border-top: black thin solid;
                                                            border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                                                            height: 200px; width: 420px;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 32px"
                                                                    width="420" colspan="2">
                                                                    &nbsp;Update Comments
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Name:</span>
                                                                </td>
                                                                <td style="height: 22px; text-align: left;">
                                                                    &nbsp;<asp:Label ID="lbl_onlinename" runat="server" CssClass="Label"></asp:Label><asp:Label
                                                                        ID="Label3" runat="server" Visible="False" CssClass="Label"></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Email Address:</span>
                                                                </td>
                                                                <td width="160" style="height: 22px; text-align: left">
                                                                    &nbsp;<asp:Label ID="lbl_onlineemail" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" align="left">
                                                                    <span class="clssubhead">&nbsp;Contact Number:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 22px;">
                                                                    &nbsp;<asp:Label ID="lbl_ph" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Question:</span>
                                                                </td>
                                                                <td style="text-align: left; width: 270px">
                                                                    <div style="width: 270px; height: 55px; overflow: auto; vertical-align: middle" runat="server"
                                                                        id="divQuestion">
                                                                        &nbsp;<asp:Label ID="lbl_onlinequestion" runat="server" CssClass="Label"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Call Back:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 23px; width: 160px">
                                                                    <asp:DropDownList ID="ddl_onlinecallback" runat="server" CssClass="clsInputCombo"
                                                                        Width="160px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle" style="width: 136px">
                                                                    <span class="clssubhead">&nbsp;Current Follow-Up date:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 23px;">
                                                                    &nbsp;<asp:Label ID="lbl_currentFollowUp" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Next Follow-Up date:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 24px; width: 160px">
                                                                    <div>
                                                                        &nbsp;<ew:CalendarPopup ID="calfollowupdate" runat="server" EnableHideDropDown="True"
                                                                            ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                                            AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
                                                                            PadSingleDigits="True" ToolTip="Follow-Up Date" Font-Names="Tahoma" Font-Size="8pt"
                                                                            ImageUrl="../images/calendar.gif" Text=" " Width="80px" JavascriptOnChangeFunction="CheckDate">
                                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                        </ew:CalendarPopup>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td>
                                                                    <span class="clssubhead">&nbsp;Comments:</span>
                                                                </td>
                                                                <td align="left">
                                                                    <div style="width: 270px; height: 83px; overflow: auto;" runat="server" id="divCommentOnline">
                                                                        &nbsp;<asp:Label ID="lblOldOnlineComments" runat="server" CssClass="Label" Width="250px"></asp:Label>
                                                                    </div>
                                                                    <asp:TextBox ID="txt_onlinecomment" runat="server" Height="62px" Text="" TextMode="MultiLine"
                                                                        Width="270px" CssClass="form-control"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#eff4fb">
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" style="height: 21px">
                                                                    &nbsp;<asp:Button ID="btn_onlineupdate" runat="server" CssClass="clsbutton" Text="Update"
                                                                        OnClick="btn_onlineupdate_Click" OnClientClick="return checkcomments(1);" Width="60px" />
                                                                    <asp:Button ID="btn_onlinecancel" runat="server" CssClass="clsbutton" Text="Cancel"
                                                                        Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:HiddenField ID="hf_onlineoldcomments" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>


            <ajaxToolkit:ModalPopupExtender ID="mpeShowPolm" runat="server" TargetControlID="polmHiddenButton"
                                PopupControlID="pnlPolm" BackgroundCssClass="modalBackground" DropShadow="false">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button runat="server" ID="polmHiddenButton" Text="More" Style="display: none;" />
                            <asp:Panel ID="pnlPolm" runat="server">
                                <table id="table1" style="border-top: black thin solid; border-left: black thin solid;
                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                    cellspacing="0" class="clsLeftPaddingTable">
                                    <tr>
                                        <td align="left" valign="bottom" background="../Images/subhead_bg.gif" colspan="2">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td style="height: 26px" class="clssubhead">
                                                        <asp:Label runat="server" ID="lbl_title" Text="Consultation Request"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:LinkButton ID="lbtn_close2" runat="server" OnClientClick="return HideModalPopup('mpeShowPolm');">X</asp:LinkButton>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <uc1:PolmControl ID="polmControl1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>






                 </ContentTemplate>
        </aspnew:UpdatePanel>


            </section>
                     </section>
             </div>
                
            
  
    </form>

    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
      <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">
        document.getElementById("calfollowupdate_div").style.zIndex = 10003;
        document.getElementById("calfollowupdate_monthYear").style.zIndex = 10004;
    </script>

     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>

</body>
</html>

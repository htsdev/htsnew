<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DoNotMailUpdate.aspx.cs" Inherits="HTP.Activities.DoNotMailUpdate" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Do not mail flag update</title>
     <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<script type="text/jscript">
function ClearForm()
{

document.getElementById('txt_FirstName').value='';
document.getElementById('txt_LastName').value='';
document.getElementById('txt_Address').value='';
return(false);
}

function ValidateForm()
{

var var1 = document.getElementById('txt_FirstName');
var var2 = document.getElementById('txt_LastName');
var var3 = document.getElementById('txt_Address');

if(var1.value == '')
{
 alert('Please enter First Name');
 var1.focus();
 return(false);
}

if(var2.value == '')
{
 alert('Please enter Last Name')
 var2.focus();
 return(false);
}

if(var3.value == '')
{
 alert('Please enter an address')
 var3.focus();
 return(false);
}
return(true);
}

</script>

    <form id="form1" runat="server">
    <div>
    <table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
    <tr>
    <td>
    </td>
    </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lbl_Message" runat="server" ForeColor="Red" Text="Label" CssClass="label"></asp:Label></td>
        </tr>
        <tr>
            <td background="../Images/subhead_bg.gif" height="34" class ="clssubhead">
            </td>
        </tr>
        <tr>
            <td align="center">
            <table width="100%">
            <tr>
            <td width="50%" align="right">First Name: 
            </td>
            <td width="50%" align="left">
                <asp:TextBox ID="txt_FirstName" runat="server" CssClass="clsinputadministration" Width="150px"></asp:TextBox>
            </td>
            </tr>
                <tr>
                    <td align="right" width="50%">
                        Last Name:
                    </td>
                    <td align="left" width="50%">
                        <asp:TextBox ID="txt_LastName" runat="server" CssClass="clsinputadministration" Width="150px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" width="50%">
                        Address:
                    </td>
                    <td align="left" width="50%">
                        <asp:TextBox ID="txt_Address" runat="server" Width="366px" CssClass="clsinputadministration"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" width="50%">
                        <asp:Button ID="btnSave" runat="server" CssClass="clsbutton" Text="Save" Height="19px" OnClick="btnSave_Click" Width="52px" OnClientClick="return ValidateForm()" /></td>
                    <td align="left" width="50%">
                        <asp:Button ID="btn_Reset" runat="server" CssClass="clsbutton" Text="Reset" OnClick="btnSave_Click" Width="50px" OnClientClick="return ClearForm();" /></td>
                </tr>
            </table>
            
            </td>
        </tr>
        <tr>
            <td align="center">
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoMailFlagRecords.aspx.cs"
    Inherits="HTP.Reports.NoMailFlagRecords" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TrafficMenuMain.ascx" TagName="TrafficMenuMain"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc4" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>No mail flag records</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">
    
function ClearForm()
{
//Clear all values
document.getElementById('txt_FirstName').value='';
document.getElementById('txt_LastName').value='';
document.getElementById('txt_Address').value='';
document.getElementById('txt_ZipCode').value='';
document.getElementById('lbl_Message').innerText = '';
document.getElementById('txt_City').value='';
//Yasir Kamal 6100 07/03/2009 reset calendar control date.
document.getElementById('chk_useRange').checked=true;
document.getElementById('ddl_States').value = 0;
document.getElementById('td_grid').style.display='none';
document.getElementById('tblPaging').style.display='none';

var date='<%=ViewState["date"]%>';
document.form1.calFrom.value = date;
document.form1.calTo.value = date;

return(false);
}


function useRange()
		{		
		 
		    if (document.getElementById("chk_useRange").checked)
		    {
		      document.getElementById("calFrom").disabled =false;
		      document.getElementById("calTo").disabled =false;
		    }
		    else
		    {
	        document.getElementById("calFrom").disabled =true;
	        document.getElementById("calTo").disabled =true;
		    }
		}	

function ValidateForm()
{

  /*  var var1 = document.getElementById('txt_FirstName');
    var var2 = document.getElementById('txt_LastName');
    var var3 = document.getElementById('txt_Address');
    var var4 = document.getElementById('txt_ZipCode');

    if(var1.value == '' && var2.value == '' && var3.value == '' && var4.value == '')
    {
        alert('Please enter a value to search');
        var1.focus();
        return(false);
    }
    
    if(var4.value != '' && (var1.value == '' && var2.value == '' && var3.value == ''))
    {
        alert('To search by zip code, please supply either the first name, last name or address with the zip code');
        var1.focus();
        return(false);
    }
*/
    return(true);
}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" method="post" runat="server">
         <aspnew:ScriptManager ID="ScriptManager1" runat="server"/>
         <div class="page-container row-fluid container-fluid">
        <asp:Panel ID="pnl" runat="server">
                        <uc3:ActiveMenu ID="ActiveMenu1" runat="server"></uc3:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="Table2">
        <section class="wrapper main-wrapper row" id="" style="">

               <div class="col-xs-12">
           
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <asp:Label ID="lbl_Message" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
                </div>
                 

                 
        </div>

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Do Not Mail Update</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>

              
                                <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Search</h2>
                     <div class="actions panel_actions pull-right">
                         <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="DoNotMail.aspx?sMenu=121">Back</asp:HyperLink>
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>


                                     <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">First Name:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_FirstName" runat="server" CssClass="form-control "
                                    Width=""></asp:TextBox>


                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Last Name:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <asp:TextBox ID="txt_LastName" runat="server" CssClass="form-control " Width=""></asp:TextBox>

                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">City:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:TextBox ID="txt_City" runat="server" CssClass="form-control " Width=""></asp:TextBox>


                                    </div>
                                                                </div>
                         </div>


                     <div class="clearfix"></div>


                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Address:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <asp:TextBox ID="txt_Address" runat="server" CssClass="form-control " Width=""></asp:TextBox>

                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">Zip Code:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <asp:TextBox ID="txt_ZipCode" runat="server" CssClass="form-control " Width=""></asp:TextBox>

                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">State:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                     <asp:DropDownList ID="ddl_States" runat="server" CssClass="form-control">
                                </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                     <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">From Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <picker:datepicker id="calFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>

                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                              <label class="form-label">To Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    

                                     <picker:datepicker id="calTo" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                    </div>
                                                                </div>
                         </div>
                    <div class="col-md-4">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Date</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:CheckBox ID="chk_useRange" runat="server" onclick="useRange()" CssClass="checkbox-custom"
                                                Checked="true" Text="Use Date Range" />


                                    </div>
                                                                </div>
                         </div>
                     <div class="clearfix"></div>

                     <div class="col-md-6">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Date</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click"
                                    Width="" OnClientClick="return ValidateForm();"></asp:Button>
                                    <asp:Button ID="btn_Reset" runat="server" CssClass="btn btn-primary" Text="Reset" OnClientClick="return ClearForm();"
                                    Width=""></asp:Button>


                                    </div>
                                                                </div>
                         </div>


                    </div>
                     </div>
                                    </section>


            <div class="clearfix"></div>

             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">LIST</h2>
                     <div class="actions panel_actions pull-right">
                         <table id="tblPaging" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                          <uc4:PagingControl ID="Pagingctrl" runat="server" />
                     
                        <%-- <a class="box_toggle fa fa-chevron-down"></a>--%>
                    </td>
                        </tr>
                    </table>
                </div>
            </header>


                                     <div class="content-body" id="td_grid">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                             <%-- <label class="form-label">First Name:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                   <div class="table-responsive" data-pattern="priority-columns">
                                     <asp:GridView ID="gv_Records" runat="server" AutoGenerateColumns="False" CssClass="table table-small-font table-bordered table-striped"
                        Width="100%" PageSize="20" OnRowCommand="gv_Records_RowCommand" AllowPaging="True"
                        OnPageIndexChanging="gv_Records_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="S. No">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_sno" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.sno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FirstName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_LastName" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Address" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_city" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_State" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zip">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Zip" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Zip") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Insert Date">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_insertdate" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.insertdate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Update Source">
                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_updatSource" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.UpdateSource") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="clssubhead" Width="3%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.FirstName") +"," +DataBinder.Eval(Container, "DataItem.LastName") +"," + DataBinder.Eval(Container, "DataItem.Address") + "," + DataBinder.Eval(Container, "DataItem.Zip") %>'
                                        ImageUrl="~/Images/cross.gif" CommandName="Remove" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&nbsp;Next &gt;" PreviousPageText="&lt; Previous"
                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="&nbsp;&nbsp;&nbsp;Last Page &gt;&gt;" />
                    </asp:GridView>
                                       </div>
                                    </div>
                                                                </div>
                         </div>

                    </div>
                                         </div>
                </section>















            </section>
                     </section>
             </div>



   
    </form>


     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>

     <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Error message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>




</body>
</html>

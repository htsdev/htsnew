﻿<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.PaymentInfo.CloseOutReport"
    SmartNavigation="False" CodeBehind="CloseOutReport.aspx.cs" %>

<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <%--Waqas Javed 3229 12/23/08 Header changed --%>
    <title>Close Out Report</title>
    <%--<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet">--%>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
        <meta content="C#" name="CODE_LANGUAGE" />
        <meta content="JavaScript" name="vs_defaultClientScript" />
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1">

        <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
        <meta content="C#" name="CODE_LANGUAGE">
        <meta content="JavaScript" name="vs_defaultClientScript">
        <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
        <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
        <!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
        <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
        <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
        <!-- For iPad Retina display -->

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
        <!-- CORE CSS FRAMEWORK - END -->

    <link href="../assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" />

        <!-- CORE CSS TEMPLATE - START -->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>

    <script type="text/javascript">	
		function MaxLength()
		{
		    if(document.getElementById("txtRemarks").value.length >1200)
		        {
		        alert("The length of character is not in the specified Range");
		        document.getElementById("txtRemarks").focus();
		        }
		}
		
		 function ValidateLenght()
	     { 
			var cmts = document.getElementById("txtRemarks").value;
			if (cmts.length > 1200)
			{
			    event.returnValue=false;
                event.cancel = true;
			}
	    }
		
		
	    // tahir ahmed 4171 6/28/2008
		// added for grouping Hiring Source Summary grouping...
		// and collapse/expand feature...
		function ShowHideMailerDetail(txtToggle)
		{
		
			var txt = document.getElementById(txtToggle );
			
			if (txt != null)
			{
			    if (txt.value == "0")
			        txt.value = "1";
			    else 
			        txt.value = "0";
		    }
			    
			
			
			var cnt = parseInt(document.getElementById("iMailerSummaryCount").value);
			var grdName = "dgMailerSummary";
			var idx=2;
			cnt = cnt + 2;

            var tempCourt = "";
            var tempToggle = "";

			for (idx=2; idx < (cnt); idx++)
			{
				var tr = ""; 
				var IsGroup= "";
				var Category = "";
				var textbox = "";
				var img1 = "";
				var img2 = "";
				 
				if( idx < 10)
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 IsGroup  = grdName+ "_ctl0" + idx + "_lbl_IsGroup";
				 Category  = grdName+ "_ctl0" + idx + "_lblCourtCategorynum";
				 textbox = grdName+ "_ctl0" + idx + "_txtToggleImage";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				
				}
				else
				{
				 tr = grdName+ "_ctl" + idx + "_trDetail";
				 IsGroup  = grdName+ "_ctl" + idx + "_lbl_IsGroup";
				 Category  = grdName+ "_ctl" + idx + "_lblCourtCategorynum";
				 textbox = grdName+ "_ctl" + idx + "_txtToggleImage";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				}

				var trCC = document.getElementById(tr);
				var bGroup = document.getElementById(IsGroup).innerText;
				var iCat = document.getElementById(Category).innerText;
				var sTxt = document.getElementById(textbox).value;
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);



				// if it is court category group
				if (bGroup == "True")
				{
				    tempCourt = iCat;
				    tempToggle = sTxt;

                    // if collapsed then expand....
				    if (sTxt == "0" )
				    {
				        img11.style.display = "none";   
				        img22.style.display = "block";   
				    }

                    // if expanded then collapse...
				    else if (sTxt == "1" )
				    {
				        img11.style.display = "block";   
				        img22.style.display = "none";   
				    }
				        
				}
				
				// if child row of court category......
				else
				{
				    // if court category is collapsed then 
				    // do not display the mailer type detail....
				    if (tempCourt == iCat && tempToggle == "0")
				    {
				        trCC.style.display = "none";
				    }
				    
				    // if court category is expanded then 
				    // display the mailer type detail....
				    else
				    {
				        trCC.style.display = "block";
				    }
				}
				
			}

		}
		
		
		//tahir ahmed 3736 05/02/2008
		//added for grouping of courts
		// the function will show or hide the sub courts
		// if clicked on a court category.....
		function ShowHideCourtDetail(category)
		{  			
			
			var txt = document.getElementById("txtHCJP");	
			var txtcheck=document.getElementById("txtOthers");

            if (category != '0')
            {
			    if (category==2)
			    {
			    if (txt.value  == '0')
				    txt.value = '1';
			    else
				    txt.value = '0';
			    }
			    else if(category ==100)
			    {
    	
			    if (txtcheck.value == '0')
				    txtcheck.value = '1';
			    else
				    txtcheck.value = '0';
		        }
		    }
		
			var cnt = parseInt(document.getElementById("iCourtCount").value);
			var grdName = "dgrdCourt";
			var idx=2;
			cnt = cnt + 2;

			for (idx=2; idx < (cnt); idx++)
			{
				var tr = ""; 
				var courttype = "";
				var IsGroup= "";
				var img1 = "";
				var img2 = "";
				var img3 = "";
				 
				if( idx < 10)
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 courttype = grdName+ "_ctl0" + idx + "_lblCategoryNumber";
				 IsGroup  = grdName+ "_ctl0" + idx + "_lblIsCourtCategory";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				 img3 = grdName+ "_ctl0" + idx + "_iLink";
				
				}
				else
				{
				 tr = grdName+ "_ctl" + idx + "_trDetail";
				 courttype = grdName+ "_ctl" + idx + "_lblCategoryNumber";
				 IsGroup  = grdName+ "_ctl" + idx + "_lblIsCourtCategory";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				 img3 = grdName+ "_ctl" + idx + "_iLink";
				}

				var trCC = document.getElementById(tr);
				var cType = document.getElementById(courttype);
				var rType = document.getElementById(IsGroup);
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);
				var img33 = document.getElementById(img3);
				
				cType = parseInt(cType.innerText);
				rType = parseInt(rType.innerText);
		
				if (rType == 0 && cType!=2 && cType != 100)
		        {
				    img11.style.display = 'none';
				    img22.style.display = 'block';
				    img33.style.display = 'none';
		        }
				
				if (rType == 1)
				{
				    trCC.style.display = 'block';
				    img11.style.display = 'block';
				    img22.style.display = 'none';
				    img33.style.display = 'none';
				}
				
				if (cType == 2 && rType == 0 )
				{				
					if (txt.value == '1')
						{
						trCC.style.display = 'block';
						img22.style.display = 'block';
						img33.style.display = 'block';
						
						}
					else
						{
						trCC.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';						

						}
				}
				 
				 if (cType == 2 && rType == 1 )
				{				
					if (txt.value == '1')
						{
						img11.style.display = 'block';
						img22.style.display = 'none';
						}
					else
						{
						img11.style.display = 'none';
						img22.style.display = 'block';
						}
				}
				 
				 if (cType == 100 && rType == 0)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}
				
					if (cType == 100 && rType == 1 )
				{				
					if (txtcheck.value == '1')
						{
						img11.style.display = 'block';
						img22.style.display = 'none';
						}
					else
						{
						img11.style.display = 'none';
						img22.style.display = 'block';
						}
				}
			}
		}

		
		
		function ShowHideCCDetail(paytype)
		{  			
			
			var txt = document.getElementById("txtCC");	
			var txtcheck=document.getElementById("txtCheck");
			
			if (paytype==5)
			{
			if (txt.value  == '0')
				txt.value = '1';
			else
				txt.value = '0';
			}
			else if(paytype==200)
			{
	
			if (txtcheck.value == '0')
				txtcheck.value = '1';
			else
				txtcheck.value = '0';
		}
		
			var cnt = parseInt(document.getElementById("iCount").value);
			var grdName = "dgrdPayType";
			var idx=2;
			cnt = cnt + 2;

			for (idx=2; idx < (cnt); idx++)
			{
				var tr = "";
				var paytype =""; 
				var img1 = "";
				var img2 = "";
				var img3 = "";
				
				if( idx < 10 )
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 paytype = grdName+ "_ctl0" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				 img3 = grdName+ "_ctl0" + idx + "_iLink";
				}
				else
				{
				tr = grdName+ "_ctl" + idx + "_trDetail";
				 paytype = grdName+ "_ctl" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				 img3 = grdName+ "_ctl" + idx + "_iLink";
				}

				var trCC = document.getElementById(tr);
				var pType = document.getElementById(paytype);
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);
				var img33 = document.getElementById(img3);
				
				pType = parseInt(pType.innerText);
				
		
				if (pType > 500 )
				{				
					if (txt.value == '1')
						{
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
						
						}
					else
						{
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';						

						}
				}
				 if (pType == 2)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}
				//Sabir 5755 04/06/2009 Check for Prepaid leagal
				 if (pType == 3)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}
				 if (pType == 4)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}

			}
			
		}
		
		function CheckMail()
		{
			var Email;
			//a;
			Email= document.getElementById("txtEmail").value;
			if ((Email == '')  || !(isEmail(Email)))
			{
				alert ("Please Enter valid email address");				
				return false;
			}
			return true;		
		}
		
		function ShowEmailBox()
		{		
			if (document.getElementById("TableEmail").style.display=='none')
			{
				document.getElementById("TableEmail").style.display='block'	;						
			}
			else
			{
				document.getElementById("TableEmail").style.display='none'	;				
			}		
		}		
		
				
		
		function ShowViolationFee(TicketID)
		{
			//a;
				//strURL = "../../ClientInfo/violationsfees.asp?caseNumber=" & intTicketID
			var strURL= "//ClientInfo/violationsfees.asp?caseNumber=" & TicketID;
			window.navigate(strURL);				
		}
			
			
		function show( divid )
		{
			divid.style.display = 'block';
			divid.style.margin = '1.5px 0px';
		}

		function hide( divid )
		{
			divid.style.display = 'none';
			divid.style.margin = '0px';
		}
		
		function DefaultLoad()
		{		
			document.getElementById("txtPC").style.display='none';
			document.getElementById("txtPR").style.display='none';
			
			if (document.getElementById("txtPC").value == 1)			
			{
				show(HA2);show(HA3);hide(HA1);			
			}
			else
			{
				hide(HA2);hide(HA3);show(HA1);		
			}
			
			if (document.getElementById("txtPR").value == 1)
			{
				show(HB2);show(HB3);hide(HB1);			
			}
			else
			{
				hide(HB2);hide(HB3);show(HB1);		
			}
			
			if (document.getElementById("txtRS").value == 1)
			{
				show(HAA2);show(HAA3);hide(HAA1);			
			}
			else
			{
				hide(HAA2);hide(HAA3);show(HAA1);		
			}			
			if (document.getElementById("txtCS").value == 1)
			{
				show(HC2);show(HC3);hide(HC1);			
			}
			else
			{
				hide(HC2);hide(HC3);show(HC1);		
			}	
			// Mailer Summary Section.....
		    
			if (document.getElementById("txtMailerSummary").value == 1)			
			{
				show(Div2);
				show(Div3);
				hide(Div1);			
			}
			else
			{
				hide(Div2);
				hide(Div3);
				show(Div1);		
			}
			//show(HA2);show(HA3);hide(HA1);			
			//hide(HB2);hide(HB3);show(HB1);		
			//show(HB2);show(HB3);hide(HB1);

	
		    //tahir ahmed 3736 05/02/2008
		    //SHOW HIDE COURT GROUPS.....
		    ShowHideCourtDetail(0);
		    
		    //tahir 4171 06/28/2008
		    ShowHideMailerDetail(null);

	
			// SHOW HIDE CREDIT CARD TYPES IN PAYMENT TYPE SUMMARY.....
			var txt = document.getElementById("txtCC");
			var txtcheck=document.getElementById("txtCheck");
			var cnt = parseInt(document.getElementById("iCount").value);
			var grdName = "dgrdPayType";
			var idx=2;

			cnt = cnt + 2;

			for (idx=2; idx < (cnt); idx++)
			{
				var tr =""; 
				var paytype =""; 
				var img1 = "";
				var img2 = "";
				var img3 = "";
				
				if( idx < 10)
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 paytype = grdName+ "_ctl0" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				 img3 = grdName+ "_ctl0" + idx + "_iLink";
				}
				else
				{
				 tr = grdName+ "_ctl" + idx + "_trDetail";
				 paytype = grdName+ "_ctl" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				 img3 = grdName+ "_ctl" + idx + "_iLink";
				}

				var trCC = document.getElementById(tr);
				var pType = document.getElementById(paytype);
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);
				var img33 = document.getElementById(img3);
				
				pType = parseInt(pType.innerText);
				
				if (pType == 5 )
					img11.style.display = 'block';


				if (pType > 500)
				{
					if (txt.value == '1' )
						{
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
						}
					else
						{
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';						
						
						}
				}	
				 if (pType == 200)
					img11.style.display = 'block';

				 if (pType == 2)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';
												
					}									
				}
				//Sabir 5755 04/06/2009 Check for Prepaid leagal
				 if (pType == 3)
				{ 
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
					}
				}
				
				 if (pType == 4)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
						
						
					}
					
				
				}

			}
	
			
				
		}
		
		function SetFlag(Control,Value)
		{
			
			document.getElementById(Control).value = Value ;		
			//document.Form1.innerHTML ;
			//document.Form1.outerHTML ; 
			//a;
		}
		
		
		function ValidateMe(txtbox)
		{
			//a;
			//var AmountValue= document.Form1.TextBox3.value;
			var AmountValue= txtbox.value; 
			if (AmountValue !="")
			{
				if (!(isSignedFloat(AmountValue) || isSignedInteger(AmountValue)))
				{
					alert("Please Enter actual amount in textbox");						
					//txtbox.focus();		
					return false;
				}					
			}
			return true;		
		}
		
		
		function PopUpShowPreviewPDF()
		{
				//a;
				//strURL = "../../ClientInfo/violationsfees.asp?caseNumber=" & intTicketID
				//var strURL= "PreviewPDF.aspx?DocID=" & DocID;
				//window.open(strURL);				
				
				//window.open ("QuoteCallBackDetail.aspx?TicketID="+ TicketID + "&QuoteID=" + QuoteID ,"QuoteCallBack", 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=no,resizable=no,left=150,Top=100,height=430,width=500' );				
				//window.open ('PreviewPDF.aspx?DocID='+ DocID,'toolbar=yes,location=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,left=150,Top=100,height=430,width=500' );
				//window.open ("PreviewMain.aspx?DocID="+ DocID);
				window.open ("PreviewMain.aspx");
				
		}
		function DefaultLoad2()
		{		

			//document.getElementById("txtPC").style.display='none';
			//document.getElementById("txtPR").style.display='none';

			// PAYMENT SUMMARY SECTION.....
			if (document.getElementById("txtPC").value == 1)			
			{
				show(HA2);
				show(HA3);
				hide(HA1);			
			}
			else
			{
				hide(HA2);
				hide(HA3);
				show(HA1);		
			}
			
			// TRANSACTION DETAIL SECTION......
			if (document.getElementById("txtPR").value == 1)
			{
				show(HB2);
				show(HB3);
				hide(HB1);			
			}
			else
			{
				hide(HB2);
				hide(HB3);
				show(HB1);		
			}			

			// REP. SUMMARY SECTION.....
			if (document.getElementById("txtRS").value == 1)
			{
				show(HAA2);
				show(HAA3);
				hide(HAA1);			
			}
			else
			{
				hide(HAA2);
				hide(HAA3);
				show(HAA1);		
			}			

			// COURT SUMMARY SECTION......
			if (document.getElementById("txtCS").value == 1)
			{
				show(HC2);
				show(HC3);
				hide(HC1);			
			}
			else
			{
				hide(HC2);
				hide(HC3);
				show(HC1);		
			}			
			
			// CATEGORY SUMMARY SECTION.....
			if (document.getElementById("txtCT").value == 1)
			{
				show(HD2);
				show(HD3);
				hide(HD1);			
			}
			else
			{
				hide(HD2);
				hide(HD3);
				show(HD1);		
			}	
		
			// SHOW HIDE CREDIT CARD TYPES IN PAYMENT TYPE SUMMARY.....
			var txt = document.getElementById("txtCC");
			var txtcheck=document.getElementById("txtCheck");
			var cnt = parseInt(document.getElementById("iCount").value);
			var grdName = "dgrdPayType";
			var idx=2;

			cnt = cnt + 2;

			for (idx=2; idx < (cnt); idx++)
			{
				var tr = "";
				var paytype = "";
				var img1 = "";
				var img2 = "";
				var img3 = "";
				
				if( idx < 10 )
				{
				 tr = grdName+ "_ctl0" + idx + "_trDetail";
				 paytype = grdName+ "_ctl0" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl0" + idx + "_iParent";
				 img2 = grdName+ "_ctl0" + idx + "_iChild";
				 img3 = grdName+ "_ctl0" + idx + "_iLink";

				}
				else
				{
				 tr = grdName+ "_ctl" + idx + "_trDetail";
				 paytype = grdName+ "_ctl" + idx + "_lblPayTypeID";
				 img1 = grdName+ "_ctl" + idx + "_iParent";
				 img2 = grdName+ "_ctl" + idx + "_iChild";
				 img3 = grdName+ "_ctl" + idx + "_iLink";
				}

				var trCC = document.getElementById(tr);
				var pType = document.getElementById(paytype);
				var img11 = document.getElementById(img1);
				var img22 = document.getElementById(img2);
				var img33 = document.getElementById(img3);
				
				pType = parseInt(pType.innerText);
				
				if (pType == 5 )
					img11.style.display = 'block';


				if (pType > 500)
				{
					if (txt.value == '1' )
						{
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
						}
					else
						{
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';						
						
						}
				}	
				 if (pType == 200)
					img11.style.display = 'block';

				 if (pType == 2)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';
						txtcheck.value='0';
						
					}
					
				
				}
				//Sabir 5755 04/06/2009 Check for Prepaid leagal
				 if (pType == 3)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
						txtcheck.value='0';
						
					}
				}
				 if (pType == 4)	
				{  
					if (txtcheck.value == '1')
					{ 
						trCC.style.display = 'block';
						img11.style.display = 'none';
						img22.style.display = 'block';
						img33.style.display = 'block';
					}
					else
					{ 
						trCC.style.display = 'none';
						img11.style.display = 'none';
						img22.style.display = 'none';
						img33.style.display = 'none';
						trCC.style.margin= '0px';	
						txtcheck.value='0';
						
					}
					
				
				}

			}
		}
		
    </script>

    <style type="text/css">
        /*TABLE
        {
            font: 11px verdana,arial,helvetica,sans-serif;
        }*/
        DIV.divHidden
        {
            display: none;
            margin: 0px;
        }
        DIV
        {
            margin: 0px;
        }
    </style>
    <%--<link href="../Styles.css" rel="stylesheet" type="text/css" />--%>
</head>
    <body class=" ">
        
        <form id="Form1" method="post" runat="server">

            <!-- START CONTAINER -->
            <div class="page-container row-fluid container-fluid">

                <asp:Panel ID="pnl" runat="server">
                    <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                </asp:Panel>

                <!-- START CONTENT -->
                <section id="main-content" class=" ">
                    <section class="wrapper main-wrapper row" style=''>

                        <div class='col-xs-12'>
                            <div class="page-title">

                                <div class="pull-left">
                                    <!-- PAGE HEADING TAG - START -->
                                    <h1 class="title">Close Out</h1>
                                    <!-- PAGE HEADING TAG - END -->  
                                </div>
                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- MAIN CONTENT AREA STARTS -->
    
                        <div class="col-xs-12">
                            <section class="box ">
                                    <div class="content-body">
                                        <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td>

                                                    <div class="row">

                                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                                            <div class="form-group">
                                                                <label class="form-label" for="field-1">Date</label>
                                                                <div class="controls">
                                                                    <%--<ew:CalendarPopup ID="calQueryDate" runat="server" ToolTip="Select Report Date" 
                                                                        PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                                                        Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
                                                                        ControlDisplay="TextBoxImage" Font-Names="Tahoma" Font-Size="8pt" AutoPostBack="True">
                                                                        <TextboxLabelStyle CssClass="form-control"></TextboxLabelStyle>
                                                                    </ew:CalendarPopup>--%>

                                                                    <asp:TextBox ID="txtDate" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="col-md-6 col-sm-7 col-xs-8">

                                                            <div class="form-group">
                                                                <asp:Label ID="lblBranchTitle" runat="server" Text="Branch :" class="form-label"></asp:Label>
                                                                <div class="controls">
                                                                    <asp:DropDownList ID="ddl_Branch" runat="server" CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddl_Branch_SelectedIndexChanged"></asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="lblBranch" runat="server" CssClass="form-label"></asp:Label>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <table id="TableEmail" style="display: none; width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <div class="row">

                                                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                                                        <div class="form-group">
                                                                            <label class="form-label" for="field-1">Email</label>
                                                                            <div class="controls">
                                                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-md-6 col-sm-7 col-xs-8">

                                                                        <div class="form-group">
                                                                            <div class="controls">
                                                                                <asp:ImageButton ID="imgbtnSendMail" runat="server" ImageUrl="../Images/SendMail.ICO" CssClass="btn btn-primary pull-right" ToolTip="Send Mail"></asp:ImageButton>
                                                                                <img id="imgHide" onclick="ShowEmailBox()" alt="Cancel" class="pull-left" src="../Images/DELETE.jpg">
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <div class="row">

                                                        <div class="col-xs-12">

                                                            <div class="form-group">
                                                                <asp:LinkButton ID="lnkbtnPrint" runat="server" ToolTip="Print Report in HTML" CssClass="form-label pull-right">Print Report</asp:LinkButton>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-xs-12">

                                                            <div class="form-group">
                                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="form-label"></asp:Label>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </td>
                                            </tr>
                                        </table>
                                        

                                    </div>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">
                                        <div id="HA1">
                                            <asp:TextBox ID="lblHeading1" runat="server" ReadOnly="True">Payment Type Summary</asp:TextBox>
                                        </div>
                                        <div class="divHidden" id="HA2">
                                            <asp:TextBox ID="lblHeading1_1" runat="server" ReadOnly="True">Payment Type Summary</asp:TextBox>
                                        </div>
                                    </h2>
                                    <div class="actions panel_actions pull-right">
                                        <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:DataGrid ID="dgrdPayType" runat="server" CssClass="table" ShowFooter="True" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString" CellPadding="0" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <HeaderTemplate>
                                                            <table  style="width:100%" class="table" cellspacing="0" rules="cols" border="0">
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        Payment Method
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        Count
                                                                    </td>
                                                                    <td align="center" width="20%">
                                                                        Amount
                                                                    </td>
                                                                    <td style="display: none" align="right" width="5%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table style="width:100%" class="table" bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1">
                                                                <tr id="trDetail" runat="server">
                                                                    <td valign="top" align="left" width="60%">
                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="200" border="0">
                                                                            <tr>
                                                                                <td id="iParent" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iLink" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iChild" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td width="170">
                                                                                    &nbsp;
                                                                                    <asp:LinkButton ID="lnkbtnDocumentID" runat="server" CssClass="form-label" ForeColor="black"
                                                                                        CommandName="DoGetPayment">
																								<%# DataBinder.Eval(Container, "DataItem.Description") %>
                                                                                    </asp:LinkButton>
                                                                                    <asp:Label ID="lblPayType" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'
                                                                                        Visible="False">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lblCount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'
                                                                            Visible="True">
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lblAmount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
                                                                        </asp:Label>&nbsp; &nbsp;
                                                                    </td>
                                                                    <td class="GrdHeader" style="display: none" align="right" width="5%">
                                                                        <asp:Label ID="lblPayTypeID" runat="server" Width="0px" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType_PK") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <table style="width:100%" class="table" bordercolor="silver" cellspacing="0" rules="cols" border="1">
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="form-label">Total</asp:Label>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lbl_Count" runat="server" CssClass="form-label"></asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lbl_Amount" runat="server" CssClass="form-label"></asp:Label>
                                                                    </td>
                                                                    <td class="GrdHeader" style="display: none" align="right" width="5%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                </PagerStyle>
                                            </asp:DataGrid>
                                            <%--<table id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td>
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>--%>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">
                                        <div id="HAA1">
                                            <asp:TextBox ID="Textbox3" runat="server" ReadOnly="True">Representative Summary</asp:TextBox>
                                        </div>
                                        <div class="divHidden" id="HAA2">
                                            <asp:TextBox ID="Textbox4" runat="server" ReadOnly="True">Representative Summary</asp:TextBox>
                                        </div>
                                    </h2>
                                    <div class="actions panel_actions pull-right">
                                        <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <table id="Table3" class="table">
                                                <tr style="border: 1px solid #eaeaea">
                                                    <td style="width:7%">
                                                        Rep
                                                    </td>
                                                    <td style="text-align: center;">
                                                        Cash
                                                    </td>
                                                    <td style="text-align: center;">
                                                        Check
                                                    </td>
                                                    <td style="text-align: center;">
                                                        Total
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" colspan="4">
                                                        <asp:DataGrid ID="dgrdPayByRep" CssClass="table" runat="server" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString">
                                                            
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmployeeID" CssClass="form-label" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepID") %>'>
                                                                        </asp:Label>
                                                                        <asp:Label ID="lblEmployee" CssClass="form-label" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepName") %>'>
                                                                        </asp:Label>
                                                                        <asp:LinkButton ID="lnkbtnRepID" runat="server" ForeColor="black" CssClass="form-label"
                                                                            CommandName="DoGetRep">
															                        <%# DataBinder.Eval(Container, "DataItem.RepName") %>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="System">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSystemCash" CssClass="form-label" runat="server" Visible="True" Text=''></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Actual">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblActualCash" runat="server" CssClass="form-label" Text="" Visible="false"></asp:Label>
                                                                        <asp:TextBox ID="txtActualCash" Width="50" CssClass="form-control" Visible="True" MaxLength="10"
                                                                            runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="System">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSystemCheck" CssClass="form-label" runat="server" Visible="true" Text=''></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Actual">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblActualCheck" CssClass="form-label" runat="server" Visible="false" Text=''></asp:Label>
                                                                        <asp:TextBox ID="txtActualCheck" CssClass="form-control" runat="server" Visible="True" MaxLength="10"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="System">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSystemTotal" CssClass="form-label" runat="server" Visible="true" Text=''></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Actual">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblActualTotal" CssClass="form-label" runat="server" Visible="true" Text=''></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                            <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                            </PagerStyle>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td width="22%">
                                                                    Total
                                                                </td>
                                                                <td width="14%">
                                                                    <asp:Label ID="lblTotalCashSys" runat="server" CssClass="form-label"></asp:Label>
                                                                </td>
                                                                <td width="13%">
                                                                    <asp:Label ID="lblTotalCashActual" runat="server" CssClass="form-label"></asp:Label>
                                                                </td>
                                                                <td width="15%">
                                                                    <asp:Label ID="lblTotalCheckSys" runat="server" CssClass="form-label"></asp:Label>
                                                                </td>
                                                                <td width="12%">
                                                                    <asp:Label ID="lblTotalCheckActual" runat="server" CssClass="form-label"></asp:Label>
                                                                </td>
                                                                <td width="13%">
                                                                    <asp:Label ID="lblTotalSystem" runat="server" CssClass="form-label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblTotalActual" runat="server" CssClass="form-label" DESIGNTIMEDRAGDROP="4631"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-xs-12">
                            <section class="box ">
                                    <div class="content-body">

                                        <div class="row">
                                            
                                            <div class="col-md-8 col-sm-9 col-xs-10">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">Notes</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtRemarks" onkeypress="ValidateLenght();" runat="server" ToolTip="Notes" CssClass="form-control" Height="52px" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-8 col-sm-9 col-xs-10">

                                                <div class="form-group">
                                                    <label class="form-label" for="field-1">&nbsp;</label>
                                                    <div class="controls">
                                                        <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary" Text="Update" OnClientClick="MaxLength();"></asp:Button>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">
                                        <div id="HC1">
                                            <asp:TextBox ID="Textbox5" runat="server" ReadOnly="True"> Court Summary</asp:TextBox>
                                        </div>
                                        <div class="divHidden" id="HC2">
                                            <asp:TextBox ID="Textbox6" runat="server" ReadOnly="True"> Court Summary</asp:TextBox>
                                        </div>
                                    </h2>
                                    <div class="actions panel_actions pull-right">
                                        <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:DataGrid ID="dgrdCourt" runat="server" CssClass="table" AutoGenerateColumns="False" ShowFooter="True" OnItemCommand="DoGetQueryString">
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <HeaderTemplate>
                                                            <table class="table" border="0">
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        Court Location
                                                                    </td>
                                                                    <td width="15%">
                                                                        Transactions
                                                                    </td>
                                                                    <td width="20%">
                                                                        Fee Amount
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table class="table" border="1">
                                                                <tr id="trDetail" runat="server">
                                                                    <td valign="top" align="left" width="60%">
                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="416" border="0">
                                                                            <tr>
                                                                                <td id="iParent" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iLink" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iChild" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td class="style1">
                                                                                    &nbsp;<asp:Label ID="lblCourtID" CssClass="form-label" runat="server" Visible="False"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblIsCourtCategory" CssClass="form-label" runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.IsCourtCategory") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblCategoryNumber" CssClass="form-label" runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CategoryNumber") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblCourtName" CssClass="form-label" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:LinkButton ID="lnkbtnCourt" runat="server" ForeColor="black" CssClass="form-label"
                                                                                        CommandName="DoGetCourt">
																		<%# DataBinder.Eval(Container, "DataItem.CourtName") %>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lblTrans" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Trans") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lblFee" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
                                                                        </asp:Label>&nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <table class="table">
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="form-label bold">Total</asp:Label>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lbl_Count" runat="server" CssClass="form-label"></asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lbl_Amount" runat="server" CssClass="form-label"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                </PagerStyle>
                                            </asp:DataGrid>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">
                                        <div id="HB1">
                                            <asp:TextBox ID="Textbox1" runat="server" ReadOnly="True">Transaction Details</asp:TextBox>
                                        </div>
                                        <div class="divHidden" id="HB2">
                                            <asp:TextBox ID="Textbox2" runat="server" ReadOnly="True">Transaction Details</asp:TextBox>
                                        </div>
                                    </h2>
                                    <div class="actions panel_actions pull-right">
                                        <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <table id="Table7" class="table">
                                                <tr>
                                                    <td align="center" bgcolor="#eeeeee" colspan="5" height="8">
                                                        <div class="col-md-4 col-sm-5 col-xs-6">
                                                            <asp:DropDownList ID="cmbRep" runat="server" AutoPostBack="True" CssClass="form-control m-bot15">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-4 col-sm-5 col-xs-6">
                                                            <asp:DropDownList ID="cmbCourt" runat="server" AutoPostBack="True" CssClass="form-control m-bot15">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-4 col-sm-5 col-xs-6">
                                                            <asp:DropDownList ID="cmbPayType" runat="server" AutoPostBack="True"
                                                                CssClass="form-control m-bot15">
                                                            </asp:DropDownList>
                                                        </div>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DataGrid ID="dgrdPayDetail" CssClass="table" runat="server" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString"
                                                            AllowSorting="True" AllowCustomPaging="True" PageSize="100">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="No.">
                                                                    
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTicketID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                                                            Visible="false">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lblCardTypeID" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>'
                                                                            Visible="false">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lblNo" runat="server" CssClass="form-label" Text="" Visible="True"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDate" CssClass="form-label" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Time" SortExpression="sorttime">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTime" CssClass="form-label" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn SortExpression="CustomerName" HeaderText="Client Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCustomer" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                                                            Visible="false">
                                                                        </asp:Label>
                                                                        <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="form-label" ForeColor="black"
                                                                            CommandName="DoGetCustomer">
															                        <%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn SortExpression="Lastname" HeaderText="Rep">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRep" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'
                                                                            Visible="true">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn SortExpression="BondFlag" HeaderText="Bond">
                                                                    
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBond" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'
                                                                            Visible="true">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn SortExpression="ChargeAmount" HeaderText="Paid">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPaidAmount" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount","{0:n2}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Adj1">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Adj2">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn SortExpression="PaymentType" HeaderText="Pmt Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPayTypeFR" runat="server" CssClass="form-label" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                                                            Visible="true">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                                                            Visible="False">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Card">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCardType" CssClass="form-label" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="How Hired">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblHowHired" CssClass="form-label" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.HowHired") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Court">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCourt" CssClass="form-label" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                        
                                                                <asp:TemplateColumn HeaderText="Source">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_MailType" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>'
                                                                            runat="server" CssClass="form-label" Visible="true">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>                                        
                                                                <asp:TemplateColumn HeaderText="Branch">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBranch" Text='<%# DataBinder.Eval(Container, "DataItem.Branch") %>'
                                                                            runat="server" CssClass="form-label" Visible="true">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                            <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                            </PagerStyle>
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" style="height: 20px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">
                                        <div id="Div1">
                                            <asp:TextBox ID="Textbox27" runat="server" ReadOnly="True"> Hiring Source Summary</asp:TextBox>
                                        </div>
                                        <div class="divHidden" id="Div2">
                                            <asp:TextBox ID="Textbox28" runat="server" ReadOnly="True"> Hiring Source Summary</asp:TextBox>
                                        </div>
                                    </h2>
                                    <div class="actions panel_actions pull-right">
                                        <a class="box_toggle fa fa-chevron-down"></a>
                                    </div>
                                </header>
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col-xs-12">

                                            <asp:DataGrid ID="dgMailerSummary" runat="server" AutoGenerateColumns="False" ShowFooter="True" OnItemCommand="DoGetQueryString" CssClass="table" onitemdatabound="dgMailerSummary_ItemDataBound">
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                       <HeaderTemplate>
                                                            <tablestyle="width: 100%; border-collapse: collapse"
                                                                cellspacing="0" rules="cols" border="1">
                                                                <tr>
                                                                    <td align="left" width="60%" colspan="2">
                                                                        Court Category / Hired Source
                                                                    </td>
                                                                    
                                                                     <td align="center" width="20%">
                                                                        Total Clients
                                                                    </td>
                                                                     <td align="right" width="20%">
                                                                        Total Revenue
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate >
                                                               <table style="width: 100%; border-collapse: collapse"
                                                                bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1" runat="server" id="tblSummary">
                                                                <tr id="trDetail" runat="server">
                                                                    <td valign="middle"  align="left" width="40%" runat="server" id="tdCategory" style="display:block">
                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td id="iParent" style="display: none; width: 25px" align="left"  runat="server">
                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server" ToolTip="Collapse"></asp:Image>
                                                                                </td>
                                                                                
                                                                                <td id="iChild" style="display: none; width: 25px" align="left"  runat="server">
                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server" ToolTip="Expand"></asp:Image>
                                                                                </td>
                                                                                <td align="left" >
                                                                                    
                                                                                   
                                                                                    <asp:Label  ID="lblCourtCategory"  ForeColor="black" CssClass="form-label" runat="server"  Style="display: block; "
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CourtCategoryName") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lbl_IsGroup"   runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.IsGroup") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblCourtCategorynum"   runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CourtCategoryNum") %>'>
                                                                                    </asp:Label>
                                                                                </td>
                                                                                <td id="iLink"  style="display: none" align="left"  runat="server">
                                                                               
                                                                                        <asp:TextBox ID="txtToggleImage" runat="server" >1</asp:TextBox>
                                                                                   
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                   <td align="left" runat="server" id="tdMailer" style="display: none">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Image ID="Imagechild2" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblLetterType" CssClass="form-label" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center" width="20%">
                                                                        <asp:Label ID="lblTotalClients" CssClass="form-label" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalClients") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lblTotalRev" CssClass="form-label" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.TotalRev","{0:C}") %>'>
                                                                        </asp:Label>&nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                       
                                                    </asp:TemplateColumn>
                                                   
                                                </Columns>
                                            </asp:DataGrid>

                                            <table>
                                                <tr>
                                                    <td colspan="5" style="display: none">
                                                        <asp:TextBox ID="txtPR" runat="server" Width="0px">1</asp:TextBox>
                                                        <asp:TextBox ID="txtPC" runat="server" Width="2px" Height="18px">1</asp:TextBox>
                                                        <asp:TextBox ID="txtRS" runat="server" Width="0px" Height="18px">1</asp:TextBox>
                                                        <asp:TextBox ID="txtCS" runat="server" Width="0px">1</asp:TextBox>
                                                        <asp:TextBox ID="txtCC" runat="server" Width="0px">0</asp:TextBox>
                                                        <asp:TextBox ID="txtCheck" runat="server" Width="0px">0</asp:TextBox>
                                                        <asp:TextBox ID="iCount" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                                                        <asp:TextBox ID="txtHCJP" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                                                        <asp:TextBox ID="txtOthers" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                                                        <asp:TextBox ID="iCourtCount" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                                                         <asp:TextBox ID="iMailerSummaryCount" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                                                        <asp:TextBox ID="txtMailerSummary" runat="server" Width="0px" Height="18px">1</asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    
                        <!-- MAIN CONTENT AREA ENDS -->
                    </section>
                </section>
                <!-- END CONTENT -->


            </div>
            <!-- END CONTAINER -->

        </form>

        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

        <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
        <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
        <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

        <!-- CORE TEMPLATE JS - START --> 
        <script src="../assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        <script type="text/javascript">
            $(document).ready(function () {
                var today = new Date();
                $('.datepicker').datepicker({
                    setDate: new Date,
                    autoclose: true,

                });
            });
        </script>

    </body>


























<%--<body onload="DefaultLoad();">
    <form id="Form1" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="780" align="center" border="0">
        <tr>
            <td colspan="7">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="frmtd" width="20" height="35">
                            &nbsp;
                        </td>
                        <td class="frmtd" style="width: 45px" align="left" height="35">
                            Date
                        </td>
                        <td align="left" height="35">
                            <ew:CalendarPopup ID="calQueryDate" runat="server" ImageUrl="../images/calendar.gif"
                                Width="90px" ToolTip="Select Report Date" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
                                Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
                                ControlDisplay="TextBoxImage" Font-Names="Tahoma" Font-Size="8pt" AutoPostBack="True">
                                <TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
                                <WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></WeekdayStyle>
                                <MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></MonthHeaderStyle>
                                <OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
                                    BackColor="AntiqueWhite"></OffMonthStyle>
                                <GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></GoToTodayStyle>
                                <TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGoldenrodYellow"></TodayDayStyle>
                                <DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Orange"></DayHeaderStyle>
                                <WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="LightGray"></WeekendStyle>
                                <SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="Yellow"></SelectedDateStyle>
                                <ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                    ForeColor="Black" BackColor="White"></ClearDateStyle>
                                <HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
                                    BackColor="White"></HolidayStyle>
                            </ew:CalendarPopup>
                        </td>
                        <td>
                             <asp:Label ID="lblBranchTitle" runat="server" Text="Branch :" class="frmtd"></asp:Label>
                            <asp:DropDownList ID="ddl_Branch" runat="server" CssClass="clsInputCombo" AutoPostBack="true" OnSelectedIndexChanged="ddl_Branch_SelectedIndexChanged">
                            </asp:DropDownList>
                           
                        </td>
                        <td align="center" style="color: #006699; font-family: Verdana,Arial,Helvetica,sans;
                            font-size: 8pt; font-weight: bold;">
                            <asp:Label ID="lblBranch" runat="server"></asp:Label>
                        </td>
                        <td valign="baseline" align="left" height="35">
                            <table id="TableEmail" style="display: none" cellspacing="0" cellpadding="0" align="right"
                                border="0">
                                <tr>
                                    <td class="frmtd" valign="middle">
                                        Email&nbsp;
                                    </td>
                                    <td style="width: 189px" valign="middle">
                                        <asp:TextBox ID="txtEmail" runat="server" Width="172px" CssClass="TextBox"></asp:TextBox>&nbsp;
                                    </td>
                                    <td style="width: 35px" align="left">
                                        <asp:ImageButton ID="imgbtnSendMail" runat="server" ImageUrl="../Images/SendMail.ICO"
                                            ToolTip="Send Mail"></asp:ImageButton>
                                    </td>
                                    <td align="left">
                                        &nbsp;<img id="imgHide" onclick="ShowEmailBox()" alt="Cancel" src="../Images/DELETE.jpg">
                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                        </td>
                        <td valign="bottom" align="right" width="20%" height="35">
                            &nbsp;&nbsp;<asp:LinkButton ID="lnkbtnPrint" runat="server" ToolTip="Print Report in HTML"
                                CssClass="NormalLink">Print Report</asp:LinkButton>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="frmtd" style="height: 5px" width="20" height="5">
                        </td>
                        <td class="frmtd" style="width: 45px; height: 5px" width="45" height="5">
                        </td>
                        <td style="width: 117px; height: 5px" height="5">
                        </td>
                        <td style="height: 5px" valign="bottom" align="right" height="5">
                        </td>
                        <td style="height: 5px" valign="bottom" align="right" height="5">
                        </td>
                    </tr>
                    <tr>
                        <td class="frmtd" align="center" colspan="5" height="5">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TDHeading" colspan="5">
                <div id="HA1">
                    &nbsp;<span onclick="SetFlag('txtPC',1);show(HA2);show(HA3);hide(HA1);">+</span>&nbsp;&nbsp;<asp:TextBox
                        ID="lblHeading1" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Type Summary</asp:TextBox></div>
                <div class="divHidden" id="HA2">
                    &nbsp;<span onclick="SetFlag('txtPC',0);show(HA1);hide(HA2);hide(HA3);"> -</span>&nbsp;&nbsp;<asp:TextBox
                        ID="lblHeading1_1" runat="server" CssClass="ProductHead" ReadOnly="True">Payment Type Summary</asp:TextBox></div>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="7">
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div class="divHidden" id="HA3">
                    <table id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">
                                <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td colspan="5">
                                            <asp:DataGrid ID="dgrdPayType" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
                                                ShowFooter="True" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString"
                                                CellPadding="0" GridLines="None">
                                                <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                </HeaderStyle>
                                                <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <HeaderTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                cellspacing="0" rules="cols" border="0">
                                                                <tr>
                                                                    <td class="GrdHeader" align="left" width="60%">
                                                                        Payment Method
                                                                    </td>
                                                                    <td class="GrdHeader" align="center" width="15%">
                                                                        Count
                                                                    </td>
                                                                    <td class="GrdHeader" align="center" width="20%">
                                                                        Amount&nbsp;&nbsp;
                                                                    </td>
                                                                    <td class="GrdHeader" style="display: none" align="right" width="5%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1">
                                                                <tr id="trDetail" runat="server">
                                                                    <td valign="top" align="left" width="60%">
                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="200" border="0">
                                                                            <tr>
                                                                                <td id="iParent" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iLink" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iChild" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td width="170">
                                                                                    &nbsp;
                                                                                    <asp:LinkButton ID="lnkbtnDocumentID" runat="server" CssClass="ProductGrdLinks" ForeColor="black"
                                                                                        CommandName="DoGetPayment">
																								<%# DataBinder.Eval(Container, "DataItem.Description") %>
                                                                                    </asp:LinkButton>
                                                                                    <asp:Label ID="lblPayType" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'
                                                                                        Visible="False">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lblCount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TotalCount") %>'
                                                                            Visible="True">
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lblAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
                                                                        </asp:Label>&nbsp; &nbsp;
                                                                    </td>
                                                                    <td class="GrdHeader" style="display: none" align="right" width="5%">
                                                                        <asp:Label ID="lblPayTypeID" runat="server" Width="0px" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType_PK") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                bordercolor="silver" cellspacing="0" rules="cols" border="1">
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="grdheader">Total</asp:Label>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lbl_Count" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lbl_Amount" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                    </td>
                                                                    <td class="GrdHeader" style="display: none" align="right" width="5%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                </PagerStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" height="20">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="TDHeading" colspan="5">
                <div id="HAA1">
                    &nbsp;<span onclick="SetFlag('txtRS',1);show(HAA2);show(HAA3);hide(HAA1);">+</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox3" runat="server" Width="168px" CssClass="ProductHead" ReadOnly="True">Representative Summary</asp:TextBox></div>
                <div class="divHidden" id="HAA2">
                    &nbsp;<span onclick="SetFlag('txtRS',0);show(HAA1);hide(HAA2);hide(HAA3);"> -</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox4" runat="server" Width="168px" CssClass="ProductHead" ReadOnly="True">Representative Summary</asp:TextBox></div>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="7">
            </td>
        </tr>
        <tr>
            <td class="frmtd" colspan="5">
                <div class="divHidden" id="HAA3">
                    <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="frmtd" style="border-top: 1px solid; border-left: 1px solid" valign="top"
                                align="left" width="18%" bgcolor="#eeeeee" colspan="2" height="18">
                                Rep
                            </td>
                            <td class="frmtd" style="border-top: 1px solid" align="center" width="25%" bgcolor="#eeeeee"
                                height="18">
                                Cash
                            </td>
                            <td class="frmtd" style="border-top: 1px solid" align="center" width="25%" bgcolor="#eeeeee"
                                height="18">
                                Check
                            </td>
                            <td class="frmtd" style="border-right: gray 1px solid; border-top: 1px solid" align="center"
                                width="25%" bgcolor="#eeeeee" height="18">
                                Total
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="6">
                                <asp:DataGrid ID="dgrdPayByRep" runat="server" Width="100%" Font-Names="Verdana"
                                    Font-Size="2px" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString">
                                    <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                    </HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmployeeID" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepID") %>'>
                                                </asp:Label>
                                                <asp:Label ID="lblEmployee" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.RepName") %>'>
                                                </asp:Label>
                                                <asp:LinkButton ID="lnkbtnRepID" runat="server" ForeColor="black" CssClass="ProductGrdLinks"
                                                    CommandName="DoGetRep">
															<%# DataBinder.Eval(Container, "DataItem.RepName") %>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="System">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSystemCash" CssClass="GrdLbl" runat="server" Visible="True" Text=''></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Actual">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblActualCash" runat="server" CssClass="GrdLbl" Text="" Visible="false"></asp:Label>
                                                <asp:TextBox ID="txtActualCash" Width="50" CssClass="TextBox" Visible="True" MaxLength="10"
                                                    runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="System">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSystemCheck" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Actual">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblActualCheck" CssClass="GrdLbl" runat="server" Visible="false" Text=''></asp:Label>
                                                <asp:TextBox ID="txtActualCheck" CssClass="TextBox" runat="server" Visible="True"
                                                    Width="50" MaxLength="10"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="System">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSystemTotal" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Actual">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblActualTotal" CssClass="GrdLbl" runat="server" Visible="true" Text=''></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                    </PagerStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td class="frmtd" style="border-right: gray 1px solid; border-left: 1px solid; border-bottom: black 1px solid"
                                colspan="5">
                                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="frmtd" width="22%" height="25">
                                            Total
                                        </td>
                                        <td width="14%" height="25">
                                            <asp:Label ID="lblTotalCashSys" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td width="13%" height="25">
                                            <asp:Label ID="lblTotalCashActual" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td width="15%" height="25">
                                            <asp:Label ID="lblTotalCheckSys" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td width="12%" height="25">
                                            <asp:Label ID="lblTotalCheckActual" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td width="13%">
                                            <asp:Label ID="lblTotalSystem" runat="server" CssClass="Label"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotalActual" runat="server" CssClass="Label" DESIGNTIMEDRAGDROP="4631"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="frmtd" colspan="5">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="frmtd" colspan="5">
                                Notes
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:TextBox ID="txtRemarks" onkeypress="ValidateLenght();" runat="server" Width="100%"
                                    ToolTip="Notes" CssClass="TextArea" Height="52px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" height="7">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:Button ID="btnUpdate" runat="server" CssClass="FrmBtn" Text="Update" OnClientClick="MaxLength();">
                                </asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" height="20">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="TDHeading" colspan="5">
                <div id="HC1">
                    &nbsp;<span onclick="SetFlag('txtCS',1);show(HC2);show(HC3);hide(HC1);">+</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox5" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:TextBox></div>
                <div class="divHidden" id="HC2">
                    &nbsp;<span onclick="SetFlag('txtCS',0);show(HC1);hide(HC2);hide(HC3);"> -</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox6" runat="server" CssClass="ProductHead" ReadOnly="True"> Court Summary</asp:TextBox></div>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="7">
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="5">
                <div class="divHidden" id="HC3">
                    <table id="Table61" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">
                                <table id="Table21" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td colspan="5" height="20">
                                            <asp:DataGrid ID="dgrdCourt" runat="server" Width="100%" Font-Names="Verdana" Font-Size="2px"
                                                border="0" AutoGenerateColumns="False" ShowFooter="True" OnItemCommand="DoGetQueryString"
                                                CellPadding="0" CellSpacing="0">
                                                <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                </HeaderStyle>
                                                <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <HeaderTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                cellspacing="0" rules="cols" border="1">
                                                                <tr>
                                                                    <td class="GrdHeader" align="left" width="60%">
                                                                        Court Location
                                                                    </td>
                                                                    <td class="GrdHeader" align="center" width="15%">
                                                                        Transactions
                                                                    </td>
                                                                    <td class="GrdHeader" align="center" width="20%">
                                                                        Fee Amount&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1">
                                                                <tr id="trDetail" runat="server">
                                                                    <td valign="top" align="left" width="60%">
                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="416" border="0">
                                                                            <tr>
                                                                                <td id="iParent" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iLink" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td id="iChild" style="display: none" align="left" width="10" runat="server">
                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td class="style1">
                                                                                    &nbsp;<asp:Label ID="lblCourtID" CssClass="GrdLbl" runat="server" Visible="False"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CurrentCourtloc") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblIsCourtCategory" CssClass="GrdLbl" runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.IsCourtCategory") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblCategoryNumber" CssClass="GrdLbl" runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CategoryNumber") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblCourtName" CssClass="GrdLbl" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CourtName") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:LinkButton ID="lnkbtnCourt" runat="server" ForeColor="black" CssClass="ProductGrdLinks"
                                                                                        CommandName="DoGetCourt">
																		<%# DataBinder.Eval(Container, "DataItem.CourtName") %>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lblTrans" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Trans") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lblFee" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>'>
                                                                        </asp:Label>&nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                bordercolor="silver" cellspacing="0" rules="cols" border="1">
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="grdheader">Total</asp:Label>
                                                                    </td>
                                                                    <td align="center" width="15%">
                                                                        <asp:Label ID="lbl_Count" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lbl_Amount" runat="server" CssClass="GrdLbl"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                                </PagerStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px" colspan="5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 2px" valign="top" colspan="5">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="TDHeading" colspan="5">
                <div id="HB1">
                    &nbsp;<span onclick="SetFlag('txtPR',1);show(HB2);show(HB3);hide(HB1);">+</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox1" runat="server" CssClass="ProductHead" ReadOnly="True">Transaction Details</asp:TextBox></div>
                <div class="divHidden" id="HB2">
                    &nbsp;<span onclick="SetFlag('txtPR',0);show(HB1);hide(HB2);hide(HB3);"> -</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox2" runat="server" CssClass="ProductHead" ReadOnly="True">Transaction Details</asp:TextBox></div>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="7">
            </td>
        </tr>
        <tr>
            <td colspan="5" height="3">
                <div class="divHidden" id="HB3">
                    <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="border-right: gray 1px solid; border-top: 1px solid; border-left: 1px solid;
                                height: 8px" align="center" bgcolor="#eeeeee" colspan="5" height="8">
                                <asp:DropDownList ID="cmbRep" runat="server" Width="100px" AutoPostBack="True" CssClass="frmtd">
                                </asp:DropDownList>
                                &nbsp;&nbsp;
                                <asp:DropDownList ID="cmbPayType" runat="server" Width="280px" AutoPostBack="True"
                                    CssClass="frmtd">
                                </asp:DropDownList>
                                &nbsp;&nbsp;
                                <asp:DropDownList ID="cmbCourt" runat="server" Width="150px" AutoPostBack="True"
                                    CssClass="frmtd">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgrdPayDetail" runat="server" Width="100%" Font-Names="Verdana"
                                    Font-Size="2px" AutoGenerateColumns="False" OnItemCommand="DoGetQueryString"
                                    AllowSorting="True" AllowCustomPaging="True" PageSize="100">
                                    <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                    </HeaderStyle>
                                    <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No.">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.TicketID") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:Label ID="lblCardTypeID" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CardType") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:Label ID="lblNo" runat="server" CssClass="GrdLbl" Text="" Visible="True"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Date">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDate" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Time" SortExpression="sorttime">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTime" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.RecTime") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="CustomerName" HeaderText="Client Name">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomer" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="ProductGrdLinks" ForeColor="black"
                                                    CommandName="DoGetCustomer">
															<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="Lastname" HeaderText="Rep">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblRep" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.Lastname") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="BondFlag" HeaderText="Bond">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBond" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'
                                                    Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="ChargeAmount" HeaderText="Paid">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaidAmount" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount","{0:n2}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Adj1">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj1") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Adj2">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.adj2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="PaymentType" HeaderText="Pmt Type">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="TaskGrid_Head"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPayTypeFR" runat="server" CssClass="GrdLbl" Text='<%# DataBinder.Eval(Container, "DataItem.CCType") %>'
                                                    Visible="true">
                                                </asp:Label>
                                                <asp:Label ID="lblPTypeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentType") %>'
                                                    Visible="False">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Card">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCardType" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.ChargeAmount") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="How Hired">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblHowHired" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.HowHired") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Court">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourt" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.Court") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                        </asp:TemplateColumn>
                                        
                                        <asp:TemplateColumn HeaderText="Source">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_MailType" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>'
                                                    runat="server" CssClass="GrdLbl" Visible="true">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>                                        
                            <asp:TemplateColumn HeaderText="Branch">
                                <HeaderStyle HorizontalAlign="Left" CssClass="GrdHeader"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBranch" Text='<%# DataBinder.Eval(Container, "DataItem.Branch") %>'
                                        runat="server" CssClass="GrdLbl" Visible="true">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle NextPageText="" PrevPageText="" HorizontalAlign="Center" Mode="NumericPages">
                                    </PagerStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="height: 20px">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="TDHeading" colspan="5">
                <div id="Div1">
                    &nbsp;<span onclick="SetFlag('txtMailerSummary',1);show(Div2);show(Div3);hide(Div1);">+</span>&nbsp;&nbsp;<asp:TextBox
                        ID="Textbox27" runat="server" CssClass="ProductHead" ReadOnly="True"> Hiring Source Summary</asp:TextBox></div>
                <div class="divHidden" id="Div2">
                    &nbsp;<span onclick="SetFlag('txtMailerSummary',0);show(Div1);hide(Div2);hide(Div3);">
                        -</span>&nbsp;&nbsp;<asp:TextBox ID="Textbox28" runat="server" CssClass="ProductHead"
                            ReadOnly="True"> Hiring Source Summary</asp:TextBox></div>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="7">
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="5">
                <div  id="Div3">
                    <table id="Table8" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top" align="left">
                                <table id="Table9" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td colspan="5" height="20">
                                             <asp:DataGrid ID="dgMailerSummary" runat="server" Width="100%" Font-Names="Verdana"
                                                Font-Size="2px" AutoGenerateColumns="False" ShowFooter="True" OnItemCommand="DoGetQueryString"
                                                border="0" cellpadding="0" cellspacing = "0" 
                                                onitemdatabound="dgMailerSummary_ItemDataBound">
                                                <AlternatingItemStyle BackColor="#EEEEEE"></AlternatingItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="GrdHeader" VerticalAlign="Middle">
                                                </HeaderStyle>
                                                <FooterStyle CssClass="GrdFooter"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                       <HeaderTemplate>
                                                            <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                cellspacing="0" rules="cols" border="1">
                                                                <tr>
                                                                    <td class="GrdHeader" align="left" width="60%" colspan="2">
                                                                        Court Category / Hired Source
                                                                    </td>
                                                                    
                                                                     <td class="GrdHeader" align="center" width="20%">
                                                                        Total Clients
                                                                    </td>
                                                                     <td class="GrdHeader" align="right" width="20%">
                                                                        Total Revenue
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <ItemTemplate >
                                                               <table style="font-size: 2px; width: 100%; font-family: Verdana; border-collapse: collapse"
                                                                bordercolor="gainsboro" cellspacing="0" cellpadding="0" rules="all" border="1" runat="server" id="tblSummary">
                                                                <tr id="trDetail" runat="server">
                                                                    <td valign="middle"  align="left" width="40%" runat="server" id="tdCategory" style="display:block">
                                                                        <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                            border-bottom-style: none" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td id="iParent" style="display: none; width: 25px" align="left"  runat="server">
                                                                                    <asp:Image ID="imgParent" ImageUrl="../Images/folderopen.gif" runat="server" ToolTip="Collapse"></asp:Image>
                                                                                </td>
                                                                                
                                                                                <td id="iChild" style="display: none; width: 25px" align="left"  runat="server">
                                                                                    <asp:Image ID="imgChild" ImageUrl="../Images/folder.gif" runat="server" ToolTip="Expand"></asp:Image>
                                                                                </td>
                                                                                <td align="left" >
                                                                                    
                                                                                   
                                                                                    <asp:Label  ID="lblCourtCategory"  ForeColor="black" CssClass="ProductGrdLinks" runat="server"  Style="display: block; "
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CourtCategoryName") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lbl_IsGroup"   runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.IsGroup") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblCourtCategorynum"   runat="server" Style="display: none"
                                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.CourtCategoryNum") %>'>
                                                                                    </asp:Label>
                                                                                </td>
                                                                                <td id="iLink"  style="display: none" align="left"  runat="server">
                                                                               
                                                                                        <asp:TextBox ID="txtToggleImage" runat="server" >1</asp:TextBox>
                                                                                   
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                   <td align="left" runat="server" id="tdMailer" style="display: none">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Image ID="imgLink" ImageUrl="../Images/T.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Image ID="Imagechild2" ImageUrl="../Images/folder.gif" runat="server"></asp:Image>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblLetterType" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.MailType") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center" width="20%">
                                                                        <asp:Label ID="lblTotalClients" CssClass="GrdLbl" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.TotalClients") %>'>
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td align="right" width="20%">
                                                                        <asp:Label ID="lblTotalRev" CssClass="GrdLbl" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container, "DataItem.TotalRev","{0:C}") %>'>
                                                                        </asp:Label>&nbsp;&nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                       
                                                    </asp:TemplateColumn>
                                                   
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px" colspan="5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 2px" valign="top" colspan="5">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="display: none">
                <asp:TextBox ID="txtPR" runat="server" Width="0px">1</asp:TextBox><asp:TextBox ID="txtPC"
                    runat="server" Width="2px" Height="18px">1</asp:TextBox><asp:TextBox ID="txtRS" runat="server"
                        Width="0px" Height="18px">1</asp:TextBox><asp:TextBox ID="txtCS" runat="server" Width="0px">1</asp:TextBox><asp:TextBox
                            ID="txtCC" runat="server" Width="0px">0</asp:TextBox><asp:TextBox ID="txtCheck" runat="server"
                                Width="0px">0</asp:TextBox><asp:TextBox ID="iCount" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                <asp:TextBox ID="txtHCJP" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                <asp:TextBox ID="txtOthers" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                <asp:TextBox ID="iCourtCount" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                 <asp:TextBox ID="iMailerSummaryCount" runat="server" Width="0px" Height="18px">0</asp:TextBox>
                <asp:TextBox ID="txtMailerSummary" runat="server" Width="0px" Height="18px">1</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
            </td>
        </tr>
    </table>
    </form>
</body>--%>
</html>

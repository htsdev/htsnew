using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;

namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for Arraignment.
	/// </summary>
	public partial class Arraignment : System.Web.UI.Page
	{

        protected System.Web.UI.WebControls.DataGrid dg_Arraignment;
        protected System.Web.UI.WebControls.Label lblMessage;

			clsENationWebComponents ClsDb = new clsENationWebComponents();
		
			clsSession ClsSession = new clsSession();
		clsLogger clog = new clsLogger();
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (ClsSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					
					DataSet ds_Arraignment =  ClsDb.Get_DS_BySP ("USP_HTS_Get_pending_trial_sets");
					dg_Arraignment.DataSource  = ds_Arraignment;
					dg_Arraignment.DataBind();
					if (dg_Arraignment.Items.Count == 0)
					{
                        LblSucessdiv.Attributes.Add("class", "alert alert-danger alert-dismissable fade in");
                        LblSucesstext.Text = "No Records";
                        LblSucessdiv.Visible = true;
                        //lblMessage.Text = "No Records";
					}
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_Arraignment.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgArraignment_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		private void dgArraignment_ItemDataBound(object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{

				DateTime VgridDate = Convert.ToDateTime(((Label) e.Item.FindControl("LblCourtDate")).Text);
				int a = DateTime.Compare(VgridDate.Date,System.DateTime.Now.Date);
				if  (a == 0) 
				{
					e.Item.BackColor = System.Drawing.Color.YellowGreen;
						
				}
					
			}
		}

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
        protected void dg_Arraignment_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
}
}

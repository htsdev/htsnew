<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.Dockets1" Codebehind="Dockets1.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Dockets1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<SCRIPT src="../Scripts/dates.js" type="text/javascript"></SCRIPT>
		<script language="javascript">
		function validate()
		{
			//a;
			var d1 = document.getElementById("calstartdate").value;
			var d2 = document.getElementById("calenddate").value;			
			
			if (d1 == d2)
				return true;
			if (compareDates(d2,'MM/dd/yyyy',d1,'MM/dd/yyyy')==false)
				{
					alert("start date cannot greater than end date");
					document.getElementById("calenddate").focus(); 
					return false;
				}
				return true;
		}
		function CalendarValidation()
		{ 
			var dtStartdate=document.getElementById("calstartdate").value;
			var dtEndDate=document.getElementById("calenddate").value;
			if(dtStartdate > dtEndDate)
			{ 
				alert("PLease Specify Valid Future Date!");
				return false;
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<script>
		
		function PopUpCallBack(DocID)
			{
				
				window.open ("processPDF.aspx?docid="+ DocID);
				return false;
			
			}
		
		</script>
		<FORM id="Form1" method="post" runat="server">
			<TABLE id="TableMain"
				cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
				<TR>
					<TD style="WIDTH: 815px" colSpan="4">
						<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 816px" colSpan="4">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="10"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 46px" colSpan="4">
									<TABLE id="Table2" style="WIDTH: 784px; HEIGHT: 14px" cellSpacing="1" cellPadding="1" width="784"
										border="0">
										<TR>
											<TD class="clsLeftPaddingTable" style="WIDTH: 198px; HEIGHT: 23px"><STRONG>Search By Appearance</STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 57px; HEIGHT: 23px"></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px; HEIGHT: 23px"></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 92px; HEIGHT: 23px"></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 137px; HEIGHT: 23px"></TD>
											<TD class="clsLeftPaddingTable" style="HEIGHT: 23px"></TD>
										</TR>
										<TR>
											<TD class="clsLeftPaddingTable" style="WIDTH: 198px"><STRONG>Start Date</STRONG>&nbsp;:&nbsp;
												<ew:calendarpopup id="calstartdate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True"
													ShowClearDate="True" Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left"
													ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage" Width="99px" Font-Size="8pt" Font-Names="Tahoma">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 57px"><STRONG>End Date:</STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 131px"><ew:calendarpopup id="calenddate" runat="server" ToolTip="Call Back Date" PadSingleDigits="True" ShowClearDate="True"
													Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Left" ImageUrl="../images/calendar.gif" ControlDisplay="TextBoxImage"
													Width="102px" Font-Size="8pt" Font-Names="Tahoma">
													<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
													<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></WeekdayStyle>
													<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></MonthHeaderStyle>
													<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
														BackColor="AntiqueWhite"></OffMonthStyle>
													<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></GoToTodayStyle>
													<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGoldenrodYellow"></TodayDayStyle>
													<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Orange"></DayHeaderStyle>
													<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="LightGray"></WeekendStyle>
													<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="Yellow"></SelectedDateStyle>
													<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></ClearDateStyle>
													<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
														BackColor="White"></HolidayStyle>
												</ew:calendarpopup></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 92px"><STRONG>Attorney Name:</STRONG></TD>
											<TD class="clsLeftPaddingTable" style="WIDTH: 137px"><asp:dropdownlist id="ddl_attorney" runat="server" Width="127px" CssClass="ClsInputCombo"></asp:dropdownlist></TD>
											<TD class="clsLeftPaddingTable"><asp:button id="btnSearch" runat="server" CssClass="clsbutton" Text="Submit"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 11px" align="left" background="../Images/separator_repeat.gif" colSpan="4">&nbsp;
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="4"><asp:datagrid id="dgresult" runat="server" Width="784px" CssClass="clsleftpaddingtable" AutoGenerateColumns="False"
										BorderColor="White" BorderStyle="None">
										<Columns>
											<asp:TemplateColumn HeaderText="S.No">
												<HeaderStyle HorizontalAlign="Left" Width="50px" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id="lbl_sno" runat="server" CssClass="label"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Scan Date">
												<HeaderStyle HorizontalAlign="Left" Width="200px" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:HyperLink id=lnk_date runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntered") %>' Visible="False">
													</asp:HyperLink>
													<asp:LinkButton id=lnkbtn_date runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntered") %>' Visible="False">
													</asp:LinkButton>
													<asp:Label id=lblScanDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntered") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Doc Date">
												<HeaderStyle HorizontalAlign="Left" Width="200px" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lblDocDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.dateofdoc", "{0:d}") %>' Visible="False">
													</asp:Label>
													<asp:LinkButton id=lnkbtn_docdate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dateofdoc", "{0:d}") %>'>
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="100px"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_pageno runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.DocNum") %>'>
													</asp:Label>
													<asp:Label id=lbl_docid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.doc_id") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Attorney Name">
												<HeaderStyle HorizontalAlign="Left" Width="300px" CssClass="clsaspcolumnheader"></HeaderStyle>
												<ItemTemplate>
													<asp:Label id=lbl_attname runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Importance") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="100%" background="../Images/separator_repeat.gif" colSpan="4" height="11"></TD>
							</TR>
							<TR>
								<TD align="center" width="100%" colSpan="4" height="11">
									<asp:label id="lbl_error" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							
							<TR>
								<TD style="WIDTH: 760px" align="left" colSpan="4">
									<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components.Entities;
using HTP.Components.Services;


namespace HTP.Activities
{
    public partial class jurytrial : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger bugTracker = new clsLogger();
        //Yasir Kamal 6279 08/05/2009 dataBase routines moved from presentation layer to Business Logic components.
        AttorneyTrials AtrnyTrail = new AttorneyTrials();
        AttorneyTrialsService AtrnyTrailService = new AttorneyTrialsService();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LblSucessdiv.Visible = false;
                    LblSucesstext.Text = "";
                    ddl_court.Items.Clear();
                    ddl_attorney.Items.Clear();

                    ///////////  Fill drop down with cournames
                    DataSet ds = AtrnyTrailService.GetCourtName();
                    int i = 1;
                    ddl_court.Items.Add("------------- Select ------------");
                    ddl_court.Items[0].Value = "0";

                    foreach (DataRow rw in ds.Tables[0].Rows)
                    {
                        ddl_court.Items.Add(rw["courtname"].ToString());
                        ddl_court.Items[i].Value = rw["courtid"].ToString();
                        i++;
                    }

                    ///////// fill drop down with firtsname+lastname from tblusers
                    DataSet dsattorney = AtrnyTrailService.GetAttorney();
                    i = 1;
                    ddl_attorney.Items.Add("---- Select ----");
                    ddl_attorney.Items[0].Value = "0";

                    foreach (DataRow rw in dsattorney.Tables[0].Rows)
                    {
                        ddl_attorney.Items.Add(rw["name"].ToString());
                        ddl_attorney.Items[i].Value = rw["EmployeeID"].ToString();
                        i++;
                    }

                    hf_rowid.Value= Request.QueryString["id"];
                    if (hf_rowid.Value != "" && hf_rowid.Value != null)
                    {
                        AtrnyTrail.RowId = Convert.ToInt64(hf_rowid.Value);

                        DataTable dt = AtrnyTrailService.GetJuryTrialById(AtrnyTrail);

                        if (dt.Rows.Count > 0)
                        {
                            
                            calQueryFrom.SelectedDate = Convert.ToDateTime(dt.Rows[0]["recdate"]);
                            ddl_attorney.SelectedValue = dt.Rows[0]["attorney"].ToString();
                            txt_prosecutor.Text = dt.Rows[0]["prosecutor"].ToString();
                            txt_officer.Text = dt.Rows[0]["officer"].ToString();
                            txt_defendant.Text = dt.Rows[0]["defendant"].ToString();
                            txt_caseno.Text = dt.Rows[0]["caseno"].ToString();
                            txt_ticketno.Text = dt.Rows[0]["ticketno"].ToString();
                            ddl_court.SelectedValue = dt.Rows[0]["courtid"].ToString();
                            txt_judge.Text = dt.Rows[0]["judge"].ToString();
                            txt_brieffacts.Text = dt.Rows[0]["brieffacts"].ToString();
                            ddl_verdict.SelectedValue = dt.Rows[0]["verdictid"].ToString();
                            txt_fine.Text = dt.Rows[0]["fine"].ToString();

                            txt_fine.Text = string.Format("{0:g}", Convert.ToDouble(txt_fine.Text));                            
                            txt_creporter.Text = dt.Rows[0]["courtreport"].ToString();

                            btn_submit.Text = "Update";
                            hf_status.Value = "Update";

                            btn_reset.Visible = false;
                        }
                    }
                    else
                    {
                        btn_submit.Text = "Submit";
                        hf_status.Value = "New";
                        calQueryFrom.SelectedDate = DateTime.Now;
                        
                    }
                }
                btn_submit.Attributes.Add("OnClick", "return ValidateForm();");
                //btn_reset.Attributes.Add("OnClick", "return ClearControls();");
                
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
                
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {
                //////////// Update the reacord 
                if (hf_status.Value == "Update")
                {
                    AtrnyTrail.RowId = Convert.ToInt64(hf_rowid.Value);
                    AtrnyTrail.Date = calQueryFrom.SelectedDate;
                    AtrnyTrail.Attorney = Convert.ToInt32(ddl_attorney.SelectedValue);
                    AtrnyTrail.Prosecutor = txt_prosecutor.Text;
                    AtrnyTrail.Officer = txt_officer.Text;
                    AtrnyTrail.Defendant = txt_defendant.Text;
                    AtrnyTrail.CaseNo = txt_caseno.Text;
                    AtrnyTrail.TicketNo = txt_ticketno.Text;
                    AtrnyTrail.CourtId = Convert.ToInt32(ddl_court.SelectedValue);
                    AtrnyTrail.Judge = txt_judge.Text;
                    AtrnyTrail.BriefFacts = txt_brieffacts.Text;
                    AtrnyTrail.VerdictId = Convert.ToInt32(ddl_verdict.SelectedValue);
                    AtrnyTrail.Verdict = ddl_verdict.SelectedItem.Text;
                    AtrnyTrail.Fine = Convert.ToDouble(txt_fine.Text);
                    AtrnyTrail.CourtReport = txt_creporter.Text;

                    AtrnyTrailService.UpdateJuryTrial(AtrnyTrail);

                    lblMessage.Text = "Record has been updated";
                    LblSucessdiv.Visible = true;
                    LblSucesstext.Text = "Record has been updated";
                    btn_submit.Text = "Submit";
                    hf_rowid.Value = "";
                    hf_status.Value = "New";
                    ClearControls();
                    Response.Redirect("../Reports/frmRptjurytrial.aspx?sMenu=111");
                }
                else
                {
                    AtrnyTrail.RowId = 1;
                    AtrnyTrail.Date = calQueryFrom.SelectedDate;
                    AtrnyTrail.Attorney = Convert.ToInt32(ddl_attorney.SelectedValue);
                    AtrnyTrail.Prosecutor = txt_prosecutor.Text;
                    AtrnyTrail.Officer = txt_officer.Text;
                    AtrnyTrail.Defendant = txt_defendant.Text;
                    AtrnyTrail.CaseNo = txt_caseno.Text;
                    AtrnyTrail.TicketNo = txt_ticketno.Text;
                    AtrnyTrail.CourtId = Convert.ToInt32(ddl_court.SelectedValue);
                    AtrnyTrail.Judge = txt_judge.Text;
                    AtrnyTrail.BriefFacts = txt_brieffacts.Text;
                    AtrnyTrail.VerdictId = Convert.ToInt32(ddl_verdict.SelectedValue);
                    AtrnyTrail.Verdict =  ddl_verdict.SelectedItem.Text;
                    AtrnyTrail.Fine = Convert.ToDouble(txt_fine.Text);
                    AtrnyTrail.CourtReport = txt_creporter.Text; 

                    ///////////// Insert new record

                    AtrnyTrailService.InsertJuryTrial(AtrnyTrail);

                    lblMessage.Text = "Record has been saved";
                    btn_submit.Text = "Submit";
                    LblSucessdiv.Visible = true;
                    LblSucesstext.Text = "Record has been saved";
                    
                    ClearControls();
                }
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }

        }
        private void ClearControls()
        {
            ddl_attorney.SelectedIndex = 0;
            ddl_attorney.SelectedIndex = 0;
            txt_officer.Text = "";
            txt_defendant.Text = "";
            txt_caseno.Text = "";
            txt_ticketno.Text = "";
            ddl_court.SelectedIndex = 0;
            txt_judge.Text = "";
            txt_brieffacts.Text = "";
            ddl_verdict.SelectedIndex = 0;
            txt_fine.Text = "";
            txt_creporter.Text = "";
            txt_prosecutor.Text = "";
            calQueryFrom.SelectedDate = DateTime.Now;
            hf_rowid.Value = "";
            hf_status.Value = "New";
            btn_submit.Text = "Submit";
        }

        protected void btn_reset_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                LblSucessdiv.Visible = false;
                LblSucesstext.Text = "";
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                bugTracker.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }
    }
}

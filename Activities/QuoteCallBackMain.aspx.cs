using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Activities
{
    // Noufil 5894 06/08/2009 code modified according to standard.
    public partial class QuoteCallBackMain : System.Web.UI.Page
    {
        #region Declaration

        clsSession ClsSession = new clsSession();
        CaseType clscasetype = new CaseType();
        clsQuoteCallBack clsquote = new clsQuoteCallBack();

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion        

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    //ozair 6934 01/22/2010 page method implemented
                    Navigation1.PageMethod += delegate()
                    {
                        SearchQCB();
                    };

                    if (!IsPostBack)
                    {
                        btnSearch.Attributes.Add("Onclick", "return SearchValidation();");
                        BindControls();
                        //Yasir Kamal 5744 04/06/2009 call quote back changes.
                        calFrom1.Reset();
                        calTo1.Reset();


                        if (Convert.ToString(DateTime.Now.AddDays(1).DayOfWeek) == "Friday")
                        {
                            calFrom1.SelectedDate = DateTime.Now.AddDays(1);
                            calTo1.SelectedDate = DateTime.Now.AddDays(3);
                        }
                        else
                        {
                            calFrom1.SelectedDate = DateTime.Now.AddDays(1);
                            calTo1.SelectedDate = DateTime.Now.AddDays(1);
                        }

                        ViewState["from"] = calFrom1.SelectedDate.ToShortDateString();
                        ViewState["to"] = calTo1.SelectedDate.ToShortDateString();
                        calFrom3.Reset();

                        calTo3.Reset();
                        CallDateCalender.SelectedDate = DateTime.Today;
                        ViewState["date"] = CallDateCalender.SelectedDate.ToShortDateString();
                        // Noufil 4487 08/06/2008 show records on pageload with traffic case and Day Before Court Date List
                        dd_casetype.SelectedValue = "1";
                        rlstQuoteType.SelectedIndex = 0;
                        // Noufil 5894 06/08/2009 set case type dynamically.
                        DataTable dt_casetype = clscasetype.GetAllCaseType();
                        dd_casetype.DataSource = clscasetype.GetAllCaseType();
                        dd_casetype.DataBind();
                        dd_casetype.Items.Insert(0, new ListItem("All", "-1"));
                        dd_casetype.SelectedIndex = 1;

                        BindGrid(1);
                    }
                    Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                    Pagingctrl.GridView = gv_Quote;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btnSearch_Click1(object sender, EventArgs e)
        {
            try
            {
                SearchQCB();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_Quote_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Fahad 6934 12/16/2009 Added following lines in RowCommand Event
            if (e.CommandName == "Click")
            {
                clsQuoteCallBack objQuoteCallback = new clsQuoteCallBack();
                Navigation1.StartinTicketID = Convert.ToInt32(e.CommandArgument);
                Navigation1.DT_TicketIDs = (DataTable)(Session["dt_Control"]);
                Navigation1.ModalPopUpID = mpeQuoteCall.ClientID;
                Navigation1.ShowDataWithStartinTicketID();
                mpeQuoteCall.Show();
            }
        }
        
        //Ozair 7496 03/03/2010 Removed Row Data Bound event

        protected void gv_Quote_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Quote.PageIndex = e.NewPageIndex;
                BindGrid(Convert.ToInt32(rlstQuoteType.SelectedValue));
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        #endregion

        #region Methods

        void Pagingctrl_PageIndexChanged()
        {
            gv_Quote.PageIndex = Pagingctrl.PageIndex - 1;
            BindGrid(Convert.ToInt32(rlstQuoteType.SelectedValue));
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            if (pageSize > 0)
            {
                gv_Quote.PageIndex = 0;
                gv_Quote.PageSize = pageSize;
                gv_Quote.AllowPaging = true;
            }
            else
            {
                gv_Quote.AllowPaging = false;
            }
            BindGrid(Convert.ToInt32(rlstQuoteType.SelectedValue));
        }

        private void BindControls()
        {
            // Geting Options For Radio List             
            rlstQuoteType.DataSource = clsquote.GetAllQuoteCallBackQuery();            
            rlstQuoteType.DataBind();
        }

        private void SearchQCB()
        {
            // Enabling or Disabling Date Controls on the Form
            int intoptSearchID;
            DateTime dt = new DateTime();

            // Checking Search Option
            if (rlstQuoteType.SelectedValue == "")
                intoptSearchID = -1; 	// Set To -1 If Not Select Option
            else
                intoptSearchID = Convert.ToInt32(rlstQuoteType.SelectedValue);

            if (intoptSearchID > 0)
            {
                if (calFrom1.SelectedDate.ToShortDateString() == "01/01/0001" || calFrom1.SelectedDate.ToShortDateString() == "1/1/0001")
                {
                    if (intoptSearchID == 1)
                    {
                        dt = DateTime.Now.AddDays(1);
                        calFrom1.SelectedDate = dt;
                        calTo1.SelectedDate = dt;
                    }

                    calFrom1.VisibleDate = calFrom1.SelectedDate;
                    calTo1.VisibleDate = calTo1.SelectedDate;
                }

                if (calFrom3.SelectedDate.ToShortDateString() == "01/01/0001" || calFrom3.SelectedDate.ToShortDateString() == "1/1/0001")
                {
                    if (intoptSearchID == 3)
                    {
                        dt = DateTime.Now.AddDays(4);
                        calFrom3.SelectedDate = DateTime.Now.AddDays(1);
                        calTo3.SelectedDate = dt;
                    }

                    calFrom3.VisibleDate = calFrom3.SelectedDate;
                    calTo3.VisibleDate = calTo3.SelectedDate;
                }
                BindGrid(intoptSearchID);
            }
        }

        private void BindGrid(int Criteria)
        {
            // Getting Data For Displaying in Grid 
            try
            {
                DataTable dt = new DataTable();
                dt = clsquote.GetQuoteCallBack(Criteria, calFrom1.SelectedDate, calTo1.SelectedDate, CallDateCalender.SelectedDate, Convert.ToInt32(dd_casetype.SelectedValue.ToString()));
                //Fahad 6934 12/17/2009 Sved datatable in session in order to assigned to QuoteCall Modal Popup 
                Session["dt_Control"] = dt;
                if (dt.Rows.Count > 0)
                {
                    if (dt.Columns.Contains("sno") == false)
                    {
                        dt.Columns.Add("sno");
                        int sno = 1;
                        if (dt.Rows.Count >= 1)
                            dt.Rows[0]["sno"] = 1;
                        if (dt.Rows.Count >= 2)
                        {
                            for (int i = 1; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i - 1]["ticketid_pk"].ToString() != dt.Rows[i]["ticketid_pk"].ToString())
                                {
                                    dt.Rows[i]["sno"] = ++sno;
                                }
                                else
                                    dt.Rows[i]["sno"] = sno;
                            }
                        }
                    }

                    gv_Quote.Visible = true;
                    lblMessage.Visible = false;
                    Pagingctrl.Visible = true;
                    gv_Quote.DataSource = dt;
                    gv_Quote.DataBind();
                    Pagingctrl.PageCount = gv_Quote.PageCount;
                    Pagingctrl.PageIndex = gv_Quote.PageIndex;
                    Pagingctrl.SetPageIndex();

                }
                else
                {
                    lblMessage.Text = "No Records Found";
                    lblMessage.Visible = true;
                    Pagingctrl.Visible = false;
                    gv_Quote.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion
    }
}




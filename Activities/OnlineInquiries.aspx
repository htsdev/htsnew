﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineInquiries.aspx.cs" 
    Inherits="HTP.Activities.OnlineInquiries" %>
<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>
<%@ Register TagPrefix="uc4" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%--<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>--%>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagName="PolmControl" TagPrefix="uc2" Src="~/WebControls/PolmControl.ascx" %>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Leads</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->






    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <style type="text/css">
        .Labelfrmmain, table.table td span.form-label {
            font-size: 14px !important;
        }

        .Labelfrmmain, table.table td span.form-label-blue {
            font-size: 14px !important;
            color: #11a2cf !important;
        }

        .clssubhead {
            /* font-weight: bold; */
            font-size: 14px !important;
            color: #3366cc !important;
            font-weight:normal !important;
            text-decoration: none !important;;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            $('.input-group-addon').click(function () {
                $(this).closest('div').find('input').focus();
            });

            $('.datepicker').datepicker({ autoclose: true });
        }
        $('.datepicker').datepicker({ autoclose: true });

        function checkradio() {

            if (document.getElementById("rb_report_0").checked == true) {
                document.getElementById("tr_image").style.display = 'none';
                document.getElementById("tr_image2").style.display = 'block';
            }
            if (document.getElementById("rb_report_1").checked == true) {
                document.getElementById("tr_image2").style.display = 'none';
                document.getElementById("tr_image").style.display = 'block';
            }

        }

        function checkcomments(type) {
            var datelenght;
            if (type == 1) {
                var comm = document.getElementById("txt_onlinecomment").value;
                var comm_lenght = document.getElementById("txt_onlinecomment").value.length;
                var oldcomm_lenght = document.getElementById("hf_onlineoldcomments").value.length;
            }
            else if (type == 0) {
                var comm = document.getElementById("txt_comment").value;
                var comm_lenght = document.getElementById("txt_comment").value.length;
                var oldcomm_lenght = document.getElementById("hf_oldcomments").value.length;
            }
            else if (type == 2) {
                var comm = document.getElementById("tbContactRepComments").value;
                var comm_lenght = document.getElementById("tbContactRepComments").value.length;
                var oldcomm_lenght = document.getElementById("hfContactOldComments").value.length;
            }

            if (comm_lenght == 0) {
                //alert("Please Enter Comments")
                $("#txtErrorMessage").text("Please Enter Comments");
                $("#errorAlert").modal();
                return false;
            }

            if (CheckName(comm) == false) {
                // alert("Please enter valid comments.");
                $("#txtErrorMessage").text("Please Enter Comments");
                $("#errorAlert").modal();
                return false;
            }
            if (comm_lenght > 0)
                dateLenght = 27
            else
                dateLenght = 0

            if ((oldcomm_lenght + comm_lenght) > 5000 - dateLenght)//-27 bcoz to show last time partconcatenated with comments
            {
                //alert("Sorry You cannot type in more than 5000 characters in contact comments")
                $("#txtErrorMessage").text("Sorry You cannot type in more than 5000 characters in contact comments");
                $("#errorAlert").modal();
                return false;
            }

            //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
            return CheckDate();
        }

        function CheckName(name) {
            for (i = 0 ; i < name.length ; i++) {
                var asciicode = name.charCodeAt(i)
                //If not valid alphabet 
                if ((asciicode == 60) || (asciicode == 62))
                    return false;
            }
        }

        function showHideDateRange(chkPending) {
            var dateRange = document.getElementById("tblContactUs");

            if (document.getElementById("rb_report_1").checked) {
                dateRange.style.display = "block";
                document.getElementById("cal_EffectiveFrom").disabled = true;

                if (chkPending == 1)
                    document.getElementById("dd_callback").value = "0";
            }
            else {
                dateRange.style.display = "none";
                document.getElementById("cal_EffectiveFrom").disabled = false;
            }
        }

        //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
        function DateDiff(date1, date2) {
            var one_day = 1000 * 60 * 60 * 24;
            var objDate1 = new Date(date1);
            var objDate2 = new Date(date2);
            return (objDate1.getTime() - objDate2.getTime()) / one_day;
        }

        function CheckDate() {
            today = new Date();
            seldate = document.getElementById("calfollowupdatenew").value;
            var diff = Math.ceil(DateDiff(seldate, today));
            var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
            var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth() + 1) + "/" + Date.parseInvariant(seldate).getDate() + "/" + Date.parseInvariant(seldate).getFullYear(), "MM/dd/yyyy");
            newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
            var callBackddl = document.getElementById("ddl_onlinecallback").selectedIndex;

            if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {
                //alert("Please select any business day");
                $("#txtErrorMessage").text("Please select any business day");
                $("#errorAlert").modal();

                return false;
            }

            if (diff <= 0 && (callBackddl == 0 || callBackddl == 2 || callBackddl == 3)) {
                //alert("Please select future date");
                $("#txtErrorMessage").text("Please select future date");
                $("#errorAlert").modal();
                return false;
            }

        }

        // Noufil 6766 02/10/2010 Hide modal popup
        function HideModalPopup(popupId) {
            debugger;
            //            document.getElementById('polmControl1_dd_QuickLegalDescription').value = "-1";
            //            document.getElementById('polmControl1_dd_damagevalue').value = "-1";
            //            document.getElementById('polmControl1_dd_Bodilydamage').value = "-1";
            //            document.getElementById('polmControl1_dd_attorney').value = "-1";
            //            try { document.getElementById('polmControl1_dd_Division').value = "1"; } catch(e){}
            //            document.getElementById('polmControl1_TxtLegalMatterDescription').value = ""; 
            //            document.getElementById('polmControl1_lblMessage').innerHTML = "";            
            //            document.getElementById('polmControl1_td_buttons').style.display = "";
            var modalPopupBehavior = $find(popupId);
            modalPopupBehavior.hide();
            return false;
        }

    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1 {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            width: 40%;
            padding-left: 5px;
            background-color: #EFF4FB;
        }

        .clsInputadministration {
            width: 82.8% !important;
            margin-left: 8px !important;
            /*height :60px;*/
        }

        /*.clsLeftPaddingTable {
            color: #555555 !important;
            font-weight: 400 !important;
            font-size: 14px !important;
            line-height: 23px !important;
            font-family: 'Open Sans', Arial, Helvetica !important;
        }*/
        .clsLeftPaddingTable {
            background-color: #fff !important;
        }

        select#polmControl1_dd_QuickLegalDescription {
            width: 90% !important;
            margin-left: 11px !important;
        }

        #polmControl1_dd_QuickLegalDescription {
            width: 35% !important;
        }

        #polmControl1_dd_QuickLegalDescription {
            width: 99% !important;
        }

        .clsLeftPaddingTable > tbody > tr > td > table > tbody tr > td {
            width: 100% !important;
        }

        .clsLeftPaddingTable > tbody > tr > td > table {
            width: 100% !important;
        }

            .clsLeftPaddingTable > tbody > tr > td > table > tbody tr > td textarea {
                width: 82.8% !important;
                margin-left: 11px !important;
            }

        .clsLeftPaddingTable > tbody > tr:nth-child(7) {
            height: 75px !important;
        }

        .clsLeftPaddingTable > tbody > tr:nth-child(9) {
            height: 75px !important;
        }

        #polmControl1_dd_Division {
            margin-left: 12px !important;
            width: 88% !important;
        }

        .clsLeftPaddingTable > tbody > tr:nth-child(2) > td {
            background-image: url();
            /*background:#11a2cf;*/
            border: 1px solid #11a2cf;
            height: .1px !important;
        }

        #polmControl1_btn_save {
            border-color: #11a2cf !important;
        }
    </style>
</head>
<body>
    <form id="Form2" method="post" runat="server">
         <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
       
       
       

        <%--<asp:Panel ID="pnl" runat="server">
                <uc4:ActiveMenu ID="ActiveMenu1" runat="server"></uc4:ActiveMenu>
            </asp:Panel>--%>

        <%-- Active menu start --%>
            <asp:HiddenField ID="hidMenuID" runat="server" />
       <!-- START TOPBAR -->
    <div class='page-topbar '>
        <div class="logo-area">
            <%--<asp:Image ID="Image2" runat="server" ImageUrl="../Images/enation_logo.gif"></asp:Image>--%>
        </div>
        <div class='quick-area'>
            <div class='pull-left'>
                <ul class="info-menu left-links list-inline list-unstyled">
                    <li class="sidebar-toggle-wrap">
                        <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                    <li class="hidden-sm hidden-xs searchform">
                        <form action="ui-search.html" method="post">
                            <div class="input-group" style="display:none">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" class="form-control animated fadeIn" placeholder="Search & Enter"/>
                            </div>
                            <input type='submit' value=""/>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="addnewleadTopRightBtn">
                <asp:LinkButton ID="lnkb_addnewlead" runat="server"  ToolTip="Add New Lead"
                                            OnClientClick="return openTopWindow('../clientinfo/addnewlead.aspx',400,420);"><i class="fa fa-user"></i>Add New Lead</asp:LinkButton>            
            </div>
            <div class='pull-right'>
                <ul class="info-menu right-links list-inline list-unstyled">
                    <li class="profile">
                        <a href="#" data-toggle="dropdown" class="toggle">
                            <asp:Label ID="lbl_Name" style=" font-size: 11px;" runat="server" ></asp:Label>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu profile animated fadeIn">
                            <li>
                                <asp:LinkButton ID="LinkButton6" runat="server" 
                                                         onclientclick="return openCenteredWindow('../backroom/changeEmpPassword.aspx',400,250);"   >
                                    <i class="fa fa-lock"></i><span>Change Password</span>
                                </asp:LinkButton>
                            </li>

                            
                            <li style="display:none;">

                               
                            </li>

                           

                            <li>
                               <asp:LinkButton ID="img_Logic" runat="server"  OnClientClick="OpenLogic()" > <i class="fa fa-question"></i><span>Help</span></asp:LinkButton>
                                
                            </li>

                            <li>
                                <a href="../backroom/Admin.aspx?sMenu=67"><i class="fa fa-user"></i><span>User Management</span></a>

                                
                            </li>

                            <li>
                                <a href="../backroom/Courts.aspx?sMenu=67"><i class="fa fa-gavel"></i><span>Courts</span></a>
                            </li>

                            <li>
                                <a href="../Backroom/outsidefirm.aspx?sMenu=67"><i class="fa fa-external-link"></i><span>Outside Firm</span></a>
                            </li>

                            <li>
                                <a href="../PaymentInfo/FinancialReps.aspx?sMenu=67"><i class="fa fa-calculator"></i><span>Financial Reports</span></a>
                            </li>

                            <li>
                                <a href="../backroom/PrincingMain.aspx?sMenu=67"><i class="fa fa-money"></i><span>Price Plan</span></a>
                            </li>

                            <li>
                                <a href="../backroom/CategoryInfo.aspx?sMenu=67"><i class="fa fa-info"></i><span>Category Info</span></a>
                            </li>

                            <li>
                                <a href="../Backroom/ViolationList.aspx?sMenu=100"><i class="fa fa-ticket"></i><span>Manage Violation</span></a>
                            </li>

                            <li>
                                <a href="../backroom/TemplateMain.aspx?sMenu=67"><i class="fa fa-columns"></i><span>Pricing Templates</span></a>
                            </li>

                            
                            <li>
                                <a href="../backroom/frmSPList.aspx?sMenu=67"><i class="fa fa-pencil-square-o"></i><span>Report Editor</span></a>
                            </li>


                            <li>
                                <a href="../backroom/frmReportCategory.aspx?sMenu=67"><i class="fa fa-bar-chart"></i><span>Report Category</span></a>
                            </li>

                            <li>
                                <a href="../Configuration/AttorneyPayout.aspx?sMenu=67"><i class="fa fa-twitch"></i><span>Attorney Payout</span></a>
                            </li>

                            <li>
                                <a href="../Reports/PaymentTypes.aspx?sMenu=67"><i class="fa fa-credit-card"></i><span>Payment Types</span></a>
                            </li>

                            
                            <li>
                                <a href="../Activities/ServiceTicketCategories.aspx?sMenu=148"><i class="fa fa-shield"></i><span>Service Type Categories</span></a>
                            </li>

                            <li>
                                <a href="../ErrorLog/Bugtracker.aspx?sMenu=148"><i class="fa fa-exclamation-triangle"></i><span>Error Logs</span></a>
                            </li>
                            
                            
                            <li class="" id="td_logoff1" runat="server">
                                
                                <asp:LinkButton ID="lnkb_LogOff" runat="server"  > <i class="fa fa-sign-out"></i><span>Log Off</span></asp:LinkButton>
                                
                            </li>
                        </ul>
                    </li>
                   
                </ul>
            </div>
        </div>

    </div>


    <%--<div class="page-sidebar pagescroll">

        <div class="page-sidebar-wrapper" id="main-menu-wrapper">

           <div class="profile-info row">
            </div>
            <ul class='wraplist' id="mainmenu" >
                <li class="">
                    <a href="../frmMain.aspx?sMenu=67" menu-id="~/frmMain.aspx">
                        <i class="fa fa-search"></i>
                        <span class="title">Search</span>
                    </a>
                </li>
                <li class="open" id="limatter">
                    <a href="javascript:;">
                        <i class="fa fa-gears"></i>
                        <span class="title">Matter</span>
                        <span class="arrow" ></span>
                    </a>
                    <ul class="sub-menu" style='' id="ulmatter">
                        <li>
                            <a id="idviolationinfo" menu-id="~/ClientInfo/ViolationFeeold.aspx" runat="server" class="active" href="../ClientInfo/ViolationFeeold.aspx?sMenu=61">Violation</a>
                        </li>
                        <li>
                            <a id="idgenerailinfo" menu-id="~/ClientInfo/GeneralInfo.aspx" runat="server" class="" href="../Clientinfo/generalinfo.aspx?sMenu=62">General Info</a>
                        </li>
                        <li>
                            <a id="idpaymentinfo" menu-id="~/ClientInfo/NewPaymentInfo.aspx" runat="server" class="" href="../Clientinfo/newpaymentinfo.aspx?sMenu=63">Payment Info</a>
                        </li>
                        <li>
                            <a id="iddocument" menu-id="~/Paperless/Documents.aspx" runat="server" class="" href="../Paperless/Documents.aspx?sMenu=119">Documents</a>
                        </li>
                        <li>
                            <a id="idcasehistory" menu-id="~/Clientinfo/CaseHistory.aspx" runat="server" class="" href="../Clientinfo/CaseHistory.aspx?sMenu=64">History</a>
                        </li>
                        <li>
                            <a id="idcomment" runat="server" menu-id="~/Clientinfo/Comments.aspx" class="" href="../Clientinfo/Comments.aspx?sMenu=221">Comments</a>
                        </li>
                        <li id="associateMenu" runat="server" visible="false">
                            <a id="idassociatedmatters" runat="server" menu-id="~/Clientinfo/AssociatedMatters.aspx" class="" href="../Clientinfo/AssociatedMatters.aspx?sMenu=213">Associated Matters</a>
                        </li>
                        <li>
                            <a id="idcasedisposition" runat="server" menu-id="~/Clientinfo/CaseDisposition.aspx" class="" href="../Clientinfo/CaseDisposition.aspx?sMenu=65">Disposition</a>
                        </li>
                      </ul>
                </li>
                <li id="liactivity">
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Activities</span>
                        <span class="arrow" ></span>
                    </a>
                    <ul class="sub-menu" id="ulactivity">
                        <li>
                            <a id="A1" runat="server" class="active" href="../QuickEntryNew/ReminderCalls.aspx?sMenu=95">Reminder Calls</a>
                        </li>
                        <li>
                            <a id="A25" runat="server" class="active" href="../Activities/NewPartialPay.aspx?sMenu=120">Partial Pay</a>
                        </li>
                        <li>
                            <a id="A2" runat="server" class="" href="../Activities/QuoteCallBackMain.aspx?sMenu=39">Quote Call Back</a>
                        </li>
                        <li>
                            <a id="A3" runat="server" class="" href="../Activities/nexttelemail.aspx?sMenu=26">Text Message</a>
                        </li>
                        <li>
                            <a id="A12" runat="server" class="" href="../Activities/frmClientLookup.aspx?sMenu=29">Traffic Alerts</a>
                        </li>
                        <li>
                            <a id="A13" runat="server" class="" href="../PaymentInfo/CloseOutReport.aspx?sMenu=27">Close Out</a>
                        </li>
                        <li>
                            <a id="A4" runat="server" class="" href="../Activities/DoNotMail.aspx?sMenu=121">Do Not Mail Update</a>
                        </li>
                        <li>
                            <a id="A5" runat="server" class="" href="../Activities/frmManualCC.aspx?sMenu=107">Manual CC</a>
                        </li>
                        <li>
                            <a id="A26" runat="server" class="" href="../Reports/frmRptjurytrial.aspx?sMenu=111">Tried Cases Report</a>
                        </li>
                        <li>
                            <a id="A6" runat="server" class="" href="../Activities/jurytrial.aspx?sMenu=110">Atty - Trials</a>
                        </li>
                        <li>
                            <a id="A11" runat="server" class="" href="../Reports/BatchPrint.aspx?sMenu=87">Batch Prints</a>
                        </li>
                        <li>
                            <a id="A24" runat="server" class="" href="../webscan/Newsearch.aspx?sMenu=102">BSDA (HMC)</a>
                        </li>                         
                        <li>
                            <a id="A14" runat="server" class="" href="../returneddockets/scandocket.aspx?sMenu=114">Scan/Upload Docket</a>
                        </li>                        
                        <li id="Li1" runat="server" visible="false">
                            <a id="A7" runat="server" class="" href="../returneddockets/scandocket.aspx?sMenu=114">Scan Docket</a>
                        </li>

                        <li id="Li2" runat="server" visible="false">
                            <a id="A9" runat="server" class="" href="../returneddockets/scandocket.aspx?sMenu=114">Scan Docket</a>
                        </li>
                        <li>
                            <a id="A16" runat="server" class="" href="../Reports/OpenServiceTicketReport.aspx?sMenu=140">Service Ticket Reports</a>
                        </li>
                        <li>
                            <a id="A17" runat="server" class="" href="../Activities/PaymentPlans.aspx?sMenu=136">Payment Due Report</a>
                        </li>
                        <li id="Li3" runat="server" visible="false">
                            <a id="A10" runat="server" class="" href="../Activities/PaymentPlans.aspx?sMenu=136">Payment Due Reports</a>
                        </li>
                        <li>
                            <a id="A8" runat="server" class="" href="../activities/FTACall.aspx?sMenu=195">Missed Call Date Courts</a>
                        </li>
                        <li>
                            <a id="A18" runat="server" class="" href="../Activities/OnlineInquiries.aspx?sMenu=205">Leads</a>
                        </li>
                        <li>
                            <a id="A23" runat="server" class="" href="../Activities/SetCalls.aspx?sMenu=194">Set Call</a>
                        </li>
                        <li>
                            <a id="A19" runat="server" class="" href="../Reports/TrafficWaitingFollowUp.aspx?sMenu=196">Waiting Follow Up</a>
                        </li>
                        <li>
                            <a id="A20" runat="server" class="" href="../Reports/ComplaintComments.aspx?sMenu=223">Complaint Comment Report</a>
                        </li>
                        <li>
                            <a id="A21" runat="server" class="" href="../quickentrynew/trialdockets.aspx?sMenu=97">Trial Docket</a>
                        </li>
                        <li>
                            <a id="A22" runat="server" class="" href="../quickentrynew/frmRptDocketULNew.aspx?sMenu=98">Docket Report</a>
                        </li>
                        <li>
                            <a id="A15" runat="server" class="" href="../Reports/NewDocketCloseOutOptimize.aspx?sMenu=104">Docket Close out</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">
                        <i class="fa fa-check"></i>
                        <span class="title">Validations</span>
                        <span class="arrow" ></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a id="A27" runat="server" class="active" href="../Reports/NOSReport.aspx?sMenu=159">Nos Reports</a>
                        </li>
                        <li>
                            <a id="A28" runat="server" class="active" href="../Reports/SplitReport.aspx?sMenu=60">Split Report</a>
                        </li>
                        <li>
                            <a id="A30" runat="server" class="active" href="../Reports/PendingTrialLetters.aspx?sMenu=116">No Trial Report</a>
                        </li>
                        <li>
                            <a id="A31" runat="server" class="active" href="../Reports/DisposedDiscrepancies.aspx?sMenu=131">No Disposition Letter</a>
                        </li>
                        <li>
                            <a id="A32" runat="server" class="active" href="../Reports/bondreport.aspx?sMenu=183">Bond Alert</a>
                        </li>
                        <li>
                            <a id="A33" runat="server" class="active" href="../Reports/PastCourtdateNonHMC.aspx?sMenu=209">Past Court Dates</a>
                        </li>
                        <li>
                            <a id="A34" runat="server" class="active" href="../Reports/ReminderCallValidation.aspx?sMenu=142">Reminder Calls</a>
                        </li>
                        <li>
                            <a id="A35" runat="server" class="active" href="../Reports/SignedContractReport.aspx?sMenu=201">Unsigned Contract</a>
                        </li>
                        <li>
                            <a id="A36" runat="server" class="active" href="../Reports/NOSReport.aspx?sMenu=159">NOS</a>
                        </li>
                        <li>
                            <a id="A37" runat="server" class="active" href="../Reports/WaitingFollowUpCriminal.aspx?sMenu=266">Criminal Waiting</a>
                        </li>
                        <li>
                            <a id="A45" runat="server" class="active" href="../reports/NoLORReport.aspx?sMenu=113">No Lor Sent</a>
                        </li>
                        <li>
                            <a id="A46" runat="server" class="active" href="../Reports/LORconfirmation.aspx?sMenu=193">No Lor Confirmation Alert</a>
                        </li>
                        <li>
                            <a id="A47" runat="server" class="active" href="../Reports/WaitingFollowUpTraffic.aspx?sMenu=264">Traffice Waiting</a>
                        </li>
                        <li>
                            <a id="A48" runat="server" class="active" href="../backroom/ValidationEmail.aspx?sMenu=143">Generate Validation Report</a>
                        </li>
                        <li>
                            <a id="A49" runat="server" class="active" href="../Reports/legalconsultation.aspx?sMenu=192">Leads Report</a>
                        </li>
                        
                        
                    </ul>
                </li>

                <li id="lireport">
                    <a href="javascript:;">
                        <i class="fa fa-bar-chart"></i>
                        <span class="title">Reports</span>
                        <span class="arrow" ></span>
                    </a>
                    <ul class="sub-menu"  id="ulreport">
                        
                        
                        <li>
                            <a id="A51" runat="server" class="active" href="../activities/Arraignment.aspx?sMenu=20">Arraignment</a>
                        </li>
                        <li>
                            <a id="A52" runat="server" class="active" href="../quickentrynew/arrcombined.aspx?sMenu=96">Docket Settings</a>
                        </li>
                        <li>
                            <a id="A38" runat="server" class="active" href="../Reports/FlagsReport.aspx?sMenu=172">SOL Report</a>
                        </li>
                        <li>
                            <a id="A39" runat="server" class="active" href="../Reports/FlagsReport.aspx?sMenu=172">Flag report</a>
                        </li>
                        <li>
                            <a id="A40" runat="server" class="active" href="../Reports/frmRptjurytrial.aspx?sMenu=111">Tried Cases</a>
                        </li>
                        <li>
                            <a id="A41" runat="server" class="active" href="../Reports/ContinuanceReport.aspx?sMenu=118">Continuance Report</a>
                        </li>
                        <li>
                            <a id="A42" runat="server" class="active" href="../Reports/CommentsReport.aspx?sMenu=222">Consultation Comment Report</a>
                        </li>
                        <li>
                            <a id="A43" runat="server" class="active" href="../Reports/ConcentrationReport.aspx?sMenu=145">Concentration Report</a>
                        </li>
                        <li>
                            <a id="A44" runat="server" class="active" href="../Reports/PastDueCalls.aspx?sMenu=207">Past Due Calls Report</a>
                        </li>
                        <li id="Li4">
                            <a id="A50" runat="server" class="" href="../returneddockets/docketsreports.aspx?sMenu=115">Scanned Docket</a>
                        </li>
                        
                    </ul>
                </li>

                <li>
                    <a href="javascript:;">
                        <i class="fa fa-gear"></i>
                        <span class="title">Configuration</span>
                        <span class="arrow" ></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a id="A29" runat="server" class="active" href="../Configuration/AttorneyPayout.aspx?sMenu=225">Attorney Payout Report</a>
                        </li>
                    </ul>
                </li>
        </div>
    </div>--%>

  <%-- Active menu start --%>














































        <div class="page-container row-fluid container-fluid">
            

            <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">
            <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red" EnableViewState="False"></asp:Label>
                 </div>
            
                </div>
        

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Leads</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>
             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Leads</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                   <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Start Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="calStartDate" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                                   <%--  <ew:CalendarPopup ID="calStartDate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                            CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                            ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                            ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Text=" " Width="80px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>

                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">End Date:</label>

                                <span class="desc"></span>
                                <div class="controls">
                                     <picker:datepicker id="calenddate"  runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                 <%--    <ew:CalendarPopup ID="calenddate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                            CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                            ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                            ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Text=" " Width="80px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                       <div class="col-md-3">
                                                            <div class="form-group">
                              <%--<label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:CheckBox ID="cb_showall" runat="server" Text="All Dates" CssClass="checkbox-custom"
                                            Checked="true" />
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
                 <span class="desc"></span>
                                <div class="controls">
                                    <asp:CheckBox ID="cb_AllFollowUpDate" runat="server" Text="All Follow-Up Dates" CssClass="checkbox-custom"
                                            Checked="false" />
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>


                      <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Practice Area:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddl_practiceArea" runat="server" CssClass="form-control" Width="">
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Call Back Status:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="dd_callback" runat="server" CssClass="form-control"
                                            Width="">
                                            <asp:ListItem Text="Active" Value="1" Selected="True" Enabled="true"></asp:ListItem>
                                            <asp:ListItem Text="Close" Value="0" Selected="False" Enabled="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>
                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">Call Back Status:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btn_update1_Click1">
                                        </asp:Button>

                                     <asp:DropDownList ID="DropDownList1" CausesValidation="False" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged1">
                                         <asp:ListItem Value="1">1</asp:ListItem>
                                         <asp:ListItem Value="2">2</asp:ListItem>
                                         <asp:ListItem Value="3">3</asp:ListItem>
                                         <asp:ListItem Value="4">4</asp:ListItem>
                                     </asp:DropDownList>

                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                 </section>
             
             <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                
            <ContentTemplate>
            

             <div class="clearfix"></div>

             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Leads</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
                         <div class="row">
                             <div class="col-md-5"></div>
                             <div class="col-md-5" style="padding:6px;">
                           <uc3:PagingControl ID="Pagingctrl" runat="server" />

                                

                             </div>
                             
                         </div>

            </header>

                 <div class="content-body">
                <div class="row">

                   <div class="col-md-12">
                                                            <div class="form-group">
                              <label class="form-label">Start Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                      <asp:GridView ID="gv_records" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CellPadding="3" CssClass="table table-small-font table-bordered table-striped" OnRowCommand="gv_records_RowCommand"
                                                        OnRowDataBound="gv_records_RowDataBound" ShowHeader="False" AllowPaging="true"
                                                        PageSize="20" OnPageIndexChanging="gv_records_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table class="table-responsive" id="Table1" cellspacing="1" cellpadding="0" width="100%"
                                                                        align="center" border="0">
                                                                        <tr>
                                                                            <td align="left"  height="20" valign="middle">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td align="left"  height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="lbl_CallerID" runat="server" CssClass="form-label-blue">Caller ID :</asp:Label>
                                                                            </td>
                                                                            <td   height="20" align="left" style="width: 100%">
                                                                                <asp:Label ID="lbl_CallerIDValue" runat="server" Text='<%# Eval("[CallerId]") %>' CssClass="form-label-blue"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>    
                                                                             <td valign="middle" align="left"  rowspan="7" style="width: 8%">
                                                                                <asp:HyperLink ID="hl_SerialNumber" runat="server" Text='<%# Eval("Sno") %>' CssClass="clssubhead"></asp:HyperLink>
                                                                                <asp:HiddenField ID = "hf_ticketId" runat ="server" Value = '<%# Eval("TicketID") %>' />
                                                                            </td>
                                                                            <td align="left"  height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label11" runat="server" CssClass="form-label-blue">Name:</asp:Label>
                                                                            </td>
                                                                            <td   height="20" align="left" style="width: 100%">
                                                                                <asp:Label ID="lnk_name" runat="server" Text='<%# Eval("[Name]") %>' CssClass="form-label-blue"></asp:Label>
                                                                                <asp:Label ID="lbl_legalid" runat="server" Text='<%# Eval("ID") %>' CssClass="Label"
                                                                                    ForeColor="#123160" Visible="False">
                                                                                </asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top"  height="20"
                                                                                rowspan="4" style="width: 175px">
                                                                                <asp:Label ID="lbl_phone" runat="server" Text='<%# Eval("Phone") %>' ></asp:Label><br />
                                                                                <asp:Label ID="lbl_fax" runat="server" Text='<%# Eval("Fax") %>'> </asp:Label><br>
                                                                                <asp:LinkButton ID="lnk_Comment" CssClass="btn btn-primary" runat="server" Width="130px" CommandName="Add_gv_Comments"
                                                                                    Text="Add Comments"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkShowPolm" Visible="false" runat="server" Width="130px" CommandName="AddPolm"
                                                                                    Text="Add Polm"></asp:LinkButton>
                                                                                <br>
                                                                            </td>
                                                                            <td valign="top" width="205"  height="20" rowspan="4" align="left">
                                                                                <div style="width: 280px; height: 83px; overflow: auto;">
                                                                               <%-- Muhammad Nadir siddiqui 9158 04/15/2011--%>
                                                                                    <asp:Label ID="lbl_comment" runat="server" Text='<%# Eval("Repcomments") %>' ></asp:Label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left"  height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label1" runat="server">Call Back Status :</asp:Label>
                                                                            </td>
                                                                            <td align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lbl_callback" runat="server" Text='<%# Eval("calldescription") %>'
                                                                                    CssClass="form-label" ForeColor="#123160">
                                                                                </asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label2" runat="server" CssClass="form-label-blue">Email Received Date 
                                                                                :</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lbl_recdate" runat="server" Text='<%# Eval("recdate") %>' CssClass="form-label"
                                                                                    ForeColor="#123160">
                                                                                </asp:Label>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                        <tr>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label4" runat="server" CssClass="form-label-blue">Language :</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("Language") %>'
                                                                                    CssClass="form-label" ForeColor="#123160">
                                                                                </asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label12" runat="server" CssClass="form-label-blue">Email Address:</asp:Label>
                                                                            </td>
                                                                            <td   height="20" align="left" style="width: 100px">
                                                                                <asp:Label ID="lbl_emailadress" runat="server" Text='<%# Eval("EmailAddress") %>'
                                                                                    CssClass="form-label" ForeColor="#123160" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label13" runat="server" CssClass="form-label-blue">Question/Comments:</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lbl_Question" runat="server" Text='<%# Eval("Comments") %>' CssClass="form-label"
                                                                                    ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- Fahad 6429 Add New Row--%>
                                                                        <tr id="trPracArea" runat="server">
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="lblPracArea" runat="server" CssClass="form-label-blue">Practice Area:</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("PracticeArea") %>' CssClass="form-label"
                                                                                    ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trFollowUpdate" runat="server">
                                                                            <td>
                                                                            </td>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label5" runat="server" CssClass="form-label-blue">Follow-Up date:</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lblfollowupdate" runat="server" Text='<%# Eval("followupdate") %>'
                                                                                    CssClass="form-label" ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- Haris Ahmed 10181 04/10/2012 Add IPAddress and SaleRep Columns--%>
                                                                        <tr id="trIPAddress" runat="server">
                                                                            <td>
                                                                            </td>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label8" runat="server" CssClass="form-label-blue">IP Address:</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lblIPAddress" runat="server" Text='<%# Eval("IPAddress") %>'
                                                                                    CssClass="form-label" ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                         <tr id="trSaleRep" runat="server">
                                                                            <td>
                                                                            </td>
                                                                            <td align="left"   height="20" style="width: 40%"
                                                                                valign="middle">
                                                                                <asp:Label ID="Label9" runat="server" CssClass="form-label-blue">Sales Rep:</asp:Label>
                                                                            </td>
                                                                            <td   align="left" style="height: 20px;
                                                                                width: 100%">
                                                                                <asp:Label ID="lblSaleRep" runat="server" Text='<%# Eval("SaleRep") %>'
                                                                                    CssClass="form-label" ForeColor="#123160"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%-- Fahad 6429 end Add New Row--%>
                                                                         
                                                                        <tr>
                                                                            <td align="left" colspan="3">
                                                                                <div class="well-sm"></div>
                                                                                <asp:Label ID="lbl_sitetype" CssClass="bg-primary" runat="server"  Font-Bold="True" Text='<%# Eval("sitetype") %>'></asp:Label> <%--Kashif jawed 9043 03/28/2011 set text for lbl_sitetype--%> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr ID = "tr_clientInfo" runat = "server">
                                                                            <td align="left" colspan="3" >
                                                                                <asp:Label ID="lblClientInfo" runat="server" BackColor="#FFCC66" Font-Bold="True" Text='<%# Eval("CelintInfo") %>'></asp:Label> <%--Kashif jawed 9043 03/28/2011 set text for lbl_sitetype--%> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                                &nbsp;
                                                                            </td>
                                                                            <td  colspan="4">
                                                                           <%-- Muhammad Nadir Siddiqui 9158 04/15/2011--%>
                                                                                
                                                                                <asp:HiddenField ID="hf_GeneralComments" runat="server" Value='<%# Eval("Comments") %>' />
                                                                                <%--<asp:HiddenField ID="hf_question" runat="server" Value='<%# Eval("Comments") %>' />--%>
                                                                                <%--<asp:HiddenField ID="hf_email" runat="server" Value='<%# Eval("EmailAddress") %>' />--%>
                                                                                <%--<asp:HiddenField ID="hf_onlinecallback" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.callback") %>' />--%>
                                                                                <%--<asp:HiddenField ID="hf_comm" runat="server" Value='<%# Eval("legal") %>'/>--%>
                                                                                <asp:Label ID="hf_onlinecallback" style="display:none;" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.callback") %>'></asp:Label>
                                                                                <asp:Label ID="hf_email" style="display:none;" runat="server" Text='<%# Eval("EmailAddress") %>'></asp:Label>
                                                                                <asp:Label ID="hf_question" style="display:none;" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                                                <asp:Label ID="hf_comm" style="display:none;" runat="server" Text='<%# Eval("legal") %>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 11px" width="780" 
                                                                                height="11">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td colspan="4" height="11" style="height: 11px"
                                                                                width="780">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <hr style="border-top: 1px solid #11a2cf;"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="&#160;Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&#160;&#160;&#160;" LastPageText="&#160;&#160;&#160;Last Page &gt;&gt;" />
                                                    </asp:GridView>

                                        </div>

                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                 </section>

             <div class="clearfix"></div>

            <ajaxToolkit:ModalPopupExtender ID="Modal_gvrecords" runat="server" TargetControlID="Button1"
                                                        PopupControlID="pnl_online" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        CancelControlID="btn_onlinecancel">
                  
                                                    </ajaxToolkit:ModalPopupExtender>
               
                                                    <asp:Button ID="btn" runat="server" Style="display: none" />
                                                    <asp:Button ID="Button1" runat="server" Style="display: none" />


                                              <%--<asp:Panel ID="pnl_online" runat="server" Height="100px" Width="420px" Style="display: none;">
                    
                                                      <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                                        <div class="col-sm-10">
                                                          <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                                        </div>
                                                      </div>
                                             </asp:Panel>--%>



                <asp:Panel ID="pnl_online" runat="server" Height="" Width="800px" Style="display: none;">
                    
                    <section id="main-content-popup class="addnewleadpopup">
                    <section class="wrapper main-wrapper row" style="">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Update Comments</h2>

                          <%--  <div class="actions panel_actions remove-absolute pull-right">
                       
                	            <a class="box_toggle fa fa-chevron-down"></a>
                            </div>--%>
                      </header>
                        <div class="content-body">
                   <div class="table-responsive">
      <table id="Table2" class="table-responsive"  style="height: 200px; width: 589px;background:white;" cellpadding="0" cellspacing="0">
      <tbody>
          <input type="hidden" id="fhtest" runat="server" title='<script type="text/javascript">alert();</script>' />
         <%--<tr>
          <td><input type="text" class="form-control" value="Some Long Name Here" /></td>
          <td><input type="text" class="form-control" value="13-Jul-2015" /></td>
          <td><input type="text" class="form-control" value="$12355.12" /></td>
          <td>
            <select class="form-control">
                            <option>Monthly</option>
                            <option>Yearly</option>
                        </select>
          </td>
          <td><input type="text" class="form-control" value="another long value" /></td>
          <td><input type="text" class="form-control" value="some val" /></td>
          <td><input type="text" class="form-control" value="some val" /></td>
          <td><input type="text" class="form-control" value="some val" /></td>
          <td>$505.79</td>
        </tr>--%>

                                                            <%--<tr>
                                                                <td class="" align="left" style="height: 32px;background-image:url(../Images/subhead_bg.gif)"
                                                                    width="420" colspan="2">
                                                                    &nbsp;Update Comments
                                                                </td>
                                                            </tr>--%>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Name:</span>
                                                                </td>
                                                                <td style="height: 22px; text-align: left;">
                                                                    &nbsp;<asp:Label ID="lbl_onlinename" runat="server" CssClass="Label"></asp:Label><asp:Label
                                                                        ID="Label3" runat="server" Visible="False" CssClass="Label"></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Email Address:</span>
                                                                </td>
                                                                <td width="160" style="height: 22px; text-align: left">
                                                                    &nbsp;<asp:Label ID="lbl_onlineemail" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle" align="left">
                                                                    <span class="clssubhead">&nbsp;Contact Number:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 22px;">
                                                                    &nbsp;<asp:Label ID="lbl_ph" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Question:</span>
                                                                </td>
                                                                <td style="text-align: left; width: 270px">
                                                                    <div style="width: 270px; height: 55px; overflow: auto; vertical-align: middle" runat="server"
                                                                        id="divQuestion">
                                                                        &nbsp;<asp:Label ID="lbl_onlinequestion" runat="server" CssClass="Label"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Call Back:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 23px; width: 160px">
                                                                    <asp:DropDownList ID="ddl_onlinecallback" runat="server" CssClass="form-control inline-textbox" Width="97%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle" style="width: 136px">
                                                                    <span class="clssubhead">&nbsp;Current Follow-Up date:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 23px;">
                                                                    &nbsp;<asp:Label ID="lbl_currentFollowUp" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                                
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Next Follow-Up date:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 24px; width: 160px">
                                                                    <%--<picker:datepicker id="calfollowupdate" runat="server"  Width="90px" JavascriptOnChangeFunction="CheckDate" max="12/31/9999" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>--%>
                                                                    <div>
                                                                         <div class="input-group date" id="">
                                     <asp:TextBox ID="calfollowupdatenew" CssClass="form-control datepicker" data-format="mm/dd/yyyy" runat="server"></asp:TextBox>
                                    <label class="input-group-addon btn" for="testdate" id="addonondt">
                                       <span class="fa fa-calendar"></span>
                                    </label> </div>
                                                                    <%--<picker:datepicker id="calfollowupdatenew" runat="server" Dateformat="mm/dd/yyyy" ></picker:datepicker>--%>

                                                                        <%--&nbsp;<ew:CalendarPopup ID="calfollowupdate" runat="server" EnableHideDropDown="True" 
                                                                            ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                                            AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
                                                                            PadSingleDigits="True" ToolTip="Follow-Up Date" Font-Names="Tahoma" Font-Size="8pt"
                                                                            ImageUrl="../images/calendar.gif" Text=" " Width="80px" JavascriptOnChangeFunction="CheckDate">
                                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                        </ew:CalendarPopup>--%>
                                                                    </div>
                                                                </td>
                                                            </tr>
           <tr >
                                                                <td valign="middle" style="width: 136px">
                                                                   
                                                                </td>
                                                                <td style="text-align: left; height: 23px;">
                                                                   
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td>
                                                                    <span class="clssubhead">&nbsp;Comments:</span>
                                                                </td>
                                                                <td align="left">
                                                                    <div style="width: 270px; height: 83px; overflow: auto;" runat="server" id="divCommentOnline">
                                                                        &nbsp;<asp:Label ID="lblOldOnlineComments" runat="server" CssClass="Label" Width="250px"></asp:Label>
                                                                    </div>
                                                                    <asp:TextBox ID="txt_onlinecomment" runat="server" Height="62px" Text="" TextMode="MultiLine" style="resize: vertical;"
                                                                        Width="270px" CssClass="form-control inline-textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:HiddenField ID="hf_onlineoldcomments" runat="server" />
                                                                </td>
                                                            </tr>
      </tbody>
    </table>
                     </div>
                            <div class="well-sm"></div>
                            <div class="row">
                                <div class="co-md-12 pull-right">
                                <asp:Button ID="btn_onlineupdate" runat="server" CssClass="btn btn-primary" Text="Update" OnClick="btn_onlineupdate_Click" OnClientClick="return checkcomments(1);"  />
                                 <asp:Button ID="btn_onlinecancel" runat="server" CssClass="btn btn-primary" Text="Cancel"/>
                            </div>
                            </div>
                            

                            </div>    
                                
                </section>     



                 </section>
            </section>
</asp:Panel>


                                                    <%--<asp:Panel ID="pnl_online" runat="server" Height="100px" Width="420px" Style="display: none;">
                                                        <table id="" bgcolor="white" border="1" style="border-top: black thin solid;
                                                            border-left: black thin solid; border-bottom: black thin solid; border-right: black thin solid;
                                                            height: 200px; width: 420px;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 32px"
                                                                    width="420" colspan="2">
                                                                    &nbsp;Update Comments
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Name:</span>
                                                                </td>
                                                                <td style="height: 22px; text-align: left;">
                                                                    &nbsp;<asp:Label ID="lbl_onlinename" runat="server" CssClass="Label"></asp:Label><asp:Label
                                                                        ID="Label3" runat="server" Visible="False" CssClass="Label"></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Email Address:</span>
                                                                </td>
                                                                <td width="160" style="height: 22px; text-align: left">
                                                                    &nbsp;<asp:Label ID="lbl_onlineemail" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle" align="left">
                                                                    <span class="clssubhead">&nbsp;Contact Number:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 22px;">
                                                                    &nbsp;<asp:Label ID="lbl_ph" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Question:</span>
                                                                </td>
                                                                <td style="text-align: left; width: 270px">
                                                                    <div style="width: 270px; height: 55px; overflow: auto; vertical-align: middle" runat="server"
                                                                        id="divQuestion">
                                                                        &nbsp;<asp:Label ID="lbl_onlinequestion" runat="server" CssClass="Label"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Call Back:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 23px; width: 160px">
                                                                    <asp:DropDownList ID="ddl_onlinecallback" runat="server" CssClass="clsInputCombo"
                                                                        Width="160px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle" style="width: 136px">
                                                                    <span class="clssubhead">&nbsp;Current Follow-Up date:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 23px;">
                                                                    &nbsp;<asp:Label ID="lbl_currentFollowUp" runat="server" CssClass="Label"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td valign="middle">
                                                                    <span class="clssubhead">&nbsp;Next Follow-Up date:</span>
                                                                </td>
                                                                <td style="text-align: left; height: 24px; width: 160px">
                                                                    <div>
                                                                        &nbsp;<ew:CalendarPopup ID="calfollowupdate" runat="server" EnableHideDropDown="True"
                                                                            ControlDisplay="TextBoxImage" CalendarLocation="Bottom" ShowGoToToday="True"
                                                                            AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00"
                                                                            PadSingleDigits="True" ToolTip="Follow-Up Date" Font-Names="Tahoma" Font-Size="8pt"
                                                                            ImageUrl="../images/calendar.gif" Text=" " Width="80px" JavascriptOnChangeFunction="CheckDate">
                                                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Gray" />
                                                                            <TextboxLabelStyle CssClass="clstextarea" />
                                                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                ForeColor="Black" />
                                                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                Font-Size="XX-Small" ForeColor="Black" />
                                                                        </ew:CalendarPopup>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td>
                                                                    <span class="clssubhead">&nbsp;Comments:</span>
                                                                </td>
                                                                <td align="left">
                                                                    <div style="width: 270px; height: 83px; overflow: auto;" runat="server" id="divCommentOnline">
                                                                        &nbsp;<asp:Label ID="lblOldOnlineComments" runat="server" CssClass="Label" Width="250px"></asp:Label>
                                                                    </div>
                                                                    <asp:TextBox ID="txt_onlinecomment" runat="server" Height="62px" Text="" TextMode="MultiLine"
                                                                        Width="270px" CssClass="form-control"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" style="height: 21px">
                                                                    &nbsp;<asp:Button ID="btn_onlineupdate" runat="server" CssClass="clsbutton" Text="Update"
                                                                        OnClick="btn_onlineupdate_Click" OnClientClick="return checkcomments(1);" Width="60px" />
                                                                    <asp:Button ID="btn_onlinecancel" runat="server" CssClass="clsbutton" Text="Cancel"
                                                                        Width="60px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:HiddenField ID="hf_onlineoldcomments" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>--%>


            <ajaxToolkit:ModalPopupExtender ID="mpeShowPolm" runat="server" TargetControlID="polmHiddenButton"
                                PopupControlID="pnlPolm" BackgroundCssClass="modalBackground" DropShadow="false">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button runat="server" ID="polmHiddenButton" Text="More" Style="display: none;" />


                <asp:Panel ID="pnlPolm" runat="server">
                <section id="main-content-popup" class="addnewleadpopup">
                    <section class="wrapper main-wrapper row" style="">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Consultation Request</h2>

                            <div class="actions panel_actions remove-absolute pull-right" >
                       
                	            <a id="lbtn_close2" class="box_toggle fa fa-remove" runat="server" onclick="return HideModalPopup('mpeShowPolm');"></a>
                            </div>
                      </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <uc2:PolmControl ID="polmControl1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </section>  
                 </section>
            </section>

</asp:Panel>

                           <%-- <asp:Panel ID="pnlPolm" runat="server">
                                <table id="table1" style="border-top: black thin solid; border-left: black thin solid;
                                    border-bottom: black thin solid; border-right: black thin solid;" cellpadding="0"
                                    cellspacing="0" >
                                    <tr>
                                        <td align="left" valign="bottom" background="../Images/subhead_bg.gif" colspan="2">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td style="height: 26px" class="clssubhead">
                                                        <asp:Label runat="server" ID="lbl_title" Text="Consultation Request"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:LinkButton ID="lbtn_close2" runat="server" OnClientClick="return HideModalPopup('mpeShowPolm');">X</asp:LinkButton>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <uc1:PolmControl ID="polmControl1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>--%>






                 </ContentTemplate>
        </aspnew:UpdatePanel>

            </section>
                     </section>
        </div>
    </form>
     <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
   <%-- <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>--%>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">

        //document.getElementById("calfollowupdate_div").style.zIndex = 10003;
        //document.getElementById("calfollowupdate_monthYear").style.zIndex = 10004;
        $(document).ready(function () {
            $('#calStartDate_txtDate, #calenddate_txtDate').focusin(function () {
                $('#calStartDate_txtDate, #calenddate_txtDate').show();
            });

            $("#errorAlert").on('shown.bs.modal', function (event) {
                document.getElementById("pnl_online").style.zIndex = 1;
            });
            $("#errorAlert").on('hidden.bs.modal', function (event) {
                document.getElementById("pnl_online").style.zIndex = 10004;
            });

            $('#polmControl1_TxtLegalMatterDescription').removeClass('clsInputadministration').addClass('form-control');
            //$("#pnlPolmli > select").each(function (i, e) {
            //    debugger;;
            //    alert($(this).text())
            //});

        });

    </script>

    <div id="errorAlert" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body" style="min-height: 93px !important; max-height: 162px;">
                    <p id="txtErrorMessage">Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <!-- CORE JS FRAMEWORK - START -->
    <%--<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>--%>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script type="text/javascript">
        $('.datepicker').datepicker({ autoclose: true });
    </script>
    <script type="text/javascript">
        
    </script>
</body>
</html>

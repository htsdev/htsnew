﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
using HTP.Activities;
// Yasir Kamal 5427 02/06/2009 Pagging added for dataGrid.
using lntechNew.WebControls;
using HTP.Components;

namespace HTP.Activities
{
    /// <summary>
    /// Summary description for setcalls.
    /// </summary>
    public partial class Lead : System.Web.UI.Page
    {
        clsSession ClsSession = new clsSession();
        clscalls clscall = new clscalls();
        clsAddNewLead _objLead = new clsAddNewLead();
        protected System.Web.UI.WebControls.TextBox calfollowupdatenew;



        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else
                {
                    if (!IsPostBack)
                    {
                        ViewState["empid"] = Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request));
                        calStartDate.SelectedDate = calenddate.SelectedDate = DateTime.Today;
                        //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
                        //GetReminderStatus(dd_callback, 1);
                        GetReminderStatus(ddl_onlinecallback, 0);

                        // Rab Nawaz Khan 11473 10/22/2013 Added Practice Area Drop down in report. . . 
                        GetPracticeArea();
                        FillGrid();

                        // Noufil 6766 02/10/2009 Set Ticket id to POLM control
                        polmControl1.TicketId = 0;
                        // Noufil 6766 02/10/2009 Set empid to POLM control
                        polmControl1.EmpId = Convert.ToInt32(ViewState["empid"]);
                        // Noufil 6766 02/10/2009 Set Modal popup Client Id to POLM control
                        polmControl1.ModalPopupName = mpeShowPolm.ClientID;
                        // Noufil 6766 02/10/2009 Set User Access Type to POLM control
                        polmControl1.IsPrimaryUser =
                            (ClsSession.GetCookie("sAccessType", Request) == "2");


                    }

                    Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);

                    cb_showall.Checked = cb_showall.Checked;
                    PagingControl.grdType = GridType.GridView;
                    Pagingctrl.GridView = gv_records;
                    //Pagingctrl.DataGrid = dg_ReminderCalls;
                    Pagingctrl.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.ddl_rStatus.SelectedIndexChanged += new System.EventHandler(this.ddl_rStatus_SelectedIndexChanged);
            //this.btn_update1.Click += new System.EventHandler(this.btn_update1_Click);
            //this.lbRefresh.Click += new EventHandler(this.btn_update1_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
        #region Event
        private void GetPracticeArea()
        {
            try
            {

                DataSet dsPracticeArea = _objLead.GetActivePracticeArea("USP_PS_Get_ActivePracticeArea");
                ddl_practiceArea.DataSource = dsPracticeArea;
                ddl_practiceArea.DataTextField = "Description";
                ddl_practiceArea.DataValueField = "ID";
                ddl_practiceArea.DataBind();
                ddl_practiceArea.Items.Insert(0, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_onlineupdate_Click(object sender, EventArgs e)
        {
            try
            {
                //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
                SullolawReports.UpdateComments(Convert.ToInt32(ViewState["legalid"]), Convert.ToInt32(ViewState["empid"]), txt_onlinecomment.Text, Convert.ToInt32(ViewState["Sitetype"]), Convert.ToInt32(ddl_onlinecallback.SelectedValue), Convert.ToDateTime(calfollowupdatenew.Text));
                FillGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void btn_update1_Click1(object sender, EventArgs e)
        {
            try
            {
                FillGrid(); //filling the grid
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Add_gv_Comments")
                {
                    int rowId = Convert.ToInt32(e.CommandArgument);
                    ClearControls();
                    ViewState["commenttype"] = 1;
                    ViewState["legalid"] = Convert.ToInt32((((Label)gv_records.Rows[rowId].Cells[0].FindControl("lbl_legalid")).Text));


                    //var t =  (((HiddenField)gv_records.Rows[rowId].Cells[0].FindControl("hf_comm")).Value);
                    //var t1 = 

                    ViewState["Sitetype"] = Convert.ToInt32((((Label)gv_records.Rows[rowId].Cells[0].FindControl("hf_comm")).Text));
                    lblOldOnlineComments.Text = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("lbl_comment")).Text);
                    if (lblOldOnlineComments.Text.Trim() == "No Comments")
                    {
                        lblOldOnlineComments.Text = "";
                    }
                    divCommentOnline.Style["Display"] = (lblOldOnlineComments.Text == "") ? "none" : "block";

                    if (txt_onlinecomment.Text.Trim() == "No Comments")
                    {
                        txt_onlinecomment.Text = "";
                    }
                    lbl_onlinename.Text = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("lnk_name")).Text);
                    lbl_onlineemail.Text = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("hf_email")).Text);
                    lbl_onlinequestion.Text = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("hf_question")).Text);
                    lbl_ph.Text = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("lbl_phone")).Text);
                    //Muhammad Nadir Siddiqui 9158 04/15/2011
                    ddl_onlinecallback.SelectedValue = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("hf_onlinecallback")).Text) != "" ? (((Label)gv_records.Rows[rowId].Cells[0].FindControl("hf_onlinecallback")).Text) : "0";
                    hf_onlineoldcomments.Value = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("lbl_comment")).Text);
                    if (hf_onlineoldcomments.Value.Trim() == "No Comments")
                    {
                        hf_onlineoldcomments.Value = "";
                    }
                    //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
                    Label lbl_followUpdate = ((Label)gv_records.Rows[rowId].Cells[0].FindControl("lblfollowupdate"));

                    lbl_currentFollowUp.Text = lbl_followUpdate.Text;

                    if (lbl_followUpdate.Text == "N/A")
                    {
                        calfollowupdatenew.Text = DateTime.Now.ToShortDateString();
                    }
                    else
                    {
                        calfollowupdatenew.Text = lbl_followUpdate.Text;
                    }
                    Modal_gvrecords.Show();

                }
                // Noufil 6766 02/10/2009 Show polm modal popup on clicking Add polm hyperlink.
                else if (e.CommandName == "AddPolm")
                {
                    int rowId = Convert.ToInt32(e.CommandArgument);
                    string name = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("lnk_name")).Text);
                    string fName = (name.IndexOf(" ") != -1) ? name.Substring(0, name.LastIndexOf(' ')) : name;
                    string lName = (name.IndexOf(" ") != -1) ? name.Substring(name.LastIndexOf(' ')) : name;
                    polmControl1.ClientFirstName = fName;
                    polmControl1.ClientLastName = lName;
                    polmControl1.ClientPrimaryContactNumber = (((Label)gv_records.Rows[rowId].Cells[0].FindControl("lbl_phone")).Text);
                    polmControl1.ClientAddress = "N/A";
                    polmControl1.ClientLanguageId = 1;
                    polmControl1.ClientContactTypeId = 1;
                    polmControl1.ClientDob = Convert.ToDateTime("01/01/1900");
                    polmControl1.ClientEmailAddress = (((HiddenField)gv_records.Rows[rowId].Cells[0].FindControl("hf_email")).Value);
                    polmControl1.IsTicketIdNotAvailable = true;
                    polmControl1.ResetControls();

                    //polmControl1.
                    mpeShowPolm.Show();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }

        }

        protected void gv_records_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((LinkButton)e.Row.FindControl("lnk_comment")).CommandArgument = e.Row.RowIndex.ToString();
                    // Noufil 6766 01/04/2010 Set Command argument to row index of grid
                    ((LinkButton)e.Row.FindControl("lnkShowPolm")).CommandArgument = e.Row.RowIndex.ToString();

                    Label lbl_emmail = ((Label)e.Row.FindControl("lbl_emailadress"));
                    Label lbl_ques = ((Label)e.Row.FindControl("lbl_Question"));

                    //Muhammad Nadir Siddiqui 9043 03/17/2011 added comments tool tip 
                    if (lbl_emmail.Text.Length > 28)
                    {
                        lbl_emmail.ToolTip = lbl_emmail.Text;
                        lbl_emmail.Text = lbl_emmail.Text.Substring(0, 25) + "...";
                    }
                    if (lbl_ques.Text.Length > 28)
                    {
                        lbl_ques.ToolTip = lbl_ques.Text.TrimStart(';');
                        lbl_ques.Text = lbl_ques.Text.TrimStart(';').Substring(0, 25) + "...";

                    }
                    //Kashif Jawed 9043 03/28/2011 comment code b/c we get sitetype from SP do not require to check hf_comm value
                    //HiddenField lbltype = (HiddenField)e.Row.FindControl("hf_comm");
                    //Kashif Jawed 9043 03/28/2011 comment code b/c we get sitetype 
                    //if (Convert.ToInt32(lbltype.Value) == 1)
                    //    ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Consultation Request";
                    //else if (Convert.ToInt32(lbltype.Value) == 2)
                    //   ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Contact us Request";
                    //else if (Convert.ToInt32(lbltype.Value) == 3)
                    //    ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Offline Support Request";
                    //Fahad 6429 09/16/2009 Add new SiteType Label for ThankYou Email
                    //Fahad 7167 12/23/2009 Changed lable from Thank Email to Thank You Email
                    //Kashif Jawed 9043 03/28/2011 comment code b/c we get sitetype 
                    //else if (Convert.ToInt32(lbltype.Value) == 4)
                    //    ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Thank You Email Request";
                    //Afaq 8728 12/30/2010 set text to "Depuy Contact Us" for depuy clients.
                    //Kashif Jawed 9043 03/28/2011 comment code b/c we get sitetype 
                    //else if (Convert.ToInt32(lbltype.Value) == 5)
                    //    ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Depuy Contact Us";
                    // Afaq 8752 01/03/2011 set text to Zimmer Contact Us for Zimmer Leads.
                    //Kashif Jawed 9043 03/28/2011 comment code b/c we get sitetype 
                    //else if (Convert.ToInt32(lbltype.Value) == 6)
                    //    ((Label)e.Row.FindControl("lbl_sitetype")).Text = "Zimmer Contact Us";
                    //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
                    Label lbl_followupdate = ((Label)e.Row.FindControl("lblfollowupdate"));
                    if (lbl_followupdate.Text == "")
                    {
                        lbl_followupdate.Text = "N/A";
                    }
                    // Rab Nawaz Khan 11473 10/06/2013 Added to display the client info. . . 
                    System.Web.UI.HtmlControls.HtmlTableRow tr = ((System.Web.UI.HtmlControls.HtmlTableRow)e.Row.FindControl("tr_clientInfo"));
                    if (((Label)e.Row.FindControl("lblClientInfo")).Text.Equals("N/A"))
                        tr.Style["display"] = "none";
                    else
                    {
                        tr.Style["display"] = "block";
                        ((HyperLink)e.Row.FindControl("hl_SerialNumber")).NavigateUrl = "/ClientInfo/ViolationFeeold.aspx?search=0&caseNumber=" + ((HiddenField)e.Row.FindControl("hf_ticketId")).Value;
                    }

                    // ((LinkButton)e.Row.FindControl("lnk_comment")).Attributes.Add("onmouseover","")
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gv_records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_records.PageIndex = e.NewPageIndex;
            FillGrid();
        }

#endregion

        #region Methods

        protected void Pagingctrl_PageIndexChanged()
        {
            if(Pagingctrl.PageIndex == 0)
                gv_records.PageIndex = Pagingctrl.PageIndex;
            else
                gv_records.PageIndex = Pagingctrl.PageIndex-1;
            FillGrid();


        }


        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            var gridrowscount = Session["dtLegalConsultationReportRowsCount"];
            int actualcount = 0;
            if (Session["dtLegalConsultationReportRowsCount"] != null)
            {
                int cnt = 0;
                if (int.TryParse(Session["dtLegalConsultationReportRowsCount"].ToString(), out cnt))
                {
                    actualcount = Convert.ToInt32(gridrowscount);
                }
            }
            if (pageSize > 0 && pageSize >= actualcount)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = actualcount;
                gv_records.AllowPaging = true;
            }
            if (pageSize > 0)
            {
                gv_records.PageIndex = 0;
                gv_records.PageSize = pageSize;
                gv_records.AllowPaging = true;
            }
            else
            {
                gv_records.AllowPaging = false;
            }
            FillGrid();
        }

        private void GetReminderStatus(DropDownList ddl, int showall)
        {
            SullolawReports slReport = new SullolawReports();
            DataTable dtStatus = clscall.GetReminderStatus();
            ddl.DataSource = dtStatus;
            ddl.DataMember = "Description";
            ddl.DataValueField = "Reminderid_PK";
            ddl.DataTextField = "Description";
            ddl.DataBind();
            if (showall == 1)
            {
                ListItem itm = new ListItem("All", "5");
                ddl.Items.Insert(0, itm);
                ddl.SelectedValue = "0";
            }
        }

        private void ClearControls()
        {
            txt_onlinecomment.Text = lbl_onlinename.Text = lbl_onlineemail.Text = lbl_onlinequestion.Text = hf_onlineoldcomments.Value = hf_onlineoldcomments.Value = "";
        }

        private void FillGrid() // Filling the Grid
        {
            try
            {
                calStartDate.SelectedDate = (calStartDate.SelectedDate.ToShortDateString() == "1/1/0001") ? DateTime.Now : calStartDate.SelectedDate;
                calenddate.SelectedDate = (calenddate.SelectedDate.ToShortDateString() == "1/1/0001") ? DateTime.Now : calenddate.SelectedDate;
                //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
                cb_showall.Checked = true;
                DataTable dtLegalConsultationReport = SullolawReports.GetLegalConsultationReport(1, Convert.ToInt32(cb_showall.Checked), calStartDate.SelectedDate, Convert.ToInt32(dd_callback.SelectedValue.ToString()), calenddate.SelectedDate, cb_AllFollowUpDate.Checked, Convert.ToInt32(ddl_practiceArea.SelectedValue)); // 1 for Houston records
                int serialnumber = 1;
                if (dtLegalConsultationReport.Rows.Count > 0)
                {
                    
                    if (!dtLegalConsultationReport.Columns.Contains("Sno"))
                    {
                        dtLegalConsultationReport.Columns.Add("Sno");
                    }
                    for (int i = 0; i < dtLegalConsultationReport.Rows.Count; i++)
                    {
                        dtLegalConsultationReport.Rows[i]["Sno"] = serialnumber.ToString();
                        serialnumber++;
                    }
                    if (Session["dtLegalConsultationReportRowsCount"] != null)
                    {
                        Session["dtLegalConsultationReportRowsCount"] = null;
                    }
                    Session["dtLegalConsultationReportRowsCount"] = dtLegalConsultationReport.Rows.Count;
                    gv_records.Visible = true;
                    gv_records.DataSource = dtLegalConsultationReport;
                    gv_records.DataBind();
                    Pagingctrl.PageCount = gv_records.PageCount;
                    Pagingctrl.PageIndex = gv_records.PageIndex;
                    Pagingctrl.SetPageIndex();
                }
                else
                {
                    lblMessage.Text = "No Records Found";
                    gv_records.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clsLogger.ErrorLog(ex);
            }
        }

        #endregion

    }
}


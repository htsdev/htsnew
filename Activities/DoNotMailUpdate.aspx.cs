using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Activities
{
    public partial class DoNotMailUpdate : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";

            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session               
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
            }
            catch(Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string[] keys = { "@FirstName", "@LastName", "@Address" };
                object[] values = { txt_FirstName.Text, txt_LastName.Text, txt_Address.Text };
                ClsDb.ExecuteSP("usp_hts_add_to_DoNotMail", keys, values);
                lbl_Message.Text = "Record saved!";
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

       
    }
}

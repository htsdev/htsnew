using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Activities
{
	/// <summary>
	/// Summary description for nexttelemail.
	/// </summary>
	public partial class nexttelemail : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lbl_error;
		protected System.Web.UI.WebControls.DataGrid dgresult;
		clsENationWebComponents ClsDB = new clsENationWebComponents();
		clsSession ClsSession=new clsSession();
		clsLogger clog = new clsLogger();
		string noticefrom="";
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList ddl_noticefrom;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.DropDownList ddl_noticeto;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		string	noticeto;


		private void Page_Load(object sender, System.EventArgs e)
		{
		// Put user code to initialize the page here

            //Waqas 5057 03/17/2009 Checking employee info in session
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
			{
				Response.Redirect("../frmlogin.aspx");
			}
			else //To stop page further execution
			{
				if ( ! IsPostBack)
				{

					try
					{

						DataSet ds = ClsDB.Get_DS_BySP("usp_HTS_List_All_Email");
                        if(ds!=null && ds.Tables.Count>0 && ds.Tables[0].Rows.Count>0)
                        {
                            dgresult.DataSource = ds;
                            dgresult.DataBind();
                            lblMessage.Visible = false;
                            lblMessage.Text = "";
                            msgDv.Visible = false;
                        }
                        else
                        {
                            lblMessage.Visible = true;
                            lblMessage.Text = "Record not found!";
                            msgDv.Visible = true;
                        }
						
			
								
						// Selecting Current Notice Dates in Drop Down Lists
						try
						{
							if ( ds.Tables.Count > 0) 
							{

								ddl_noticefrom.SelectedValue=ds.Tables[0].Rows[0][3].ToString();
								ddl_noticeto.SelectedValue=ds.Tables[0].Rows[0][5].ToString();
							}
						
						}
						catch(Exception ex)	{clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace); }
										
					}
					catch( Exception ex)
					{
						lbl_error.Text = ex.ToString();
						clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);				
					}

				}
			}			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgresult.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgresult_ItemCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void dgresult_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
			try
			{
					
	

				// Check IF Update Button is Clicked
				if  (e.CommandName=="update")
				{

					
					//Getting Dates Range
					noticefrom  = ddl_noticefrom.SelectedValue;
					noticeto= ddl_noticeto.SelectedValue;
	

					// Check All The Users And Update Their Information	
					foreach ( DataGridItem item in dgresult.Items )
					{
																							
							bool b=((CheckBox)item.FindControl("chkbox_name")).Checked;
							string uid = ((Label)item.FindControl("lbl_uid")).Text;
							string emailaddress = ((TextBox)item.FindControl("tb_email")).Text;
							// Updating User Status
							string[] Keys={"@emailID","@email","@notice","@noticeEnd","@default"};
							object[] values={uid,emailaddress,noticefrom,noticeto,Convert.ToInt16(b)};
							ClsDB.ExecuteSP("USP_HTS_update_email_settings",Keys,values);	
							
							
						}
					}
					
				}
			catch ( Exception ex ) 
			{
			lbl_error.Text = ex.ToString();
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}


		}

	}
}

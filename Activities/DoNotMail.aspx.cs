using System;
using System.Data;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using HTP.Components; //Sabir Khan 6167 07/17/2009 Added for NoMailFlag class...

//Yasir Kamal 6100 07/03/2009 namespace changed.
namespace HTP.Reports
{
    public partial class DoNotMail : System.Web.UI.Page
    {
        #region Variables
        clsLogger clog = new clsLogger();
        NoMailFlag noMail = null;
        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_Message.Text = "";
            if (!IsPostBack)
            {
                try
                {
                    FillStates();
                }
                catch (Exception ex)
                {
                    lbl_Message.Text = ex.Message;
                    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }
            }
        }

        protected void btnSearch_Details_Click(object sender, EventArgs e)
        {
            try
            {
                SearchByDetails();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSearchByLetterID_Click(object sender, EventArgs e)
        {
            try
            {
                SearchByLetterId();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void btnSaveByTicketNum_Click(object sender, EventArgs e)
        {
            try
            {
                SearchByTicketNumber();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // Abbas Qamar 9726 01-01-2011  
        // Adding the record if it doest not exist in tblTicketsArchive table 
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                SearchAndMarkedAsBadEmail();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        // Abbas Qamar 9726 10-26-2011 
        // Search by Address and adding the records
        protected void UpdateByAddress_Click(object sender, EventArgs e)
        {
            try
            {
                InsertRecordAsBadEmail();
                gv.DataSource = null;
                gv.DataBind();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        // tahir 4418 07/25/2008
        // changed the structure to update by DBId & record id instead of 
        // ticket number, LID or personal details...
        protected void gv_command(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                lbl_Message.Text = "";

                GridView gv = (GridView)sender;
                int RowIndex = Convert.ToInt32(e.CommandArgument) - 1;
                int result = 0;
                int DBId = 0;
                int RecordId = 0;

                int.TryParse(((Label)gv.Rows[RowIndex].FindControl("lbl_DBId")).Text.Trim(), out  DBId);
                int.TryParse(((Label)gv.Rows[RowIndex].FindControl("lbl_RecordId")).Text.Trim(), out  RecordId);

                string firstname = ((Label)gv.Rows[RowIndex].FindControl("lblFirstName")).Text.Trim();
                string lastname = ((Label)gv.Rows[RowIndex].FindControl("lbllastName")).Text.Trim();
                string address = ((Label)gv.Rows[RowIndex].FindControl("lbladdress")).Text.Trim();
                //Sabir Khan 6167 remove ticket number field for removing duplication in clients...
                //string ticketnumber = ((Label)gv.Rows[RowIndex].FindControl("lbl_TicketNumber")).Text.Trim();
                string city = ((Label)gv.Rows[RowIndex].FindControl("lblCity")).Text.Trim();
                string state = ((Label)gv.Rows[RowIndex].FindControl("lblState")).Text.Trim();
                string ZipCode = ((Label)gv.Rows[RowIndex].FindControl("lblzipcode")).Text.Trim();
                //Sabir Khan 6167 remove ticket number field for removing duplication in clients...
                noMail = new NoMailFlag(firstname, lastname, address, city, state, ZipCode, DBId, RecordId);
                result = noMail.UpdateFlag();
                ShowResult(result);

            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods


        private void FillStates()
        {
            try
            {
                DataTable dtStates = ClsDb.Get_DT_BySPArr("usp_HTS_List_All_States");

                foreach (DataRow dr in dtStates.Rows)
                {
                    ddl_States.Items.Add(new ListItem(dr["state"].ToString(), dr["stateid"].ToString()));
                    //Abbas Qamar 9726 10-25-2011 adding dropdown of state
                    ddlStates.Items.Add(new ListItem(dr["state"].ToString(), dr["stateid"].ToString()));
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
      
        // Abbas Qamar  9726 11/02/2011
        // searching records and if record doest not exist then inserting records by USP_hts_Add_donotmail_records
        /// <summary>
        /// Insert the Record as Bad E-mail
        /// </summary>
        private void InsertRecordAsBadEmail()
        {
            noMail = new NoMailFlag("", "", txtAddress.Text.Trim(), txtCity.Text.Trim(), Convert.ToInt32(ddlStates.SelectedValue),txtZipCode.Text.Trim(),3);

            DataSet ds = noMail.SearchAndAddAsBadMailByAddress();

            if(ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ErrMessage"].ToString() == "Successfully marked as Do Not Mail!")
                {
                    lbl_Message.Text = @"Address has been added sucessfully";
                }
                else
                {
                    lbl_Message.Text = ds.Tables[0].Rows[0]["ErrMessage"].ToString();
                }
            }
        }

        // Abbas Qamar  9726 11/02/2011
        // searching records and if record doest not exist then inserting records By USP_hts_Add_donotmail_records
        /// <summary>
        /// Search the Record and Mark them as Bad E-mail
        /// </summary>
        private void SearchAndMarkedAsBadEmail()
        {
            noMail = new NoMailFlag(txt_FirstName.Text.Trim(), txt_LastName.Text.Trim(), txt_Address.Text.Trim(), txt_City.Text.Trim(), Convert.ToInt32(ddl_States.SelectedValue),txt_ZipCode.Text.Trim(),0);
            
            DataSet ds = noMail.SearchAndAddAsBadMail();

            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                gv.DataSource = ds;
                gv.DataBind();
                gv.Columns[10].Visible = false;
                lbl_Message.Text = ds.Tables[0].Rows[0]["ErrMessage"].ToString();                   
            }
            
        }


        // tahir 4418 07/25/2008 
        // searching records by personal details..
        private void SearchByDetails()
        {
            noMail = new NoMailFlag(txt_FirstName.Text.Trim(), txt_LastName.Text.Trim(), txt_Address.Text.Trim(), txt_City.Text.Trim(), Convert.ToInt32(ddl_States.SelectedValue), ddl_States.SelectedItem.Text, txt_ZipCode.Text.Trim());
            DataSet ds = noMail.GetDataByDetails();
            //Yasir Kamal 6144 07/17/2009 display "no record found" message if no record found
            if (ds.Tables[0].Rows.Count > 0)
            {

                gv.DataSource = ds;
                gv.DataBind();
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["nomailflag"]))
                {                    
                    gv.Columns[10].Visible = false;
                    lbl_Message.Text = "Already exists";
                }
                else
                {                    
                    gv.Columns[10].Visible = true;                    
                    lbl_Message.Text = "";
                }
                
            }
            else
            {
                gv.DataSource = ds;
                gv.DataBind();
                lbl_Message.Text = "No Record Found";
            }
        }

        // tahir 4418 07/25/2008 
        // searching by letter id
        private void SearchByLetterId()
        {
            noMail = new NoMailFlag(Convert.ToInt32(this.txt_LetterID.Text.Trim()));
            DataSet ds = noMail.GetDataByLetterId();
            if (ds.Tables[0].Rows.Count > 0)
            {
                gv.DataSource = ds;
                gv.DataBind();
            }
            else
            {
                gv.DataSource = ds;
                gv.DataBind();
                lbl_Message.Text = "No Record Found";
            }
        }


        // tahir 4418 07/25/2008 
        // changed the application structure
        private void SearchByTicketNumber()
        {
            noMail = new NoMailFlag(txt_TicketID.Text);
            DataSet ds = noMail.GetDataByTicketNumber();
            if (ds.Tables[0].Rows.Count > 0)
            {
                gv.DataSource = ds;
                gv.DataBind();
            }
            else
            {
                gv.DataSource = ds;
                gv.DataBind();
                lbl_Message.Text = "No Record Found";
            }
            searcharea.Style["display"] = "block";
        }

        private void ShowResult(int value)
        {
            switch (value)
            {
                case 0:
                    lbl_Message.Text = "No such record exists";
                    break;

                case 1:
                    lbl_Message.Text = "Record updated!";
                    break;

                case 2:
                    lbl_Message.Text = "Address could not be verified";
                    break;

                case 3:
                    lbl_Message.Text = "Record already in the list";
                    break;
            }
        }

        #endregion

    }
}

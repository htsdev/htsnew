﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="HTP.Activities.nexttelemail"
    CodeBehind="nexttelemail.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>HTP Nexttel Email</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>

     <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <meta content="C#" name="CODE_LANGUAGE"/>
    <meta content="JavaScript" name="vs_defaultClientScript"/>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
    <link href="../Styles.css" type="text/css" rel="stylesheet"/>
</head>
<body ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        
        <aspnew:ScriptManager ID="scriptManager" runat="server">
            </aspnew:ScriptManager>
        <div class="page-container row-fluid container-fluid">

 <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">
            <div class="col-xs-12">
            <div class="alert alert-info alert-dismissable fade in" id="msgDv" visible="false" runat="server" style="    margin-top: 10px;">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server"  EnableViewState="False"></asp:Label>
                 </div>
            
                </div>

             <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Text Message</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>
            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Global Settings</h2>
                     <div class="actions panel_actions pull-right">
                     <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="emailhistory.aspx?sMenu=26">History</asp:HyperLink>
                         <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                             
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:Label ID="Label6" runat="server" CssClass="form-label">Notice From</asp:Label>
                                        <asp:DropDownList ID="ddl_noticefrom" runat="server" CssClass="form-control"
                                            >
                                            <asp:ListItem Value="0" Selected ="True">0</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                        </asp:DropDownList>
                                        
                                      
                                     
                                    </div>
                                                                </div>
                         </div>
                      <div class="col-md-4">
                                                            <div class="form-group">
                              
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                       <asp:Label ID="Label5" runat="server" CssClass="form-label">To</asp:Label>
                                        <asp:DropDownList ID="ddl_noticeto" runat="server" CssClass="form-control"
                                            >
                                            <asp:ListItem Value="0" Selected ="True">0</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="6">6</asp:ListItem>
                                            <asp:ListItem Value="7">7</asp:ListItem>
                                            <asp:ListItem Value="8">8</asp:ListItem>
                                            <asp:ListItem Value="9">9</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                     <div class="clearfix"></div>

                    <div class="col-md-12">
                                                            <div class="form-group">
                              <asp:Label ID="Label1" runat="server" CssClass="form-label"></asp:Label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <div class="table-responsive" data-pattern="priority-columns">
                                    <asp:DataGrid ID="dgresult" runat="server" BorderColor="White" CssClass="table table-small-font table-bordered table-striped"
                                Width="100%" CellPadding="0" ShowHeader="False" ShowFooter="True" BorderStyle="None"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle HorizontalAlign="Left" Width="100px" CssClass="clsaspcolumnheader">
                                        </HeaderStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkbox_name" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'
                                                Checked='<%# DataBinder.Eval(Container, "DataItem.defaultflag") %>'></asp:CheckBox>
                                            <asp:Label ID="lbl_uid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderStyle HorizontalAlign="Left" Width="168px" CssClass="clsaspcolumnheader">
                                        </HeaderStyle>
                                        <HeaderTemplate>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="tb_email" runat="server" CssClass="form-control inline-textbox" Width="100%"
                                                Text='<%# DataBinder.Eval(Container, "DataItem.email") %>' MaxLength="200"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <div class="col-md-12">
<asp:Button ID="btn_update" runat="server" CssClass="btn btn-primary pull-right" Text="Update" CommandName="update">
                                            </asp:Button>
                                            </div>
                                            
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                                         </div>
                                     
                                    </div>
                                                                </div>
                         </div>


                    


                    </div>
                     </div>

                 </section>


             <div class="clearfix"></div>

             

            <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>

      <%--      <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <uc1:footer id="Footer1" runat="server"></uc1:footer>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>--%>



            </section>
                     </section>
            </div>
   
    </form>
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
</body>
</html>

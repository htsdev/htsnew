<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.frmReports" Codebehind="frmReports.aspx.cs" %>
<%@ Register TagPrefix="uc2" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>frmReports</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script lang="javascript">
			
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" >
		<form id="Form1" method="post" runat="server">
			<table id="tblMain1" borderColor="#e4e2e2" cellSpacing="0" cellPadding="0" width="764"
				align="center" border="0">
				<tr>
					<td vAlign="top">
						<!-- For Header --><uc1:activemenu id="ActiveMenu1" runat="server"></uc1:activemenu></td>
				</tr>
				<tr>
					<td>
						<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
							<tr>
								<td style="HEIGHT: 16px" background="../../images/separator_repeat.gif" height="16"></td>
							</tr>
							<TR>
								<TD class="clsleftpaddingtable" id="h1" vAlign="middle" align="center">
									<table cellSpacing="0" cellPadding="0" width="780" border="0">
										<tr>
											<td class="clsleftpaddingtable" style="WIDTH: 83px">Search Report :</td>
											<td style="WIDTH: 101px"><asp:textbox id="txtSearchSp" Runat="server" CssClass="clsinputadministration"></asp:textbox>&nbsp;
											</td>
											<td class="clsleftpaddingtable">City</td>
											<td style="WIDTH: 90px"><asp:dropdownlist id=cboLoc Runat="server" CssClass="clsinputcombo" DataSource='<%# DataBinder.Eval(Container, "DataItem.ReportLoc") %>'>
													<asp:ListItem Value="0">Both</asp:ListItem>
													<asp:ListItem Value="1">Houston</asp:ListItem>
													<asp:ListItem Value="2">Dallas</asp:ListItem>
												</asp:dropdownlist></td>
											<td class="clsleftpaddingtable" style="WIDTH: 140px">
                                                &nbsp;<asp:DropDownList ID="ddl_category" runat="server" Width="130px">
                                                </asp:DropDownList></td>
                                            <td class="clsleftpaddingtable" style="width: 90px">
                                                &nbsp;
                                                <asp:button id="imgbtnGo" runat="server" CssClass="clsbutton" Text="Search"></asp:button></td>
											<td class="clsleftpaddingtable" align="right"><asp:label id="lblCurrPage" runat="server" CssClass="label" Font-Size="Smaller">Current Page :</asp:label><asp:label id="lblPNo" runat="server" CssClass="label" Font-Size="Smaller">a</asp:label>&nbsp;<asp:label id="lblGoto" runat="server" CssClass="label" Font-Size="Smaller">Goto</asp:label>&nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" CssClass="clsinputcombo" Font-Size="Smaller" AutoPostBack="True"></asp:dropdownlist>
											</td>
										</tr>
									</table>
									<asp:label id="lblMessage" runat="server" CssClass="cmdlinks" ForeColor="Red" Font-Bold="True"></asp:label></TD>
							</TR>
							<TR>
								<TD class="clssubhead" style="HEIGHT: 34px" align="left" background="../../Images/subhead_bg.gif">&nbsp;List 
									of Reports</TD>
							</TR>
							<TR>
								<TD vAlign="top">
								<asp:datagrid id="DgSqlProcedure" runat="server" CssClass="clsleftpaddingtable" PageSize="20"
										AllowSorting="True" AutoGenerateColumns="False" AllowPaging="True" Width="100%" BorderColor="White">
										<Columns>										    
										    <asp:TemplateColumn>
										    
										        <ItemTemplate>
									            <table id="tbl_categoryname" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" visible="false">
                                                    <tr id="tr_Sep" runat="server" visible="false" style="width: 100%; height: 35%">
                                                        <td width="100%" style="background-color:Gray">
                                                        </td>
                                                        <td width="100%" style="background-color:Gray">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td width="80%" bgcolor="#003f7d" class="textmain" style="height: 16px">                                                        
                                                        &nbsp;<asp:Label ID="lbl_Category" CssClass="Labelfrmmain" Font-Bold="true" runat="server" ForeColor="White" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryName") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_courtID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ReportCategoryID") %>' />
                                                        <asp:HiddenField ID="hf_catn" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.ShortName") %>' />
                                                    </td>                                                    
                                                    </tr>
                                                </table>
                                                
                                                <table runat="server" visible="false" id="tblheader" width="100%" cellpadding="0" cellspacing="0" border="0" >
                                                    <tr>
                                                        <td class="Labelfrmmain"  style="height:22px;width:5%; font-weight:bold">
                                                            S.No.
                                                        </td>
                                                        <td class="Labelfrmmain" style="height:22;width:25%;font-weight:bold">
                                                           Report Name
                                                        </td>                                                         
                                                        <td class="Labelfrmmain" style="height:22;width:40%;font-weight:bold">
                                                         Description
                                                        </td>
                                                        <td class="Labelfrmmain" style="height:22;width:15%;font-weight:bold">
                                                           Create Date
                                                        </td>                                                                                                                                                      
                                                    </tr>
                                                </table>
                                                                
                                                 
                                                <table id="tbl_info" runat="server" cellpadding="0" cellspacing="0" border="1" width="100%" style="border-collapse: collapse; border-bottom-style: none;border-right-style :none; border-right-color:White">                                                                   
                                                    <tr>
                                                         <td  width="5%" style="border-right-style: solid;">
                                                            &nbsp;<asp:Label id="lblSno" runat="server" CssClass="textmain"></asp:Label>
                                                         </td>
                                                        <td  width="25%">     
                                                           
                                                            <asp:HyperLink id="HLSpnm" runat="server" CssClass="textmain" Text='<%# DataBinder.Eval(Container, "DataItem.RptName") %>'>
													        </asp:HyperLink>
													        <asp:HiddenField ID="hf_rptid" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.RptId") %>' />
                                                         </td>                                                            
                                                        <td width="40%">
                                                            <asp:Label id="Label1" runat="server" CssClass="textmain" Text='<%# DataBinder.Eval(Container, "DataItem.RptDescription") %>'>
													        </asp:Label>
                                                         </td>       
                                                         <td width="15%">
                                                            <asp:Label id="Label2" runat="server" CssClass="textmain" Text='<%# DataBinder.Eval(Container, "DataItem.CrtDate") %>'>
												            </asp:Label>
												            <asp:HyperLink id="HLEdit" Visible="false" runat="server" CssClass="textmain">Edit</asp:HyperLink>
                                                         </td>    
                                                    </tr>                                                                    
                                                </table>
                                                                               
										        </ItemTemplate>
										    </asp:TemplateColumn>
										</Columns>										
                                    <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Center" PageButtonCount="5" NextPageText="Next &gt;&gt;" PrevPageText="&lt;&lt; Previous" />
									</asp:datagrid>
									</td>
							</tr>
									<tr>
									    <td width="780" background="../../images/separator_repeat.gif" colSpan="5" height="11">
                                            <asp:HiddenField ID="hf_sno" runat="server" />
                                        </td>
									</tr>
									<tr>
									<td><uc2:footer id="Footer1" runat="server"></uc2:footer></TD>
									</tr>
						</TABLE>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

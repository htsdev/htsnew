using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using FrameWorkEnation.Components;
using LntechLocalCaching;
using lntechNew.Components.ClientInfo;


namespace HTP.Activities
{
    /// <summary>
    /// Summary description for frmClientLookup.
    /// </summary>
    public partial class frmClientLookup : System.Web.UI.Page
    {

        #region Object Declarations
        DataSet ds;
        DataSet dsNewInfo;
        DataSet dsPrevInfo;
        //Change by ajmal
        //SqlDataReader drComments;
        IDataReader drComments;

        string StrExp;
        string StrAcsDec;
        Int32 recCount;
        bool blnUpdated = false;
        bool blnClientMoved = false;
        bool isSignUp = false;


        private LntechLocalCaching.ICacheService mCache;// = new LntechLocalCaching.CacheManager(HttpContext.Current.Session.SessionID);
        private DateAndVersion dav = new DateAndVersion(DateTime.UtcNow, "no version");
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsLogger clog = new clsLogger();
        protected System.Web.UI.WebControls.Label lblFromDate;
        protected System.Web.UI.WebControls.DropDownList cmbCaseType;
        protected System.Web.UI.WebControls.DropDownList cmbCourtType;
        protected System.Web.UI.WebControls.Label lblMessage;
        //protected eWorld.UI.CalendarPopup dtFrom;
        protected System.Web.UI.WebControls.Label lblCourtType;
        protected System.Web.UI.WebControls.Label lblGoto;
        protected System.Web.UI.WebControls.Label lblPNo;
        protected System.Web.UI.WebControls.DropDownList cmbPageNo;
        protected System.Web.UI.WebControls.Label lblClientType;
        protected System.Web.UI.WebControls.Label lblCaseType;
        protected System.Web.UI.WebControls.DataGrid dgClient;
        protected System.Web.UI.WebControls.ImageButton btnSubmit;
        protected System.Web.UI.WebControls.DataGrid dgViolPrevious;
        protected System.Web.UI.WebControls.Label lblPCCR;
        protected System.Web.UI.WebControls.DropDownList cmbPCCR;
        protected System.Web.UI.WebControls.Label Label8;
        protected System.Web.UI.WebControls.LinkButton btnPrevClient;
        protected System.Web.UI.WebControls.LinkButton btnNextClient;
        protected System.Web.UI.WebControls.DataGrid dgViolNew;
        protected System.Web.UI.WebControls.Label lblName;
        protected System.Web.UI.WebControls.Label lblLanguage;
        protected System.Web.UI.WebControls.Label lblFee;
        protected System.Web.UI.WebControls.RadioButton optCDLYes;
        protected System.Web.UI.WebControls.RadioButton optCDLNo;
        protected System.Web.UI.WebControls.Label lblContact1;
        protected System.Web.UI.WebControls.Label lblContact2;
        protected System.Web.UI.WebControls.Label lblContact3;
        protected System.Web.UI.WebControls.Label lblContact4;
        protected System.Web.UI.WebControls.TextBox txtComments;
        protected System.Web.UI.WebControls.DropDownList ddlUpdateStatus;
        protected System.Web.UI.WebControls.CheckBox chkNoContactList;
        protected System.Web.UI.WebControls.ImageButton btnUpdateComments;
        protected System.Web.UI.WebControls.RadioButton optFutureNo;
        protected System.Web.UI.WebControls.RadioButton optFutureYes;
        protected System.Web.UI.WebControls.TextBox txtRecCount;
        protected System.Web.UI.WebControls.Label Label2;
       // protected eWorld.UI.CalendarPopup dtTo;
        protected System.Web.UI.WebControls.ImageButton btn_Reset;
        protected System.Web.UI.WebControls.RadioButton optAccNo;
        protected System.Web.UI.WebControls.RadioButton optAccYes;
        protected System.Web.UI.WebControls.Label lblContact5;
        protected System.Web.UI.WebControls.Label lblContact6;
        protected System.Web.UI.WebControls.Label lblRecCount;
        protected System.Web.UI.WebControls.CheckBox chkPriceShopping;
        protected System.Web.UI.WebControls.CheckBox chkHiredAnother;
        protected System.Web.UI.WebControls.CheckBox CheckBox5;
        protected System.Web.UI.WebControls.CheckBox CheckBox6;
        protected System.Web.UI.WebControls.CheckBox CheckBox7;
        protected System.Web.UI.WebControls.CheckBox CheckBox1;
        protected System.Web.UI.WebControls.CheckBox chkHandleMySelf;
        protected System.Web.UI.WebControls.CheckBox chkPaidTickets;
        protected System.Web.UI.WebControls.CheckBox chkDSCProb;
        protected System.Web.UI.WebControls.CheckBox chkNoMoney;
        protected System.Web.UI.WebControls.CheckBox chkTooExpensive;
        protected System.Web.UI.WebControls.CheckBox chkAll;
        protected System.Web.UI.WebControls.Label lblCurrPage;
        protected System.Web.UI.WebControls.Label lblAll;
        protected System.Web.UI.WebControls.Label lblPageSize;
        protected System.Web.UI.WebControls.DropDownList ddlPageSize;
        protected System.Web.UI.WebControls.CheckBox chkPoorService;
        protected System.Web.UI.WebControls.TextBox txtStatus;
        protected System.Web.UI.WebControls.ImageButton btnSignUp;
        protected System.Web.UI.WebControls.TextBox txtNewCaseNo;
        protected System.Web.UI.WebControls.TextBox txtFTATicket;
        protected System.Web.UI.WebControls.DropDownList cmbClientType;
        clsSession ClsSession = new clsSession();
        #endregion

        #region Procedures for server control events....

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (ClsSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }

                btnUpdateComments.Attributes.Add("onclick", "javascript: return DoValidation();");
                txtRecCount.Attributes.Add("OnTextChanged ", "javascript: ShowHidePageNavigation();");
                chkAll.Attributes.Add("OnClick", "javascript: TogglePageNavigation();");
                btnSignUp.Attributes.Add("onclick", "javascript: return SignUp();");

                if (chkAll.Checked == true)
                    dgClient.AllowPaging = false;
                else
                    dgClient.AllowPaging = true;

                mCache = new LntechLocalCaching.CacheManager(HttpContext.Current.Session.SessionID);


                if (!IsPostBack)
                {

                    Session["CurrentPageNumber"] = String.Empty;
                    Session["CurrentSelectedClient"] = String.Empty;

                    FillPCCR();
                    FillStatuses();
                }
            }
            catch (Exception e_pload)
            {
                lblMessage.Text = e_pload.Message;
                clog.ErrorLog(e_pload.Message, e_pload.Source, e_pload.TargetSite.ToString(), e_pload.StackTrace);
            }
        }


        private void chkAll_CheckedChanged(object sender, System.EventArgs e)
        {
            try
            {
                PopulateControlsFromSession();
                if (chkAll.Checked == false)
                {
                    if (dgClient.SelectedIndex >= dgClient.PageSize)
                    {
                        int idxCurrClientAll = dgClient.SelectedIndex;
                        int idxCurrClientPaging = ((int)(idxCurrClientAll % dgClient.PageSize));

                        //Modified by kazim Task-id:2567
                        //To resolve problem i first check pageindex property, if it is negative then it will not set
                        //dgclient currentpageindex property.

                        int index = (int)(dgClient.SelectedIndex / dgClient.PageSize);
                        if (index >= 0)
                        {
                            dgClient.CurrentPageIndex = index;
                        }

                        dgClient.SelectedIndex = idxCurrClientPaging;
                        dgClient.DataSource = mCache.retrieveDataView("dvClient", "", typeof(DataView));
                        dgClient.DataBind();
                        GenerateSerial();
                        SetNavigation();

                        //Modfified by kazim Task-id:2566  
                        //To resolve the problem i first check selectedindex property, if it's value is negative
                        //then it will not go to selectedindex property of datagrid.

                        if (dgClient.SelectedIndex != -1)
                        {
                            GetDetailInfo(dgClient.Items[dgClient.SelectedIndex].Cells[4].Text);
                        }
                    }
                    //khalid 3082  2/13/08 to show pages dropdown
                    cmbPageNo.Style[HtmlTextWriterStyle.Display] = "block";
                    lblGoto.Style[HtmlTextWriterStyle.Display] = "block";
                    lblPageSize.Style[HtmlTextWriterStyle.Display] = "block";

                    //ddlPageSize.Style.Add(HtmlTextWriterStyle.Display,"block");
                    ddlPageSize.Visible = true;
                }
                else
                {
                    if (dgClient.CurrentPageIndex != 0)
                    {
                        int idxClient = (dgClient.CurrentPageIndex * dgClient.PageSize) + dgClient.SelectedIndex;
                        dgClient.SelectedIndex = idxClient;
                    }
                    dgClient.CurrentPageIndex = 0;
                    dgClient.DataSource = mCache.retrieveDataView("dvClient", "", typeof(DataView));
                    dgClient.DataBind();
                    GenerateSerial();
                    SetNavigation();
                    if (dgClient.SelectedIndex != -1)
                    {
                        GetDetailInfo(dgClient.Items[dgClient.SelectedIndex].Cells[4].Text);
                    }

                    //khalid 3082  2/13/08 to show pages dropdown
                    cmbPageNo.Style[HtmlTextWriterStyle.Display] = "none";
                    lblGoto.Style[HtmlTextWriterStyle.Display] = "none";
                    lblPageSize.Style[HtmlTextWriterStyle.Display] = "none";
                    //ddlPageSize.Style.Add(HtmlTextWriterStyle.Display, "none");
                    ddlPageSize.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // Procedure for setting the grid page number as per selected from page no. combo.......

            try
            {
                if (chkAll.Checked == false)
                {
                    dgClient.CurrentPageIndex = cmbPageNo.SelectedIndex;
                    ClearComments();
                    FillGrid(false);
                    SetNavigation();
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void SetHyperLinks()
        {
            try
            {
                txtNewCaseNo.Text = String.Empty;
                foreach (DataGridItem ItemX in dgViolNew.Items)
                {
                    string strNewTicketNumber = ((HyperLink)(ItemX.FindControl("lnkNewCaseNo"))).Text;
                    strNewTicketNumber = strNewTicketNumber.Substring(0, strNewTicketNumber.IndexOf("-"));
                    string FirstChar = strNewTicketNumber.Substring(0, 1);

                    //if (ddlUpdateStatus.SelectedIndex != 0 )
                    {
                        if (FirstChar == "F" || FirstChar == "f")
                        {
                            //ozair 4767 09/11/2008 changed the old asp application logic to current application.                            
                            ((HyperLink)(ItemX.FindControl("lnkNewCaseNo"))).NavigateUrl = "../frmmain.aspx?search=1&lstcriteriaValue3=" + strNewTicketNumber + "&lstcriteria3=0";
                            txtFTATicket.Text = strNewTicketNumber;
                        }
                        else
                        {
                            txtNewCaseNo.Text = strNewTicketNumber;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void dgViolPrevious_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                Label lblPrevTicketId = (Label)e.Item.FindControl("lblTicketId");

                if (e.CommandName == "cmdPrevCaseNo")
                {
                    Response.Redirect("../ClientInfo/violationfeeold.aspx?search=0&caseNumber=" + lblPrevTicketId.Text, false);
                }
                else if (e.CommandName == "cmdViolOutcome")
                {
                    ClsSession.CreateCookie("sTicketID", lblPrevTicketId.Text, this.Request, this.Response);
                    //Response.Redirect("../ClientInfo/CaseDisposition.aspx?casenumber=" + lblPrevTicketId.Text );
                    Response.Redirect("../ClientInfo/CaseDisposition.aspx?search=0&sMenu=65&casenumber=" + lblPrevTicketId.Text, false);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void dgClient_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                lblMessage.Text = String.Empty;

                if (e.CommandName == "GetDetails")
                {

                    dgClient.SelectedIndex = e.Item.ItemIndex;

                    //CLEARING PREVIOUSLY ENTERED COMMENTS.....
                    ClearComments();

                    // CALLING PROCEDURE FOR GETTING NEW & PREVIOUS TICKETS INFORMATION...
                    //GetDetailInfo ( dgClient.Items[dgClient.SelectedIndex].Cells[4].Text );
                    GetDetailInfo(dgClient.Items[dgClient.SelectedIndex].Cells[4].Text, dgClient.Items[dgClient.SelectedIndex].Cells[6].Text);
                    EnableDisableButtons();
                    hf_FocusFlag.Value = "1";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void dgClient_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                GetCurrentDetailInfo();
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void dgClient_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            try
            {

                if (chkAll.Checked == false)
                    GotoPage(e.NewPageIndex);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void dgClient_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            try
            {
                // Procedure for handling sorting requests with in data grid......


                //string SortNm = e.SortExpression;
                // Calling procedure for checking ASC/DESC order in sort expression....
                SetAcsDesc(e.SortExpression);

                // Setting the data grid as per sort expression......
                SortDataGrid(e.SortExpression);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void btnNextClient_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (dgClient.SelectedIndex >= 0)
                {
                    lblMessage.Text = String.Empty;
                    if (dgClient.SelectedIndex == dgClient.Items.Count - 1)  /// if last row in grid is selected..
                    {
                        if (chkAll.Checked == false)
                        {
                            if (dgClient.CurrentPageIndex != dgClient.PageCount - 1)
                            {
                                GotoPage(++dgClient.CurrentPageIndex);
                                dgClient.SelectedIndex = 0;
                                SetNavigation();
                            }
                        }
                    }
                    else
                    {
                        dgClient.SelectedIndex += 1;
                    }
                    ClearComments();
                    GetCurrentDetailInfo();

                }
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void btnPrevClient_Click(object sender, System.EventArgs e)
        {
            try
            {
                lblMessage.Text = String.Empty;
                if (dgClient.SelectedIndex > 0)
                {
                    dgClient.SelectedIndex -= 1;
                    ClearComments();
                    GetCurrentDetailInfo();

                }
                else
                {
                    if (chkAll.Checked == false)
                    {
                        if (dgClient.SelectedIndex == 0)
                        {
                            if (dgClient.CurrentPageIndex > 0)
                            {
                                GotoPage(--dgClient.CurrentPageIndex);
                                dgClient.SelectedIndex = dgClient.Items.Count - 1;
                                ClearComments();
                                SetNavigation();
                                GetCurrentDetailInfo();
                            }
                        }
                    }
                }

                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void btnUpdateComments_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            cmbPCCR.SelectedIndex = 2;
            try
            {

                //	Checking if this a valid session
                if (this.Session.Count == 0 || Session["intRecordCount"] == null || Convert.ToString(Session["intRecordCount"]).Length == 0)
                {
                    Response.Redirect("../frmLogin.aspx", false);
                }

                if (dgViolNew.Items.Count != 0 && Convert.ToString(Session["intRecordCount"]) != "0")
                {
                    // procedure for saving comments & PCCR updated status.
                    SaveComments();

                    blnUpdated = true;
                    //dgClient.Items[dgClient.SelectedIndex].re

                    if (chkNoContactList.Checked == true)
                    {
                        // Calling procedure for populating grid with data.....
                        //FillGrid(false);
                        PopulateControlsFromSession();
                        ClearPhoneNumbers();
                        GetPhoneNumbers();

                    }
                    else
                    {
                        PopulateControlsFromSession();
                    }

                    // Calling procedure for populating page numbers in combo.....
                    FillPageList();

                    // Calling procedure for setting current page number of data grid....
                    SetNavigation();

                    GenerateSerial();

                    // refreshing comments grid
                    GetCommentsInfo();

                    // procedure for initializing controls (comments & pccr )......
                    ClearComments();

                    blnUpdated = false;
                    SetHyperLinks();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void PopulateControlsFromSession()
        {
            try
            {
                //mCache.clearCache();

                dgClient.DataSource = mCache.retrieveDataView("dvClient", "", typeof(DataView));
                dgClient.DataBind();
                GenerateSerial();
                SetNavigation();

                dgViolNew.DataSource = mCache.retrieveDataset("dsNewInfo", "", typeof(DataSet));
                dgViolNew.DataBind();

                dgViolPrevious.DataSource = mCache.retrieveDataset("dsPrevInfo", "", typeof(DataSet));
                dgViolPrevious.DataBind();
                PaymentGrouping();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }


        private void btn_Reset_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                // Procedure for handling page resets.......
                
                // Setting & Resetting controls.........
                txtfrom.Text = DateTime.Now.Date.ToShortDateString();
                txtto.Text = DateTime.Now.Date.ToShortDateString();
                cmbCaseType.SelectedIndex = 0;
                cmbCourtType.SelectedIndex = 0;
                cmbPCCR.SelectedIndex = 0;
                cmbClientType.SelectedIndex = 0;
                optFutureNo.Checked = true;
                optFutureYes.Checked = false;
                lblMessage.Text = String.Empty;

                lblCurrPage.Visible = false;
                lblGoto.Visible = false;
                cmbPageNo.Visible = false;
                lblPNo.Visible = false;
                lblRecCount.Visible = false;
                cmbPageNo.ClearSelection();

                lblName.Text = String.Empty;
                lblLanguage.Text = String.Empty;
                lblFee.Text = String.Empty;
                lblContact1.Text = String.Empty;
                lblContact2.Text = String.Empty;
                lblContact3.Text = String.Empty;
                lblContact4.Text = String.Empty;
                lblContact5.Text = String.Empty;
                lblContact6.Text = String.Empty;

                optCDLYes.Checked = false;
                optCDLNo.Checked = false;

                optAccYes.Checked = false;
                optAccNo.Checked = false;

                chkDSCProb.Checked = false;
                chkPriceShopping.Checked = false;
                chkPaidTickets.Checked = false;
                chkHandleMySelf.Checked = false;
                chkHiredAnother.Checked = false;
                chkNoMoney.Checked = false;
                chkTooExpensive.Checked = false;

                txtComments.Text = String.Empty;
                ddlUpdateStatus.SelectedIndex = 0;
                chkNoContactList.Checked = false;
                txtRecCount.Text = "0";

                chkAll.Checked = false;
                ddlPageSize.SelectedIndex = 0;

                SetClientGrid();
                SetOtherGrids();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {


                //Kazim 3556 4/17/2008 Set SelectedIndex=0 at when searching the records 

                dgClient.SelectedIndex = 0;
                //	Checking if this a valid session
                if (this.Session.Count == 0)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                // Procedure for handling search requests.......			

                // Calling procedure for checking valid input from user.....
                if (DoValidation() == true)
                {
                    // initializing grid.......
                    dgClient.EditItemIndex = -1;
                    dgClient.CurrentPageIndex = 0;

                    // adding dataset to the local cache....
                    AddDStoCache(PullData());

                    // Calling procedure for populating grid with data.....
                    FillGrid(true);

                    // Calling procedure for populating page numbers in combo.....
                    FillPageList();

                    // Calling procedure for setting current page number of data grid....
                    SetNavigation();

                    td_title.Visible = true;
                    td_title_hired.Visible = true;
                    // GETTING DETAILED INFORMATION....
                    //GetCurrentDetailInfo();
                    chkAll.Visible = true;
                    lblAll.Visible = true;

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void AddDStoCache(DataSet ds)
        {
            try
            {
                // storing data set....
                mCache.storeDataset("dsClient", "", dav, ds);

                //storing data view....
                mCache.storeDataView("dvClient", "", dav, ds.Tables[0].DefaultView);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void ddlPageSize_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                dgClient.PageSize = (int)(Convert.ChangeType(ddlPageSize.Items[ddlPageSize.SelectedIndex].Value, typeof(int)));
                FillGrid(true);
                FillPageList();
                SetNavigation();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }


        private void btnSignUp_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                isSignUp = true;
                SaveComments();
                isSignUp = false;
                /*
                ClsSession.CreateCookie("sMidNum",  ((Label) (dgViolNew.Items[0].FindControl("lblMidNum"))).Text ,this.Request  ,this.Response);
                ClsSession.CreateCookie("sAdd1",((Label) (dgViolNew.Items[0].FindControl("lblAddress"))).Text,this.Request ,this.Response);
                ClsSession.CreateCookie("sZip",((Label) (dgViolNew.Items[0].FindControl("lblZip"))).Text,this.Request , this.Response);
                */
                string Add1 = ((Label)(dgViolNew.Items[0].FindControl("lblAddress"))).Text;
                string MidNum = ((Label)(dgViolNew.Items[0].FindControl("lblMidNum"))).Text;
                string zipcode = ((Label)(dgViolNew.Items[0].FindControl("lblZip"))).Text;
                try
                {
                    /*
                    if (txtNewCaseNo.Text.Length != 0)
                        Response.Redirect("../ClientInfo/ViolationFeeOld.aspx?search=999&caseNumber=" + txtNewCaseNo.Text,false) ;
                    else if (txtFTATicket.Text.Length != 0 )
                        Response.Redirect("../frmMain.aspx?search=2&lstcriteriaValue3="+ txtFTATicket.Text +"&lstcriteria3=0",false)  ;
                    */
                    //
                    if (txtNewCaseNo.Text.Length != 0)
                    {
                        clsCase ClsCase = new clsCase();

                        //Kazim 3938 6/10/2008 pass 1 for Traffic Client 

                        string[] ticketidFlag = ClsCase.GetCaseInfoFromAllNonClients(999, txtNewCaseNo.Text, Convert.ToInt32(ClsSession.GetCookie("sEmpID", this.Request)), Add1, zipcode, 0, MidNum, 1);
                        if (ticketidFlag[1] != "2")
                        {
                            //HttpContext.Current.Response.Write("<script language='javascript'>alert('Profile Already Exists.');</script>");
                            Response.Redirect("../ClientInfo/ViolationFeeold.aspx?search=" + ticketidFlag[1] + "&PC=1&caseNumber=" + ticketidFlag[0], false);
                        }
                        else
                        {
                            Response.Redirect("../ClientInfo/ViolationFeeold.aspx?search=1&PC=1&caseNumber=" + ticketidFlag[0], false);
                        }
                    }
                    else if (txtFTATicket.Text.Length != 0)
                        Response.Redirect("../frmMain.aspx?search=2&lstcriteriaValue3=" + txtFTATicket.Text + "&lstcriteria3=0", false);
                    //
                }
                catch (Exception ex)
                {
                    //ozair 4767 09/11/2008 Commented the check for index out of bond check, as exception must be logged
                    //and index out of bond exception won't be thrown agian it was handled in procedure
                    //if (ex.Message == "Index was outside the bounds of the array.")
                    //{
                    //    //Response.Write("<script language='javascript'>alert('The selected record had already signed up.');</script>");
                    //    Server.Transfer("frmclientlookup.aspx");
                    //}
                    //else
                    //{
                    lblMessage.Text = ex.Message + ex.StackTrace.ToString() + ex.TargetSite.ToString();
                    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                    //}
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        protected void dgViolPrevious_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            try
            {
                Label hf = (Label)e.Item.FindControl("lbl_CDLFlag");

                if (hf != null)
                {
                    if (hf.Text == "1")
                    {
                        optCDLYes.Checked = true;
                        optCDLNo.Checked = false;
                    }
                    else if (hf.Text == "0")
                    {
                        optCDLYes.Checked = false;
                        optCDLNo.Checked = true;
                    }
                }

                ImageButton ibtn = (ImageButton)e.Item.FindControl("ibtn_GC");

                if (ibtn != null)
                {
                    if (ibtn.ToolTip == "" || ibtn.ToolTip == String.Empty)
                        ibtn.Visible = false;
                    else
                        ibtn.Visible = true;
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.btnSubmit_Click);
            this.btn_Reset.Click += new System.Web.UI.ImageClickEventHandler(this.btn_Reset_Click);
            this.ddlPageSize.SelectedIndexChanged += new System.EventHandler(this.ddlPageSize_SelectedIndexChanged);
            this.cmbPageNo.SelectedIndexChanged += new System.EventHandler(this.cmbPageNo_SelectedIndexChanged);
            this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            this.dgClient.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgClient_ItemCommand);
            this.dgClient.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgClient_PageIndexChanged);
            this.dgClient.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgClient_SortCommand);
            this.dgClient.SelectedIndexChanged += new System.EventHandler(this.dgClient_SelectedIndexChanged);
            this.btnPrevClient.Click += new System.EventHandler(this.btnPrevClient_Click);
            this.btnNextClient.Click += new System.EventHandler(this.btnNextClient_Click);
            this.btnSignUp.Click += new System.Web.UI.ImageClickEventHandler(this.btnSignUp_Click);
            this.btnUpdateComments.Click += new System.Web.UI.ImageClickEventHandler(this.btnUpdateComments_Click);
            this.dgViolPrevious.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgViolPrevious_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        #region Filling Searching Criteria Controls from lookup tables

        private void FillStatuses()
        {
            //change by ajmal
            //SqlDataReader drStatus;

            IDataReader drStatus;

            cmbCaseType.Items.Clear();
            cmbCaseType.Items.Add(new ListItem("ALL", "0"));
            cmbCaseType.Items.Add(new ListItem("ARRAIGNMENT OR BLANK", "2"));

            int idx = 2;

            drStatus = ClsDb.Get_DR_BySP("usp_MID_Get_CaseStatusTypes");
            while (drStatus.Read())
            {
                cmbCaseType.Items.Add(drStatus.GetString(1).ToUpper());
                cmbCaseType.Items[idx].Value = drStatus.GetString(0);
                idx += 1;
            }

            drStatus.Close();
        }


        private void FillPCCR()
        {
            ////Change by Ajmal
            ////SqlDataReader drPCCR;
            //IDataReader drPCCR;
            //int idx=1;

            ////Modified by farhan
            //ddlUpdateStatus.Items.Clear();
            //ddlUpdateStatus.Items.Add(new ListItem("NO ACTION", "1"));
            //cmbPCCR.Items.Clear();
            //cmbPCCR.Items.Add(new ListItem("ALL", "0"));
            //cmbPCCR.Items.Add(new ListItem("NO ACTION", "1"));
            ////End of modification

            //drPCCR = ClsDb.Get_DR_BySP("usp_MID_Get_PCCRStatus");
            ////drPCCR = ClsDb.Get_DR_BySP("USP_HTS_GET_ALL_REMINDERCALL_STATUS");

            //while(drPCCR.Read()) 
            //{
            //    cmbPCCR.Items.Add(drPCCR.GetString(1).ToUpper ());
            //    cmbPCCR.Items[idx+1].Value=drPCCR.GetString(0);
            //    ddlUpdateStatus.Items.Add (drPCCR.GetString(1).ToUpper ());
            //    ddlUpdateStatus.Items[idx].Value= drPCCR.GetString(0);

            //    idx+=1;

            //}
            //drPCCR.Close();

            cmbPCCR.Items.Clear();
            ddlUpdateStatus.Items.Clear();
            DataTable dtPCCRStatuses = ClsDb.Get_DT_BySPArr("usp_MID_Get_PCCRStatus");
            cmbPCCR.Items.Add(new ListItem("ALL", "0"));
            cmbPCCR.Items.Add(new ListItem("ALL NON PENDING", "-2"));
            //ddlUpdateStatus.Items.Add(new ListItem("", "0"));

            foreach (DataRow dr in dtPCCRStatuses.Rows)
            {
                cmbPCCR.Items.Add(new ListItem(dr["StatusDescription"].ToString(), dr["StatusID"].ToString()));
                ddlUpdateStatus.Items.Add(new ListItem(dr["StatusDescription"].ToString(), dr["StatusID"].ToString()));
                if (dr["StatusDescription"].ToString().ToUpper().Trim() == "PENDING")
                {
                    ViewState["PendingStatusID"] = dr["StatusID"].ToString();
                }
            }
            cmbPCCR.Items.Add(new ListItem("NO CONTACT LIST", "-1"));
        }


        private void FillCourts()
        // PROCEDURE FOR FILLING COURT SHORT NAMES IN DROP DOWN LIST FOR SELECTION CRITERIA....
        {
            //Change by Ajmal
            //SqlDataReader drCourts;

            IDataReader drCourts;
            int idx = 1;

            drCourts = ClsDb.Get_DR_BySP("usp_MID_Get_CourtInfo");

            while (drCourts.Read())
            {
                cmbCourtType.Items.Add(drCourts.GetString(1).ToUpper());
                cmbCourtType.Items[idx].Value = drCourts.GetString(0);
                idx += 1;
            }
            drCourts.Close();

        }


        #endregion

        #region Database Interaction Routines

        private DataSet PullData()
        {
            // Procedure for fetching data from the database.......


            // Declaring Variables for inpurt parameters for stored procedures........
            string CourtType;
            string CaseType;
            int CheckCourtDate;
            string ClientType;
            string PCCRType;

            // Setting variables for input parameters for stored procedures.....
            CourtType = cmbCourtType.SelectedValue;
            CaseType = cmbCaseType.SelectedValue;
            ClientType = cmbClientType.SelectedValue;
            PCCRType = cmbPCCR.SelectedValue;

            if (optFutureYes.Checked)
                CheckCourtDate = 1;
            else
                CheckCourtDate = 0;

            try
            {
                // Assigning stored procedure's parameter names in an array......
                string[] key = { "@casedescid", "@sdate", "@edate", "@courtloc", "@futureDate", "@clienttype", "@pccrstatus", "@NoContactFilter" };

                int NoContactFilter = 0;
                if (cmbPCCR.SelectedValue == "-1")
                    NoContactFilter = 1;
                // Assigning stored procedure's parameter values in an array......
                object[] value1 = { CaseType, txtfrom.Text, txtto.Text, CourtType, CheckCourtDate, ClientType, PCCRType, NoContactFilter };

                // Calling wrapper class function for fetching in data set.........
                ds = ClsDb.Get_DS_BySPArr("USP_Mid_Get_ClientLookupReport", key, value1);

                //CALLING PROCEDURE FOR EXTRACTING TICKETS INFO FIELD INTO 3 COLUMNS....


                lblMessage.Text = String.Empty;
                recCount = ds.Tables[0].Rows.Count;
                hf_Rec_count.Value = recCount.ToString();
                Session["intRecordCount"] = recCount.ToString();

                if (recCount > 0)
                {
                    txtRecCount.Text = "1";
                    lblRecCount.Visible = true;
                    lblRecCount.Text = recCount.ToString() + " Record(s)";
                }


                if (recCount < 1)
                {
                    lblMessage.Text = "No record found";
                    txtRecCount.Text = "0";
                    lblRecCount.Visible = false;
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            return ds;
        }

        private void PaymentGrouping()
        {
            try
            {
                string tid = "";
                foreach (DataGridItem ItemX in dgViolPrevious.Items)
                {
                    Label lbl_tid = (Label)ItemX.FindControl("lblTicketId");

                    if (lbl_tid != null)
                    {
                        if (lbl_tid.Text != tid)
                        {

                            tid = lbl_tid.Text;
                        }
                        else
                        {
                            ItemX.Cells[8].Text = " ";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region User Procedures

        private void GenerateSerial()
        {
            // Procedure for writing serial numbers in "S.No." column in data grid.......	


            long sNo = (dgClient.CurrentPageIndex) * (dgClient.PageSize);
            try
            {
                // looping for each row of record.............
                foreach (DataGridItem ItemX in dgClient.Items)
                {
                    sNo += 1;
                    // setting text of hyper link to serial number after bouncing of hyper link...
                    ((LinkButton)(ItemX.FindControl("btnSerial"))).Text = (string)Convert.ChangeType(sNo, typeof(string));

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void FillGrid(bool blnMoveToFirstPage)
        {
            int intLastPageNo = 0;
            int intLastClientNo = 0;

            dgClient.DataSource = mCache.retrieveDataView("dvClient", "", typeof(DataView));

            try
            {
                dgClient.DataBind();

            }
            catch (Exception ex)
            {
                // Tahir Ahmed... Routing to handle the selected page index.

                if (chkAll.Checked == false && dgClient.CurrentPageIndex > dgClient.PageCount - 1)
                {
                    dgClient.CurrentPageIndex = dgClient.PageCount - 1;
                    dgClient.DataBind();
                    dgClient.SelectedIndex = dgClient.Items.Count - 1;
                }
                else
                {
                    lblMessage.Text = ex.ToString();
                    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                }


            }

            // Calling procedure for generating serial numbers in data grid......
            GenerateSerial();
            try
            {
                if (chkAll.Checked == false)
                {
                    intLastPageNo = dgClient.PageCount - 1;
                    intLastClientNo = dgClient.Items.Count - 1;
                }

                if (Session["intRecordCount"] == null || Session["intRecordCount"].ToString().Length == 0)
                {
                    //ozair 4767 09/11/2008 pointing to old asp page instead of new aspx page.
                    Response.Redirect("../frmlogin.aspx", false);
                }

                if (Convert.ToInt32(Session["intRecordCount"]) > 0)
                {
                    if (chkAll.Checked == false)
                    {
                        if (blnUpdated == true)
                        {
                            if (dgClient.CurrentPageIndex != 0 && dgClient.CurrentPageIndex == intLastPageNo)
                            {
                                if (dgClient.SelectedIndex == 0 && dgClient.Items.Count == 1)
                                {
                                    dgClient.CurrentPageIndex -= 1;
                                    dgClient.SelectedIndex = intLastClientNo;
                                }
                                else if (dgClient.SelectedIndex == dgClient.Items.Count)
                                {
                                    dgClient.SelectedIndex = dgClient.SelectedIndex - 1;
                                    //dgClient.SelectedIndex -=1 ;
                                }
                            }


                            txtRecCount.Text = "1";
                        }
                        else
                        {
                            if (blnMoveToFirstPage == true)
                            {
                                dgClient.CurrentPageIndex = 0;
                                dgClient.DataBind();
                                GenerateSerial();
                            }
                            if (blnUpdated == false)
                            {
                                dgClient.SelectedIndex = 0;
                            }
                            txtRecCount.Text = "1";


                        }
                    }
                }
                else
                {
                    txtRecCount.Text = "0";
                }


                GetCurrentDetailInfo();
                EnableDisableButtons();
            }
            catch (Exception Ex)
            {
                lblMessage.Text = Ex.Message;
                clog.ErrorLog(Ex.Message, Ex.Source, Ex.TargetSite.ToString(), Ex.StackTrace);
            }
        }


        private bool DoValidation()
        {
            try
            {
                if (Convert.ToDateTime(txtfrom.Text)< Convert.ToDateTime(txtto.Text))
                {
                    lblMessage.Text = "From Date should be less than To Date.";
                    return false;
                }



                return true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
                return false;
            }
        }


        private void InitializeSearch()
        {
            try
            {
                txtfrom.Text = "";
                txtto.Text = "";
                cmbCaseType.SelectedIndex = 0;
                cmbCourtType.SelectedIndex = 0;
                cmbPCCR.SelectedIndex = 0;
                cmbClientType.SelectedIndex = 0;
                lblCurrPage.Visible = false;
                lblGoto.Visible = false;
                cmbPageNo.Visible = false;
                lblPNo.Visible = false;
                cmbPageNo.ClearSelection();
                lblName.Text = String.Empty;
                lblLanguage.Text = String.Empty;
                lblFee.Text = String.Empty;
                lblContact1.Text = String.Empty;
                lblContact2.Text = String.Empty;
                lblContact3.Text = String.Empty;
                lblContact4.Text = String.Empty;
                optCDLYes.Checked = false;
                optCDLYes.Checked = false;
                txtComments.Text = String.Empty;
                ddlUpdateStatus.SelectedIndex = 0;
                chkNoContactList.Checked = false;
                txtRecCount.Text = "0";
                //tblNewInfoDetail.Style[HtmlTextWriterStyle.Display] = "none";
                SetClientGrid();
                SetOtherGrids();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }


        }


        private void SortDataGrid(string Sortnm)
        {
            try
            {
                // Setting dataview for sorting....
                DataView dv = ((DataView)(mCache.retrieveDataView("dvClient", "", typeof(DataView))));
                dv.Sort = Sortnm + " " + StrAcsDec;

                dgClient.DataSource = dv;
                dgClient.DataBind();
                GenerateSerial();
                SetNavigation();
                GetCurrentDetailInfo();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void SetAcsDesc(string Val)
        {

            try
            {
                //StrExp = Session["StrExp"].ToString();
                //StrAcsDec = Session["StrAcsDec"].ToString();
                StrExp = ViewState["StrExp"].ToString();
                StrAcsDec = ViewState["StrAcsDec"].ToString();

            }
            catch { }
            try
            {
                if (StrExp == Val)
                {
                    if (StrAcsDec == "ASC")
                    {
                        StrAcsDec = "DESC";
                        //Session["StrAcsDec"] = StrAcsDec ;
                        ViewState["StrAcsDec"] = StrAcsDec;
                    }
                    else
                    {
                        StrAcsDec = "ASC";
                        //Session["StrAcsDec"] = StrAcsDec ;
                        ViewState["StrAcsDec"] = StrAcsDec;
                    }
                }
                else
                {
                    StrExp = Val;
                    StrAcsDec = "ASC";
                    //Session["StrExp"] = StrExp ;
                    //Session["StrAcsDec"] = StrAcsDec ;
                    ViewState["StrExp"] = StrExp;
                    ViewState["StrAcsDec"] = StrAcsDec;

                }

                // Procedure for checking current sorting order (ASC or DESC) from the session.......			

                //try
                //{
                //    if (Session["StrExp"] == null || Session["StrAcsDec"] == null || Session["StrAcsDec"].ToString().Length == 0 || Session["StrExp"].ToString().Length == 0 )
                //    {
                //        Response.Redirect("../frmlogin.aspx",false);
                //    }

                //    StrExp = Session["StrExp"].ToString();
                //    StrAcsDec = Session["StrAcsDec"].ToString();

                //if (StrExp == Val)
                //{
                //    if (StrAcsDec == "ASC")
                //    {
                //        StrAcsDec = "DESC";
                //        Session["StrAcsDec"] = StrAcsDec ;
                //    }
                //    else 
                //    {
                //        StrAcsDec = "ASC";
                //        Session["StrAcsDec"] = StrAcsDec ;
                //    }
                //}
                //else 
                //{
                //    StrExp = Val;
                //    StrAcsDec = "ASC";
                //    Session["StrExp"] = StrExp ;
                //    Session["StrAcsDec"] = StrAcsDec ;				
                //}
                //}
                //catch
                //{
                //    if (this.Session.Count == 0 )
                //        Response.Redirect("../frmlogin.aspx",false);
                //}

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void FillPageList()
        {
            try
            {
                // Procedure for filling page numbers in page number combo........
                Int16 idx;

                if (dgClient.Items.Count == 0)
                {
                    cmbPageNo.Items.Clear();
                    cmbPageNo.Items.Add("--");
                    cmbPageNo.Items[0].Value = "--";
                    lblPNo.Text = String.Empty;
                    return;
                }

                cmbPageNo.Items.Clear();
                for (idx = 1; idx <= dgClient.PageCount; idx++)
                {
                    cmbPageNo.Items.Add("Page - " + idx.ToString());
                    cmbPageNo.Items[idx - 1].Value = idx.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void SetNavigation()
        {
            // Procedure for setting data grid's current  page number on header ........

            try
            {
                if (dgClient.Items.Count == 0)
                {
                    lblCurrPage.Visible = false;
                    lblPNo.Visible = false;
                    lblGoto.Visible = false;
                    cmbPageNo.Visible = false;
                }
                else
                {

                    // setting current page number
                    lblPNo.Text = Convert.ToString(dgClient.CurrentPageIndex + 1);
                    lblCurrPage.Visible = true;
                    lblPNo.Visible = true;

                    // filling combo with page numbers
                    lblGoto.Visible = true;
                    cmbPageNo.Visible = true;
                    //FillPageList();

                    for (int idx = 0; idx < cmbPageNo.Items.Count; idx++)
                    {
                        cmbPageNo.Items[idx].Selected = false;
                    }
                    if (dgClient.CurrentPageIndex != -1)
                    {
                        if (cmbPageNo.Items.Count > dgClient.CurrentPageIndex)
                        {
                            cmbPageNo.Items[dgClient.CurrentPageIndex].Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void GetCurrentDetailInfo()
        {
            try
            {
                string strMidNum = String.Empty;

                if (dgClient.Items.Count == 0)
                {
                    SetClientGrid();
                    SetOtherGrids();

                    return;

                }

                // GETTING MIDNUMBER FROM CLIENT LIST.......

                if (dgClient.SelectedIndex != -1)
                {
                    strMidNum = dgClient.Items[dgClient.SelectedIndex].Cells[4].Text;
                    GetDetailInfo(strMidNum);
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void SetClientGrid()
        {
            try
            {
                DataSet ds4 = new DataSet();

                ds4.Tables.Add();
                ds4.Tables[0].Columns.Add("S. No");
                ds4.Tables[0].Columns.Add("Client Name");
                ds4.Tables[0].Columns.Add("Language");
                ds4.Tables[0].Columns.Add("list date");
                ds4.Tables[0].Columns.Add("ticket number");
                ds4.Tables[0].Columns.Add("violation status");
                ds4.Tables[0].Columns.Add("court date");
                ds4.Tables[0].Columns.Add("court name");
                ds4.Tables[0].Columns.Add("pccr status");
                ds4.Tables[0].Columns.Add("ticketid");
                ds4.Tables[0].Columns.Add("recordid");
                ds4.Tables[0].Columns.Add("courttype");
                ds4.Tables[0].Columns.Add("ticketinfo");
                dgClient.DataSource = ds4;
                dgClient.CurrentPageIndex = 0;
                dgClient.DataBind();
                txtRecCount.Text = "0";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void SetOtherGrids()
        {
            try
            {

                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();

                ds1.Tables.Add();
                ds1.Tables[0].Columns.Add("Case Number");
                ds1.Tables[0].Columns.Add("Violation Description");
                ds1.Tables[0].Columns.Add("Fine Amount");
                ds1.Tables[0].Columns.Add("Viol Status");
                ds1.Tables[0].Columns.Add("Court Date");
                ds1.Tables[0].Columns.Add("Court Name");
                ds1.Tables[0].Columns.Add("Recordid");
                ds1.Tables[0].Columns.Add("ticketnumber");

                dgViolNew.DataSource = ds1;
                dgViolNew.DataBind();

                ds2.Tables.Add();
                ds2.Tables[0].Columns.Add("Case Number");
                ds2.Tables[0].Columns.Add("Violation Description");
                ds2.Tables[0].Columns.Add("Case Status");
                ds2.Tables[0].Columns.Add("Court Date");
                ds2.Tables[0].Columns.Add("Court Name");
                ds2.Tables[0].Columns.Add("ticketid");

                dgViolPrevious.DataSource = ds2;
                dgViolPrevious.DataBind();
                PaymentGrouping();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void GetDetailInfo(string MIDNo)
        // PROCEDURE FOR GETTING NEW & PREVIOUS TICKETS INFORMATION RELATED TO SELECTED ROW IN CLIENT LIST.....
        {

            string ListDate = String.Empty;
            ListDate = ((Label)(dgClient.Items[dgClient.SelectedIndex].FindControl("label6"))).Text;
            HiddenField hf_ticnum = (HiddenField)dgClient.Items[dgClient.SelectedIndex].FindControl("hf_TicketNumber");

            try
            {

                // Assigning stored procedure's parameter names in an array......
                //string[] key    = {"@MIDNum","@listdate"};

                string[] key = { "@midnumber", "@TicketNumber" };

                // Assigning stored procedure's parameter values in an array......
                //object[] value1 = {MIDNo, ListDate};
                object[] value1 = { MIDNo, hf_ticnum.Value };

                //dsNewInfo =  ClsDb.Get_DS_BySPArr("usp_MID_Get_NewTicketsInfo", key, value1);
                dsNewInfo = ClsDb.Get_DS_BySPArr("usp_MID_Get_NewTicketsInfo_New", key, value1);
                dgViolNew.DataSource = dsNewInfo.Tables[0];

                int p_exists = Convert.ToInt32(dsNewInfo.Tables[1].Rows[0]["TicketID"]);
                if (p_exists == 0)
                    hf_ProfileExists.Value = "0";
                else
                    hf_ProfileExists.Value = "1";

                dgViolNew.DataBind();

                mCache.storeDataset("dsNewInfo", "", dav, dsNewInfo);



                // Calling procedure to obtain comments previously entered for these tickets.....
                if (!blnClientMoved)
                    GetCommentsInfo();

                lblFee.Text = "$" + (string)(Convert.ChangeType(dgViolNew.Items.Count * 50, typeof(string)));

                // Assigning stored procedure's parameter names in an array......
                string[] key2 = { "@MIDNum" };

                // Assigning stored procedure's parameter values in an array......
                object[] value2 = { MIDNo };

                dsPrevInfo = ClsDb.Get_DS_BySPArr("usp_MID_Get_PrevTicketsInfo", key2, value2);
                dgViolPrevious.DataSource = dsPrevInfo;
                dgViolPrevious.DataBind();
                PaymentGrouping();
                mCache.storeDataset("dsPrevInfo", "", dav, dsPrevInfo);

                ClearPhoneNumbers();
                GetPhoneNumbers();
                SetHyperLinks();


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + " - " + ex.Source;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void GetDetailInfo(string MIDNo, string TicketNumber)
        // PROCEDURE FOR GETTING NEW & PREVIOUS TICKETS INFORMATION RELATED TO SELECTED ROW IN CLIENT LIST.....
        {

            string ListDate = String.Empty;
            ListDate = ((Label)(dgClient.Items[dgClient.SelectedIndex].FindControl("label6"))).Text;
            HiddenField hf_ticnum = (HiddenField)dgClient.Items[dgClient.SelectedIndex].FindControl("hf_TicketNumber");

            try
            {

                // Assigning stored procedure's parameter names in an array......
                //string[] key    = {"@MIDNum","@listdate"};

                string[] key = { "@midnumber", "@TicketNumber" };

                // Assigning stored procedure's parameter values in an array......
                //object[] value1 = {MIDNo, ListDate};
                object[] value1 = { MIDNo, TicketNumber };

                //dsNewInfo =  ClsDb.Get_DS_BySPArr("usp_MID_Get_NewTicketsInfo", key, value1);
                dsNewInfo = ClsDb.Get_DS_BySPArr("usp_MID_Get_NewTicketsInfo_New", key, value1);
                dgViolNew.DataSource = dsNewInfo.Tables[0];

                int p_exists = Convert.ToInt32(dsNewInfo.Tables[1].Rows[0]["TicketID"]);
                if (p_exists == 0)
                    hf_ProfileExists.Value = "0";
                else
                    hf_ProfileExists.Value = "1";

                dgViolNew.DataBind();

                mCache.storeDataset("dsNewInfo", "", dav, dsNewInfo);



                // Calling procedure to obtain comments previously entered for these tickets.....
                if (!blnClientMoved)
                    GetCommentsInfo();

                lblFee.Text = "$" + (string)(Convert.ChangeType(dgViolNew.Items.Count * 50, typeof(string)));

                // Assigning stored procedure's parameter names in an array......
                string[] key2 = { "@MIDNum" };

                // Assigning stored procedure's parameter values in an array......
                object[] value2 = { MIDNo };

                dsPrevInfo = ClsDb.Get_DS_BySPArr("usp_MID_Get_PrevTicketsInfo", key2, value2);
                dgViolPrevious.DataSource = dsPrevInfo;
                dgViolPrevious.DataBind();
                PaymentGrouping();
                mCache.storeDataset("dsPrevInfo", "", dav, dsPrevInfo);

                ClearPhoneNumbers();
                GetPhoneNumbers();
                SetHyperLinks();


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + " - " + ex.Source;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        private void GetPhoneNumbers()
        {
            try
            {
                //Change by Ajmal
                //SqlDataReader drPh;

                if (hf_ProfileExists.Value == "1")
                    return;

                IDataReader drPh;
                string strTicketIds = String.Empty;
                string strRecordIds = String.Empty;

                foreach (DataGridItem Itemx in dgViolNew.Items)
                {
                    strRecordIds += ((Label)(Itemx.FindControl("Label1"))).Text + ",";
                }

                foreach (DataGridItem Itemx in dgViolPrevious.Items)
                {
                    strTicketIds += ((Label)(Itemx.FindControl("lblTicketId"))).Text + ",";
                }

                string[] ClKey = { "@TicketIDs", "@RecordIds" };
                object[] ClValue = { strTicketIds, strRecordIds };

                drPh = ClsDb.Get_DR_BySPArr("USP_HTS_GET_MID_Phonenumbers", ClKey, ClValue);

                if (drPh.Read())
                    lblContact1.Text = drPh.GetString(0).ToUpper();

                if (drPh.Read())
                    lblContact2.Text = drPh.GetString(0).ToUpper();

                if (drPh.Read())
                    lblContact3.Text = drPh.GetString(0).ToUpper();

                if (drPh.Read())
                    lblContact4.Text = drPh.GetString(0).ToUpper();

                if (drPh.Read())
                    lblContact5.Text = drPh.GetString(0).ToUpper();

                if (drPh.Read())
                    lblContact6.Text = drPh.GetString(0).ToUpper();
                drPh.Close();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void ClearPhoneNumbers()
        {
            lblContact1.Text = String.Empty;
            lblContact2.Text = String.Empty;
            lblContact3.Text = String.Empty;
            lblContact4.Text = String.Empty;
            lblContact5.Text = String.Empty;
            lblContact6.Text = String.Empty;
        }


        private void GetCommentsInfo()
        {
            try
            {
                string strRecIDS = String.Empty;

                foreach (DataGridItem ItemX in dgViolNew.Items)
                {
                    strRecIDS += ((Label)(ItemX.FindControl("Label1"))).Text + ",";
                }
                // Assigning stored procedure's parameter names in an array......
                string[] key2 = { "@RecIDs" };

                // Assigning stored procedure's parameter values in an array......
                object[] value2 = { strRecIDS };

                drComments = ClsDb.Get_DR_BySPArr("usp_MID_Get_CommentsInfo", key2, value2);


                // pccr status....

                // clearing previous selection.......
                for (int idx = 0; idx < ddlUpdateStatus.Items.Count; idx++)
                    ddlUpdateStatus.Items[idx].Selected = false;

                // setting the value as per fetched from database......
                string strStatusValue = null;
                bool hasData = false;
                hasData = drComments.Read();

                if (hasData == false)
                {
                    txtComments.Text = "";
                    return;
                }

                //strStatusValue = (string)(Convert.ChangeType(drComments.GetInt32(2), typeof(string)));
                strStatusValue = (string)(Convert.ChangeType(drComments["pccrstatus"], typeof(string)));
                if (strStatusValue == "0")
                    hf_UpdateFlag.Value = "1";
                else
                    hf_UpdateFlag.Value = "0";

                for (int idx = 0; idx < ddlUpdateStatus.Items.Count; idx++)
                {
                    if (ddlUpdateStatus.Items[idx].Value == strStatusValue)
                    {
                        ddlUpdateStatus.Items[idx].Selected = true;
                        break;
                    }
                }
                txtStatus.Text = ddlUpdateStatus.Items[ddlUpdateStatus.SelectedIndex].Text;

                // client name....
                if (dgClient.Items.Count == 0)
                {
                    hf_Hide_flag.Value = "1";
                    return;
                }
                if (dgClient.Items[dgClient.SelectedIndex].Cells[1].Text.Length > 20)
                {
                    lblName.Text = dgClient.Items[dgClient.SelectedIndex].Cells[1].Text.Substring(0, 17) + "..";
                    lblName.ToolTip = dgClient.Items[dgClient.SelectedIndex].Cells[1].Text;
                }
                else
                {
                    lblName.Text = dgClient.Items[dgClient.SelectedIndex].Cells[1].Text;
                    lblName.ToolTip = String.Empty;
                }

                //language.....
                lblLanguage.Text = dgClient.Items[dgClient.SelectedIndex].Cells[2].Text;


                // cdl...
                //if(drComments.GetInt32(13)== 1)
                if (Convert.ToInt32(drComments["isCDL"]) == 1)
                {
                    optCDLYes.Checked = true;
                    optCDLNo.Checked = false;
                }
                //else if(drComments.GetInt32(13)== 0)
                else if (Convert.ToInt32(drComments["isCDL"]) == 0)
                {
                    optCDLNo.Checked = true;
                    optCDLYes.Checked = false;
                }
                else
                {
                    optCDLNo.Checked = false;
                    optCDLYes.Checked = false;
                }

                // accident......			
                //if(drComments.GetInt32(12)== 1)
                if (Convert.ToInt32(drComments["isAccident"]) == 1)
                {
                    optAccYes.Checked = true;
                    optAccNo.Checked = false;
                }
                else if (Convert.ToInt32(drComments["isAccident"]) == 0)
                {
                    optAccNo.Checked = true;
                    optAccYes.Checked = false;
                }
                else
                {
                    optAccNo.Checked = false;
                    optAccYes.Checked = false;
                }

                // comments.....
                //if (drComments.GetString(3).Length != 0)
                if (drComments["comments"].ToString().Length != 0)
                {
                    txtComments.Text = drComments["comments"].ToString();//.GetString(3);

                }
                else
                {
                    txtComments.Text = String.Empty;
                }


                /*
                //if(drComments.GetBoolean(4)== true)
                if (Convert.ToBoolean(drComments["PriceShopping"]) == true)
                    chkPriceShopping.Checked = true;
                else
                    chkPriceShopping.Checked=false;

                //if(drComments.GetBoolean(5)== true)
                if (Convert.ToBoolean(drComments["HiredAnother"]) == true)
                    chkHiredAnother.Checked=true;
                else
                    chkHiredAnother.Checked=false;

                //if(drComments.GetBoolean(6)== true)
                if (Convert.ToBoolean(drComments["HandleMySelf"]) == true)
                    chkHandleMySelf.Checked=true;
                else
                    chkHandleMySelf.Checked=false;

                //if(drComments.GetBoolean(7)== true)
                if (Convert.ToBoolean(drComments["PaidTickets"]) == true)
                    chkPaidTickets.Checked=true;
                else
                    chkPaidTickets.Checked=false;

                //if(drComments.GetBoolean(8)== true)
                if (Convert.ToBoolean(drComments["DSCProb"]) == true)
                    chkDSCProb.Checked = true;
                else
                    chkDSCProb.Checked=false;

                //if(drComments.GetBoolean(9)== true)
                if (Convert.ToBoolean(drComments["NoMoney"]) == true)
                    chkNoMoney.Checked = true;
                else
                    chkNoMoney.Checked=false;

                //if(drComments.GetBoolean(10)== true)
                if (Convert.ToBoolean(drComments["TooExpensive"]) == true)
                    chkTooExpensive.Checked = true;
                else
                    chkTooExpensive.Checked=false;

                //if(drComments.GetBoolean(11)== true)
                if(Convert.ToBoolean(drComments["PoorService"])==true)
                    chkPoorService.Checked = true;
                else
                    chkPoorService.Checked=false;*/
            }
            catch (Exception ex)
            {
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
            finally
            {
                drComments.Close();
            }

        }


        private void AddRestrictedPhoneNumbers()
        {
            try
            {
                string[] key1 = { "" };
                object[] value1 = { "" };
                string strtemp;
                Int16 intIndex;

                if (lblContact1.Text.Length != 0)
                {
                    strtemp = lblContact1.Text;
                    intIndex = (Int16)Convert.ChangeType(strtemp.IndexOf("("), typeof(Int16));

                    if (intIndex != -1)
                    {
                        strtemp = strtemp.Substring(0, intIndex);
                    }

                    key1[0] = "@PhoneNo";
                    value1[0] = strtemp;
                    ClsDb.InsertBySPArr("usp_MID_Insert_RestrictedPhoneNumbers", key1, value1);

                }

                if (lblContact2.Text.Length != 0)
                {
                    strtemp = lblContact2.Text;
                    intIndex = (Int16)Convert.ChangeType(strtemp.IndexOf("("), typeof(Int16));

                    if (intIndex != -1)
                    {
                        strtemp = strtemp.Substring(0, intIndex);
                    }

                    key1[0] = "@PhoneNo";
                    value1[0] = strtemp;
                    ClsDb.InsertBySPArr("usp_MID_Insert_RestrictedPhoneNumbers", key1, value1);

                }

                if (lblContact3.Text.Length != 0)
                {
                    strtemp = lblContact3.Text;
                    intIndex = (Int16)Convert.ChangeType(strtemp.IndexOf("("), typeof(Int16));

                    if (intIndex != -1)
                    {
                        strtemp = strtemp.Substring(0, intIndex);
                    }

                    key1[0] = "@PhoneNo";
                    value1[0] = strtemp;
                    ClsDb.InsertBySPArr("usp_MID_Insert_RestrictedPhoneNumbers", key1, value1);

                }

                if (lblContact4.Text.Length != 0)
                {
                    strtemp = lblContact4.Text;
                    intIndex = (Int16)Convert.ChangeType(strtemp.IndexOf("("), typeof(Int16));

                    if (intIndex != -1)
                    {
                        strtemp = strtemp.Substring(0, intIndex);
                    }

                    key1[0] = "@PhoneNo";
                    value1[0] = strtemp;
                    ClsDb.InsertBySPArr("usp_MID_Insert_RestrictedPhoneNumbers", key1, value1);

                }

                if (lblContact5.Text.Length != 0)
                {
                    strtemp = lblContact5.Text;
                    intIndex = (Int16)Convert.ChangeType(strtemp.IndexOf("("), typeof(Int16));

                    if (intIndex != -1)
                    {
                        strtemp = strtemp.Substring(0, intIndex);
                    }

                    key1[0] = "@PhoneNo";
                    value1[0] = strtemp;
                    ClsDb.InsertBySPArr("usp_MID_Insert_RestrictedPhoneNumbers", key1, value1);

                }

                if (lblContact6.Text.Length != 0)
                {
                    strtemp = lblContact6.Text;
                    intIndex = (Int16)Convert.ChangeType(strtemp.IndexOf("("), typeof(Int16));

                    if (intIndex != -1)
                    {
                        strtemp = strtemp.Substring(0, intIndex);
                    }

                    key1[0] = "@PhoneNo";
                    value1[0] = strtemp;
                    ClsDb.InsertBySPArr("usp_MID_Insert_RestrictedPhoneNumbers", key1, value1);

                }

                RemoveClientFromCache();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void RemoveClientFromCache()
        {

            try
            {
                int idxRowToRemove = ((int)(Convert.ChangeType(((LinkButton)(dgClient.Items[dgClient.SelectedIndex].FindControl("btnSerial"))).Text, typeof(int)))) - 1;

                DataView dv = mCache.retrieveDataView("dvClient", "", typeof(DataView));
                DataSet ds = new DataSet();
                ds.Tables.Add(dv.Table.Copy());
                DataRow myRow = ds.Tables[0].Rows[idxRowToRemove];
                ds.Tables[0].Rows.Remove(myRow);
                ds.AcceptChanges();
                if (ds.Tables[0].Rows.Count == 0)
                    hf_Hide_flag.Value = "1";
                dv = ds.Tables[0].DefaultView;
                mCache.storeDataView("dvClient", "", dav, dv);
                FillGrid(false);
                SetNavigation();
                lblRecCount.Text = ds.Tables[0].Rows.Count.ToString() + " Record(s)";
                dv.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }


        private void ClearComments()
        {
            chkNoContactList.Checked = false;
        }


        private void SaveComments()
        {

            string strRecordIds = String.Empty;
            int CDLFlag;
            bool AccFlag = false;
            bool PriceShopping = false;
            bool HiredAnother = false;
            bool HandleMySelf = false;
            bool PaidTicket = false;
            bool DSCProb = false;
            bool NoMoney = false;
            bool TooExpensive = false;
            bool PoorService = false;
            int isStatusChanged;
            string LangaugeSpeak = lblLanguage.Text;


            if (chkPriceShopping.Checked)
                PriceShopping = true;
            else
                PriceShopping = false;

            if (chkHiredAnother.Checked)
                HiredAnother = true;
            else
                HiredAnother = false;

            if (chkHandleMySelf.Checked)
                HandleMySelf = true;
            else
                HandleMySelf = false;

            if (chkPaidTickets.Checked)
                PaidTicket = true;
            else
                PaidTicket = false;

            if (chkDSCProb.Checked)
                DSCProb = true;
            else
                DSCProb = false;

            if (chkNoMoney.Checked)
                NoMoney = true;
            else
                NoMoney = false;

            if (chkTooExpensive.Checked)
                TooExpensive = true;
            else
                TooExpensive = false;

            if (chkPoorService.Checked)
                PoorService = true;
            else
                PoorService = false;


            if (optCDLYes.Checked)
                CDLFlag = 1;
            else if (optCDLNo.Checked)
                CDLFlag = 0;
            else
                CDLFlag = 3;

            if (optAccYes.Checked)
                AccFlag = true;
            else if (optAccNo.Checked)
                AccFlag = false;

            if (hf_UpdateFlag.Value == "1")
            {
                isStatusChanged = 1;
            }
            else
            {
                if (ddlUpdateStatus.Items[ddlUpdateStatus.SelectedIndex].Text.Equals(txtStatus.Text))
                    isStatusChanged = 0;
                else
                    isStatusChanged = 1;
            }


            try
            {
                foreach (DataGridItem ItemX in dgViolNew.Items)
                {
                    strRecordIds += ((Label)(ItemX.FindControl("Label1"))).Text + ",";
                }

                // tahir ahmed.... this line to be comment before upload...					
                //Response.Cookies["Employeeid"].Value="3992";

                string[] key1 = { "@RecordIds", "@PCCRStatus", "@Comments", "@Employeeid", "@IsCDL", "@PriceShopping", "@HiredAnother", "@HandleMySelf", "@PaidTicket", "@DSCProb", "@NoMoney", "@TooExpensive", "@PoorService", "@isAccident", "@isStatusChanged", "@IsSignUp", "@NoContactFlag", "@LanguageSpeak" };
                object[] value1 = { strRecordIds, ddlUpdateStatus.SelectedValue, txtComments.Text.Trim(), Request.Cookies["sEmpID"].Value, CDLFlag, PriceShopping, HiredAnother, HandleMySelf, PaidTicket, DSCProb, NoMoney, TooExpensive, PoorService, AccFlag, isStatusChanged, isSignUp, Convert.ToInt16(chkNoContactList.Checked), LangaugeSpeak };
                ClsDb.InsertBySPArr("usp_MID_Insert_TicketPCCRStatus", key1, value1);
                hf_UpdateFlag.Value = "0";
                if (chkNoContactList.Checked == true)
                {
                    AddRestrictedPhoneNumbers();
                    hf_Refresh.Value = "1";
                }
                else
                    hf_Refresh.Value = "0";

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        private void GotoPage(int intNewIndex)
        {
            try
            {
                // Procedure for handling page navigation in data grid......

                if (chkAll.Checked == false)
                {
                    // setting current page to new page index in data grid....
                    dgClient.CurrentPageIndex = intNewIndex;

                    // Filling & binding data for new page index with data grid......
                    FillGrid(false);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        protected void EnableDisableButtons()
        {
            try
            {
                if (dgClient.CurrentPageIndex == 0 && dgClient.SelectedIndex == 0)
                    btnPrevClient.Enabled = false;
                else
                    btnPrevClient.Enabled = true;

                if (dgClient.CurrentPageIndex == dgClient.PageCount - 1 && dgClient.SelectedIndex == dgClient.Items.Count - 1)
                    btnNextClient.Enabled = false;
                else
                    btnNextClient.Enabled = true;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }


        #endregion

    }  // End of Class

}	// End of Namespace
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jurytrial.aspx.cs" Inherits="HTP.Activities.jurytrial" %>

<%@ Register TagName="ActiveMenu" TagPrefix="uc3" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuTop" Src="../WebControls/MenuTop.ascx" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>

<%@ Register Src="../WebControls/Footer.ascx" TagName="Footer" TagPrefix="uc2" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attorney Trials</title>
    <script src="../Scripts/validatecreditcard.js" type="text/javascript"></script>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
    <script language="javascript" type="text/javascript">

        function ValidateForm() {


            var input;
            input = document.getElementById("ddl_attorney").selectedIndex;
            if (input == 0) {
                //alert("Please Select Attorney");
                $("#txtErrorMessage").text("Please Select Attorney");
                $("#errorAlert").modal();
                document.getElementById("ddl_attorney").focus();
                return false;
            }
            input = document.getElementById("txt_prosecutor").value;
            if (input.length == 0) {
                //alert("Please Enter Prosecutor");
                $("#txtErrorMessage").text("Please Enter Prosecutor");
                $("#errorAlert").modal();
                document.getElementById("txt_prosecutor").focus();
                return false;
            }
            input = document.getElementById("txt_officer").value;
            if (input.length == 0) {
                // alert("Please Enter Officer Name");
                $("#txtErrorMessage").text("Please Enter Officer Name");
                $("#errorAlert").modal();
                document.getElementById("txt_officer").focus();
                return false;
            }
            input = document.getElementById("txt_defendant").value;
            if (input.length == 0) {
                // alert("Please Enter Defendant");
                $("#txtErrorMessage").text("Please Enter Defendant");
                $("#errorAlert").modal();
                document.getElementById("txt_defendant").focus();
                return false;
            }
            input = document.getElementById("txt_caseno").value;
            if (input.length == 0) {
                //alert("Please Enter Case No");
                $("#txtErrorMessage").text("Please Enter Case No");
                $("#errorAlert").modal();
                document.getElementById("txt_caseno").focus();
                return false;
            }
            input = document.getElementById("txt_ticketno").value;
            if (input.length == 0) {
                //alert("Please Enter Ticket No");
                $("#txtErrorMessage").text("Please Enter Ticket No");
                $("#errorAlert").modal();
                document.getElementById("txt_ticketno").focus();
                return false;
            }
            input = document.getElementById("ddl_court").selectedIndex;
            if (input == 0) {
                //alert("Please select Court");
                $("#txtErrorMessage").text("Please select Court");
                $("#errorAlert").modal();
                document.getElementById("ddl_court").focus();
                return false;
            }
            input = document.getElementById("txt_judge").value;
            if (input.length == 0) {
                //alert("Please Enter Judge Name");
                $("#txtErrorMessage").text("Please Enter Judge Name");
                $("#errorAlert").modal();
                document.getElementById("txt_judge").focus();
                return false;
            }
            input = document.getElementById("txt_brieffacts").value;
            if (input.length == 0) {
                //alert("Please Enter Brief Facts of the Case");
                $("#txtErrorMessage").text("Please Enter Brief Facts of the Case");
                $("#errorAlert").modal();
                document.getElementById("txt_brieffacts").focus();
                return false;
            }
            input = document.getElementById("ddl_verdict").selectedIndex;
            if (input == 0) {
                //alert("Please select Verdict");
                $("#txtErrorMessage").text("Please select Verdict");
                $("#errorAlert").modal();
                document.getElementById("ddl_verdict").focus();
                return false;
            }

            input = document.getElementById("txt_fine").value;
            if (input.length == 0) {
                //alert("Please Enter Fine Amount");
                $("#txtErrorMessage").text("Please Enter Fine Amount");
                $("#errorAlert").modal();
                document.getElementById("txt_fine").focus();
                return false;
            }
            ///Checking Valid amount    
            if (isDecimalNum(document.getElementById("txt_fine").value) != true) {
                //alert("Sorry, This is not Valid amount");
                $("#txtErrorMessage").text("Sorry, This is not Valid amount");
                $("#errorAlert").modal();
                document.getElementById("txt_fine").focus();
                return false;
            }

            input = document.getElementById("txt_creporter").value;
            if (input.length == 0) {
                //alert("Please Enter Court Reporter");
                $("#txtErrorMessage").text("Please Enter Court Reporter");
                $("#errorAlert").modal();
                document.getElementById("txt_creporter").focus();
                return false;
            }

            //Yasir Kamal 6279 08/05/2009 javascript bug fixed.
            //   var dt =document.form1.calQueryFrom.value;
            //    var dtarray =new Array(3);
            //    dtarray =dt.split("/");   
            //    
            //   if(isValidDate(dtarray[2],dtarray[0],dtarray[1]) == false)
            //    {
            //     alert("Sorry, This is not a Valid Date");
            //     return false;
            //     }

            var con = confirm("Are you sure you want to save this record (OK = Yes Cancel = No)? ");
            if (con == false)
                return false;

            plzwait();
            //   ClearControls();
        }

        function ClearControls() {
            document.getElementById("ddl_attorney").selectedIndex = 0;
            document.getElementById("txt_prosecutor").value = "";
            document.getElementById("txt_officer").value = "";
            document.getElementById("txt_defendant").value = "";
            document.getElementById("txt_caseno").value = "";
            document.getElementById("txt_ticketno").value = "";
            document.getElementById("ddl_court").selectedIndex = 0;
            document.getElementById("txt_judge").value = "";
            document.getElementById("txt_brieffacts").value = "";
            document.getElementById("ddl_verdict").selectedIndex = 0;
            document.getElementById("txt_fine").value = "";
            document.getElementById("txt_creporter").value = "";
            document.getElementById("lblMessage").innerText = "";

            document.getElementById("btn_submit").value = "Submit";
            document.getElementById("hf_rowid").value = "";
            document.getElementById("hf_status").value = "New";
            return false;
        }

        function plzwait() {

            document.getElementById("tbl_plzwait").style.display = 'block';
            document.getElementById("allcreditcard").style.display = 'none';

        }


    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/Validationfx.js" type="text/javascript">function Table1_onclick() {

}

    </script>


    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">

        <aspnew:ScriptManager ID="ScriptManager1" runat="server" />

        <div class="page-container row-fluid container-fluid">
            <asp:Panel ID="pnl" runat="server">
                <uc3:ActiveMenu ID="ActiveMenu1" runat="server"></uc3:ActiveMenu>
            </asp:Panel>
            <section id="main-content" class="">
                <section class="wrapper main-wrapper row" id="" style="">

                    <div class="col-xs-12">

                        <div class="alert alert-danger alert-dismissable fade in" style="display: none">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Width="675px"></asp:Label>
                        </div>



                    </div>

                    <div class="col-xs-12">
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Atty-Trials</h1>
                                <!-- PAGE HEADING TAG - END -->

                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>

                    <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <section class="box" id="allcreditcard" style="">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Atty-Trials</h2>
                                    <div class="actions panel_actions pull-right">

                                        <a class="box_toggle fa fa-chevron-down"></a>

                                    </div>
                                </header>

                                <div class="content-body">
                                    <div class="col-md-12">
                                        <div id="LblSucessdiv" visible="false" runat="server" class="alert alert-success alert-dismissable fade in">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <asp:Label runat="server" ID="LblSucesstext"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Date</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <picker:datepicker ID="calQueryFrom" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                                    <%--<ew:CalendarPopup ID="calQueryFrom" runat="server" AllowArbitraryText="False" CssClass="form-control"
                                                CalendarLocation="Bottom" ControlDisplay="TextBoxImage" Culture="(Default)" EnableHideDropDown="True"
                                                Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif" PadSingleDigits="True"
                                                ShowGoToToday="True" ToolTip="Select end date" UpperBoundDate="12/31/9999 23:59:00"
                                                Width="80px">
                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                    ForeColor="Black" />
                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                    Font-Size="XX-Small" ForeColor="Black" />
                                            </ew:CalendarPopup>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Attorney</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddl_attorney" runat="server" CssClass="form-control" Width="">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Prosecutor</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:TextBox ID="txt_prosecutor" runat="server" CssClass="form-control" Width="" MaxLength="50"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Officer</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:TextBox ID="txt_officer" runat="server" CssClass="form-control" Width="" MaxLength="50"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Defendant</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:TextBox ID="txt_defendant" runat="server" CssClass="form-control" Width="" MaxLength="50"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Case No</label>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:TextBox ID="txt_caseno" runat="server" CssClass="form-control" Width="" MaxLength="25"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Ticket No</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:TextBox ID="txt_ticketno" runat="server" CssClass="form-control" Width="" MaxLength="25"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Court</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddl_court" runat="server" CssClass="form-control" Width="">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Judge</label>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:TextBox ID="txt_judge" runat="server" CssClass="form-control" Width="" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12" style="display: none">
                                            <div class="form-group">
                                                <label class="form-label">Card Type</label>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:DropDownList ID="ddl_cardtype" runat="server" CssClass="form-control" Width="">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label">Brief Facts</label>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:TextBox ID="txt_brieffacts" runat="server" CssClass="form-control"
                                                        Height="200px" MaxLength="2000" TextMode="MultiLine"
                                                        Width="650px"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Verdict</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddl_verdict" runat="server" CssClass="form-control" Width="">
                                                        <asp:ListItem Selected="True" Value="0">--------- Select ---------</asp:ListItem>
                                                        <asp:ListItem Value="1">Guilty</asp:ListItem>
                                                        <asp:ListItem Value="2">Not Guilty</asp:ListItem>
                                                        <asp:ListItem Value="3">Hung Jury - Case Dismissed</asp:ListItem>
                                                        <asp:ListItem Value="4">Hung Jury - Case Reset</asp:ListItem>
                                                        <asp:ListItem Value="5">Mistrial</asp:ListItem>
                                                        <asp:ListItem Value="6">Other</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Fine ($)</label>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:TextBox ID="txt_fine" runat="server" CssClass="form-control" Width="" MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label">Court Reporter</label>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:TextBox ID="txt_creporter" runat="server" CssClass="form-control" Width="" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <%--<label class="form-label">Court Reporter</label>--%>
                                                <span class="desc"></span>
                                                <div class="controls">

                                                    <asp:Button ID="btn_submit" runat="server" CssClass="btn btn-primary" Text="Submit" Width="" OnClick="btn_submit_Click" />
                                                    <asp:Button ID="btn_reset" runat="server" CssClass="btn btn-primary" EnableViewState="False"
                                                        Text="Reset" Width="" CausesValidation="False" OnClick="btn_reset_Click" />

                                                    <asp:HiddenField ID="hf_rowid" runat="server" />
                                                    <asp:HiddenField ID="hf_status" runat="server" Value="New" />

                                                </div>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                            </section>
                            <table id="tbl_plzwait" class="clssubhead" style="display: none; height: 60px" width="100%">
                                <tr>
                                    <td align="center" class="clssubhead" valign="middle">
                                        <img src="../Images/plzwait.gif" />
                                        &nbsp; Please wait while we process your request.
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </aspnew:UpdatePanel>

                    <%--   <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <uc2:Footer ID="Footer1" runat="server" />
                                        
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>--%>
                </section>
            </section>
        </div>


    </form>
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

    <div id="errorAlert" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body" style="min-height: 93px !important; max-height: 162px;">
                    <p id="txtErrorMessage">Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>

</body>
</html>

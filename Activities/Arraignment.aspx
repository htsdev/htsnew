﻿<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.Arraignment" Codebehind="Arraignment.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Arraignment</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=7.5;" />
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <script src="../Scripts/Validationfx.js" type="text/javascript"></script>


    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="../Styles.css" type="text/css" rel="stylesheet">--%>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->

    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body>
		
			<FORM id="Form2" method="post" runat="server">
                <div class="page-container row-fluid container-fluid">
                    <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
            </asp:Panel>
                </div>
                 <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>
                    <div class="col-md-12">
<div id="LblSucessdiv" visible="false" runat="server" >
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<asp:Label runat="server" ID="LblSucesstext"></asp:Label>
</div>
</div>
                    <div class='col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <!-- PAGE HEADING TAG - START -->
                                <h1 class="title">Arraignment</h1>
                                <!-- PAGE HEADING TAG - END -->
                            </div>
                    
                        </div>
                    </div>

                    <div class="clearfix"></div>
                     <asp:label id="lblMessage" runat="server" Width="351px" CssClass="label" ForeColor="Red" EnableViewState="False"></asp:label>
                     <div class="col-lg-12">
                        <section class="box ">
                               <header class="panel_header">
                                
                               
                                <div class="actions panel_actions pull-right">
                                    <uc3:PagingControl ID="Pagingctrl" runat="server" />
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <asp:datagrid id="dg_Arraignment" runat="server" Width="100%" CssClass="table"
											AutoGenerateColumns="False" OnSelectedIndexChanged="dg_Arraignment_SelectedIndexChanged">
											<Columns>
												<asp:TemplateColumn HeaderText="Last Name">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left" CssClass="form-label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_LastName runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.lastname") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="First Name">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left"  CssClass="form-label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_FirstName runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.firstname") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Case Number">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left"  CssClass="form-label"></HeaderStyle>
													<ItemTemplate>
														<asp:HyperLink id=hyp_CaseNum runat="server" CssClass="label" NavigateUrl='<%# String.Concat("../clientinfo/ViolationFeeOld.aspx?caseNumber=",DataBinder.Eval(Container, "DataItem.ticketid_pk"),"&search=0") %>' Text='<%# DataBinder.Eval(Container, "DataItem.refcasenumber")%>'></asp:HyperLink>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="MID #">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left"  CssClass="form-label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_MidNum runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.midnum") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Arraignment">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left"  CssClass="form-label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_Arraignment runat="server" CssClass="label" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.courtdatemain"),"#",DataBinder.Eval(Container, "DataItem.courtnumbermain")) %>'>
														</asp:Label>
														<asp:Label id=LblCourtDate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container,"DataItem.courtdatemain") %>' Visible="False">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Location">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Left"  CssClass="form-label"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lbl_Location runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.address") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
                                        <uc1:Footer id="Footer1" runat="server"></uc1:Footer>
                                         </div>
                                </div>
                            </div>
                            </section></div>
                    </section>
            </section>
				
			</FORM>
            <!-- CORE JS FRAMEWORK - START --> 
    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END --> 


    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 

    <script src="../assets/plugins/autosize/autosize.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


    <!-- CORE TEMPLATE JS - START --> 
    <script src="../assets/js/scripts.js" type="text/javascript"></script> 
    <!-- END CORE TEMPLATE JS - END --> 
		
	</body>
</HTML>

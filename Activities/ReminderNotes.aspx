<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.ReminderNotes" Codebehind="ReminderNotes.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReminderNotes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<script language ="javascript">
	function MaxLength()
		{
		if(document.getElementById("txt_RComments").value.length >500)
		{
		alert("The length of character is not in the specified Range");
		document.getElementById("txt_RComments").focus();
				
		}
				
		}
		
		 function ValidateLenght()
	     { 
			var cmts = document.getElementById("txt_RComments").value;
			if (cmts.length > 500)
			{
			    event.returnValue=false;
                event.cancel = true;
			}
	    }
	</script>
	<body >
		<form id="Form1" method="post" runat="server">
			<table class="clsLeftPaddingTable" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 372px"
				height="372" cellSpacing="1" cellPadding="0" align="left" border="0">
				<tr>
					<td><asp:label id="lblMessage" runat="server" EnableViewState="False" CssClass="label" ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td vAlign="top" align="right" width="220">
						<TABLE class="clsLeftPaddingTable" cellSpacing="0" cellPadding="0" width="100%" align="left"
							border="0">
							<tr >
								<td background="../images/separator_repeat.gif" colSpan="3" height="11"></td>
							</tr>
							<tr bgColor="#eff4fb">
								<td style="WIDTH: 133px" vAlign="middle" width="133" height="20"><font color="#3366cc">Name:</font></td>
								<td width="520" colSpan="2" height="20"><font color="#3366cc"><asp:label id="lbl_Name" runat="server" CssClass="label" ForeColor="#3366CC"></asp:label><asp:label id="lbl_rid" runat="server" Visible="False"></asp:label></font></td>
							</tr>
							<tr bgColor="#eff4fb">
								<td style="WIDTH: 133px" vAlign="middle" width="133" height="20"><font color="#3366cc">Case 
										Status:</font></td>
								<td width="520" colSpan="2" height="20"><font color="#3366cc"><asp:label id="lbl_Status" runat="server" CssClass="label" ForeColor="#3366CC"></asp:label></font></td>
							</tr>
							<tr bgColor="#eff4fb">
								<td style="WIDTH: 133px" vAlign="middle"><font color="#3366cc">Location:</font></td>
								<td colSpan="2" height="20"><font color="#3366cc"><asp:label id="lbl_location" runat="server" CssClass="label" ForeColor="#3366CC"></asp:label></font></td>
							</tr>
							<tr bgColor="#eff4fb">
								<td><font color="#3366cc">Call Back:</font></td>
								<td><asp:dropdownlist id="ddl_rStatus" runat="server" CssClass="clsInputCombo">
									</asp:dropdownlist></td>
							</tr>
							<tr bgColor="#eff4fb">
								<td rowSpan="2"><FONT color="#3366cc"><asp:label id="lbl_contacttext" runat="server" Width="104px" ForeColor="#3366CC">Contact Number:</asp:label></FONT></td>
								<td rowSpan="2"><font color="#3366cc"></font>
									<table align="left" border="0">
										<tr align="left">
											<td align="left" colSpan="1"><asp:label id=lbl_contact1 runat="server" CssClass="label" ForeColor="#3366CC" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'>
												</asp:label><br>
												<asp:label id=lbl_Contact2 runat="server" CssClass="label" ForeColor="#3366CC" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'>
												</asp:label><br>
												<asp:label id=lbl_Contact3 runat="server" CssClass="label" ForeColor="#3366CC" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'>
												</asp:label><asp:label id=lbl_phone runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'>
												</asp:label><asp:label id=lbl_Fax runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.fax") %>'>
												</asp:label></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr bgColor="#eff4fb">
								<td style="HEIGHT: 91px"><asp:label id="lbl_Language" runat="server" CssClass="label" ForeColor="#3366CC"></asp:label></td>
								&nbsp;
								<td style="HEIGHT: 91px" vAlign="top"><asp:textbox id="txt_RComments" runat="server" Width="250px" CssClass="clsinputadministration"
										TextMode="MultiLine" Height="88px" onkeypress = "ValidateLenght();"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<TR>
								<TD vAlign="top"><asp:label id="lbl_outsidefirm" runat="server" Width="104px" ForeColor="#3366CC" Visible="False">Outside attorney=</asp:label></TD>
								<TD vAlign="top" align="left" colSpan="1" rowSpan="1"><asp:label id="lbl_Firm" runat="server" CssClass="label" ForeColor="#3366CC" Visible="False"
										Font-Bold="True"></asp:label>&nbsp;
								</TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:label id="lbl_Bond" runat="server" Visible="False" Font-Bold="True" BackColor="#FFCC66">Bond</asp:label></TD>
								<TD vAlign="top" align="right"><asp:button id="btn_update" runat="server" CssClass="clsbutton" Text="Update" OnClientClick ="MaxLength();"></asp:button></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr >
					<td background="../images/separator_repeat.gif" colSpan="3" height="11"></td>
				</tr>
			<tr>
			<td>
			<asp:label id="lblComment" style="Z-INDEX: 101; LEFT: 104px; POSITION: absolute; TOP: 386px"
				runat="server" ForeColor="White">Label</asp:label><asp:label id="lblStatus" style="Z-INDEX: 102; LEFT: 242px; POSITION: absolute; TOP: 384px"
				runat="server" ForeColor="White">Label</asp:label>
				</td>
			</tr>
				</TABLE>
				</form>
	</body>
</HTML>

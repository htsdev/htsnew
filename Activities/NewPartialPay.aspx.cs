using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Activities
{
    public partial class NewPartialPay : System.Web.UI.Page
    {
        clsENationWebComponents clsdb = new clsENationWebComponents();
        clsSession cSession = new clsSession();
        clsLogger clog = new clsLogger();
        DataView Dv;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (cSession.IsValidSession(this.Request) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    if (!IsPostBack)
                    {
                        lblMessage.Text = "";

                        sduedate.SelectedDate = DateTime.Today;
                        eduedate.SelectedDate = DateTime.Today;

                        // Tahir 4842 09/29/2008 Fill Case types for court locations
                        FillCourts();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void FillCourts()
        {
            ddlCourtLocation.DataSource = (DataSet)clsdb.Get_DS_BySP("USP_HTP_GET_ALL_CASETYPE");
            ddlCourtLocation.DataTextField = "CaseTypeName";
            ddlCourtLocation.DataValueField = "CaseTypeId";
            ddlCourtLocation.DataBind();
            ddlCourtLocation.Items.Insert(0, new ListItem("All Courts", "0"));
        }

        private void BindGrid()
        {
            try
            {
                dg_partial.CurrentPageIndex = 0;
                int showpast = 0;
                if (chk_pastdues.Checked == true)
                {
                    showpast = 1;
                }
                // tahir 4842 09/29/2008 added case status type filter...
                string[] key ={ "@dsdata", "@dedate", "@cSdate", "@cEdate", "@status", "@chk", "@CourtType", "@CaseStatus" };
                object[] value ={ sduedate.SelectedDate.ToShortDateString(), eduedate.SelectedDate.ToShortDateString(), System.DateTime.Now, System.DateTime.Now, Convert.ToInt32(ddl_partialstatus.SelectedValue), showpast, Convert.ToInt32(ddlCourtLocation.SelectedValue), Convert.ToInt16(ddl_CaseStatus.SelectedValue ) };
                // end 4842
                DataSet DS = clsdb.Get_DS_BySPArr("USP_HTS_GET_PARTIALPAYREPORTNEW", key, value);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    GenerateSerialNo(DS);
                    Dv = new DataView(DS.Tables[0]);
                    dg_partial.DataSource = DS;
                    Session["DS"] = DS;
                    Session["Dv_PartialPay"] = Dv;
                    dg_partial.DataBind();
                    ddl_Pageno.Visible = true;
                    FillPageList();
                }
                else
                {
                    dg_partial.DataSource = DS;
                    dg_partial.DataBind();
                    ddl_Pageno.Visible = false;
                    lblMessage.Text = "No Record Found";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #region Navigational LOgic
        private void FillPageList()
        {
            try
            {
                Int16 idx;

                ddl_Pageno.Items.Clear();
                for (idx = 1; idx <= dg_partial.PageCount; idx++)
                {
                    ddl_Pageno.Items.Add("Page - " + idx.ToString());
                    ddl_Pageno.Items[idx - 1].Value = idx.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        #endregion

        protected void ddlPageno_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //dg_partial.CurrentPageIndex = ddlPageno.SelectedIndex;
                //Dv = (DataView)Session["Dv_PartialPay"];


                //    dg_partial.DataSource = Dv;
                //    dg_partial.DataBind();
                //    SerialNumber();
                //FillPageList();



            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        #region

        #endregion

        #region GenerateSerialNo
        private void GenerateSerialNo(DataSet ds)
        {
            //long sNo = (dg_partial.CurrentPageIndex) * (dg_partial.PageSize);
            //try
            //{
            //    foreach (DataGridItem ItemX in dg_partial.Items)
            //    {
            //        sNo += 1;

            //        ((HyperLink)(ItemX.FindControl("hlnk_Sno"))).Text = sNo.ToString();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    lblMessage.Text = ex.Message;
            //    clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            //}

            if (ds.Tables[0].Columns.Contains("sno") == false)
            {
                ds.Tables[0].Columns.Add("sno");
                int sno = 1;
                ds.Tables[0].Rows[0]["sno"] = 1;

                for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i - 1]["ticketid_pk"].ToString() != ds.Tables[0].Rows[i]["ticketid_pk"].ToString())
                    {
                        ds.Tables[0].Rows[i]["sno"] = ++sno;
                    }

                }

            }
        }
        #endregion

        protected void dg_partial_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                Dv = (DataView)Session["Dv_PartialPay"];
                dg_partial.CurrentPageIndex = e.NewPageIndex;
                dg_partial.DataSource = Dv;
                dg_partial.DataBind();
                //SerialNumber();
                //FillPageList();
                ddl_Pageno.SelectedIndex = dg_partial.CurrentPageIndex;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        // Fahad 2780 (2-08-08)
        // Paging issues has been resolved 
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (chk_AllowPaging.Checked)
            {
                dg_partial.AllowPaging = true;
                lblMessage.Text = "";
                BindGrid();
                ddl_Pageno.Visible = true;
            }
            else
            {
                dg_partial.AllowPaging = false;
                lblMessage.Text = "";
                BindGrid();
                ddl_Pageno.Visible = false;
            }
        }
        //End Fahad

        protected void dg_partial_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                if (drv == null)
                    return;
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (e.Item.ItemIndex > 0)
                    {
                        int ticketId = Convert.ToInt32(((Label)e.Item.FindControl("lbl_ticketid")).Text);
                        int lastRowTicketId = Convert.ToInt32(((Label)dg_partial.Items[(e.Item.ItemIndex) - 1].FindControl("lbl_ticketid")).Text);
                        if (ticketId == lastRowTicketId)
                        {
                            e.Item.Cells[1].Text = "";
                            e.Item.Cells[2].Text = "";
                            e.Item.Cells[3].Text = "";
                        }
                    }
                    //((HyperLink)e.Item.FindControl("hlnk_Sno")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber=" + ((Label)e.Item.FindControl("lbl_ticketid")).Text.ToString();
                    ((ImageButton)e.Item.FindControl("ImgComment")).Attributes.Add("OnClick", "return OpenEditWin(" + (int)drv["TicketID_PK"] + "," + (int)drv["scheduleid"] + ");");
                    
                    //Check if Phone Number Exists
                    string tele1 = "", tele2 = "", tele3 = "";
                    if (e.Item.Cells[2].Controls.Count > 0)
                    {
                        //Eliminating Bracket of telephone number
                        tele1 = ((Label)(e.Item.FindControl("lbl_contact1"))).Text;
                        tele2 = ((Label)(e.Item.FindControl("lbl_contact2"))).Text;
                        tele3 = ((Label)(e.Item.FindControl("lbl_contact3"))).Text;
                    }
                    if (tele1 != "")
                    {
                        if (tele1.StartsWith("("))
                        {
                            ((Label)(e.Item.FindControl("lbl_contact1"))).Visible = false;
                        }
                    }
                    if (tele2 != "")
                    {
                        if (tele2.StartsWith("("))
                        {
                            ((Label)(e.Item.FindControl("lbl_contact2"))).Visible = false;
                        }
                    }
                    if (tele3 != "")
                    {
                        if (tele3.StartsWith("("))
                        {
                            ((Label)(e.Item.FindControl("lbl_contact3"))).Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        protected void ddl_Pageno_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                dg_partial.CurrentPageIndex = ddl_Pageno.SelectedIndex;
                Dv = (DataView)Session["Dv_PartialPay"];
                dg_partial.DataSource = Dv;
                dg_partial.DataBind();
                //SerialNumber();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

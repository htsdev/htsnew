using System;
using System.Collections;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.WebControls;
using System.IO;


namespace lntechNew.Activities
{
    public partial class frmManualCC : System.Web.UI.Page
    {
        clsSession cSession = new clsSession();
        clsManualCC clsprocessdata = new clsManualCC();
        clsLogger bugTracker = new clsLogger();
        lntechNew.Components.clsGeneralMethods clsGMethod = new lntechNew.Components.clsGeneralMethods();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //// Card Reader //
                txt_creditcard.Attributes.Add("onblur", "GetFocus();return false;");
                imgbtn_creditcard.Attributes.Add("onclick", "ShowReaderPopup();return false;");
                txt_creditcard.Attributes.Add("onKeyPress", "return VerifyCard();");
                btn_popupCancel.Attributes.Add("OnClick", "closereaderpopup(" + 1 + ");return false;");
                btn_popupok.Attributes.Add("OnClick", "FillCreditInfo();return false;");


                ////

                clsprocessdata.TransactionType = clsGMethod.SetTransactionMode(this.Request);

                if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //to stop page further execution
				   ViewState["vEmpID"] = cSession.GetCookie("sEmpID", this.Request);             

                if(!IsPostBack)
                {
                   
                                        
                    //Populating year Combo
                    int lbound = DateTime.Now.Date.Year;
                    string lb = lbound.ToString();
                    int ubound = DateTime.Now.AddYears(10).Date.Year;
                    string ub = ubound.ToString();
                    lb = lb.Substring(2);
                    ub = ub.Substring(2);

                    for (int i = Convert.ToInt32(lb); i <= Convert.ToInt32(ub); i++)
                    {
                        if (i < 10)
                        {
                            //string year =""i.ToString()
                            ddl_year.Items.Add(String.Format("0{0}", i.ToString()));
                        }
                        else
                        {
                            ddl_year.Items.Add(i.ToString());
                        }
                    }

                    //CardType DropDown
                    DataSet ds_cardtype = clsprocessdata.GetCardType();
                    ddl_cardtype.DataSource = ds_cardtype;
                    ddl_cardtype.DataTextField = ds_cardtype.Tables[0].Columns[1].ColumnName;
                    ddl_cardtype.DataValueField = ds_cardtype.Tables[0].Columns[0].ColumnName;
                    ddl_cardtype.DataBind();
                    ddl_cardtype.Items.Insert(0, "--Choose--");

                   
                }

                //btn_submit.Attributes.Add("onclick", "return FormValidation();");
                btn_reset.Attributes.Add("onclick", "return ClearControls();");

                //// Check transaction mode in webconfig///
                if (clsprocessdata.TransactionType == true)
                {
                    lbl_transmode.Text = "Application running in test mode.";
                    hf_transactionmode.Value = "1";
                }
                else
                {
                    lbl_transmode.Text = "";
                    hf_transactionmode.Value = "0";
                }
            }

            catch (Exception ex)
            {
                this.lblMessage.Text =ex.Message + ex.InnerException;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        //Sabir Khan 4959  10/15/2008
        private void SendMail(string name, string Description, decimal Amount)
        {
            try
            {
                string subject = "Oscar Client Payment";                               
                string message = "<table border=1 align=\"center\" class=\"label\"><tr><td align=\"center\" class=\"label\" style=\"height: 14px\"><h3><b>Oscar Client Payment </b></h3></td></tr></table><br /><br /><br /><table class=\"label\" align=\"center\" style=\"width: 446px\"><tr><td align=\"left\"><table width=\"100%\" class=\"label\"><tr><td style=\"width: 130px\"><b>CLIENT NAME : </b></td><td>" + name + "</td>\n</tr><tr><td style=\"width: 130px\"><b>DESCRIPTION : </b></td><td>" + Description + "</td>\n</tr><tr><td style=\"width: 130px\"><b>AMOUNT : </b></td><td>" + Amount + "</td>\n</tr></table></td></tr></table>";
                message = lntechNew.Components.MailClass.GetEmailBody(message);
                string toUser = (string)ConfigurationManager.AppSettings["OsEmailTo"];
                string ccUser = (string)ConfigurationManager.AppSettings["OsEmailCC"];
                lntechNew.Components.MailClass.SendMailToAttorneys(message, subject, "",toUser, ccUser);
            }
            catch (Exception ex)
            {
                this.lblMessage.Text = ex.Message + ex.InnerException;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }
        //protected void btn_submit_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ////// set data to variables               
               

        //        this.lblMessage.Text = "";
        //        divinfo.Visible = false;

        //        clsprocessdata.EmployeeID = Convert.ToInt32(ViewState["vEmpID"]);
        //        clsprocessdata.Fname = txt_fname.Text;
        //        clsprocessdata.Lname = txt_lname.Text;
        //        clsprocessdata.Nameoncard = txt_nameoncard.Text;
        //        clsprocessdata.CCNO = txt_ccnumber.Text;
        //        clsprocessdata.ExpDate = ddl_month.SelectedValue + "/" + ddl_year.SelectedValue;                 
        //        clsprocessdata.CIN = txt_cin.Text;
        //        clsprocessdata.Amount = Convert.ToDecimal (txt_amount.Text);
        //        clsprocessdata.ClientType = ddl_clienttype.SelectedValue;
        //        clsprocessdata.Description = txt_description.Text;
        //        //if (rdb_charge.Checked)
        //        //    clsprocessdata.TransactType = "Charge";
        //        //else if (rdb_refund.Checked)
        //        //    clsprocessdata.TransactType = "Refund";
                
        //        //Modified By Ozair
        //        if (clsprocessdata.ProcessData())
        //        {
        //            //Sabir Khan 4959  10/15/2008
        //            if (ddl_clienttype.SelectedValue == "2")
        //            { SendMail(clsprocessdata.Lname + " " + clsprocessdata.Fname, clsprocessdata.Description, clsprocessdata.Amount); }
        //            ClearControls();
        //            this.lblMessage.Text = "Transaction has been completed";
        //            divinfo.Attributes.Add("class", "alert alert-info alert-dismissable");
        //            divinfo.Visible = true;
        //        }
        //        else
        //        {
        //            ClearControls();
        //            this.lblMessage.Text = "An error occured during processing.";
        //            divinfo.Attributes.Add("class", "alert alert-danger alert-dismissable");
        //            divinfo.Visible = true;
        //        }

        //        //// Check transaction mode in webconfig///
        //        if (clsprocessdata.TransactionType == true)
        //        {
        //            divinfo.Attributes.Add("class", "alert alert-info alert-dismissable");
        //            divinfo.Visible = true;
        //            lbl_transmode.Text = "Application running in test mode.";
        //        }

        //        else
        //        {
        //            lbl_transmode.Text = "";
        //            divinfo.Visible = false;
        //        }
        //        ////////

        //    }
        //    catch (Exception ex)
        //    {
        //        this.lblMessage.Text = ex.Message + ex.InnerException;
        //        divinfo.Attributes.Add("class", "alert alert-danger alert-dismissable");
        //        divinfo.Visible = true;
        //        bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
        //    }

        //}

        //////////Clear All Controls
        private void ClearControls()
        {          
                txt_fname.Text = "";
                txt_lname.Text = "";
                txt_nameoncard.Text = "";
                txt_ccnumber.Text = "";
                ddl_month.SelectedIndex = 0;
                ddl_year.SelectedIndex = 0;
                txt_cin.Text = "";
                txt_amount.Text = "";
                ddl_clienttype.SelectedIndex = 0;
                txt_description.Text = "";
                this.lblMessage.Text = "";
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            try
            {
                ////// set data to variables               


                this.lblMessage.Text = "";
                divinfo.Visible = false;

                clsprocessdata.EmployeeID = Convert.ToInt32(ViewState["vEmpID"]);
                clsprocessdata.Fname = txt_fname.Text;
                clsprocessdata.Lname = txt_lname.Text;
                clsprocessdata.Nameoncard = txt_nameoncard.Text;
                clsprocessdata.CCNO = txt_ccnumber.Text;
                clsprocessdata.ExpDate = ddl_month.SelectedValue + "/" + ddl_year.SelectedValue;
                clsprocessdata.CIN = txt_cin.Text;
                clsprocessdata.Amount = Convert.ToDecimal(txt_amount.Text);
                clsprocessdata.ClientType = ddl_clienttype.SelectedValue;
                clsprocessdata.Description = txt_description.Text;
                //if (rdb_charge.Checked)
                //    clsprocessdata.TransactType = "Charge";
                //else if (rdb_refund.Checked)
                //    clsprocessdata.TransactType = "Refund";

                //Modified By Ozair
                if (clsprocessdata.ProcessData())
                {
                    //Sabir Khan 4959  10/15/2008
                    if (ddl_clienttype.SelectedValue == "2")
                    { SendMail(clsprocessdata.Lname + " " + clsprocessdata.Fname, clsprocessdata.Description, clsprocessdata.Amount); }
                    ClearControls();
                    this.lblMessage.Text = "Transaction has been completed";
                    divinfo.Attributes.Add("class", "alert alert-info alert-dismissable");
                    divinfo.Visible = true;
                }
                else
                {
                    ClearControls();
                    this.lblMessage.Text = "An error occured during processing.";
                    divinfo.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    divinfo.Visible = true;
                }

                //// Check transaction mode in webconfig///
                if (clsprocessdata.TransactionType == true)
                {
                    divinfo.Attributes.Add("class", "alert alert-info alert-dismissable");
                    divinfo.Visible = true;
                    lbl_transmode.Text = "Application running in test mode.";
                }

                else
                {
                    lbl_transmode.Text = "";
                    divinfo.Visible = false;
                }
                ////////

            }
            catch (Exception ex)
            {
                this.lblMessage.Text = ex.Message + ex.InnerException;
                divinfo.Attributes.Add("class", "alert alert-danger alert-dismissable");
                divinfo.Visible = true;
                bugTracker.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

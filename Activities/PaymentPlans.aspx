﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentPlans.aspx.cs" Inherits="HTP.Activities.PaymentPlans"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="WCtl_Comments" Namespace="LNTech.CommentControl" TagPrefix="cc1" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Due Report</title>
    <link href="../Styles.css" rel="stylesheet" type="text/css" />

     <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript ">
     function ValidateCommentControls()
		{ 
            
	      //Comments Textboxes
		  var dateLenght = 0;
		  var newLenght = 0;
		  
          newLenght = document.getElementById("wctl_GeneralComments_txt_comments").value.length
		  if(newLenght > 0){dateLenght = newLenght + 27}else{dateLenght = 0}//27 is lenght for salesrep shortname and date time

		  if (document.getElementById("wctl_GeneralComments_txt_comments").value.length + document.getElementById("wctl_GeneralComments_lbl_comments").innerText.length > (2000 - dateLenght ))//-27 bcoz to show last time partconcatenated with comments
		  {
		      //alert("Sorry You cannot type in more than 2000 characters in General comments box")
		      $("#txtErrorMessage").text("Sorry You cannot type in more than 2000 characters in General comments box");
		      $("#errorAlert").modal();
			return false;		  
		  }
    }
    
    function Showfollowup()
    {
        if (document.getElementById("lbl_Paymentfollowupdate") != "block")
        {        
            document.getElementById("lbl_Paymentfollowupdate").style.display="none";
            document.getElementById("cal_followup").style.display="block";            
        }
         
    }
    
    function CheckDate(seldate, tbID)
    {
          var weekday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
          var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth()+1)+"/"+Date.parseInvariant(seldate).getDate() + "/"+Date.parseInvariant(seldate).getFullYear(),"MM/dd/yyyy");                
          newseldate = Date.parseInvariant(newseldatestr,"MM/dd/yyyy");
          // Noufil 5802 06/03/2009 Allow followupdate to set on saturday
          if (weekday[newseldate.getDay()] == "Sunday") 
          {
              //alert("Please select any business day");
              $("#txtErrorMessage").text("Please select any business day");
              $("#errorAlert").modal();
              return false;
          }
    }
    
    function Validate()
    {
        var followupdate = document.getElementById("cal_followup").value;
        var weekday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
          //var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth()+1)+"/"+Date.parseInvariant(seldate).getDate() + "/"+Date.parseInvariant(seldate).getFullYear(),"MM/dd/yyyy");                
          newseldate = Date.parseInvariant(followupdate,"MM/dd/yyyy");
          // Noufil 5802 06/03/2009 Allow followupdate to set on saturday
          if (weekday[newseldate.getDay()] == "Sunday" ) 
          {
              //alert("Please select any business day");
              $("#txtErrorMessage").text("Please select any business day");
              $("#errorAlert").modal();
              return false;
          }
    }
    
    function ShowFollowupdate()
    {    
        if (document.getElementById("td_followcal").style.display == "block")
        {
            document.getElementById("td_followcal").style.display="none";
            document.getElementById("td_followlabel").style.display="block";
             document.getElementById("hf_showhide").value ="0";
        }
        else
        {
            document.getElementById("td_followcal").style.display="block";
            document.getElementById("td_followlabel").style.display="none";
            document.getElementById("hf_showhide").value ="1";
            
        }
        return false;
    }
    
    function cc()
    {
        document.getElementById("td1").style.display="block";
        return false;
    }
    </script>

    <script language="JavaScript" type="text/javascript">

     var Page;
     var postBackElement;
     
     function pageLoad()
     {
          Page = Sys.WebForms.PageRequestManager.getInstance(); 
          Page.add_beginRequest(OnBeginRequest);
          Page.add_endRequest(endRequest);
     }

    function OnBeginRequest(sender, args)
    {   
	    var IpopTop = (document.body.clientHeight - document.getElementById("IMGDIV").offsetHeight) / 2;
        var IpopLeft = (document.body.clientWidth - document.getElementById("IMGDIV").offsetWidth) / 2;            
        document.getElementById("IMGDIV").style.left=IpopLeft + document.body.scrollLeft;
        document.getElementById("IMGDIV").style.top=IpopTop + document.body.scrollTop;            
        document.getElementById("IMGDIV").style.display = "block";
        $get("IMGDIV").style.display="";
    } 

    function endRequest(sender, args)
    {
            
            $get("IMGDIV").style.display="none";
            
            //Nasir 6049 07/16/2009 move down when client select
            var val=sender._postBackSettings.panelID;                                    
            if(val.indexOf("lnk_clientname")!=-1)
            {
                var el=$get("lbl_clientname");
                el.scrollIntoView(true);
            }
            
    }
    //Nasir 6098 08/22/20009 hide iscomplaint check box
     function HideCheckBox()
        {
                document.getElementById("wctl_GeneralComments_chk_Complaint").style.display="none";
                document.getElementById("wctl_GeneralComments_lbl_Complaint").style.display="none";
        }
    
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <aspnew:ScriptManager ID="ScriptManager1" runat="server">
    </aspnew:ScriptManager>
    <div id="IMGDIV" style="display: none; height: 60px; width: 200px; position: absolute;
        vertical-align: middle; background-color: White; z-index: 40; margin-top: auto;
        margin-left: auto; border: 0; border-top: black thin solid; border-left: black thin solid;
        border-bottom: black thin solid; border-right: black thin solid;">
        <%--top: 90%; left: 45%;--%>
        <table id="Table1" width="100%">
            <%--<tr>
                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <img alt="" src="../images/plzwait.gif" />&nbsp;
                    <asp:Label ID="lbl1" runat="server" CssClass="clssubhead" Text="Please Wait ......"></asp:Label>
                </td>
            </tr>--%>

            <section class="box" id="" style="">
                    
       
                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                              
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:Label ID="lbl1" runat="server" CssClass="form-label" Text="Please Wait ......"></asp:Label>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>
                </section>

        </table>
    </div>
   
        <aspnew:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>

                 <div class="page-container row-fluid container-fluid">

 <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="paymentPlansPage" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">

             <div class="col-xs-12">
           
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                </div>
                 </div>
            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Payment Due Report</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>

            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Payment Due Report</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Court Type :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlCourtLocation" runat="server" CssClass="form-control" >
                                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                                    <asp:ListItem Value="1">Traffic</asp:ListItem>
                                                                    <asp:ListItem Value="2">Criminal</asp:ListItem>
                                                                    <asp:ListItem Value="4">Family Law</asp:ListItem>
                                                                    <asp:ListItem Value="3">Civil</asp:ListItem>
                                                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>
                      <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label"> Criteria :</label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:DropDownList ID="ddl_criteria" runat="server" CssClass="form-control" >
                                                                    <asp:ListItem Value="1">Payment Reminder Calls</asp:ListItem>
                                                                    <asp:ListItem Value="4">Past Dues Calls</asp:ListItem>
                                                                    <asp:ListItem Value="2">Payment Defaults</asp:ListItem>
                                                                    <asp:ListItem Value="3">Waived Payments</asp:ListItem>
                                                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>
                     <div class="clearfix"></div>

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Start Date :</label>
                                <span class="desc"></span>
                                                                <picker:datepicker id="sdate" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                              <%--  <ew:CalendarPopup ID="sdate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" CssClass="form-control"
                                                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" SelectedDate="2006-05-26"
                                                                    ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                                    Width="65px">
                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         

                     <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">End Date :</label>
                                <span class="desc"></span>
                                <div class="">
                                    <picker:datepicker id="edate" runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                 <%--  <ew:CalendarPopup ID="edate" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom" CssClass="form-control"
                                                                    ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                    ImageUrl="../images/calendar.gif" PadSingleDigits="True" SelectedDate="2006-05-26"
                                                                    ShowGoToToday="True" ToolTip="Select Report Date Range" UpperBoundDate="12/31/9999 23:59:00"
                                                                    Width="65px">
                                                                    <TextboxLabelStyle CssClass="clstextarea" />
                                                                    <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Gray" />
                                                                    <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                        Font-Size="XX-Small" ForeColor="Black" />
                                                                    <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                    <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                        ForeColor="Black" />
                                                                </ew:CalendarPopup>--%>
                                   
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>
                    
<div class="col-md-12">
<div class="row">
<div class="col-md-6">
<div class="form-group showAllCheck">
<span class="desc"></span>
<div class="controls">
<asp:CheckBox ID="chk_pplan" runat="server" CssClass="clssubhead" Checked="true" Text="Show All" />
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<div class="controls pull-right">
<asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" />
</div>
</div>
</div>
</div>                                               
</div>



            </section>

             <div class="clearfix"></div>

            <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">LIST</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <%--<a class="box_toggle fa fa-chevron-down"></a>--%>
                         <uc3:PagingControl ID="Pagingctrl" runat="server" />
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12" id="TblGrid">
                                                            <div class="form-group">
                             <%-- <label class="form-label">Court Type :</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    
                                    <div class="table-responsive" data-pattern="priority-columns">
                                                    <asp:GridView ID="gv_paymentplan" runat="server" AutoGenerateColumns="False" 
                                                        CssClass="table table-small-font table-bordered table-striped" AllowPaging="True" AllowSorting="True" PageSize="30"
                                                        OnPageIndexChanged="gv_paymentplan_PageIndexChanged" OnSorting="gv_paymentplan_Sorting"
                                                        OnPageIndexChanging="gv_paymentplan_PageIndexChanging" OnRowCommand="gv_paymentplan_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="S.no">
                                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" VerticalAlign="Top"
                                                                    Width="3%" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hlnk_Sno" runat="server" Text='<%# Eval("sno") %>' NavigateUrl='<%# "../clientinfo/newpaymentinfo.aspx?casenumber=" + Eval("TicketID_PK")+"&search=0" %>'></asp:HyperLink>
                                                                    <asp:Label ID="lbl_ticketid" runat="server" CssClass="form-label" Text='<%# Eval("TicketID_PK") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Client</u>" SortExpression="ClientName">
                                                                <HeaderStyle CssClass="clsaspcolumnheader" HorizontalAlign="Left" VerticalAlign="Top"
                                                                    Width="20%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnk_clientname" runat="server" CommandName="paymentdetail" Text='<%# Eval("ClientName") %>'
                                                                        CommandArgument='<%# Eval("TicketID_PK") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Crt</u>" SortExpression="crt">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                                <ItemStyle VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt" Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCrt" CssClass="form-label" runat="server" Text='<%# Eval("crt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Crt Type</u>" SortExpression="CrtType">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                                <ItemStyle VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt" Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCrtType" CssClass="form-label" runat="server" Text='<%# Eval("CrtType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Crt Date</u>" SortExpression="CourtDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                                                <ItemStyle VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt" Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_contactdate" CssClass="form-label" runat="server" Text='<%# Eval("CourtDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Total Fee</u>" SortExpression="TotalFeeCharged">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFee" CssClass="form-label" runat="server" Text='<%# Eval("TotalFeeCharged","{0:C0}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Amt Due</u>" SortExpression="Owes">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblowes" CssClass="form-label" runat="server" Text='<%# Eval("Owes", "{0:C0}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Payment Due</u>" SortExpression="PaymentDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPayplandate" CssClass="form-label" runat="server" Text='<%# Eval("PaymentDate","{0:MM/dd/yyyy}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<u>Follow up</u>" SortExpression="PaymentFollowUpDate">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFollowupdate" CssClass="form-label" runat="server" Text='<%# Eval("PaymentFollowUpDate","{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField SortExpression="Pastdays">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" ForeColor="#123160" Font-Size="8pt"
                                                                    Font-Names="Tahoma" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPastdays" CssClass="form-label" runat="server" Text='<%# Eval("Pastdays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle HorizontalAlign="Center" CssClass="paginationBox1" />
                                                        <PagerSettings Mode="NextPreviousFirstLast" NextPageText="Next &gt;" PreviousPageText="&lt; Previous"
                                                            FirstPageText="&lt;&lt; First Page&nbsp;&nbsp;&nbsp;" LastPageText="Last Page &gt;&gt;" />
                                                    </asp:GridView>
                                        </div>
                                   
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                </section>

            <table style="display:none">
            <tr>
                                    <td align="center" valign="top" style="width: 100%">
                                        <a name="client"></a>
                                        <table runat="server" id="tbl_paymentdetail" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11" width="100%">
                                                    </td>
                                                </tr>
                                                <tr class="clsLeftPaddingTable">
                                                    <td height="11px" colspan="7" align="right" style="width: 800px">
                                                        <asp:LinkButton ID="lnkbtn_First" runat="server" Text=" << First Record " OnClick="lnkbtn_First_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkbtn_Previous" runat="server" Text=" < Previous " OnClick="lnkbtn_Previous_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkbtn_Next" runat="server" Text="Next > " OnClick="lnkbtn_Next_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkbtn_Last" runat="server" Text=" Last Record >>" OnClick="lnkbtn_Last_Click"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="clsLeftPaddingTable">
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td height="11px" colspan="7" style="font-size: 18px;">
                                                                    <asp:Label ID="lbl_clientname" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td style="width: 10px">
                                                                </td>
                                                                <td valign="middle" align="right">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 33%; border: 1; border-color: Black; border-right-width: thin"
                                                                                align="left">
                                                                                <asp:Label ID="lblContact1" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 33%; border: 1; border-color: Black;" align="left">
                                                                                <asp:Label ID="lblContact2" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 33%; border: 1; border-color: Black;" align="left">
                                                                                <asp:Label ID="lblContact3" runat="server" CssClass="clsLeftPaddingTable"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                                        <table>
                                                            <tr>
                                                                <td align="right" class="clssubhead">
                                                                    Payment Details
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <asp:GridView ID="GV_paymentdetail" runat="server" AutoGenerateColumns="False" Width="100%"
                                                            CssClass="clsLeftPaddingTable">
                                                            <Columns>
                                                                <asp:BoundField HeaderText="Payment Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="15%" DataField="paymentdate" DataFormatString="{0:MM/dd/yyyy}"
                                                                    ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Rep" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="5%" DataField="rep" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Amount" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="10%" DataField="amount" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Type" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="25%" DataField="types" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:BoundField HeaderText="Response" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-Width="30%" DataField="response" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_ticketid" runat="server" Value='<%# Eval("TicketID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td background="../../images/separator_repeat.gif" height="11">
                                                    </td>
                                                </tr>
                                            
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table runat="server" id="tbl_paymplan" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td background="../Images/subhead_bg.gif" class="clssubhead" align="left" style="height: 34px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="800px">
                                                        <tr>
                                                            <td align="left" valign="middle" class="clssubhead" style="width: 20%">
                                                                &nbsp;Payment Plans
                                                            </td>
                                                            <td style="width: 80%;" align="center">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="clssubhead" align="center" style="width: 21%">
                                                                            Owes$:&nbsp;<asp:Label ID="lbl_owes" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="center" class="clssubhead" style="width: 21%">
                                                                            Plan$:&nbsp;<asp:Label ID="lbl_plan" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="center" class="clssubhead" style="width: 24%">
                                                                            Difference$:&nbsp;<asp:Label ID="lbl_defrence" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td align="right" class="clssubhead" style="width: 15%">
                                                                            Follow-Up Date :
                                                                        </td>
                                                                        <td align="left" style="width: 15%; display: block;" valign="middle" id="td_followlabel"
                                                                            runat="server">
                                                                            <asp:Label ID="lbl_Paymentfollowupdate" CssClass="clssubhead" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td id="td_followcal" runat="server" style="display: none; width: 15%" align="left">
                                                                            <ew:CalendarPopup ID="cal_followup" runat="server" AllowArbitraryText="False" CalendarLocation="Bottom"
                                                                                ControlDisplay="TextBoxImage" Culture="(Default)" Font-Names="Tahoma" Font-Size="8pt"
                                                                                ImageUrl="../images/calendar.gif" PadSingleDigits="True" ShowGoToToday="True"
                                                                                ToolTip="Select Follow-Up Date" UpperBoundDate="12/31/9999 23:59:00" Width="65px"
                                                                                JavascriptOnChangeFunction="CheckDate">
                                                                                <TextboxLabelStyle CssClass="clstextarea" />
                                                                                <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                                <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Gray" />
                                                                                <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                                <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                                                    Font-Size="XX-Small" ForeColor="Black" />
                                                                                <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                                <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                                                    ForeColor="Black" />
                                                                            </ew:CalendarPopup>
                                                                        </td>
                                                                        <td style="width: 4%" align="left">
                                                                            <asp:ImageButton ID="ImgBtn_Followup" runat="server" ImageUrl="~/Images/Rightarrow.gif"
                                                                                ToolTip="Payment Follow-Up Date" OnClientClick="return ShowFollowupdate();" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../../images/separator_repeat.gif" colspan="7" height="11">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <asp:GridView ID="GV_paymplan" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="clsLeftPaddingTable" OnRowDataBound="GV_paymplan_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Amount($)">
                                                                <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="33%" />
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_amount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Amount") %>'
                                                                        CssClass="clsLeftPaddingTable"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Payment Date" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="33%" DataField="paymentdate" DataFormatString="{0:MM/dd/yyyy}"
                                                                ItemStyle-CssClass="clsLeftPaddingTable" />
                                                            <asp:BoundField HeaderText="Days Left" HeaderStyle-CssClass="clssubhead" HeaderStyle-HorizontalAlign="Left"
                                                                HeaderStyle-Width="33%" DataField="daysleft" ItemStyle-CssClass="clsLeftPaddingTable" />
                                                            <%--<asp:TemplateField HeaderText="Payment Date">
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="33%" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_rep" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"paymentdate","{0:d}") %>'
                                                                    CssClass="clsLeftPaddingTable"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                            <%--<asp:TemplateField HeaderText="Days Left">
                                                            <HeaderStyle CssClass="clssubhead" HorizontalAlign="Left" VerticalAlign="Top" Width="33%" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_amout" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"daysleft") %>'
                                                                    CssClass="clsLeftPaddingTable"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <td align="center" valign="top">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../../images/separator_repeat.gif" height="11">
                                                </td>
                                                <td background="../../images/separator_repeat.gif" height="11">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" valign="top">
                                        <table runat="server" id="tbl_generalcomments" cellpadding="0" cellspacing="0" border="0"
                                            width="100%">
                                            <tr>
                                                <td height="25px" colspan="7">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" class="clssubhead">
                                                    General Comments
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="11px" colspan="7">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" width="100%">
                                                    <cc1:WCtl_Comments ID="wctl_GeneralComments" runat="server" Width="753px"></cc1:WCtl_Comments>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="../../images/separator_repeat.gif" height="11" width="800px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="Button1" runat="server" CssClass="clsbutton" Text="Update" OnClick="Button1_Click"
                                                        OnClientClick="return Validate();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


            </table>

            <section class="box" id="" style="display:none">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <uc1:Footer ID="Footer1" runat="server" />
                                        <asp:HiddenField ID="hf_showhide" runat="server" Value="0" />
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>













                     </section>
                     </section>
                     </div>

                </ContentTemplate>
             <Triggers>
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_First" />
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_Previous" />
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_Next" />
                <aspnew:AsyncPostBackTrigger ControlID="lnkbtn_Last" />
                <aspnew:AsyncPostBackTrigger ControlID="btnSubmit" />
                <aspnew:AsyncPostBackTrigger ControlID="Button1" />
            </Triggers>
            </aspnew:UpdatePanel>
    </form>

     <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
     <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>
</html>

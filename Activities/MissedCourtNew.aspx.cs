using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;
using lntechNew.Components;
//Waqas 5057 03/19/2009 Changed to HTP
namespace HTP.Activities
{
    public partial class MissedCourtNew : System.Web.UI.Page
    {
        clsENationWebComponents ClsDb = new clsENationWebComponents();
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        string SessionRecid;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Waqas 5057 03/17/2009 Checking employee info in session
                if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
                {
                    Response.Redirect("../frmlogin.aspx", false);
                }
                else //To stop page further execution
                {
                    SessionRecid = Convert.ToString(Session["ReminID"]);

                    if (Page.IsPostBack != true)
                    {
                        cal_EffectiveFrom.SelectedDate = DateTime.Now; //setting the date
                        if (SessionRecid == "")					 //If not redirecting from Notes Page
                            Session["ReminID"] = "1";		 //initialize Session RecID	

                    }
                    FillGrid(); //filling the grid
                    SetUrl();	//	putting validations on grid
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
        }


        private void FillGrid() // Filling the Grid
        {
            try
            {	//"@employeeid"  ClsSession.GetSessionVariable("sEmpID",this.Session)
                String[] parameters = { "@StartDate_Input", "@EndDate_Input", "@ClientStatus" };
                object[] values = { cal_EffectiveFrom.SelectedDate, cal_EffectiveTo.SelectedDate, ddl_ClientStatus.SelectedItem.Value };

                DataSet ds_MissedCourt = ClsDb.Get_DS_BySPArr("USP_HTS_Get_MissedCourtInfoNew", parameters, values);

                dg_MissedCourt.DataSource = ds_MissedCourt;
                dg_MissedCourt.DataBind();
                //S No
                BindReport();
                if (dg_MissedCourt.Items.Count == 0)
                {
                    lblMessage.Text = "No Records";
                    dg_MissedCourt.Visible = false;
                }
                else
                {
                    dg_MissedCourt.Visible = true;

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void BindReport()
        {
            long sNo = (dg_MissedCourt.CurrentPageIndex) * (dg_MissedCourt.PageSize);
            try
            {
                foreach (DataGridItem ItemX in dg_MissedCourt.Items)
                {
                    sNo += 1;

                    ((TextBox)(ItemX.FindControl("txt_sno"))).Text = sNo.ToString();
                }
                //To assign total num of records
                txt_totalrecords.Text = sNo.ToString();

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        private void SetUrl() // setting the grid
        {
            try
            {
                foreach (DataGridItem items in dg_MissedCourt.Items)
                {
                    // setting hyperlink
                    ((HyperLink)(items.FindControl("lnkName"))).NavigateUrl = "../ClientInfo/ViolationFeeold.aspx?search=0&casenumber=" + ((Label)(items.FindControl("lbl_TicketID"))).Text;

                    //setting Callbacks
                    /*Label lblCalls = (Label)(items.FindControl("lbl_CallBacks"));
                    if (lblCalls.Text == "0")
                    {
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = "Pending";
                    }
                    else if (lblCalls.Text == "1")
                    {
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = "Contacted";
                    }
                    else if (lblCalls.Text == "2")
                    {
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = "All";
                    }
                    else
                    {
                        ((Label)(items.FindControl("lbl_CallBacks"))).Text = "None";
                    }
                    // Link to move back to violation fee page
                    ((HyperLink)(items.FindControl("lnk_Comments"))).NavigateUrl = "javascript:window.open('remindernotes.aspx?casenumber=" + ((Label)(items.FindControl("lbl_TicketID"))).Text + "&violationID=" + ((Label)(items.FindControl("lbl_TicketViolationID"))).Text + "&searchdate=" + cal_EffectiveFrom.SelectedDate + "&Recid=" + ((TextBox)(items.FindControl("txt_sno"))).Text + "','','status=yes,width=450,height=310');void('');";
                    // Comments setting			 
                    if (((Label)(items.FindControl("lbl_Comments"))).Text == "")
                    {
                        ((Label)(items.FindControl("lbl_Comments"))).Text = "No Comments";
                    }*/
                    /*
                    // Status setting
                    if(((Label) (items.FindControl("lbl_Status"))).Text == "---Choose----")
                    {
                        ((Label) (items.FindControl("lbl_Status"))).Text = "";
                    }
                    */

                    /*
                    // Firm setting
                    if (((Label)(items.FindControl("lbl_Firm"))).Text != "SULL")
                    {
                        ((Label)(items.FindControl("lbl_contact1"))).Text = ((Label)(items.FindControl("lbl_Firm"))).Text;
                        ((Label)(items.FindControl("lbl_contact1"))).Font.Bold = true;
                        ((Label)(items.FindControl("lbl_contact2"))).Visible = false; ;
                        ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                    }
                    // Contact no setting
                    else if
                        (((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("(") &&
                        ((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("(") &&
                        ((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("("))
                    {
                        ((Label)(items.FindControl("lbl_contact1"))).Text = "No contact";
                        ((Label)(items.FindControl("lbl_contact2"))).Visible = false; ;
                        ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                    }
                    else
                    {
                        if (((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact1"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact1"))).Visible = false;
                        }
                        if (((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact2"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact2"))).Visible = false;
                        }
                        if (((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("(") ||
                            ((Label)(items.FindControl("lbl_contact3"))).Text.StartsWith("000"))
                        {
                            ((Label)(items.FindControl("lbl_contact3"))).Visible = false;
                        }
                    }
                    */
                    // Bond flag setting
                    if (((Label)items.FindControl("lbl_BondFlag")).Text == "1")
                    {
                        ((Label)items.FindControl("lbl_Bond")).Visible = true;
                    }
                    if (((Label)items.FindControl("lbl_ActiveClientFlag")).Text == "1")
                    {
                        ((Label)items.FindControl("lbl_ActiveClient")).Visible = true;
                    }
                    else
                    {
                        ((Label)items.FindControl("lbl_QuoteClient")).Visible = true;
                    }

                    /*
                    // Owes setting
                    if (((Label)(items.FindControl("lbl_owes"))).Text.StartsWith("$0") == false)
                    {
                        ((Label)(items.FindControl("lbl_owes"))).Text = "(" + ((Label)(items.FindControl("lbl_owes"))).Text + ")";
                        ((Label)(items.FindControl("lbl_owes"))).Visible = true;
                    }
                     */
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
        
        
        protected void btn_Update_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid(); //filling the grid
                SetUrl();  // setting the grid contants
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }
    }
}

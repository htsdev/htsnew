﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace HTP.Reports
{
    public partial class NoMailFlagRecordsByAddress : System.Web.UI.Page
    {

        #region Variables

        clsENationWebComponents ClsDb = new clsENationWebComponents("Connection String");
        clsLogger clog = new clsLogger();

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lbl_Message.Text = "";
                if (!IsPostBack)
                {
                    //Yasir Kamal 6100 07/02/2009 date range search criteria added.
                    ViewState["date"] = DateTime.Now.Date.ToShortDateString();
                    FillGrid();
                    
                }

                // tahir 4418 07/25/2008 
                // added paging functionality.
                Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
                Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
                Pagingctrl.GridView = gv_Records;
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }



        protected void gv_Records_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Remove")
                {

                    string[] keys = { "@Address","@City", "@Zip" };
                    object[] values = e.CommandArgument.ToString().Split(',');
                    ClsDb.ExecuteSP("USP_HTS_NOMAILFLAG_UNDO_ByAddress", keys, values);

                    FillGrid();
                    lbl_Message.Text = "Record removed from the list";
                }
                else
                {
                    lbl_Message.Text = "";
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // tahir 4418 07/25/2008 
        // added paging functionality.
        protected void Pagingctrl_PageIndexChanged()
        {
            try
            {
                gv_Records.PageIndex = Pagingctrl.PageIndex - 1;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        /// <summary>
        /// handling PageSizeChanged  of pagging control
        /// </summary>
        /// <param name="pageSize"></param>
        void Pagingctrl_PageSizeChanged(int pageSize)
        {
            try
            {
                if (pageSize > 0)
                {
                    gv_Records.PageIndex = 0;
                    gv_Records.PageSize = pageSize;
                    gv_Records.AllowPaging = true;
                }
                else
                    gv_Records.AllowPaging = false;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        // tahir 4418 07/25/2008
        // added paging functionality.
        protected void gv_Records_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gv_Records.PageIndex = e.NewPageIndex;
                FillGrid();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Filling the Grid with records which dont have no mail Flag.
        /// </summary>

        private void FillGrid()
        {
            try
            {

                DataTable dt = ClsDb.Get_DT_BySPArr("USP_HTS_Get_NoMailFlagByAddress");
                if (dt.Rows.Count > 0)
                {
                    GenerateSerialNo(dt);
                    gv_Records.DataSource = dt;
                    lbl_Message.Text = "";
                }
                else
                {
                    lbl_Message.Text = "No Record Found";
                }
                gv_Records.DataBind();
                Pagingctrl.PageCount = gv_Records.PageCount;
                Pagingctrl.PageIndex = gv_Records.PageIndex;
                Pagingctrl.SetPageIndex();
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }

        }

        /// <summary>
        /// Generating Serial Numbers
        /// </summary>
        /// <param name="dtRecords"></param>
        public void GenerateSerialNo(DataTable dtRecords)
        {
            try
            {
                int sno = 1;
                if (!dtRecords.Columns.Contains("sno"))
                    dtRecords.Columns.Add("sno");


                if (dtRecords.Rows.Count >= 1)
                    dtRecords.Rows[0]["sno"] = 1;

                if (dtRecords.Rows.Count >= 2)
                {
                    for (int i = 1; i < dtRecords.Rows.Count; i++)
                    {
                        dtRecords.Rows[i]["sno"] = ++sno;
                    }
                }
            }
            catch (Exception ex)
            {
                lbl_Message.Text = ex.Message;
                clog.ErrorLog(ex.Message, ex.Source, ex.TargetSite.ToString(), ex.StackTrace);
            }
        }



        #endregion

    }
}

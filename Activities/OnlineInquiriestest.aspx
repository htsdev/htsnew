﻿<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %><%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%@ Register TagName="PolmControl" TagPrefix="uc1" Src="~/WebControls/PolmControl.ascx" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineInquiriestest.aspx.cs"
    Inherits="HTP.Activities.OnlineInquiries" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Leads</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->






    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <style type="text/css">
        .Labelfrmmain, table.table td span.form-label {
            font-size: 14px !important;
        }

        .Labelfrmmain, table.table td span.form-label-blue {
            font-size: 14px !important;
            color: #11a2cf !important;
        }

        .clssubhead {
            /* font-weight: bold; */
            font-size: 14px !important;
            color: #3366cc !important;
            font-weight:normal !important;
            text-decoration: none !important;;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            $('.input-group-addon').click(function () {
                $(this).closest('div').find('input').focus();
            });

            $('.datepicker').datepicker({ autoclose: true });
        }
        $('.datepicker').datepicker({ autoclose: true });

        function checkradio() {

            if (document.getElementById("rb_report_0").checked == true) {
                document.getElementById("tr_image").style.display = 'none';
                document.getElementById("tr_image2").style.display = 'block';
            }
            if (document.getElementById("rb_report_1").checked == true) {
                document.getElementById("tr_image2").style.display = 'none';
                document.getElementById("tr_image").style.display = 'block';
            }

        }

        function checkcomments(type) {
            var datelenght;
            if (type == 1) {
                var comm = document.getElementById("txt_onlinecomment").value;
                var comm_lenght = document.getElementById("txt_onlinecomment").value.length;
                var oldcomm_lenght = document.getElementById("hf_onlineoldcomments").value.length;
            }
            else if (type == 0) {
                var comm = document.getElementById("txt_comment").value;
                var comm_lenght = document.getElementById("txt_comment").value.length;
                var oldcomm_lenght = document.getElementById("hf_oldcomments").value.length;
            }
            else if (type == 2) {
                var comm = document.getElementById("tbContactRepComments").value;
                var comm_lenght = document.getElementById("tbContactRepComments").value.length;
                var oldcomm_lenght = document.getElementById("hfContactOldComments").value.length;
            }

            if (comm_lenght == 0) {
                //alert("Please Enter Comments")
                $("#txtErrorMessage").text("Please Enter Comments");
                $("#errorAlert").modal();
                return false;
            }

            if (CheckName(comm) == false) {
                // alert("Please enter valid comments.");
                $("#txtErrorMessage").text("Please Enter Comments");
                $("#errorAlert").modal();
                return false;
            }
            if (comm_lenght > 0)
                dateLenght = 27
            else
                dateLenght = 0

            if ((oldcomm_lenght + comm_lenght) > 5000 - dateLenght)//-27 bcoz to show last time partconcatenated with comments
            {
                //alert("Sorry You cannot type in more than 5000 characters in contact comments")
                $("#txtErrorMessage").text("Sorry You cannot type in more than 5000 characters in contact comments");
                $("#errorAlert").modal();
                return false;
            }

            //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
            return CheckDate();
        }

        function CheckName(name) {
            for (i = 0 ; i < name.length ; i++) {
                var asciicode = name.charCodeAt(i)
                //If not valid alphabet 
                if ((asciicode == 60) || (asciicode == 62))
                    return false;
            }
        }

        function showHideDateRange(chkPending) {
            var dateRange = document.getElementById("tblContactUs");

            if (document.getElementById("rb_report_1").checked) {
                dateRange.style.display = "block";
                document.getElementById("cal_EffectiveFrom").disabled = true;

                if (chkPending == 1)
                    document.getElementById("dd_callback").value = "0";
            }
            else {
                dateRange.style.display = "none";
                document.getElementById("cal_EffectiveFrom").disabled = false;
            }
        }

        //Yasir Kamal 7372 02/12/2010 allow user to set follow Up date.
        function DateDiff(date1, date2) {
            var one_day = 1000 * 60 * 60 * 24;
            var objDate1 = new Date(date1);
            var objDate2 = new Date(date2);
            return (objDate1.getTime() - objDate2.getTime()) / one_day;
        }

        function CheckDate() {
            today = new Date();
            seldate = document.getElementById("calfollowupdatenew").value;
            var diff = Math.ceil(DateDiff(seldate, today));
            var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
            var newseldatestr = formatDate((Date.parseInvariant(seldate).getMonth() + 1) + "/" + Date.parseInvariant(seldate).getDate() + "/" + Date.parseInvariant(seldate).getFullYear(), "MM/dd/yyyy");
            newseldate = Date.parseInvariant(newseldatestr, "MM/dd/yyyy");
            var callBackddl = document.getElementById("ddl_onlinecallback").selectedIndex;

            if (weekday[newseldate.getDay()] == "Saturday" || weekday[newseldate.getDay()] == "Sunday") {
                //alert("Please select any business day");
                $("#txtErrorMessage").text("Please select any business day");
                $("#errorAlert").modal();

                return false;
            }

            if (diff <= 0 && (callBackddl == 0 || callBackddl == 2 || callBackddl == 3)) {
                //alert("Please select future date");
                $("#txtErrorMessage").text("Please select future date");
                $("#errorAlert").modal();
                return false;
            }

        }

        // Noufil 6766 02/10/2010 Hide modal popup
        function HideModalPopup(popupId) {
            debugger;
            //            document.getElementById('polmControl1_dd_QuickLegalDescription').value = "-1";
            //            document.getElementById('polmControl1_dd_damagevalue').value = "-1";
            //            document.getElementById('polmControl1_dd_Bodilydamage').value = "-1";
            //            document.getElementById('polmControl1_dd_attorney').value = "-1";
            //            try { document.getElementById('polmControl1_dd_Division').value = "1"; } catch(e){}
            //            document.getElementById('polmControl1_TxtLegalMatterDescription').value = ""; 
            //            document.getElementById('polmControl1_lblMessage').innerHTML = "";            
            //            document.getElementById('polmControl1_td_buttons').style.display = "";
            var modalPopupBehavior = $find(popupId);
            modalPopupBehavior.hide();
            return false;
        }

    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1 {
            font-size: 8pt;
            color: #123160;
            font-family: Tahoma;
            width: 40%;
            padding-left: 5px;
            background-color: #EFF4FB;
        }

        .clsInputadministration {
            width: 82.8% !important;
            margin-left: 8px !important;
            /*height :60px;*/
        }

        /*.clsLeftPaddingTable {
            color: #555555 !important;
            font-weight: 400 !important;
            font-size: 14px !important;
            line-height: 23px !important;
            font-family: 'Open Sans', Arial, Helvetica !important;
        }*/
        .clsLeftPaddingTable {
            background-color: #fff !important;
        }

        select#polmControl1_dd_QuickLegalDescription {
            width: 90% !important;
            margin-left: 11px !important;
        }

        #polmControl1_dd_QuickLegalDescription {
            width: 35% !important;
        }

        #polmControl1_dd_QuickLegalDescription {
            width: 99% !important;
        }

        .clsLeftPaddingTable > tbody > tr > td > table > tbody tr > td {
            width: 100% !important;
        }

        .clsLeftPaddingTable > tbody > tr > td > table {
            width: 100% !important;
        }

            .clsLeftPaddingTable > tbody > tr > td > table > tbody tr > td textarea {
                width: 82.8% !important;
                margin-left: 11px !important;
            }

        .clsLeftPaddingTable > tbody > tr:nth-child(7) {
            height: 75px !important;
        }

        .clsLeftPaddingTable > tbody > tr:nth-child(9) {
            height: 75px !important;
        }

        #polmControl1_dd_Division {
            margin-left: 12px !important;
            width: 88% !important;
        }

        .clsLeftPaddingTable > tbody > tr:nth-child(2) > td {
            background-image: url();
            /*background:#11a2cf;*/
            border: 1px solid #11a2cf;
            height: .1px !important;
        }

        #polmControl1_btn_save {
            border-color: #11a2cf !important;
        }
    </style>
</head>
<body>
    <form id="Form2" method="post" runat="server">
         <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
        <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
       
       


        <div class="page-container row-fluid container-fluid">
            

            <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="" style="">
            <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server" CssClass="Label" ForeColor="Red" EnableViewState="False"></asp:Label>
                 </div>
            
                </div>
        

            <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Leads</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>
             <div class="clearfix"></div>
             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Leads</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>

                 <div class="content-body">
                <div class="row">

                   <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Start Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="calStartDate" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                                   <%--  <ew:CalendarPopup ID="calStartDate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                            CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                            ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                            ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Text=" " Width="80px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>

                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">End Date:</label>

                                <span class="desc"></span>
                                <div class="controls">
                                     <picker:datepicker id="calenddate"  runat="server" Dateformat="mm/dd/yyyy" Enabled="true"></picker:datepicker>
                                 <%--    <ew:CalendarPopup ID="calenddate" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
                                            CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)"
                                            ShowClearDate="True" UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True"
                                            ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt" ImageUrl="../images/calendar.gif"
                                            Text=" " Width="80px">
                                            <SelectedDateStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <HolidayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <OffMonthStyle BackColor="AntiqueWhite" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Gray" />
                                            <TextboxLabelStyle CssClass="clstextarea" />
                                            <MonthHeaderStyle BackColor="Yellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                            <WeekdayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <GoToTodayStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <ClearDateStyle BackColor="White" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <WeekendStyle BackColor="LightGray" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <DayHeaderStyle BackColor="Orange" Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small"
                                                ForeColor="Black" />
                                            <TodayDayStyle BackColor="LightGoldenrodYellow" Font-Names="Verdana,Helvetica,Tahoma,Arial"
                                                Font-Size="XX-Small" ForeColor="Black" />
                                        </ew:CalendarPopup>--%>
                                    </div>
                                                                </div>
                         </div>

                       <div class="col-md-3">
                                                            <div class="form-group">
                              <%--<label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:CheckBox ID="cb_showall" runat="server" Text="All Dates" CssClass="checkbox-custom"
                                            Checked="true" />
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
                 <span class="desc"></span>
                                <div class="controls">
                                    <asp:CheckBox ID="cb_AllFollowUpDate" runat="server" Text="All Follow-Up Dates" CssClass="checkbox-custom"
                                            Checked="false" />
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>


                      <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Practice Area:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="ddl_practiceArea" runat="server" CssClass="form-control" Width="">
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="col-md-6">
                                                            <div class="form-group">
                              <label class="form-label">Call Back Status:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <asp:DropDownList ID="dd_callback" runat="server" CssClass="form-control"
                                            Width="">
                                            <asp:ListItem Text="Active" Value="1" Selected="True" Enabled="true"></asp:ListItem>
                                            <asp:ListItem Text="Close" Value="0" Selected="False" Enabled="true"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>

                    <div class="clearfix"></div>
                     <div class="col-md-12">
                                                            <div class="form-group">
                              <%--<label class="form-label">Call Back Status:</label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                     <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btn_update1_Click1">
                                        </asp:Button>
                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                 </section>
             <aspnew:UpdatePanel ID="upnlResult" runat="server" RenderMode="Inline">
                 <Triggers>
    <aspnew:PostBackTrigger ControlID="DropDownList1" />
    <%--<asp:AsyncPostBackTrigger ControlID="DropDownList1" EventName="SelectedIndexChanged"  />--%>
</Triggers>
            <ContentTemplate>
            

             <div class="clearfix"></div>

             <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Leads</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
                         <div class="row">
                             <div class="col-md-5"></div>
                             <div class="col-md-5" style="padding:6px;">
                           <uc3:PagingControl ID="Pagingctrl" runat="server" />

                                 <asp:DropDownList ID="DropDownList1" AutoPostBack="true" runat="server">
                                     <asp:ListItem>1</asp:ListItem>
                                     <asp:ListItem>2</asp:ListItem>
                                     <asp:ListItem>3</asp:ListItem>
                                    
                                 </asp:DropDownList>

                             </div>
                             
                         </div>

            </header>

                 <div class="content-body">
                <div class="row">

                   <div class="col-md-12">
                                                            <div class="form-group">
                              <label class="form-label">Start Date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                     

                                        </div>

                                    </div>
                                                                </div>
                         </div>

                    </div>
                     </div>
                 </section>

             <div class="clearfix"></div>

            <ajaxToolkit:ModalPopupExtender ID="Modal_gvrecords" runat="server" TargetControlID="Button1"
                                                        PopupControlID="pnl_online" BackgroundCssClass="modalBackground" HideDropDownList="false"
                                                        CancelControlID="btn_onlinecancel">
                  
                                                    </ajaxToolkit:ModalPopupExtender>
               
                                                    <asp:Button ID="btn" runat="server" Style="display: none" />
                                                    <asp:Button ID="Button1" runat="server" Style="display: none" />


                                              <%--<asp:Panel ID="pnl_online" runat="server" Height="100px" Width="420px" Style="display: none;">
                    
                                                      <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                                        <div class="col-sm-10">
                                                          <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                                        </div>
                                                      </div>
                                             </asp:Panel>--%>



            <ajaxToolkit:ModalPopupExtender ID="mpeShowPolm" runat="server" TargetControlID="polmHiddenButton"
                                PopupControlID="pnlPolm" BackgroundCssClass="modalBackground" DropShadow="false">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Button runat="server" ID="polmHiddenButton" Text="More" Style="display: none;" />








                 </ContentTemplate>
        </aspnew:UpdatePanel>


            </section>
                     </section>
        </div>



    </form>
     <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
   <%-- <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>--%>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">

        //document.getElementById("calfollowupdate_div").style.zIndex = 10003;
        //document.getElementById("calfollowupdate_monthYear").style.zIndex = 10004;
        $(document).ready(function () {
            $('#calStartDate_txtDate, #calenddate_txtDate').focusin(function () {
                $('#calStartDate_txtDate, #calenddate_txtDate').show();
            });

            $("#errorAlert").on('shown.bs.modal', function (event) {
                document.getElementById("pnl_online").style.zIndex = 1;
            });
            $("#errorAlert").on('hidden.bs.modal', function (event) {
                document.getElementById("pnl_online").style.zIndex = 10004;
            });

            $('#polmControl1_TxtLegalMatterDescription').removeClass('clsInputadministration').addClass('form-control');
            //$("#pnlPolmli > select").each(function (i, e) {
            //    debugger;;
            //    alert($(this).text())
            //});

        });

    </script>

    <div id="errorAlert" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body" style="min-height: 93px !important; max-height: 162px;">
                    <p id="txtErrorMessage">Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <!-- CORE JS FRAMEWORK - START -->
    <%--<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>--%>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script type="text/javascript">
        $('.datepicker').datepicker({ autoclose: true });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#mainmenu li, #limatter").attr("class", "");
            $("#ulmatter").css("display", "none");
            $("#liactivity").attr("class", "open");
            $("#ulactivity").css("display", "block");
            $("#ActiveMenu1_A18").addClass("active");
            $("#ActiveMenu1_A18 > a li > span:nth-child(2)").addClass("arrow open");
            
        });
    </script>
</body>
</html>

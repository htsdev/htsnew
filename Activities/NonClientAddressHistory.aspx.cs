﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FrameWorkEnation.Components;

namespace lntechNew.Activities
{
    // Rab Nawaz Khan 11431 10/03/2013 Added the History page for non-cliens addresses update.
    /// <summary>
    /// 
    /// </summary>
    public partial class NonClientAddressHistory : System.Web.UI.Page
    {
        private clsENationWebComponents clsdb;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                clsdb = new clsENationWebComponents();
                string[] kyes = {"@recordId"};
                object[] value = {Convert.ToInt32(Request.QueryString["TicketId"])};
                DataTable dt = clsdb.Get_DT_BySPArr("USP_HTP_GetUpdatedAddressesHistory", kyes, value);
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    lbl_FirstName.Text = row["ClientFirstName"].ToString();
                    lbl_LastName.Text = row["ClientLastName"].ToString();
                    gv_AddressUpdateHistory.DataSource = dt;
                    gv_AddressUpdateHistory.DataBind();
                }
                else
                {
                    tr_msg.Style["display"] = "block";
                    lblMessage.Text = @"No Record Found";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                tr_msg.Style["display"] = "block";
                Components.ClientInfo.clsLogger.ErrorLog(ex);
            }
        }
    }
}

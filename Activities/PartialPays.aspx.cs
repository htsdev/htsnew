using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FrameWorkEnation.Components;
using lntechNew.Components.ClientInfo;

namespace lntechNew.Activities
{
	/// <summary>
	/// Summary description for PartialPay
	/// </summary>
	public partial class PartialPays : System.Web.UI.Page
	{
		clsENationWebComponents clsdb = new clsENationWebComponents();
		clsSession cSession = new clsSession();
		clsLogger	clog = new clsLogger();
		int Userid=0;
		int Partialstatus=0;
		int Selectreport=0; //either partialpay or crim partial pay 
		string StrExp="" ;
		string StrAcsDec="" ;		
		string SortNm="" ;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		DataView Dv;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if (cSession.IsValidSession(this.Request)==false)
				{
					Response.Redirect("../frmlogin.aspx",false);
				}
				else //To stop page further execution
				{
					//addedd to hide rep column when criminal selected
					if(ddl_reportselect.SelectedValue=="1")
					{
						dg_partial.Columns[1].Visible=false;
					}
					else
					{
						dg_partial.Columns[1].Visible=true;
					}
			

				
					if(!IsPostBack)
					{
						//Populating Employee Dropdown
						DataSet ds_emp= clsdb.Get_DS_BySP("USP_HTS_Get_Users");
						ddl_emplist.DataSource=ds_emp;
						ddl_emplist.DataTextField= ds_emp.Tables[0].Columns[1].ColumnName;
						ddl_emplist.DataValueField= ds_emp.Tables[0].Columns[0].ColumnName;
						ddl_emplist.DataBind();
						ddl_emplist.Items.Insert(0,"--All--");
						ddl_emplist.Items[0].Value="0";
						ddl_emplist.SelectedValue="0";		
						//For records not having emp name	
						ddl_emplist.Items.Insert(1,"N/A");
						ddl_emplist.Items[1].Value="-1";
						//ddl_emplist.SelectedValue="-1";		
						//Fill Grid for first time
						FillGrid(Partialstatus,Userid,Selectreport);                
					}
					lblMessage.Text="";
				}
			}
			catch(Exception pload)
			{
				lblMessage.Text=pload.Message;
				clog.ErrorLog(pload.Message,pload.Source,pload.TargetSite.ToString(),pload.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_partial.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_partial_PageIndexChanged);
			this.dg_partial.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_partial_SortCommand);
			this.dg_partial.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_partial_ItemDataBound);

		}
		#endregion

		//Method To Fill Grid
		private void FillGrid(int PartialStatus,int UserID,int Selectreport)
		{
			int chk=0;
			try
			{
				if (chk_showschedule.Checked)
				{
					chk=1;
				}
			
				string[] key1  = {"@partialpay","@Uid","@selectreport","@sduedate","@eduedate","@shownonschedule"};
				object[] value1= {PartialStatus,UserID,Selectreport,sduedate.SelectedDate,eduedate.SelectedDate,chk}; 
				DataSet ds_partial= clsdb.Get_DS_BySPArr("USP_HTS_GET_PartialPays",key1,value1);

				Dv = new DataView(ds_partial.Tables[0]);
				dg_partial.DataSource=Dv;
				//
				Session["DS"]=ds_partial;
				Session["Dv_PartialPay"]= Dv;				
				dg_partial.DataBind();
				if(dg_partial.Items.Count<=0)
				{
					lblMessage.Visible=true;
					lblMessage.Text="No Record Found";
				}
			
				BindReport();
				FillPageList();
				SetNavigation();
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}

		}
		//Method To Generate Serial Number
		private void BindReport()
		{
			long sNo=(dg_partial.CurrentPageIndex)*(dg_partial.PageSize);									
			try
			{
				foreach (DataGridItem ItemX in dg_partial.Items) 
				{ 					
					sNo +=1 ;
      
					((HyperLink)(ItemX.FindControl("hlnk_Sno"))).Text= sNo.ToString();			
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text = ex.Message ;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}		
		}

		//Event Fired When Employee Name is Selected 
		protected void ddl_emplist_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg_partial.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			Partialstatus=Convert.ToInt32(ddl_partialstatus.SelectedValue);
			Selectreport=Convert.ToInt32(ddl_reportselect.SelectedValue);
			FillGrid(Partialstatus,Userid,Selectreport);
		}

		

		

		private void dg_partial_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DataRowView drv = (DataRowView) e.Item.DataItem;
			if (drv == null)
				return;

			try
			{
				if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					//In order to redirect to violation fees page
					((HyperLink) e.Item.FindControl("hlnk_Sno")).NavigateUrl = "../ClientInfo/violationfeeold.aspx?search=0&casenumber="+ (int) drv["TicketID_PK"];

					//Eliminating Bracket of telephone number
					string tele1=((Label)(e.Item.FindControl("lbl_contact1"))).Text;
					string tele2=((Label)(e.Item.FindControl("lbl_contact2"))).Text;
					string tele3=((Label)(e.Item.FindControl("lbl_contact3"))).Text;
					if (tele1.StartsWith("("))
					{
						((Label)(e.Item.FindControl("lbl_contact1"))).Visible=false;					
					}
					if (tele2.StartsWith("("))
					{
						((Label)(e.Item.FindControl("lbl_contact2"))).Visible=false;					
					}
					if (tele3.StartsWith("("))
					{
						((Label)(e.Item.FindControl("lbl_contact3"))).Visible=false;					
					}
					//formating Date
					string crtdate=((Label)(e.Item.FindControl("lbl_contactdate"))).Text;
					Label lbl_crtdate=(Label)(e.Item.FindControl("lbl_contactdate"));
					lbl_crtdate.Text= crtdate.Substring(0,crtdate.IndexOf(" "));

					string duedate=((Label)(e.Item.FindControl("lbl_appeardate"))).Text;
					Label lbl_duedate=(Label)(e.Item.FindControl("lbl_appeardate"));
					lbl_duedate.Text= duedate.Substring(0,duedate.IndexOf(" "));
					//Initalize due date set to ""	
					if (lbl_duedate.Text=="1/1/1900")
					{
						lbl_duedate.Text="";
					}				
				
					//Open Comments Popup							
					((LinkButton)e.Item.FindControl("lnkb_clientname")).Attributes.Add("OnClick","return OpenEditWin("+(int)drv["TicketID_PK"]+","+(int)drv["RecID"]+");"); 
				}
			}
			catch(Exception ex)
			{
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
		
		}
		#region Navigation logic
		//----------------------------------Navigational LOGIC-----------------------------------------//
		
		// Procedure for filling page numbers in page number combo........
		private void FillPageList()
		{
			Int16 idx;

			cmbPageNo.Items.Clear();
			for (idx=1; idx<=dg_partial.PageCount;idx++)
			{
				cmbPageNo.Items.Add ("Page - " + idx.ToString());
				cmbPageNo.Items[idx-1].Value=idx.ToString();
			}
		}	

		// Procedure for setting data grid's current  page number on header ........
		private void SetNavigation()
		{
			try
			{			
				// setting current page number
				lblPNo.Text =Convert.ToString(dg_partial.CurrentPageIndex+1);
				lblCurrPage.Visible=true;
				lblPNo.Visible=true;

				// filling combo with page numbers
				lblGoto.Visible=true;
				cmbPageNo.Visible=true;
				//FillPageList();

				cmbPageNo.SelectedIndex =dg_partial.CurrentPageIndex;
			}
			catch (Exception ex)
			{
				lblMessage.Text = ex.Message ;
			}
		}
		//Event fired When page changed from drop down list
		protected void cmbPageNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{				
				dg_partial.CurrentPageIndex= cmbPageNo.SelectedIndex ;		
				Dv=(DataView) Session["Dv_PartialPay"]; 
				if(Session["StrExp"] !=null)
				{
					SortDataGrid(Session["StrExp"].ToString() );
				}
				else
				{
					dg_partial.DataSource=Dv;
					dg_partial.DataBind();	
					BindReport();
					FillPageList();
					SetNavigation();						
				}
			//	Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			//	FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
			}
			catch (Exception ex)
			{
				lblMessage.Text=ex.Message;
			}		
		}
		#endregion

		//Event fired When Page is changed 
		private void dg_partial_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				dg_partial.CurrentPageIndex=e.NewPageIndex;
				Dv=(DataView) Session["Dv_PartialPay"]; 
				
				int b= Dv.Count;
				if(Session["StrExp"] !=null)
				{
					SortDataGrid(Session["StrExp"].ToString() );
				}
				else
				{
					dg_partial.DataSource=Dv;
					dg_partial.DataBind();	
					BindReport();
					FillPageList();
					SetNavigation();
				}
			}
			catch(Exception ex)
			{
				lblMessage.Text=ex.Message;
				clog.ErrorLog(ex.Message,ex.Source,ex.TargetSite.ToString(),ex.StackTrace);
			}
			
			
			//FillGrid(AppSDate.SelectedDate,AppEDate.SelectedDate,ContactSDate.SelectedDate,ContactEDate.SelectedDate,Userid,ddl_callstatus.SelectedValue);
		}
		#region Sort logic
		private void dg_partial_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			SortNm = e.SortExpression;				
			SetAcsDesc(SortNm);
			SortDataGrid(SortNm);			
		}
		//Method to set 'ASC' 'DESC' 
		private void SetAcsDesc(string Val)
		{
			try
			{
				StrExp = Session["StrExp"].ToString();
				StrAcsDec = Session["StrAcsDec"].ToString();
			}
			catch
			{

			}

			if (StrExp == Val)
			{
				if (StrAcsDec == "ASC")
				{
					StrAcsDec = "DESC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
				else 
				{
					StrAcsDec = "ASC";
					Session["StrAcsDec"] = StrAcsDec ;
				}
			}
			else 
			{
				StrExp = Val;
				StrAcsDec = "ASC";
				Session["StrExp"] = StrExp ;
				Session["StrAcsDec"] = StrAcsDec ;				
			}
		}
		//Method to sort Data Grid
		private void SortDataGrid(string Sortnm)
		{
			StrExp=Sortnm;
			Dv=(DataView )Session["Dv_PartialPay"];	
			
			int cc= Dv.Count;
			//if coming from notes screen
			string fromnotes=Convert.ToString(Session["new"]);
			if (fromnotes!="")
			{
				dg_partial.CurrentPageIndex=0;
				Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
				Partialstatus=Convert.ToInt32(ddl_partialstatus.SelectedValue);
				Selectreport=Convert.ToInt32(ddl_reportselect.SelectedValue);
				FillGrid(Partialstatus,Userid,Selectreport);				
				Session["new"]="";
			}
			else
			{
				//Dv = ds.Tables[0].DefaultView;
				Dv.Sort = Sortnm + " " +  Session["StrAcsDec"].ToString();
				dg_partial.DataSource= Dv;
				dg_partial.DataBind();	
				BindReport();	
				FillPageList();
				SetNavigation();
			}
		}

#endregion

		//Event fired When PartialStatus is changed 
		protected void ddl_partialstatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg_partial.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			Partialstatus=Convert.ToInt32(ddl_partialstatus.SelectedValue);
			Selectreport=Convert.ToInt32(ddl_reportselect.SelectedValue);
			FillGrid(Partialstatus,Userid,Selectreport);		
		}
		//Event Fired when Report Type is selected
		protected void ddl_reportselect_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			dg_partial.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			Partialstatus=Convert.ToInt32(ddl_partialstatus.SelectedValue);
			Selectreport=Convert.ToInt32(ddl_reportselect.SelectedValue);
			FillGrid(Partialstatus,Userid,Selectreport);	
		
		}

		protected void btn_update1_Click(object sender, System.EventArgs e)
		{
			dg_partial.CurrentPageIndex=0;
			Userid=Convert.ToInt32(ddl_emplist.SelectedValue);
			Partialstatus=Convert.ToInt32(ddl_partialstatus.SelectedValue);
			Selectreport=Convert.ToInt32(ddl_reportselect.SelectedValue);
			FillGrid(Partialstatus,Userid,Selectreport);	
		
		}

		

		
		

		

		
		
	}
}

<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" Codebehind="PartialPays.aspx.cs" AutoEventWireup="True" Inherits="lntechNew.Activities.PartialPays" smartNavigation="False"%>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Partial Pays</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<script>
			// Open CallBackNotes Popup
	      function OpenEditWin(ticketid,RecID)
	      {
	       
	          var PDFWin
		      PDFWin = window.open("PartialPayNotes.aspx?casenumber="+ticketid+"&RecID="+RecID,"","fullscreen=no,toolbar=no,width=380,height=320,left=120,top=100,status=no,menubar=no,scrollbars=yes,resizable=yes");				
		      return false;				      
		      //&"RecID="+RecID
	       } 
	     //   Mouse hover func
	        function ChangeBGColor(pCell, pMouseStatus)
			{
			if (pMouseStatus == 'in')
			pCell.style.backgroundColor = '#FFF2D9';
			else
			pCell.style.backgroundColor = '#EFF4FB';				
			}
			
			
	  
		</script>

        <link href="../Styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" style="Z-INDEX: 101" cellSpacing="0" cellPadding="0" width="780"
				align="center" border="0">
				<tbody>
					<tr>
						<td style="HEIGHT: 14px; width: 827px;">
							<uc1:ActiveMenu id="ActiveMenu1" runat="server"></uc1:ActiveMenu></td>
					</tr>
					<TR>
						<TD>
							<TABLE id="TableSub" cellSpacing="0" cellPadding="0" border="0" style="width: 100%">
								<!--<tr vAlign="middle">
									<td><IMG height="18" src="../Images/head_icon.gif" width="25">&nbsp; <STRONG><font color="#3366cc">
												Partial Pays&nbsp; </font></STRONG>
									</td>
								</tr>-->
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<tr>
									<td>
										<table id="TableSearchCriteria" cellSpacing="0" cellPadding="0" align="center"
											border="0" style="width: 100%">
											<tr>
												<TD vAlign="top" height="29" style="HEIGHT: 29px; width: 136px;">&nbsp;<asp:dropdownlist id="ddl_partialstatus" runat="server" CssClass="clsinputcombo" AutoPostBack="True" onselectedindexchanged="ddl_partialstatus_SelectedIndexChanged" Width="119px">
														<asp:ListItem Value="0" Selected="True">Partial Pays - All</asp:ListItem>
														<asp:ListItem Value="1">Partial Pays - Active</asp:ListItem>
														<asp:ListItem Value="2">Partial Pays - InActive</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD vAlign="top" class="clssubhead" height="29" style="HEIGHT: 29px">&nbsp;Representative</TD>
												<TD vAlign="top" align="left" height="29" style="WIDTH: 81px; HEIGHT: 29px">
													<asp:dropdownlist id="ddl_emplist" runat="server" CssClass="clsinputcombo" AutoPostBack="True" onselectedindexchanged="ddl_emplist_SelectedIndexChanged"></asp:dropdownlist></TD>
												<TD vAlign="top" align="left" height="29" class="clssubhead" style="WIDTH: 71px; HEIGHT: 29px">Report 
													Type</TD>
												<TD vAlign="top" align="right" height="29">
													<asp:dropdownlist id="ddl_reportselect" runat="server" AutoPostBack="True" CssClass="clsinputcombo" onselectedindexchanged="ddl_reportselect_SelectedIndexChanged" Width="100px">
														<asp:ListItem Value="0" Selected="True">Partial Pays</asp:ListItem>
														<asp:ListItem Value="1">Partial Pays Criminal</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD vAlign="top" align="right" height="29" style="HEIGHT: 29px">
													<asp:label id="lblCurrPage" runat="server" CssClass="cmdlinks" Width="83px" Font-Size="Smaller"
														ForeColor="#3366cc" Height="8px" Font-Bold="True">Current Page :</asp:label>
													<asp:label id="lblPNo" runat="server" CssClass="cmdlinks" Width="9px" Font-Size="Smaller" ForeColor="#3366cc"
														Height="10px" Font-Bold="True">a</asp:label>
													<asp:label id="lblGoto" runat="server" CssClass="cmdlinks" Width="16px" Font-Size="Smaller"
														ForeColor="#3366cc" Height="7px" Font-Bold="True">Goto</asp:label><br />
                                                    &nbsp;<asp:dropdownlist id="cmbPageNo" runat="server" AutoPostBack="True" CssClass="clinputcombo" Font-Size="Smaller"
														ForeColor="#3366cc" Font-Bold="True" onselectedindexchanged="cmbPageNo_SelectedIndexChanged"></asp:dropdownlist></TD>
											</tr>
											<TR>
												<TD vAlign="top" align="left" colSpan="4" class="clssubhead" height="25">&nbsp;Due 
													Date
													<ew:calendarpopup id="sduedate" runat="server" Font-Size="8pt" Width="90px" Font-Names="Tahoma"
														ToolTip="Select Report Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
														Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
														ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif" SelectedDate="2006-05-26">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>&nbsp;-
													<ew:calendarpopup id="eduedate" runat="server" Font-Size="8pt" Width="90px" Font-Names="Tahoma"
														ToolTip="Select Report Date Range" PadSingleDigits="True" UpperBoundDate="12/31/9999 23:59:00"
														Culture="(Default)" AllowArbitraryText="False" ShowGoToToday="True" CalendarLocation="Bottom"
														ControlDisplay="TextBoxImage" ImageUrl="../images/calendar.gif" SelectedDate="2006-05-26">
														<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
														<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></WeekdayStyle>
														<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></MonthHeaderStyle>
														<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
															BackColor="AntiqueWhite"></OffMonthStyle>
														<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></GoToTodayStyle>
														<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGoldenrodYellow"></TodayDayStyle>
														<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Orange"></DayHeaderStyle>
														<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="LightGray"></WeekendStyle>
														<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="Yellow"></SelectedDateStyle>
														<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></ClearDateStyle>
														<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
															BackColor="White"></HolidayStyle>
													</ew:calendarpopup>&nbsp;
													<asp:CheckBox id="chk_showschedule" runat="server" CssClass="clslabelnew" Text="Non Scheduled Payments"></asp:CheckBox></TD>
												<TD vAlign="top" align="center" height="25"></TD>
												<TD vAlign="top" align="left" height="25">
													<asp:button id="btn_update1" runat="server" CssClass="clsbutton" Text="Submit" onclick="btn_update1_Click"></asp:button></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 436px" vAlign="top" align="center" colSpan="4"><asp:label id="lblMessage" runat="server" ForeColor="Red" Font-Size="X-Small" Visible="False"
														Font-Names="Verdana"></asp:label></TD>
												<TD vAlign="top" align="center"></TD>
												<TD vAlign="top" align="center"></TD>
											</TR>
										</table>
									</td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif" colSpan="7" height="11"></td>
								</tr>
								<tr>
									<td vAlign="top" align="center">
										<table id="TblGrid" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td><asp:datagrid id="dg_partial" runat="server" CssClass="clsLeftPaddingTable" Width="100%" AllowSorting="True"
														BorderColor="White" BackColor="#EFF4FB" AutoGenerateColumns="False" PageSize="20" AllowPaging="True">
														<Columns>
															<asp:TemplateColumn HeaderText="S.No.">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:HyperLink id="hlnk_Sno" runat="server">HyperLink</asp:HyperLink>
																	<asp:Label id=lbl_ticketid runat="server" CssClass="label" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ticketid_pk") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Representative">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_Username runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"empName") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Client">
																<HeaderStyle HorizontalAlign="Center" Width="25%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id=lnkb_clientname runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ClientName") %>'>
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Phone Number">
																<HeaderStyle HorizontalAlign="Center" Width="23%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<TABLE class="clsleftpaddingtable" id="tbl_contacts" cellSpacing="1" cellPadding="1" width="100%"
																		border="0">
																		<TR>
																			<TD>
																				<asp:Label id=lbl_contact1 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contact1") %>'>
																				</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD>
																				<asp:Label id=lbl_contact2 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contact2") %>'>
																				</asp:Label></TD>
																		</TR>
																		<TR>
																			<TD>
																				<asp:Label id=lbl_contact3 runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"contact3") %>'>
																				</asp:Label></TD>
																		</TR>
																	</TABLE>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Payment Comments">
																<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_comments runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem, "PmytComments") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="currentdateset" HeaderText="Crt">
																<HeaderStyle HorizontalAlign="Center" Width="12%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_contactdate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"currentdateset") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Amt">
																<HeaderStyle HorizontalAlign="Center" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_quote runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.amount", "{0:C0}") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn SortExpression="DueDate" HeaderText="Due">
																<HeaderStyle HorizontalAlign="Center" Width="12%" CssClass="clsaspcolumnheader" VerticalAlign="Top"></HeaderStyle>
																<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id=lbl_appeardate runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"DueDate") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
														<PagerStyle NextPageText=" Next &gt;" PrevPageText="  &lt; Previous        " HorizontalAlign="Center"></PagerStyle>
													</asp:datagrid></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td background="../../images/separator_repeat.gif"  height="11"></td>
								</tr>
								<TR>
									<TD ><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</tbody>
			</TABLE>
		</form>
	</body>
</HTML>

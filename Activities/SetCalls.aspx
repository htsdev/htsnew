﻿<%@ Page Language="c#" AutoEventWireup="true" Inherits="HTP.Activities.SetCalls"
    CodeBehind="SetCalls.aspx.cs" %>

<%@ Register TagName="datepicker" TagPrefix="picker" Src="~/WebControls/CalendarControl.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%--Yasir Kamal 5427 01/31/2009 Pagging functionality Added.--%>
<%@ Register Src="../WebControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc3" %>
<%--Yasir Kamal 5427 end--%>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Set Call Alert</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />

    <script src="../Scripts/Dates.js" type="text/javascript"></script>

    <script src="../Scripts/jsDate.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />


    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->

    <script language="javascript" type="text/javascript">
        // Agha Usman Ahmed for Bug Id 3882 04/28/2008
    
        function refreshContent(){
            //document.Form2.button_name.click();
            document.getElementById("tdData").style.display = 'none';
            //Yasir Kamal 5427 02/06/2009 Pagging Added.
            //document.getElementById("tdWait").style.display = 'none';
            //Yasir Kamal 5427 end.
            __doPostBack('lbRefresh','');
        }
         
        //Agha Usman Ahmed for Task Id 3870 05/01/2008
        function performCheck(check)
        {
            var dt =new Date(); 
            var datestring;         
            var checkbox = document.getElementById("cb_showall");
            //Sabir 4272. set true value for bond and reguler checkbox 
            var chkbond=document.getElementById("chkBondClient");
            var chkReguler=document.getElementById("chkRegulerClient");
            checkbox.checked = check;
            chkbond.checked=true;
            chkbond.disabled=false;   // Add 3977 Zahoor        
            chkReguler.checked=true;
            chkReguler.disabled=false;
            document.getElementById("cal_EffectiveFrom");
            //Sabir 4272. set three days back date for set call option and current date for reminder call option.        
            //ozair 4783 10/16/2008 date undefined issue resolved;  also refactor the code
            if(check)
            {
                dt.setDate(dt.getDate()-3);        
            }       
            dt.setMonth(dt.getMonth());        
            var dd=dt.getDate();
            if(dd<10)
                dd="0"+dd;
            var mm=dt.getMonth()+1;
            if(mm<10)
                mm="0"+mm;
            var yyyy=dt.getFullYear();
        
            datestring= mm + "/" + dd + "/" + yyyy;       
            document.getElementById("cal_EffectiveFrom").value=datestring;
        
            //End here                
        
        }
    
        // Added Zahoor 3977 08/11/2008
        // Description: for Setting FTA Activity report
        function FTACheckSettings(perform)
        {        
            var dt =new Date(); 
            var datestring;         
            var checkbox = document.getElementById("cb_showall");        
            var chkbond=document.getElementById("chkBondClient");
            var chkReguler=document.getElementById("chkRegulerClient");
            checkbox.checked = perform;
            chkbond.checked=false;
            chkbond.disabled=true;
            chkReguler.checked=true;
            chkReguler.disabled=true;
            //ozair 4783 10/16/2008 date undefined issue resolved;  also refactor the code
            dt.setDate(dt.getDate()-2);
            dt.setMonth(dt.getMonth());        
            var dd=dt.getDate();
            if(dd<10)
                dd="0"+dd;
            var mm=dt.getMonth()+1;
            if(mm<10)
                mm="0"+mm;            
            var yyyy=dt.getFullYear();
        
            datestring= mm + "/" + dd + "/" + yyyy;       
            document.getElementById("cal_EffectiveFrom").value=datestring;             
        }
        // ended 3977 Zahoor.
    
        //Sabir 4272. checked reguler client check box if bond client check box is unchecked and vice versa
        function IsCheck(check)
        {
            var chkbond=document.getElementById("chkBondClient");
            var chkReguler=document.getElementById("chkRegulerClient");
            // Added Zahoor 3977
            var cmbbox = document.Form2.rb_calls;  
       
            //if (cmbbox[2].checked != true)
            //{        
       
            if(chkbond.checked==false && check=="1")        
                chkReguler.checked=true;            
            if(chkReguler.checked==false && check=="2")
                chkbond.checked=true;
            
            //}        
            // End 3977
        }
    
        //Sabir Khan 4760  09/10/2008  Display alert in case of past date selected.
        function CheckDate(seldate, tbID)
        {
	    
            //	    if(document.getElementById("rb_calls_0").checked==true)
            //        {
            //            today=new Date();
            //            var diff =Math.ceil(DateDiff(seldate, today));
            //	        if (diff < 0)
            //	        {
            //	        alert("Court Date should not be in past for the reminder calls.");
            //	        var datestring;
            //	        //ozair 4837 10/08/2008 month issue resolved
            //	        if(today.getDate() < 10)
            //                datestring=(today.getMonth()+1)  + "/0" + today.getDate() + "/" + today.getFullYear();
            //            if((today.getMonth()+1) < 10)
            //                datestring="0" + (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getFullYear();
            //            if(today.getDate() < 10 && (today.getMonth()+1) < 10)
            //                datestring="0" + (today.getMonth()+1) + "/0" + today.getDate() + "/" + today.getFullYear(); 
            //            //end ozair 4837
            //	        document.getElementById("cal_EffectiveFrom").value=datestring
            //            return false;
            //            }             
            //        }
        }
        //Sabir Khan 4760  09/10/2008  Return Diffrence b/w the selected date and current date.
        function DateDiff(date1, date2)
        {
            var objDate1=new Date(date1);
            var objDate2=new Date(date2);
            return (objDate1.getTime()-objDate2.getTime())/1000;
        }
      
         
        //In order to set focus on clicked record 
        //		function SetFocusOnGrid()
        //		{
        //		//a;
        //			var id= <%=Session["ReminID"]%>;		
        //			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
        //			var grdName = "dg_ReminderCalls";		
        //			var idx=2;			
        //			cnt = cnt+2;
        //			for (idx=2; idx < (cnt); idx++)
        //			{					
        //			var txtbox = grdName+ "__ctl" + idx + "_txt_sno"
        //			var txtfocus=document.getElementById(txtbox);							
        //			var compare=txtfocus.value;
        //			  if (compare==id)
        //			  {
        //			  //a;			  
        //				  txtfocus.focus();
        //				  <%Session["ReminID"] = 1;%>;				 //initializing to 1 
        //				  break;			
        //			  }
        //			}		
        //		}
 
        /// --------------------------------------------------
        /// mainScreen object
        /// ------------------------------------------------ 
        function validation()
        {
            if (document.getElementById("ddl_rStatus").value=="0")
            {
                var r= confirm ("Are You Sure You Want To Active This Flag and Send A Letter To The Client Requested Updated Contact Information");
                if (r==true)
                {                   
                      
                }
                else
                {
                  
                }                 
            }                 
        }          
   
        function DGSelectOrUnselectAll(grdid,obj,objlist)
        { 
            var chk
        
            //this function decides whether to check or uncheck all
            if(obj.checked) 
            {
                DGSelectAll(grdid,objlist) 
            }
            else 
            {
                DGUnselectAll(grdid,objlist) 
            }
        } 
        //---------- 
 
        function DGSelectAll(grdid,objid)
              
        { 
            //.this function is to check all the items
            var chkbox; 
            var i=2; 
            
            chkbox=document.getElementById(grdid + '_ctl0' + i + '_' + objid); 
            while(chkbox!=null){ 
                chkbox.checked=true; 
                i=i+1;
                if ( i < 10 ) chkbox=document.getElementById(grdid +'_ctl0' + i + '_' + objid); 
                else
                    chkbox=document.getElementById(grdid +'_ctl' + i + '_' + objid); }                 
                   
     

        }//-------------- 

        function DGUnselectAll(grdid,objid)
        { 
            //.this function is to uncheckcheck all the items
            var chkbox; 
            var i=2;             
            chkbox=document.getElementById(grdid + '_ctl0' + i + '_' + objid); 
            while(chkbox!=null)
            { 
                chkbox.checked=false; 
                i=i+1; 
                if ( i < 10 ) chkbox=document.getElementById(grdid +'_ctl0' + i + '_' + objid); 
                else
                    chkbox=document.getElementById(grdid +'_ctl' + i + '_' + objid); 
            } 
        }



        function SetFocusOnGrid()
        {
		
            if ( document.getElementById("dg_ReminderCalls")!= null)
            {
		
                var id= <%=Session["ReminID"]%>;		
                var cnt = parseInt(document.getElementById("txt_totalrecords").value);
                var grdName = "dg_ReminderCalls";
                // Yasir Kamal 5427 01/23/2009 Pagging functionality Added.
                var idx=3;			
                cnt = cnt+2;
                for (idx=3; idx < (cnt); idx++)
                {	
                    var txtbox="";
                    if( idx < 10)
                    {				
                        txtbox = grdName+ "_ctl0" + idx + "_txt_sno"
                    }
                    else
                        txtbox = grdName+ "_ctl" + idx + "_txt_sno"    
                    var txtfocus=document.getElementById(txtbox);							
                    var compare=txtfocus.value;
                    if (compare==id)
                    {
                        txtfocus.focus();
                        <%Session["ReminID"] = 1;%>;				 //initializing to 1 
		                break;			
		            }
                }		
            }
            var chkbond=document.getElementById("chkBondClient");        
            var chkReguler=document.getElementById("chkRegulerClient");
            //        var cmbbox = document.Form2.rb_calls;    
            //        //alert ("Before");
            //        if (cmbbox[2].checked == true)
            //        {           
            //            chkbond.disabled = true;
            //            chkbond.checked = false;
            //            chkReguler.disabled = true;
            //            chkReguler.checked = true;            
            //        }
			
        }
    </script>

    <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->
    <style type="text/css">
        .Labelfrmmain, table.table td span.form-label {
            font-size: 14px !important;
        }

        .Labelfrmmain, table.table td span.form-label-blue {
            font-size: 14px !important;
            color: #11a2cf !important;
        }

        .clssubhead {
            /* font-weight: bold; */
            font-size: 14px !important;
            color: #3366cc !important;
            font-weight: normal !important;
            text-decoration: none !important;
            ;
        }

        tr.border_bottom td {
            border-bottom: 1pt solid black;
        }

        td {
            border: 0;
        }

        .customlabel label {
            font-family: 'Open Sans', Arial, Helvetica !important;
            font-weight: normal !important;
            color: #555555 !important;
            font-size: 14px !important;
        }
        #pageinfo table {
            width:auto !important;
            margin-top:8px !important;
        }
        #commentpar a:link:hover {
        background-color:#11a2cf !important;
        color:white !important;
        width:auto !important;
        height :auto !important;
            font-weight: normal !important;
                font-size: 14px !important;

        }
/*#11a2cf*/
    </style>
</head>
<body onload="SetFocusOnGrid();">
    <form id="Form2" method="post" runat="server">
        <aspnew:ScriptManager ID="ScriptManager2" runat="server" />
        <div class="page-container row-fluid container-fluid">
            <asp:Panel ID="pnl" runat="server">
                <uc1:ActiveMenu ID="ActiveMenu2" runat="server"></uc1:ActiveMenu>
            </asp:Panel>
            <section id="main-content" class="" id="TableMain">
           <section class="wrapper main-wrapper row" id="" style="">
            <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissable fade in" style="display:none">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <asp:Label ID="lblMessage" runat="server"  ForeColor="Red" EnableViewState="False"></asp:Label>
                 </div>
            
                </div>
               <div class="col-xs-12">
                    <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Set Call</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>
               </div>
               <div class="clearfix"></div>
               <section class="box" id="" style="">
                     <header class="panel_header">
                     <h2 class="title pull-left">Set Call</h2>
                     <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
            </header>
                   <div class="content-body">
                        <div class="row">
                             <div class="col-md-3">
                                                            <div class="form-group">
                              <label class="form-label">Crt/Set date:</label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <picker:datepicker id="cal_EffectiveFrom" runat="server" Dateformat="mm/dd/yyyy"  Enabled="true"></picker:datepicker>
                                   

                                    </div>
                                                                </div>
                         </div>
                            <div class="col-md-3">
                                                            <div class="form-group">
                             
<label class="form-label" style="color:white;">.</label>
                               
                                <div class="controls">
                                   <asp:CheckBox ID="cb_showall" runat="server" Text="All Dates" CssClass="checkbox-inline customlabel"
                                    Checked="true" />
                                    </div>
                                                                </div>
                         </div>
                            
                       <div class="col-md-3">
                                                            <div class="form-group">
                              <%--<label class="form-label"></label>--%>
                                <span class="desc"></span>
                                <div class="controls">
                                    <label class="form-label">Call status :</label>
                                     
                                    <asp:DropDownList runat="server" ID="ddl_rStatus" CssClass="form-control" OnSelectedIndexChanged="ddl_rStatus_SelectedIndexChanged1">
                                </asp:DropDownList>
                                    </div>
                                                                </div>
                         </div>
                               <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
                                    <label class="form-label">Language :</label>
                                    <div class="controls">
                                   <asp:DropDownList ID="dd_language" class="form-control"  runat="server">
                                    <asp:ListItem Selected="True" Text="All" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="English" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Spanish" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                                    </div>
                                                                </div>
                         </div>
                       <div class="row">

                             <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
                
                                <div class="controls">
                                     <asp:CheckBox ID="chkBondClient" runat="server" CssClass="checkbox-inline customlabel" Text="Bond Client"
                                    Checked="true" />
                                    </div>
                                                                </div>
                         </div>
                            <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
                
                                <div class="controls">
                                      <asp:CheckBox ID="chkRegulerClient" runat="server" CssClass="checkbox-inline customlabel" Text="Regular Client"
                                    Checked="false" />
                                    </div>
                                                                </div>
                         </div>
                             <div class="col-md-3">
                                                            <div class="form-group">
                            <%--  <label class="form-label">Date</label>--%>
               
                                <div class="controls">
                                     <asp:Button ID="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary"></asp:Button>
                                    </div>
                                                                </div>
                         </div>
                        </div>
                        </div>
                   </section>

               <section>

           <aspnew:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
               <ContentTemplate>
                   <div class="clearfix"></div>
                   <section class="box" id="" style="">
                           <header class="panel_header">
                     
                               <div class="row">
                                  <div class="col-md-4"><h2 class="title pull-left">Set Call</h2></div>
                                  <div class="col-md-4 pull-left" id="pageinfo">
                                      <uc3:PagingControl ID="Pagingctrl" runat="server" />

                                  </div>
                                  
                                  <div class="col-md-4">
                                      <div class="actions panel_actions pull-right">
                     
                         <a class="box_toggle fa fa-chevron-down"></a>
                    
                </div>
                                  </div>
                               </div>
                     
                                

            </header>
                         <div class="content-body">
                <div class="row">

                   <div class="col-md-12">
                                                            <div class="form-group">
                                                                 <div class="controls">
                                                                      <div class="table-responsive" data-pattern="priority-columns">
                                                                          <asp:DataGrid AllowPaging="true" HeaderStyle-HorizontalAlign="Left" PageSize="25"
                                            ID="dg_ReminderCalls" runat="server"  AutoGenerateColumns="False"
                                            BorderStyle="Solid" OnPageIndexChanged="dg_ReminderCalls_PageIndexChanged" BorderWidth="0">
                                            <PagerStyle HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                     <div class="row-fluid" style="overflow:hidden;">
                                                                    <div class="row" >
                                                                    <div class="col-md-2">
                                                                         <asp:Label ID="lblSno" runat="server" CssClass="form-label" ForeColor="#123160">S No:</asp:Label>&nbsp;<asp:HyperLink
                                                                        ID="hl_Sno" runat="server" NavigateUrl='<%# "../ClientInfo/ViolationFeeOld.aspx?CaseNumber=" + DataBinder.Eval(Container, "DataItem.ticketid")+"&search=0" %>'
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.SNo") %>'></asp:HyperLink>
                                                                    </div>    
                                                                    <div class="col-md-3">
                                                                         <asp:Label ID="lbl_owes" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.owedamount", "{0:C0}") %>'
                                                                         ForeColor="Red" Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_Firm" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    </div>    
                                                                    <div class="col-md-3"></div>    
                                                                    <div class="col-md-3"></div>    
                                                                    </div>
                                                                    <%--Sabir Khan 5499 02/06/2009 add sno ...--%>
                                                                   
                                                             
                                                               
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <asp:Label ID="Label11" CssClass="form-label" runat="server"  ForeColor="#123160">Name:</asp:Label>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                             <asp:Label ID="lblName" CssClass="form-label" runat="server" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.LastName"),", ",(DataBinder.Eval(Container, "DataItem.FirstName") )) %>'
                                                                        ></asp:Label>
                                                                    <asp:Label ID="lbl_TicketID" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketID") %>'
                                                                         ForeColor="#123160" Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_RID" CssClass="form-label" runat="server"  ForeColor="#123160" Visible="False"></asp:Label>
 
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <p id="commentpar">
                                                                        <asp:Label ID="lbl_contact1" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>'
                                                                            >
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Contact2" CssClass="form-label" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>'
                                                                            >
                                                                        </asp:Label><br>
                                                                        <asp:Label ID="lbl_Contact3" CssClass="form-label" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'
                                                                            >
                                                                        </asp:Label><br> 
                                                                        <asp:Label ID="lbl_Language" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Languagespeak") %>'
                                                                            >
                                                                        </asp:Label><br>
                                                                        <asp:HyperLink CssClass="btn btn-primary" ID="lnk_Comments" runat="server">Add Comments </asp:HyperLink>
                                                                        <asp:Label ID="lbl_phone" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>'
                                                                            Visible="False">
                                                                        </asp:Label>
                                                                        <asp:Label ID="lbl_Fax" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fax") %>'
                                                                            Visible="False">
                                                                        </asp:Label></p>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                             <asp:Label ID="lbl_comments" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Comments") %>'
                                                                            >
                                                                        </asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                             
                                                             
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <asp:Label ID="Label12" CssClass="form-label" runat="server"  ForeColor="#123160">Case Status:</asp:Label>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <asp:Label ID="lbl_Status" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialDesc") %>'
                                                                         ForeColor="#123160" Font-Bold="True">
                                                                    </asp:Label>
                                                                    <asp:Label ID="lbl_TicketViolationID" CssClass="form-label" runat="server"  ForeColor="#123160"
                                                                        Text='<%# DataBinder.Eval(Container, "DataItem.TicketViolationIds") %>' Visible="False"> </asp:Label>
                                                                        </div>
                                                                        <div class="col-md-3"></div>
                                                                        <div class="col-md-3"></div>
                                                                    </div>
                                                                    
                                                              
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <asp:Label ID="Label14" CssClass="form-label" runat="server"  ForeColor="#123160">Call Back:</asp:Label>
                                                                    <asp:TextBox ID="txt_sno" runat="server" Width="1px" ForeColor="#EFF4FB"
                                                                        BackColor="#EFF4FB" BorderColor="#EFF4FB" BorderStyle="Solid"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                             <asp:Label ID="lbl_CallBacks" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                                                             ForeColor="#123160"></asp:Label>
                                                                        <asp:Label ID="lbl_CourtDate" CssClass="form-label" runat="server"  ForeColor="#123160"
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.courtdatemain") %>' Visible="False"> </asp:Label>
                                                                        </div>
                                                                        <div class="col-md-3"></div>
                                                                        <div class="col-md-3"></div>
                                                                    </div>
                                                            
                                                                  
                                                            
                                                                    <div class="row">
                                                                         <div class="col-md-2">
                                                                             <asp:Label ID="Label1" CssClass="form-label" runat="server"  ForeColor="#123160">Dialer Status:</asp:Label>
                                                                         </div>
                                                                         <div class="col-md-3">
                                                                             <asp:Label ID="Label2" CssClass="form-label" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DialerStatus") %>'
                                                                         ForeColor="#123160">
                                                                    </asp:Label>
                                                                         </div>
                                                                         <div class="col-md-3"></div>
                                                                         <div class="col-md-3"></div>
                                                                    </div>
                                                                    
                                                           
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                            <asp:Label CssClass="form-label" ID="lbl_BondFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>'
                                                                        Visible="False">
                                                                    </asp:Label><asp:Label CssClass="form-label" ID="lbl_Bond" runat="server" Visible="False" Font-Bold="True"
                                                                        BackColor="#FFCC66">Bond<span style="background-color:#eff4fb;">&nbsp;</span></asp:Label><asp:Label
                                                                            ID="lblWrongFlag" runat="server" Visible='<%# Convert.ToBoolean(Eval("WrongNumberFlag")) %>'
                                                                            Font-Bold="True" BackColor="#FFCC66">Wrong Telephone Number<span style="background-color:#eff4fb;">&nbsp;</span></asp:Label><asp:Label
                                                                                ID="lbl_Insurance" runat="server" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"
                                                                                Text='<%# DataBinder.Eval(Container, "DataItem.Insurance") %>' Visible="False"></asp:Label><asp:Label
                                                                                    ID="lbl_Child" runat="server" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"
                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.Child") %>' Visible="False"></asp:Label>
                                                                    <asp:HiddenField ID="hf_GeneralComments" runat="server" Value="<%# Bind('GeneralComments') %>" />
                                                                        </div>
                                                                        <div class="col-md-3"></div>
                                                                        <div class="col-md-3"></div>
                                                                        <div class="col-md-3"></div>
                                                                    </div>
                                                                    
                                                  </div>
                                                        <hr style="border-top: 1px solid #11a2cf;"/>
                                                        <div class="clearfix"></div>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Center" NextPageText="&nbsp; Next &gt;" PrevPageText="&lt; Prev &nbsp;" />
                                        </asp:DataGrid>
                                        <asp:LinkButton ID="lbRefresh" runat="server" Style="display: none">Refresh</asp:LinkButton>
                                                                          </div>
                                                                 </div>
                                                                </div>
                         </div>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </div>
                     </div>
                   </section>
               </ContentTemplate>
           </aspnew:UpdatePanel>


        </section>
        </div>
        </section>
        
        
        </div>
    </form>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <%--<script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#mainmenu li, #limatter").attr("class", "");
            $("#ulmatter").css("display", "none");
            $("#liactivity").attr("class", "open");
            $("#ulactivity").css("display", "block");
            $("#ActiveMenu1_A23").addClass("active");
            $("#ActiveMenu1_A23 > a li > span:nth-child(2)").addClass("arrow open");
            
        });
    </script>
</body>
</html>

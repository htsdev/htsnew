<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MissedCourtReport.aspx.cs" Inherits="lntechNew.Activities.MissedCourtReport" %>
<%@ Register Src="../WebControls/ActiveMenu.ascx" TagName="ActiveMenu" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Missed Court Report</title>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../Styles.css" type="text/css" rel="stylesheet" />    
    <script language="javascript">
    function SetFocusOnGrid()
		{
		//a;
			var id= <%=Session["ReminID"]%>;		
			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
			var grdName = "dg_MissedCourt";		
			var idx=2;			
			cnt = cnt+2;
			for (idx=2; idx < (cnt); idx++)
			{	
			  
			    var txtbox="";
			    if( idx < 10)
			    {				
			        txtbox = grdName+ "$ctl0" + idx + "$txt_sno";
			    }
			    else
			    {
			        txtbox = grdName+ "$ctl" + idx + "$txt_sno"; 
			    }
			    var txtfocus=document.getElementById(txtbox);							
			    var compare=txtfocus.value;
			    if (compare==id)
			    {
			        //a;			  
			        txtfocus.focus();
				    <%Session["ReminID"]=1;%>;				 //initializing to 1 
				    break;			
			    }
			}		
		}
    </script>

    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles.css" rel="stylesheet" type="text/css" />
</head>
<body onload="SetFocusOnGrid();">
    <form id="form1" runat="server">
    <div>
        <table id="TableMain" align="center" border="0" cellpadding="0" cellspacing="0" width="780">
            <tr>
                <td>
                    &nbsp;<uc1:ActiveMenu ID="ActiveMenu1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table id="TableSub" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" height="9" style="height: 9px"
                                width="780">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 9px">
                    From :
                    <ew:calendarpopup id="cal_EffectiveFrom" runat="server" allowarbitrarytext="False"
                        calendarlocation="Bottom" controldisplay="TextBoxImage" culture="(Default)" enablehidedropdown="True"
                        font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" padsingledigits="True"
                        showcleardate="True" showgototoday="True" text=" " tooltip="Call Back Date" upperbounddate="12/31/9999 23:59:00"
                        width="80px">
                        <TEXTBOXLABELSTYLE CssClass="clstextarea" />
                        <WEEKDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <MONTHHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow" ForeColor="Black" />
                        <OFFMONTHSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="AntiqueWhite" ForeColor="Gray" />
                        <GOTOTODAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <TODAYDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGoldenrodYellow" ForeColor="Black" />
                        <DAYHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Orange" ForeColor="Black" />
                        <WEEKENDSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGray" ForeColor="Black" />
                        <SELECTEDDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow" ForeColor="Black" />
                        <CLEARDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <HOLIDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                    </ew:CalendarPopup>
                    &nbsp;To : &nbsp;<ew:calendarpopup id="cal_EffectiveTo" runat="server" allowarbitrarytext="False"
                        calendarlocation="Bottom" controldisplay="TextBoxImage" culture="(Default)" enablehidedropdown="True"
                        font-names="Tahoma" font-size="8pt" imageurl="../images/calendar.gif" padsingledigits="True"
                        showcleardate="True" showgototoday="True" text=" " tooltip="Call Back Date" upperbounddate="12/31/9999 23:59:00"
                        width="80px">
                        <SELECTEDDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow" ForeColor="Black" />
                        <WEEKENDSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGray" ForeColor="Black" />
                        <GOTOTODAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <DAYHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Orange" ForeColor="Black" />
                        <TEXTBOXLABELSTYLE CssClass="clstextarea" />
                        <MONTHHEADERSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="Yellow" ForeColor="Black" />
                        <WEEKDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <HOLIDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <OFFMONTHSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="AntiqueWhite" ForeColor="Gray" />
                        <CLEARDATESTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="White" ForeColor="Black" />
                        <TODAYDAYSTYLE Font-Names="Verdana,Helvetica,Tahoma,Arial" Font-Size="XX-Small" BackColor="LightGoldenrodYellow" ForeColor="Black" />
                    </ew:CalendarPopup>
                    &nbsp;Client Status :
                    <asp:DropDownList ID="ddl_ClientStatus" runat="server" CssClass="clsInputCombo">
                        <asp:ListItem Value="0">All Clients</asp:ListItem>
                        <asp:ListItem Value="1">Quote Clients</asp:ListItem>
                        <asp:ListItem Value="2">Active Clients</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                    <asp:Button ID="btn_Update" runat="server" CssClass="clsbutton" Text="Submit" OnClick="btn_Update_Click" /></td>
            </tr>
            <tr>
                <td style="height: 13px">
                    <asp:Label ID="lblMessage" runat="server" CssClass="label" EnableViewState="False"
                        ForeColor="Red" Width="320px"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    <table id="TableGrid" bgcolor="white" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr>
                            <td background="../../images/separator_repeat.gif" colspan="5" height="11" style="height: 11px"
                                width="780">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <asp:DataGrid ID="dg_MissedCourt" runat="server" AutoGenerateColumns="False" CssClass="clsLeftPaddingTable">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="1" class="clsleftpaddingtable"
                                                    width="780">
                                                    <tr>
                                                        <td colspan="5" rowspan="1" style="height: 16px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#eff4fb" class="clsLeftPaddingTable" valign="middle" style="height: 20px; width: 105px;" align="right">
                                                            <asp:Label ID="Label11" runat="server" CssClass="label" ForeColor="#123160">Name:</asp:Label></td>
                                                        <td bgcolor="#eff4fb" class="clsLeftPaddingTable" colspan="2" style="height: 20px" align="left">
                                                        <asp:HyperLink ID="lnkName" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:HyperLink>
                                                            <asp:Label ID="lbl_TicketID" runat="server" CssClass="label" ForeColor="#123160"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.TicketID_PK") %>' Visible="False"></asp:Label>
                                                            <asp:Label ID="lbl_RID" runat="server" CssClass="label" ForeColor="#123160" Visible="False"></asp:Label></td>
                                                        <td bgcolor="#eff4fb" height="20" rowspan="4" valign="top" style="width: 123px" align="left">
                                                            <p>
                                                                <asp:Label ID="lbl_contact1" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Contact1") %>'
                                                                    Width="150px"></asp:Label><br>
                                                                <asp:Label ID="lbl_Contact2" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Contact2") %>'
                                                                    Width="150px"></asp:Label><br>
                                                                <asp:Label ID="lbl_Contact3" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>'
                                                                    Width="150px"></asp:Label><br>
                                                                <asp:Label ID="lbl_Language" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.LanguageSpeak") %>'></asp:Label><br>
                                                                <asp:HyperLink ID="lnk_Comments" runat="server" Visible="False">Add Comments </asp:HyperLink>
                                                        </td>
                                                        <td bgcolor="#eff4fb" height="20" rowspan="4" valign="top" width="205">
                                                            <table border=0 cellpadding=0 cellspacing=0 width=100%>
                                                                <tr>
                                                                    <td height=11>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lbl_ActiveClient" runat="server" BackColor="#FFCC66" Font-Bold="True"
                                                                Visible="False">Active Client</asp:Label>
                                                            <asp:Label ID="lbl_QuoteClient" runat="server" BackColor="#FFCC66" Font-Bold="True"
                                                                Visible="False">Quoted Client</asp:Label>
                                                            <asp:Label ID="lbl_Bond" runat="server" BackColor="#FFCC66" Font-Bold="True" Visible="False">Bond</asp:Label>
                                                            <asp:Label ID="lbl_comments" runat="server" CssClass="label"></asp:Label>
                                                            <asp:Label ID="lbl_BondFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BondFlag") %>'
                                                                Visible="False"></asp:Label>
                                                            <asp:Label ID="lbl_ActiveClientFlag" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Activeflag") %>'
                                                                Visible="False"></asp:Label>
                                                            <asp:TextBox ID="txt_sno" runat="server" BackColor="#EFF4FB" BorderColor="#EFF4FB"
                                                                BorderStyle="Solid" ForeColor="#EFF4FB" Height="1px" Width="25px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#eff4fb" valign="middle" style="height: 20px; width: 105px;" align="right">
                                                            <asp:Label ID="Label12" runat="server" CssClass="label" ForeColor="#123160">Case Status:</asp:Label></td>
                                                        <td bgcolor="#eff4fb" class="clsLeftPaddingTable" colspan="2" width="350" style="height: 20px" align="left">
                                                            <asp:Label ID="lbl_Status" runat="server" CssClass="label" Font-Bold="True" ForeColor="#123160"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.CaseStatus") %>'></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#eff4fb" class="clsLeftPaddingTable" height="20" valign="middle" align="right" style="width: 105px">
                                                            <asp:Label ID="Label13" runat="server" CssClass="label" ForeColor="#123160">Location:</asp:Label></td>
                                                        <td bgcolor="#eff4fb" class="clsLeftPaddingTable" colspan="2" height="20" align="left">
                                                            <asp:Label ID="lbl_Location" runat="server" CssClass="label" ForeColor="#123160"
                                                                Text='<%# DataBinder.Eval(Container, "DataItem.Location") %>'></asp:Label>
                                                            <asp:Label ID="lbl_TicketViolationID" runat="server" CssClass="label" ForeColor="#123160" Visible="False"></asp:Label></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td background="../../images/separator_repeat.gif" colspan="5" height="11" width="780">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                                                         <td style="display: none;">
                                <asp:TextBox ID="txt_totalrecords" runat="server" CssClass="lable" ForeColor="Black"></asp:TextBox></td>
                
                <td>
                    <uc1:footer id="Footer1" runat="server">
                    </uc1:footer></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

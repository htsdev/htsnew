﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using lntechNew.WebControls;
using lntechNew.Components.ClientInfo;
using HTP.Components;

namespace HTP.Activities
{
    public partial class POLM : System.Web.UI.Page
    {

        #region Variables
        clsSession ClsSession = new clsSession();
        clsLogger clog = new clsLogger();
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ClsSession.IsValidSession(this.Request, this.Response, this.Session) == false)
            {
                Response.Redirect("../frmlogin.aspx", false);
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    FillAttorneys();
                    BindData();
                }
            }
            Pagingctrl.PageIndexChanged += new lntechNew.WebControls.PageMethodHandler(Pagingctrl_PageIndexChanged);
            Pagingctrl.PageSizeChanged += new lntechNew.WebControls.PageSizeChangedMethodHandler(Pagingctrl_PageSizeChanged);
            Pagingctrl.GridView = gvPOLM;
        }

        protected void gvPOLM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           
        }

        protected void gvPOLM_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (e.NewPageIndex != -1)
                {
                    gvPOLM.PageIndex = e.NewPageIndex;
                    BindData();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        protected void gvPOLM_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //((LinkButton)e.Row.FindControl("LnkBtnUpdt")).CommandArgument = e.Row.RowIndex.ToString();
                    ((Label)e.Row.FindControl("lbl_Question")).Text = ((Label)e.Row.FindControl("lbl_Question")).Text.Replace("<", "&lt;");
                    ((Label)e.Row.FindControl("lbl_Question")).Text = ((Label)e.Row.FindControl("lbl_Question")).Text.Replace(">", "&gt;");
                    Label lblComments = (Label)e.Row.FindControl("lbl_Question");
                    //pnlFollowup.Style["display"] = "none";
                    HtmlImage aFollowup = (HtmlImage)e.Row.FindControl("aFollowup");
                    LinkButton LnkBtnName=(LinkButton)e.Row.FindControl("LnkBtnName");
                    DataRowView drv=(DataRowView)e.Row.DataItem;

                    //LnkBtnName.Attributes.Add("onClick", "ShowPanel('" + drv["status"] + "," + drv["RefAttor"] + "," + drv["Attor"] + "," + drv["Cat"] + "," + drv["Comments"] + "') ");
                    LnkBtnName.Attributes.Add("onClick", "return ShowPanel('" + drv["calldescription"] + "','" + lblComments.ClientID + "') ");
                    aFollowup.Attributes.Add("onClick", "return ShowFollowUpPanel('" + drv["Name"] + "','" + drv["EmailAddress"] + "','" + drv["Phone"] + "','" + drv["FollowupDate"] + "','" + lblComments.ClientID + "') ");
                }
            }

            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }


        }



        #endregion

        #region Methods

        public void BindData()
        {
            
            DataTable dt = SullolawReports.GetPOLMData(true, new DateTime(2009, 06, 02), new DateTime(2009, 06, 02), 0, 0);
            if (dt.Rows.Count == 0)
            {
                gvPOLM.Visible = false;
                lblMessage.Text = "No Records Found";
                Pagingctrl.Visible = false;
            }
            else
            {
                lblMessage.Text = "";
                if (!dt.Columns.Contains("SNo"))
                {
                    dt.Columns.Add("SNo");
                    int sno = 1;
                    if (dt.Rows.Count >= 1)
                    {
                        dt.Rows[0]["SNo"] = 1;
                    }
                    if (dt.Rows.Count >= 2)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {                            
                                dt.Rows[i]["SNo"] = ++sno;                            
                        }
                    }

                }
                Pagingctrl.Visible = true;
                gvPOLM.Visible = true;
                gvPOLM.DataSource = dt;
                gvPOLM.DataBind();
                Pagingctrl.GridView = gvPOLM;
                Pagingctrl.PageCount = gvPOLM.PageCount;
                Pagingctrl.PageIndex = gvPOLM.PageIndex;
                Pagingctrl.SetPageIndex();

            }

        }

        void Pagingctrl_PageIndexChanged()
        {
            gvPOLM.PageIndex = Pagingctrl.PageIndex - 1;
            BindData();
        }

        void Pagingctrl_PageSizeChanged(int pageSize)
        {

            try
            {

                if (pageSize > 0)
                {
                    gvPOLM.PageIndex = 0;
                    gvPOLM.PageSize = pageSize;
                    gvPOLM.AllowPaging = true;
                }
                else
                {
                    gvPOLM.AllowPaging = false;
                }
                BindData();
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                clsLogger.ErrorLog(ex);
            }
        }

        public void FillAttorneys()
        {
            clsFirms firm = new clsFirms();
            DataTable dtAttorney=firm.GetAllFullFirmName().Tables[0];
            //bind data for DDl Attorney
            ddlAttor.DataValueField = "FirmID";
            ddlAttor.DataTextField = "FirmFullname";
            ddlAttor.DataSource = dtAttorney;
            ddlAttor.DataBind();

            //bind data for DDl Ref Attorney
            ddlRefAttor.DataValueField = "FirmID";
            ddlRefAttor.DataTextField = "FirmFullname";
            ddlRefAttor.DataSource = dtAttorney;
            ddlRefAttor.DataBind();

        }

        #endregion

    }
}

﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="ActiveMenu" Src="../WebControls/ActiveMenu.ascx" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="lntechNew.Activities.ReminderCalls" Codebehind="ReminderCalls.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="../WebControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReminderCalls</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">

         <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon" />
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="../assets/images/apple-touch-icon-57-precomposed.png">
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/images/apple-touch-icon-114-precomposed.png">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/images/apple-touch-icon-72-precomposed.png">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/images/apple-touch-icon-144-precomposed.png">
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - START -->


    <!-- HEADER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <!-- CORE CSS TEMPLATE - END -->

		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
		//In order to set focus on clicked record 
//		function SetFocusOnGrid()
//		{
//		//a;
//			var id= <%=Session["ReminID"]%>;		
//			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
//			var grdName = "dg_ReminderCalls";		
//			var idx=2;			
//			cnt = cnt+2;
//			for (idx=2; idx < (cnt); idx++)
//			{					
//			var txtbox = grdName+ "__ctl" + idx + "_txt_sno"
//			var txtfocus=document.getElementById(txtbox);							
//			var compare=txtfocus.value;
//			  if (compare==id)
//			  {
//			  //a;			  
//				  txtfocus.focus();
//				  <%Session["ReminID"]=1;%>;				 //initializing to 1 
//				  break;			
//			  }
//			}		
//		}
        function SetFocusOnGrid()
		{
		//a;
			var id= <%=Session["ReminID"]%>;		
			var cnt = parseInt(document.getElementById("txt_totalrecords").value);
			var grdName = "dg_ReminderCalls";		
			var idx=2;			
			cnt = cnt+2;
			for (idx=2; idx < (cnt); idx++)
			{	
			var txtbox="";
			    if( idx < 10)
			    {				
			         txtbox = grdName+ "_ctl0" + idx + "_txt_sno"
			    }
			    else
			        txtbox = grdName+ "_ctl" + idx + "_txt_sno"    
			var txtfocus=document.getElementById(txtbox);							
			var compare=txtfocus.value;
			  if (compare==id)
			  {
			  //a;			  
				  txtfocus.focus();
				  <%Session["ReminID"]=1;%>;				 //initializing to 1 
				  break;			
			  }
			}		
		}
		</script>
	</HEAD>
	<body onload="SetFocusOnGrid();" >
		<FORM id="Form2" method="post" runat="server">
             <div class="page-container row-fluid container-fluid">

 <asp:Panel ID="pnl" runat="server">
                        <uc1:ActiveMenu ID="ActiveMenu1" runat="server"></uc1:ActiveMenu>
                    </asp:Panel>
                 <section id="main-content" class="" id="TableMain">
        <section class="wrapper main-wrapper row" id="TableSub" style="">

             <div class="col-xs-12">
        <div class="page-title">

            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">ACTIVITIES --> Reminder Call</h1><!-- PAGE HEADING TAG - END -->                           

            </div>
            </div>

                 </div>

             <div class="clearfix"></div>

            <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                    <ew:calendarpopup id="cal_EffectiveFrom" runat="server" EnableHideDropDown="True" ControlDisplay="TextBoxImage"
							CalendarLocation="Bottom" ShowGoToToday="True" AllowArbitraryText="False" Culture="(Default)" ShowClearDate="True"
							UpperBoundDate="12/31/9999 23:59:00" PadSingleDigits="True" ToolTip="Call Back Date" Font-Names="Tahoma" Font-Size="8pt"
							ImageUrl="../images/calendar.gif" Text=" " Width="80px">
							<TextboxLabelStyle CssClass="clstextarea"></TextboxLabelStyle>
							<WeekdayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></WeekdayStyle>
							<MonthHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Yellow"></MonthHeaderStyle>
							<OffMonthStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Gray"
								BackColor="AntiqueWhite"></OffMonthStyle>
							<GoToTodayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></GoToTodayStyle>
							<TodayDayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="LightGoldenrodYellow"></TodayDayStyle>
							<DayHeaderStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Orange"></DayHeaderStyle>
							<WeekendStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="LightGray"></WeekendStyle>
							<SelectedDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="Yellow"></SelectedDateStyle>
							<ClearDateStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></ClearDateStyle>
							<HolidayStyle Font-Size="XX-Small" Font-Names="Verdana,Helvetica,Tahoma,Arial" ForeColor="Black"
								BackColor="White"></HolidayStyle>
						</ew:calendarpopup>
                                    
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

            <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:dropdownlist id="ddl_rStatus" runat="server" AutoPostBack="True" CssClass="form-control">
						</asp:dropdownlist>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

            <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-4">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   
                                    <asp:button id="btn_update1" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btn_update1_Click1"></asp:button>
                                    <asp:label id="lblMessage" runat="server" Width="320px" CssClass="form-label" ForeColor="Red" EnableViewState="False"></asp:label>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

            <div class="clearfix"></div>

            <section class="box" id="TableGrid" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                     <div class="table-responsive" data-pattern="priority-columns">
                                   
                                     <asp:datagrid id="dg_ReminderCalls" runat="server" CssClass="table table-small-font table-bordered table-striped" AutoGenerateColumns="False">
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<TABLE class="clsleftpaddingtable" id="Table1" cellSpacing="1" cellPadding="0" width="780"
														align="center" border="0">
														<TR>
															<TD colSpan="5" rowSpan="1">
																<asp:Label id=lbl_owes runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.owedamount", "{0:C0}") %>' CssClass="form-label" ForeColor="Red" Visible="False">
																</asp:Label>
																<asp:Label id=lbl_Firm runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirmAbbreviation") %>' Visible="False">
																</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															</TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" vAlign="middle" width="105" bgColor="#eff4fb" height="20">
																<asp:Label id="Label11" runat="server" CssClass="form-label" ForeColor="#123160">Name:</asp:Label></TD>
															<TD class="clsLeftPaddingTable" bgColor="#eff4fb" colSpan="2" height="20">
																<asp:HyperLink id=lnkName runat="server" Text='<%# String.Concat(DataBinder.Eval(Container, "DataItem.LastName"),", ",(DataBinder.Eval(Container, "DataItem.FirstName") )) %>' CssClass="form-label">
																</asp:HyperLink>
																<asp:Label id=lbl_TicketID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ticketID") %>' CssClass="form-label" ForeColor="#123160" Visible="False">
																</asp:Label>
																<asp:Label id="lbl_RID" runat="server" CssClass="form-label" ForeColor="#123160" Visible="False"></asp:Label></TD>
															<TD vAlign="top" width="123" bgColor="#eff4fb" height="20" rowSpan="4">
																<P>
																	<asp:Label id=lbl_contact1 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact1") %>' CssClass="form-label">
																	</asp:Label><BR>
																	<asp:Label id=lbl_Contact2 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.contact2") %>' CssClass="form-label">
																	</asp:Label><BR>
																	<asp:Label id=lbl_Contact3 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.Contact3") %>' CssClass="form-label">
																	</asp:Label><BR>
																	<asp:Label id=lbl_Language runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Languagespeak") %>' CssClass="form-label">
																	</asp:Label><BR>
																	<asp:HyperLink id="lnk_Comments" runat="server">Add Comments </asp:HyperLink>
																	<asp:Label id=lbl_phone runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.phone") %>' Visible="False">
																	</asp:Label>
																	<asp:Label id=lbl_Fax runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fax") %>' Visible="False">
																	</asp:Label></P>
															</TD>
															<TD vAlign="top" width="205" bgColor="#eff4fb" height="20" rowSpan="4">
																<asp:Label id=lbl_comments runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Comments") %>' CssClass="form-label">
																</asp:Label></TD>
														</TR>
														<TR>
															<TD vAlign="middle" width="105" bgColor="#eff4fb" height="20">
																<asp:Label id="Label12" runat="server" CssClass="form-label" ForeColor="#123160">Case Status:</asp:Label></TD>
															<TD class="clsLeftPaddingTable" width="350" bgColor="#eff4fb" colSpan="2" height="20">
																<asp:Label id=lbl_Status runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.trialDesc") %>' CssClass="form-label" ForeColor="#123160" Font-Bold="True">
																</asp:Label></TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" vAlign="middle" bgColor="#eff4fb" height="20">
																<asp:Label id="Label13" runat="server" CssClass="form-label" ForeColor="#123160">Location:</asp:Label></TD>
															<TD class="clsLeftPaddingTable" bgColor="#eff4fb" colSpan="2" height="20">
																<asp:Label id=lbl_Location runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CourtAddress") %>' CssClass="form-label" ForeColor="#123160">
																</asp:Label>
																<asp:Label id=lbl_TicketViolationID runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TicketviolationID") %>' CssClass="form-label" ForeColor="#123160" Visible="False">
																</asp:Label></TD>
														</TR>
														<TR>
															<TD class="clsLeftPaddingTable" vAlign="middle" width="105" bgColor="#eff4fb" height="20">
																<asp:Label id="Label14" runat="server" CssClass="form-label" ForeColor="#123160">Call Back:</asp:Label>
																<asp:TextBox id="txt_sno"  runat="server" Width="1px" ForeColor="#EFF4FB" Height="1px" BackColor="#EFF4FB"
																	BorderColor="#EFF4FB" BorderStyle="Solid"></asp:TextBox></TD>
															<TD class="clsLeftPaddingTable" width="151" bgColor="#eff4fb" height="20">
																<asp:Label id=lbl_CallBacks runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>' CssClass="form-label" ForeColor="#123160"></asp:Label></TD>
															<TD class="clsLeftPaddingTable" align="center" width="125" bgColor="#eff4fb" height="20">
																<DIV align="center"></DIV>
																<asp:Label id=lbl_BondFlag runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bondflag") %>' Visible="False">
																</asp:Label><BR>
																<asp:Label id="lbl_Bond" runat="server" Visible="False" Font-Bold="True" BackColor="#FFCC66">Bond</asp:Label></TD>
														</TR>
														<TR >
															<TD width="780" background="../../images/separator_repeat.gif" colSpan="5" height="11"></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
                                         </div>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

              <div class="clearfix"></div>

              <section class="box" id="TableComment" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <asp:TextBox id="txt_totalrecords" runat="server" CssClass="form-control inline-textbox"></asp:TextBox>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>

              <div class="clearfix"></div>

             <section class="box" id="" style="">
                     

                 <div class="content-body">
                <div class="row">

                     <div class="col-md-12">
                                                            <div class="form-group">
                                <label class="form-label"></label>
                                <span class="desc"></span>
                                <div class="controls">
                                   <uc1:footer id="Footer1" runat="server"></uc1:footer>
                                    </div>
                                                                </div>
                         </div>
                    </div>
                     </div>

                 </section>























            </section>
                     </section>



                 </div>
		
		</FORM>
         <script src="../assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>
    <script>window.jQuery||document.write('<script src="../assets/js/jquery-1.11.2.min.js"><\/script>');</script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="../assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

         <div id="errorAlert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body" style="min-height: 93px !important;max-height: 162px;">
        <p id="txtErrorMessage">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
	</body>
</HTML>

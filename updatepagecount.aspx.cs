using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WebSupergoo.ABCpdf6;
using FrameWorkEnation.Components;

namespace lntechNew
{
    public partial class updatepagecount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Click(object sender, EventArgs e)
        {
            try
            {
                clsENationWebComponents clsdb = new clsENationWebComponents();
                string vpath = ConfigurationSettings.AppSettings["NTPATHBatchPrint"].ToString();

                DataSet ds = clsdb.Get_DS_BySP("usp_hts_get_docpathwithid");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string[] key = { "@id", "@pagecount" };
                    int id = 0;
                    string filepath = vpath.Replace("\\\\","\\");
                    string filename = String.Empty;
                    int pagecount = 1;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            id = Convert.ToInt32(ds.Tables[0].Rows[i]["recordid"].ToString());
                            filename = ds.Tables[0].Rows[i]["docpath"].ToString();
                            Doc doc = new Doc();
                            doc.Read(filepath + filename);
                            pagecount = doc.PageCount;
                            doc.Dispose();
                            object[] val1 ={ id, pagecount };
                            clsdb.InsertBySPArr("usp_hts_update_pagecount", key, val1);
                        }
                        catch (Exception ex)
                        {
                            Response.Write(ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using clsDBase;
using System.IO;
using CrystalAddOns;
using System.Reflection;
using PDF2Image;

namespace ESignatures
{
    public partial class ESign : Form
    {
        #region Variables
            clsData cls_DataBase = new clsData();
            PDF2Image.ClsPDF2Image cls_PDF2Image = new PDF2Image.ClsPDF2Image(); 
            CrystalAddOns.Crystal cls_Crystal = new CrystalAddOns.Crystal();
    
            DataTable dtTest = new DataTable();
            DataTable dtClient = new DataTable();
            DataTable dtRecordLocking = new DataTable();
            DataTable ds_underlying = new DataTable();
            DataTable dt_BondPages = new DataTable();
            DataTable dt_Signature = new DataTable();
            private string TicketID = "", EmployeeID = "", SessionID = "";
            bool Outside_Flag = false, ContLetter_Flag = false, BondInside_Flag = false, BondOutside_Flag = false, TrialLetter_Flag = false, LetterRep_Flag = false;
            bool UserSelectedFlag_Receipt = false, UserSelectedFlag_ContLetter = false, UserSelectedFlag_Bond = false, UserSelectedFlag_TrialLetter = false, UserSelectedFlag_LetterRep = false;
            bool FirstSelectFlag_Receipt = true, FirstSelectFlag_ContLetter = true, FirstSelectFlag_Bond = true, FirstSelectFlag_TrialLetter = true, FirstSelectFlag_LetterRep = true;
            //bool RefreshFlag_Receipt = true, RefreshFlag_ContLetter = true, RefreshFlag_Bond = true, RefreshFlag_TrialLetter = true, RefreshFlag_LetterRep = true;
            bool isAnyDocSelected = false, Flag_SignAgain = false, isZoomIN = true;
            bool GotoFirstSign = true , Flag_DocumentShouldBeSaved =false ;
            private ArrayList alFiles = new ArrayList();
            private string LoginEntity = "", ExistingPictureName = "";
            private string filename = "", ClientLanguage = "", KeyWord = "", SignAll_KeyWord = "", SelectionOrder = "0";
            private int NoOfViolations = 1;
            private int CurrentPageNumber = 0;
            private string CurrentVisibleReportName = null, ScannedDocStoragePath="";
            private string ApplicationPath = Application.ExecutablePath.ToString().Substring(0, Application.ExecutablePath.ToString().LastIndexOf("\\"));
            private int pageCount = 0;
            private TabPage PreviousTab = new TabPage();
            ESignatures.Reports.Receipt objRpt_Receipt = new ESignatures.Reports.Receipt();
            ESignatures.Reports.BondDocs objRpt_BondDocs = new ESignatures.Reports.BondDocs();
            ESignatures.Reports.Letter_Continuance objRpt_Letter_Continuance = new ESignatures.Reports.Letter_Continuance();
            ESignatures.Reports.Letter_Rep objRpt_Letter_Rep = new ESignatures.Reports.Letter_Rep();
            ESignatures.Reports.Letter_Trial objRpt_Letter_Trial = new ESignatures.Reports.Letter_Trial();
            System.Windows.Forms.Panel pnl_Signature = new Panel();
            System.Windows.Forms.Panel pnl_Signature_Preview = new Panel();
            
            //*For Impersonation ************************************
            const int LOGON32_LOGON_NEW_CREDENTIALS = 9;
            const int LOGON32_PROVIDER_DEFAULT = 0;

            [DllImport("advapi32.dll", SetLastError = true)]
            public static extern int LogonUser(
                string lpszUsername,
                string lpszDomain,
                string lpszPassword,
                int dwLogonType,
                int dwLogonProvider,
                out IntPtr phToken
                );
            [DllImport("advapi32.dll", SetLastError = true)]
            public static extern int ImpersonateLoggedOnUser(
                IntPtr hToken
            );

            [DllImport("advapi32.dll", SetLastError = true)]
            static extern int RevertToSelf();

            [DllImport("kernel32.dll", SetLastError = true)]
            static extern int CloseHandle(IntPtr hObject);
            //*For Impersonation ************************************

        public static bool ThumbnailCallback()
        {
            return true;
            
        }

        #endregion
        
        public string Opener
        {
            get
            {
                return TicketID + "-" + UserSelectedDocs + "." + SessionID ;
            }
            set 
            {
                try { TicketID = value.Substring(0, value.IndexOf("-")); }
                catch { TicketID = "0"; }
                try { EmployeeID = value.Substring(value.IndexOf("-") + 1, value.LastIndexOf("-") - value.IndexOf("-") - 1);}
                catch { EmployeeID = "0"; }
                try { UserSelectedDocs = value.Substring(value.LastIndexOf("-") + 1, 5); }
                catch { UserSelectedDocs = "FFFFF"; }
                try { SessionID  = value.Substring(value.LastIndexOf(".") + 1); }
                catch { SessionID = "0"; }
                LoginEntity = "Client";
            }
        }

        private string UserSelectedDocs
        {
            get
            {
                return "RRTLB";
            }
            set
            {
                if (value.ToString().Substring(0, 1) == "T")
                {
                    UserSelectedFlag_Receipt = true;
                }
                if (value.ToString().Substring(1, 1) == "T")
                {
                    UserSelectedFlag_LetterRep = true;
                }
                if (value.ToString().Substring(2, 1) == "T")
                {
                    UserSelectedFlag_TrialLetter = true;
                }
                if (value.ToString().Substring(3, 1) == "T")
                {
                    UserSelectedFlag_ContLetter = true;
                }
                if (value.ToString().Substring(4, 1) == "T")
                {
                    UserSelectedFlag_Bond = true;
                }
            }
        }

        public ESign()
        {
            InitializeComponent();
        }

        private void ESign_Load(object sender, EventArgs e)
        {
            //TicketID = Outside "124229"; "124546"; "124140";//"124228";// "124140";// Inside - English 124177
            //Opener = "124546-3991-TTTTT.995185845767931";
            //MessageBox.Show(SessionID);
            GetClientInfo();
            initializeSettings();
        }

        private void initializeSettings()
        {
            //*Deleting one day old directories from client's machine.
            foreach (string drc in Directory.GetDirectories(ApplicationPath + @"\Images\"))
            {
                DirectoryInfo di = new DirectoryInfo(drc);
                if (di.CreationTime.Date != System.DateTime.Now.Date)
                {
                    di.Delete(true);
                }
            }
            //*Setting application path for images
            ApplicationPath = ApplicationPath + @"\Images\" + TicketID + EmployeeID + SessionID;

            //*Deleting current directory if exist
            try { Directory.Delete(ApplicationPath, true); }
            catch { }
            //*Creating directory for images
            try { Directory.CreateDirectory(ApplicationPath); }
            catch { }

            if (dtClient.Rows.Count != 0)
            {
                UpdateTabPages();
                UpdateSignedFieldsCounter();
                tab_Docs_Click(null, null);
            }
            PB_MainImage.KeyDown += new KeyEventHandler(PB_MainImage_KeyDown);
            PB_FirstImg.KeyDown += new KeyEventHandler(PB_FirstImage_KeyDown);
            CurrentPageNumber = 1;
        }

        private void UpdateTabPages()
        {
            try
            {
                tab_Docs.TabPages.RemoveByKey("TabPg_Receipt");
                tab_Docs.TabPages.RemoveByKey("TabPg_LetterOfRep");
                tab_Docs.TabPages.RemoveByKey("TabPg_TrailLetter");
                tab_Docs.TabPages.RemoveByKey("TabPg_LetterOfContinuation");
                tab_Docs.TabPages.RemoveByKey("TabPg_BondDocs");
                string[] parameterKeys = {"@TicketID"};
                object[] values = {TicketID};

                ds_underlying = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_Get_PAYMENTINFO_BUTTONSTATUS", parameterKeys, values).Tables[0];

                for (int i = 0; i < ds_underlying.Rows.Count; i++)
                {
                    if ((Convert.ToInt32(ds_underlying.Rows[0]["CourtID"]) == 3001) || (Convert.ToInt32(ds_underlying.Rows[0]["CourtID"]) == 3002) || (Convert.ToInt32(ds_underlying.Rows[0]["CourtID"]) == 3003))
                    {
                        Outside_Flag = false;
                    }
                    else
                    {
                        Outside_Flag = true;
                    }

                    if ((Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 1 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 2 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 11 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 12) && (Convert.ToInt32(ds_underlying.Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Rows[i]["CourtID"]) != 3003))
                    {
                        LetterRep_Flag = true;
                    }

                    DateTime dcourt = Convert.ToDateTime(ds_underlying.Rows[i]["CourtDateMain"]);
                    if ((dcourt >= DateTime.Now.Date) && (Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 2 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 3 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 4 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 5) && (Convert.ToInt32(ds_underlying.Rows[i]["CourtID"]) != 3001 && Convert.ToInt32(ds_underlying.Rows[i]["CourtID"]) != 3002 && Convert.ToInt32(ds_underlying.Rows[i]["CourtID"]) != 3003))
                    {
                        TrialLetter_Flag = true;
                    }
                    else
                    {
                        //Inside court status in Arr
                        if ((dcourt >= DateTime.Now.Date) && Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 4 && (Convert.ToInt32(ds_underlying.Rows[i]["CourtId"]) == 3001 || Convert.ToInt32(ds_underlying.Rows[i]["CourtId"]) == 3002 || Convert.ToInt32(ds_underlying.Rows[i]["CourtId"]) == 3003))
                        {
                            TrialLetter_Flag = true;
                        }
                    }

                    if (Convert.ToInt32(ds_underlying.Rows[i]["Bondflag"]) == 1 && (Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 1 || Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 2))
                    {
                        if (Outside_Flag == true)
                        {
                            BondOutside_Flag = true;
                        }
                        else
                        {
                            BondInside_Flag = true;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(ds_underlying.Rows[i]["categoryid"]) == 12)
                        {
                            if (Outside_Flag == true)
                            {
                                BondOutside_Flag = true;
                            }
                            else
                            {
                                BondInside_Flag = true;
                            }
                        }
                    }

                    if ((Convert.ToInt32(ds_underlying.Rows[0]["ContinuanceAmount"]) > 0))
                    {
                        ContLetter_Flag = true;
                    }
                }

                try
                {
                    if (UserSelectedFlag_Receipt == true)
                    {
                        tab_Docs.TabPages.Add(tabPg_Receipt);
                        tab_Docs.TabPages["tabPg_Receipt"].Select();
                        isAnyDocSelected = true;
                    }

                    if (ContLetter_Flag == true && UserSelectedFlag_ContLetter ==true)
                    {
                        tab_Docs.TabPages.Add(TabPg_LetterOfContinuation);
                        tab_Docs.TabPages["TabPg_LetterOfContinuation"].Select();
                        isAnyDocSelected = true;
                    }

                    if (LetterRep_Flag == true && UserSelectedFlag_LetterRep ==true )
                    {
                        tab_Docs.TabPages.Add(TabPg_LetterOfRep);
                        tab_Docs.TabPages["TabPg_LetterOfRep"].Select();
                        isAnyDocSelected = true;
                    }

                    if (TrialLetter_Flag == true && UserSelectedFlag_TrialLetter ==true )
                    {
                        tab_Docs.TabPages.Add(TabPg_TrailLetter);
                        tab_Docs.TabPages["TabPg_TrailLetter"].Select();
                        isAnyDocSelected = true;
                    }

                    if ((BondInside_Flag == true || BondOutside_Flag == true) && UserSelectedFlag_Bond ==true )
                    {
                        tab_Docs.TabPages.Add(TabPg_BondDocs);
                        tab_Docs.TabPages["TabPg_BondDocs"].Select();
                        isAnyDocSelected = true;
                    }
                }
                catch (Exception ex)
                {
                    cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString()); 
                    MessageBox.Show(ex.Message, "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (isAnyDocSelected == false) { MessageBox.Show("No Documents Found For This Ticket/Client!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            }
            catch (Exception ex)
            {
                cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                MessageBox.Show(ex.Message, "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetClientInfo()
        {
            try
            {
                string[] parameterKeys = { "@TicketID" };
                object[] values = { TicketID };

                dtClient = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_ClientInfo", parameterKeys, values).Tables[0];
                if (dtClient.Rows.Count == 0)
                {
                    throw new Exception("No document(s) found for the given ticket ID");
                    ds_underlying = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_Get_PAYMENTINFO_BUTTONSTATUS", parameterKeys, values).Tables[0];
                    return;
                }
                lbl_Name.Text = dtClient.Rows[0]["Clientname"].ToString();
                lbl_TicketNo.Text = dtClient.Rows[0]["TicketNumber"].ToString();
                lbl_CauseNo.Text = dtClient.Rows[0]["CauseNumber"].ToString();
                lbl_Address.Text = dtClient.Rows[0]["Address1"].ToString() + ". " + dtClient.Rows[0]["City"].ToString() + ", " + dtClient.Rows[0]["State"].ToString() + ", " + dtClient.Rows[0]["Zip"].ToString();
                lbl_Phone.Text = dtClient.Rows[0]["Contact1"].ToString();
                ClientLanguage = dtClient.Rows[0]["ClientLanguage"].ToString();
            }
            catch (Exception ex)
            {
                //cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString()); 
                //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Get_BondPages()
        {
            string[] key = { "@NoOfViolations"};
            object[] values = { NoOfViolations};
            dt_BondPages = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_BondPages", key, values).Tables[0];
        }

        private void RefreshReports()
        {
            string CurrentTab = tab_Docs.SelectedTab.Name;

            if (FirstSelectFlag_Receipt == false) 
            {
                tab_Docs.SelectedTab.Name = "tabPg_Receipt"; 
                GetData(false); 
            }
            if (FirstSelectFlag_LetterRep == false)
            {
                tab_Docs.SelectedTab.Name = "TabPg_LetterOfRep";
                GetData(false);
            }
            if (FirstSelectFlag_TrialLetter == false)
            {
                tab_Docs.SelectedTab.Name = "TabPg_TrailLetter";
                GetData(false);
            }
            if (FirstSelectFlag_ContLetter == false)
            {
                tab_Docs.SelectedTab.Name = "TabPg_LetterOfContinuation";
                GetData(false);
            }
            if (FirstSelectFlag_Bond == false)
            {
                tab_Docs.SelectedTab.Name = "TabPg_BondDocs";
                GetData(false);
            }
            tab_Docs.SelectedTab.Name = CurrentTab;  
         }

        private void GetData(bool Preview)
        {
            if (isAnyDocSelected == false) { return; }
            this.Cursor = Cursors.WaitCursor;
            try
            {
                CurrentPageNumber = 1;
                switch (tab_Docs.SelectedTab.Name.ToString())
                {
                    case "TabPg_BondDocs":
                        {
                            if (FirstSelectFlag_Bond == true)
                            {
                                string[] AutoSignKeys = { "@TicketID", "@Entity", "@KeyWord", "@CurrentDoc", "@ClientLanguage", "@ForDefaultSigns", "@SignID", "@SessionID" };
                                object[] AutoSignvalues = { TicketID, "Attorney", "Bond", 1, ClientLanguage, 1, "0", SessionID };

                                if (cls_DataBase.ExecuteSP("USP_HTS_ESignature_Insert_SignAll", AutoSignKeys, AutoSignvalues) == 0)
                                {
                                    //MessageBox.Show("Default signature of the Attorney not found! please make signature for auto sign", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            int FirstTime = 1;
                            if (FirstSelectFlag_Bond == true) { FirstTime = 1; } else { FirstTime = 0; }
                            string[] key = { "@TicketIDList", "@empid", "@FirstTime", "@SessionID" };
                            object[] values = { TicketID, EmployeeID, FirstTime, SessionID };
                            objRpt_BondDocs = new ESignatures.Reports.BondDocs();
                            dtTest = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_BondDocs", key, values).Tables[0];
                            if (dtTest.Rows.Count > 0)
                            {
                                NoOfViolations = (dtTest.Rows.Count - 1) / 2;
                                Get_BondPages();
                            }
                                objRpt_BondDocs.SetDataSource(dtTest);

                                string[] keySub = { "@TicketID" };
                                object[] valSub = { TicketID };
                                DataTable dtPlea = null;
                                dtPlea = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_LOR", keySub, valSub).Tables[0];
                                objRpt_BondDocs.OpenSubreport("SubReport_PleaOfNotGuiltyEnglish.rpt").SetDataSource(dtPlea);
                                
                                objRpt_BondDocs.ExportToDisk(ExportFormatType.PortableDocFormat, ApplicationPath + @"\BondDoc.pdf");
                                
                                SignAll_KeyWord = "Bond"; KeyWord = "Bond"; 
                                if (Preview == true)
                                {
                                    GenerateThumbnails(ApplicationPath + @"\BondDoc.pdf", 614, 790);
                                    CurrentVisibleReportName = "BondDoc";
                                    Display_PreviewPages();
                                }
                            break;
                        }
                    case "TabPg_LetterOfContinuation":
                        {
                            if (FirstSelectFlag_ContLetter == true)
                            {
                                string[] AutoSignKeys = { "@TicketID", "@Entity", "@KeyWord", "@CurrentDoc", "@ClientLanguage", "@ForDefaultSigns", "@SignID", "@SessionID" };
                                object[] AutoSignvalues = { TicketID, "Attorney", "LetterContinuance", 1, ClientLanguage, 1, "0", SessionID };

                                if (cls_DataBase.ExecuteSP("USP_HTS_ESignature_Insert_SignAll", AutoSignKeys, AutoSignvalues) == 0)
                                {
                                    //MessageBox.Show("Default signature of the Attorney not found! please make signature for auto sign", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            string[] key = { "@ticketid", "@employeeid", "@SessionID" };
                            object[] values = { TicketID, EmployeeID, @SessionID };
                            dtTest = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_ContinuanceLetter", key, values).Tables[0];
                            objRpt_Letter_Continuance.SetDataSource(dtTest);

                            objRpt_Letter_Continuance.ExportToDisk(ExportFormatType.PortableDocFormat, ApplicationPath + @"\LetterContinuance.pdf");
                            SignAll_KeyWord = "LetterContinuance"; KeyWord = "LetterContinuance"; 
                            if (Preview == true)
                            {

                                GenerateThumbnails(ApplicationPath + @"\LetterContinuance.pdf", 614, 790);
                                CurrentVisibleReportName = "LetterContinuance";
                                Display_PreviewPages();
                            }
                            break;
                        }
                    case "TabPg_TrailLetter":
                        {
                            if (FirstSelectFlag_TrialLetter == true)
                            {
                                string[] AutoSignKeys = { "@TicketID", "@Entity", "@KeyWord", "@CurrentDoc", "@ClientLanguage", "@ForDefaultSigns", "@SignID", "@SessionID" };
                                object[] AutoSignvalues = { TicketID, "Attorney", "LetterTrial", 1, ClientLanguage, 1, "0", SessionID };

                                if (cls_DataBase.ExecuteSP("USP_HTS_ESignature_Insert_SignAll", AutoSignKeys, AutoSignvalues) == 0)
                                {
                                    //MessageBox.Show("Default signature of the Attorney not found! please make signature for auto sign", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }

                            string[] key = { "@TicketIDList", "@empid", "@SessionID" };
                            object[] values = { TicketID, EmployeeID, SessionID };
                            dtTest = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_TrialLetter", key, values).Tables[0];
                            objRpt_Letter_Trial.SetDataSource(dtTest);
                            //objRpt_Letter_Trial.OpenSubreport("SubReport_AttorneySign_Img1.rpt").SetDataSource(cls_Crystal.CreateData("ImageStream_LetterTrial_Attorney1", TicketID, false ));

                            objRpt_Letter_Trial.ExportToDisk(ExportFormatType.PortableDocFormat, ApplicationPath + @"\LetterTrial.pdf");
                            SignAll_KeyWord = "LetterTrial"; KeyWord = "Trial Letter"; 
                            if (Preview == true)
                            {

                                GenerateThumbnails(ApplicationPath + @"\LetterTrial.pdf", 614, 790);
                                CurrentVisibleReportName = "LetterTrial";
                                Display_PreviewPages();
                            }
                            break;
                        }
                    case "TabPg_LetterOfRep":
                        {
                            if (FirstSelectFlag_LetterRep == true)
                            {
                                string[] AutoSignKeys = { "@TicketID", "@Entity", "@KeyWord", "@CurrentDoc", "@ClientLanguage", "@ForDefaultSigns", "@SignID", "@SessionID" };
                                object[] AutoSignvalues = { TicketID, "Attorney", "LetterOfRep", 1, ClientLanguage, 1, "0", SessionID };

                                if (cls_DataBase.ExecuteSP("USP_HTS_ESignature_Insert_SignAll", AutoSignKeys, AutoSignvalues) == 0)
                                {
                                    //MessageBox.Show("Default signature of the Attorney not found! please make signature for auto sign", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }

                            string[] key = { "@ticketid", "@empid", "@SessionID" };
                            object[] values = { TicketID, EmployeeID, SessionID };
                            dtTest = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_LetterOfRep", key, values).Tables[0];
                            objRpt_Letter_Rep.SetDataSource(dtTest);

                            objRpt_Letter_Rep.ExportToDisk(ExportFormatType.PortableDocFormat, ApplicationPath + @"\LetterOfRep.pdf");
                            SignAll_KeyWord = "LetterOfRep"; KeyWord = "Letter Of Rep"; 
                            if (Preview == true)
                            {
                                GenerateThumbnails(ApplicationPath + @"\LetterOfRep.pdf", 614, 790);
                                CurrentVisibleReportName = "LetterOfRep";
                                Display_PreviewPages();
                            }
                            break;
                        }
                    case "tabPg_Receipt":
                        {
                            if (ClientLanguage.ToUpper() != "ENGLISH" && FirstSelectFlag_Receipt ==true)
                            {
                                string[] AutoSignKeys = { "@TicketID", "@Entity", "@KeyWord", "@CurrentDoc", "@ClientLanguage", "@ForDefaultSigns", "@SignID", "@SessionID" };
                                object[] AutoSignvalues = { TicketID, "SalesRep", "Receipt", 1, ClientLanguage, 1, EmployeeID, SessionID };

                                if (cls_DataBase.ExecuteSP("USP_HTS_ESignature_Insert_SignAll", AutoSignKeys, AutoSignvalues) == 0)
                                {
                                    //MessageBox.Show("Default signature of the SalesRep not found! please make signature for auto sign", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            string[] key = { "@ticketid", "@employeeid", "@SessionID" };
                            object[] values = { TicketID, EmployeeID, SessionID };
                            dtTest = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_Receipt", key, values).Tables[0];

                            objRpt_Receipt.SetDataSource(dtTest);

                            objRpt_Receipt.ExportToDisk(ExportFormatType.PortableDocFormat, ApplicationPath + @"\Receipt.pdf");
                            SignAll_KeyWord = "Receipt";
                            if (ClientLanguage.ToUpper() == "ENGLISH")
                            {
                                KeyWord = "ReceiptEnglish";
                            }
                            else
                            {
                                KeyWord = "ReceiptSpanish";
                            }
                            if (Preview == true)
                            {
                                GenerateThumbnails(ApplicationPath + @"\Receipt.pdf", 614, 790);
                                CurrentVisibleReportName = "Receipt";
                                Display_PreviewPages();
                            }
                            break;
                        }
                }
                UpdateSignedFieldsCounter();
            }
            catch (Exception ex)
            {
                cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString()); 
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;  
            }
        }

        private void PrintCurrentReport()
        {
            bool IsAnyDocPrintable = false;
            try
            {
                DialogResult drt = MessageBox.Show("Are you sure you want to print this document?", "ESignature", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (drt == DialogResult.No) { return; }

                this.Cursor = Cursors.WaitCursor;
                GetData(false); 
                switch (tab_Docs.SelectedTab.Name.ToString())
                {
                    case "TabPg_BondDocs":
                        {
                            if (FirstSelectFlag_Bond == false)
                            {
                                objRpt_BondDocs.PrintToPrinter(1, true, 1, 100);
                                IsAnyDocPrintable = true;
                            }
                            break;
                        }
                    case "TabPg_LetterOfContinuation":
                        {
                            if (FirstSelectFlag_ContLetter == false)
                            {
                                objRpt_Letter_Continuance.PrintToPrinter(1, true, 1, 100);
                                IsAnyDocPrintable = true;
                            }
                            break;
                        }
                    case "TabPg_TrailLetter":
                        {
                            if (FirstSelectFlag_TrialLetter == false)
                            {
                                objRpt_Letter_Trial.PrintToPrinter(1, true, 1, 100);
                                IsAnyDocPrintable = true;
                            }
                            break;
                        }
                    case "TabPg_LetterOfRep":
                        {
                            if (FirstSelectFlag_LetterRep == false)
                            {
                                objRpt_Letter_Rep.PrintToPrinter(1, true, 1, 100);
                                IsAnyDocPrintable = true;
                            }
                            break;
                        }
                    case "tabPg_Receipt":
                        {
                            if (FirstSelectFlag_Receipt == false)
                            {
                                objRpt_Receipt.PrintToPrinter(1, true, 1, 100);
                                IsAnyDocPrintable = true;
                            }
                            break;
                        }
                }
                this.Cursor = Cursors.Default;
                if (IsAnyDocPrintable == true)
                {
                    MessageBox.Show("Document printed successfuly!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("You didn't made any signature or thumbprint at this document therefore you can't print it!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch(Exception ex)
            {
                cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                MessageBox.Show(ex.Message.ToString(), "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error); this.Cursor = Cursors.Default; 
            }
            finally
            { this.Cursor = Cursors.Default; }
        }

        private void PrintAllReports()
        {
            bool IsAnyDocPrintable = false;
            try
            {
                DialogResult drt = MessageBox.Show("Are you sure you want to print these all documents?", "ESignature", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (drt == DialogResult.No) { return; }

                this.Cursor = Cursors.WaitCursor;
                RefreshReports(); 
                // To Print Receipt
                if (FirstSelectFlag_Receipt == false)
                {
                    objRpt_Receipt.PrintToPrinter(1, true, 1, 100);
                    IsAnyDocPrintable = true;
                }

                // To Print Letter of Rep Conditionally
                if (LetterRep_Flag == true && FirstSelectFlag_LetterRep ==false )
                {
                    objRpt_Letter_Rep.PrintToPrinter(1, true, 1, 100);
                    IsAnyDocPrintable = true;
                }

                // To Print Bond Docs Conditionally
                if ((BondInside_Flag == true || BondOutside_Flag == true) && FirstSelectFlag_Bond ==false )
                {
                    objRpt_BondDocs.PrintToPrinter(1, true, 1,100);
                    IsAnyDocPrintable = true;
                }

                // To Print Letter of Continuance Conditionally
                if (ContLetter_Flag == true && FirstSelectFlag_ContLetter ==false )
                {
                    objRpt_Letter_Continuance.PrintToPrinter(1, true, 1, 100);
                    IsAnyDocPrintable = true;
                }

                // To Print Trial Letter Conditionally
                if (TrialLetter_Flag == true && FirstSelectFlag_TrialLetter ==false )
                {
                    objRpt_Letter_Trial.PrintToPrinter(1, true, 1, 100);
                    IsAnyDocPrintable = true;
                }
                this.Cursor = Cursors.Default;
                if (IsAnyDocPrintable == true)
                {
                    MessageBox.Show("Document(s) printed successfuly!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);    
                }
                else
                {
                    MessageBox.Show("You didn't made any signature or thumbprint at any document therefore you can't print them!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);    
                }
            }
            catch (Exception ex)
            {
                cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                MessageBox.Show(ex.Message.ToString(), "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error); this.Cursor = Cursors.Default; 
            }
            finally
            { this.Cursor = Cursors.Default; }
        }

        private void UpdatePageCounter(int PageIndex, int TotalPages)
        {
            lblPageCounter.Text = "Page " + PageIndex.ToString() + " of " + TotalPages.ToString();
        }

        private void Display_PreviewPages()
        {
            try 
            {
                if (pageCount == 1)
                {
                    PB_FirstImg.ImageLocation = ApplicationPath + @"\" + CurrentVisibleReportName + ".JPG";
                }
                else
                {
                    PB_FirstImg.ImageLocation = ApplicationPath + @"\" + CurrentVisibleReportName + "0001.JPG";
                }
                Pnl_Shadow_FirstImg.Left = 221;
                Pnl_Shadow_FirstImg_X.Left = 221;
                Pnl_Shadow_FirstImg_Y.Left = 314;
                PB_FirstImg.Left = 226;
           
                pnl_PreviewMain.Visible = true;
                pnl_MainImage.Visible = false;

                if (pageCount > 1)
                {
                    btn_Page_Next.Enabled = true;
                }
                else
                {
                    btn_Page_Next.Enabled = false;
                    btn_Page_Previous.Enabled = false;
                }

                Set_ImageToPB(true);
                PB_FirstImg.Select();  

            }
            catch { }
        }

        private void TakeSignature(AxeSign3.AxesCapture Cnt_Sign )
        {
            this.Cursor = Cursors.WaitCursor;
            Cnt_Sign.DisplayName = "W A I T";
            Cnt_Sign.StartSign(eSign3.ButtonStyle.BT_OK_CANCEL_CLEAR, 1);
            if (Cnt_Sign.IsSigned == 1)
            {
                Cnt_Sign.ClearSign();
                LocateImage();
                if (Flag_SignAgain == false)
                {
                    btn_NextSign_Click(null, null);
                }
                else
                {
                    LocateSignature(pnl_Signature.Tag.ToString(), 2);
                }
            }
            this.Cursor = Cursors.Default; 
        }

        private void LocateSignature(string ImageTrace)
        {
            string PageDescription = "";
            string Input_Keyword = "";
            if (filename.Substring(0, 7).ToString() == "BondDoc" && dt_BondPages.Rows.Count > 0)
            {
                for (int LoopCounter = 0; LoopCounter < pageCount; LoopCounter++)
                {
                    if (dt_BondPages.Rows[LoopCounter]["PageNumber"].ToString() == CurrentPageNumber.ToString())
                    {
                        PageDescription = dt_BondPages.Rows[LoopCounter]["PageDescription"].ToString();
                        if (PageDescription == "ContractPage") { PageDescription = PageDescription + ClientLanguage; }
                        break;
                    }
                }
            }
            if (ClientLanguage.ToUpper() == "ENGLISH" && PageDescription == "ClientInitialsPage") { Input_Keyword = "Inside"; } else { Input_Keyword = KeyWord; }
            string[] parameterKeys = { "@ImageTrace", "@SignLocation", "@KeyWord", "@TicketID", "@Entity", "@PageDescription" };
            object[] values = { ImageTrace, 0, Input_Keyword, TicketID, LoginEntity, PageDescription };
            DataTable dtSignatureLocation = new DataTable();
            dtSignatureLocation = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_SignatureLocation", parameterKeys, values).Tables[0];
            PB_MainImage.Controls.Clear();
            PB_FirstImg.Controls.Clear();
            pnl_Signature.Controls.Clear();
            pnl_Signature_Preview.Controls.Clear();    
            pnl_Signature.Visible = false;
            pnl_Signature_Preview.Visible = false; 

            if (dtSignatureLocation.Rows.Count != 0)
            {
                ExistingPictureName = ApplicationPath + @"\" + dtSignatureLocation.Rows[0]["SelectionOrder"].ToString() + "RunTimeSign" + PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1) + "Top" + dtSignatureLocation.Rows[0]["Pnl_Left"].ToString() + "Left" + dtSignatureLocation.Rows[0]["Pnl_Left"].ToString() + ".GIF";
                pnl_Signature.Tag = dtSignatureLocation.Rows[0]["ImageTrace"].ToString();
                SelectionOrder = dtSignatureLocation.Rows[0]["SelectionOrder"].ToString();
                
                pnl_Signature.Width = 146;
                pnl_Signature.Height = 62;
                pnl_Signature.Left = Convert.ToInt16(dtSignatureLocation.Rows[0]["Pnl_Left"]);
                pnl_Signature.Top = Convert.ToInt16(dtSignatureLocation.Rows[0]["Pnl_Top"]);
                if (File.Exists(ExistingPictureName) == true)
                {
                    pnl_Signature.BackColor = Color.DodgerBlue;
                }
                else
                {
                    pnl_Signature.BackColor = Color.Gold;
                }
                pnl_Signature.Visible = true;

                Cnt_Signature_Runtime.Width = 140;
                Cnt_Signature_Runtime.Height = 56;
                Cnt_Signature_Runtime.Left = 3;
                Cnt_Signature_Runtime.Top = 3;
                Cnt_Signature_Runtime.DisplayName = "C L I C K   H E R E";
                Cnt_Signature_Runtime.Visible = true;

                pnl_Signature_Preview.Width = 73;
                pnl_Signature_Preview.Height = 26;
                double PreviewPanelLeft = (((Convert.ToDouble(pnl_Signature.Left) / Convert.ToDouble(PB_MainImage.Width)) * 100) * Convert.ToDouble(PB_FirstImg.Width) / 100);
                pnl_Signature_Preview.Left = Convert.ToInt32(PreviewPanelLeft);
                double PreviewPanelTop = (((Convert.ToDouble(pnl_Signature.Top) / Convert.ToDouble(PB_MainImage.Height)) * 100) * Convert.ToDouble(PB_FirstImg.Height) / 100);
                pnl_Signature_Preview.Top = Convert.ToInt32(PreviewPanelTop) + 2;
                if (File.Exists(ExistingPictureName) == true)
                {
                    pnl_Signature_Preview.BackColor = Color.DodgerBlue;
                }
                else
                {
                    pnl_Signature_Preview.BackColor = Color.Gold;
                }
                pnl_Signature_Preview.Visible = true;

                Cnt_PreviewSignature.Width = 69;
                Cnt_PreviewSignature.Height = 22;
                Cnt_PreviewSignature.Left = 2;
                Cnt_PreviewSignature.Top = 2;
                Cnt_PreviewSignature.DisplayName = "C L I C K   H E R E";
                Cnt_PreviewSignature.Visible = true;

                this.Font = new Font(this.Font.FontFamily.Name, 8, GraphicsUnit.Point);
                PB_MainImage.Controls.Add(pnl_Signature);
                pnl_Signature.Controls.Add(Cnt_Signature_Runtime);
                PB_FirstImg.Controls.Add(pnl_Signature_Preview);
                pnl_Signature_Preview.Controls.Add(Cnt_PreviewSignature);

                Cnt_Signature_Runtime.SetSignerDetailsEx(lbl_Name.Text, "", "", "", "", "", "", "Sign Here");
                Cnt_Signature_Runtime.SetSignRect(Cnt_Signature_Runtime.Width, Cnt_Signature_Runtime.Height);
                Cnt_Signature_Runtime.ClearSign();


                Cnt_PreviewSignature.SetSignerDetailsEx(lbl_Name.Text, "", "", "", "", "", "", "Sign Here");
                Cnt_PreviewSignature.SetSignRect(Cnt_PreviewSignature.Width, Cnt_PreviewSignature.Height);
                Cnt_PreviewSignature.ClearSign();
                if (isZoomIN == false)
                {
                    Cnt_Signature_Runtime.Select();
                }
                else
                {
                    Cnt_PreviewSignature.Select();
                }

                if (isZoomIN == false)
                {
                    int ScrollValue = pnl_MainImage.VerticalScroll.Value;
                    PB_MainImage.Select();
                    try { pnl_MainImage.VerticalScroll.Value = ScrollValue + 15; }
                    catch { pnl_MainImage.VerticalScroll.Value = ScrollValue; }
                }
                else
                {
                    PB_FirstImg.Select();  
                }
                
            }
        }

        private void LocateSignature(string ImageTrace, int SignLocation)
        {
            string Input_Keyword = "";
            string PageDescription = "";
            if (filename.Substring(0, 7).ToString() == "BondDoc" && dt_BondPages.Rows.Count > 0)
            {
                for (int LoopCounter = 0; LoopCounter < pageCount; LoopCounter++)
                {
                    if (dt_BondPages.Rows[LoopCounter]["PageNumber"].ToString() == CurrentPageNumber.ToString())
                    {
                        PageDescription = dt_BondPages.Rows[LoopCounter]["PageDescription"].ToString();
                        if (PageDescription == "ContractPage") { PageDescription = PageDescription + ClientLanguage; }
                        break;
                    }
                }
            }
            if (ClientLanguage.ToUpper() == "ENGLISH" && PageDescription == "ClientInitialsPage") { Input_Keyword = "Inside"; } else { Input_Keyword = KeyWord; }
            string[] parameterKeys = { "@ImageTrace", "@SignLocation", "@KeyWord", "@TicketID", "@Entity", "@PageDescription" };
            object[] values = { ImageTrace, SignLocation, Input_Keyword, TicketID, LoginEntity, PageDescription };

            DataTable dtSignatureLocation = new DataTable();
            dtSignatureLocation = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_SignatureLocation", parameterKeys, values).Tables[0];
            PB_MainImage.Controls.Clear();
            PB_FirstImg.Controls.Clear();
            pnl_Signature.Controls.Clear();
            pnl_Signature_Preview.Controls.Clear();
            pnl_Signature.Visible = false;
            pnl_Signature_Preview.Visible = false; 

            if (dtSignatureLocation.Rows.Count != 0)
            {
                ExistingPictureName = ApplicationPath + @"\" + dtSignatureLocation.Rows[0]["SelectionOrder"].ToString() + "RunTimeSign" + PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1) + "Top" + dtSignatureLocation.Rows[0]["Pnl_Top"].ToString() + "Left" + dtSignatureLocation.Rows[0]["Pnl_Left"].ToString() + ".GIF";
                pnl_Signature.Tag = dtSignatureLocation.Rows[0]["ImageTrace"].ToString();
                SelectionOrder = dtSignatureLocation.Rows[0]["SelectionOrder"].ToString();

                pnl_Signature.Width = 146;
                pnl_Signature.Height = 62;
                pnl_Signature.Left = Convert.ToInt16(dtSignatureLocation.Rows[0]["Pnl_Left"]);
                pnl_Signature.Top = Convert.ToInt16(dtSignatureLocation.Rows[0]["Pnl_Top"]);
                if (File.Exists(ExistingPictureName) == true)
                {
                    pnl_Signature.BackColor = Color.DodgerBlue;
                }
                else
                {
                    pnl_Signature.BackColor = Color.Gold;
                }
                pnl_Signature.Visible = true;

                Cnt_Signature_Runtime.Width = 140;
                Cnt_Signature_Runtime.Height = 56;
                Cnt_Signature_Runtime.Left = 3;
                Cnt_Signature_Runtime.Top = 3;
                Cnt_Signature_Runtime.DisplayName = "C L I C K   H E R E";
                Cnt_Signature_Runtime.Visible = true;

                pnl_Signature_Preview.Width = 73;
                pnl_Signature_Preview.Height = 26;
                double PreviewPanelLeft = (((Convert.ToDouble(pnl_Signature.Left) / Convert.ToDouble(PB_MainImage.Width)) * 100) * Convert.ToDouble(PB_FirstImg.Width) / 100);
                pnl_Signature_Preview.Left = Convert.ToInt32(PreviewPanelLeft);
                double PreviewPanelTop =  (((Convert.ToDouble(pnl_Signature.Top) / Convert.ToDouble(PB_MainImage.Height)) * 100) * Convert.ToDouble(PB_FirstImg.Height) / 100  );
                pnl_Signature_Preview.Top = Convert.ToInt32(PreviewPanelTop) + 2;
                if (File.Exists(ExistingPictureName) == true)
                {
                    pnl_Signature_Preview.BackColor = Color.DodgerBlue;
                }
                else
                {
                    pnl_Signature_Preview.BackColor = Color.Gold;
                }
                pnl_Signature_Preview.Visible = true;

                Cnt_PreviewSignature.Width = 69;
                Cnt_PreviewSignature.Height = 22;
                Cnt_PreviewSignature.Left = 2;
                Cnt_PreviewSignature.Top = 2;
                Cnt_PreviewSignature.DisplayName = "C L I C K   H E R E";
                Cnt_PreviewSignature.Visible = true;

                this.Font = new Font(this.Font.FontFamily.Name, 8, GraphicsUnit.Point);
                PB_MainImage.Controls.Add(pnl_Signature);
                pnl_Signature.Controls.Add(Cnt_Signature_Runtime);
                PB_FirstImg.Controls.Add(pnl_Signature_Preview);
                pnl_Signature_Preview.Controls.Add(Cnt_PreviewSignature);

                Cnt_Signature_Runtime.SetSignerDetailsEx(lbl_Name.Text, "", "", "", "", "", "", "Sign Here");
                Cnt_Signature_Runtime.SetSignRect(Cnt_Signature_Runtime.Width, Cnt_Signature_Runtime.Height);
                Cnt_Signature_Runtime.ClearSign();


                Cnt_PreviewSignature.SetSignerDetailsEx(lbl_Name.Text, "", "", "", "", "", "", "Sign Here");
                Cnt_PreviewSignature.SetSignRect(Cnt_PreviewSignature.Width, Cnt_PreviewSignature.Height);
                Cnt_PreviewSignature.ClearSign();
                if (isZoomIN == false)
                {
                    Cnt_Signature_Runtime.Select();
                }
                else
                {
                    Cnt_PreviewSignature.Select();  
                }

                if (isZoomIN == false)
                {
                    int ScrollValue = pnl_MainImage.VerticalScroll.Value;
                    PB_MainImage.Select();
                    try { pnl_MainImage.VerticalScroll.Value = ScrollValue + 15; }
                    catch { pnl_MainImage.VerticalScroll.Value = ScrollValue; }
                }
                else
                {
                    PB_FirstImg.Select();
                }
              }
        }

        private void LocateImage()
        {
            try
            {
                string saveFileNameMain=null;
                string saveFileNameThumbnail = null;
                
                int SignLeft = 0;
                int SignTop = 0;
                Flag_SignAgain = false;
                System.Drawing.Graphics objGraphic = null;
                Image imgMainForThumbnail;
                Image imgSignature;
                Image imgMerged;
                try
                {
                    saveFileNameMain = PB_MainImage.ImageLocation.ToString().Replace("_Main","_Blank")  ;
                    saveFileNameThumbnail = PB_FirstImg.ImageLocation.ToString();
                    
                    imgMerged = Image.FromFile(saveFileNameMain);
                    objGraphic = System.Drawing.Graphics.FromImage(imgMerged);

                    foreach (string SignatureFileName in Directory.GetFiles(ApplicationPath,"*.GIF"))
                    {
                        //Checking, is the searched signature image relating with the current page? and is it Gif?
                        ///MessageBox.Show(PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1).ToString());  
                        if (SignatureFileName.IndexOf(PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1)) > 1 && SignatureFileName.IndexOf("Left") > 1)
                        {
                            imgSignature = Image.FromFile(SignatureFileName);  
                            SignLeft = Convert.ToInt32(SignatureFileName.Substring(SignatureFileName.IndexOf("Left") + 4, SignatureFileName.IndexOf(".") - SignatureFileName.IndexOf("Left") - 4));
                            SignTop = Convert.ToInt32(SignatureFileName.Substring(SignatureFileName.IndexOf("Top") + 3, SignatureFileName.IndexOf("Left") - SignatureFileName.IndexOf("Top") - 3));
                            // Drawing the signature(s) at graphic object over the main image.
                            objGraphic.DrawImageUnscaled(imgSignature, SignLeft, SignTop);
                            objGraphic.Save();
                            imgSignature.Dispose();        
                        }
                    }

                    imgMerged.Save(saveFileNameMain.Replace(".", "Backup."));
                    imgMerged = Image.FromFile(saveFileNameMain.Replace(".", "Backup."));

                    imgMainForThumbnail = imgMerged.GetThumbnailImage(422, 655, ThumbnailCallback, IntPtr.Zero);
                    imgMainForThumbnail.Save(saveFileNameThumbnail.Replace(".","Backup.") );
                    
                    imgMerged.Dispose();
                    
                    imgMainForThumbnail.Dispose();
                    objGraphic.Flush();
                    objGraphic.Dispose();  

                    File.Copy(saveFileNameMain.Replace(".", "Backup."), PB_MainImage.ImageLocation  ,true );
                    File.Copy(saveFileNameThumbnail.Replace(".", "Backup."), PB_FirstImg.ImageLocation , true);
                    // Refreshing the main image.
                    PB_MainImage.ImageLocation = PB_MainImage.ImageLocation;
                    PB_FirstImg.ImageLocation = saveFileNameThumbnail;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message, "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            catch (Exception ex)
            {
                //LocateSignature(pnl_Signature.Tag.ToString(),0); 
                //MessageBox.Show(ex.Message);
            }
        }

        private void GenerateThumbnails(string SourceFileName, int width, int height)//150, 200
        {
            try 
            {
                pageCount = ClsPDF2Image.PDFToImageGetPageCount(SourceFileName);
                UpdatePageCounter(1, pageCount);
                //For Main Images
                string outputFile = SourceFileName.Replace(".pdf", "_Main.JPG");
                int x = ClsPDF2Image.PDFToImageConverter(SourceFileName, outputFile, "", "", 92, 101, 24, ClsPDF2Image.CompressionType.COMPRESSION_JPEG, 70, false, true, -1, -1);
                //For Backup Images
                outputFile = SourceFileName.Replace(".pdf", "_Blank.JPG");
                x = ClsPDF2Image.PDFToImageConverter(SourceFileName, outputFile, "", "", 92, 101, 24, ClsPDF2Image.CompressionType.COMPRESSION_JPEG, 70, false, true, -1, -1);
                //For Thumbnail Images
                outputFile = SourceFileName.Replace(".pdf", ".JPG");
                x = ClsPDF2Image.PDFToImageConverter(SourceFileName, outputFile, "", "", 55, 55, 24, ClsPDF2Image.CompressionType.COMPRESSION_JPEG, 70, false, true, -1, -1);
            }
            catch (Exception ex)
            {
                cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                MessageBox.Show(ex.Message.ToString(), "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GenerateThumbnailsOfSpecificPage(string SourceFileName, int width, int height)//150, 200
        {
            try
            {
                string outputFile = "";
                int x = 0;

                outputFile = SourceFileName.Replace(".pdf", "_Main.JPG");
                x = ClsPDF2Image.PDFToImageConverter(SourceFileName, outputFile, "", "", 92, 101, 24, ClsPDF2Image.CompressionType.COMPRESSION_JPEG, 70, false, true, -1, -1);
                
                outputFile = SourceFileName.Replace(".pdf", ".JPG");
                x = ClsPDF2Image.PDFToImageConverter(SourceFileName, outputFile, "", "", 55, 55, 24, ClsPDF2Image.CompressionType.COMPRESSION_JPEG, 70, false, true, -1, -1);
            }
            catch (Exception ex)
            {
                cls_DataBase.ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                MessageBox.Show(ex.Message.ToString(), "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tab_Docs_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { return; }
            if (dtClient.Rows.Count != 0)
                
            PB_MainImage.Controls.Clear();
            PB_FirstImg.Controls.Clear();
            pnl_Signature.Controls.Clear();
            pnl_Signature_Preview.Controls.Clear();
            pnl_Signature.Visible = false;
            pnl_Signature_Preview.Visible = false; 

            switch (tab_Docs.SelectedTab.Name.ToString())
            {
                case "TabPg_BondDocs":
                    {
                        KeyWord = "Bond";
                        if (FirstSelectFlag_Bond == true )//|| RefreshFlag_Bond ==true )
                        {
                            GetData(true);
                        }
                        else
                        {
                            CurrentVisibleReportName = "BondDoc";
                            pageCount = ClsPDF2Image.PDFToImageGetPageCount(ApplicationPath + @"\BondDoc.pdf" );
                            UpdatePageCounter(1, pageCount);
                            Display_PreviewPages(); 
                        }
                        break;
                    }
                case "TabPg_LetterOfContinuation":
                    {
                        KeyWord = "LetterContinuance";
                        if (FirstSelectFlag_ContLetter == true )//|| RefreshFlag_ContLetter ==true )
                        {
                            GetData(true);
                        }
                        else
                        {
                            CurrentVisibleReportName = "LetterContinuance";
                            pageCount = ClsPDF2Image.PDFToImageGetPageCount(ApplicationPath + @"\LetterContinuance.pdf");
                            UpdatePageCounter(1, pageCount);
                            Display_PreviewPages();
                        }
                        break;
                    }
                case "TabPg_TrailLetter":
                    {
                        KeyWord = "Trial Letter";
                        if (FirstSelectFlag_TrialLetter == true )//|| RefreshFlag_TrialLetter ==true )
                        {
                            GetData(true);
                        }
                        else 
                        {
                            CurrentVisibleReportName = "LetterTrial";
                            pageCount = ClsPDF2Image.PDFToImageGetPageCount(ApplicationPath + @"\LetterTrial.pdf");
                            UpdatePageCounter(1, pageCount);
                            Display_PreviewPages(); 
                        }
                        break;
                    }
                case "TabPg_LetterOfRep":
                    {
                        KeyWord = "Letter Of Rep";
                        if (FirstSelectFlag_LetterRep == true )//|| RefreshFlag_LetterRep ==true )
                        {
                            GetData(true);
                        }
                        else 
                        {
                            CurrentVisibleReportName = "LetterOfRep";
                            pageCount = ClsPDF2Image.PDFToImageGetPageCount(ApplicationPath + @"\LetterOfRep.pdf");
                            UpdatePageCounter(1, pageCount);
                            Display_PreviewPages(); 
                        }
                        break;
                    }
                case "tabPg_Receipt":
                    {
                        if (ClientLanguage.ToUpper() == "ENGLISH")
                        {
                            KeyWord = "ReceiptEnglish";
                        }
                        else
                        {
                            KeyWord = "ReceiptSpanish";
                        }

                        if (FirstSelectFlag_Receipt == true )//|| RefreshFlag_Receipt ==true )
                        {
                            GetData(true);
                        }
                        else
                        {
                            CurrentVisibleReportName = "Receipt";
                            pageCount = ClsPDF2Image.PDFToImageGetPageCount(ApplicationPath + @"\Receipt.pdf");
                            UpdatePageCounter(1, pageCount);
                            Display_PreviewPages();
                        }
                        break;
                    }
            }
        }

        private void btn_Page_Next_Click(object sender, EventArgs e)
        {
            if ( CurrentPageNumber  < pageCount)
            {
                 CurrentPageNumber ++;
                if ( CurrentPageNumber .ToString().Length > 1)
                {
                    try
                    {
                        PB_FirstImg.ImageLocation = ApplicationPath + @"\" + CurrentVisibleReportName + "00" + CurrentPageNumber + ".JPG";
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        PB_FirstImg.ImageLocation = ApplicationPath + @"\" + CurrentVisibleReportName + "000" + CurrentPageNumber + ".JPG";
                    }
                    catch { }
                }
            }

            if (pageCount > 1)
            {
                UpdatePageCounter(Convert.ToInt16(PB_FirstImg.ImageLocation.Substring(PB_FirstImg.ImageLocation.IndexOf(".") - 4, 4)), pageCount);
            }
            PB_MainImage.Controls.Clear();
            PB_FirstImg.Controls.Clear();
            pnl_Signature.Controls.Clear();
            pnl_Signature_Preview.Controls.Clear();
            pnl_Signature.Visible = false;
            pnl_Signature_Preview.Visible = false; 
            
            Set_ImageToPB(false);
            LocateImage();
            Set_PBFocus();
        }

        private void btn_Page_Previous_Click(object sender, EventArgs e)
        {
            if ( CurrentPageNumber  > 1)
            {
                 CurrentPageNumber --;
                if ( CurrentPageNumber .ToString().Length > 1)
                {
                    try
                    {
                        PB_FirstImg.ImageLocation = ApplicationPath + @"\" + CurrentVisibleReportName + "00" + CurrentPageNumber + ".JPG";
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        PB_FirstImg.ImageLocation = ApplicationPath + @"\" + CurrentVisibleReportName + "000" + CurrentPageNumber + ".JPG";
                    }
                    catch { }
                }
            }
            if (pageCount > 1)
            {
                UpdatePageCounter(Convert.ToInt16(PB_FirstImg.ImageLocation.Substring(PB_FirstImg.ImageLocation.IndexOf(".") - 4, 4)), pageCount);
            }
            PB_MainImage.Controls.Clear();
            PB_FirstImg.Controls.Clear();
            pnl_Signature.Controls.Clear();
            pnl_Signature_Preview.Controls.Clear();
            pnl_Signature.Visible = false;
            pnl_Signature_Preview.Visible = false; 

            Set_ImageToPB(false );
            LocateImage(); 
            Set_PBFocus();
        }
        
        private void Set_PBFocus()
        {
            if (isZoomIN == true)
            {
                PB_FirstImg.Select();
            }
            else
            {
                PB_MainImage.Select();
            }
        }

        private void btn_Zoom_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { return; }
            if (isZoomIN == false )
            {
                pnl_MainImage.Visible = false;
                isZoomIN = true ;
                PB_FirstImg.Select();
            }
            else
            {
                pnl_MainImage.Visible = true;
                isZoomIN = false;
                int ScrollValue = pnl_MainImage.VerticalScroll.Value;
                PB_MainImage.Select();
                pnl_MainImage.VerticalScroll.Value = ScrollValue;
            }
            Set_PBFocus();
        }

        private void tab_Docs_Deselected(object sender, TabControlEventArgs e)
        {
            PreviousTab = tab_Docs.SelectedTab;
        }

        private void PB_FirstImg_MouseEnter(object sender, EventArgs e)
        {
            Pnl_Shadow_FirstImg.BackColor = Color.DodgerBlue;
            if (pageCount > 1)
            {
                UpdatePageCounter(Convert.ToInt16(PB_FirstImg.ImageLocation.Substring(PB_FirstImg.ImageLocation.IndexOf(".") - 4, 4)), pageCount);
            }
        }

        private void Set_ImageToPB(bool IsLocateSignature)
        {
            filename = PB_FirstImg.ImageLocation.ToString().Remove(0, PB_FirstImg.ImageLocation.ToString().LastIndexOf("\\") + 1);
            try
            {
                CurrentPageNumber = Convert.ToInt16(filename.Substring(filename.IndexOf(".") - 4, 4));

                PB_MainImage.ImageLocation = PB_FirstImg.ImageLocation.ToString().Insert(PB_FirstImg.ImageLocation.ToString().IndexOf(".") - 4, "_Main");
            }
            catch
            {
                CurrentPageNumber = 1;

                PB_MainImage.ImageLocation = PB_FirstImg.ImageLocation.ToString().Insert(PB_FirstImg.ImageLocation.ToString().IndexOf("."), "_Main");
            }

            if ( CurrentPageNumber  > 1 && CurrentPageNumber < pageCount)
            {
                btn_Page_Previous.Enabled = true;
                btn_Page_Next.Enabled = true;
            }
            else if ( CurrentPageNumber  == 1 && CurrentPageNumber < pageCount)
            {
                btn_Page_Previous.Enabled = false;
                btn_Page_Next.Enabled = true;
            }
            else if ( CurrentPageNumber  == pageCount)
            {
                btn_Page_Next.Enabled = false;
                btn_Page_Previous.Enabled = true;
            }

            if (IsLocateSignature == true)
            {
                switch (filename.Substring(0, 7).ToString())
                {
                    case "Receipt":
                        {
                            if (ClientLanguage.ToUpper() == "ENGLISH")
                            {
                                if (GotoFirstSign == true)
                                {
                                    LocateSignature("ImageStream_Receipt_ClientSign1", 0);
                                }
                                else
                                {
                                    LocateSignature("ImageStream_Receipt_ClientSign1", 3);
                                }
                            }
                            else
                            {
                                if (GotoFirstSign == true)
                                {
                                    LocateSignature("ImageStream_Receipt_ClientSign2", 0);
                                }
                            }
                            break;
                        }
                    case "LetterC":
                        {
                            if (GotoFirstSign == true)
                            {
                                LocateSignature("ImageStream_LetterContinuance_Attorney1", 0);
                            }
                            else
                            {
                                LocateSignature("ImageStream_LetterContinuance_Attorney1", 3);
                            }
                            break; 
                        }
                    case "LetterO":
                        {
                            if (GotoFirstSign == true)
                            {
                                LocateSignature("ImageStream_LetterOfRep_Attorney1", 0);
                            }
                            else
                            {
                                LocateSignature("ImageStream_LetterOfRep_Attorney1", 3);
                            }
                            break;
                        }
                    case "LetterT":
                        {
                            if (GotoFirstSign == true)
                            {
                                LocateSignature("ImageStream_LetterTrial_Attorney1", 0);
                            }
                            else
                            {
                                LocateSignature("ImageStream_LetterTrial_Attorney1", 3);
                            }
                            break;
                        }
                    case "BondDoc":
                        {
                            if (GotoFirstSign == true)
                            {
                                LocateSignature("ImageStream_Bond_ClientSign1", 0);
                            }
                            else
                            {
                                LocateSignature("ImageStream_Bond_ClientSign1", 3);
                            }
                            break; 
                        }
                }
            }
        }

        private void PB_FirstImg_MouseLeave(object sender, EventArgs e)
        {
            Pnl_Shadow_FirstImg.BackColor = Color.Gainsboro;
        }

        private void PB_FirstImg_Click(object sender, EventArgs e)
        {
            btn_SignAll_Close_Click(null, null);
            pnl_MainImage.Visible = true;
            isZoomIN = false;
            btn_NextSign.Enabled = true;
            btn_PreviousSign.Enabled = true;
            btn_FirstSign.Enabled = true;
            btn_LastSign.Enabled = true;
            
            Set_ImageToPB(true);
        }

        private void Cnt_Signature_Runtime_MouseDownEvent(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;  
            if (File.Exists(ExistingPictureName) == true)
            {
                this.Cursor = Cursors.Default;  
                DialogResult drt = MessageBox.Show("Signature already exist!, Do you want to over write?", "ESignature", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (drt == DialogResult.Yes) { TakeSignature(Cnt_Signature_Runtime); }
            }
            else
            {
                this.Cursor = Cursors.Default;  
                TakeSignature(Cnt_Signature_Runtime);
            }
            ///MessageBox.Show("Left: " + pnl_Signature.Left.ToString() + ", Top: " + pnl_Signature.Top.ToString());
        }

        private void Cnt_PreviewSignature_MouseDownEvent(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (File.Exists(ExistingPictureName) == true)
            {
                this.Cursor = Cursors.Default;
                DialogResult drt = MessageBox.Show("Signature already exist!, Do you want to over write?", "ESignature", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (drt == DialogResult.Yes) { TakeSignature(Cnt_PreviewSignature); }
            }
            else
            {
                this.Cursor = Cursors.Default;
                TakeSignature(Cnt_PreviewSignature);
            }
            ///MessageBox.Show("Left: " + pnl_Signature.Left.ToString() + ", Top: " + pnl_Signature.Top.ToString());
        }

        private void MoveToScannedDoc(string FileName, int DocID)
        {
            string[] key = { "@TicketID", "@EmployeeID", "@DocID"};
            object[] value1 = { TicketID, EmployeeID, DocID};
            //call sP and get the book ID back from sP
            DataTable dt_BookID = new DataTable();
            dt_BookID=cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Insert_ScanDoc", key, value1).Tables[0] ;
            
            //Move file
            ScannedDocStoragePath = ScannedDocStoragePath + dt_BookID.Rows[0][0].ToString() + ".PDF"; //DestinationDoc

            System.IO.File.Copy(FileName, ScannedDocStoragePath);
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { Set_PBFocus(); return; }
            if (FirstSelectFlag_Bond == true && FirstSelectFlag_ContLetter == true && FirstSelectFlag_LetterRep == true && FirstSelectFlag_Receipt == true && FirstSelectFlag_TrialLetter == true)
            { MessageBox.Show("No any document has signature(s), initial(s) or thumbprint(s)! Document(s) not saved", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning); Set_PBFocus(); return; }
            this.Cursor = Cursors.WaitCursor;
            RefreshReports(); 
            string DocPath = "";
            if (FirstSelectFlag_Receipt == false)
            {
                DocPath = System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Year.ToString() + " " + System.DateTime.Now.ToLongTimeString().Replace(":", "") + "-" + TicketID + "-Receipt.pdf";
                if (MoveDocsToServer(DocPath, @"\Receipt.pdf", 9) == true)
                {
                    string[] parameterKeys = { "@TicketID", "@LetterID", "@EmployeeID", "@Notes", "@DocPath" };
                    object[] values = { TicketID, 3, EmployeeID, "Receipt Signed", DocPath };
                    cls_DataBase.ExecuteSP("USP_HTS_ESignature_MakeHistory", parameterKeys, values);
                }
            }
            if (FirstSelectFlag_Bond == false)
            {
                if (BondInside_Flag == true || BondOutside_Flag == true)
                {
                    DocPath = System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Year.ToString() + " " + System.DateTime.Now.ToLongTimeString().Replace(":", "") + "-" + TicketID + "-Bond.pdf";
                    if (MoveDocsToServer(DocPath, @"\BondDoc.pdf", 8) == true)
                    {
                        string[] parameterKeys = { "@TicketID", "@LetterID", "@EmployeeID", "@Notes", "@DocPath" };
                        object[] values = { TicketID, 4, EmployeeID, "Bond Signed", DocPath };
                        cls_DataBase.ExecuteSP("USP_HTS_ESignature_MakeHistory", parameterKeys, values);
                    }
                }
            }
            if (FirstSelectFlag_LetterRep == false)
            {
                if (LetterRep_Flag == true)
                {
                    DocPath = System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Year.ToString() + " " + System.DateTime.Now.ToLongTimeString().Replace(":", "") + "-" + TicketID + "-LetterofRep.pdf";
                    if (MoveDocsToServer(DocPath, @"\LetterOfRep.pdf", 11) == true)
                    {
                        string[] parameterKeys = { "@TicketID", "@LetterID", "@EmployeeID", "@Notes", "@DocPath" };
                        object[] values = { TicketID, 6, EmployeeID, "Letter of Rep Signed", DocPath };
                        cls_DataBase.ExecuteSP("USP_HTS_ESignature_MakeHistory", parameterKeys, values);
                    }
                }
            }
            if (FirstSelectFlag_TrialLetter == false)
            {
                if (TrialLetter_Flag == true)
                {
                    DocPath = System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Year.ToString() + " " + System.DateTime.Now.ToLongTimeString().Replace(":", "") + "-" + TicketID + "-TrailNotificationLetters.pdf";
                    if (MoveDocsToServer(DocPath, @"\LetterTrial.pdf", 11) == true)
                    {
                        string[] parameterKeys = { "@TicketID", "@LetterID", "@EmployeeID", "@Notes", "@DocPath" };
                        object[] values = { TicketID, 2, EmployeeID, "Trail Notification Letters Signed", DocPath };
                        cls_DataBase.ExecuteSP("USP_HTS_ESignature_MakeHistory", parameterKeys, values);
                    }
                }
            }
            if (FirstSelectFlag_ContLetter == false)
            {
                if (ContLetter_Flag == true)
                {
                    DocPath = System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Year.ToString() + " " + System.DateTime.Now.ToLongTimeString().Replace(":", "") + "-" + TicketID + "-CONTINUANCELetter.pdf";
                    if (MoveDocsToServer(DocPath, @"\LetterContinuance.pdf", 2) == true)
                    {
                        string[] parameterKeys = { "@TicketID", "@LetterID", "@EmployeeID", "@Notes", "@DocPath" };
                        object[] values = { TicketID, 5, EmployeeID, "CONTINUANCE Letter Signed", DocPath };
                        cls_DataBase.ExecuteSP("USP_HTS_ESignature_MakeHistory", parameterKeys, values);
                    }
                }
            }
            this.Cursor = Cursors.Default;
            MessageBox.Show("Document(s) saved and copied to server! successfully!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            Flag_DocumentShouldBeSaved = false;
            Set_PBFocus();
        }

        private bool MoveDocsToServer(string DocPath, string DocName, int DocID)
        {
            DataSet ds_Configuration = new DataSet();
            ds_Configuration = cls_DataBase.ExecuteSQLQuery_ReturnDataSet("Select * From Tbl_HTS_ESignature_Configuration");
            string FileUser = ds_Configuration.Tables[0].Rows[0]["UserName"].ToString();
            string FilePassword = ds_Configuration.Tables[0].Rows[0]["Password"].ToString();
            string FileServer = ds_Configuration.Tables[0].Rows[0]["ServerName"].ToString();
            string DocStoragePath = ds_Configuration.Tables[0].Rows[0]["DocStoragePath"].ToString();
            ScannedDocStoragePath = ds_Configuration.Tables[0].Rows[0]["ScannedDocStoragePath"].ToString();
            bool Result = false;
            IntPtr lnToken;
            int TResult = LogonUser(FileUser, FileServer, FilePassword, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_DEFAULT, out lnToken);
            if (TResult > 0)
            {
                ImpersonateLoggedOnUser(lnToken);
                try
                {
                    File.Copy(ApplicationPath + DocName, DocStoragePath + DocPath, true);
                    
                    RevertToSelf();
                    CloseHandle(lnToken);
                    Result = true;
                    MoveToScannedDoc(DocStoragePath + DocPath, DocID); 
                }
                catch(Exception ex)
                {
                    RevertToSelf();
                    CloseHandle(lnToken);
                    Result = false;
                }
            }
            return Result;
        }

        private bool MakeThumbprintTransparent(string FilePath)
        {
            Bitmap objimg = null;
            try { objimg = new Bitmap(FilePath); }
            catch (Exception ex)
            {
                MessageBox.Show("Device not attached!, OR not a valid image file!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                return false;
            }
            Color objColor = new Color();
            for (int OL_H = 0; OL_H < objimg.Size.Width; OL_H++)
            {
                for (int IL_W = 0; IL_W < objimg.Size.Height; IL_W++)
                {
                    objColor = objimg.GetPixel(OL_H, IL_W);

                    if (objColor.R > 120 && objColor.G > 120 && objColor.B > 120)
                    {
                        //objimg.MakeTransparent(objColor);
                        objimg.SetPixel(OL_H, IL_W, Color.White); 
                    }
                }
            }
            objimg.Save(ApplicationPath + @"\FPTP" + TicketID + ".JPG");
            objimg.Dispose();
            File.Delete(ApplicationPath + @"\FP" + TicketID + ".JPG"); 
            File.Move(ApplicationPath + @"\FPTP" + TicketID + ".JPG", ApplicationPath + @"\FP" + TicketID + ".JPG");
            Cnt_FPAll.Visible = false;
            pb_FP_Main.ImageLocation = FilePath;
            return true;
        }

        private void btn_PrintAll_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { Set_PBFocus(); return; }
            PrintAllReports();
            Set_PBFocus();
        }

        private void btn_PrintCurrentReport_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { Set_PBFocus(); return; }
            PrintCurrentReport();
            Set_PBFocus();
        }

        private void btn_SignAll_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { return; }
            pnl_SignAll.Visible = true;
            pnl_Signature.Visible = false;
            btn_DefaultSignature_Open.Enabled =false;
            btn_ClearFingerprint.Enabled = false; 
            if (LoginEntity == "Client")
            { rb_Client.Checked = true; }
            else if (LoginEntity == "Attorney")
            { rb_Attorney.Checked = true; }
            else if (LoginEntity == "SalesRep")
            { rb_SalesRep.Checked = true; }
        }

        private void pb_Signature_Click(object sender, EventArgs e)
        {
            pb_Signature_Back.BackColor = Color.Gold;
            pb_Initial_Back.BackColor = Color.DodgerBlue;
            pb_Fingerprint_Back.BackColor = Color.DodgerBlue;
            btn_DefaultSignature_Open.Enabled = true;
            btn_ClearFingerprint.Enabled = false;
            btn_SignAll_InsertSignature.Enabled = false; 
            pb_FP_Main.Visible =false; 
            pb_Signature_Main.Controls.Clear(); 
            pb_Signature_Main.Visible = true;
            Cnt_SignAll_Runtime = new AxeSign3.AxesCapture();
            Cnt_SignAll_Runtime.SignCompleteStatus += new AxeSign3._ICaptureEvents_SignCompleteStatusEventHandler(Cnt_SignAll_Runtime_SignCompleteStatus);
            this.Font = new Font(this.Font.FontFamily.Name, 8, GraphicsUnit.Point);
            pb_Signature_Main.Controls.Add(Cnt_SignAll_Runtime);
            Cnt_SignAll_Runtime.Width = pb_Signature_Main.Width - 6;
            Cnt_SignAll_Runtime.Height = pb_Signature_Main.Height - 6;
            Cnt_SignAll_Runtime.Left = 3;
            Cnt_SignAll_Runtime.Top = 3;
            Cnt_SignAll_Runtime.SetSignerDetailsEx(lbl_Name.Text, "", "", "", "", "", "", "Sign Here");
            Cnt_SignAll_Runtime.SetSignRect(Cnt_SignAll_Runtime.Width, Cnt_SignAll_Runtime.Height);
            Cnt_SignAll_Runtime.ClearSign();
            Cnt_SignAll_Runtime.Select();
            Cnt_SignAll_Runtime.DisplayName = "W A I T"; 
            Cnt_SignAll_Runtime.StartSign(eSign3.ButtonStyle.BT_OK_CANCEL_CLEAR, 1);
        }

        private void pb_Initial_Click(object sender, EventArgs e)
        {
            pb_Signature_Back.BackColor = Color.DodgerBlue;
            pb_Initial_Back.BackColor = Color.Gold;
            pb_Fingerprint_Back.BackColor = Color.DodgerBlue;
            btn_DefaultSignature_Open.Enabled = false;
            btn_ClearFingerprint.Enabled = false ;
            btn_SignAll_InsertSignature.Enabled = false; 
            pb_FP_Main.Visible = false;
            pb_Signature_Main.Controls.Clear();
            pb_Signature_Main.Visible = true;
            Cnt_SignAll_Runtime = new AxeSign3.AxesCapture();
            Cnt_SignAll_Runtime.SignCompleteStatus += new AxeSign3._ICaptureEvents_SignCompleteStatusEventHandler(Cnt_SignAll_Runtime_SignCompleteStatus);
            this.Font = new Font(this.Font.FontFamily.Name, 8, GraphicsUnit.Point);
            pb_Signature_Main.Controls.Add(Cnt_SignAll_Runtime);
            Cnt_SignAll_Runtime.Width = pb_Signature_Main.Width - 6;
            Cnt_SignAll_Runtime.Height = pb_Signature_Main.Height - 6;
            Cnt_SignAll_Runtime.Left = 3;
            Cnt_SignAll_Runtime.Top = 3;
            Cnt_SignAll_Runtime.SetSignerDetailsEx(lbl_Name.Text, "", "", "", "", "", "", "Sign Here");
            Cnt_SignAll_Runtime.SetSignRect(Cnt_SignAll_Runtime.Width, Cnt_SignAll_Runtime.Height);
            Cnt_SignAll_Runtime.ClearSign();
            Cnt_SignAll_Runtime.Select();
            Cnt_SignAll_Runtime.DisplayName = "W A I T"; 
            Cnt_SignAll_Runtime.StartSign(eSign3.ButtonStyle.BT_OK_CANCEL_CLEAR, 1);
        }

        private void pb_Fingerprint_Click(object sender, EventArgs e)
        {
            pb_Signature_Back.BackColor = Color.DodgerBlue;
            pb_Initial_Back.BackColor = Color.DodgerBlue;
            pb_Fingerprint_Back.BackColor = Color.Gold;
            btn_DefaultSignature_Open.Enabled = false;
            btn_ClearFingerprint.Enabled = true;
            btn_SignAll_InsertSignature.Enabled =true ; 
            pb_Signature_Main.Visible = false;
            pb_FP_Main.Controls.Clear();
            pb_FP_Main.Visible = true;
            this.Font = new Font(this.Font.FontFamily.Name, 8, GraphicsUnit.Point);
            pb_FP_Main.Controls.Add(Cnt_FPAll);
            Cnt_FPAll.Visible = true;
            Cnt_FPAll.Width = pb_FP_Main.Width - 6;
            Cnt_FPAll.Height = pb_FP_Main.Height - 6;
            Cnt_FPAll.Left = 3;
            Cnt_FPAll.Top = 3;
            Cnt_FPAll.ClearCapture();
            Cnt_FPAll.Select();
            Cnt_FPAll.EnableFPDevice();
        }

        private void btn_NextSign_Click(object sender, EventArgs e)
        {
            string PreviousImageTrace = "";
            try
            {
                PreviousImageTrace = pnl_Signature.Tag.ToString();
            }
            catch { return; }
            if (isAnyDocSelected == false) { return; }
            if (pnl_Signature.Tag == null) { MessageBox.Show("No any signature found for client", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            GotoFirstSign = true;
            LocateSignature(pnl_Signature.Tag.ToString(), 1); 
            if (pnl_Signature.Tag.ToString() == PreviousImageTrace)
            {
                if (CurrentPageNumber != pageCount)
                {
                    btn_Page_Next_Click(null, null);
                    Set_ImageToPB(true);
                }
            }
        }

        private void btn_PreviousSign_Click(object sender, EventArgs e)
        {
            string PreviousImageTrace = "";
            try
            {
                PreviousImageTrace = pnl_Signature.Tag.ToString();
            }
            catch { return; }
           
            if (isAnyDocSelected == false) { return; }
            if (pnl_Signature.Tag == null) { MessageBox.Show("No any signature found for client", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            GotoFirstSign = false;
            LocateSignature(pnl_Signature.Tag.ToString(), 2);
            if (pnl_Signature.Tag.ToString() == PreviousImageTrace)
            {
                btn_Page_Previous_Click(null, null);
                Set_ImageToPB(true);
            }
        }

        private void btn_SignAll_Close_Click(object sender, EventArgs e)
        {
            rb_DefaultSign.Enabled = false;
            rb_Attorney.Checked = false;
            rb_Client.Checked = false;
            rb_SalesRep.Checked = false;
            btn_DefaultSignature_Open.Enabled = false;
            btn_ClearFingerprint.Enabled = false;
            btn_SignAll_InsertSignature.Enabled = false; 
            pb_Fingerprint_Back.BackColor = Color.DodgerBlue;
            pb_Signature_Back.BackColor = Color.DodgerBlue;
            pb_Initial_Back.BackColor = Color.DodgerBlue;
            Cnt_FPAll.Dispose();  
            pb_FP_Main.ImageLocation = null;
            pb_FP_Main.Visible = false;
            pb_Signature_Main.Visible = false;
            pnl_SignAll.Visible = false;
            pnl_Signature.Visible =false;
            Set_PBFocus();
        }

        private void Cnt_Signature_Runtime_SignCompleteStatus(object sender, AxeSign3._ICaptureEvents_SignCompleteStatusEvent e)
        {
            Cnt_Signature_Runtime.Select();  
            int ScrollValue = pnl_MainImage.VerticalScroll.Value;  
            if (Cnt_Signature_Runtime.IsSigned == 1)
            {
                string ViolationNumber = "1";
                try { ViolationNumber = dt_BondPages.Rows[CurrentPageNumber - 1]["ViolationNumber"].ToString(); }catch { }
                System.Array byte_Con = Cnt_Signature_Runtime.GetImageBytes(190, 70, eSign3.FILETYPE.JPEG,55);
                
                byte[] byte_Content = (byte[])byte_Con;
                if (cls_DataBase.SPInsertImage("Update Tbl_HTS_ESignature_Images Set ImageStream = convert(varbinary(8000),@Image) where ImageTrace = '" + pnl_Signature.Tag.ToString() + "' And TicketID = '" + TicketID + "' And PageNumber = " + CurrentPageNumber + " And SessionID = '" + SessionID + "'", "Insert Into Tbl_HTS_ESignature_Images (TicketID, ImageStream, ImageTrace, PageNumber, SessionID) Values('" + TicketID + "', @Image, '" + pnl_Signature.Tag.ToString() + "', " + CurrentPageNumber + ", '" + SessionID + "')", byte_Content) == true)
                {
                    if (pnl_Signature.Tag.ToString().IndexOf("Initial") > 1)
                    {
                        Cnt_Signature_Runtime.SaveToFile(ApplicationPath + @"\" + SelectionOrder  + "RunTimeSign" + PB_MainImage.ImageLocation.Substring (PB_MainImage.ImageLocation.LastIndexOf("\\")+1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1)  + "Top" + pnl_Signature.Top.ToString() + "Left" + pnl_Signature.Left.ToString() + ".GIF", 95, 35, eSign3.FILETYPE.GIF, 55, 1);
                    }
                    else
                    {
                        Cnt_Signature_Runtime.SaveToFile(ApplicationPath + @"\" + SelectionOrder + "RunTimeSign" + PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1) + "Top" + pnl_Signature.Top.ToString() + "Left" + pnl_Signature.Left.ToString() + ".GIF", 190, 70, eSign3.FILETYPE.GIF, 55, 1);
                    }

                    pnl_Signature.Visible = false;

                    Flag_DocumentShouldBeSaved = true;

                    switch (tab_Docs.SelectedTab.Name.ToString())
                    {
                        case "TabPg_BondDocs":
                            {
                                FirstSelectFlag_Bond = false;
                                //RefreshFlag_Bond = false;
                                break;
                            }
                        case "TabPg_LetterOfContinuation":
                            {
                                FirstSelectFlag_ContLetter = false;
                                //RefreshFlag_ContLetter = false;
                                break;
                            }
                        case "TabPg_TrailLetter":
                            {
                                FirstSelectFlag_TrialLetter = false;
                                //RefreshFlag_TrialLetter = false;
                                break;
                            }
                        case "TabPg_LetterOfRep":
                            {
                                FirstSelectFlag_LetterRep = false;
                                //RefreshFlag_LetterRep = false;
                                break;
                            }
                        case "tabPg_Receipt":
                            {
                                FirstSelectFlag_Receipt = false;
                                //RefreshFlag_Receipt = false;
                                break;
                            }
                    }
                }
                else
                {
                    MessageBox.Show("Error", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                pnl_Signature.Visible = false;
                pnl_Signature_Preview.Visible = false; 
            }
            UpdateSignedFieldsCounter();
            PB_MainImage.Select();
            PB_MainImage.Controls.Clear(); 
            pnl_MainImage.VerticalScroll.Value = ScrollValue;
        }

        private void Cnt_PreviewSignature_SignCompleteStatus(object sender, AxeSign3._ICaptureEvents_SignCompleteStatusEvent e)
        {
            if (Cnt_PreviewSignature.IsSigned == 1)
            {
                string ViolationNumber = "1";
                try { ViolationNumber = dt_BondPages.Rows[CurrentPageNumber - 1]["ViolationNumber"].ToString(); }
                catch { }
                System.Array byte_Con = Cnt_PreviewSignature.GetImageBytes(190, 70, eSign3.FILETYPE.JPEG, 55);

                byte[] byte_Content = (byte[])byte_Con;
                if (cls_DataBase.SPInsertImage("Update Tbl_HTS_ESignature_Images Set ImageStream = convert(varbinary(8000),@Image) where ImageTrace = '" + pnl_Signature.Tag.ToString() + "' And TicketID = '" + TicketID + "' And PageNumber = " + CurrentPageNumber + " And SessionID = '" + SessionID + "'", "Insert Into Tbl_HTS_ESignature_Images (TicketID, ImageStream, ImageTrace, PageNumber, SessionID) Values('" + TicketID + "', @Image, '" + pnl_Signature.Tag.ToString() + "', " + CurrentPageNumber + ", '" + SessionID + "')", byte_Content) == true)
                {
                    if (pnl_Signature.Tag.ToString().IndexOf("Initial") > 1)
                    {
                        Cnt_PreviewSignature.SaveToFile(ApplicationPath + @"\" + SelectionOrder + "RunTimeSign" + PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1) + "Top" + pnl_Signature.Top.ToString() + "Left" + pnl_Signature.Left.ToString() + ".GIF", 95, 35, eSign3.FILETYPE.GIF, 55, 1);
                    }
                    else
                    {
                        Cnt_PreviewSignature.SaveToFile(ApplicationPath + @"\" + SelectionOrder + "RunTimeSign" + PB_MainImage.ImageLocation.Substring(PB_MainImage.ImageLocation.LastIndexOf("\\") + 1, PB_MainImage.ImageLocation.LastIndexOf(".") - PB_MainImage.ImageLocation.LastIndexOf("\\") - 1) + "Top" + pnl_Signature.Top.ToString() + "Left" + pnl_Signature.Left.ToString() + ".GIF", 190, 70, eSign3.FILETYPE.GIF, 55, 1);
                    }

                    Flag_DocumentShouldBeSaved = true;

                    switch (tab_Docs.SelectedTab.Name.ToString())
                    {
                        case "TabPg_BondDocs":
                            {
                                FirstSelectFlag_Bond = false;
                                //RefreshFlag_Bond = false;
                                break;
                            }
                        case "TabPg_LetterOfContinuation":
                            {
                                FirstSelectFlag_ContLetter = false;
                                //RefreshFlag_ContLetter = false;
                                break;
                            }
                        case "TabPg_TrailLetter":
                            {
                                FirstSelectFlag_TrialLetter = false;
                                //RefreshFlag_TrialLetter = false;
                                break;
                            }
                        case "TabPg_LetterOfRep":
                            {
                                FirstSelectFlag_LetterRep = false;
                                //RefreshFlag_LetterRep = false;
                                break;
                            }
                        case "tabPg_Receipt":
                            {
                                FirstSelectFlag_Receipt = false;
                                //RefreshFlag_Receipt = false;
                                break;
                            }
                    }
                }
                else
                {
                    MessageBox.Show("Error", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                pnl_Signature_Preview.Visible = false;
                pnl_Signature.Visible = false; 
            }
            UpdateSignedFieldsCounter();
            PB_FirstImg.Select();
            PB_FirstImg.Controls.Clear();  
        }

        private void Cnt_SignAll_Runtime_SignCompleteStatus(object sender, AxeSign3._ICaptureEvents_SignCompleteStatusEvent e)
        {
            AxeSign3.AxesCapture Cnt_SignAll_Runtime = (AxeSign3.AxesCapture)sender;
            if (Cnt_SignAll_Runtime.IsSigned == 1)
            {
                System.Array byte_Con = Cnt_SignAll_Runtime.GetImageBytes(150, 100, eSign3.FILETYPE.JPEG, 55);
                byte[] byte_Content = (byte[])byte_Con;
                if (cls_DataBase.SPInsertImage("Update Tbl_HTS_ESignature_Images Set ImageStream = convert(varbinary(8000),@Image) where ImageTrace = 'Current Sign' And TicketID = '" + TicketID + "'" + " And SessionID = '" + SessionID + "'", "Insert Into Tbl_HTS_ESignature_Images (TicketID, ImageStream, ImageTrace, SessionID) Values('" + TicketID + "', @Image, 'Current Sign', '" + SessionID + "')", byte_Content) == false)
                {
                    MessageBox.Show("Signature Not Saved! Sorry", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                btn_SignAll_InsertSignature.Enabled =true ; 
            }
            Cnt_SignAll_Runtime.DisplayName = "CLICK ICON TO RESTART"; 
        }

        private void btn_SignAll_InsertSignature_Click(object sender, EventArgs e)
        {
            if (rb_SelectedDocSign.Checked == true)
            {
                SignAll(1);
            }
            else if (rb_AllDocSign.Checked == true)
            {
                SignAll(0);
            }
            if (rb_DefaultSign.Checked == true)
            {
                if (Cnt_SignAll_Runtime.IsSigned == 1)
                {
                    if (pb_Signature_Back.BackColor != Color.Gold) { MessageBox.Show("Select signature control instead of initial or thumbprint!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                    string Entity = "";
                    string SignID = "0";
                    if (rb_Attorney.Checked == true) { Entity = "Attorney"; }
                    else if (rb_SalesRep.Checked == true) { Entity = "SalesRep"; SignID = EmployeeID; }
                    else { MessageBox.Show("Select any one of Attorney or Sales Rep entities", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }

                    System.Array byte_Con = Cnt_SignAll_Runtime.GetImageBytes(150, 100, eSign3.FILETYPE.JPEG, 55);
                    byte[] byte_Content = (byte[])byte_Con;
                    string SignData = "";
                    SignData = Cnt_SignAll_Runtime.GetSignData();
                    if (cls_DataBase.SPInsertImage("Update Tbl_HTS_ESignature_DefaultSignatures Set ImageStream = convert(varbinary(8000),@Image), SignData = '" + SignData + "' where Entity = '" + Entity + "' And SignID = '" + SignID + "'", "Insert Into Tbl_HTS_ESignature_DefaultSignatures (Entity, ImageStream, SignData, SignID) Values('" + Entity + "', @Image, '" + SignData + "', '" + SignID + "')", byte_Content) == true)
                    {
                        MessageBox.Show("Default signature has been saved successfully!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        btn_DefaultSignature_Open.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Error", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void SignAll(int CurrentDoc)
        {
            string Entity = "";
            string SignID = "0";

            if (rb_Attorney.Checked == false && rb_Client.Checked == false && rb_SalesRep.Checked == false) { MessageBox.Show("Select any entity! Attorney, Client or Sales Rep", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
            if ((pb_Initial_Back.BackColor == Color.Gold || pb_Fingerprint_Back.BackColor == Color.Gold) && rb_Client.Checked == false)
            { MessageBox.Show("Only client can make the initial(s) or finger-print(s)! Please select client entity to make initial(s) or finger-print(s)", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            if ((pb_Initial_Back.BackColor == Color.Gold || pb_Fingerprint_Back.BackColor == Color.Gold) && (tab_Docs.SelectedTab.Name != "TabPg_BondDocs" && CurrentDoc ==1))
            { MessageBox.Show("Only bond document has initial(s) or finger-print(s)! Please select bond document tab to make initial(s) or finger-print(s) or use Apply 2 All Documents", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            if (rb_Attorney.Checked == true) { Entity = "Attorney"; SignID = "0"; }
            else if (rb_Client.Checked == true)
            {
                SignID = "999999";
                if (pb_Signature_Back.BackColor == Color.Gold)
                {
                    Entity = "ClientSign";
                }
                else if (pb_Initial_Back.BackColor == Color.Gold)
                {
                    Entity = "ClientInitial";
                }
                else if (pb_Fingerprint_Back.BackColor == Color.Gold)
                {
                    Entity = "ClientThumbprint";
                    Cnt_FPAll.SaveFPImage(ApplicationPath + @"\FP" + TicketID + ".JPG", ESFINGERPRINTLib.FILETYPE.JPEG);
                    if (MakeThumbprintTransparent(ApplicationPath + @"\FP" + TicketID + ".JPG") == false) { return; };
                    File.Copy(ApplicationPath + @"\FP" + TicketID + ".JPG", ApplicationPath + @"\CurrentImage.GIF", true);
                    System.Array byte_Con = Cnt_FPAll.GetImageBytes(ApplicationPath + @"\FP" + TicketID + ".JPG");
                    byte[] byte_Content = (byte[])byte_Con;
                    if (cls_DataBase.SPInsertImage("Update Tbl_HTS_ESignature_Images Set ImageStream = convert(varbinary(8000),@Image) where ImageTrace = 'Current Sign' And TicketID = '" + TicketID + "' And SessionID = '" + SessionID + "'", "Insert Into Tbl_HTS_ESignature_Images (TicketID, ImageStream, ImageTrace, PageNumber, SessionID) Values('" + TicketID + "', @Image, 'Current Sign', 0, '" + SessionID + "')", byte_Content) == false)
                    {
                        MessageBox.Show("Thumbprint Not Saved! Sorry", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (rb_SalesRep.Checked == true) { Entity = "SalesRep"; SignID = EmployeeID; }

            string[] parameterKeys = { "@TicketID", "@Entity", "@KeyWord", "@CurrentDoc", "@ClientLanguage", "@ForDefaultSigns", "@SignID", "@SessionID" };
            object[] values = { TicketID, Entity, SignAll_KeyWord, CurrentDoc, ClientLanguage, 0, SignID, SessionID };
             
            if (cls_DataBase.ExecuteSP("USP_HTS_ESignature_Insert_SignAll", parameterKeys, values) == 0)
            {
                MessageBox.Show("Signature(s)/Fingerprint(s) not saved! Sorry", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Error);  
            }
            else
            {
                MessageBox.Show("Signature(s)/Fingerprint(s) saved successfully", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (CurrentDoc == 0)
                {
                    FirstSelectFlag_Bond = false; FirstSelectFlag_Receipt = false; FirstSelectFlag_ContLetter = false; FirstSelectFlag_LetterRep = false; FirstSelectFlag_TrialLetter = false;
                    //RefreshFlag_Bond = true; RefreshFlag_Receipt = true; RefreshFlag_ContLetter = true; RefreshFlag_LetterRep = true; RefreshFlag_TrialLetter = true;
                }
                else if (tab_Docs.SelectedTab.Name == "TabPg_BondDocs") { FirstSelectFlag_Bond = false;}// RefreshFlag_Bond = true; }
                else if (tab_Docs.SelectedTab.Name == "tabPg_Receipt") { FirstSelectFlag_Receipt = false; }// RefreshFlag_Receipt = true; }
                else if (tab_Docs.SelectedTab.Name == "TabPg_LetterOfContinuation") { FirstSelectFlag_ContLetter = false; }// RefreshFlag_ContLetter = true; }
                else if (tab_Docs.SelectedTab.Name == "TabPg_TrailLetter") { FirstSelectFlag_TrialLetter = false; }// RefreshFlag_TrialLetter = true; }
                else if (tab_Docs.SelectedTab.Name == "TabPg_LetterOfRep") { FirstSelectFlag_LetterRep = false; }// RefreshFlag_LetterRep = true; }
                Flag_DocumentShouldBeSaved = true;
                switch (tab_Docs.SelectedTab.Name.ToString())
                {
                    case "TabPg_BondDocs":
                        {
                            MakeSignAll_Images("BondDoc", Entity, 2);
                            break;
                        }
                    case "TabPg_LetterOfContinuation":
                        {
                            MakeSignAll_Images("LetterContinuance", Entity, 2);
                            break;
                        }
                    case "TabPg_TrailLetter":
                        {
                            MakeSignAll_Images("LetterTrial", Entity, 2);
                            break;
                        }
                    case "TabPg_LetterOfRep":
                        {
                            MakeSignAll_Images("LetterOfRep", Entity, 1);
                            break;
                        }
                    case "tabPg_Receipt":
                        {
                            MakeSignAll_Images("Receipt", Entity, 1);
                            break;
                        }
                }
                
                LocateImage();
                UpdateSignedFieldsCounter();
                //GetData(true);
                btn_SignAll_Close_Click(null, null);
            }
        }

        private void MakeSignAll_Images(string DocName, string Entity, int MultiPage)
        { 
                string[] parameterKeys = { "@DocumentName", "@Entity", "@ClientLanguage", "@PageCount", "@SessionID" };
                object[] values = { DocName, Entity , ClientLanguage, MultiPage, SessionID };
                DataTable dt_ImageFileNames = new DataTable();
                dt_ImageFileNames = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_GetSignAll_ImageFileNames", parameterKeys, values).Tables[0];
                if (pb_Initial_Back.BackColor == Color.Gold)
                {
                    Cnt_SignAll_Runtime.SaveToFile(ApplicationPath + @"\CurrentImage.GIF", 95, 35, eSign3.FILETYPE.GIF, 55, 1);
                }
                else if (pb_Signature_Back.BackColor == Color.Gold)
                { 
                    Cnt_SignAll_Runtime.SaveToFile(ApplicationPath + @"\CurrentImage.GIF", 190, 70, eSign3.FILETYPE.GIF, 55, 1);
                }

                for (int LoopCounter = 0; LoopCounter < dt_ImageFileNames.Rows.Count;LoopCounter ++ )
                {
                    string ImageFileName = dt_ImageFileNames.Rows[LoopCounter]["ImageFileName"].ToString();
                    
                    File.Copy(ApplicationPath + @"\CurrentImage.GIF",ApplicationPath+ @"\"+ ImageFileName,true);
                }
        }

        private void UpdateSignedFieldsCounter()
        {
            string[] parameterKeys = { "@TicketID", "@ClientLanguage", "@SessionID"};
            object[] values = { TicketID, ClientLanguage, SessionID };
            DataTable dtSignedFieldsCounter = new DataTable();
            dtSignedFieldsCounter = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_SignedFieldsCounters", parameterKeys, values).Tables[0];
            if (dtSignedFieldsCounter.Rows.Count != 0)
            {
                if (UserSelectedFlag_Receipt == true) { try { tab_Docs.TabPages["tabPg_Receipt"].Text = dtSignedFieldsCounter.Rows[0]["Receipt"].ToString(); } catch { }}
                if (UserSelectedFlag_ContLetter == true) { try { tab_Docs.TabPages["TabPg_LetterOfContinuation"].Text = dtSignedFieldsCounter.Rows[0]["LetterOfContinuation"].ToString();} catch { }}
                if (UserSelectedFlag_Bond == true) { try { tab_Docs.TabPages["TabPg_BondDocs"].Text = dtSignedFieldsCounter.Rows[0]["BondDocs"].ToString();} catch { }}
                if (UserSelectedFlag_TrialLetter == true) { try { tab_Docs.TabPages["TabPg_TrailLetter"].Text = dtSignedFieldsCounter.Rows[0]["TrialLetter"].ToString();} catch { }}
                if (UserSelectedFlag_LetterRep == true) { try { tab_Docs.TabPages["TabPg_LetterOfRep"].Text = dtSignedFieldsCounter.Rows[0]["LetterOfRep"].ToString(); } catch { } }
            }
        }

        private void OpenDefaultSignature()
        {
            string Entity = "";
            string SignID = "0";
            if (rb_Attorney.Checked == true) { Entity = "Attorney"; }
            else if (rb_SalesRep.Checked == true) { Entity = "SalesRep"; SignID = EmployeeID; }
            else { MessageBox.Show("First select the entity. Attorney or Sales Rep", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }

            DataTable dtPrestoredSign = new DataTable();
            string[] parameterKeys = { "@Entity", "@SignID" };
            object[] values = { Entity, SignID };
            
            dtPrestoredSign = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_PrestoredSignature", parameterKeys, values).Tables[0];
            if (dtPrestoredSign.Rows.Count != 0)
            {
                rb_DefaultSign.Enabled = true;
                pb_FP_Main.Visible = false;
                pb_Signature_Main.Controls.Clear();
                pb_Signature_Main.Visible = true;
                Cnt_SignAll_Runtime = new AxeSign3.AxesCapture();
                this.Font = new Font(this.Font.FontFamily.Name, 8, GraphicsUnit.Point);
                pb_Signature_Main.Controls.Add(Cnt_SignAll_Runtime);
                Cnt_SignAll_Runtime.Width = pb_Signature_Main.Width - 6;
                Cnt_SignAll_Runtime.Height = pb_Signature_Main.Height - 6;
                Cnt_SignAll_Runtime.Left = 3;
                Cnt_SignAll_Runtime.Top = 3;
                Cnt_SignAll_Runtime.SetSignerDetailsEx(Entity, "", "", "", "", "", "", "Sign Here");
                Cnt_SignAll_Runtime.SetSignRect(Cnt_SignAll_Runtime.Width, Cnt_SignAll_Runtime.Height);
                Cnt_SignAll_Runtime.ClearSign();
                Cnt_SignAll_Runtime.Select();

                Cnt_SignAll_Runtime.OpenSign(dtPrestoredSign.Rows[0]["SignData"].ToString());
            }
            else
            {
                MessageBox.Show("Default signature not found for the selected entity!", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pb_Signature_Main.Controls.Clear();
                pb_Signature_Main.Visible = false ;
            }
        }

        private void btn_DefaultSignature_Open_Click(object sender, EventArgs e)
        {
            OpenDefaultSignature();
        }

        private void btn_ClearFingerprint_Click(object sender, EventArgs e)
        {
            pb_Fingerprint_Back.BackColor = Color.DodgerBlue;
            pb_FP_Main.Visible = false;
            pb_Fingerprint_Back.Controls.Clear();
            btn_ClearFingerprint.Enabled = false;
            btn_SignAll_InsertSignature.Enabled = false; 
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == true)
            {
                try { Directory.Delete(ApplicationPath, true); }
                catch { }
            }
        }

        private void btn_FirstSign_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { return; }
            if (pnl_Signature.Tag == null) { MessageBox.Show("No any signature found for client", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            LocateSignature(pnl_Signature.Tag.ToString(), 0); 
        }

        private void btn_LastSign_Click(object sender, EventArgs e)
        {
            if (isAnyDocSelected == false) { return; }
            if (pnl_Signature.Tag == null) { MessageBox.Show("No any signature found for client", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            LocateSignature(pnl_Signature.Tag.ToString(), 3); 
        }

        private void PB_MainImage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.ToString() == "32")//Spacebar
            {
                btn_NextSign_Click(null, null);
            }
            else if (e.KeyValue.ToString() == "8")//Back Spacebar
            {
                btn_PreviousSign_Click(null, null);
            }
            else if (e.KeyValue.ToString() == "13")//Enter
            {
                Cnt_Signature_Runtime_MouseDownEvent(null, null);  
            }
            else if (e.KeyValue.ToString() == "27")//Scape
            {
                Cnt_Signature_Runtime.ClearSign();
                pnl_Signature.Controls.Clear();
                pnl_Signature.Visible = false;
                PB_MainImage.Select();  
            }
        }

        private void PB_FirstImage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.ToString() == "32")//Spacebar
            {
                btn_NextSign_Click(null, null);
            }
            else if (e.KeyValue.ToString() == "8")//Back Spacebar
            {
                btn_PreviousSign_Click(null, null);
            }
            else if (e.KeyValue.ToString() == "13")//Enter
            {
                Cnt_PreviewSignature_MouseDownEvent(null, null);
            }
            else if (e.KeyValue.ToString() == "27")//Scape
            {
                Cnt_PreviewSignature.ClearSign();
                pnl_Signature_Preview.Controls.Clear();
                pnl_Signature_Preview.Visible = false;
                PB_FirstImg.Select(); 
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnl_Signature.Visible = true;
            pnl_Signature.Top = pnl_Signature.Top - Convert.ToInt16 (textBox1.Text) ;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pnl_Signature.Visible = true;
            pnl_Signature.Left = pnl_Signature.Left  + Convert.ToInt16(textBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pnl_Signature.Visible = true;
            pnl_Signature.Left = pnl_Signature.Left - Convert.ToInt16(textBox1.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pnl_Signature.Visible = true;
            pnl_Signature.Top = pnl_Signature.Top + Convert.ToInt16(textBox1.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Left : " + pnl_Signature.Left.ToString() + ", Top :" + pnl_Signature.Top.ToString());  
        }

        private void PB_MainImage_Click(object sender, EventArgs e)
        {
            btn_SignAll_Close_Click(null, null); 
        }

        private void btn_SignAll_DeleteSignature_Click(object sender, EventArgs e)
        {
            DialogResult dr = new DialogResult();
            dr=MessageBox.Show("Are you sure to delete signature(s)?","ESignature",MessageBoxButtons.YesNo ,MessageBoxIcon.Question   ) ;
            if (dr == DialogResult.Yes)
            {
                if (rb_Attorney.Checked == true)
                {
                    if (rb_SelectedDocSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%Attorney%' And ImageTrace Like '%" + SignAll_KeyWord + "%' And TicketID = " + TicketID + " And SessionID = '" + SessionID + "'"); 
                    }
                    else if (rb_AllDocSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%Attorney%' And TicketID = " + TicketID + " And SessionID = '" + SessionID + "'"); 
                    }
                    else if (rb_DefaultSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From TBL_HTS_ESignature_DefaultSignatures Where SignID = 0");
                    }
                }
                else if (rb_SalesRep.Checked == true)
                {
                    if (rb_SelectedDocSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%SalesRep%' And ImageTrace Like '%" + SignAll_KeyWord + "%' And TicketID = " + TicketID + " And SessionID = '" + SessionID + "'");
                    }
                    else if (rb_AllDocSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%SalesRep%' And TicketID = " + TicketID + " And SessionID = '" + SessionID + "'");
                    }
                    else if (rb_DefaultSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From TBL_HTS_ESignature_DefaultSignatures Where SignID = " + EmployeeID);
                    }
                }
                else if (rb_Client.Checked == true)
                {
                    if (rb_SelectedDocSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%Client%' And ImageTrace Like '%" + SignAll_KeyWord + "%' And TicketID = " + TicketID + " And SessionID = '" + SessionID + "'");
                    }
                    else if (rb_AllDocSign.Checked == true)
                    {
                        cls_DataBase.ExecuteSQLQuery_ReturnString("Delete From Tbl_HTS_ESignature_Images Where ImageTrace Like '%Client%' And TicketID = " + TicketID + " And SessionID = '" + SessionID + "'");
                    }
                }
                //***************** To referesh reports **********
                if (rb_AllDocSign.Checked == true)
                {
                    FirstSelectFlag_Bond = false; FirstSelectFlag_Receipt = false; FirstSelectFlag_ContLetter = false; FirstSelectFlag_LetterRep = false; FirstSelectFlag_TrialLetter = false;
                    //RefreshFlag_Bond = true; RefreshFlag_Receipt = true; RefreshFlag_ContLetter = true; RefreshFlag_LetterRep = true; RefreshFlag_TrialLetter = true;
                    GetData(true);
                }
                else if (rb_SelectedDocSign.Checked == true)
                {
                    if (tab_Docs.SelectedTab.Name == "TabPg_BondDocs") { FirstSelectFlag_Bond = false; }// RefreshFlag_Bond = true; }
                    else if (tab_Docs.SelectedTab.Name == "tabPg_Receipt") { FirstSelectFlag_Receipt = false; }// RefreshFlag_Receipt = true; }
                    else if (tab_Docs.SelectedTab.Name == "TabPg_LetterOfContinuation") { FirstSelectFlag_Bond = false; }// RefreshFlag_ContLetter = true; }
                    else if (tab_Docs.SelectedTab.Name == "TabPg_TrailLetter") { FirstSelectFlag_Bond = false; }// RefreshFlag_TrialLetter = true; }
                    else if (tab_Docs.SelectedTab.Name == "TabPg_LetterOfRep") { FirstSelectFlag_Bond = false; }// RefreshFlag_LetterRep = true; }
                    btn_SignAll_Close_Click(null, null);
                    GetData(true);
                }
                //***********************************************
                MessageBox.Show("Signature(s) deleted successfully", "ESignature", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void ESign_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult DlgRes = new DialogResult();
            if (Flag_DocumentShouldBeSaved == true)
            {
                DlgRes = MessageBox.Show("Do you want to save document(s)?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (DlgRes == DialogResult.Yes)
                {
                    btn_Update_Click(null, null);
                    DlgRes = MessageBox.Show("Now do you want to close the application?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (DlgRes == DialogResult.No)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else if (DlgRes == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return ;
                }
            }
            try { Directory.Delete(ApplicationPath, true); }
            catch { }
        }
    }
}
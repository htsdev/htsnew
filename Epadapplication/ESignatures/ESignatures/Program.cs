using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ESignatures
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string[] Args = Environment.GetCommandLineArgs();
            ESign frmESign = new ESign();
            //frmESign.Opener = Args[1];
            frmESign.Opener = "124546-3991-TTTTT.995185845767931";
            Application.Run(frmESign);
        }
    }
}
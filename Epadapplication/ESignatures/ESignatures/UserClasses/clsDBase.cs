using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Windows.Forms; 

namespace clsDBase
{
    class clsData
    {
        string strCnn = "Server=srv-data;Database=traffictickets;User ID=traffic;Password=tickets;Trusted_Connection=False";
        //string strCnn = "Server=srv-dev;Database=TrafficTickets;User ID=traffic;Password=tickets;Trusted_Connection=False";

        public DataSet ExecuteSP_ReturnDataSet(string StoredProcedureName, string[] Parameters, object[] values)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlConnection Conn = new SqlConnection(strCnn);
                SqlCommand Sqlcmd = new SqlCommand();

                Sqlcmd.Connection = Conn;
                SqlDataAdapter SQLAD = new SqlDataAdapter(Sqlcmd);
                
                Sqlcmd.CommandType = CommandType.StoredProcedure;
                Sqlcmd.CommandText = StoredProcedureName;

                for (int a = 0; a <= Parameters.GetUpperBound(0); a++)
                {
                    SqlParameter p1 = new SqlParameter((string)Parameters.GetValue(a), GetDBType(values.GetValue(a).GetType().Name));
                    p1.Direction = ParameterDirection.Input;
                    p1.Value = values.GetValue(a);
                    Sqlcmd.Parameters.Add(p1);
                }

                SQLAD.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                if (ex.Source == ".Net SqlClient Data Provider")
                {
                    MessageBox.Show("SQL Server not valid! Or something wrong in its connection! Or Timeout Expired!");
                }
                else
                {
                    ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                }
                DataSet dd = null;
                return dd;
            }
        }

        public int ExecuteSP(string StoredProcedureName, string[] Parameters, object[] values)
        {
            try
            {
                SqlConnection Conn = new SqlConnection(strCnn);
                SqlCommand Sqlcmd = new SqlCommand();
                Conn.Open();
                int rows_Affected = 0;

                Sqlcmd.Connection = Conn;

                Sqlcmd.CommandType = CommandType.StoredProcedure;
                Sqlcmd.CommandText = StoredProcedureName;

                for (int a = 0; a <= Parameters.GetUpperBound(0); a++)
                {
                    SqlParameter p1 = new SqlParameter((string)Parameters.GetValue(a), GetDBType(values.GetValue(a).GetType().Name));
                    p1.Direction = ParameterDirection.Input;
                    p1.Value = values.GetValue(a);
                    Sqlcmd.Parameters.Add(p1);
                }
                rows_Affected = Sqlcmd.ExecuteNonQuery();
                return rows_Affected;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString()); 
                return 0;
            }
        }

        public DataSet ExecuteSQLQuery_ReturnDataSet(string SQLQuery)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlConnection Conn = new SqlConnection(strCnn);
                SqlCommand Sqlcmd = new SqlCommand();

                Sqlcmd.Connection = Conn;
                SqlDataAdapter SQLAD = new SqlDataAdapter(Sqlcmd);

                Sqlcmd.CommandType = CommandType.Text;
                Sqlcmd.CommandText = SQLQuery;

                SQLAD.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString());
                DataSet dd = null;
                return dd;
            }
        }

        public bool SPInsertImage(string Update_SQLCommand_String, string Insert_SQLCommand_String, byte[] Image_Content)
        {
            try
            {
                int rows_Affected = 0;
                SqlConnection Conn = new SqlConnection(strCnn);

                Conn.Open();
                SqlCommand Sqlcmd = new SqlCommand(Update_SQLCommand_String);
                Sqlcmd.Connection = Conn;

                SqlParameter imageParameter =
                Sqlcmd.Parameters.Add("@Image", SqlDbType.Binary);
                imageParameter.Value = Image_Content;
                imageParameter.Size = Image_Content.Length;
                rows_Affected = Sqlcmd.ExecuteNonQuery();
                if (rows_Affected == 0)
                {
                    SqlCommand SqlcmdInsert = new SqlCommand(Insert_SQLCommand_String);
                    SqlcmdInsert.Connection = Conn;

                    SqlParameter imageParameterInsert =
                    SqlcmdInsert.Parameters.Add("@Image", SqlDbType.Binary);
                    imageParameterInsert.Value = Image_Content;
                    imageParameterInsert.Size = Image_Content.Length;

                    rows_Affected = SqlcmdInsert.ExecuteNonQuery();
                }
                ExecuteSQLQuery_ReturnString("Delete from Tbl_HTS_ESignature_Images where datediff(d, timestamp, getdate())>1");
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message.ToString(), ex.TargetSite.ToString(), ex.StackTrace.ToString()); 
                //string s = ex.Message;
                return false;
            }
        }

        public string ExecuteSQLQuery_ReturnString(string SQLQuery)
        {
            try
            {
                SqlConnection Conn = new SqlConnection(strCnn);
                SqlCommand Sqlcmd = new SqlCommand();
                Conn.Open();
                Sqlcmd.Connection = Conn;
                Sqlcmd.CommandType = CommandType.Text;
                Sqlcmd.CommandText = SQLQuery;
                try
                {
                    SQLQuery = Sqlcmd.ExecuteScalar().ToString();
                }
                catch(Exception ex) { SQLQuery = ""; }
                Conn.Close();
                return SQLQuery;
            }
            catch (Exception ex)
            {
                return "No Records Found";
            }
        }

        private SqlCommand CreateCommand(string StoredProcedureName, string[] Parameters, object[] values, SqlCommand myCommand)
        {
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandText = StoredProcedureName;

            for (int a = 0; a <= Parameters.GetUpperBound(0); a++)
            {
                SqlParameter p1 = new SqlParameter((string)Parameters.GetValue(a), GetDBType(values.GetValue(a).GetType().Name));
                p1.Direction = ParameterDirection.Input;
                p1.Value = values.GetValue(a);
                myCommand.Parameters.Add(p1);
            }

            return myCommand;
        }

        public void ErrorLog(string Message, string TargetSite, string StackTrace)
        {
            try
            {
                string[] parameterKeys = { "@Message", "@Source", "@TargetSite", "@StackTrace" };
                object[] values = { Message.ToString(), "ESignature", TargetSite.ToString(), StackTrace.ToString() };
                ExecuteSP("usp_hts_ErrorLog", parameterKeys, values);
                ErrorEmail("\nError Message :" + Message + "\nTargetSite :" + TargetSite + "\nStack Trace :" + StackTrace + "\nSource : ESignature");
            }
            catch { } 
        }

        public void ErrorEmail(string SummaryMessage)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlConnection Conn = new SqlConnection(strCnn);
                SqlCommand Sqlcmd = new SqlCommand();

                Sqlcmd.Connection = Conn;
                SqlDataAdapter SQLAD = new SqlDataAdapter(Sqlcmd);

                Sqlcmd.CommandType = CommandType.Text;
                Sqlcmd.CommandText = "Select * From Tbl_HTS_ESignature_Configuration";

                SQLAD.Fill(ds);

                string smtpClient = ds.Tables[0].Rows[0]["SmtpClient"].ToString();
                string emailSender = ds.Tables[0].Rows[0]["EmailSender"].ToString();
                string emailReceiver = ds.Tables[0].Rows[0]["EmailReceiver"].ToString();

                MailMessage ObjMailMessage = new MailMessage(emailSender, emailReceiver, "ESignature : ERROR OCCURED!", SummaryMessage + "\n\nThis is a system generated email for developer");
                SmtpClient ObjSMTPClient = new SmtpClient(smtpClient);
                //System.Net.NetworkCredential ObjSMTPUserInfo = new System.Net.NetworkCredential("adil@lntechnologies.com", "password");
                ObjSMTPClient.UseDefaultCredentials = true;
                //ObjSMTPClient.Credentials = ObjSMTPUserInfo;
                ObjSMTPClient.Send(ObjMailMessage);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private System.Data.DbType GetDBType(string strType)
        {
            switch (strType.Trim())
            {
                case "Binary":
                    return System.Data.DbType.Binary;

                case "Boolean":
                    return System.Data.DbType.Boolean;

                case "Byte":
                    return System.Data.DbType.Byte;

                case "Currency":
                    return System.Data.DbType.Currency;

                case "Date":
                    return System.Data.DbType.Date;

                case "DateTime":
                    return System.Data.DbType.DateTime;

                case "Decimal":
                    return System.Data.DbType.Decimal;

                case "Double":
                    return System.Data.DbType.Double;

                case "Int16":
                    return System.Data.DbType.Int16;

                case "Int32":
                    return System.Data.DbType.Int32;

                case "Int64":
                    return System.Data.DbType.Int64;

                case "SByte":
                    return System.Data.DbType.SByte;

                case "Single":
                    return System.Data.DbType.Single;

                case "String":
                    return System.Data.DbType.String;

                case "StringFixedLength":
                    return System.Data.DbType.StringFixedLength;

                case "Time":
                    return System.Data.DbType.Time;

                case "UInt16":
                    return System.Data.DbType.UInt16;

                case "UInt32":
                    return System.Data.DbType.UInt32;

                case "UInt64":
                    return System.Data.DbType.UInt64;

                case "VarNumeric":
                    return System.Data.DbType.VarNumeric;
                case "Byte[]":
                    return System.Data.DbType.Object;
                default:
                    return System.Data.DbType.Int64;

            }
        }
    }
}

using System;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using clsDBase;



namespace CrystalAddOns
{
//'========================

    public class Crystal
    {
        clsData cls_DataBase = new clsData();

        public DataSet CreateData(string ImageTrace, string TicketID, bool Blank)
        {
            if (Blank == true) { TicketID = "0"; }
            string[] parameterKeys = { "@TicketID", "@ImageTrace" };
            object[] values = { TicketID, ImageTrace };

            DataSet data = new DataSet();
            data.Tables.Add("ImgTable");
            data.Tables[0].Columns.Add("Img", System.Type.GetType("System.Byte[]"));
            DataTable dt_Image = new DataTable();
            dt_Image = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_ImageByte", parameterKeys, values).Tables[0];
            if (dt_Image.Rows.Count != 0)
            {
                try
                {
                    AddImageRow(data.Tables[0], (byte[])dt_Image.Rows[0]["ImageStream"]);
                }
                catch
                {
                }
            }
            return (data);
        }

        public DataTable CreateDataForSubReport(string ImageTrace, string TicketID, bool Blank)
        {
            if (Blank == true) { TicketID = "0"; }
            string[] parameterKeys = { "@TicketID", "@ImageTrace" };
            object[] values = { TicketID, ImageTrace };

            DataTable dt_ImageData = new DataTable();
            dt_ImageData = cls_DataBase.ExecuteSP_ReturnDataSet("USP_HTS_ESignature_Get_ImageForSubReport", parameterKeys, values).Tables[0];
            return (dt_ImageData);
        }

        void AddImageRow(DataTable tbl, string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);	// create a file stream
            BinaryReader br = new BinaryReader(fs);						// create binary reader
            DataRow row;

            row = tbl.NewRow();
            row[0] = br.ReadBytes((int)br.BaseStream.Length);
            tbl.Rows.Add(row);
            br.Close();
            fs.Close();
        }

        void AddImageRow(DataTable tbl, byte[] byte_Image)
        {
            byte[] Image_Content = byte_Image;
            MemoryStream Memory_Stream = new MemoryStream(Image_Content);
            Bitmap BMP_Image = new Bitmap(Memory_Stream);
            DataRow dr_Image;
            dr_Image = tbl.NewRow();
            dr_Image[0] = byte_Image;
            tbl.Rows.Add(dr_Image);
        }
}
}
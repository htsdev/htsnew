using System;
using System.Collections.Generic;
using System.Text;

namespace PDF2Image
{
    public class ClsPDF2Image
    {
        public enum CompressionType
        {
            COMPRESSION_NONE = 1,              /* dump mode */
            COMPRESSION_CCITTRLE = 2,          /* CCITT modified Huffman RLE */
            COMPRESSION_CCITTFAX3 = 3,         /* CCITT Group 3 fax encoding */
            COMPRESSION_CCITTFAX4 = 4,         /* CCITT Group 4 fax encoding */
            COMPRESSION_LZW = 5,               /* Lempel-Ziv  & Welch */
            COMPRESSION_JPEG = 7,              /* JPEG DCT compression */
            COMPRESSION_PACKBITS = 32773       /* Macintosh RLE */
        }

        [System.Runtime.InteropServices.DllImport("pdf2image.dll")]
        public static extern int PDFToImageConverter(string strInputPDF, string strOutputImage, string strUsername, string strPassword, int lngXDPI, int lngYDPI, int lngBitCount, CompressionType enumCompression, int lngQuality, bool blnGreyscale, bool blnMultipage, int lngFirstPage, int lngLastPage);
        [System.Runtime.InteropServices.DllImport("pdf2image.dll")]
        public static extern int PDFToImageGetPageCount(string szPDFFileName);
    }
}

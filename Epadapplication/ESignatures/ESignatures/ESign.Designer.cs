namespace ESignatures
{
    partial class ESign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ESign));
            this.Pnl_Shadow_FirstImg_X = new System.Windows.Forms.Panel();
            this.Pnl_Shadow_FirstImg_Y = new System.Windows.Forms.Panel();
            this.PB_FirstImg = new System.Windows.Forms.PictureBox();
            this.TabPg_LetterOfRep = new System.Windows.Forms.TabPage();
            this.tab_Docs = new System.Windows.Forms.TabControl();
            this.tabPg_Receipt = new System.Windows.Forms.TabPage();
            this.TabPg_LetterOfContinuation = new System.Windows.Forms.TabPage();
            this.TabPg_BondDocs = new System.Windows.Forms.TabPage();
            this.TabPg_TrailLetter = new System.Windows.Forms.TabPage();
            this.Pnl_Shadow_FirstImg = new System.Windows.Forms.Panel();
            this.pnl_PreviewMain = new System.Windows.Forms.Panel();
            this.pnl_MainImage = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.PB_MainImage = new System.Windows.Forms.PictureBox();
            this.pnl_SignAll = new System.Windows.Forms.Panel();
            this.pnl_ToolBar_SignAll = new System.Windows.Forms.Panel();
            this.rb_DefaultSign = new System.Windows.Forms.RadioButton();
            this.rb_AllDocSign = new System.Windows.Forms.RadioButton();
            this.rb_SelectedDocSign = new System.Windows.Forms.RadioButton();
            this.btn_SignAll_DeleteSignature = new System.Windows.Forms.Button();
            this.btn_ClearFingerprint = new System.Windows.Forms.Button();
            this.btn_DefaultSignature_Open = new System.Windows.Forms.Button();
            this.btn_SignAll_InsertSignature = new System.Windows.Forms.Button();
            this.rb_SalesRep = new System.Windows.Forms.RadioButton();
            this.rb_Attorney = new System.Windows.Forms.RadioButton();
            this.rb_Client = new System.Windows.Forms.RadioButton();
            this.pb_FP_Main = new System.Windows.Forms.PictureBox();
            this.btn_SignAll_Close = new System.Windows.Forms.Button();
            this.pb_Signature_Main = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pb_Fingerprint = new System.Windows.Forms.PictureBox();
            this.pb_Initial = new System.Windows.Forms.PictureBox();
            this.pb_Signature = new System.Windows.Forms.PictureBox();
            this.pb_Signature_Back = new System.Windows.Forms.PictureBox();
            this.pb_Initial_Back = new System.Windows.Forms.PictureBox();
            this.pb_Fingerprint_Back = new System.Windows.Forms.PictureBox();
            this.pnl_ToolBar = new System.Windows.Forms.Panel();
            this.btn_Page_Next = new System.Windows.Forms.Button();
            this.btn_Page_Previous = new System.Windows.Forms.Button();
            this.btn_FirstSign = new System.Windows.Forms.Button();
            this.btn_LastSign = new System.Windows.Forms.Button();
            this.btn_PreviousSign = new System.Windows.Forms.Button();
            this.btn_SignAll = new System.Windows.Forms.Button();
            this.btn_NextSign = new System.Windows.Forms.Button();
            this.btn_Zoom = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_PrintCurrentReport = new System.Windows.Forms.Button();
            this.btn_PrintAll = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_Exit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Cnt_FPAll = new AxESFINGERPRINTLib.AxesFP();
            this.Cnt_PreviewSignature = new AxeSign3.AxesCapture();
            this.Cnt_SignAll_Runtime = new AxeSign3.AxesCapture();
            this.Cnt_Signature_Runtime = new AxeSign3.AxesCapture();
            this.lbl_CauseNo = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_TicketNo = new System.Windows.Forms.Label();
            this.lbl_Phone = new System.Windows.Forms.Label();
            this.lbl_Address = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPageCounter = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PB_FirstImg)).BeginInit();
            this.tab_Docs.SuspendLayout();
            this.pnl_PreviewMain.SuspendLayout();
            this.pnl_MainImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_MainImage)).BeginInit();
            this.pnl_SignAll.SuspendLayout();
            this.pnl_ToolBar_SignAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_FP_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Signature_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Fingerprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Initial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Signature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Signature_Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Initial_Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Fingerprint_Back)).BeginInit();
            this.pnl_ToolBar.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_FPAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_PreviewSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_SignAll_Runtime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_Signature_Runtime)).BeginInit();
            this.SuspendLayout();
            // 
            // Pnl_Shadow_FirstImg_X
            // 
            this.Pnl_Shadow_FirstImg_X.BackColor = System.Drawing.Color.Gainsboro;
            this.Pnl_Shadow_FirstImg_X.Location = new System.Drawing.Point(16, 116);
            this.Pnl_Shadow_FirstImg_X.Name = "Pnl_Shadow_FirstImg_X";
            this.Pnl_Shadow_FirstImg_X.Size = new System.Drawing.Size(403, 307);
            this.Pnl_Shadow_FirstImg_X.TabIndex = 35;
            // 
            // Pnl_Shadow_FirstImg_Y
            // 
            this.Pnl_Shadow_FirstImg_Y.BackColor = System.Drawing.Color.Gainsboro;
            this.Pnl_Shadow_FirstImg_Y.Location = new System.Drawing.Point(109, 18);
            this.Pnl_Shadow_FirstImg_Y.Name = "Pnl_Shadow_FirstImg_Y";
            this.Pnl_Shadow_FirstImg_Y.Size = new System.Drawing.Size(210, 494);
            this.Pnl_Shadow_FirstImg_Y.TabIndex = 33;
            // 
            // PB_FirstImg
            // 
            this.PB_FirstImg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PB_FirstImg.Location = new System.Drawing.Point(21, 23);
            this.PB_FirstImg.Name = "PB_FirstImg";
            this.PB_FirstImg.Size = new System.Drawing.Size(393, 485);
            this.PB_FirstImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_FirstImg.TabIndex = 28;
            this.PB_FirstImg.TabStop = false;
            this.PB_FirstImg.MouseLeave += new System.EventHandler(this.PB_FirstImg_MouseLeave);
            this.PB_FirstImg.Click += new System.EventHandler(this.PB_FirstImg_Click);
            this.PB_FirstImg.MouseEnter += new System.EventHandler(this.PB_FirstImg_MouseEnter);
            // 
            // TabPg_LetterOfRep
            // 
            this.TabPg_LetterOfRep.Location = new System.Drawing.Point(4, 22);
            this.TabPg_LetterOfRep.Name = "TabPg_LetterOfRep";
            this.TabPg_LetterOfRep.Size = new System.Drawing.Size(866, -3);
            this.TabPg_LetterOfRep.TabIndex = 3;
            this.TabPg_LetterOfRep.Text = "Letter Of Rep 0/1";
            this.TabPg_LetterOfRep.UseVisualStyleBackColor = true;
            // 
            // tab_Docs
            // 
            this.tab_Docs.Controls.Add(this.tabPg_Receipt);
            this.tab_Docs.Controls.Add(this.TabPg_LetterOfContinuation);
            this.tab_Docs.Controls.Add(this.TabPg_BondDocs);
            this.tab_Docs.Controls.Add(this.TabPg_TrailLetter);
            this.tab_Docs.Controls.Add(this.TabPg_LetterOfRep);
            this.tab_Docs.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tab_Docs.Location = new System.Drawing.Point(5, 123);
            this.tab_Docs.Name = "tab_Docs";
            this.tab_Docs.SelectedIndex = 0;
            this.tab_Docs.Size = new System.Drawing.Size(874, 23);
            this.tab_Docs.TabIndex = 0;
            this.tab_Docs.Click += new System.EventHandler(this.tab_Docs_Click);
            this.tab_Docs.Deselected += new System.Windows.Forms.TabControlEventHandler(this.tab_Docs_Deselected);
            // 
            // tabPg_Receipt
            // 
            this.tabPg_Receipt.Location = new System.Drawing.Point(4, 22);
            this.tabPg_Receipt.Name = "tabPg_Receipt";
            this.tabPg_Receipt.Size = new System.Drawing.Size(866, 0);
            this.tabPg_Receipt.TabIndex = 4;
            this.tabPg_Receipt.Text = "Receipt 0/2";
            this.tabPg_Receipt.UseVisualStyleBackColor = true;
            // 
            // TabPg_LetterOfContinuation
            // 
            this.TabPg_LetterOfContinuation.Location = new System.Drawing.Point(4, 22);
            this.TabPg_LetterOfContinuation.Name = "TabPg_LetterOfContinuation";
            this.TabPg_LetterOfContinuation.Size = new System.Drawing.Size(866, -3);
            this.TabPg_LetterOfContinuation.TabIndex = 2;
            this.TabPg_LetterOfContinuation.Text = "Letter Of Continuation 0/2";
            this.TabPg_LetterOfContinuation.UseVisualStyleBackColor = true;
            // 
            // TabPg_BondDocs
            // 
            this.TabPg_BondDocs.Location = new System.Drawing.Point(4, 22);
            this.TabPg_BondDocs.Name = "TabPg_BondDocs";
            this.TabPg_BondDocs.Padding = new System.Windows.Forms.Padding(3);
            this.TabPg_BondDocs.Size = new System.Drawing.Size(866, -3);
            this.TabPg_BondDocs.TabIndex = 0;
            this.TabPg_BondDocs.Text = "Bond Docs 0/16";
            this.TabPg_BondDocs.UseVisualStyleBackColor = true;
            // 
            // TabPg_TrailLetter
            // 
            this.TabPg_TrailLetter.Location = new System.Drawing.Point(4, 22);
            this.TabPg_TrailLetter.Name = "TabPg_TrailLetter";
            this.TabPg_TrailLetter.Padding = new System.Windows.Forms.Padding(3);
            this.TabPg_TrailLetter.Size = new System.Drawing.Size(866, -3);
            this.TabPg_TrailLetter.TabIndex = 1;
            this.TabPg_TrailLetter.Text = "Trial Letter 0/1";
            this.TabPg_TrailLetter.UseVisualStyleBackColor = true;
            // 
            // Pnl_Shadow_FirstImg
            // 
            this.Pnl_Shadow_FirstImg.BackColor = System.Drawing.Color.Gainsboro;
            this.Pnl_Shadow_FirstImg.Location = new System.Drawing.Point(16, 18);
            this.Pnl_Shadow_FirstImg.Name = "Pnl_Shadow_FirstImg";
            this.Pnl_Shadow_FirstImg.Size = new System.Drawing.Size(403, 495);
            this.Pnl_Shadow_FirstImg.TabIndex = 30;
            // 
            // pnl_PreviewMain
            // 
            this.pnl_PreviewMain.Controls.Add(this.PB_FirstImg);
            this.pnl_PreviewMain.Controls.Add(this.Pnl_Shadow_FirstImg_X);
            this.pnl_PreviewMain.Controls.Add(this.Pnl_Shadow_FirstImg_Y);
            this.pnl_PreviewMain.Controls.Add(this.Pnl_Shadow_FirstImg);
            this.pnl_PreviewMain.Location = new System.Drawing.Point(9, 148);
            this.pnl_PreviewMain.Name = "pnl_PreviewMain";
            this.pnl_PreviewMain.Size = new System.Drawing.Size(867, 535);
            this.pnl_PreviewMain.TabIndex = 40;
            this.pnl_PreviewMain.Visible = false;
            // 
            // pnl_MainImage
            // 
            this.pnl_MainImage.AutoScroll = true;
            this.pnl_MainImage.Controls.Add(this.button5);
            this.pnl_MainImage.Controls.Add(this.textBox1);
            this.pnl_MainImage.Controls.Add(this.button4);
            this.pnl_MainImage.Controls.Add(this.button3);
            this.pnl_MainImage.Controls.Add(this.button2);
            this.pnl_MainImage.Controls.Add(this.button1);
            this.pnl_MainImage.Controls.Add(this.PB_MainImage);
            this.pnl_MainImage.Location = new System.Drawing.Point(8, 147);
            this.pnl_MainImage.Name = "pnl_MainImage";
            this.pnl_MainImage.Size = new System.Drawing.Size(867, 535);
            this.pnl_MainImage.TabIndex = 43;
            this.pnl_MainImage.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(0, 886);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(26, 22);
            this.button5.TabIndex = 75;
            this.button5.Text = "?";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(0, 860);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(25, 20);
            this.textBox1.TabIndex = 74;
            this.textBox1.Text = "10";
            this.textBox1.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(-1, 825);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(26, 22);
            this.button4.TabIndex = 73;
            this.button4.Text = "v";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(-1, 800);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 22);
            this.button3.TabIndex = 72;
            this.button3.Text = "<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(-1, 750);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 22);
            this.button2.TabIndex = 71;
            this.button2.Text = "^";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-1, 775);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 22);
            this.button1.TabIndex = 70;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PB_MainImage
            // 
            this.PB_MainImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PB_MainImage.Location = new System.Drawing.Point(28, 33);
            this.PB_MainImage.Name = "PB_MainImage";
            this.PB_MainImage.Size = new System.Drawing.Size(787, 1122);
            this.PB_MainImage.TabIndex = 0;
            this.PB_MainImage.TabStop = false;
            this.PB_MainImage.Click += new System.EventHandler(this.PB_MainImage_Click);
            // 
            // pnl_SignAll
            // 
            this.pnl_SignAll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_SignAll.Controls.Add(this.pnl_ToolBar_SignAll);
            this.pnl_SignAll.Controls.Add(this.rb_SalesRep);
            this.pnl_SignAll.Controls.Add(this.rb_Attorney);
            this.pnl_SignAll.Controls.Add(this.rb_Client);
            this.pnl_SignAll.Controls.Add(this.pb_FP_Main);
            this.pnl_SignAll.Controls.Add(this.btn_SignAll_Close);
            this.pnl_SignAll.Controls.Add(this.pb_Signature_Main);
            this.pnl_SignAll.Controls.Add(this.label10);
            this.pnl_SignAll.Controls.Add(this.label9);
            this.pnl_SignAll.Controls.Add(this.label7);
            this.pnl_SignAll.Controls.Add(this.pb_Fingerprint);
            this.pnl_SignAll.Controls.Add(this.pb_Initial);
            this.pnl_SignAll.Controls.Add(this.pb_Signature);
            this.pnl_SignAll.Controls.Add(this.pb_Signature_Back);
            this.pnl_SignAll.Controls.Add(this.pb_Initial_Back);
            this.pnl_SignAll.Controls.Add(this.pb_Fingerprint_Back);
            this.pnl_SignAll.Location = new System.Drawing.Point(41, 185);
            this.pnl_SignAll.Name = "pnl_SignAll";
            this.pnl_SignAll.Size = new System.Drawing.Size(465, 327);
            this.pnl_SignAll.TabIndex = 46;
            this.pnl_SignAll.Visible = false;
            // 
            // pnl_ToolBar_SignAll
            // 
            this.pnl_ToolBar_SignAll.Controls.Add(this.rb_DefaultSign);
            this.pnl_ToolBar_SignAll.Controls.Add(this.rb_AllDocSign);
            this.pnl_ToolBar_SignAll.Controls.Add(this.rb_SelectedDocSign);
            this.pnl_ToolBar_SignAll.Controls.Add(this.btn_SignAll_DeleteSignature);
            this.pnl_ToolBar_SignAll.Controls.Add(this.btn_ClearFingerprint);
            this.pnl_ToolBar_SignAll.Controls.Add(this.btn_DefaultSignature_Open);
            this.pnl_ToolBar_SignAll.Controls.Add(this.btn_SignAll_InsertSignature);
            this.pnl_ToolBar_SignAll.Location = new System.Drawing.Point(3, 251);
            this.pnl_ToolBar_SignAll.Name = "pnl_ToolBar_SignAll";
            this.pnl_ToolBar_SignAll.Size = new System.Drawing.Size(458, 74);
            this.pnl_ToolBar_SignAll.TabIndex = 76;
            // 
            // rb_DefaultSign
            // 
            this.rb_DefaultSign.Enabled = false;
            this.rb_DefaultSign.Location = new System.Drawing.Point(272, 54);
            this.rb_DefaultSign.Name = "rb_DefaultSign";
            this.rb_DefaultSign.Size = new System.Drawing.Size(134, 17);
            this.rb_DefaultSign.TabIndex = 22;
            this.rb_DefaultSign.Text = "Default signature";
            this.rb_DefaultSign.UseVisualStyleBackColor = true;
            // 
            // rb_AllDocSign
            // 
            this.rb_AllDocSign.Checked = true;
            this.rb_AllDocSign.Location = new System.Drawing.Point(272, 31);
            this.rb_AllDocSign.Name = "rb_AllDocSign";
            this.rb_AllDocSign.Size = new System.Drawing.Size(183, 17);
            this.rb_AllDocSign.TabIndex = 20;
            this.rb_AllDocSign.TabStop = true;
            this.rb_AllDocSign.Text = "All document\'s signature(s)";
            this.rb_AllDocSign.UseVisualStyleBackColor = true;
            // 
            // rb_SelectedDocSign
            // 
            this.rb_SelectedDocSign.Location = new System.Drawing.Point(272, 10);
            this.rb_SelectedDocSign.Name = "rb_SelectedDocSign";
            this.rb_SelectedDocSign.Size = new System.Drawing.Size(181, 17);
            this.rb_SelectedDocSign.TabIndex = 19;
            this.rb_SelectedDocSign.Text = "Selected document\'s signature(s)";
            this.rb_SelectedDocSign.UseVisualStyleBackColor = true;
            // 
            // btn_SignAll_DeleteSignature
            // 
            this.btn_SignAll_DeleteSignature.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn_SignAll_DeleteSignature.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SignAll_DeleteSignature.Image = ((System.Drawing.Image)(resources.GetObject("btn_SignAll_DeleteSignature.Image")));
            this.btn_SignAll_DeleteSignature.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SignAll_DeleteSignature.Location = new System.Drawing.Point(196, 7);
            this.btn_SignAll_DeleteSignature.Name = "btn_SignAll_DeleteSignature";
            this.btn_SignAll_DeleteSignature.Size = new System.Drawing.Size(71, 65);
            this.btn_SignAll_DeleteSignature.TabIndex = 18;
            this.btn_SignAll_DeleteSignature.Text = "Delete By Criteria";
            this.btn_SignAll_DeleteSignature.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_SignAll_DeleteSignature, "Delete default signature");
            this.btn_SignAll_DeleteSignature.UseVisualStyleBackColor = false;
            this.btn_SignAll_DeleteSignature.Click += new System.EventHandler(this.btn_SignAll_DeleteSignature_Click);
            // 
            // btn_ClearFingerprint
            // 
            this.btn_ClearFingerprint.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn_ClearFingerprint.Enabled = false;
            this.btn_ClearFingerprint.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ClearFingerprint.Image = ((System.Drawing.Image)(resources.GetObject("btn_ClearFingerprint.Image")));
            this.btn_ClearFingerprint.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_ClearFingerprint.Location = new System.Drawing.Point(1, 6);
            this.btn_ClearFingerprint.Name = "btn_ClearFingerprint";
            this.btn_ClearFingerprint.Size = new System.Drawing.Size(47, 66);
            this.btn_ClearFingerprint.TabIndex = 15;
            this.btn_ClearFingerprint.Text = "Clear Thumb";
            this.btn_ClearFingerprint.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_ClearFingerprint, "Clear fingerprint");
            this.btn_ClearFingerprint.UseVisualStyleBackColor = false;
            this.btn_ClearFingerprint.Click += new System.EventHandler(this.btn_ClearFingerprint_Click);
            // 
            // btn_DefaultSignature_Open
            // 
            this.btn_DefaultSignature_Open.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DefaultSignature_Open.Image = ((System.Drawing.Image)(resources.GetObject("btn_DefaultSignature_Open.Image")));
            this.btn_DefaultSignature_Open.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_DefaultSignature_Open.Location = new System.Drawing.Point(120, 7);
            this.btn_DefaultSignature_Open.Name = "btn_DefaultSignature_Open";
            this.btn_DefaultSignature_Open.Size = new System.Drawing.Size(74, 65);
            this.btn_DefaultSignature_Open.TabIndex = 17;
            this.btn_DefaultSignature_Open.Text = "Open Default Sign";
            this.btn_DefaultSignature_Open.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_DefaultSignature_Open, "Open default sign");
            this.btn_DefaultSignature_Open.UseVisualStyleBackColor = true;
            this.btn_DefaultSignature_Open.Click += new System.EventHandler(this.btn_DefaultSignature_Open_Click);
            // 
            // btn_SignAll_InsertSignature
            // 
            this.btn_SignAll_InsertSignature.Enabled = false;
            this.btn_SignAll_InsertSignature.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SignAll_InsertSignature.Image = ((System.Drawing.Image)(resources.GetObject("btn_SignAll_InsertSignature.Image")));
            this.btn_SignAll_InsertSignature.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SignAll_InsertSignature.Location = new System.Drawing.Point(50, 7);
            this.btn_SignAll_InsertSignature.Name = "btn_SignAll_InsertSignature";
            this.btn_SignAll_InsertSignature.Size = new System.Drawing.Size(68, 65);
            this.btn_SignAll_InsertSignature.TabIndex = 16;
            this.btn_SignAll_InsertSignature.Text = "Apply By Criteria";
            this.btn_SignAll_InsertSignature.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_SignAll_InsertSignature, "Apply to selected document");
            this.btn_SignAll_InsertSignature.UseVisualStyleBackColor = true;
            this.btn_SignAll_InsertSignature.Click += new System.EventHandler(this.btn_SignAll_InsertSignature_Click);
            // 
            // rb_SalesRep
            // 
            this.rb_SalesRep.AutoSize = true;
            this.rb_SalesRep.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_SalesRep.Location = new System.Drawing.Point(325, 36);
            this.rb_SalesRep.Name = "rb_SalesRep";
            this.rb_SalesRep.Size = new System.Drawing.Size(78, 17);
            this.rb_SalesRep.TabIndex = 13;
            this.rb_SalesRep.TabStop = true;
            this.rb_SalesRep.Text = "SalesRep";
            this.rb_SalesRep.UseVisualStyleBackColor = true;
            // 
            // rb_Attorney
            // 
            this.rb_Attorney.AutoSize = true;
            this.rb_Attorney.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_Attorney.Location = new System.Drawing.Point(325, 13);
            this.rb_Attorney.Name = "rb_Attorney";
            this.rb_Attorney.Size = new System.Drawing.Size(74, 17);
            this.rb_Attorney.TabIndex = 12;
            this.rb_Attorney.TabStop = true;
            this.rb_Attorney.Text = "Attorney";
            this.rb_Attorney.UseVisualStyleBackColor = true;
            // 
            // rb_Client
            // 
            this.rb_Client.AutoSize = true;
            this.rb_Client.Checked = true;
            this.rb_Client.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_Client.Location = new System.Drawing.Point(325, 59);
            this.rb_Client.Name = "rb_Client";
            this.rb_Client.Size = new System.Drawing.Size(58, 17);
            this.rb_Client.TabIndex = 14;
            this.rb_Client.TabStop = true;
            this.rb_Client.Text = "Client";
            this.rb_Client.UseVisualStyleBackColor = true;
            // 
            // pb_FP_Main
            // 
            this.pb_FP_Main.BackColor = System.Drawing.Color.DodgerBlue;
            this.pb_FP_Main.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_FP_Main.Location = new System.Drawing.Point(143, 127);
            this.pb_FP_Main.Name = "pb_FP_Main";
            this.pb_FP_Main.Size = new System.Drawing.Size(93, 117);
            this.pb_FP_Main.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_FP_Main.TabIndex = 63;
            this.pb_FP_Main.TabStop = false;
            this.pb_FP_Main.Visible = false;
            // 
            // btn_SignAll_Close
            // 
            this.btn_SignAll_Close.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn_SignAll_Close.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SignAll_Close.Image = ((System.Drawing.Image)(resources.GetObject("btn_SignAll_Close.Image")));
            this.btn_SignAll_Close.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SignAll_Close.Location = new System.Drawing.Point(428, 5);
            this.btn_SignAll_Close.Name = "btn_SignAll_Close";
            this.btn_SignAll_Close.Size = new System.Drawing.Size(27, 25);
            this.btn_SignAll_Close.TabIndex = 23;
            this.btn_SignAll_Close.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_SignAll_Close, "Close sign all dialog");
            this.btn_SignAll_Close.UseVisualStyleBackColor = false;
            this.btn_SignAll_Close.Click += new System.EventHandler(this.btn_SignAll_Close_Click);
            // 
            // pb_Signature_Main
            // 
            this.pb_Signature_Main.BackColor = System.Drawing.Color.DodgerBlue;
            this.pb_Signature_Main.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Signature_Main.Location = new System.Drawing.Point(90, 134);
            this.pb_Signature_Main.Name = "pb_Signature_Main";
            this.pb_Signature_Main.Size = new System.Drawing.Size(199, 91);
            this.pb_Signature_Main.TabIndex = 62;
            this.pb_Signature_Main.TabStop = false;
            this.pb_Signature_Main.Visible = false;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(229, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 14);
            this.label10.TabIndex = 57;
            this.label10.Text = "Finger Print";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(165, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 14);
            this.label9.TabIndex = 56;
            this.label9.Text = "Initial";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(41, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 14);
            this.label7.TabIndex = 55;
            this.label7.Text = "Signature";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_Fingerprint
            // 
            this.pb_Fingerprint.BackColor = System.Drawing.Color.White;
            this.pb_Fingerprint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Fingerprint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Fingerprint.Image = ((System.Drawing.Image)(resources.GetObject("pb_Fingerprint.Image")));
            this.pb_Fingerprint.Location = new System.Drawing.Point(236, 13);
            this.pb_Fingerprint.Name = "pb_Fingerprint";
            this.pb_Fingerprint.Size = new System.Drawing.Size(60, 72);
            this.pb_Fingerprint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Fingerprint.TabIndex = 54;
            this.pb_Fingerprint.TabStop = false;
            this.toolTip1.SetToolTip(this.pb_Fingerprint, "Click here for fingerprint");
            this.pb_Fingerprint.Click += new System.EventHandler(this.pb_Fingerprint_Click);
            // 
            // pb_Initial
            // 
            this.pb_Initial.BackColor = System.Drawing.Color.White;
            this.pb_Initial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Initial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Initial.Image = ((System.Drawing.Image)(resources.GetObject("pb_Initial.Image")));
            this.pb_Initial.Location = new System.Drawing.Point(162, 13);
            this.pb_Initial.Name = "pb_Initial";
            this.pb_Initial.Size = new System.Drawing.Size(52, 72);
            this.pb_Initial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Initial.TabIndex = 53;
            this.pb_Initial.TabStop = false;
            this.toolTip1.SetToolTip(this.pb_Initial, "Click here for initial");
            this.pb_Initial.Click += new System.EventHandler(this.pb_Initial_Click);
            // 
            // pb_Signature
            // 
            this.pb_Signature.BackColor = System.Drawing.Color.White;
            this.pb_Signature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_Signature.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Signature.Image = ((System.Drawing.Image)(resources.GetObject("pb_Signature.Image")));
            this.pb_Signature.Location = new System.Drawing.Point(6, 13);
            this.pb_Signature.Name = "pb_Signature";
            this.pb_Signature.Size = new System.Drawing.Size(134, 72);
            this.pb_Signature.TabIndex = 52;
            this.pb_Signature.TabStop = false;
            this.toolTip1.SetToolTip(this.pb_Signature, "Click here for signature");
            this.pb_Signature.Click += new System.EventHandler(this.pb_Signature_Click);
            // 
            // pb_Signature_Back
            // 
            this.pb_Signature_Back.BackColor = System.Drawing.Color.DodgerBlue;
            this.pb_Signature_Back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Signature_Back.Location = new System.Drawing.Point(3, 10);
            this.pb_Signature_Back.Name = "pb_Signature_Back";
            this.pb_Signature_Back.Size = new System.Drawing.Size(140, 78);
            this.pb_Signature_Back.TabIndex = 58;
            this.pb_Signature_Back.TabStop = false;
            // 
            // pb_Initial_Back
            // 
            this.pb_Initial_Back.BackColor = System.Drawing.Color.DodgerBlue;
            this.pb_Initial_Back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Initial_Back.Location = new System.Drawing.Point(159, 10);
            this.pb_Initial_Back.Name = "pb_Initial_Back";
            this.pb_Initial_Back.Size = new System.Drawing.Size(58, 78);
            this.pb_Initial_Back.TabIndex = 59;
            this.pb_Initial_Back.TabStop = false;
            // 
            // pb_Fingerprint_Back
            // 
            this.pb_Fingerprint_Back.BackColor = System.Drawing.Color.DodgerBlue;
            this.pb_Fingerprint_Back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_Fingerprint_Back.Location = new System.Drawing.Point(233, 10);
            this.pb_Fingerprint_Back.Name = "pb_Fingerprint_Back";
            this.pb_Fingerprint_Back.Size = new System.Drawing.Size(66, 78);
            this.pb_Fingerprint_Back.TabIndex = 60;
            this.pb_Fingerprint_Back.TabStop = false;
            // 
            // pnl_ToolBar
            // 
            this.pnl_ToolBar.Controls.Add(this.btn_Page_Next);
            this.pnl_ToolBar.Controls.Add(this.btn_Page_Previous);
            this.pnl_ToolBar.Controls.Add(this.btn_FirstSign);
            this.pnl_ToolBar.Controls.Add(this.btn_LastSign);
            this.pnl_ToolBar.Controls.Add(this.btn_PreviousSign);
            this.pnl_ToolBar.Controls.Add(this.btn_SignAll);
            this.pnl_ToolBar.Controls.Add(this.btn_NextSign);
            this.pnl_ToolBar.Controls.Add(this.btn_Zoom);
            this.pnl_ToolBar.Controls.Add(this.btn_Update);
            this.pnl_ToolBar.Controls.Add(this.btn_PrintCurrentReport);
            this.pnl_ToolBar.Controls.Add(this.btn_PrintAll);
            this.pnl_ToolBar.Location = new System.Drawing.Point(5, 58);
            this.pnl_ToolBar.Name = "pnl_ToolBar";
            this.pnl_ToolBar.Size = new System.Drawing.Size(871, 62);
            this.pnl_ToolBar.TabIndex = 67;
            // 
            // btn_Page_Next
            // 
            this.btn_Page_Next.Enabled = false;
            this.btn_Page_Next.Font = new System.Drawing.Font("Verdana", 6.75F);
            this.btn_Page_Next.Image = ((System.Drawing.Image)(resources.GetObject("btn_Page_Next.Image")));
            this.btn_Page_Next.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Page_Next.Location = new System.Drawing.Point(559, 6);
            this.btn_Page_Next.Name = "btn_Page_Next";
            this.btn_Page_Next.Size = new System.Drawing.Size(65, 57);
            this.btn_Page_Next.TabIndex = 8;
            this.btn_Page_Next.Text = "Next Page";
            this.btn_Page_Next.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Page_Next.UseVisualStyleBackColor = true;
            this.btn_Page_Next.Click += new System.EventHandler(this.btn_Page_Next_Click);
            // 
            // btn_Page_Previous
            // 
            this.btn_Page_Previous.Enabled = false;
            this.btn_Page_Previous.Font = new System.Drawing.Font("Verdana", 6.75F);
            this.btn_Page_Previous.Image = ((System.Drawing.Image)(resources.GetObject("btn_Page_Previous.Image")));
            this.btn_Page_Previous.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Page_Previous.Location = new System.Drawing.Point(468, 6);
            this.btn_Page_Previous.Name = "btn_Page_Previous";
            this.btn_Page_Previous.Size = new System.Drawing.Size(85, 57);
            this.btn_Page_Previous.TabIndex = 7;
            this.btn_Page_Previous.Text = "Previous Page";
            this.btn_Page_Previous.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Page_Previous.UseVisualStyleBackColor = true;
            this.btn_Page_Previous.Click += new System.EventHandler(this.btn_Page_Previous_Click);
            // 
            // btn_FirstSign
            // 
            this.btn_FirstSign.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_FirstSign.Image = ((System.Drawing.Image)(resources.GetObject("btn_FirstSign.Image")));
            this.btn_FirstSign.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_FirstSign.Location = new System.Drawing.Point(118, 6);
            this.btn_FirstSign.Name = "btn_FirstSign";
            this.btn_FirstSign.Size = new System.Drawing.Size(61, 57);
            this.btn_FirstSign.TabIndex = 2;
            this.btn_FirstSign.Text = "First Sign";
            this.btn_FirstSign.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_FirstSign, "First location for signature on this page");
            this.btn_FirstSign.UseVisualStyleBackColor = true;
            this.btn_FirstSign.Click += new System.EventHandler(this.btn_FirstSign_Click);
            // 
            // btn_LastSign
            // 
            this.btn_LastSign.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LastSign.Image = ((System.Drawing.Image)(resources.GetObject("btn_LastSign.Image")));
            this.btn_LastSign.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_LastSign.Location = new System.Drawing.Point(341, 6);
            this.btn_LastSign.Name = "btn_LastSign";
            this.btn_LastSign.Size = new System.Drawing.Size(61, 57);
            this.btn_LastSign.TabIndex = 5;
            this.btn_LastSign.Text = "Last Sign";
            this.btn_LastSign.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_LastSign, "Last location for signature on this page");
            this.btn_LastSign.UseVisualStyleBackColor = true;
            this.btn_LastSign.Click += new System.EventHandler(this.btn_LastSign_Click);
            // 
            // btn_PreviousSign
            // 
            this.btn_PreviousSign.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PreviousSign.Image = ((System.Drawing.Image)(resources.GetObject("btn_PreviousSign.Image")));
            this.btn_PreviousSign.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_PreviousSign.Location = new System.Drawing.Point(185, 6);
            this.btn_PreviousSign.Name = "btn_PreviousSign";
            this.btn_PreviousSign.Size = new System.Drawing.Size(82, 57);
            this.btn_PreviousSign.TabIndex = 3;
            this.btn_PreviousSign.Text = "Previous Sign";
            this.btn_PreviousSign.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_PreviousSign, "Previous location for signature on this page");
            this.btn_PreviousSign.UseVisualStyleBackColor = true;
            this.btn_PreviousSign.Click += new System.EventHandler(this.btn_PreviousSign_Click);
            // 
            // btn_SignAll
            // 
            this.btn_SignAll.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SignAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_SignAll.Image")));
            this.btn_SignAll.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SignAll.Location = new System.Drawing.Point(408, 6);
            this.btn_SignAll.Name = "btn_SignAll";
            this.btn_SignAll.Size = new System.Drawing.Size(54, 57);
            this.btn_SignAll.TabIndex = 6;
            this.btn_SignAll.Text = "Sign All";
            this.btn_SignAll.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_SignAll, "Open sign all dialog");
            this.btn_SignAll.UseVisualStyleBackColor = true;
            this.btn_SignAll.Click += new System.EventHandler(this.btn_SignAll_Click);
            // 
            // btn_NextSign
            // 
            this.btn_NextSign.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NextSign.Image = ((System.Drawing.Image)(resources.GetObject("btn_NextSign.Image")));
            this.btn_NextSign.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_NextSign.Location = new System.Drawing.Point(273, 6);
            this.btn_NextSign.Name = "btn_NextSign";
            this.btn_NextSign.Size = new System.Drawing.Size(62, 57);
            this.btn_NextSign.TabIndex = 4;
            this.btn_NextSign.Text = "Next Sign";
            this.btn_NextSign.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_NextSign, "Next location for signature on this page");
            this.btn_NextSign.UseVisualStyleBackColor = true;
            this.btn_NextSign.Click += new System.EventHandler(this.btn_NextSign_Click);
            // 
            // btn_Zoom
            // 
            this.btn_Zoom.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Zoom.Image = ((System.Drawing.Image)(resources.GetObject("btn_Zoom.Image")));
            this.btn_Zoom.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Zoom.Location = new System.Drawing.Point(630, 6);
            this.btn_Zoom.Name = "btn_Zoom";
            this.btn_Zoom.Size = new System.Drawing.Size(80, 57);
            this.btn_Zoom.TabIndex = 9;
            this.btn_Zoom.Text = "Zoom IN/OUT";
            this.btn_Zoom.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_Zoom, "Zoom IN/OUT");
            this.btn_Zoom.UseVisualStyleBackColor = true;
            this.btn_Zoom.Click += new System.EventHandler(this.btn_Zoom_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Update.Image = ((System.Drawing.Image)(resources.GetObject("btn_Update.Image")));
            this.btn_Update.Location = new System.Drawing.Point(5, 6);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(107, 57);
            this.btn_Update.TabIndex = 1;
            this.btn_Update.Text = "Save Document(s)";
            this.btn_Update.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_Update, "Save document");
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_PrintCurrentReport
            // 
            this.btn_PrintCurrentReport.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PrintCurrentReport.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintCurrentReport.Image")));
            this.btn_PrintCurrentReport.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_PrintCurrentReport.Location = new System.Drawing.Point(716, 6);
            this.btn_PrintCurrentReport.Name = "btn_PrintCurrentReport";
            this.btn_PrintCurrentReport.Size = new System.Drawing.Size(88, 57);
            this.btn_PrintCurrentReport.TabIndex = 10;
            this.btn_PrintCurrentReport.Text = "Print Selected";
            this.btn_PrintCurrentReport.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_PrintCurrentReport, "Print selected document");
            this.btn_PrintCurrentReport.UseVisualStyleBackColor = true;
            this.btn_PrintCurrentReport.Click += new System.EventHandler(this.btn_PrintCurrentReport_Click);
            // 
            // btn_PrintAll
            // 
            this.btn_PrintAll.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PrintAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintAll.Image")));
            this.btn_PrintAll.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_PrintAll.Location = new System.Drawing.Point(810, 6);
            this.btn_PrintAll.Name = "btn_PrintAll";
            this.btn_PrintAll.Size = new System.Drawing.Size(57, 57);
            this.btn_PrintAll.TabIndex = 11;
            this.btn_PrintAll.Text = "Print All";
            this.btn_PrintAll.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btn_PrintAll, "Print all document");
            this.btn_PrintAll.UseVisualStyleBackColor = true;
            this.btn_PrintAll.Click += new System.EventHandler(this.btn_PrintAll_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.BackColor = System.Drawing.Color.Thistle;
            this.btn_Exit.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exit.Image = ((System.Drawing.Image)(resources.GetObject("btn_Exit.Image")));
            this.btn_Exit.Location = new System.Drawing.Point(840, 27);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(27, 24);
            this.btn_Exit.TabIndex = 72;
            this.toolTip1.SetToolTip(this.btn_Exit, "Close ESignature");
            this.btn_Exit.UseVisualStyleBackColor = false;
            this.btn_Exit.Visible = false;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(242)))));
            this.panel1.Controls.Add(this.Cnt_FPAll);
            this.panel1.Controls.Add(this.Cnt_PreviewSignature);
            this.panel1.Controls.Add(this.Cnt_SignAll_Runtime);
            this.panel1.Controls.Add(this.Cnt_Signature_Runtime);
            this.panel1.Controls.Add(this.btn_Exit);
            this.panel1.Controls.Add(this.lbl_CauseNo);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lbl_TicketNo);
            this.panel1.Controls.Add(this.lbl_Phone);
            this.panel1.Controls.Add(this.lbl_Address);
            this.panel1.Controls.Add(this.lbl_Name);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(872, 50);
            this.panel1.TabIndex = 34;
            // 
            // Cnt_FPAll
            // 
            this.Cnt_FPAll.Enabled = true;
            this.Cnt_FPAll.Location = new System.Drawing.Point(710, 23);
            this.Cnt_FPAll.Name = "Cnt_FPAll";
            this.Cnt_FPAll.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Cnt_FPAll.OcxState")));
            this.Cnt_FPAll.Size = new System.Drawing.Size(28, 27);
            this.Cnt_FPAll.TabIndex = 76;
            this.Cnt_FPAll.Visible = false;
            // 
            // Cnt_PreviewSignature
            // 
            this.Cnt_PreviewSignature.Enabled = true;
            this.Cnt_PreviewSignature.Location = new System.Drawing.Point(750, 30);
            this.Cnt_PreviewSignature.Name = "Cnt_PreviewSignature";
            this.Cnt_PreviewSignature.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Cnt_PreviewSignature.OcxState")));
            this.Cnt_PreviewSignature.Size = new System.Drawing.Size(33, 17);
            this.Cnt_PreviewSignature.TabIndex = 75;
            this.Cnt_PreviewSignature.Visible = false;
            this.Cnt_PreviewSignature.SignCompleteStatus += new AxeSign3._ICaptureEvents_SignCompleteStatusEventHandler(this.Cnt_PreviewSignature_SignCompleteStatus);
            this.Cnt_PreviewSignature.MouseDownEvent += new System.EventHandler(this.Cnt_PreviewSignature_MouseDownEvent);
            // 
            // Cnt_SignAll_Runtime
            // 
            this.Cnt_SignAll_Runtime.Enabled = true;
            this.Cnt_SignAll_Runtime.Location = new System.Drawing.Point(809, 34);
            this.Cnt_SignAll_Runtime.Name = "Cnt_SignAll_Runtime";
            this.Cnt_SignAll_Runtime.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Cnt_SignAll_Runtime.OcxState")));
            this.Cnt_SignAll_Runtime.Size = new System.Drawing.Size(31, 15);
            this.Cnt_SignAll_Runtime.TabIndex = 74;
            this.Cnt_SignAll_Runtime.Visible = false;
            this.Cnt_SignAll_Runtime.SignCompleteStatus += new AxeSign3._ICaptureEvents_SignCompleteStatusEventHandler(this.Cnt_SignAll_Runtime_SignCompleteStatus);
            // 
            // Cnt_Signature_Runtime
            // 
            this.Cnt_Signature_Runtime.Enabled = true;
            this.Cnt_Signature_Runtime.Location = new System.Drawing.Point(782, 33);
            this.Cnt_Signature_Runtime.Name = "Cnt_Signature_Runtime";
            this.Cnt_Signature_Runtime.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Cnt_Signature_Runtime.OcxState")));
            this.Cnt_Signature_Runtime.Size = new System.Drawing.Size(33, 17);
            this.Cnt_Signature_Runtime.TabIndex = 73;
            this.Cnt_Signature_Runtime.Visible = false;
            this.Cnt_Signature_Runtime.SignCompleteStatus += new AxeSign3._ICaptureEvents_SignCompleteStatusEventHandler(this.Cnt_Signature_Runtime_SignCompleteStatus);
            this.Cnt_Signature_Runtime.MouseDownEvent += new System.EventHandler(this.Cnt_Signature_Runtime_MouseDownEvent);
            // 
            // lbl_CauseNo
            // 
            this.lbl_CauseNo.BackColor = System.Drawing.Color.White;
            this.lbl_CauseNo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CauseNo.Location = new System.Drawing.Point(283, 30);
            this.lbl_CauseNo.Name = "lbl_CauseNo";
            this.lbl_CauseNo.Size = new System.Drawing.Size(141, 17);
            this.lbl_CauseNo.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(268, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "/";
            // 
            // lbl_TicketNo
            // 
            this.lbl_TicketNo.BackColor = System.Drawing.Color.White;
            this.lbl_TicketNo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TicketNo.Location = new System.Drawing.Point(115, 30);
            this.lbl_TicketNo.Name = "lbl_TicketNo";
            this.lbl_TicketNo.Size = new System.Drawing.Size(149, 17);
            this.lbl_TicketNo.TabIndex = 18;
            // 
            // lbl_Phone
            // 
            this.lbl_Phone.BackColor = System.Drawing.Color.White;
            this.lbl_Phone.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Phone.Location = new System.Drawing.Point(562, 30);
            this.lbl_Phone.Name = "lbl_Phone";
            this.lbl_Phone.Size = new System.Drawing.Size(182, 17);
            this.lbl_Phone.TabIndex = 11;
            // 
            // lbl_Address
            // 
            this.lbl_Address.BackColor = System.Drawing.Color.White;
            this.lbl_Address.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Address.Location = new System.Drawing.Point(416, 7);
            this.lbl_Address.Name = "lbl_Address";
            this.lbl_Address.Size = new System.Drawing.Size(446, 17);
            this.lbl_Address.TabIndex = 9;
            // 
            // lbl_Name
            // 
            this.lbl_Name.BackColor = System.Drawing.Color.White;
            this.lbl_Name.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Name.Location = new System.Drawing.Point(99, 7);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(248, 17);
            this.lbl_Name.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(448, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "Phone Number :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(353, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "Address :";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ticket / Cause # :";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Client Name  :";
            // 
            // lblPageCounter
            // 
            this.lblPageCounter.BackColor = System.Drawing.Color.Transparent;
            this.lblPageCounter.Location = new System.Drawing.Point(789, 126);
            this.lblPageCounter.Name = "lblPageCounter";
            this.lblPageCounter.Size = new System.Drawing.Size(82, 13);
            this.lblPageCounter.TabIndex = 68;
            this.lblPageCounter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ESign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(242)))), ((int)(((byte)(245)))));
            this.ClientSize = new System.Drawing.Size(881, 687);
            this.Controls.Add(this.lblPageCounter);
            this.Controls.Add(this.pnl_ToolBar);
            this.Controls.Add(this.pnl_SignAll);
            this.Controls.Add(this.tab_Docs);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl_MainImage);
            this.Controls.Add(this.pnl_PreviewMain);
            this.Name = "ESign";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ESign_FormClosing);
            this.Load += new System.EventHandler(this.ESign_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PB_FirstImg)).EndInit();
            this.tab_Docs.ResumeLayout(false);
            this.pnl_PreviewMain.ResumeLayout(false);
            this.pnl_MainImage.ResumeLayout(false);
            this.pnl_MainImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_MainImage)).EndInit();
            this.pnl_SignAll.ResumeLayout(false);
            this.pnl_SignAll.PerformLayout();
            this.pnl_ToolBar_SignAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_FP_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Signature_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Fingerprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Initial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Signature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Signature_Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Initial_Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Fingerprint_Back)).EndInit();
            this.pnl_ToolBar.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_FPAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_PreviewSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_SignAll_Runtime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cnt_Signature_Runtime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Pnl_Shadow_FirstImg_X;
        private System.Windows.Forms.Panel Pnl_Shadow_FirstImg_Y;
        private System.Windows.Forms.PictureBox PB_FirstImg;
        private System.Windows.Forms.TabPage TabPg_LetterOfRep;
        private System.Windows.Forms.TabControl tab_Docs;
        private System.Windows.Forms.TabPage tabPg_Receipt;
        private System.Windows.Forms.TabPage TabPg_LetterOfContinuation;
        private System.Windows.Forms.TabPage TabPg_BondDocs;
        private System.Windows.Forms.TabPage TabPg_TrailLetter;
        private System.Windows.Forms.Panel Pnl_Shadow_FirstImg;
        private System.Windows.Forms.Panel pnl_PreviewMain;
        private System.Windows.Forms.Panel pnl_MainImage;
        private System.Windows.Forms.PictureBox PB_MainImage;
        private System.Windows.Forms.Panel pnl_SignAll;
        private System.Windows.Forms.PictureBox pb_Signature;
        private System.Windows.Forms.PictureBox pb_Fingerprint;
        private System.Windows.Forms.PictureBox pb_Initial;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pb_Signature_Back;
        private System.Windows.Forms.PictureBox pb_Initial_Back;
        private System.Windows.Forms.PictureBox pb_Fingerprint_Back;
        private System.Windows.Forms.PictureBox pb_Signature_Main;
        private System.Windows.Forms.PictureBox pb_FP_Main;
        private System.Windows.Forms.RadioButton rb_SalesRep;
        private System.Windows.Forms.RadioButton rb_Attorney;
        private System.Windows.Forms.RadioButton rb_Client;
        private System.Windows.Forms.Panel pnl_ToolBar;
        private System.Windows.Forms.Button btn_PreviousSign;
        private System.Windows.Forms.Button btn_SignAll;
        private System.Windows.Forms.Button btn_NextSign;
        private System.Windows.Forms.Button btn_Zoom;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_PrintCurrentReport;
        private System.Windows.Forms.Button btn_PrintAll;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel pnl_ToolBar_SignAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_CauseNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_TicketNo;
        private System.Windows.Forms.Label lbl_Phone;
        private System.Windows.Forms.Label lbl_Address;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Button btn_FirstSign;
        private System.Windows.Forms.Button btn_LastSign;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn_ClearFingerprint;
        private System.Windows.Forms.Button btn_DefaultSignature_Open;
        private System.Windows.Forms.Button btn_SignAll_Close;
        private System.Windows.Forms.Button btn_SignAll_InsertSignature;
        private System.Windows.Forms.Label lblPageCounter;
        private System.Windows.Forms.Button btn_SignAll_DeleteSignature;
        private System.Windows.Forms.Button btn_Page_Next;
        private System.Windows.Forms.Button btn_Page_Previous;
        private AxeSign3.AxesCapture Cnt_Signature_Runtime;
        private AxeSign3.AxesCapture Cnt_SignAll_Runtime;
        private System.Windows.Forms.RadioButton rb_DefaultSign;
        private System.Windows.Forms.RadioButton rb_AllDocSign;
        private System.Windows.Forms.RadioButton rb_SelectedDocSign;
        private AxeSign3.AxesCapture Cnt_PreviewSignature;
        private AxESFINGERPRINTLib.AxesFP Cnt_FPAll;
    }
}

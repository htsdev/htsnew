﻿using System;
using System.Data;
using FrameWorkEnation.Components;
using PolmWcfClient;
using POLMDTO;
using System.Collections.Generic;

namespace HTP.ClientController
{
    /// <summary>
    /// This class providing all methods to interact in Traffic program related to POLM.
    /// </summary>
    internal class PolmController
    {
        #region PolmWCFClientMethods

        #region BodilyDamage


        /// <summary>
        ///     Add Bodily Damage
        /// </summary>
        /// <param name="value">Bodily Damage Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddBodilyDamage(string value, bool isActive)
        {
            var polmdivisionalDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddBodilyDamage(polmdivisionalDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Bodily Damage
        /// </summary>
        /// <param name="id">Bodily Damage id</param>
        /// <param name="value">Bodily Damage Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateBodilyDamage(int id, string value, bool isActive)
        {
            var polmdivisionalDto = new PolmDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateBodilyDamage(polmdivisionalDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Bodily Damage
        /// </summary>
        /// <param name="id">Bodily Damage id</param>
        /// <returns>True/False</returns>
        public bool DeleteBodilyDamage(int id)
        {
            var polmdivisionalDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteBodilyDamage(polmdivisionalDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Bodily Damage
        /// </summary>
        /// <param name="isActive">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllBodilyDamage(bool? isActive)
        {
            var polmDto = new PolmDto { IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllBodilyDamage(polmDto);
            polmServiceClient.Close();
            return returnList;
        }

        /// <summary>
        ///     Get Bodily Damage of specific Source.
        /// </summary>
        /// <param name="sourceId">Source from where information came</param>
        /// <returns>List of PolmDivisionDto</returns>
        public List<PolmDivisionDto> GetAllBodilyDamageOfDivision(int sourceId)
        {
            var polmDivisionDto = new PolmDivisionDto { SourceId = sourceId };
            var polmServiceClient = new PolmServiceClient();
            var returnDataTable = polmServiceClient.GetAllBodilyDamageOfSource(polmDivisionDto);
            polmServiceClient.Close();
            return returnDataTable;
        }

        /// <summary>
        ///     Add Bodily Damage of Source.
        /// </summary>
        /// <param name="id">Bodily Damage id<</param>
        /// <param name="sourceId">Source from where information came</param>
        /// <returns>True/False</returns>
        public bool AddBodilyDamageOfSOurce(int id, int sourceId)
        {
            var polmDivisionalDto = new PolmDivisionDto { Id = id, SourceId = sourceId };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddBodilyDamageForSource(polmDivisionalDto);
            polmServiceClient.Close();
            return returnData;

        }

        /// <summary>
        ///     Delete Bodily Damage od Source.
        /// </summary>
        /// <param name="primaryId">Bodily Damage id<</param>
        /// <returns>True/False</returns>
        public bool DeleteBodilyDamageOfSOurce(int primaryId)
        {
            var polmDivisionalDto = new PolmDivisionDto { PrimaryId = primaryId };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteBodilyDamageForSource(polmDivisionalDto);
            polmServiceClient.Close();
            return returnData;

        }

        #endregion

        #region Case Status

        /// <summary>
        ///     Add Case Status
        /// </summary>
        /// <param name="value">Case Status Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddCaseStatus(string value, bool isActive)
        {
            var polmDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddCaseStatus(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Case Status
        /// </summary>
        /// <param name="id">Case Status id</param>
        /// <param name="value">Case Status Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateCaseStatus(int id, string value, bool isActive)
        {
            var polmDto = new PolmDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateCaseStatus(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Case Status
        /// </summary>
        /// <param name="id">Case Status id</param>
        /// <returns>True/False</returns>
        public bool DeleteCaseStatus(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteCaseStatus(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Case Status
        /// </summary>
        /// <param name="isActive">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllCaseStatus(bool? isActive)
        {
            var polmDto = new PolmDto { IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllCaseStatus(polmDto);
            polmServiceClient.Close();
            return returnList;
        }

        #endregion

        #region Contact Type

        /// <summary>
        ///     Add Contact Type
        /// </summary>
        /// <param name="value">Contact Type Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddContactType(string value, bool isActive)
        {
            var polmDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddContactType(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Contact Type
        /// </summary>
        /// <param name="id">Contact Type id</param>
        /// <param name="value">Contact Type Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateContactType(int id, string value, bool isActive)
        {
            var polmDto = new PolmDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateContactType(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Contact Type
        /// </summary>
        /// <param name="id">Contact Type id</param>
        /// <returns>True/False</returns>
        public bool DeleteContactType(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteContactType(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Contact Type
        /// </summary>
        /// <param name="isActive">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllContactType(bool? isActive)
        {
            var polmDto = new PolmDto { IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllContactType(polmDto);
            polmServiceClient.Close();
            return returnList;
        }

        #endregion

        #region Damage Value

        /// <summary>
        ///     Add Damage Value
        /// </summary>
        /// <param name="value">Damage Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddDamageValue(string value, bool isActive)
        {
            var polmDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddDamageValue(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Damage Value
        /// </summary>
        /// <param name="id">Damage Value id</param>
        /// <param name="value">Damage Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateDamageValue(int id, string value, bool isActive)
        {
            var polmDto = new PolmDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateDamageValue(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Damage Value
        /// </summary>
        /// <param name="id">Damage Value id</param>
        /// <returns>True/False</returns>
        public bool DeleteDamageValue(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteDamageValue(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Damage Value
        /// </summary>
        /// <param name="getAllActive">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllDamageValue(bool? getAllActive)
        {
            var polmDto = new PolmDto { IsActive = getAllActive };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllDamageValue(polmDto);
            polmServiceClient.Close();
            return returnList;
        }

        #endregion

        #region Division

        /// <summary>
        ///     Add Division
        /// </summary>
        /// <param name="value">Division Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddDivision(string value, bool isActive)
        {
            var polmdivisionalDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddDivision(polmdivisionalDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Division
        /// </summary>
        /// <param name="id">Division id</param>
        /// <param name="value">Division Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateDivision(int id, string value, bool isActive)
        {
            var polmdivisionalDto = new PolmDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateDivision(polmdivisionalDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Division
        /// </summary>
        /// <param name="id">Division id</param>
        /// <returns>True/False</returns>
        public bool DeleteDivision(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteDivision(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Division
        /// </summary>
        /// <param name="getAll">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllDivision(bool? getAll)
        {
            var polmDto = new PolmDto { IsActive = getAll };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllDivision(polmDto);
            polmServiceClient.Close();
            return returnList;
        }


        #endregion

        #region Language

        /// <summary>
        ///     Add Language
        /// </summary>
        /// <param name="value">Language Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddLanguage(string value, bool isActive)
        {
            var polmdivisionalDto = new PolmDivisionDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            return polmServiceClient.AddLanguage(polmdivisionalDto);
        }

        /// <summary>
        ///     Update Language
        /// </summary>
        /// <param name="id">Language id</param>
        /// <param name="value">Language Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateLanguage(int id, string value, bool isActive)
        {
            var polmDto = new PolmDivisionDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateLanguage(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Language
        /// </summary>
        /// <param name="id">Language id</param>
        /// <returns>True/False</returns>
        public bool DeleteLanguage(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteLanguage(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Languages
        /// </summary>
        /// <param name="getAll">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllLanguage(bool? getAll)
        {
            var polmDto = new PolmDto { IsActive = getAll };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllLanguages(polmDto);
            polmServiceClient.Close();
            return returnList;
        }


        #endregion

        #region QLMD

        /// <summary>
        ///     Add Quick Legal Matter Description
        /// </summary>
        /// <param name="value">Quick Legal Matter Description Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddQuickLegalMatterDescription(string value, bool isActive)
        {
            var polmDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddQuickLegalMatterDescription(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Quick Legal Matter Description
        /// </summary>
        /// <param name="id">Quick Legal Matter Description id</param>
        /// <param name="value">Quick Legal Matter Description Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateQuickLegalMatterDescription(int id, string value, bool isActive)
        {
            var polmDto = new PolmDto
                                                    {
                                                        Id = id,
                                                        Name = value,
                                                        IsActive = isActive
                                                    };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateQuickLegalMatterDescription(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Quick Legal Matter Description
        /// </summary>
        /// <param name="id">Quick Legal Matter Description id</param>
        /// <returns>True/False</returns>
        public bool DeleteQuickLegalMatterDescription(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteQuickLegalMatterDescription(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Quick Legal Matter Description
        /// </summary>
        /// <param name="getAll">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllQuickLegalMatterDescription(bool? getAll)
        {
            var polmDivisionalDto = new PolmDto { IsActive = getAll };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllQuickLegalMatterDescription(polmDivisionalDto);
            polmServiceClient.Close();
            return returnList;
        }

        /// <summary>
        ///     Get All Quick Legal Matter Description related to Source
        /// </summary>
        /// <param name="sourceType">Id of the source</param>
        /// <returns>List of PolmDivisionDto</returns>
        public List<PolmDivisionDto> GetAllQuickLegalMatterDescriptionOfSource(int sourceType)
        {
            var polmDivisionalDto = new PolmDivisionDto { SourceId = sourceType };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllQlmdOfSource(polmDivisionalDto);
            polmServiceClient.Close();
            return returnList;
        }

        /// <summary>
        ///     Add Quick Legal Matter Description of Source
        /// </summary>
        /// <param name="qlmdId">Id of Quick Legal Matter Description</param>
        /// <param name="sourceId">Id of the source</param>
        /// <returns>True/False</returns>
        public bool AddQlmdOfSOurce(int qlmdId, int sourceId)
        {
            var polmDivisionalDto = new PolmDivisionDto { SourceId = sourceId, Id = qlmdId };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddQlmdForSource(polmDivisionalDto);
            polmServiceClient.Close();
            return returnData;

        }

        /// <summary>
        ///     Delete Quick Legal Matter Description of Source
        /// </summary>
        /// <param name="primaryId">Quick Legal Matter Description id</param>
        /// <returns>True/False</returns>
        public bool DeleteQlmdOfSOurce(int primaryId)
        {
            var polmDivisionalDto = new PolmDivisionDto { PrimaryId = primaryId };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteQlmdForSource(polmDivisionalDto);
            polmServiceClient.Close();
            return returnData;

        }

        #endregion

        #region Source

        /// <summary>
        ///     Add Source
        /// </summary>
        /// <param name="value">Source Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool AddSource(string value, bool isActive)
        {
            var polmDto = new PolmDto { Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.AddSource(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Update Source
        /// </summary>
        /// <param name="id">Source id</param>
        /// <param name="value">Source Value</param>
        /// <param name="isActive">IsActive True/False</param>
        /// <returns>True/False</returns>
        public bool UpdateSource(int id, string value, bool isActive)
        {
            var polmDto = new PolmDto { Id = id, Name = value, IsActive = isActive };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.UpdateSource(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Delete Source
        /// </summary>
        /// <param name="id">Source id</param>
        /// <returns>True/False</returns>
        public bool DeleteSource(int id)
        {
            var polmDto = new PolmDto { Id = id };
            var polmServiceClient = new PolmServiceClient();
            var returnData = polmServiceClient.DeleteSource(polmDto);
            polmServiceClient.Close();
            return returnData;
        }

        /// <summary>
        ///     Get All Source
        /// </summary>
        /// <param name="getAll">True (to get all active)/False (to get all inactive)/Null (To get all active and inactive)</param>
        /// <returns>List of PolmDto</returns>
        public List<PolmDto> GetAllSource(bool? getAll)
        {
            var polmDivisionalDto = new PolmDto { IsActive = getAll };
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAllSource(polmDivisionalDto);
            polmServiceClient.Close();
            return returnList;
        }


        #endregion

        
        #endregion

        #region ProspectWCFClientMethods

        /// <summary>
        ///     Get Prospect Report
        /// </summary>
        /// <param name="prospectReportSearchDto">prospectReportSearchDto</param>
        /// <returns>List of type ProspectReportDto</returns>
        public List<ProspectReportDto> GetProspectReport(ProspectReportSearchDto prospectReportSearchDto)
        {
            var prospectClientService = new PolmServiceClient();
            prospectClientService.Open();
            var returnList = prospectClientService.GetProspectReport(prospectReportSearchDto);
            prospectClientService.Close();
            return returnList;
        }

        /// <summary>
        ///     Get Prospect
        /// </summary>
        /// <param name="clientId">Id of the Client.</param>
        /// <returns>ProspectDto</returns>
        public ProspectDto GetProspect(int clientId)
        {
            var prospectService = new PolmServiceClient();
            prospectService.Open();
            var returnprospectDto = prospectService.GetProspect(clientId);
            prospectService.Close();
            return returnprospectDto;
        }

        /// <summary>
        ///     Updated the Prospect
        /// </summary>
        /// <param name="prospectDto">prospectDto</param>
        /// <returns>True/False</returns>
        public bool UpdateProspect(ProspectDto prospectDto)
        {
            var prospectService = new PolmServiceClient();
            prospectService.Open();
            var returnData = prospectService.UpdateProspect(prospectDto);
            prospectService.Close();
            return returnData;
        }

        /// <summary>
        ///     Add Comments
        /// </summary>
        /// <param name="prospectDto">prospectDto</param>
        /// <returns>True/False</returns>
        public bool AddComments(ProspectDto prospectDto)
        {
            var prospectService = new PolmServiceClient();
            prospectService.Open();
            var returnData = prospectService.AddUserComments(prospectDto);
            prospectService.Close();
            return returnData > 0;
        }

        /// <summary>
        ///     Add Prospect
        /// </summary>
        /// <param name="prospectDto">prospectDto</param>
        /// <returns>0 or 1</returns>
        public int AddProspect(ProspectDto prospectDto)
        {
            var prospectService = new PolmServiceClient();
            prospectService.Open();
            var returnData = prospectService.AddProspect(prospectDto);
            prospectService.Close();
            return Convert.ToInt32(returnData);
        }

        /// <summary>
        ///     Get User Comments
        /// </summary>
        /// <param name="clientId">Id of the Client</param>
        /// <returns>List of type CommentsHistoryDto</returns>
        public List<CommentsHistoryDto> GetUserComments(int clientId)
        {
            var prospectService = new PolmServiceClient();
            prospectService.Open();
            var returnList = prospectService.GetUserComments(clientId);
            prospectService.Close();
            return returnList;
        }

        /// <summary>
        ///     Delete Attachments
        /// </summary>
        /// <param name="prospectDto">prospectDto</param>
        /// <returns>True False</returns>
        public bool DeleteAttachments(ProspectDto prospectDto)
        {
            var prospectService = new PolmServiceClient();
            prospectService.Open();
            var returnData = prospectService.DeleteAttachment(prospectDto);
            prospectService.Close();
            return returnData;
        }

        /// <summary>
        ///      Get All Polm Consultation History
        /// </summary>
        /// <param name="ticketId">Ticket Id of the Client</param>
        /// <returns>Data Table</returns>
        public DataTable GetAllPolmConsultationHistory(int ticketId)
        {
            var clsDb = new clsENationWebComponents();
            string[] key = { "@TicketId" };
            object[] value = { ticketId };
            return clsDb.Get_DT_BySPArr("[dbo].[USP_HTP_GetPolmConsultation]", key, value);
        }

        /// <summary>
        ///     Add Polm Consultation History
        /// </summary>
        /// <param name="ticketId">Ticket Id of the Client</param>
        /// <param name="qlmd">Quick Legal Matter Description</param>
        /// <param name="legalMatterDesc">Legal Matter Description</param>
        /// <param name="damage">Damage Value</param>
        /// <param name="bodilyDamage">Bodily Damage</param>
        /// <param name="attorney">Id of the Attorney</param>
        /// <param name="polmCaseId">Id of the Polm Case</param>
        /// <returns>True/False</returns>
        public bool AddPolmConsultationHistory(int ticketId, string qlmd, string legalMatterDesc, string damage, string bodilyDamage, int attorney, int polmCaseId)
        {
            var clsDb = new clsENationWebComponents();
            string[] key = { "@TicketID", "@QuickLegalMatter", "@LegalMatterDescription", "@Damage", "@BodilyDamage", "@AttorneyID", "@POLMCaseID" };
            object[] value = { ticketId, qlmd, legalMatterDesc, damage, bodilyDamage, attorney, polmCaseId };
            return Convert.ToBoolean(clsDb.ExecuteScalerBySp("[dbo].[USP_HTP_InsertPolmConsultationHistory]", key, value));
        }

        /// <summary>
        /// This method brings all information about the Attachments.
        /// </summary>
        /// <param name="clientId">Id of the client</param>
        /// <returns>List of type AttachmentDto</returns>
        public List<AttachmentDto> GetAttachments(int clientId)
        {
            var polmServiceClient = new PolmServiceClient();
            var returnList = polmServiceClient.GetAttachments(clientId);
            polmServiceClient.Close();
            return returnList;
        }


        #endregion
    }
}

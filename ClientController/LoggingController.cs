﻿using System.Collections.Generic;
using Lntech.Logging.Dto.DataTransferObjects;
using Lntech.Logging.Dto.Messages;
using Lntech.Logging.Service.Client;
using Lntech.Base;

namespace HTP.ClientController
{
    // Noufil 7897 06/17/2010 Class added
    /// <summary>
    ///     This class serves as the bridge from where web application with communicate with the service methods.
    /// </summary>
    internal class LoggingController : ControllerBase
    {
        /// <summary>
        ///     Returns all logs that exisit in our system with respect to searching critirea provided.
        /// </summary>
        /// <param name="loggingReportDto">loggingReportDto objec</param>
        /// <returns>list of LoggingReportDto</returns>
        internal List<LoggingReportDto> GetAllLogs(LoggingReportDto loggingReportDto)
        {
            // Create object of service client
            LoggingServiceClient client = new LoggingServiceClient();

            // Open client to communicate with service
            client.Open();

            // Delcare Request object
            LoggingReportRequest request = new LoggingReportRequest { RequestData = loggingReportDto };

            // Declare repsone object to get all Logs by calling Service method
            LoggingReportResponse response = client.GetLogs(request);

            // Close client to end all communication
            client.Close();

            //throw Exception/validation message if Acknowledge Type is Failure
            ValidateResponseSuccess(response);

            // return list of all logs
            return response.ResponseDataList;
        }

        /// <summary>
        ///     Returns all available applications from where exception has been logged
        /// </summary>
        /// <param name="applicationDto">applicationDto with property 'ApplicationName' having application name to search. Provide null to get all</param>
        /// <returns>List of ApplicationDto</returns>
        internal List<ApplicationDto> GetAllApplication(ApplicationDto applicationDto)
        {
            // Create object of service client
            LoggingServiceClient client = new LoggingServiceClient();

            // Open client to communicate with service
            client.Open();

            // Delcare Request object
            ApplicationRequest request = new ApplicationRequest { RequestData = applicationDto };

            // Declare repsone object to get all Logs by calling Service method
            ApplicationResponse response = client.GetAllApplications(request);

            // Close client to end all communication
            client.Close();

            //throw Exception/validation message if Acknowledge Type is Failure
            ValidateResponseSuccess(response);

            // return list of applications
            return response.ResponseDataList;
        }
    }
}

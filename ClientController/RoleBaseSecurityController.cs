﻿using System.Collections.Generic;
using RoleBasedSecurity.DataTransferObjects;


namespace HTP.ClientController
{
    /// <summary>
    /// This class providing all methods to interact in Traffic program related to Role Based Security.
    /// </summary>
    internal class RoleBasedSecurityController
    {
        #region Application

        /// <summary>
        /// This Method Adds Application in the User database.
        /// </summary>
        /// <param name="applicationDto">Application Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddApplication(ApplicationDto applicationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add Application.
            var returnData = roleBasedSecurityServiceClient.AddApplication(applicationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This Method Updates Application in the User database.
        /// </summary>
        /// <param name="applicationDto">Application Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateApplication(ApplicationDto applicationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update Application.
            var returnData = roleBasedSecurityServiceClient.UpdateApplication(applicationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }


        /// <summary>
        /// This Method Deletes Application in the User database.
        /// </summary>
        /// <param name="applicationDto">Application Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteApplication(ApplicationDto applicationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delete Application.
            var returnData = roleBasedSecurityServiceClient.DeleteApplication(applicationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This method converts datatable to appropirate list of Application Data Transfer Object.
        /// </summary>
        /// <param name="applicationDto">Application Data Transfer Object.</param>
        /// <returns>List of Application Data Transfer Object.</returns>
        public List<ApplicationDto> GetAllApplications(ApplicationDto applicationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Applications and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllApplications(applicationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }
        #endregion

        #region Role

        /// <summary>
        /// This methods Adds Role in the User Database.
        /// </summary>
        /// <param name="roleDto">Role Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddRole(RoleDto roleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add Role.
            var returnData = roleBasedSecurityServiceClient.AddRole(roleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Updates Role in the User Database.
        /// </summary>
        /// <param name="roleDto">Role Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateRole(RoleDto roleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update Role.
            var returnData = roleBasedSecurityServiceClient.UpdateRole(roleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Deletes Role in the User Database.
        /// </summary>
        /// <param name="roleDto">Role Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteRole(RoleDto roleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delete Role.
            var returnData = roleBasedSecurityServiceClient.DeleteRole(roleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about Roles.
        /// </summary>
        /// <param name="roleDto">Roles Data Transfer Object.</param>
        /// <returns>List of type Roles Data Transfer Object.</returns>
        public List<RoleDto> GetAllRole(RoleDto roleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Roles and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllRole(roleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }

        /// <summary>
        ///     This method used to Get all Divisions.
        /// </summary>
        /// <returns>List of type CompanyDto</returns>
        public List<CompanyDto> GetAllDivisions()
        {
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();
            roleBasedSecurityServiceClient.Open();
            var returnList = roleBasedSecurityServiceClient.GetAllCompanyByApplication(new UserDto { ApplicationId = 2 });
            roleBasedSecurityServiceClient.Close();
            return returnList;
        }

        #endregion

        #region Company

        /// <summary>
        /// This methods Adds Company in the User Database.
        /// </summary>
        /// <param name="companyDto">Company Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddCompany(CompanyDto companyDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add Compnay.
            var returnData = roleBasedSecurityServiceClient.AddCompany(companyDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Updates Company in the User Database.
        /// </summary>
        /// <param name="companyDto">Company Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateCompany(CompanyDto companyDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update Compnay.
            var returnData = roleBasedSecurityServiceClient.UpdateCompany(companyDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Deletes Company in the User Database.
        /// </summary>
        /// <param name="companyDto">Company Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteCompany(CompanyDto companyDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delete Compnay.
            var returnData = roleBasedSecurityServiceClient.DeleteCompany(companyDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about Company.
        /// </summary>
        /// <param name="companyDto">Company Data Transfer Object.</param>
        /// <returns>List of type Company Data Transfer Object.</returns>
        public List<CompanyDto> GetAllCompany(CompanyDto companyDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Companies and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllCompany(companyDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }
        #endregion

        #region Process

        /// <summary>
        /// This methods Adds Process in the User Database.
        /// </summary>
        /// <param name="processDto">Process Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddProcess(ProcessDto processDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add Process.
            var returnData = roleBasedSecurityServiceClient.AddProcess(processDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Updates Process in the User Database.
        /// </summary>
        /// <param name="processDto">Process Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateProcess(ProcessDto processDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Call the Role Based Security Client method to Update Process.
            var returnData = roleBasedSecurityServiceClient.UpdateProcess(processDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Deletes Process in the User Database.
        /// </summary>
        /// <param name="processDto">Process Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteProcess(ProcessDto processDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delete Process.
            var returnData = roleBasedSecurityServiceClient.DeleteProcess(processDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about Company.
        /// </summary>
        /// <param name="processDto">Process Data Transfer Object.</param>
        /// <returns>List of type Process Data Transfer Object.</returns>
        public List<ProcessDto> GetAllProcess(ProcessDto processDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Processes and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllProcess(processDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }
        #endregion

        #region Associstion

        /// <summary>
        /// This methods Adds Association in the User Database.
        /// </summary>
        /// <param name="associationDto">Association Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddAssociation(AssociationDto associationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add Association.
            var returnData = roleBasedSecurityServiceClient.AddAssociation(associationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Updates Association in the User Database.
        /// </summary>
        /// <param name="associationDto">Association Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateAssociation(AssociationDto associationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update Association.
            var returnData = roleBasedSecurityServiceClient.UpdateAssociation(associationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Deletes Association in the User Database.
        /// </summary>
        /// <param name="associationDto">Association Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteAssociation(AssociationDto associationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delete Association.
            var returnData = roleBasedSecurityServiceClient.DeleteAssociation(associationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about Association.
        /// </summary>
        /// <param name="associationDto">Association Data Transfer Object.</param>
        /// <returns>List of type Association Data Transfer Object.</returns>
        public List<AssociationDto> GetAllAssociation(AssociationDto associationDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Associations and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllAssociation(associationDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }
        #endregion

        #region UserInRoles

        /// <summary>
        /// This methods Adds UserInRoles in the User Database.
        /// </summary>
        /// <param name="userRoleDto">UserInRoles Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddUserInRole(UserInRolesDto userRoleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add User In Role.
            var returnData = roleBasedSecurityServiceClient.AddUserInRole(userRoleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This method Updates UserInRoles in the User Database.
        /// </summary>
        /// <param name="userRoleDto">UserInRoles Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateUserInRole(UserInRolesDto userRoleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update User In Role.
            var returnData = roleBasedSecurityServiceClient.UpdateUserInRole(userRoleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This method Deletes UserInRoles in the User Database.
        /// </summary>
        /// <param name="userRoleDto">UserInRoles Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteUserInRole(UserInRolesDto userRoleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delete User In Role.
            var returnData = roleBasedSecurityServiceClient.DeleteUserInRole(userRoleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about User.
        /// </summary>
        /// <param name="userRoleDto">UserInRoles Data Transfer Object.</param>
        /// <returns>List of type User Data Transfer Object.</returns>
        public List<UserInRolesDto> GetAllUserInRole(UserInRolesDto userRoleDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all User In Role and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllUserInRole(userRoleDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;

        }
        #endregion

        #region UserAccessRights

        /// <summary>
        /// This methods get all information about All Menu Role Rights.
        /// </summary>
        /// <param name="userAccessRightsDto">UserAccessRights Data Transfer Object.</param>
        /// <returns>List of type UserAccessRights Data Transfer Object.</returns>
        public List<UserAccessRightsDto> GetAllMenuRoleRights(UserAccessRightsDto userAccessRightsDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Menu Role Rights and assigned to the List.
            var returnList = roleBasedSecurityServiceClient.GetAllMenuRoleRights(userAccessRightsDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnList;
        }

        /// <summary>
        /// This methods get all information about All Process Role Rights.
        /// </summary>
        /// <param name="userAccessRightsDto">UserAccessRights Data Transfer Object.</param>
        /// <returns>List of type UserAccessRights Data Transfer Object.</returns>
        public List<UserAccessRightsDto> GetAllProcessRoleRights(UserAccessRightsDto userAccessRightsDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Process Role Rights and assigned to the List.
            var returnList = roleBasedSecurityServiceClient.GetAllProcessRoleRights(userAccessRightsDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnList;
        }

        /// <summary>
        /// This methods Updates UserAccessRights against the Menu in the User Database.
        /// </summary>
        /// <param name="userAccessRightsDto">UserAccessRights Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateMenuRoleRights(UserAccessRightsDto userAccessRightsDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update Menu Role Rights.
            var returnList = roleBasedSecurityServiceClient.UpdateMenuRoleRights(userAccessRightsDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnList;
        }

        /// <summary>
        /// This methods Updates UserAccessRights against the Process in the User Database.
        /// </summary>
        /// <param name="userAccessRightsDto">UserAccessRights Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateProcessRoleRights(UserAccessRightsDto userAccessRightsDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update Process Role Rights.
            var returnList = roleBasedSecurityServiceClient.UpdateProcessRoleRights(userAccessRightsDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnList;
        }


        #endregion

        #region User

        /// <summary>
        /// This methods Adds User in the User Database.
        /// </summary>
        /// <param name="userDto">User Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool AddUser(UserDto userDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Add User.
            var returnData = roleBasedSecurityServiceClient.AddUser(userDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods Updates User in the User Database.
        /// </summary>
        /// <param name="userDto">User Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool UpdateUser(UserDto userDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Update User.
            var returnData = roleBasedSecurityServiceClient.UpdateUser(userDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        ///     This method used to Get all users by role
        /// </summary>
        /// <param name="companyId">Company/Division Id</param>
        /// <param name="roleId">Role Id</param>
        /// <returns>List of UserDto</returns>
        public List<UserDto> GetUserByRole(int companyId, int roleId)
        {
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();
            var returnlist = roleBasedSecurityServiceClient.GetAllUserByApplicationCompanyRole(new UserDto
            {
                ApplicationId = 2,
                CompnyId = companyId,
                RoleId = roleId
            });
            roleBasedSecurityServiceClient.Close();
            return returnlist;

        }

        /// <summary>
        /// This methods Deletes User in the User Database.
        /// </summary>
        /// <param name="userDto">User Data Transfer Object.</param>
        /// <returns>True/False</returns>
        public bool DeleteUser(UserDto userDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Delte User.
            var returnData = roleBasedSecurityServiceClient.DeleteUser(userDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about User.
        /// </summary>
        /// <param name="userDto">User Data Transfer Object.</param>
        /// <returns>List of type User Data Transfer Object.</returns>
        public List<UserDto> GetAllUser(UserDto userDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Users and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllUser(userDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about User by Id.
        /// </summary>
        /// <param name="userDto">User Data Transfer Object.</param>
        /// <returns>List of type User Data Transfer Object.</returns>
        public List<UserDto> GetUserById(UserDto userDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Users by ID and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetUserById(userDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }

        /// <summary>
        /// This methods get all information about Traffic Program User by Id.
        /// </summary>
        /// <param name="userDto">User Data Transfer Object.</param>
        /// <returns>List of type User Data Transfer Object.</returns>
        public List<UserDto> GetUserByTpId(UserDto userDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Users by Traffic Program ID and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetUserByTpId(userDto);

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }

        /// <summary>
        /// This Method Returns information about the all Attorneys.
        /// </summary>
        /// <returns>List of type User Data Transfer Object.</returns>
        public List<UserDto> GetAllAttorney()
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the method to get all Attorneys and assigned to the List.
            var returnData = roleBasedSecurityServiceClient.GetAllAttorney();

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated List.
            return returnData;
        }

        /// <summary>
        /// This method Resets User Password.
        /// </summary>
        /// <param name="userLoginDto">userLogin Data Transfer Object.</param>
        /// <returns>True/False</returns>     
        public bool ResetPassword(UserLoginDto userLoginDto)
        {
            //Decalred the instance of the Role Based Security Service Client
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();

            //Open the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Open();

            //Call the Role Based Security Client method to Reset Password.
            var returnData = roleBasedSecurityServiceClient.ResetPassword(new UserLoginDto
            {
                LoginUserId = userLoginDto.LoginUserId,
                LoginUserPassword = userLoginDto.LoginUserPassword
            });

            //Close the Connection by using Role Based Security Client
            roleBasedSecurityServiceClient.Close();

            //Returning the Populated Object.
            return returnData;
        }

        /// <summary>
        ///     This method used to Get all User. 
        /// </summary>
        /// <returns>List of type UserDto</returns>
        public List<UserDto> GetAllUsers()
        {
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();
            roleBasedSecurityServiceClient.Open();
            var returnList = roleBasedSecurityServiceClient.GetAllUserByApplication(new UserDto { ApplicationId = 2 });
            roleBasedSecurityServiceClient.Close();
            return returnList;
        }

        /// <summary>
        ///     This method used to Get all roles of user
        /// </summary>
        /// <param name="userId">Id of User</param>
        /// <returns>List of RoleDto</returns>
        public List<RoleDto> GetUserRole(int userId)
        {
            var roleBasedSecurityServiceClient = new RoleBasedSecurityServiceClient();
            var returnList = roleBasedSecurityServiceClient.GetAllRoleByUserApplication(new UserDto { ApplicationId = 2, UserId = userId });
            roleBasedSecurityServiceClient.Close();
            return returnList;
        }

        #endregion
    }
}



﻿using System.Collections.Generic;
using RoleBasedSecurity.DataTransferObjects;


namespace RoleBasedSecurity.WCFClient
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "IRoleBaseSecurityService")]
    public interface IRoleBaseSecurityService
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddApplication", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddApplicationResponse")]
        bool AddApplication(ApplicationDto applicationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateApplication", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateApplicationResponse")]
        bool UpdateApplication(ApplicationDto applicationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteApplication", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteApplicationResponse")]
        bool DeleteApplication(ApplicationDto applicationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllApplications", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllApplicationsResponse")]
        List<ApplicationDto> GetAllApplications(ApplicationDto applicationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddAssociation", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddAssociationResponse")]
        bool AddAssociation(AssociationDto associationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateAssociation", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateAssociationResponse")]
        bool UpdateAssociation(AssociationDto associationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteAssociation", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteAssociationResponse")]
        bool DeleteAssociation(AssociationDto associationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllAssociation", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllAssociationResponse")]
        List<AssociationDto> GetAllAssociation(AssociationDto associationDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddCompany", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddCompanyResponse")]
        bool AddCompany(CompanyDto companyDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateCompany", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateCompanyResponse")]
        bool UpdateCompany(CompanyDto companyDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteCompany", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteCompanyResponse")]
        bool DeleteCompany(CompanyDto companyDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllCompany", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllCompanyResponse")]
        List<CompanyDto> GetAllCompany(CompanyDto companyDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddProcess", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddProcessResponse")]
        bool AddProcess(ProcessDto processDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateProcess", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateProcessResponse")]
        bool UpdateProcess(ProcessDto processDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteProcess", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteProcessResponse")]
        bool DeleteProcess(ProcessDto processDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllProcess", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllProcessResponse")]
        List<ProcessDto> GetAllProcess(ProcessDto processDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddRights", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddRightsResponse")]
        bool AddRights(RightsDto rightsDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateRights", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateRightsResponse")]
        bool UpdateRights(RightsDto rightsDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteRights", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteRightsResponse")]
        bool DeleteRights(RightsDto rightsDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllRights", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllRightsResponse")]
        List<RightsDto> GetAllRights(RightsDto rightsDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddRoleResponse")]
        bool AddRole(RoleDto roleDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateRoleResponse")]
        bool UpdateRole(RoleDto roleDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteRoleResponse")]
        bool DeleteRole(RoleDto roleDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllRoleResponse")]
        List<RoleDto> GetAllRole(RoleDto roleDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddUserInRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddUserInRoleResponse")]
        bool AddUserInRole(UserInRolesDto userInRolesDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateUserInRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateUserInRoleResponse")]
        bool UpdateUserInRole(UserInRolesDto userInRolesDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteUserInRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteUserInRoleResponse")]
        bool DeleteUserInRole(UserInRolesDto userInRolesDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllUserInRole", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllUserInRoleResponse")]
        List<UserInRolesDto> GetAllUserInRole(UserInRolesDto userInRolesDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/AddUser", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/AddUserResponse")]
        bool AddUser(UserDto userDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/UpdateUser", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/UpdateUserResponse")]
        bool UpdateUser(UserDto userDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/DeleteUser", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/DeleteUserResponse")]
        bool DeleteUser(UserDto userDto);

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/IRoleBaseSecurityService/GetAllUser", ReplyAction = "http://tempuri.org/IRoleBaseSecurityService/GetAllUserResponse")]
        List<UserDto> GetAllUser(UserDto userDto);
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public interface IRoleBaseSecurityServiceChannel : IRoleBaseSecurityService, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public partial class RoleBaseSecurityServiceClient : System.ServiceModel.ClientBase<IRoleBaseSecurityService>, IRoleBaseSecurityService
    {

        public RoleBaseSecurityServiceClient()
        {
        }

        public RoleBaseSecurityServiceClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public RoleBaseSecurityServiceClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public RoleBaseSecurityServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public RoleBaseSecurityServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public bool AddApplication(ApplicationDto applicationDto)
        {
            return Channel.AddApplication(applicationDto);
        }

        public bool UpdateApplication(ApplicationDto applicationDto)
        {
            return Channel.UpdateApplication(applicationDto);
        }

        public bool DeleteApplication(ApplicationDto applicationDto)
        {
            return Channel.DeleteApplication(applicationDto);
        }

        public List<ApplicationDto> GetAllApplications(ApplicationDto applicationDto)
        {
            return Channel.GetAllApplications(applicationDto);
        }

        public bool AddAssociation(AssociationDto associationDto)
        {
            return Channel.AddAssociation(associationDto);
        }

        public bool UpdateAssociation(AssociationDto associationDto)
        {
            return Channel.UpdateAssociation(associationDto);
        }

        public bool DeleteAssociation(AssociationDto associationDto)
        {
            return Channel.DeleteAssociation(associationDto);
        }

        public List<AssociationDto> GetAllAssociation(AssociationDto associationDto)
        {
            return Channel.GetAllAssociation(associationDto);
        }

        public bool AddCompany(CompanyDto companyDto)
        {
            return Channel.AddCompany(companyDto);
        }

        public bool UpdateCompany(CompanyDto companyDto)
        {
            return Channel.UpdateCompany(companyDto);
        }

        public bool DeleteCompany(CompanyDto companyDto)
        {
            return Channel.DeleteCompany(companyDto);
        }

        public List<CompanyDto> GetAllCompany(CompanyDto companyDto)
        {
            return Channel.GetAllCompany(companyDto);
        }

        public bool AddProcess(ProcessDto processDto)
        {
            return Channel.AddProcess(processDto);
        }

        public bool UpdateProcess(ProcessDto processDto)
        {
            return Channel.UpdateProcess(processDto);
        }

        public bool DeleteProcess(ProcessDto processDto)
        {
            return Channel.DeleteProcess(processDto);
        }

        public List<ProcessDto> GetAllProcess(ProcessDto processDto)
        {
            return Channel.GetAllProcess(processDto);
        }

        public bool AddRights(RightsDto rightsDto)
        {
            return Channel.AddRights(rightsDto);
        }

        public bool UpdateRights(RightsDto rightsDto)
        {
            return Channel.UpdateRights(rightsDto);
        }

        public bool DeleteRights(RightsDto rightsDto)
        {
            return Channel.DeleteRights(rightsDto);
        }

        public List<RightsDto> GetAllRights(RightsDto rightsDto)
        {
            return Channel.GetAllRights(rightsDto);
        }

        public bool AddRole(RoleDto roleDto)
        {
            return Channel.AddRole(roleDto);
        }

        public bool UpdateRole(RoleDto roleDto)
        {
            return Channel.UpdateRole(roleDto);
        }

        public bool DeleteRole(RoleDto roleDto)
        {
            return Channel.DeleteRole(roleDto);
        }

        public List<RoleDto> GetAllRole(RoleDto roleDto)
        {
            return Channel.GetAllRole(roleDto);
        }

        public bool AddUserInRole(UserInRolesDto userInRolesDto)
        {
            return Channel.AddUserInRole(userInRolesDto);
        }

        public bool UpdateUserInRole(UserInRolesDto userInRolesDto)
        {
            return Channel.UpdateUserInRole(userInRolesDto);
        }

        public bool DeleteUserInRole(UserInRolesDto userInRolesDto)
        {
            return Channel.DeleteUserInRole(userInRolesDto);
        }

        public List<UserInRolesDto> GetAllUserInRole(UserInRolesDto userInRolesDto)
        {
            return Channel.GetAllUserInRole(userInRolesDto);
        }

        public bool AddUser(UserDto userDto)
        {
            return Channel.AddUser(userDto);
        }

        public bool UpdateUser(UserDto userDto)
        {
            return Channel.UpdateUser(userDto);
        }

        public bool DeleteUser(UserDto userDto)
        {
            return Channel.DeleteUser(userDto);
        }

        public List<UserDto> GetAllUser(UserDto userDto)
        {
            return Channel.GetAllUser(userDto);
        }
    }
}
